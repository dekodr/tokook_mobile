package dev.tokook.id;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {
  TextView versionName;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash_screen);
    versionName = findViewById(R.id.tvVersion);
    versionName.setText("V" + BuildConfig.VERSION_NAME);
  }

  @Override
  protected void onStart() {
    super.onStart();
    new Handler().postDelayed((Runnable) () -> {

      // setelah loading maka akan langsung berpindah ke home activity
      Intent home = new Intent(this, MainActivity.class);
      Bundle extras = getIntent().getExtras();
      if (extras != null) {
        home.putExtras(extras);
      }
      startActivity(home);
      finish();

    }, 3000);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }
}
