### Containers Folder
A container is what they call a "Smart Component" in Redux.  It is a component
which knows about Redux.  They are usually used as "Screens".

Also located in here are 2 special containers: `App.js` and `RootContainer.js`.

`App.js` is first component loaded after `index.ios.js` or `index.android.js`.  The purpose of this file is to setup Redux or any other non-visual "global" modules.  Having Redux setup here helps with the hot-reloading process in React Native during development as it won't try to reload your sagas and reducers should your colors change (for example).

`RootContainer.js` is the first visual component in the app.  It is the ancestor of all other screens and components.

You'll probably find you'll have great mileage in Ignite apps without even touching these 2 files.  They, of course, belong to you, so when you're ready to add something non-visual like Firebase or something visual like an overlay, you have spots to place these additions.



# Halaman Kategori
1. Install `tabs-section-list` 

    ```shell
    yarn add react-native-tabs-section-list
    ```
    
2. Import package
    ```js
        <SectionList
        tabBarStyle={[page.tesTab]}
        sections={SECTIONS}
        keyExtractor={item => item.title}
        stickySectionHeadersEnabled={true}
        scrollToLocationOffset={5}
        renderTab={({
            title,
            isActive,
            icon_active,
            icon_inactive,
        }) => (
            <View
            style={[
                page.tabContainer,
                isActive ? page.tabActive : null,
                {
                borderTopColor: isActive
                    ? Colors.borderGrey
                    : 'rgba(0,0,0,0)',
                borderBottomColor: isActive
                    ? Colors.borderGrey
                    : 'rgba(0,0,0,0)',
                },
            ]}>
            <Image
                source={isActive ? icon_active : icon_inactive}
                style={[page.tabIcon]}
            />
            <Text
                style={[
                page.tabText,
                {
                    color: Colors.textGreyDark,
                },
                ]}>
                {title}
            </Text>
            </View>
        )}
        renderSectionHeader={({section}) => (
            <View
            style={[
                page.textDivider,
                {
                borderTopColor:
                    section.key - 1 == 0
                    ? 'rgba(0,0,0,0)'
                    : Colors.borderGrey,
                width: width,
                },
            ]}>
            <Text style={page.textDividerText}>
                {section.title}
            </Text>
            </View>
        )}
        renderItem={({item, index}) => (
            <SectionCategory
            image={item.image}
            title={item.title}
            onPress={() => _handleMarketplaceCategoryDetail()}
            styles={{
                // backgroundColor: '#ddd',
                marginLeft: index % 3 == 3 ? 0 : 30,
                width: width / 4 - 32,
                marginBottom: 12,
            }}
            />
        )}
    />
    ```

## Menu Kategori
Gunakan variabel berikut pada `<SectionList sections="{{SECTIONS}}" />`

```javascript
    const SECTIONS = [
        {
            title: 'Pria',
            key: 1,
            icon_inactive: require('../../../../assets/icons/icon-men-inactive.png'),
            icon_active: require('../../../../assets/icons/icon-men-active.png'),
            data: {{data}}
        },
    ];
```

Key | Type | Description | Mandatory
--- | ---- | ----------- | --------
title | string | Judul Menu | ✅
key | int | Index Menu | ✅
icon_inactive | url | Icon menu | ✅
icon_active | url | Icon menu aktif | ✅
data | array(object) | data array detail kategori | ✅



## Detail Kategori Card Component
Komponen ini digunakan dalam halaman ```Category``` sebagai area klik user untuk menuju halaman ```Detail Category```

```javascript
  <SectionCategory
    image={item.image}
    title={item.title}
    onPress={() => _handleMarketplaceCategoryDetail()}
    styles={{
      marginLeft: index % 3 == 3 ? 0 : 30,
      width: width / 4 - 32,
      marginBottom: 12,
    }}
  />
```

Key | Type | Description | Mandatory
--- | ---- | ----------- | --------
item.image | url         |url gambar| ✅
item.title | string      |judul product| ✅
onPress    | function    |function pada saat produk diklik| ✅

                      


# Animated Header Component

1. import component
``` import AnimatedHeaderComponent from '../../../../Components/AnimatedHeader'; ```

2. define const for updating offset touch value
``` const offsetHeader = useRef(new Animated.Value(0)).current; ```

3. AnimatedHeaderComponent should be paired with scrollview wrapper in order to work and update the offset value. ``` <AnimatedHeaderComponent animatedValue={offsetHeader} headerText={text_to_show} /> ```
    
    ``` 
    <ScrollView
        showsVerticalScrollIndicator={false}
        style={[styles.scrollArea, {marginTop: -68}]}
        onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: offsetHeader}}}],
            {useNativeDriver: false},
        )}>
        {/* B O D Y */}
            (.....)
        {/* B O D Y - - - END */}
    </ScrollView>
    ```



