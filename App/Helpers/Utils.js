import Share from 'react-native-share';
import React, {Component} from 'react';
import PushNotification from 'react-native-push-notification';
// var PushNotification = require("react-native-push-notification");
import Config from 'react-native-config';
const rupiah = number => {
  number += '';
  let {x, x1, x2} = 0;
  x = number.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? ',' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
  }
  return 'Rp ' + x1;
};
const handleShare = options => {
  Share.open(options)
    .then(res => {})
    .catch(err => {});
};
export class PushController extends Component {
  componentDidMount() {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function(token) {},

      // (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
        // process the notification here
        // required on iOS only
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // Android only
      senderID: '1090501687137',
      // iOS only
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }

  render() {
    return null;
  }
}
const compressImage = image => {
  return `${Config.IMAGE_COMPRESS_URL}${image}&device=mobile-sm`;
};
module.exports = {
  rupiah,
  handleShare,
  PushController,
  compressImage,
};
