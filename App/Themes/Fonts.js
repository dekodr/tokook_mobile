import {Dimensions, Platform, PixelRatio} from 'react-native';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

const scale = SCREEN_WIDTH / 480;

function normalize(size) {
  const newSize = size * scale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}

const Fonts = {
  size_28: normalize(28) + 1,
  size_26: normalize(26) + 1,
  size_24: normalize(24) + 1,
  size_22: normalize(22) + 1,
  size_20: normalize(20) + 1,
  size_18: normalize(18) + 1,
  size_16: normalize(16) + 1,
  size_14: normalize(14) + 1,

  skeleton_anim_time: 0.1,
  skeleton_delay_anim_time: 0.1,
};

export default Fonts;
