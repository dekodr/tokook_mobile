import {StyleSheet} from 'react-native';
import {Colors} from '../../Themes/';

export default StyleSheet.create({
  applicationView: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },

  myImage: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
});
