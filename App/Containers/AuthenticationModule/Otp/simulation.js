import React, {useEffect, useState} from 'react';

import {
  ScrollView,
  Text,
  Image,
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Modal,
  ActivityIndicator,
  Linking,
  Keyboard,
} from 'react-native';

import {Images, Colors} from '../../../Themes';
import {FontAwesome5Icon} from '../../../assets/icons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
// Styles
import {connect} from 'react-redux';
import AuthActions from './../../../Redux/AuthenticationRedux';
import styles from './styles';
import * as c from '../../../Components';
function OtpScreen(props) {
  const {navigation, dispatch} = props;
  const [code, setCode] = useState('');
  const [errMsg, setErrMsg] = useState(null);
  const [timeLeft, setTimeLeft] = useState(30);
  const getParamFunc = navigation.getParam('func');
  console.log('cek params', getParamFunc);
  useEffect(() => {
    if (props.errSystemReOtp) {
      setErrMsg(props.errSystemReOtp.message);
      setTimeout(() => {
        setErrMsg(null);
        // props.dispatch(AuthActions.loginEmailPostReset());
      }, 3000);
    }
  }, [props.errSystemReOtp]);

  useEffect(() => {
    if (props.authLoginData.login.token) {
      setTimeLeft(30);
    }
  }, [props.authLoginData.login.token]);
  useEffect(() => {
    if (props.isSuccess) {
      Keyboard.dismiss();
    }
  }, [props.isSuccess]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }

    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
      getParamFunc.setTimeLeftHandler(120 - 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);

  // useEffect(() => {
  //   startTimer();
  //   clockCall = setInterval(() => {
  //     decrementClock();
  //   }, 1000);
  //   return () => {
  //     clearInterval(clockCall);
  //   };
  // }, []);
  useEffect(() => {
    if (props.errSystem.message) {
      setErrMsg(props.errSystem.message);
      setTimeout(() => {
        setErrMsg(null);
        props.dispatch(AuthActions.otpPostReset());
      }, 3000);
    }
  }, [props.errSystem.message]);
  const handleWhatsapp = () => {
    Linking.openURL('whatsapp://send?phone=62213108024').catch(() =>
      Linking.openURL(
        'https://apps.apple.com/id/app/whatsapp-messenger/id310633997',
      ),
    );
  };
  //   const B = children => {
  //     return <Text style={styles.boldFont}>{...children}</Text>;
  //   };
  const handleGoBack = () => {
    props.navigation.navigate({routeName: 'LoginScreen'});
  };

  const handleResendOtp = () => {
    if (timeLeft === 0) {
      const data = {
        details: {
          email: props.userLogin.data.email
            ? props.userLogin.data.email
            : props.userLogin.data.user.email,
          source: 'login_or_register',
          type: 'email',
        },
        getParamFunc,
      };
      props.dispatch(AuthActions.loginEmailPostRequest(data));
    } else null;
  };
  const handleSubmit = () => {
    const data = {
      email: props.userLogin.data.user.email,
      code: code,
      token: props.authLoginData.login.token,
    };

    if (props.authLoginData.login.isRegistered) {
      props.dispatch(AuthActions.loginOtpPostRequest(data));
    } else {
      props.dispatch(AuthActions.registerOtpPostRequest(data));
    }
  };

  const B = props => (
    <Text onPress={props.onPress} style={props.style}>
      {props.children}
    </Text>
  );
  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={{width: '100%', height: '70%'}}>
        <View style={{width: '100%', flexDirection: 'row'}}>
          <TouchableOpacity onPress={handleGoBack}>
            <Image
              style={{
                resizeMode: 'contain',
                width: 25,
                height: 25,
                marginRight: 10,
              }}
              source={Images.arrowBack}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            marginTop: 35,
            color: 'white',
            fontSize: 30,
            textAlign: 'center',
            fontFamily: 'NotoSans-ExtraBold',
            letterSpacing: 1.98,
          }}>
          Masukkan OTP
        </Text>

        <Text
          style={{
            marginTop: 4,
            color: 'white',

            textAlign: 'center',
            fontFamily: 'NotoSans',
            letterSpacing: 1.98,
          }}>
          {
            'Terima kasih telah bergabung dengan kami. Kode verifikasi telah dikirim ke '
          }{' '}
          <B style={{fontFamily: 'NotoSans-Bold', color: 'white'}}>
            {props.userLogin.data.email
              ? props.userLogin.data.email
              : props.userLogin.data.user.email}
          </B>
        </Text>
        <View style={styles.itemCenter}>
          <c.PinVerify
            autoFocus={props.isSuccess ? false : true}
            cellStyle={styles.cellStyle}
            cellStyleFocused={styles.cellStyleFocused}
            textStyle={styles.txtCellStyle}
            textStyleFocused={styles.textStyleFocused}
            value={code}
            onTextChange={code => setCode(code)}
          />
          <View
            style={{
              top: 30,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={handleResendOtp}
              disabled={props.isFetchingOtp}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  opacity: timeLeft === 0 ? 1 : 0.4,
                  color: 'white',
                  marginTop: 2,
                }}>
                {' '}
                Kirim Ulang
              </Text>
            </TouchableOpacity>

            <Text
              style={{
                marginLeft: 6,
                fontFamily: 'NotoSans-ExtraBold',
                color: 'white',
                fontSize: 15,
                opacity: 1,
              }}>
              00:{timeLeft < 10 ? '0' : ''}
              {timeLeft}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          disabled={props.isFetching}
          onPress={handleSubmit}
          style={{
            marginTop: 80,
            alignSelf: 'center',
            width: '100%',
            height: 45,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 25,
          }}>
          {props.isFetching || props.isFetchingOtp ? (
            <ActivityIndicator
              color={Colors.blueBold}
              animating={true}
              size="large"
            />
          ) : (
            <Text
              style={{
                fontSize: 17,
                color: Colors.blueBold,
                fontFamily: 'NotoSans-Bold',
              }}>
              Konfirmasi
            </Text>
          )}
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: 'row',
          height: '30%',
          width: '85%',
          alignSelf: 'center',

          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: 16,
            fontFamily: 'NotoSans',
            bottom: 45,
            color: 'white',
            alignSelf: 'flex-end',
          }}>
          Butuh bantuan?
        </Text>

        <TouchableOpacity
          onPress={handleWhatsapp}
          style={{
            bottom: 40,
            marginTop: 25,
            marginLeft: 12,
            backgroundColor: 'white',
            height: 40,
            justifyContent: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 25,
            paddingLeft: 10,
            paddingRight: 10,
            alignSelf: 'flex-end',
          }}>
          <Image
            style={{
              resizeMode: 'contain',
              width: 25,
              height: 25,
              marginRight: 10,
            }}
            source={Images.waIcon}
          />
          <Text
            style={{marginTop: 0, fontFamily: 'NotoSans', letterSpacing: 1.79}}>
            Hubungi Kami
          </Text>
        </TouchableOpacity>
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={errMsg != null ? true : false}
        //  onRequestClose={this.closeModal}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
          }}>
          <View
            style={{
              width: 300,
              height: 85,
              backgroundColor: 'white',
              borderRadius: 12,
              borderWidth: 0.7,
            }}>
            <IconAntDesign
              // onPress={stepBack}
              color={'red'}
              name="warning"
              size={21}
              style={{alignSelf: 'center', marginTop: 8}}
            />
            <Text
              style={{
                alignSelf: 'center',
                marginTop: 12,
                fontFamily: 'NotoSans',
                letterSpacing: 0.98,
                textAlign: 'center',
                bottom: 5,
              }}>
              {errMsg}
            </Text>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}
const mapDispatchToProps = dispatch => ({
  dispatch,
});

const mapStateToProps = state => {
  return {
    userLogin: state.auth.loginEmailPostData,
    authLoginData: state.auth.loginEmailPostData.data,
    errSystem: state.auth.otpPostData.error,
    isSuccess: state.auth.otpPostData.success,
    errSystemReOtp: state.auth.loginEmailPostData.error,
    isFetchingOtp: state.auth.loginEmailPostData.fetching,
    isFetching: state.auth.otpPostData.fetching,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OtpScreen);
