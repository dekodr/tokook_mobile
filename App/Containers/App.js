import '../Config';
import DebugConfig from '../Config/DebugConfig';
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import RootContainer from './RootContainer';
import createStore from '../Redux';
import configureSentry from '../Config/SentryConfig';
import {PersistGate} from 'redux-persist/integration/react';
import {ActivityIndicator} from 'react-native';

import {MenuProvider} from 'react-native-popup-menu';
require('moment/locale/id.js');

const {store, persistor} = createStore();

configureSentry();
/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={<ActivityIndicator />} persistor={persistor}>
        <MenuProvider>
          <RootContainer />
        </MenuProvider>
      </PersistGate>
    </Provider>
  );
}

// allow reactotron overlay for fast design in dev mode
export default DebugConfig.useReactotron ? console.tron.overlay(App) : App;
