import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Linking,
  TextInput,
  Alert,
  BackHandler,
  ToastAndroid,
} from 'react-native';
import {Container, Content, CheckBox, Button} from 'native-base';
import RBSheet from 'react-native-raw-bottom-sheet';
import * as c from '../../Components';
// import * as func from '../../helpers/handle';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFA from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import TokoActions from './../../Redux/TokoRedux';
import {Images} from './../../Themes';

const fullWidth = Dimensions.get('screen').width;
const fullHeight = Dimensions.get('screen').height;

function HomeScreen(props) {
  const [step, setStep] = useState(1);
  const [formToko, setFormToko] = useState({
    tncCheck: true,
    tokoLink: null,
    nameToko: null,
    whatsappToko: null,
    businessTypeSelected: {
      id: 12,
      icon:
        'https://payok-toko-bucket.s3-ap-southeast-1.amazonaws.com/business-type/Others.png',
    },
    inputOtherBusinessType: null,
    tokopediaLink: null,
    shopeeLink: null,
    blibliLink: null,
    instagramLink: null,
    facebookLink: null,
    lineLink: null,
    youtubeLink: null,
  });
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
  });
  const [listBusinessType, setListBusinessType] = useState([]);
  const [addTokoSuccess, setAddTokoSuccess] = useState(null);

  const refInputBusinessType = useRef();

  useEffect(() => {
    props.dispatch(TokoActions.getBusinessTypeRequest(props.access_token));
    const backAction = () => {
      ToastAndroid.show('hh', ToastAndroid.SHORT);
      // BackHandler.exitApp();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
    // func.handleGetBusinessType(setListBusinessType, props.dispatch);
  }, []);

  useEffect(() => {
    if (addTokoSuccess) {
      props.navigation.navigate('Toko');
    }
  }, [addTokoSuccess]);

  useEffect(() => {
    setFormToko({...formToko, isBusinessTypeEmpty: false});
    if (step === 2 && formToko.businessTypeSelected.id === 12) {
      refInputBusinessType.current.focus();
    }
  }, [formToko.businessTypeSelected.id]);

  const stepBack = () => {
    setStep(step - 1);
  };

  const stepNext = () => {
    const {
      isBusinessTypeEmpty,
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
    } = tokoValidation;

    const {
      nameToko,
      tokoLink,
      whatsappToko,
      tokopediaLink,
      shopeeLink,
      blibliLink,
      instagramLink,
      facebookLink,
      lineLink,
      youtubeLink,
      businessTypeSelected,
      inputOtherBusinessType,
      tncCheck,
    } = formToko;

    const data = {
      business_type_id: businessTypeSelected.id,
      name: nameToko,
      link: tokoLink,
      whatsapp: whatsappToko,
      instagram: instagramLink,
      facebook: facebookLink,
      line: lineLink,
      youtube: youtubeLink,
      tokopedia: tokopediaLink,
      shopee: shopeeLink,
      blibli: blibliLink,
    };
    switch (step) {
      case 1:
        //Validate first
        if (!nameToko) {
          setTokoValidation({...tokoValidation, isNameTokoEmpty: true});
        }
        if (!tokoLink) {
          setTokoValidation({...tokoValidation, isLinkTokoEmpty: true});
        }
        if (!whatsappToko) {
          setTokoValidation({...tokoValidation, isWhatsappEmpty: true});
        }

        if (
          tncCheck &&
          isNameTokoEmpty === false &&
          isLinkTokoEmpty === false &&
          isWhatsappEmpty === false
        ) {
          setStep(step + 1);
        }
        break;
      case 2:
        if (businessTypeSelected.id) {
          if (businessTypeSelected.id === 12 && !inputOtherBusinessType) {
            setTokoValidation({...tokoValidation, isBusinessTypeEmpty: true});
          } else {
            setStep(step + 1);
          }
        }
        break;
      case 3:
        props.dispatch(TokoActions.tokoPostRequest(data));
        // func.handleAddToko(setAddTokoSuccess, props.dispatch, data);

        break;

      default:
        break;
    }
  };

  const createMerchant = () => {
    const {
      tncCheck,
      nameToko,
      tokoLink,
      whatsappToko,
      tokopediaLink,
      shopeeLink,
      blibliLink,
      instagramLink,
      facebookLink,
      lineLink,
      youtubeLink,
      businessTypeSelected,
      inputOtherBusinessType,
    } = formToko;

    const {
      isBusinessTypeEmpty,
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
    } = tokoValidation;
    return step == 1 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <Text style={c.Styles.txtCreateMerchant}>Buka Toko Online</Text>
        </View>
        <ScrollView
          style={c.Styles.viewTokoStepContent}
          showsVerticalScrollIndicator={false}>
          <Text style={c.Styles.lineHeightTwenty}>
            Fitur ini dibuat untuk kamu yang ingin memiliki website usaha kamu
            sendiri. Bagikan dan tingkatkan penjualan kamu.
          </Text>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={c.Styles.txtInputWithIcon}>
              <View style={c.Styles.marginRightTen}>
                <c.Image source={Images.toko} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  autoCapitalize="words"
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Nama Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, nameToko: text});
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: text ? false : true,
                    });
                  }}
                  value={nameToko}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: nameToko ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              Nama toko di website kamu
            </Text>
            {isNameTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Nama toko harus diisi
              </Text>
            )}
          </View>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image source={Images.at} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Link Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({
                      ...formToko,
                      tokoLink: text.replace(' ', '-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: text ? false : true,
                    });
                  }}
                  value={tokoLink}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: tokoLink ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              toko.payok.id/
              <Text style={{fontWeight: '600', fontSize: 14, letterSpacing: 1}}>
                {!tokoLink ? 'link-toko' : tokoLink}
              </Text>
            </Text>
            {isLinkTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Link toko harus diisi
              </Text>
            )}
          </View>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={require('../../assets/image/whatsapp.png')}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  keyboardType="numeric"
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Nomor Whatsapp"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, whatsappToko: text});
                    setTokoValidation({
                      ...tokoValidation,
                      isWhatsappEmpty: text ? false : true,
                    });
                  }}
                  value={whatsappToko}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isWhatsappEmpty: whatsappToko ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              Agar pembeli bisa menghubungi kamu
            </Text>
            {isWhatsappEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Nomor whatsapp harus diisi
              </Text>
            )}
          </View>
          {/* <View style={c.Styles.viewTnCWrapper}>
            <CheckBox
              style={{left: 0}}
              checked={tncCheck}
              color={tncCheck ? c.Colors.newBlue : c.Colors.grayWhite}
              onPress={() => setFormToko({...formToko, tncCheck: !tncCheck})}
            />
            <Text style={c.Styles.marginLeftTen}>
              Saya setuju dengan
              <Text
                onPress={() =>
                  Linking.openURL('http://payok.id/terms-and-conditions/')
                }
                style={{fontWeight: '600', color: c.Colors.newBlue}}>
                {' '}
                Syarat dan Ketentuan
              </Text>
            </Text>
          </View> */}
        </ScrollView>
      </View>
    ) : step == 2 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <IconAntDesign
            onPress={stepBack}
            name="arrowleft"
            size={21}
            style={c.Styles.marginTopFive}
          />
          <Text style={[c.Styles.txtCreateMerchant, c.Styles.marginLeftTen]}>
            Jenis Usaha
          </Text>
        </View>
        <ScrollView
          style={[c.Styles.viewTokoStepContent]}
          showsVerticalScrollIndicator={false}>
          <View
            style={[
              c.Styles.flexRow,
              c.Styles.viewheaderDay,
              c.Styles.justifyspaceBetween,
            ]}>
            <View>
              <View
                style={[
                  c.Styles.paddingTen,
                  {
                    borderRadius: 100,
                    backgroundColor: c.Colors.blueWhite,
                  },
                ]}>
                <c.Image
                  source={{uri: businessTypeSelected.icon}}
                  style={c.Styles.imgaddNotificationToTransaction}
                />
              </View>
            </View>
            <TouchableOpacity style={{width: '80%'}}>
              <View style={c.Styles.borderBottom}>
                <TextInput
                  onChangeText={text =>
                    setFormToko({...formToko, inputOtherBusinessType: text})
                  }
                  ref={refInputBusinessType}
                  editable={
                    businessTypeSelected.id && businessTypeSelected.id !== 12
                      ? false
                      : true
                  }
                  placeholder="Masukkan jenis usaha kamu"
                  style={[c.Styles.txtcategoryNotification]}>
                  {businessTypeSelected.id && businessTypeSelected.id !== 12
                    ? businessTypeSelected.name
                    : inputOtherBusinessType}
                </TextInput>
              </View>
              {isBusinessTypeEmpty ? (
                <View style={c.Styles.viewCheck}>
                  <Text
                    style={[
                      c.Styles.txtsubcategoryNotification,
                      {color: 'red'},
                    ]}>
                    Wajib diisi
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>
          </View>
          <View style={[c.Styles.viewTokoStep2WrapCategory]}>
            {props.businessType.businessType != null
              ? props.businessType.businessType.map(item => {
                  return (
                    <TouchableOpacity
                      key={item.id}
                      onPress={() =>
                        setFormToko({...formToko, businessTypeSelected: item})
                      }
                      style={[
                        c.Styles.justifyCenter,
                        c.Styles.alignItemsCenter,
                        c.Styles.marginTopFifteen,
                        c.Styles.marginBottomFifteen,
                        {width: '33.33%'},
                      ]}>
                      <View
                        style={[
                          c.Styles.paddingTen,
                          {
                            borderRadius: 100,
                            backgroundColor: c.Colors.blueWhite,
                          },
                        ]}>
                        <c.Image
                          source={{uri: item.icon}}
                          style={c.Styles.imgaddNotificationToTransaction}
                        />
                      </View>
                      <Text
                        style={{
                          marginTop: 8,
                          textAlign: 'center',
                          fontSize: 12,
                        }}>
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  );
                })
              : null}
          </View>
        </ScrollView>
      </View>
    ) : step == 3 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <IconAntDesign
            onPress={stepBack}
            name="arrowleft"
            size={21}
            style={c.Styles.marginTopFive}
          />
          <Text style={[c.Styles.txtCreateMerchant, c.Styles.marginLeftTen]}>
            Link Akun Lain
          </Text>
        </View>
        <ScrollView
          style={c.Styles.viewTokoStepContent}
          showsVerticalScrollIndicator={false}>
          <View style={c.Styles.viewCreateMerchantSectionWrapper}>
            <Text style={c.Styles.txtBold}>E-Commerce</Text>
            <Text style={c.Styles.lineHeightTwenty}>
              Untuk memberi informasi ke calon pembeli bahwa kamu juga berjualan
              di e-commerce.
            </Text>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.tokpedCircleIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.tokopedia.com/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, tokopediaLink: text});
                  }}
                  value={tokopediaLink}
                />
              </View>
            </View>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.shopeeCircleIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.shopee.co.id/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, shopeeLink: text});
                  }}
                  value={shopeeLink}
                />
              </View>
            </View>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.blibliCircleIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.blibli.com/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, blibliLink: text});
                  }}
                  value={blibliLink}
                />
              </View>
            </View>
          </View>
          <View style={c.Styles.viewCreateMerchantSectionWrapper}>
            <Text style={{fontWeight: '600'}}>Social Media</Text>
            <Text>Supaya calon pembeli lebih mengenal kamu.</Text>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.instagramIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.instagram.com/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, instagramLink: text});
                  }}
                  value={instagramLink}
                />
              </View>
            </View>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.facebookIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.facebook.com/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, facebookLink: text});
                  }}
                  value={facebookLink}
                />
              </View>
            </View>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.lineMedsosIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.line.com/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, lineLink: text});
                  }}
                  value={lineLink}
                />
              </View>
            </View>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image
                  source={Images.youtubeIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="https://www.youtube.com/user/nama-toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={text => {
                    setFormToko({...formToko, youtubeLink: text});
                  }}
                  value={youtubeLink}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    ) : (
      <View />
    );
  };

  return (
    <View
      style={{
        width: '100%',
        height: fullHeight,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <TouchableOpacity
        onPress={() => refRBSheet.open()}
        style={{
          backgroundColor: '#073f99',
          paddingVertical: 10,
          paddingHorizontal: 20,
          borderRadius: 10,
        }}>
        <Text style={{color: '#fff'}}>Toko</Text>
      </TouchableOpacity>
      <RBSheet
        ref={ref => {
          refRBSheet = ref;
        }}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.9}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <View style={{paddingHorizontal: 25, marginBottom: 75}}>
          {createMerchant()}
          <Button onPress={stepNext} style={c.Styles.btnCreateMerchant}>
            <Text style={c.Styles.txtCreateMerchantBtn}>
              {step === 1 ? 'Buat Toko' : step === 2 ? 'Next' : 'Selesai'}
            </Text>
          </Button>
        </View>
      </RBSheet>
    </View>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const mapStateToProps = state => {
  return {
    businessType: state.toko.getBusinessTypeData.data,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeScreen);
