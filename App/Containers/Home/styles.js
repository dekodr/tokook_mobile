import {StyleSheet} from 'react-native';
import {Colors} from '../../../Themes';

export default StyleSheet.create({
  containerrStep1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtCreateMerchant: {
    fontSize: 18,
    fontWeight: '600',
    marginTop: 10,
    marginBottom: 5,
  },
  viewTokoStepContent: {
    height: '83%',
  },
  lineHeightTwenty: {
    lineHeight: 20,
  },
});
