import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  ImageBackground,
  Image,
  BackHandler,
  SafeAreaView,
  Animated,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import styles from '../../Intro/styles';
import page from './styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
import SupplierTab from '../../../../Components/SupplierTab';
import ButtonIcon from '../../../../Components/ButtonIcon';
import AnimatedHeaderComponent from '../../../../Components/AnimatedHeader';
import {PaymentOption, DeliveryOption} from '../../../../Components/Option';

function MarketplaceSupplier(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);

  let {height, width} = Dimensions.get('window');

  const offsetHeader = useRef(new Animated.Value(0)).current;

  const headerSearchOpacity = offsetHeader.interpolate({
    inputRange: [0, 100],
    outputRange: [1, 0],
  });
  
  return (
    <Container style={styles.container}>
      {/* H E A D E R */}
      <View
        style={[
          styles.fixedHeader,
          {
            justifyContent: 'space-between',
            backgroundColor: 'transparent',
            zIndex: 2,
            elevation: 0,
            borderBottomColor: 'rgba(0,0,0,0)',
            shadowColor: 'rgba(0,0,0,0)',
          },
        ]}>
        <Animated.View style={{
          position: 'absolute',
          left: 0,
          top: 0,
          backgroundColor: Colors.blueBold,
          height: 68,
          width: width,
          opacity: headerSearchOpacity,
        }}></Animated.View>
        <Pressable onPress={handleGoBack}>
          <ButtonIcon
            onPress={() => handleGoBack()}
            icon="keyboard-backspace"
            color="#fff"
            size={Fonts.size_28}
            style={{
              backgroundColor: Colors.blueBold,
              height: 48,
              width: 48,
              borderRadius: 48,
              alignItems: 'center',
              justifyContent: 'center',
              // marginRight: -12,
            }}
          />
        </Pressable>
        <Pressable style={[styles.searchBar, {marginLeft: 6, backgroundColor: 'transparent'}]} onPress={() => toSearchScreen()}>
          <Animated.View style={{backgroundColor: '#fff', width: '100%', opacity: headerSearchOpacity, flexDirection: 'row', alignItems: 'center'}}>
            <View style={styles.searchBarIcon}>
              <MaterialCommunityIcons
                name={'magnify'}
                color={Colors.textGreyLight}
                size={Fonts.size_24}
              />
            </View>
            <View style={styles.searchBarInput}>
              <Text style={styles.searchBarInputPlaceholder}>Cari Barang</Text>
            </View>
          </Animated.View>
        </Pressable>
        {/* CART BUTTON */}
        <View style={[styles.cartButtonWrapper]}>
          <View style={[styles.cartIndicator]}>
            <View style={[styles.cartIndicatorInner, {top: -3}]}>
              <Text style={styles.cartIndicatorInnerText}>2</Text>
            </View>
          </View>
          <ButtonIcon
            onPress={() => _handleCart()}
            style={[styles.cart]}
            icon="cart-outline"
              color="#fff"
              size={Fonts.size_24}
              style={{
                backgroundColor: Colors.blueBold,
                height: 48,
                width: 48,
                borderRadius: 48,
                alignItems: 'center',
                justifyContent: 'center',
                // marginRight: -12,
              }}
          />
        </View>
      </View>
        {/* <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color='rgba(0,0,0,0)'
              size={25}
            />
          </Pressable>
          <Pressable style={styles.searchBar} onPress={() => toSearchScreen()}>
            <View style={styles.searchBarIcon}>
              <MaterialCommunityIcons
                name={'magnify'}
                color={Colors.textGreyLight}
                size={Fonts.size_24}
              />
            </View>
            <View style={styles.searchBarInput}>
              <Text style={styles.searchBarInputPlaceholder}>Cari Barang</Text>
            </View>
          </Pressable>
          {/* CART BUTTON
          <View style={[styles.cartButtonWrapper, {width: 32, height: 32,}]}>
            <View style={[styles.cartIndicator]}>
              <View style={[styles.cartIndicatorInner]}>
                <Text style={styles.cartIndicatorInnerText}>2</Text>
              </View>
            </View>
            <ButtonIcon
              onPress={() => _handleCart()}
              style={[styles.cart]}
              icon="cart-outline"
              color="#fff"
              size={Fonts.size_24 + 4}
              style={{
                // backgroundColor: '#000',
                height: 32,
                width: 32,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 0,
              }}
            />
          </View>
        </View> */}
      {/* H E A D E R - - - END */}

      <AnimatedHeaderComponent animatedValue={offsetHeader} headerText="Toko Segala Ada" />

      <ScrollView
        style={[styles.scrollArea, {backgroundColor: '#fff', marginTop: -68}]}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: offsetHeader}}}],
          {useNativeDriver: false},
        )}>
        {/* B O D Y */}
          <SafeAreaView style={[styles.body, {backgroundColor: 'rgba(0,0,0,0)', marginTop: 68}]}>
            <View style={[styles.bodyInner, {backgroundColor: 'rgba(0,0,0,0)'}]}>
                {/* C O N T E N T */}
                  <View style={[styles.content, {backgroundColor: 'rgba(0,0,0,0)'}]}>
                    <View style={[styles.contentInner, {marginTop: 0}]}>

                      {/* Section Supplier Header */}
                      <View style={[page.header]}>
                        <ImageBackground source={require('../../../../assets/image/honda-jazz-hero.png')} style={[page.headerProfile]}>
                        </ImageBackground>
                        
                        <View style={page.headerInfo}>
                          <Text numberOfLines={1} style={[page.headerName]}>Toko Segala Ada</Text>

                          <View style={[page.headertStoreLocation, {marginBottom: 6}]}>
                            <Image 
                              source={require('../../../../assets/image/location_pin.png')} 
                              style={[page.headertStoreLocationIcon]} />
                            <Text numberOfLines={1} style={[page.headerStoreLocationText]}>Menteng, Jakarta Pusat</Text>
                          </View>

                          <View style={[page.headerProdukTotal, {marginLeft: 3}]}>
                            <Text numberOfLines={1} style={[page.headerProdukTotalText]}>50 Produk</Text>
                          </View>                       
                        </View>
                      </View>
                      {/* Section Tab Supplier */}
                      <View style={{marginBottom: 25, flex:1, height: '100%'}}>
                        <SupplierTab />
                      </View>
                    </View>
                  </View>
                {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
        {/* B O D Y - - - END */}
      </ScrollView>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceSupplier);
