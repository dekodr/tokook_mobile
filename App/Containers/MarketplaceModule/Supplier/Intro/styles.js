import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const { width, height } = Dimensions.get('window');

const page = StyleSheet.create({

  header: {
    // backgroundColor: '#ddd',
    flexDirection: 'row',
    paddingHorizontal: 12,
    marginBottom: 24,
  },
  headerProfile: {
    position: 'relative',
    width: 75,
    height: 75,
    borderRadius: 75,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  headerInfo: {
    flex: 1,
    marginLeft: 12,
  },
  headerName: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_18,
    letterSpacing: .6,
    paddingHorizontal: 8,
    paddingVertical: 6,
    lineHeight: 20,
  },
  headertStoreLocation: {
    paddingHorizontal: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headertStoreLocationIcon: {
    height: 15,
    width: 11,
    marginRight: 7,
  },
  headerStoreLocationText: {
    fontSize: Fonts.size_14+1,
    color: Colors.textGreyDark,
    // textTransform: 'uppercase',
    letterSpacing: .6,
  },
  headerProdukTotal: {
    // marginVertical: 8,
    paddingLeft: 22,
  },
  headerProdukTotalText: {
    fontSize: Fonts.size_14+1,
    color: Colors.textGreyDark,
    // textTransform: 'uppercase',
    letterSpacing: .6,
  },
  ProductPhotoWrapper: {
    
  },
  ProductName: {
    fontFamily: 'NotoSans-Regular',
    paddingHorizontal: 8,
    paddingVertical: 6,
    fontSize: Fonts.size_16,
    color: Colors.textGreyDark,
    lineHeight: 20,
  },
  ProductStoreLocation: {
    position: 'absolute',
    right: 8,
    left: 2,
    bottom: 6,
    
  },
  ProductStoreLocationIcon: {
    
  },
  ProductStoreLocationText: {
    
  },

});

export default page;