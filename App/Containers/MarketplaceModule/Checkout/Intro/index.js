import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  SafeAreaView,
  ImageBackground,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import uuid from 'react-native-uuid';
import {StackActions, NavigationActions} from 'react-navigation';

import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import func from './../../../../Helpers/Utils';
import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
import VerticalTabBar from '../../../../Components/SupplierTab';
import tc from '../../../TokoModule/Intro/styles';
import styles from '../../Intro/styles';
import page from './styles';
import SupStyle from '../../Supplier/Intro/styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';

import ButtonIcon from '../../../../Components/ButtonIcon';
// import Button from '../../../../Components/Button';
import Button from '../../../../Components/Button_V2';

import {PaymentOption, DeliveryOption} from '../../../../Components/Option';
import MarketplaceActions from '../../../../Redux/MarketPlaceRedux';
// import TabViewVertical from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {padEnd} from 'lodash-es';
import {mergeRight} from 'ramda';
const RenderComponent = props => {
  return (
    <TextInput
      key={props.index}
      style={{borderWidth: 2, borderColor: 'orange'}}
      value={props.data[props.index].name}
      onChangeText={val => {
        let newArray = [...props.data];
        newArray[props.index].name = val;
        props.setData(newArray);
        console.log(props.data); //always rerender when type one character.. please help!!
      }}
    />
  );
};
// const RenderComponent = props => {
//   return (
//     <TextInput
//       key={props.index}
//       style={{borderWidth: 2, borderColor: 'orange'}}
//       value={
//         props.data[props.parentIndex].product_data[props.index].markup_price
//       }
//       onChangeText={val => {
//         let newArray = [...props.data];
//         newArray[props.parentIndex].product_data[
//           props.index
//         ].markup_price = val;
//         props.setOrderData(newArray);
//         console.log(props.data); //always rerender when type one character.. please help!!
//       }}
//     />
//   );
// };
function Category(props) {
  const {
    dispatch,
    navigation,
    getFinalCartData,
    getOwnerTokoCourierData,
    getCheckoutBuyerAndSenderData,
    getShippingServices,
    getPaymentChannelData,
    tokoId,
    isFetchingCheckout,
    paymentAdminFeeData,
  } = props;
  console.log('paymentAdminFeeData', paymentAdminFeeData);
  const [data, setData] = useState([
    {id: 1, name: 'dono'},
    {id: 2, name: 'kasino'},
    {id: 3, name: 'indro'},
  ]);
  const sellerPrices = useRef();
  const parentScrollRef = useRef();
  const [orderData, setOrderData] = useState([]);
  const [isPaymentMethEmpty, setIsPaymentEmpty] = useState(false);
  const [orderIndex, setOrderIndex] = useState(-1);
  const [paymentChannel, setPaymentChannel] = useState({fee: 0});
  const refRBSheet = useRef();
  useEffect(() => {
    dispatch(MarketplaceActions.getPaymentChannelRequest());
    dispatch(MarketplaceActions.postCheckoutReset());
  }, []);
  useEffect(() => {
    if (getFinalCartData) {
      const res = getFinalCartData.map((item, index) => {
        return {
          ...item,
          markup_price: 0,
          courier_pickup: '',
        };
      });
      setOrderData(JSON.parse(JSON.stringify(res)));
    }
  }, [getFinalCartData]);
  const handleGoBack = () => {
    navigation.goBack(null);
  };
  const _handleGetPriceCartOrder = item => {
    let total = 0;
    item.product_data.map((value, index) => {
      total +=
        value.markup_price > value.price
          ? Number(value.markup_price)
          : Number(value.price);
    });
    return func.rupiah(total);
  };
  const _handleAdminFee = () => {
    if (paymentChannel.fee === undefined) {
      return 0;
    } else {
      if (paymentChannel.fee === 0) {
        return 0;
      } else {
        if (paymentAdminFeeData !== null) {
          const res = paymentAdminFeeData.data.pay.filter(
            x => x.id === paymentChannel.id,
          );
          console.log('res', res);
          return res[0].fee;
        } else {
          return 0;
        }
      }
    }
  };
  const _handleSubTotalOrder = () => {
    var temp = [...orderData];
    let total = 0;
    orderData.map((v, index) => {
      v.product_data.map((value, i) => {
        total +=
          value.markup_price > value.price
            ? Number(value.markup_price)
            : Number(value.price);
      });
    });
    return total;
  };
  const _handlePickingFee = () => {
    let total = 0;
    orderData.map((value, index) => {
      total += value.courier_pickup === '' ? 0 : value.courier_pickup.cost;
    });
    return total;
  };
  const _renderOrder = ({index, item}) => {
    let parentIndex = index;
    return (
      <View style={[page.orderWrapper, {marginTop: 12}]}>
        <View style={[page.sectionInfoRow, page.orderMerchantRow]}>
          <Text style={[page.sectionInfoTitleText, {width: '27.5%'}]}>
            Merchant
          </Text>
          <View style={[styles.PlaceholderWrapper, {flex: 1}]}>
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
              }}>
              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
            </View>
            <Text
              numberOfLines={1}
              style={[page.sectionInfoText, {fontFamily: 'NotoSans-Bold'}]}>
              {item.supplier_name}
            </Text>
          </View>
        </View>

        {/* Pesanan List */}
        {item.product_data.map((item, i) => {
          return (
            <View style={[page.orderRow]}>
              <View style={page.orderRowUpper}>
                <View style={[styles.PlaceholderWrapper]}>
                  <View
                    style={{
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      zIndex: 1,
                    }}>
                    {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                  </View>
                  <ImageBackground
                    source={{uri: item.images[0].url}}
                    style={[page.orderImage]}></ImageBackground>
                </View>

                <View
                  style={[
                    styles.PlaceholderWrapper,
                    {marginLeft: 6, flex: 1, height: 64},
                  ]}>
                  <View
                    style={{
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      zIndex: 1,
                    }}>
                    {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                  </View>
                  <View
                    style={{
                      width: '100%',
                      height: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <View style={page.orderInfo1}>
                      <View
                        style={{
                          flexDirection: 'row',
                          marginBottom: 4,
                        }}>
                        <Text
                          numberOfLines={1}
                          style={[
                            page.orderInfoText1,
                            {
                              fontFamily: 'NotoSans-Bold',
                              color: Colors.blueBold,
                              marginRight: 8,
                            },
                          ]}>
                          {item.quantity}x
                        </Text>
                        <Text
                          numberOfLines={2}
                          style={[
                            page.orderInfoText1,
                            {
                              fontFamily: 'NotoSans-Bold',
                              flex: 1,
                              marginRight: 6,
                            },
                          ]}>
                          {item.name}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text numberOfLines={1} style={[page.orderInfoText2]}>
                          {item.variant},
                        </Text>
                        <Text numberOfLines={1} style={[page.orderInfoText2]}>
                          {item.Varian2}
                        </Text>
                      </View>
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        page.orderInfoText1,
                        {width: '40%', letterSpacing: 0},
                      ]}>
                      {func.rupiah(item.price)}
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={[
                  page.orderRowUnder,
                  {
                    backgroundColor:
                      item.key === item.length - 1 ? '#ddd' : 'rgba(0,0,0,0)',
                  },
                ]}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 4,
                    width: '100%',
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      page.orderInfoText2,
                      {
                        color: Colors.blueBold,
                        marginLeft: 'auto',
                        marginRight: 8,
                      },
                    ]}>
                    Masukan harga jual
                  </Text>
                  <View style={[styles.PlaceholderWrapper]}>
                    <TextInput
                      keyboardType={'decimal-pad'}
                      onChangeText={x => {
                        const temp = [...orderData];

                        temp[index].product_data[i].markup_price = x;
                        setOrderData(temp);
                        console.log('temp', temp);
                        setOrderIndex(index);
                        dispatch(
                          MarketplaceActions.postPaymentAdminFeeRequest({
                            amount:
                              _handleSubTotalOrder() + _handlePickingFee(),
                          }),
                        );
                      }}
                      placeholder={
                        item.markup_price < 1
                          ? '0'
                          : item.markup_price.toString()
                      }
                      style={[
                        page.orderProdutInput,
                        {
                          color:
                            item.markup_price > item.price
                              ? Colors.black
                              : Colors.textGreyLight,
                        },
                      ]}
                      underlineColorAndroid="transparent"
                      value={item.markup_price.toString()}
                    />
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 4,
                    width: '100%',
                  }}>
                  <View
                    style={[styles.PlaceholderWrapper, {marginLeft: 'auto'}]}>
                    <View
                      style={{
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                        zIndex: 1,
                      }}>
                      {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                    </View>
                    <Text
                      numberOfLines={1}
                      style={[
                        page.orderInfoText2,
                        {
                          color:
                            item.markup_price < item.price
                              ? Colors.redDark
                              : Colors.textGreen,
                          marginLeft: 'auto',
                        },
                      ]}>
                      Untung {func.rupiah(item.markup_price - item.price)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          );
        })}
        {/* Total Price Pesanan */}
        <View
          style={[
            page.sectionInfoRow,
            page.orderMerchantRow,
            {
              marginVertical: 12,
              borderBottomColor: 'rgba(0,0,0,0)',
              alignItems: 'center',
            },
          ]}>
          <Text
            style={[
              page.sectionInfoTitleText,
              {
                marginLeft: 'auto',
                marginRight: 16,
                width: '30%',
                textAlign: 'right',
              },
            ]}>
            Total Harga
          </Text>
          <View style={[styles.PlaceholderWrapper]}>
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
              }}>
              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
            </View>
            <Text
              numberOfLines={1}
              style={[
                page.sectionInfoText,
                {
                  fontFamily: 'NotoSans-Bold',
                  textAlign: 'right',
                  fontSize: Fonts.size_18,
                },
              ]}>
              {_handleGetPriceCartOrder(item)}
            </Text>
          </View>
        </View>
        {/* Shipping Option Pesanan */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 12,
          }}>
          <Image
            source={require('../../../../assets/image/delivery.png')}
            style={{height: 32, width: 32, marginRight: 12}}
          />
          <Text
            numberOfLines={1}
            style={[
              page.sectionInfoText,
              {fontFamily: 'NotoSans-Bold', letterSpacing: 0},
            ]}>
            Pilih Pengiriman
          </Text>
        </View>
        <Pressable
          onPress={() => {
            setOrderIndex(index);
            let dataShipping = [];
            //console.log('item', item, orderData);
            let dataItems = {
              destination_id: getCheckoutBuyerAndSenderData.buyerDistrict.id,
              items: item.product_data.map(v => {
                return {
                  id: v.product_id,
                  quantity: v.quantity,
                };
              }),
            };
            dispatch(MarketplaceActions.getAllShippingFeeRequest(dataItems));

            refRBSheet.current.open();
          }}
          style={[page.ShippingOptionButton, {marginBottom: 20}]}>
          <Text style={page.ShippingOptionButtonText}>
            {`${
              item.courier_pickup != ''
                ? item.courier_pickup.code.toUpperCase() +
                  ' - ' +
                  item.courier_pickup.service
                : `Pilih Kurir & Layanan Pengiriman`
            }`}
          </Text>
          <MaterialCommunityIcons
            name="chevron-down"
            color={Colors.blueBold}
            size={Fonts.size_28}
          />
        </Pressable>

        {item.courier_pickup === '' && (
          <Text style={[c.Styles.txtInputMerchantErrors, {marginTop: -22}]}>
            Kurir pengiriman harus diisi
          </Text>
        )}
        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          height={400}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          }}>
          {/* Your Heres Here */}
          <View
            style={{
              paddingBottom: 20,
              borderBottomColor: Colors.borderGrey,
              borderBottomWidth: 0.4,
            }}>
            {/* //kylie */}
            <Text
              style={{
                fontSize: Fonts.size_18,
                marginLeft: 24,
                fontFamily: 'NotoSans-Bold',
                color: Colors.textGreyDark,
                letterSpacing: 1,
              }}>
              Pilih kurir & Layanan Pengiriman
            </Text>
          </View>
          <ScrollView>
            {/* Delivery Option */}
            {getShippingServices.length < 1 ? null : (
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={getShippingServices.data.courierGroup[0].courierList}
                renderItem={_renderItemOwnerCourier}
              />
            )}
          </ScrollView>
        </RBSheet>
      </View>
    );
  };
  const _handleRenderItemDetailProduct = ({
    item,
    index,
    markup_price,
    parentIndex,
  }) => {
    console.log('aljh', item.images[0].url);
    return (
      <View style={[page.orderRow]}>
        <View style={page.orderRowUpper}>
          <View style={[styles.PlaceholderWrapper]}>
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
              }}>
              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
            </View>
            <ImageBackground
              source={{uri: item.images[0].url}}
              style={[page.orderImage]}></ImageBackground>
          </View>

          <View
            style={[
              styles.PlaceholderWrapper,
              {marginLeft: 6, flex: 1, height: 64},
            ]}>
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
              }}>
              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
            </View>
            <View
              style={{
                width: '100%',
                height: '100%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={page.orderInfo1}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: 4,
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      page.orderInfoText1,
                      {
                        fontFamily: 'NotoSans-Bold',
                        color: Colors.blueBold,
                        marginRight: 8,
                      },
                    ]}>
                    {item.quantity}x
                  </Text>
                  <Text
                    numberOfLines={2}
                    style={[
                      page.orderInfoText1,
                      {
                        fontFamily: 'NotoSans-Bold',
                        flex: 1,
                        marginRight: 6,
                      },
                    ]}>
                    {item.name}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text numberOfLines={1} style={[page.orderInfoText2]}>
                    {item.variant},
                  </Text>
                  <Text numberOfLines={1} style={[page.orderInfoText2]}>
                    {item.Varian2}
                  </Text>
                </View>
              </View>
              <Text
                numberOfLines={1}
                style={[page.orderInfoText1, {width: '40%', letterSpacing: 0}]}>
                {func.rupiah(item.price)}
              </Text>
            </View>
          </View>
        </View>
        {/* <RenderComponent
          item={item}
          index={index}
          data={data}
          setData={setData}
        /> */}

        {/* <RenderComponent
          item={item}
          index={index}
          data={orderData}
          setOrderData={setOrderData}
          parentIndex={parentIndex}
        /> */}
        <View
          style={[
            page.orderRowUnder,
            {
              backgroundColor:
                item.key === item.length - 1 ? '#ddd' : 'rgba(0,0,0,0)',
            },
          ]}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 4,
              width: '100%',
            }}>
            <Text
              numberOfLines={1}
              style={[
                page.orderInfoText2,
                {
                  color: Colors.blueBold,
                  marginLeft: 'auto',
                  marginRight: 8,
                },
              ]}>
              Masukan harga jual
            </Text>
            <View style={[styles.PlaceholderWrapper]}>
              <TextInput
                showSoftInputOnFocus={true}
                ref={sellerPrices}
                keyboardType={'decimal-pad'}
                onChangeText={x => {
                  setOrderIndex(index);
                  _handleChangeMarkupPrice(x, item, index, parentIndex);
                }}
                placeholder={item.markup_price.toString()}
                style={[
                  page.orderProdutInput,
                  {
                    color:
                      item.markup_price > item.price
                        ? Colors.black
                        : Colors.textGreyLight,
                  },
                ]}
                underlineColorAndroid="transparent"
                value={
                  item.markup_price === 0 ? '' : item.markup_price.toString()
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 4,
              width: '100%',
            }}>
            <View style={[styles.PlaceholderWrapper, {marginLeft: 'auto'}]}>
              <View
                style={{
                  position: 'absolute',
                  width: '100%',
                  height: '100%',
                  zIndex: 1,
                }}>
                {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
              </View>
              <Text
                numberOfLines={1}
                style={[
                  page.orderInfoText2,
                  {
                    color:
                      item.markup_price < item.price
                        ? Colors.redDark
                        : Colors.textGreen,
                    marginLeft: 'auto',
                  },
                ]}>
                {item.markup_price < item.price ? `Rugi` : `Untung`}{' '}
                {func.rupiah(item.markup_price - item.price)}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  const _handleChangeMarkupPrice = (x, item, mainIndex, parentIndex) => {
    var temp = [...orderData];
    let markupValue = null;
    let statusValue = null;
    let childValue = null;
    let newVal = null;

    temp.map((t, index) => {
      if (parentIndex === index) {
        childValue = t.product_data.map(e => {
          if (e.cart_id == item.cart_id) {
            newVal = {...e, markup_price: x};
          }

          return e;
          // return {...e, ...newVal};
        });
        var newData = [...childValue, newVal];
        const result = Array.from(
          newData.reduce((a, o) => a.set(o.cart_id, o), new Map()).values(),
        );
        const merging = t;
        statusValue = {...merging, product_data: result};
        return statusValue;
      }
    });
    temp.push(statusValue);
    const result = Object.values(
      temp.reduce(
        (acc, cur) => Object.assign(acc, {[cur.supplier_name]: cur}),
        {},
      ),
    );

    sellerPrices.current.focus();
    setOrderIndex(-1);
    setOrderData(result);
  };
  const _handleShowCourierOrder = value => {
    var temp = [...orderData];
    let courierValue = null;
    let results = null;
    temp.map((t, index) => {
      if (index == orderIndex) {
        courierValue = {
          ...t,
          courier_pickup: value,
          // value.etd === null
          //   ? '0 Hari'
          //   : value.service + ', ' + value.etd + ' Hari',
        };

        return courierValue;
      }
    });
    var newData = [...temp, courierValue];
    // temp.push(results);

    const result = Object.values(
      newData.reduce(
        (acc, cur) => Object.assign(acc, {[cur.supplier_name]: cur}),
        {},
      ),
    );
    dispatch(
      MarketplaceActions.postPaymentAdminFeeRequest({
        amount: _handleSubTotalOrder() + _handlePickingFee(),
      }),
    );
    setOrderIndex(-1);
    setOrderData(result);
    refRBSheet.current.close();
  };
  const _handleCheckout = () => {
    let supplier_idstr = null;
    let isMarkupLessThanPrice = false;
    let isShippingCourierAvail = false;
    const orderPost = orderData.map((v, i) => {
      isShippingCourierAvail =
        v.courier_pickup.code === undefined
          ? false
          : (isShippingCourierAvail = true);
      return {
        shipping_carrier: v.courier_pickup.code,
        shipping_fee: v.courier_pickup.cost,
        supplier_id: v.supplier_id,
        shipping_method: 1,
        items: v.product_data.map((e, i) => {
          supplier_idstr = e.supplier_id;
          isMarkupLessThanPrice = e.markup_price < e.price ? true : false;
          return {
            id: e.product_id,
            code: e.code,
            quantity: e.quantity,
            markup: e.markup_price,
          };
        }),
      };
    });

    const dataPosts = {
      toko_id: tokoId,
      session_id: uuid.v4(),
      orders: orderPost,
      customer_name: getCheckoutBuyerAndSenderData.buyerName,
      customer_phone: getCheckoutBuyerAndSenderData.buyerPhone,
      customer_email: `${getCheckoutBuyerAndSenderData.buyerName.trim()}@gmail.com`,
      province_id: getCheckoutBuyerAndSenderData.buyerProvince.id,
      city_id: getCheckoutBuyerAndSenderData.buyerCity.id,
      district_id: getCheckoutBuyerAndSenderData.buyerDistrict.id,
      postal_code: getCheckoutBuyerAndSenderData.buyerPostalCode.postal_code,
      address: getCheckoutBuyerAndSenderData.buyerAddress1,
      address2: getCheckoutBuyerAndSenderData.buyerAddress2,
      payment_channel: paymentChannel.id,
    };
    if (paymentChannel.name === undefined) {
      setIsPaymentEmpty(true);
      parentScrollRef.current.scrollTo(0);
    } else if (!isShippingCourierAvail || isMarkupLessThanPrice) {
      parentScrollRef.current.scrollTo({
        x: 100,
        y: 350,
        animated: true,
      });
    } else {
      dispatch(MarketplaceActions.postCheckoutRequest(dataPosts));
    }
  };
  const _renderItemOwnerCourier = ({item, index}) => {
    return (
      <Pressable
        onPress={() => {
          _handleShowCourierOrder(item);
        }}
        style={{
          marginVertical: 12,

          width: '100%',
          alignSelf: 'center',
        }}>
        <Text
          numberOfLines={1}
          style={[
            page.sectionInfoText,
            {
              fontSize: 15,
              paddingHorizontal: 24,
              marginBottom: 12,
              color: Colors.textGrey,
              fontFamily: 'NotoSans-Bold',
              letterSpacing: 0.5,
            },
          ]}>
          {item.code.toUpperCase() +
            ' - ' +
            item.service +
            ' (' +
            func.rupiah(item.cost) +
            ')'}
        </Text>
        <Text
          numberOfLines={1}
          style={[
            page.sectionInfoText,
            {
              paddingHorizontal: 24,

              color: Colors.textGrey,
              bottom: 5,
            },
          ]}>{`Estimasi tiba ${item.edt}`}</Text>
        <View
          style={{
            borderWidth: 0.15,
            backgroundColor: Colors.borderGrey,
            width: '90%',
            alignSelf: 'center',
            marginTop: 12,
          }}
        />
        {/* <FlatList
          data={item.services}
          renderItem={_rendderItemCourierServices}
        /> */}
      </Pressable>
    );
  };
  const _rendderItemCourierServices = ({item, index}) => {
    return (
      <Pressable style={{padding: 12, fontFamily: 'NoToSans-Bold'}}>
        <Text>{`${item.service}, ${item.etd}`}</Text>
      </Pressable>
    );
  };
  return (
    <Container
      style={[
        styles.PlaceholderWrapper,
        {width: '100%', backgroundColor: '#fff'},
      ]}>
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={Fonts.size_24}
            />
          </Pressable>
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={styles.fixedHeaderText}>Checkout</Text>
          </View>
        </View>
        {/* H E A D E R - - - END */}
        <ScrollView
          keyboardShouldPersistTaps={true}
          keyboardDismissMode="on-drag"
          automaticallyAdjustContentInsets={false}
          ref={parentScrollRef}
          style={[styles.scrollArea]}>
          {/* B O D Y */}
          <SafeAreaView View style={styles.body}>
            <View style={styles.bodyInner}>
              {/* C O N T E N T */}
              <View style={styles.content}>
                <View style={[styles.contentInner, {marginTop: 0}]}>
                  {/* Section 1 - Sender Information */}
                  <View style={{paddingHorizontal: 12, marginBottom: 24}}>
                    <Text style={page.sectionTitleText}>Kontak Pengirim</Text>
                    {/* Pembeli info */}
                    <FlatList
                      keyExtractor={(item, index) => index.toString()}
                      style={page.sectionInfo}
                      data={[
                        {
                          title: 'Nama',
                          info: getCheckoutBuyerAndSenderData.senderName,
                          key: 'item1',
                        },
                        {
                          title: 'No. HP',
                          info: getCheckoutBuyerAndSenderData.senderPhone,
                          key: 'item2',
                        },
                      ]}
                      renderItem={({item}) => (
                        <ListItem key={item.key} style={page.sectionInfoRow}>
                          <Text style={page.sectionInfoTitleText}>
                            {item.title}
                          </Text>

                          <View style={[styles.PlaceholderWrapper, {flex: 1}]}>
                            {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 22}]} /></SkeletonPlaceholder></FadeInView> */}
                            <Text style={[page.sectionInfoText]}>
                              {item.info}
                            </Text>
                          </View>
                        </ListItem>
                      )}
                    />
                  </View>

                  <View style={styles.divider}></View>

                  {/* Section 2 - Receiver Information */}
                  <View style={{paddingHorizontal: 12, marginVertical: 24}}>
                    <Text style={page.sectionTitleText}>Alamat Pembeli</Text>
                    {/* Penerima info */}
                    <FlatList
                      keyExtractor={(item, index) => index.toString()}
                      style={page.sectionInfo}
                      data={[
                        {
                          title: 'Nama',
                          info: getCheckoutBuyerAndSenderData.buyerName,
                          key: 'item1',
                        },
                        {
                          title: 'No. HP',
                          info: getCheckoutBuyerAndSenderData.buyerPhone,
                          key: 'item2',
                        },
                        {
                          title: 'Alamat',
                          info:
                            getCheckoutBuyerAndSenderData.buyerAddress1 +
                            ', ' +
                            getCheckoutBuyerAndSenderData.buyerAddress2,
                          key: 'item3',
                        },
                      ]}
                      renderItem={({item}) => (
                        <ListItem key={item.key} style={page.sectionInfoRow}>
                          <Text style={page.sectionInfoTitleText}>
                            {item.title}
                          </Text>

                          <View style={[styles.PlaceholderWrapper, {flex: 1}]}>
                            <View
                              style={{
                                position: 'absolute',
                                width: '100%',
                                height: '100%',
                                zIndex: 1,
                              }}>
                              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                            </View>
                            <Text style={[page.sectionInfoText]}>
                              {item.info}
                            </Text>
                          </View>
                        </ListItem>
                      )}
                    />
                  </View>

                  <View style={styles.divider}></View>
                  {/* Section 5 - Payment Info */}
                  <View style={{paddingHorizontal: 12, marginVertical: 24}}>
                    <Text style={[page.sectionTitleText]}>
                      Pilih Pembayaran
                    </Text>
                    {/* Payment Option */}
                    {getPaymentChannelData != null && (
                      <PaymentOption
                        options={getPaymentChannelData.data.pay}
                        onChange={option => {
                          // dispatch(
                          //   MarketplaceActions.getPaymentChannelRequest({
                          //     amount:
                          //       _handleSubTotalOrder() +
                          //       _handlePickingFee() +
                          //       paymentChannel.fee,
                          //   }),
                          // );
                          console.log('ggg', option);
                          setIsPaymentEmpty(false);
                          setPaymentChannel(option);
                        }}
                      />
                    )}
                    {isPaymentMethEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: 0, alignSelf: 'center'},
                        ]}>
                        Pilih pembayaran terlebih dahulu
                      </Text>
                    )}
                  </View>

                  <View style={styles.divider}></View>

                  {/* Section 3 - Pesanan */}
                  <View style={{paddingHorizontal: 12, marginVertical: 24}}>
                    <Text style={page.sectionTitleText}>Pesanan</Text>
                    {/* Pesanan info */}
                    {getFinalCartData !== null && (
                      <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={orderData}
                        renderItem={_renderOrder}
                      />
                    )}
                  </View>

                  <View style={styles.divider}></View>

                  <View style={styles.divider}></View>

                  {/* Section 6 - Tagihan Total Info */}
                  <View style={{paddingHorizontal: 12, marginVertical: 24}}>
                    <Text style={page.sectionTitleText}>Total Tagihan</Text>
                    {/* Pembeli info */}
                    <FlatList
                      keyExtractor={(item, index) => index.toString()}
                      style={[
                        page.sectionInfo,
                        {
                          paddingBottom: 12,
                          borderBottomColor: Colors.borderGrey,
                          borderBottomWidth: 1,
                        },
                      ]}
                      data={[
                        {
                          title: 'Subtotal',
                          info: _handleSubTotalOrder(),
                          key: 'item1',
                        },
                        {title: 'Diskon', info: 0, key: 'item2'},
                        {
                          title: 'Biaya Pengiriman',
                          info: _handlePickingFee(),
                          key: 'item2',
                        },

                        {
                          title: 'Biaya Admin',
                          info:
                            paymentAdminFeeData === null
                              ? 0
                              : _handleAdminFee(),
                          key: 'item3',
                        },
                      ]}
                      renderItem={({item}) => (
                        <ListItem
                          key={item.key}
                          style={[page.sectionInfoRow, {paddingRight: 0}]}>
                          <Text
                            style={[page.sectionInfoTitleText, {width: '40%'}]}>
                            {item.title}
                          </Text>

                          <View
                            style={[
                              styles.PlaceholderWrapper,
                              {marginLeft: 'auto'},
                            ]}>
                            <View
                              style={{
                                position: 'absolute',
                                width: '100%',
                                height: '100%',
                                zIndex: 1,
                              }}>
                              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                            </View>
                            <Text style={[page.sectionInfoText]}>
                              {func.rupiah(item.info)}
                            </Text>
                          </View>
                        </ListItem>
                      )}
                    />
                    <View
                      style={[
                        page.sectionInfoRow,
                        {marginTop: 12, alignItems: 'center'},
                      ]}>
                      <Text
                        style={[
                          page.sectionInfoTitleText,
                          {fontFamily: 'NotoSans-Bold', width: '30%'},
                        ]}>
                        Total Harga
                      </Text>

                      <View
                        style={[
                          styles.PlaceholderWrapper,
                          {marginLeft: 'auto'},
                        ]}>
                        <View
                          style={{
                            position: 'absolute',
                            width: '100%',
                            height: '100%',
                            zIndex: 1,
                          }}>
                          {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView> */}
                        </View>
                        <Text
                          style={[
                            page.sectionInfoText,
                            {
                              fontFamily: 'NotoSans-Bold',
                              marginLeft: 'auto',
                              fontSize: Fonts.size_20,
                            },
                          ]}>
                          {func.rupiah(
                            _handleSubTotalOrder() +
                              _handlePickingFee() +
                              _handleAdminFee(),
                          )}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
          {/* B O D Y - - - END */}
        </ScrollView>
        {/* F O O T E R */}
        <View
          style={[
            styles.fixedFooter,
            {backgroundColor: Colors.white, elevation: 1},
          ]}>
          <Pressable style={{width: '100%'}}>
            <Button
              isFetching={isFetchingCheckout}
              onPress={() => _handleCheckout()}
              theme="primary"
              text="Bayar Tagihan"
              size="medium"
              option
              style={{
                backgroundColor: Colors.blueBold,
                borderWidth: 0,
                paddingVertical: 18,
              }}
              textStyle={{
                color: Colors.white,
                fontSize: Fonts.size_18,
              }}
            />
          </Pressable>
        </View>
        {/* F O O T E R - - - END */}
      </Container>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    getFinalCartData: state.marketplace.cartCheckoutData.data,
    getOwnerTokoCourierData: state.toko.ownerTokoCourierData.data,
    getCheckoutBuyerAndSenderData: state.marketplace.addressCheckoutData.data,
    getShippingServices: state.marketplace.shippingFeeData.data,
    getPaymentChannelData: state.marketplace.paymentChannelData.data,
    isFetchingCheckout: state.marketplace.postCheckoutData.fetching,
    paymentAdminFeeData: state.marketplace.postPaymentAdminFeeData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);
