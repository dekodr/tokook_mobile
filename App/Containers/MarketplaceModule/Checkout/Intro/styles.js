import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const Page = StyleSheet.create({

  sectionTitleText: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_18,
    letterSpacing: 1,
    marginBottom: 12,
  },
  sectionInfo: {

  },
  sectionInfoRow: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 0,
    borderBottomColor: 'rgba(0,0,0,0)',
    marginVertical: 0,
    paddingTop: 0,
    paddingBottom: 6,
    // paddingHorizontal: 0,
  },
  sectionInfoTitleText: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
    width: '25%',
  },
  sectionInfoText: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },

  orderWrapper: {
    padding: 12,
    borderRadius: 6,
    borderColor: Colors.borderGrey,
    borderWidth: .8,
  },
  orderMerchantRow: {
    paddingBottom: 12,
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: .4,
  },
  orderRow: {
    marginLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    borderBottomColor: 'rgba(0,0,0,0)',
    flexDirection: 'column',
  },
  orderRowUpper: {
    paddingVertical: 12,
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: .4,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    // backgroundColor: '#ddd',
  },
  orderImage: {
    borderRadius: 6, 
    aspectRatio: 1, 
    width: 64,
    overflow: 'hidden',
    resizeMode: "cover",
  },
  orderInfo1: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    // letterSpacing: 1,
    flex: 1,
    paddingHorizontal: 12,
  },
  orderInfoText1: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    // letterSpacing: 1, 
  },
  orderInfoText2: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_14+1,
    paddingRight: 6,
    // letterSpacing: 1,
  },
  orderRowUnder: {
    paddingVertical: 12,
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: .4,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    // backgroundColor: '#bbb',
    flexDirection: 'column',
  },
  orderProdutInput: {
    width: 124,
    height: 32,
    paddingHorizontal: 8,
    paddingVertical: 0,
    backgroundColor: Colors.white,
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    borderRadius: 4,
    borderColor: Colors.borderGrey,
    borderWidth: 1,
  },
  ShippingOptionButton: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
    // backgroundColor: '#ddd',
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: .4,
  },
  ShippingOptionButtonText: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    marginRight: 12,
  },
});

export default Page;