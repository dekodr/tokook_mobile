import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const Page = StyleSheet.create({
  tesTab: {
    marginTop: -1,
    width: 84.5,
    height: '100%',
    // backgroundColor: '#bbb',
    borderRightColor: Colors.borderGrey,
    borderRightWidth: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 9999,
  },
  tabContainer: {
    backgroundColor: '#f3f3f3',
    height: 84.5 - 18,
    width: 84.5,
    alignItems: 'center',
    justifyContent: 'center',
    // borderBottomColor: Colors.borderGrey,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  tabActive: {
    backgroundColor: '#fff',
    borderLeftColor: Colors.blueBold,
    borderLeftWidth: 6,
    borderStyle: 'solid',
  },
  tabIcon: {
    width: 32,
    height: 32,
    marginBottom: 4,
  },
  tabText: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_14+1,
    color: Colors.textGreyDark,
    letterSpacing: 0.6,
  },
  itemContainer: {
    width: '75%',
    alignSelf: 'flex-start',
    paddingVertical: 12,
    paddingHorizontal: 6,
    // backgroundColor: '#000',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  textDivider: {
    paddingVertical: 6,
    paddingHorizontal: 12,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    width: '100%',
    flex: 1,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    marginBottom: 12,
  },
  textDividerText: {
    fontFamily: 'NotoSans-Bold',
    paddingVertical: 6,
    letterSpacing: 1,
    fontSize: Fonts.size_20,
    color: Colors.textGreyDark,
  },
});

export default Page;
