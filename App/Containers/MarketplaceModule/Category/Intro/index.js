import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  SafeAreaView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  StyleSheet,
  Animated,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
import { SectionCategory } from '../../../../Components/Card_V2';
// import CategoryTab from '../../../../Components/CategoryTab';
import tc from '../../../TokoModule/Intro/styles';
import styles from '../../Intro/styles';
import page from './styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';

// import TabViewVertical from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SectionList from 'react-native-tabs-section-list';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {position} from 'polished';
import ButtonIcon from '../../../../Components/ButtonIcon';

function Category(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter(data => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  const handleGoToAllPage = () => {};

  const HeaderText = item => {
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </Pressable>
      </View>
    );
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    }
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const _handleMarketplaceCategoryDetail = item => {
    console.log('press');
    navigation.navigate('MarketplaceCategoryDetailScreen', {
      params: item,
    });
  };
  const _handleCart = item => {
    console.log('press');
    navigation.navigate('MarketplaceCartScreen', {
      params: item,
    });
  };

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };

  let {height, width} = Dimensions.get('window');

  const FadeInView = props => {
    const [fadeAnim] = useState(new Animated.Value(1)); // Initial value for opacity: 0

    React.useEffect(() => {
      Animated.timing(fadeAnim, {
        toValue: -1,
        duration: Fonts.skeleton_anim_time,
        delay: Fonts.skeleton_delay_anim_time,
      }).start();
    }, []);

    return (
      <Animated.View
        style={{
          ...props.style,
          opacity: fadeAnim, // fadeAnim
          zIndex: fadeAnim, // fadeAnim
        }}>
        {props.children}
      </Animated.View>
    );
  };

  const SECTIONS = [
    {
      title: 'Pria',
      key: 1,
      icon_inactive: require('../../../../assets/icons/icon-men-inactive.png'),
      icon_active: require('../../../../assets/icons/icon-men-active.png'),
      data: Array(7)
        .fill(0)
        .map(_ => ({
          title: 'asdc',
          description: 'asdasdasdasdads',
          image: require('../../../../assets/image/honda-jazz-hero.png'),
        })),
    },
    {
      title: 'Wanita',
      key: 2,
      icon_inactive: require('../../../../assets/icons/icon-women-inactive.png'),
      icon_active: require('../../../../assets/icons/icon-women-active.png'),
      data: Array(10)
        .fill(0)
        .map(_ => ({
          title: 'Pakaian Muslim Wanita',
          description: 'asdasdasdasdads',
          image: require('../../../../assets/image/honda-jazz-hero.png'),
        })),
    },
    {
      title: 'Make Up',
      key: 3,
      icon_inactive: require('../../../../assets/icons/icon-makeup-inactive.png'),
      icon_active: require('../../../../assets/icons/icon-makeup-active.png'),
      data: Array(13)
        .fill(0)
        .map(_ => ({
          title: 'ablosd',
          description: 'asdasdasdasdads',
          image: require('../../../../assets/image/honda-jazz-hero.png'),
        })),
    },
  ];

  return (
    <Container
      style={[
        styles.PlaceholderWrapper,
        {width: '100%', backgroundColor: '#fff'},
      ]}>
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          {/* <Pressable onPress={handleGoBack} style={{paddingRight: 20}}>
              <MaterialCommunityIcons
                name={'keyboard-backspace'}
                color={'white'}
                size={Fonts.size_28}
              />
            </Pressable> */}
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={[styles.fixedHeaderText, {marginLeft: 24}]}>
              Kategori
            </Text>
          </View>
          {/* CART BUTTON */}
          <View style={[styles.cartButtonWrapper, {marginLeft: 'auto'}]}>
            <View style={[styles.cartIndicator]}>
              <View style={[styles.cartIndicatorInner]}>
                <Text style={styles.cartIndicatorInnerText}>2</Text>
              </View>
            </View>
            <ButtonIcon
              onPress={() => _handleCart()}
              style={[styles.cart]}
              icon="cart-outline"
              color="#fff"
              size={Fonts.size_24 + 4}
              style={{
                // backgroundColor: '#000',
                height: 36,
                width: 36,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 0,
              }}
            />
          </View>
        </View>
        {/* H E A D E R - - - END */}
        <View style={[styles.scrollArea]}>
          {/* B O D Y */}
          <SafeAreaView style={[styles.body, {flex: 1}]}>
            <View style={(styles.bodyInner, {flex: 1})}>
              {/* C O N T E N T */}
              <View style={[styles.content, {flex: 1}]}>
                <View
                  style={[styles.contentInner, {flex: 1, paddingBottom: 0}]}>
                  <SectionList
                    tabBarStyle={[page.tesTab]}
                    sections={SECTIONS}
                    keyExtractor={item => item.title}
                    stickySectionHeadersEnabled={true}
                    scrollToLocationOffset={5}
                    // ItemSeparatorComponent={() => <View style={stylesNew.separator} />}
                    renderTab={({
                      title,
                      isActive,
                      icon_active,
                      icon_inactive,
                    }) => (
                      <View
                        style={[
                          page.tabContainer,
                          isActive ? page.tabActive : null,
                          {
                            borderTopColor: isActive
                              ? Colors.borderGrey
                              : 'rgba(0,0,0,0)',
                            borderBottomColor: isActive
                              ? Colors.borderGrey
                              : 'rgba(0,0,0,0)',
                          },
                        ]}>
                        <Image
                          source={isActive ? icon_active : icon_inactive}
                          style={[page.tabIcon]}
                        />
                        <Text
                          style={[
                            page.tabText,
                            {
                              color: Colors.textGreyDark,
                            },
                          ]}>
                          {title}
                        </Text>
                      </View>
                    )}
                    renderSectionHeader={({section}) => (
                      <View
                        style={[
                          page.textDivider,
                          {
                            borderTopColor:
                              section.key - 1 == 0
                                ? 'rgba(0,0,0,0)'
                                : Colors.borderGrey,
                            width: width,
                          },
                        ]}>
                        <Text style={page.textDividerText}>
                          {section.title}
                        </Text>
                      </View>
                    )}
                    renderItem={({item, index}) => (
                      <SectionCategory
                        image={item.image}
                        title={item.title}
                        onPress={() => _handleMarketplaceCategoryDetail()}
                        styles={{
                          // backgroundColor: '#ddd',
                          marginLeft: index % 3 == 3 ? 0 : 30,
                          width: width / 4 - 32,
                          marginBottom: 12,
                        }}
                      />
                    )}
                  />
                </View>
              </View>
              {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
          {/* B O D Y - - - END */}
        </View>
      </Container>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);

const stylesNew = StyleSheet.create({
  separator: {
    height: 0.5,
    width: '96%',
    backgroundColor: '#eaeaea',
  },
  sectionHeaderContainer: {
    height: 10,
    width: '75%',
    alignSelf: 'flex-end',
    // position: 'absolute',
    flexDirection: 'column',
    // right: 0,
    backgroundColor: '#f6f6f6',
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
    borderBottomColor: '#f4f4f4',
    borderBottomWidth: 1,
  },
  sectionHeaderText: {
    color: '#010101',
    width: '75%',
    alignSelf: 'flex-end',
    backgroundColor: '#fff',
    fontSize: 23,
    fontWeight: 'bold',
    paddingTop: 25,
    paddingBottom: 5,
    paddingHorizontal: 15,
  },
  itemContainer: {
    width: '75%',
    // position: 'absolute',
    // right: 0,
    alignSelf: 'flex-end',
    paddingVertical: 20,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
  },
  itemTitle: {
    flex: 1,
    fontSize: 20,
    color: '#131313',
  },
  itemPrice: {
    fontSize: 18,
    color: '#131313',
  },
  itemDescription: {
    marginTop: 10,
    color: '#b6b6b6',
    fontSize: 16,
  },
  itemRow: {
    flexDirection: 'row',
  },
});

// <FadeInView style={styles.PagePlaceholderWrapper}>
//   <View style={styles.PagePlaceholderInner}>
//     <View style={{backgroundColor: Colors.blueBold, width: '100%', height: 48+10*2, flexDirection: 'row', alignItems: 'center'}}>
//       <View style={[styles.HeaderShadowText, {height: 28, marginLeft: 12}]}>
//         <SkeletonPlaceholder>
//           <View style={{width: '100%', height: '100%'}} />
//         </SkeletonPlaceholder>
//       </View>
//       <View style={[styles.HeaderShadowLink, {width: 'auto', height: 28, aspectRatio: 1, marginLeft: 'auto', marginRight: 12}]}>
//         <SkeletonPlaceholder>
//           <View style={{width: '100%', height: '100%'}} />
//         </SkeletonPlaceholder>
//       </View>
//     </View>
//     <View style={{
//       backgroundColor: '#fff',
//       width: '100%',
//       paddingHorizontal: 0,
//       paddingTop: 0,
//       height: '100%' }}>

//       <View style={{flexDirection: 'row'}}>

//         <View style={{width: 100, paddingHorizontal: 12, height, borderRightColor: Colors.borderGrey, borderRightWidth: 1}}>
//           <View style={{width: '100%', aspectRatio: 1, marginTop: 12, borderRadius: 6, overflow: 'hidden'}}>
//             <SkeletonPlaceholder>
//               <View style={{width: '100%', height: '100%'}} />
//             </SkeletonPlaceholder>
//           </View>
//           <View style={{width: '100%', aspectRatio: 1, marginTop: 12, borderRadius: 6, overflow: 'hidden'}}>
//             <SkeletonPlaceholder>
//               <View style={{width: '100%', height: '100%'}} />
//             </SkeletonPlaceholder>
//           </View>
//           <View style={{width: '100%', aspectRatio: 1, marginTop: 12, borderRadius: 6, overflow: 'hidden'}}>
//             <SkeletonPlaceholder>
//               <View style={{width: '100%', height: '100%'}} />
//             </SkeletonPlaceholder>
//           </View>
//         </View>

//         <View style={{flex: 1, height: height}}>
//           <View style={{paddingHorizontal: 12, paddingVertical: 10, borderBottomColor: Colors.borderGrey, borderBottomWidth: 1}}>
//             <View style={[styles.HeaderShadowText, {height: 28}]}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//           </View>

//           <View style={{flexDirection: 'row', flexWrap: 'wrap', paddingLeft: 12, paddingVertical: 12}}>
//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>

//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//             <View style={{width: width/4-14, aspectRatio: 1, borderRadius: 6, overflow: 'hidden', marginRight: 10, marginBottom: 48}}>
//               <SkeletonPlaceholder>
//                 <View style={{width: '100%', height: '100%'}} />
//               </SkeletonPlaceholder>
//             </View>
//           </View>
//         </View>

//       </View>

//     </View>
//   </View>
// </FadeInView>
