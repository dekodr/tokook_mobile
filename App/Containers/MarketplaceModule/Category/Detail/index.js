import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  BackHandler,
  ImageBackground,
  Animated,
  SafeAreaView,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import {Images, Colors, Fonts} from '../../../../Themes/';
import Card from '../../../../Components/Card';
import { ProductCard } from '../../../../Components/Card_V2';

import ButtonIcon from '../../../../Components/ButtonIcon';
import {FilterOption} from '../../../../Components/Option';

import page from '../../Detail/Intro/styles';
import styles from '../../Intro/styles';

import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function MarketplaceDetail(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;

  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);

  let {height, width} = Dimensions.get('window');

  const refRBSheet = useRef();
  const refRBSheetFilter = useRef();

  const FadeInView = props => {
    const [fadeAnim] = useState(new Animated.Value(1)); // Initial value for opacity: 0

    React.useEffect(() => {
      Animated.timing(fadeAnim, {
        toValue: -1,
        duration: Fonts.skeleton_anim_time,
        delay: Fonts.skeleton_delay_anim_time,
      }).start();
    }, []);

    return (
      <Animated.View
        style={{
          ...props.style,
          opacity: fadeAnim, // fadeAnim
          zIndex: fadeAnim, // fadeAnim
        }}>
        {props.children}
      </Animated.View>
    );
  };

  return (
    <Container styles={[styles.container]}>
      <ScrollView style={[styles.scrollArea]}>
        <SafeAreaView
          style={[styles.body, {backgroundColor: 'rgba(0,0,0,0)'}]}>
          <View style={[styles.bodyInner, {marginTop: 0}]}>
            {/* C O N T E N T */}
            <View style={styles.content}>
              <View style={[styles.contentInner]}>
                {/* Header Area */}
                <View style={page.titleHeader}>
                  <View style={page.titleLeft}>
                    {/* Back Button */}
                    <View style={[styles.backButtonWrapper]}>
                      <ButtonIcon
                        onPress={handleGoBack}
                        icon="keyboard-backspace"
                        color={Colors.textGreyDark}
                        size={Fonts.size_28 + 6}
                        style={{
                          // backgroundColor: '#000',
                          height: 48,
                          width: 48,
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginLeft: -6,
                          marginTop: -12,
                        }}
                      />
                    </View>
                    <View style={page.titleText}>
                      <Text style={page.textTitle}>Kategori</Text>
                      <Text style={page.textSubTitle}>Parfum Wanita</Text>
                    </View>
                  </View>

                  <View style={page.titleBg}>
                    <ImageBackground
                      source={require('../../../../assets/image/honda-jazz-hero.png')}
                      style={page.titleBgImg}></ImageBackground>
                  </View>
                </View>
                {/* Header Area */}

                {/* Filter Area */}
                <View style={[page.headerFilter]}>
                  {/* input search filter */}
                  <View
                    style={{
                      borderWidth: 0.4,
                      borderColor: Colors.textGrey,
                      paddingHorizontal: 8,
                      borderRadius: 3,
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginRight: 6,
                      flex: 1,
                    }}>
                    <View style={styles.searchBarIcon}>
                      <MaterialCommunityIcons
                        name={'magnify'}
                        color={Colors.textGreyLight}
                        size={Fonts.size_24}
                      />
                    </View>
                    <TextInput
                      placeholder="Cari barang..."
                      style={styles.searchBarInput}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                  {/* input search filter - - - end */}

                  {/* urutkan button */}
                  <View
                    style={{
                      borderWidth: 0.4,
                      borderColor: Colors.textGrey,
                      paddingHorizontal: 8,
                      borderRadius: 3,
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginRight: 6,
                    }}>
                    <ButtonIcon
                      onPress={() => refRBSheet.current.open()}
                      icon="arrow-up-down"
                      text="Urutkan"
                      color={Colors.textGrey}
                      size={Fonts.size_24}
                      style={{
                        backgroundColor: 'transparent',
                        // borderWidth: .4,
                        // borderColor: Colors.textGrey,
                        height: 35,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      textStyle={{
                        letterSpacing: 0.4,
                        paddingLeft: 4,
                        color: Colors.textGrey,
                        fontFamily: 'NotoSans-Reguler',
                      }}
                    />
                  </View>
                  {/* urutkan button - - - end */}

                  {/* filter button */}
                  <View
                    style={{
                      borderWidth: 0.4,
                      borderColor: Colors.textGrey,
                      paddingHorizontal: 8,
                      borderRadius: 3,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <ButtonIcon
                      onPress={() => refRBSheetFilter.current.open()}
                      icon="filter-variant"
                      text="Filter"
                      color={Colors.textGrey}
                      size={Fonts.size_28 + 1}
                      style={{
                        backgroundColor: 'transparent',
                        // borderWidth: .4,
                        // borderColor: Colors.textGrey,
                        height: 35,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                      textStyle={{
                        letterSpacing: 0.4,
                        paddingLeft: 4,
                        color: Colors.textGrey,
                        fontFamily: 'NotoSans-Reguler',
                      }}
                    />
                    <View
                      style={[
                        styles.cartIndicatorInner,
                        {
                          backgroundColor: Colors.blueBold,
                          height: 17,
                          width: 17,
                          marginLeft: 8,
                        },
                      ]}>
                      <Text
                        style={[
                          styles.cartIndicatorInnerText,
                          {color: Colors.white},
                        ]}>
                        2
                      </Text>
                    </View>
                  </View>
                  {/* filter button - - - end */}
                </View>
                {/* Filter Area */}

                {/* Content Area */}
                <FlatList
                  columnWrapperStyle={{justifyContent: 'space-between'}}
                  numColumns={2}
                  style={
                    ([styles.CardRow],
                    {
                      paddingHorizontal: 12,
                      marginTop: 0,
                      flexDirection: 'column',
                    })
                  }
                  data={[
                    {
                      ProductName:
                        'Tesla model 1, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '1',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                    {
                      ProductName:
                        'Tesla model 2, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '2',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                    {
                      ProductName:
                        'Tesla model 3, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '3',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                    {
                      ProductName:
                        'Tesla model 4, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '4',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                    {
                      ProductName:
                        'Tesla model 5, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '5',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                    {
                      ProductName:
                        'Tesla model 6, made indonesia super speed echo friendly',
                      ProductImage: require('../../../../assets/image/honda-jazz-hero.png'),
                      ProductPrice: '2.999.000.000',
                      productId: '6',
                      ProductStoreLocation: 'Menteng, Jakarta Pusat',
                    },
                  ]}
                  renderItem={({item}) => (
                    <ListItem
                      key={item.id}
                      style={{
                        marginRight: item.productId % 2 == 1 ? 6 : 0,
                        marginLeft: item.productId % 2 == 0 ? 6 : 0,
                        paddingTop: 0,
                        paddingBottom: 0,
                        paddingRight: 0,
                        width: width / 2 - 18,
                        marginTop: 12,
                        borderBottomColor: 'rgba(0,0,0,0)',
                      }}>
                      <ProductCard 
                        id={item.id}
                        image={item.ProductImage}
                        title={item.ProductName}
                        price={item.ProductPrice}
                        location={item.ProductStoreLocation}
                        onPress={() => OnPressCarousel()}
                        styles={{
                          width: '100%',
                        }}
                      />
                    </ListItem>
                  )}
                />
                {/* Content Area */}
              </View>
            </View>
            {/* C O N T E N T - - - END*/}
          </View>
        </SafeAreaView>
      </ScrollView>

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        // height={500}
        openDuration={250}
        closeDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'rgba(0,0,0,0.4)',
          },
          draggableIcon: {
            backgroundColor: '#d8d8d8',
          },
          container: {
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            minHeight: '75%',
            paddingBottom: 120,
          },
        }}>
        {/* Your Heres Here */}
        <View style={styles.filterHeader}>
          <MaterialCommunityIcons
            name={'arrow-up-down'}
            color={Colors.textGreyDark}
            size={Fonts.size_28}
          />
          <Text style={styles.filterHeaderText}>Urutkan</Text>
        </View>
        <View>
          <ScrollView style={{paddingBottom: 24}}>
            <FlatList
              data={[
                {key: 'Harga Termurah'},
                {key: 'Harga Termahal'},
                {key: 'Barang Terbaru'},
                {key: 'Paling Populer'},
              ]}
              renderItem={({item}) => (
                <Text
                  onPress={() => refRBSheet.current.close()}
                  style={{
                    color: '#4a4a4a',
                    fontSize: Fonts.size_16,
                    padding: 10,
                    paddingLeft: 20,
                    borderBottomColor: Colors.borderGrey,
                    borderBottomWidth: .4,
                  }}>
                  {item.key}
                </Text>
              )}
            />
          </ScrollView>
        </View>

        {/* F O O T E R */}
        <View
          style={[
            styles.fixedFooter,
            {backgroundColor: Colors.white, elevation: 1},
          ]}>
          <Pressable style={{flex: 2, marginRight: 6}}>
            <ButtonIcon
              // icon="store-outline"
              text="Hilangkan"
              color={Colors.blueBold}
              size={Fonts.size_28}
              // AddtoSomething
              plusColor={Colors.blueBold}
              style={{
                borderRadius: 6,
                width: '100%',
                height: 48,
                borderColor: Colors.blueBold,
                borderWidth: 2,
                borderStyle: 'solid',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              textStyle={{
                color: Colors.blueBold,
                fontFamily: 'NotoSans-Bold',
                marginLeft: 15,
              }}
            />
          </Pressable>
          <Pressable
            onPress={() => refRBSheet.current.close()}
            style={{flex: 2, marginLeft: 6}}>
            <ButtonIcon
              // icon="cart-outline"
              text="Terapkan"
              color={Colors.white}
              size={Fonts.size_28}
              // AddtoSomething
              plusColor={Colors.white}
              style={{
                borderRadius: 6,
                width: '100%',
                height: 48,
                backgroundColor: Colors.blueBold,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              textStyle={{
                color: Colors.white,
                fontFamily: 'NotoSans-Bold',
                marginLeft: 15,
              }}
            />
          </Pressable>
        </View>
        {/* F O O T E R - - - END */}
      </RBSheet>
      <RBSheet
        ref={refRBSheetFilter}
        closeOnDragDown={true}
        // height={500}
        openDuration={250}
        closeDuration={250}
        customStyles={{
          wrapper: {
            backgroundColor: 'rgba(0,0,0,0.4)',
          },
          draggableIcon: {
            backgroundColor: '#d8d8d8',
          },
          container: {
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            minHeight: '75%',
            paddingBottom: 120,
          },
        }}>
        {/* Your Heres Here */}
        <View style={styles.filterHeader}>
          <MaterialCommunityIcons
            name={'filter-variant'}
            color={Colors.textGreyDark}
            size={Fonts.size_28 + 4}
          />
          <Text style={styles.filterHeaderText}>Filter</Text>
        </View>
        <View>
          <ScrollView style={{paddingBottom: 24}}>
            <View style={styles.filterContentWrapper}>
              <Text style={styles.filterContentWrapperText}>
                Lokasi Supplier
              </Text>
              <FilterOption
                options={[
                  'Jabodetabek',
                  'DKI Jakarta',
                  'Banten',
                  'Sumatera Selatan',
                  'Sumatera Utara ',
                  'Jawa Barat',
                  'Jawa Tengah',
                  'Jawa Timur',
                ]}
                onChange={option => {
                  console.log(option);
                }}
              />
            </View>

            <View style={styles.filterContentWrapper}>
              <Text style={styles.filterContentWrapperText}>
                Metode Pembayaran
              </Text>
              <FilterOption
                options={[
                  'COD (Bayar di Tempat)',
                  'Transfer Bank',
                  'QRIS (e-wallet)',
                ]}
                onChange={option => {
                  console.log(option);
                }}
              />
            </View>

            <View style={styles.filterContentWrapper}>
              <Text style={styles.filterContentWrapperText}>Kategori</Text>
              <FilterOption
                options={[
                  'Parfum',
                  'Kaos',
                  'Atasan Pria',
                  'Atasan Wanita',
                  'Bawahan Pria',
                  'Rok',
                  'Sweater',
                  'Jaket Hoodie',
                ]}
                onChange={option => {
                  console.log(option);
                }}
              />
            </View>

            {/* <View style={styles.filterContentWrapper}>
              <Text style={styles.filterContentWrapperText}>harga (Rp)</Text>
              <FilterOption
                options={['Parfum', 'Kaos', 'Atasan Pria', 'Atasan Wanita', 'Bawahan Pria', 'Rok', 'Sweater', 'Jaket Hoodie']}
                onChange={(option) => {
                  console.log(option);
                }}
              />
            </View> */}

            <View style={[styles.filterContentWrapper, {marginBottom: 50}]}>
              <Text style={styles.filterContentWrapperText}>Pengiriman</Text>
              <FilterOption
                options={['Instan', 'Same Day', 'Next Day', 'Reguler']}
                onChange={option => {
                  console.log(option);
                }}
              />
            </View>
          </ScrollView>
        </View>

        {/* F O O T E R */}
        <View
          style={[
            styles.fixedFooter,
            {backgroundColor: Colors.white, elevation: 1},
          ]}>
          <Pressable style={{flex: 2, marginRight: 6}}>
            <ButtonIcon
              // icon="store-outline"
              text="Hilangkan"
              color={Colors.blueBold}
              size={Fonts.size_28}
              // AddtoSomething
              plusColor={Colors.blueBold}
              style={{
                borderRadius: 6,
                width: '100%',
                height: 48,
                borderColor: Colors.blueBold,
                borderWidth: 2,
                borderStyle: 'solid',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              textStyle={{
                color: Colors.blueBold,
                fontFamily: 'NotoSans-Bold',
                marginLeft: 15,
              }}
            />
          </Pressable>
          <Pressable
            onPress={() => refRBSheet.current.close()}
            style={{flex: 2, marginLeft: 6}}>
            <ButtonIcon
              // icon="cart-outline"
              text="Terapkan"
              color={Colors.white}
              size={Fonts.size_28}
              // AddtoSomething
              plusColor={Colors.white}
              style={{
                borderRadius: 6,
                width: '100%',
                height: 48,
                backgroundColor: Colors.blueBold,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              textStyle={{
                color: Colors.white,
                fontFamily: 'NotoSans-Bold',
                marginLeft: 15,
              }}
            />
          </Pressable>
        </View>
        {/* F O O T E R - - - END */}
      </RBSheet>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceDetail);
