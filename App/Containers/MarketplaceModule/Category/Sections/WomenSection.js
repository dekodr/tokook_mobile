import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  StyleSheet,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
// import Button from '../../../../Components/Button';
import ButtonIcon from '../../../../Components/ButtonIcon';
import tc from '../../../TokoModule/Intro/styles';
import page from './styles';
import {Images, Colors} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from '../../../../Redux/TokoRedux';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

function WomenSection(props) {
  const {
	dispatch,
	navigation,
	getToko,
	bottomSheetRef,
	tokoId,
	userData,
	tokoData,
	setStep,
	step,
	tokoAddressData,
  } = props;
  const isVerified =
	userData.data.is_phone_verified !== null &&
	tokoData &&
	tokoAddressData !== null
	  ? tokoAddressData.listAddress.length
		? tokoAddressData.listAddress.length !== 0
		: false
	  : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
	? tokoCategory.length
	  ? tokoCategory.filter(data => data.id === selectedCategory.id)
	  : []
	: [];
  const isTokoAvailable = props.getToko
	? props.getToko.listToko
	  ? props.getToko.listToko.length === 0
		? false
		: true
	  : true
	: true;
  const isCurrentIdUndefined = tokoCategory
	? tokoCategory.length
	  ? getId.id === undefined
		? true
		: false
	  : false
	: false;
  const wait = timeout => {
	return new Promise(resolve => {
	  setTimeout(resolve, timeout);
	});
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  const handleGoToAllPage = () => {
    
  }

  const _handleMarketplaceDetail = item => {
    navigation.navigate('MarketplaceDetailScreen', {
      params: item,
    });
  };

  const _handleMarketplaceProduct = item => {
    console.log("press")
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

	const OnPressCarousel = item => {
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

	const _handleCart = item => {
    navigation.navigate('MarketplaceCartScreen', {
      params: item,
    });
  };
	const _handleCategory = item => {
    navigation.navigate('MarketplaceCategoryScreen', {
      params: item,
    });
  };

  const HeaderText = (item) => {
    // console.log("----", item.text);
    return (
      <View 
        style={{paddingHorizontal: 12, flexDirection: 'row', alignItems: 'center'}}>
        <Text
          style={styles.HeaderText}>
          {item.text}
        </Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
            <Text
              style={styles.HeaderAllLink}>
              lihat semua
            </Text>
        </Pressable>
      </View>
    );
  };

  useEffect(() => {
	BackHandler.addEventListener('hardwareBackPress', handleGoBack);

	return () =>
	  BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
	setTokoCategory(
	  props.categoryData
		? props.categoryData.listCategory
		  ? props.categoryData.listCategory.length != 0
			? props.categoryData.listCategory
			: []
		  : []
		: [],
	);
  }, [props.categoryData]);

  useEffect(() => {
	if (newCategory) {
	  setIsCategoryEmpty(false);
	}
  }, [newCategory]);

  useEffect(() => {
	if (props.tokoId) {
	  props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
	}
	// func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  let {height, width} = Dimensions.get('window');

  const toDetailScreen = item => {
    navigation.navigate('MarketplaceCategoryDetailScreen', {
      params: item,
    });
  };

  const reload = () => {
	refRBSheet.current.close();
	setIsCategoryEmpty(false);
	setNewCategory('');
	setSelectedCategory({});
	props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };
  // console.log(CarouselOne);
  return (
    
    <ScrollView
      style={page.container}
    >
      <View style={page.textDivider}>
        <Text style={page.textDividerText}>
          Wanita
        </Text>
      </View>
      <View
        style={{
        height: '100%',
        paddingHorizontal: 6,
        // backgroundColor: '#ddd',
        marginTop: 12,
        flexDirection: 'row',
        flexWrap: 'wrap',
        }}>
        <View 
          style={{width: width/4-3.4, marginBottom: 12}}>
          <Pressable
            delayPressIn={0}
            onPress={() => toDetailScreen()}>
            <Card
              type="CardCategoryMedium"
              id={1}
              CategoryName="Parfum"
              CategoryImage={require('../../../../assets/image/honda-jazz-hero.png')}
            />
          </Pressable>
        </View>
        <View 
          style={{width: width/4-3.4, marginBottom: 12}}>
          <Pressable
            delayPressIn={0}
            onPress={() => toDetailScreen()}>
            <Card
              type="CardCategoryMedium"
              id={1}
              CategoryName="Atasan Wanita"
              CategoryImage={require('../../../../assets/image/honda-jazz-hero.png')}
            />
          </Pressable>
        </View>
        <View 
          style={{width: width/4-3.4, marginBottom: 12}}>
          <Pressable
            delayPressIn={0}
            onPress={() => toDetailScreen()}>
            <Card
              type="CardCategoryMedium"
              id={1}
              CategoryName="Bawahan Wanita"
              CategoryImage={require('../../../../assets/image/honda-jazz-hero.png')}
            />
          </Pressable>
        </View>
        <View 
          style={{width: width/4-3.4, marginBottom: 12}}>
          <Pressable
            delayPressIn={0}
            onPress={() => toDetailScreen()}>
            <Card
              type="CardCategoryMedium"
              id={1}
              CategoryName="Hijab"
              CategoryImage={require('../../../../assets/image/honda-jazz-hero.png')}
            />
          </Pressable>
        </View>
        <View 
          style={{width: width/4-3.4, marginBottom: 12}}>
          <Pressable
            delayPressIn={0}
            onPress={() => toDetailScreen()}>
            <Card
              type="CardCategoryMedium"
              id={1}
              CategoryName="Tas Wanita"
              CategoryImage={require('../../../../assets/image/honda-jazz-hero.png')}
            />
          </Pressable>
        </View>
      </View>
    </ScrollView>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
	getToko: state.toko.tokoData.data,

	tokoId: state.toko.tokoData.data
	  ? state.toko.tokoData.data.listToko
		? state.toko.tokoData.data.listToko.length
		  ? state.toko.tokoData.data.listToko[0].id
		  : null
		: null
	  : null,
	categoryData: state.toko.categoryTokoData.data,
	userData: state.auth.userData,
	tokoData: state.toko.tokoData.data
	  ? state.toko.tokoData.data.listToko
		? state.toko.tokoData.data.listToko.length
		  ? state.toko.tokoData.data.listToko[0]
		  : null
		: null
	  : null,
	tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WomenSection);