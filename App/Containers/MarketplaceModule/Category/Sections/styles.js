import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const Page = StyleSheet.create({
  container: {
    // backgroundColor: '#666',
    flex: 1,
    flexDirection: 'column',
  },
  block: {
    height: 150,
    width: '100%',
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 24,
  },
  content: {
    
  },
  cover: {
    width: '50%',
    height: Dimensions.get('window').width / 2,
  },

  textDivider: {
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderBottomColor: '#c9c6c6',
    borderBottomWidth: 1,
    borderStyle: 'solid',
  },
  textDividerText: {
    fontFamily: 'NotoSans-Bold',
    paddingTop: 8,
    fontSize: Fonts.size_20,
    color: Colors.textGreyDark,
  },
});

export default Page;