import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../Themes';

const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({
  CardTutorial: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    elevation: 3,
    borderColor: '#ccc',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.05,
    shadowRadius: 2,
    borderRadius: 8,
    overflow: 'hidden',
    height: 85,
    // flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  TutorialBgImage: {
    position: 'relative',
    width: '100%',
    height: 85,
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: 'cover',
  },
  TutorialBgImageOverlay: {
    backgroundColor: 'rgba(15, 15, 15, 0.65)',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  PlaceholderWrapper: {
    position: 'relative',
    overflow: 'hidden',
  },
  PlaceholderBox: {
    position: 'absolute',
    zIndex: -1,
    // zIndex: 2,
    // backgroundColor: '#ddd',
  },

  PagePlaceholderWrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  PagePlaceholderInner: {
    width: '100%',
    height: '100%',
  },

  HeaderShadow: {
    // backgroundColor: '#ddd',
    flexDirection: 'row',
    height: 24,
    marginBottom: 16,
  },
  HeaderShadowText: {
    width: '50%',
  },
  HeaderShadowLink: {
    marginLeft: 'auto',
    width: '25%',
  },

  CardRowShadow: {
    flexDirection: 'row',
    // height: 85,
  },
  CardTutorialShadow: {
    marginRight: 12,
    width: '100%',
    height: 85,
    borderRadius: 6,
    overflow: 'hidden',
  },
  CardProductShadow: {
    marginRight: 12,
    width: '100%',
    height: 285,
    borderRadius: 6,
    overflow: 'hidden',
  },

  // Row & Col
  fdRow: {
    flexDirection: 'row',
  },
  fdCol: {
    flexDirection: 'column',
  },
  mlAuto: {
    marginLeft: 'auto',
  },

  // Header Text
  HeaderText: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_18,
    letterSpacing: 1,
  },
  HeaderAllLink: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.blueBold,
    fontSize: Fonts.size_16,
    letterSpacing: 0.5,
  },

  container: {
    // backgroundColor: "#fff",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Main Area Scroll View
  scrollArea: {
    flex: 1,
  },

  body: {
    // paddingTop: StatusBar.currentHeight,
    width: width,
    position: 'relative',
    zIndex: 1,
    // padding: 20,
  },

  bodyInner: {
    marginTop: 24,
    position: 'relative',
    zIndex: 3,
    // paddingHorizontal: 20,
  },

  // Header section
  fixedHeader: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'relative',
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.4,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.05,
    shadowRadius: 2,
  },
  fixedHeaderPageIndicator: {
    height: 35,
    justifyContent: 'center',
  },
  fixedHeaderText: {
    color: Colors.white,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
    flex: 1,
    paddingVertical: 0,
    borderRadius: 2,
  },
  searchBarIcon: {
    height: 35,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  searchBarInput: {
    // backgroundColor: '#ddd',
    // marginLeft: -8,
    flex: 1,
    // height: 35,
    paddingVertical: 0,
    // backgroundColor: 'transparent',
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    // letterSpacing: 1,
    // justifyContent: 'center',
  },
  searchBarInputPlaceholder: {
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  header: {
    width: '100%',
    height: 130,
    position: 'relative',
    backgroundColor: '#fff',
    // padding: 20,
  },
  headerTop: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    // position: 'absolute',
    // right: 20,
    // top: 0,
    padding: 20,
    // backgroundColor: '#fff',
  },
  icon: {
    width: 20,
    height: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  headerBottom: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    // left: 30,
    paddingHorizontal: 30,
    bottom: 0,
    paddingVertical: 20,
  },

  cartButtonWrapper: {
    position: 'relative',
    marginLeft: 6,
    // backgroundColor: '#ddd',
  },
  cartIndicator: {
    position: 'absolute',
    backgroundColor: Colors.blueBold,
    width: 18,
    height: 18,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    right: 0,
    top: 15,
  },
  cartIndicatorInner: {
    height: 14,
    width: 14,
    borderRadius: 14,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIndicatorInnerText: {
    color: Colors.blueBold,
    fontSize: Fonts.size_14,
    fontFamily: 'NotoSans-Bold',
  },
  // Header Section - - - END

  // Footer Section
  fixedFooter: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 100,
    borderTopWidth: 4,
    borderTopColor: Colors.grayBlackWhite,
  },
  // Footer Section - - - END

  // Content Section
  content: {
    backgroundColor: '#fff',
    marginTop: 0,
    paddingHorizontal: 0,
    // padding: 30,
    paddingBottom: 0,

    // borderTopRightRadius: 25,
    // borderTopLeftRadius: 25,
    position: 'relative',
    zIndex: 3,
  },
  contentOutter: {
    width: '100%',
    paddingVertical: 60,
    backgroundColor: '#555',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'relative',
  },
  contentInner: {
    width: '100%',
    // paddingHorizontal: 30,
    // padding: 20,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 35,
  },
  cart: {
    paddingLeft: 10,
  },

  divider: {
    width: '100%',
    height: 5,
    backgroundColor: Colors.grayBlackWhite,
  },
  viewParentList: {
    borderRadius: 10,
    borderWidth: 0.3,
    margin: 3,
    shadowColor: '#4a4a4a',
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },
  txtproductLocation: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 10,
    textTransform: 'uppercase',
    letterSpacing: 0.65,
    color: '#9b9b9b',
    marginLeft: 5,
  },
  viewLocation: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  imgProduct: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: '100%',

    height: Dimensions.get('screen').height * 0.2,
    resizeMode: 'cover',
  },
  viewDetails: {
    justifyContent: 'space-between',
    flex: 1,
  },
  txtproductName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 11,
    letterSpacing: 0,
    color: '#4a4a4a',
  },
  txtproductPrice: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 13,
    letterSpacing: 0,
    color: '#4a4a4a',
  },

  filterHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingBottom: 14,
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: .4
    // justifyContent: 'center',
  },
  filterHeaderText: {
    fontSize: Fonts.size_18, 
    marginLeft: 8, 
    fontFamily: 'NotoSans-Bold', 
    color: Colors.textGreyDark, 
    letterSpacing: 1
  },
  filterContentWrapper: {
    marginTop: 24,
    paddingBottom: 24,
    borderBottomColor: Fonts.borderGrey,
    borderBottomWidth: .4,
  },
  filterContentWrapperText: {
    paddingHorizontal: 12,
    fontFamily: 'NotoSans-Regular',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_16,
    letterSpacing: 0.5,
  }
});

export default Styles;
