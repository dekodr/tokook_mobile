import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  StyleSheet,
  Animated,
  Image,
  ActivityIndicator,
  Linking,
  ImageBackground,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import * as func from '../../../Helpers/Utils';
import * as c from '../../../Components';
import Card from '../../../Components/Card';
import { TutorialCard } from '../../../Components/Card_V2';
import Button from '../../../Components/Button';
import ButtonIcon from '../../../Components/ButtonIcon';
import tc from '../../TokoModule/Intro/styles';
import styles from './styles';
import {Images, Colors, Fonts} from '../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../Redux/TokoRedux';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Carousel, {Pagination} from 'react-native-snap-carousel';
// import LinearGradient from 'react-native-linear-gradient';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Config from 'react-native-config';
import DeepLinking from 'react-native-deep-linking';

import Swiper from 'react-native-swiper';
import MarketplaceActions from '../../../Redux/MarketPlaceRedux';
import MarketplaceNewActions from '../../../Redux/MarketplaceNewRedux';
import {TouchableOpacity} from 'react-native-gesture-handler';
import { mergeLeft } from 'ramda';
function Marketplace(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    marketplaceforyouProducts,
    marketplacetrendingProducts,
    marketplaceterlarisProducts,
    bannerData,
    cartData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isBottomScrolling, setBottomScrolling] = useState(false);
  const [page, setPage] = useState(1);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter(data => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  useEffect(() => {
    dispatch(TokoActions.getTokoRequest());
    dispatch(TokoActions.getProvinceRequest());
  }, [tokoId]);
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  const handleGoToAllPage = () => {};

  // useEffect(() => {
  //   if (tokoId !== null) {
  //     console.log('kepanggil');
  //     dispatch(MarketplaceActions.getMarketplaceProductsRequest());
  //   }
  // }, [tokoId]);
  // Render Carousel
  const isCarousel = React.useRef(null);

  const INTERVAL_REFRESH = 300;

  const [isLoading,setIsLoading] = useState(true);


  // only for demo purposes
  useEffect(() => {
    if(isLoading){
    //   const timeoutId = setTimeout(() => setIsLoading(true), INTERVAL_REFRESH);
    //   return () => clearTimeout(timeoutId);
    // }
    // else{
      const timeoutId = setTimeout(() => setIsLoading(false), INTERVAL_REFRESH);
      return () => clearTimeout(timeoutId);
    }
  }, [isLoading]);

  const shadowLayout = [
    {
      flexDirection: 'row',
      backgroundColor: Colors.blueBold,
      paddingHorizontal: 12,
      paddingVertical: 12,
      Bottom: 12,
      children: [
        {
          flex: 1,
          height: 32,
          marginRight: 6
        },
        {
          aspectRatio: 1, 
          width: 32,
        },
      ]
    },
    {
      width: "50%",
      height: 28,
      marginTop: 28,
      marginBottom: 12,
      marginLeft: 12,
    },
    {
      flexDirection: 'row',
      marginLeft: 12,
      children: [
        {
          width: Dimensions.get('screen').width * 0.7,
          height: Dimensions.get('screen').height * 0.1,
          marginRight: 12,
        },
        {
          width: Dimensions.get('screen').width * 0.7,
          height: Dimensions.get('screen').height * 0.1,
        },
      ]
    },
    {
      width: "50%",
      height: 28,
      marginTop: 40,
      marginBottom: 12,
      marginLeft: 12,
    },
    {
      flexDirection: 'row',
      marginLeft: 12,
      children: [
        {
          width: Dimensions.get('screen').width * 0.4,
          height: Dimensions.get('screen').height * 0.215,
          marginRight: 12,
        },
        {
          width: Dimensions.get('screen').width * 0.4,
          height: Dimensions.get('screen').height * 0.215,
          marginRight: 12,
        },
        {
          width: Dimensions.get('screen').width * 0.4,
          height: Dimensions.get('screen').height * 0.215,
        },
      ]
    },
  ];

  const renderhorizontalItem = ({item, index}) => {
    console.log('img ku compress', func.compressImage(item.image));
    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(MarketplaceActions.getMarketplaceProductDetailsReset());
          dispatch(MarketplaceActions.getAllShippingFeeReset());
          navigation.navigate('MarketplaceProductScreen', {
            params: item,
          });
        }}
        style={styles.viewParentList}>
        <View
          style={{
            width: Dimensions.get('screen').width * 0.4,
            height: Dimensions.get('screen').width * 0.7,
          }}>
          <View style={{borderWidth: 0.1}}>
            <Image
              source={{uri: func.compressImage(item.image)}}
              style={styles.imgProduct}
            />
          </View>
          <View style={styles.viewDetails}>
            <View style={{padding: 10}}>
              <Text style={styles.txtproductName} numberOfLines={2}>
                {item.name}
              </Text>
              <Text style={styles.txtproductPrice}>
                {func.rupiah(item.price)}
              </Text>
            </View>
            <View style={styles.viewLocation}>
              <SimpleLineIcons
                size={15}
                color={'#4a4a4a'}
                name={'location-pin'}
              />
              <Text style={styles.txtproductLocation}>
                {'KOTA ' + item.city}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  // Tutorial
  const dataCarouselTutorial = [
    {
      id: 1,
      title: 'Tutorial Membuat Toko',
      link: 'https://www.youtube.com/watch?v=rM-pEOk12bY',
      bg: require('../../../assets/image/Cara_Buat_Toko.jpg'),
    },
    {
      id: 2,
      title: 'Cara Dropship Gratis Tanpa Modal',
      link: 'https://www.youtube.com/watch?v=yDF8JlZxSP0&t=20s',
      bg: require('../../../assets/image/Fitur_Dropship.jpg'),
    },
    {
      id: 3,
      title: 'Tutorial Fitur Finansial',
      link: 'https://www.youtube.com/watch?v=RAsnMjn-LM0&t=8s',
      bg: require('../../../assets/image/Fitur_Finansial.jpg'),
    },
    {
      id: 4,
      title: 'Cara Jualan Dengan Sistem COD',
      link: 'https://www.youtube.com/watch?v=GjuGuowac6U',
      bg: require('../../../assets/image/Tutorial_COD.jpg'),
    },
  ];

  const renderCarouselTutorialItem = ({item, index}) => {
    return (
      <Pressable
        onPress={() => Linking.openURL(item.content_url)}
        style={{paddingRight: 12}}>
        <Image
          source={{uri: func.compressImage(item.image_url)}}
          style={{
            width: Dimensions.get('screen').width * 0.7,
            borderRadius: 7,
            height: Dimensions.get('screen').height * 0.1,
          }}
        />
        <View
          style={{
            position: 'absolute',
            width: '100%',
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <View
            style={{
              width: '70%',
              borderRadius: 7,
              paddingLeft: 15,
              justifyContent: 'center',
              height: Dimensions.get('screen').height * 0.1,
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 14,
                letterSpacing: 0.9,
                color: '#fff',
              }}>
              {item.title}
            </Text>
          </View>
        </View>
      </Pressable>
    );
  };

  // Free Ongkir
  const dataCarouselFO = [
    {
      id: 1,
      name: 'Tesla model 4, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 5, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 6, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselFOItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
          handlePress={'test'}
        />
      </Pressable>
    );
  };

  // Most Wanted Product
  const dataCarouselMW = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselMWItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </Pressable>
    );
  };

  // bannercarousel
  const dataCarouselBanner = [
    {
      id: 1,
      image: require('../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 2,
      image: require('../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 3,
      image: require('../../../assets/image/honda-jazz-hero.png'),
    },
  ];
  const renderCarouselBannerItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 0}}>
        <Card type="CardBanner" id={item.id} BannerImage={item.image} />
      </Pressable>
    );
  };
  const PaginationCarouselBanner = () => {
    return (
      <Pagination
        dotsLength={dataCarouselBanner.length}
        activeDotIndex={0}
        containerStyle={{}}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          marginHorizontal: 2,
          backgroundColor: Colors.blueBold,
        }}
        inactiveDotStyle={{
          backgroundColor: Colors.silver,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={0.8}
      />
    );
  };

  // Trending
  const dataCarouselTR = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselTRItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </Pressable>
    );
  };

  // Render Carousel - - - END

  const _handleMarketplaceDetail = item => {
    dispatch(MarketplaceActions.getMarketplaceProductDetailsReset());
    dispatch(MarketplaceActions.getAllShippingFeeReset());
    navigation.navigate('MarketplaceDetailScreen', {
      params: item,
    });
  };

  const _handleMarketplaceProduct = item => {
    console.log('press');
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

  const OnPressCarousel = item => {
    dispatch(MarketplaceActions.getMarketplaceProductDetailsReset());
    dispatch(MarketplaceActions.getAllShippingFeeReset());
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

  const _handleCart = item => {
    navigation.navigate('MarketplaceCartScreen');
  };
  const _handleCategory = item => {
    navigation.navigate('MarketplaceCategoryScreen', {
      params: item,
    });
  };

  const HeaderText = item => {
    // console.log("----", item.text);
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        {/* <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </Pressable> */}
      </View>
    );
  };

  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  const _onEndReached = () => {
    setPage(page + 1);
    console.log('cek on end', true);
    const dataTrending = {
      date: '2021-03-29',
      page: page,
      limit: 5,
    };

    // dispatch(
    //   MarketplaceActions.getMarketplaceTrendingProductsRequest(dataTrending),
    // );
    // dispatch(MarketplaceActions.getMarketplaceProductsRequest());

    dispatch(
      MarketplaceNewActions.productRecomendationRequest(dataTrending, true),
    );
  };
  const _renderFooter = () => {
    return (
      <ActivityIndicator
        style={{marginTop: 12}}
        size={'large'}
        color={Colors.mainBlue}
      />
    );
  };
  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  useEffect(() => {
    // if (props.tokoId) {
    //   props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    // }
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const toSearchScreen = item => {
    navigation.navigate('MarketplaceSearchScreen', {
      params: item,
    });
  };

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
  };
  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  let {height, width} = Dimensions.get('window');

  const componentDidMount = () => {
    DeepLinking.addScheme('example://');
    Linking.addEventListener('url', this.handleUrl);
 
    DeepLinking.addRoute('/test', (response) => {
      // example://test
      this.setState({ response });
    });
 
    DeepLinking.addRoute('/test/:id', (response) => {
      // example://test/23
      this.setState({ response });
    });
 
    DeepLinking.addRoute('/test/:id/details', (response) => {
      // example://test/100/details
      this.setState({ response });
    });
 
    Linking.getInitialURL().then((url) => {
      if (url) {
        Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }
 
  const componentWillUnmount = () => {
    Linking.removeEventListener('url', this.handleUrl);
  }
 
  const handleUrl = ({ url }) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        DeepLinking.evaluateUrl(url);
      }
    });
  }

  // console.log(CarouselOne);
  return (
    <SkeletonContent
      containerStyle={{ 
        flex: 1,
        width: '100%',
        backgroundColor: '#fff'
      }}
      isLoading={isLoading}
      duration={900}
      boneColor="#E1E9EE"
      highlightColor="#F2F8FC"
      animationType="pulse"
      layout={shadowLayout}
    >
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable style={styles.searchBar} onPress={() => toSearchScreen()}>
            <View style={styles.searchBarIcon}>
              <MaterialCommunityIcons
                name={'magnify'}
                color={Colors.textGreyLight}
                size={Fonts.size_24}
              />
            </View>
            <View style={styles.searchBarInput}>
              <Text style={styles.searchBarInputPlaceholder}>Cari Barang</Text>
            </View>
          </Pressable>
          {/* CART BUTTON */}
          <View style={styles.cartButtonWrapper}>
            <View style={[styles.cartIndicator]}>
              <View style={[styles.cartIndicatorInner]}>
                <Text style={styles.cartIndicatorInnerText}>2</Text>
              </View>
            </View>
            <ButtonIcon
              onPress={() => _handleCart()}
              style={[styles.cart]}
              icon="cart-outline"
              color="#fff"
              size={Fonts.size_24 + 4}
              style={{
                // backgroundColor: '#000',
                height: 32,
                width: 32,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 0,
              }}
            />
          </View>
          {/* CART BUTTON */}
          <View style={[styles.cartIndicator]}>
            <View style={[styles.cartIndicatorInner]}>
              <Text style={styles.cartIndicatorInnerText}>
                {cartData === null ? 0 : cartData.length}
              </Text>
            </View>
          </View>
        </View>
        {/* H E A D E R - - - END */}
        <ScrollView
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              _onEndReached();
            } else {
              setBottomScrolling(false);
            }
          }}
          style={styles.scrollArea}>
          {/* B O D Y */}
          <SafeAreaView style={styles.body}>
            <View style={styles.bodyInner}>
              {/* C O N T E N T */}
              <View style={styles.content}>
                <View style={[styles.contentInner]}>
                  {/* <Button
                    onPress={() => Linking.openURL('payok_mobile_tokook://MarketplaceSearchScreen')}
                    text="Open example://test"
                  /> */}
                  {/* Section 1 - Tutorial */}
                  <View
                    style={{
                      marginBottom: 25
                    }}>
                    <Text style={[styles.HeaderText, {paddingHorizontal: 12}]}>
                      Tutorial pakai Toko OK
                    </Text>
                  </View>
                  <View style={{marginTop: 10}}>
                    {bannerData === null ? null : (
                      <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={bannerData.data}
                        renderItem={renderCarouselTutorialItem}
                      />
                    )}
                  </View>

                  {/* Section 2 - Free Ongkir */}
                  {/* <View style={{marginBottom: 25}}>
                    <HeaderText text={'Produk Gratis Ongkir'} />
                    <View style={c.Styles.SliderWrapper}>
                      <Carousel
                        ref={isCarousel}
                        data={dataCarouselFO}
                        renderItem={renderCarouselFOItem}
                        sliderWidth={wp('100%')}
                        itemWidth={wp('45%')}
                        loop={false}
                        loopClonesPerSide={2}
                        inactiveSlideScale={1}
                        inactiveSlideOpacity={1}
                        enableMomentum={true}
                        activeSlideAlignment={'start'}
                        activeAnimationType={'spring'}
                        // activeAnimationOptions={{
                        //     friction: 4,
                        //     tension: 40
                        // }}
                        slideStyle={{overflow: 'visible'}}
                        autoplay={false}
                        autoplayDelay={0}
                        autoplayInterval={6000}
                      />
                    </View>
                  </View> */}

                  {/* Section 3 - Terlaris */}
                  {/* <View style={{marginBottom: 25}}>
                    <HeaderText text={'Produk Terlaris'} />
                    <View style={c.Styles.SliderWrapper}>
                      <Carousel
                        ref={isCarousel}
                        data={dataCarouselMW}
                        renderItem={renderCarouselMWItem}
                        sliderWidth={wp('100%')}
                        itemWidth={wp('45%')}
                        loop={false}
                        loopClonesPerSide={2}
                        inactiveSlideScale={1}
                        inactiveSlideOpacity={1}
                        enableMomentum={true}
                        activeSlideAlignment={'start'}
                        activeAnimationType={'spring'}
                        // activeAnimationOptions={{
                        //     friction: 4,
                        //     tension: 40
                        // }}
                        slideStyle={{overflow: 'visible'}}
                        autoplay={false}
                        autoplayDelay={0}
                        autoplayInterval={6000}
                      />
                    </View>
                  </View> */}

                  {/* Section 4 - Banner */}
                  {/* <View style={{marginBottom: 25}}>
                    <View
                      style={{
                        paddingHorizontal: 12,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text style={styles.HeaderText}>Promo</Text>
                    </View>
                  </View>
                </View> */}
                {/* {marketplaceterlarisProducts != null && (
                  <View style={{marginBottom: 25}}>
                    {marketplaceterlarisProducts.data.length != 0 && (
                      <HeaderText text={'Produk Terlaris'} />
                    )}
                    {marketplaceterlarisProducts === null ? (
                      <ActivityIndicator size={'small'} color={'#000'} />
                    ) : (
                      <View
                        style={{
                          position: 'absolute',
                          alignSelf: 'center',
                          bottom: -15,
                        }}>
                        <PaginationCarouselBanner />
                      </View>
                    )}
                  </View>
                )} */}

                {/* Section 5 - Trending */}
                {marketplacetrendingProducts != null && (
                  <View style={{marginBottom: 12}}>
                    {marketplacetrendingProducts.data.length != 0 && (
                      <HeaderText text={'Produk Trending'} />
                    )}
                    {marketplacetrendingProducts === null ? (
                      <ActivityIndicator size={'small'} color={'#000'} />
                    ) : (
                      <View
                        style={{
                          paddingHorizontal: 12,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        {marketplaceforyouProducts.data.length != 0 && (
                          <Text style={styles.HeaderText}>Produk buat kamu</Text>
                        )}
                        <Pressable
                          delayPressIn={0}
                          onPress={() => _handleMarketplaceDetail()}
                          style={{
                            marginLeft: 'auto',
                          }}></Pressable>
                      </View>
                    )}
                      {marketplaceforyouProducts === null ? (
                        <ActivityIndicator size={'small'} color={'#000'} />
                      ) : (
                        <FlatList
                          horizontal
                          showsHorizontalScrollIndicator={false}
                          data={marketplacetrendingProducts.data.baru}
                          renderItem={renderhorizontalItem}
                        />
                      )}
                  </View>
                    )}
                    {marketplaceforyouProducts === null ? (
                      <ActivityIndicator size={'small'} color={'#000'} />
                    ) : (
                      <FlatList
                        columnWrapperStyle={{justifyContent: 'space-between'}}
                        numColumns={2}
                        style={
                          ([styles.CardRow],
                          {
                            paddingLeft: 6,
                            paddingRight: 12,
                            marginTop: 0,
                            flexDirection: 'column',
                          })
                        }
                        data={marketplaceforyouProducts.data.result}
                        renderItem={({item}) => (
                          <ListItem
                            key={item.id}
                            style={{
                              marginLeft: 0,
                              paddingTop: 0,
                              paddingBottom: 0,
                              paddingRight: 0,
                              width: width / 2 - 7,
                              marginTop: item.id - 1 == -1 ? 0 : 12,
                              borderBottomColor: 'rgba(0,0,0,0)',
                            }}>
                            <Pressable
                              onPress={() => OnPressCarousel(item)}
                              style={{
                                paddingRight: 12,
                                width: '100%',
                                marginLeft: item.id % 2 == 0 ? 0 : 6,
                                marginRight: item.id % 2 == 1 ? 6 : 0,
                              }}>
                              <Card
                                type="CardProduct"
                                id={item.product_id}
                                ProductName={item.name}
                                ProductThumbnail={func.compressImage(
                                  item.image,
                                )}
                                ProductPrice={func.rupiah(item.price)}
                                ProductStoreLocation={item.city}
                              />
                            </Pressable>
                          </ListItem>
                        )}
                      />
                    )}
                  </View>
              </View>
              {/* C O N T E N T - - - END*/}
            </View>
          </SafeAreaView>
          {/* B O D Y - - - END */}
        </ScrollView>
      </Container>
    </SkeletonContent>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    marketplaceforyouProducts: state.marketplace.MarketplaceProductsData.data,
    marketplacetrendingProducts:
      state.marketplace.MarketplaceTrendingProductsData.data,
    marketplaceterlarisProducts:
      state.marketplace.MarketplaceTerlarisProductsData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    bannerData: state.marketplace.getTutorialBannerData.data,
    cartData: state.marketplace.getCartData.data
      ? state.marketplace.getCartData.data.data
        ? state.marketplace.getCartData.data.data.cart
        : null
      : null,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Marketplace);
