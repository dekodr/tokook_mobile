import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const Styles = StyleSheet.create({
  titleHeader: {
    flexDirection: 'row',
    width: '100%',
    height: 200,
  },
  titleLeft: {
    width: '50%',
    height: '100%',
    paddingTop: 12,
    paddingBottom: 32,
    paddingLeft: 12,
    // backgroundColor: '#ddd'
  },
  titleText: {
    marginTop: 65,
  },
  textTitle: {
    fontSize: Fonts.size_16+1,
    color: Colors.textGreyLight,
    letterSpacing: 0.6,
    marginBottom: 3,
  },
  textSubTitle: {
    fontSize: Fonts.size_24,
    color: Colors.textGreyDark,
    fontWeight: 'bold',
    letterSpacing: 0.6,
  },
  titleBg: {
    width: '50%',
    height: '100%',
  },
  titleBgImg: {
    width: '100%',
    height: '100%',
  },

  // FILTER HEADER AREA
  headerFilter: {
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: '#C9C6C6',
    borderWidth: 0.4,
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
  },
  filterText: {
    position: 'relative',
    width: '55%',
    height: 30,
    padding: 5,
    paddingLeft: 40,
    borderColor: '#979797',
    color: '#979797',
    borderWidth: 0.4,
    borderRadius: 5,
  },
  filterTextIcon: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 5,
    left: 5,
  },
  filterButton: {
    position: 'relative',
    width: '20%',
    marginLeft: 9,
    height: 30,
    padding: 5,
    backgroundColor: '#fff',
    borderColor: '#979797',
    color: '#979797',
    borderWidth: 0.4,
    borderRadius: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterButtonIcon: {
    color: '#979797',
    alignSelf: 'flex-start',
  },
  filterButtonText: {
    color: '#979797',
    alignSelf: 'flex-end',
  },

  fixedFooter: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Styles;
