import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  BackHandler,
  ImageBackground,
  Animated,
  SafeAreaView,
  Image,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  // Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import {Images, Colors, Fonts} from '../../../../Themes/';
import Card from '../../../../Components/Card';

import Button from '../../../../Components/Button';
import ButtonIcon from '../../../../Components/ButtonIcon';
import {FilterOption} from '../../../../Components/Option';

import page from './styles';
import styles from '../../Intro/styles';
import func from './../../../../Helpers/Utils';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function MarketplaceDetailSearch(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    searchResultData,
    searchInput,
    isSearchProductSuccess,
  } = props;

  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;

  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  let {height, width} = Dimensions.get('window');

  const refRBSheet = useRef();
  const refRBSheetFilter = useRef();

  return (
    <Container style={[styles.PlaceholderWrapper, {width: '100%'}]}>
      <Container styles={[styles.container]}>
        {/* H E A D E R - - - END */}
        <ScrollView style={[styles.scrollArea]}>
          <SafeAreaView
            style={[styles.body, {backgroundColor: 'rgba(0,0,0,0)'}]}>
            <View style={[styles.bodyInner, {marginTop: 0}]}>
              {/* C O N T E N T */}
              <View style={styles.content}>
                <View style={[styles.contentInner]}>
                  {searchResultData != null && !isSearchProductSuccess && (
                    <View style={{flex: 1}}>
                      <Image
                        style={{
                          alignSelf: 'center',
                          width: '38%',
                          resizeMode: 'contain',
                        }}
                        source={Images.searchKosongIcon}
                      />
                      <Text
                        style={{
                          alignSelf: 'center',
                          fontFamily: 'NotoSans',
                          marginTop: -23,
                          color: Colors.grayWhite,
                        }}>{`"${searchInput}" tidak dapat ditemukan`}</Text>
                    </View>
                  )}
                  {/* Filter Area */}
                  {/* <View style={[page.headerFilter]}>
                    <View
                      style={{
                        borderWidth: 0.4,
                        borderColor: Colors.textGrey,
                        paddingHorizontal: 8,
                        borderRadius: 3,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <ButtonIcon
                        onPress={() => refRBSheetFilter.current.open()}
                        icon="filter-variant"
                        text="Filter"
                        color={Colors.textGrey}
                        size={Fonts.size_28 + 1}
                        style={{
                          backgroundColor: 'transparent',
                          // borderWidth: .4,
                          // borderColor: Colors.textGrey,
                          height: 35,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                        textStyle={{
                          letterSpacing: 0.4,
                          paddingLeft: 4,
                          color: Colors.textGrey,
                          fontFamily: 'NotoSans-Reguler',
                        }}
                      />
                      <View
                        style={[
                          styles.cartIndicatorInner,
                          {
                            backgroundColor: Colors.blueBold,
                            height: 17,
                            width: 17,
                            marginLeft: 8,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.cartIndicatorInnerText,
                            {color: Colors.white},
                          ]}>
                          2
                        </Text>
                      </View>
                    </View>
                    <Button
                      text="Bebas Ongkir"
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0.4,
                        borderColor: Colors.textGrey,
                        height: 35,
                        width: 'auto',
                        paddingHorizontal: 8,
                        borderRadius: 3,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginLeft: 6,
                      }}
                      textStyle={{
                        letterSpacing: 0.4,
                        color: Colors.textGrey,
                        fontSize: Fonts.size_16,
                        fontFamily: 'NotoSans-Reguler',
                      }}
                    />
                    <Button
                      text="COD"
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0.4,
                        borderColor: Colors.textGrey,
                        height: 35,
                        width: 'auto',
                        paddingHorizontal: 8,
                        borderRadius: 3,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginLeft: 6,
                      }}
                      textStyle={{
                        letterSpacing: 0.4,
                        color: Colors.textGrey,
                        fontSize: Fonts.size_16,
                        fontFamily: 'NotoSans-Reguler',
                      }}
                    />
                    <Button
                      text="Kurir Instan"
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0.4,
                        borderColor: Colors.textGrey,
                        height: 35,
                        width: 'auto',
                        paddingHorizontal: 8,
                        borderRadius: 3,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginLeft: 6,
                      }}
                      textStyle={{
                        letterSpacing: 0.4,
                        color: Colors.textGrey,
                        fontSize: Fonts.size_16,
                        fontFamily: 'NotoSans-Reguler',
                      }}
                    />
                  </View>
                */}
                  {/* Filter Area */}

                  {/* Content Area */}
                  {searchResultData != null && (
                    <FlatList
                      columnWrapperStyle={{justifyContent: 'space-between'}}
                      numColumns={2}
                      style={
                        ([styles.CardRow],
                        {
                          paddingLeft: 6,
                          paddingRight: 12,
                          marginTop: 0,
                          flexDirection: 'column',
                        })
                      }
                      data={searchResultData.data}
                      renderItem={({item}) => (
                        <ListItem
                          key={item.id}
                          style={{
                            marginLeft: 0,
                            paddingTop: 0,
                            paddingBottom: 0,
                            paddingRight: 0,
                            width: width / 2 - 7,
                            marginTop: item.id - 1 == -1 ? 0 : 12,
                            borderBottomColor: 'rgba(0,0,0,0)',
                          }}>
                          <Pressable
                            onPress={() =>
                              navigation.navigate('MarketplaceProductScreen', {
                                params: item,
                              })
                            }
                            style={{
                              paddingRight: 12,
                              width: '100%',
                              marginLeft: item.id % 2 == 0 ? 0 : 6,
                              marginRight: item.id % 2 == 1 ? 6 : 0,
                            }}>
                            <Card
                              type="CardProduct"
                              id={item.product_id}
                              ProductName={item.name}
                              ProductThumbnail={item.image}
                              ProductPrice={func.rupiah(item.price)}
                              ProductStoreLocation={item.city}
                            />
                          </Pressable>
                        </ListItem>
                      )}
                    />
                  )}

                  {/* Content Area */}
                </View>
              </View>
              {/* C O N T E N T - - - END*/}
            </View>
          </SafeAreaView>
        </ScrollView>

        <RBSheet
          ref={refRBSheetFilter}
          closeOnDragDown={true}
          // height={500}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              minHeight: '75%',
              paddingBottom: 120,
            },
          }}>
          {/* Your Heres Here */}
          <View style={styles.filterHeader}>
            <MaterialCommunityIcons
              name={'filter-variant'}
              color={Colors.textGreyDark}
              size={Fonts.size_28 + 4}
            />
            <Text style={styles.filterHeaderText}>Filter</Text>
          </View>
          <View>
            <ScrollView style={{paddingBottom: 24}}>
              <View style={styles.filterContentWrapper}>
                <Text style={styles.filterContentWrapperText}>
                  Lokasi Supplier
                </Text>
                <FilterOption
                  options={[
                    'Jabodetabek',
                    'DKI Jakarta',
                    'Banten',
                    'Sumatera Selatan',
                    'Sumatera Utara ',
                    'Jawa Barat',
                    'Jawa Tengah',
                    'Jawa Timur',
                  ]}
                  onChange={option => {
                    console.log(option);
                  }}
                />
              </View>

              <View style={styles.filterContentWrapper}>
                <Text style={styles.filterContentWrapperText}>
                  Metode Pembayaran
                </Text>
                <FilterOption
                  options={[
                    'COD (Bayar di Tempat)',
                    'Transfer Bank',
                    'QRIS (e-wallet)',
                  ]}
                  onChange={option => {
                    console.log(option);
                  }}
                />
              </View>

              <View style={styles.filterContentWrapper}>
                <Text style={styles.filterContentWrapperText}>Kategori</Text>
                <FilterOption
                  options={[
                    'Parfum',
                    'Kaos',
                    'Atasan Pria',
                    'Atasan Wanita',
                    'Bawahan Pria',
                    'Rok',
                    'Sweater',
                    'Jaket Hoodie',
                  ]}
                  onChange={option => {
                    console.log(option);
                  }}
                />
              </View>

              {/* <View style={styles.filterContentWrapper}>
								<Text style={styles.filterContentWrapperText}>harga (Rp)</Text>
								<FilterOption
									options={['Parfum', 'Kaos', 'Atasan Pria', 'Atasan Wanita', 'Bawahan Pria', 'Rok', 'Sweater', 'Jaket Hoodie']}
									onChange={(option) => {
										console.log(option);
									}}
								/>
							</View> */}

              <View style={[styles.filterContentWrapper, {marginBottom: 50}]}>
                <Text style={styles.filterContentWrapperText}>Pengiriman</Text>
                <FilterOption
                  options={['Instan', 'Same Day', 'Next Day', 'Reguler']}
                  onChange={option => {
                    console.log(option);
                  }}
                />
              </View>
            </ScrollView>
          </View>

          {/* F O O T E R */}
          <View
            style={[
              styles.fixedFooter,
              {backgroundColor: Colors.white, elevation: 1},
            ]}>
            <Pressable style={{flex: 2, marginRight: 6}}>
              <ButtonIcon
                // icon="store-outline"
                text="Hilangkan"
                color={Colors.blueBold}
                size={Fonts.size_28}
                // AddtoSomething
                plusColor={Colors.blueBold}
                style={{
                  borderRadius: 6,
                  width: '100%',
                  height: 48,
                  borderColor: Colors.blueBold,
                  borderWidth: 2,
                  borderStyle: 'solid',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                textStyle={{
                  color: Colors.blueBold,
                  fontFamily: 'NotoSans-Bold',
                  marginLeft: 15,
                }}
              />
            </Pressable>
            <Pressable
              onPress={() => refRBSheet.current.close()}
              style={{flex: 2, marginLeft: 6}}>
              <ButtonIcon
                // icon="cart-outline"
                text="Terapkan"
                color={Colors.white}
                size={Fonts.size_28}
                // AddtoSomething
                plusColor={Colors.white}
                style={{
                  borderRadius: 6,
                  width: '100%',
                  height: 48,
                  backgroundColor: Colors.blueBold,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                textStyle={{
                  color: Colors.white,
                  fontFamily: 'NotoSans-Bold',
                  marginLeft: 15,
                }}
              />
            </Pressable>
          </View>
          {/* F O O T E R - - - END */}
        </RBSheet>
      </Container>
      {/* <FadeInView style={{position: 'absolute', height: '100%', width: '100%'}}>
				<View style={{height: '100%', width: '100%', backgroundColor: '#fff'}}>
					<View style={{width: '100%', flexDirection: 'row', borderBottomColor: '#C9C6C6', borderBottomWidth: 0.4,}}>
						<View style={{height: 200, width: '50%', justifyContent: 'flex-end'}}>
							<View style={{width: '75%', height: 22, marginVertical: 48, marginLeft: 18}}>
                <SkeletonPlaceholder>
                  <View style={{width: '100%', height: '100%'}} />
                </SkeletonPlaceholder>
              </View>
						</View>
						<View style={{height: 200, width: '50%'}}>
							<SkeletonPlaceholder>
								<View style={{width: '100%', height: '100%'}} />
							</SkeletonPlaceholder>
						</View>
          </View>

					<View style={{flexDirection: 'row', paddingHorizontal: 12, paddingVertical: 10, borderBottomColor: '#C9C6C6', borderBottomWidth: 0.4,}}>
						<View style={{flex: 1, height: 30, borderRadius: 5, overflow: 'hidden'}}>
							<SkeletonPlaceholder>
								<View style={{width: '100%', height: '100%'}} />
							</SkeletonPlaceholder>
						</View>
						<View style={{width: '20%', height: 30, marginLeft: 9, borderRadius: 5, overflow: 'hidden'}}>
							<SkeletonPlaceholder>
								<View style={{width: '100%', height: '100%'}} />
							</SkeletonPlaceholder>
						</View>
						<View style={{width: '20%', height: 30, marginLeft: 9, borderRadius: 5, overflow: 'hidden'}}>
							<SkeletonPlaceholder>
								<View style={{width: '100%', height: '100%'}} />
							</SkeletonPlaceholder>
						</View>
					</View>

					<View style={{flexDirection: 'row', flexWrap: 'wrap', paddingHorizontal: 10, paddingVertical: 13}}>
						<View style={{width: wp('50%')-16, height: 285, borderRadius: 6, overflow: 'hidden', marginRight: 6, marginBottom: 12}}>
              <SkeletonPlaceholder>
                <View style={{width: '100%', height: '100%'}} />
              </SkeletonPlaceholder>
            </View>
						<View style={{width: wp('50%')-16, height: 285, borderRadius: 6, overflow: 'hidden', marginLeft: 6, marginBottom: 12}}>
              <SkeletonPlaceholder>
                <View style={{width: '100%', height: '100%'}} />
              </SkeletonPlaceholder>
            </View>
						<View style={{width: wp('50%')-16, height: 285, borderRadius: 6, overflow: 'hidden', marginRight: 6, marginBottom: 12}}>
              <SkeletonPlaceholder>
                <View style={{width: '100%', height: '100%'}} />
              </SkeletonPlaceholder>
            </View>
						<View style={{width: wp('50%')-16, height: 285, borderRadius: 6, overflow: 'hidden', marginLeft: 6, marginBottom: 12}}>
              <SkeletonPlaceholder>
                <View style={{width: '100%', height: '100%'}} />
              </SkeletonPlaceholder>
            </View>
					</View>
					
				</View>
      </FadeInView> */}
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    searchResultData: state.marketplace.productBySearchData.data,
    isSearchProductSuccess: state.marketplace.productBySearchData.success,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceDetailSearch);
