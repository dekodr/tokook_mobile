import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  ImageBackground,
  Image,
  BackHandler,
  SafeAreaView,
  aspectRatio,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import SearchResult from './../Result';
import * as c from '../../../../Components';
import styles from '../../Intro/styles';
import page from './styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
import Card from '../../../../Components/Card';
import ButtonIcon from '../../../../Components/ButtonIcon';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import MarketPlaceActions from '../../../../Redux/MarketPlaceRedux';
function MarketplaceSearch(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    getProductLastViewData,
    isFetchingSearch,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  useEffect(() => {
    dispatch(MarketPlaceActions.getProductBySearchReset());

    const params = {page: 1, limit: 9999999};
    dispatch(MarketPlaceActions.getProductLastViewRequest(params, false));
  }, []);
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  // useEffect(() => {
  //   BackHandler.addEventListener('hardwareBackPress', handleGoBack);

  //   return () =>
  //     BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  // }, []);

  let {height, width} = Dimensions.get('window');

  const HeaderText = item => {
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </Pressable>
      </View>
    );
  };

  return (
    <Container
      style={[
        styles.PlaceholderWrapper,
        {width: '100%', backgroundColor: '#fff'},
      ]}>
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 8}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </Pressable>
          <View style={[styles.searchBar]}>
            <View style={styles.searchBarIcon}>
              <MaterialCommunityIcons
                name={'magnify'}
                color={Colors.textGrey}
                size={Fonts.size_24}
              />
            </View>
            <TextInput
              onChangeText={x => {
                setSearchInput(x);
                dispatch(MarketPlaceActions.getProductBySearchRequest({q: x}));
              }}
              placeholder="Cari barang..."
              style={[styles.searchBarInput, {marginLeft: 0}]}
              underlineColorAndroid="transparent"
              autoFocus
            />
            {isFetchingSearch && (
              <ActivityIndicator color={Colors.mainBlue} size={'small'} />
            )}
          </View>
        </View>

        {/* H E A D E R - - - END */}
        {searchInput != '' || searchInput === '' ? (
          <SearchResult searchInput={searchInput} navigation={navigation} />
        ) : (
          <ScrollView style={styles.scrollArea}>
            {/* B O D Y */}
            <SafeAreaView style={styles.body}>
              <View style={styles.bodyInner}>
                {/* C O N T E N T */}
                <View style={styles.content}>
                  <View style={[styles.contentInner, {marginTop: 0}]}>
                    {/* Section 1 - Terakhir dilihat */}
                    <View style={{paddingBottom: 24, paddingHorizontal: 12}}>
                      <View
                        style={{
                          paddingHorizontal: 0,
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 12,
                        }}>
                        <Text style={styles.HeaderText}>Terakhir dilihat</Text>
                      </View>
                      {/* last seen list */}
                      <FlatList
                        horizontal={true}
                        style={page.LastSeen}
                        data={getProductLastViewData}
                        renderItem={({item}) => (
                          <ListItem
                            key={item.key}
                            style={[
                              page.LastSeenItem,
                              {
                                aspectRatio: 1,
                                width: width / 5 - 9.75,
                                marginLeft: 6,
                              },
                            ]}>
                            <Pressable
                              onPress={() =>
                                navigation.navigate(
                                  'MarketplaceProductScreen',
                                  {
                                    params: item,
                                  },
                                )
                              }
                              style={{}}>
                              <Card
                                type="CardProductSmall"
                                id={item.key}
                                ProductThumbnail={item.image}
                              />
                            </Pressable>
                          </ListItem>
                        )}
                      />
                    </View>

                    {/* Section 2 - Terakhir dicari */}
                    <View style={{paddingBottom: 24}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingHorizontal: 12,
                          paddingBottom: 12,
                        }}>
                        <Text style={styles.HeaderText}>Terakhir dicari</Text>
                        <Pressable
                          delayPressIn={0}
                          onPress={() => _handleMarketplaceDetail()}
                          style={{
                            marginLeft: 'auto',
                          }}>
                          <Text
                            style={[
                              styles.HeaderAllLink,
                              {color: Colors.redLight},
                            ]}>
                            Hapus semua
                          </Text>
                        </Pressable>
                      </View>
                      {/* Last searched list */}
                      <FlatList
                        style={page.SearchList}
                        data={[
                          {title: 'Last search 1', key: 'item1'},
                          {title: 'Last search 2', key: 'item2'},
                          {title: 'Last search 3', key: 'item3'},
                        ]}
                        renderItem={({item}) => (
                          <ListItem key={item.key} style={page.SearchListItem}>
                            <View
                              style={[styles.PlaceholderWrapper, {flex: 1}]}>
                              {/* <FadeInView><SkeletonPlaceholder><View style={{width: '100%', height: 22}} /></SkeletonPlaceholder></FadeInView> */}

                              <Text style={page.SearchListItemText}>
                                {item.title}
                              </Text>
                            </View>

                            <Pressable style={{}}>
                              <ButtonIcon
                                icon="plus"
                                color={Colors.textGrey}
                                size={Fonts.size_28+6}
                                style={{
                                  // backgroundColor: '#ddd',
                                  // borderRadius: 35,
                                  width: 24,
                                  height: 24,
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                  transform: [{rotate: '45deg'}],
                                  marginBottom: 2,
                                }}
                              />
                            </Pressable>
                          </ListItem>
                        )}
                      />
                      {/* <Pressable>
                        <Text style={page.SearchListExpandToggle}>
                          Lihat semua
                        </Text>
                      </Pressable> */}
                    </View>
                  </View>
                </View>
                {/* C O N T E N T - - - END */}
              </View>
            </SafeAreaView>
            {/* B O D Y - - - END */}
          </ScrollView>
        )}
      </Container>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    getProductLastViewData: state.marketplace.productLastViewData.data,
    isFetchingSearch: state.marketplace.productBySearchData.fetching,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceSearch);
