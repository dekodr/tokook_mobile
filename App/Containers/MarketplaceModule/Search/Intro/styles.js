import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const { width, height } = Dimensions.get('window');

const page = StyleSheet.create({

  LastSeen: {
    // backgroundColor: '#ddd',
    marginLeft: -6,
    overflow: 'visible',
  },
  LastSeenItem: {
    marginLeft: 0, 
    paddingHorizontal: 0,
    paddingVertical: 0,
    // backgroundColor: '#678',
    overflow: 'hidden',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 6,
  },

  SearchList: {
    // backgroundColor: '#ddd',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,.05)',
    borderStyle: 'solid',
  },
  SearchListItem: {
    // backgroundColor: '#678',
    marginLeft: 0,
    marginRight: 0,
    paddingHorizontal: 12,
    height: 56,
    // height: 20,
    // width: '100%',
    paddingVertical: 0,
    // marginVertical: 12,
    // justifyContent: 'space-between',
  },
  SearchListItemImage: {
    // backgroundColor: '#ddd', 
    borderRadius: 6, 
    aspectRatio: 1, 
    width: 64,
    overflow: 'hidden',
    resizeMode: "cover",
  },
  SearchListItemText: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
    // flex: 1,
    // backgroundColor: '#ddd',
    // paddingTop: 15,
    // paddingBottom: 30,
    // justifyContent: 'center',
  },
  SearchListExpandToggle: {
    paddingHorizontal: 12,
    paddingVertical: 10,
    // justifyContent: 'center',
    height: 50,
    // borderTopWidth: 1,
    // borderTopColor: 'rgba(0,0,0,0.05)',
    borderBottomWidth: 0.4,
    borderBottomColor: 'rgba(0,0,0,0.05)',
    borderStyle: 'solid',
    fontFamily: 'NotoSans-Bold',
    color: Colors.blueBold,
    fontSize: Fonts.size_16,
    letterSpacing: 0.5,
  }

});

export default page;