import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  SafeAreaView,
  ImageBackground,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import CheckBox from '@react-native-community/checkbox';
import Immutable from 'seamless-immutable';

import _, {filter} from 'lodash';
import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
import VerticalTabBar from '../../../../Components/SupplierTab';
import tc from '../../../TokoModule/Intro/styles';
// import styles from '../../Intro/styles';
import styles from './styles';
import SupStyle from '../../Supplier/Intro/styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
import MarketplaceActions from './../../../../Redux/MarketPlaceRedux';
import ButtonIcon from '../../../../Components/ButtonIcon';
import Button from '../../../../Components/Button';
import func from '../../../../Helpers/Utils';
// import TabViewVertical from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {padEnd} from 'lodash-es';

function Cart(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    cartData,
    isSuccessCartUpdated,
    fetchingcartData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [dataCart, setdataCart] = useState([]);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [chooseAll, setChooseAll] = useState(false);
  const [totalCheckout, setTotalCheckout] = useState(0);

  const [itemId, setitemId] = useState(0);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter((data) => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(MarketplaceActions.getCartDataRequest());
  };
  const handleGoBack = () => {
    dispatch(MarketplaceActions.getCartDataRequest());
    navigation.goBack();
    return true;
  };
  const handleGoToAllPage = () => {};

  const _handleMarketplaceAddress = (item) => {
    dispatch(MarketplaceActions.addCartToCheckoutProceed(dataCart));
    navigation.navigate('MarketplaceAddressScreen', {
      params: item,
    });
  };

  const handleDeleteProduct = (index, i) => {
    let _dataCart = [...dataCart];
    let arr = [];
    let filtered = [];
    filtered = _dataCart.map((e) => {
      if (_dataCart[index].product_data.length > 1) {
        return {
          supplier_name: e.supplier_name,
          supplier_isSelected: e.supplier_isSelected,
          supplier_city: e.supplier_city,
          supplier_id: e.supplier_id,
          product_data: e.product_data.filter(
            (v) => v.cart_id !== _dataCart[index].product_data[i].cart_id,
          ),
        };
      } else {
        arr = _dataCart.filter(
          (e) => e.supplier_name != _dataCart[index].supplier_name,
        );
        console.log('arr', arr);
        setdataCart(arr);
      }
    });
    if (_dataCart[index].product_data.length > 1) {
      console.log('filtered', filtered);
      setdataCart(filtered);
    }
  };
  const handlesetChildProduct = (newArr, index, dataCart) => {
    let dataFilter = [];
    let filterData = [];
    dataFilter = dataCart[index].product_data.filter(
      (v) => v.isSelected === false,
    );

    newArr = dataCart.map((v, i) => {
      if (index === i) {
        return {
          supplier_name: v.supplier_name,
          supplier_isSelected: dataFilter.length > 0 ? false : true,
          supplier_city: v.supplier_city,
          supplier_id: v.supplier_id,
          product_data: v.product_data.map((value, i) => {
            return {
              cart_id: value.cart_id,
              code: value.code,
              quantity: value.quantity,
              product_id: value.product_id,
              name: value.name,
              images: value.images,
              is_from_supplier: value.is_from_supplier,
              supplier_id: value.supplier_id,
              supplier_name: value.supplier_name,
              supplier_city: value.supplier_city,
              variant: value.variant,
              price: value.price,
              isSelected: value.isSelected,
              markup_price: value.price,
            };
          }),
        };
      } else {
        return {
          supplier_name: v.supplier_name,
          supplier_isSelected: v.supplier_isSelected,
          supplier_city: v.supplier_city,
          supplier_id: v.supplier_id,
          product_data: v.product_data.map((value, i) => {
            return {
              cart_id: value.cart_id,
              code: value.code,
              quantity: value.quantity,
              product_id: value.product_id,
              name: value.name,
              images: value.images,
              is_from_supplier: value.is_from_supplier,
              supplier_id: value.supplier_id,
              supplier_name: value.supplier_name,
              supplier_city: value.supplier_city,
              variant: value.variant,
              price: value.price,
              isSelected: value.isSelected,
              markup_price: value.price,
            };
          }),
        };
      }
    });
    filterData = newArr.filter((v) => v.supplier_isSelected === true);
    if (filterData.length === dataCart.length) {
      setChooseAll(true);
    } else {
      setChooseAll(false);
    }
    setdataCart(newArr);
    console.log('dataFilter', dataFilter);
    // dispatch(MarketplaceActions.getCartDataSuccess(result));
  };
  const handleSetChooseAll = (newArr, dataCart, newValue) => {
    newArr = dataCart.map((v, i) => {
      return {
        supplier_name: v.supplier_name,
        supplier_isSelected: newValue,
        supplier_city: v.supplier_city,
        product_data: v.product_data.map((value, i) => {
          return {
            cart_id: value.cart_id,
            code: value.code,
            quantity: value.quantity,
            product_id: value.product_id,
            name: value.name,
            images: value.images,
            is_from_supplier: value.is_from_supplier,
            supplier_id: value.supplier_id,
            supplier_name: value.supplier_name,
            supplier_city: value.supplier_city,
            variant: value.variant,
            price: value.price,
            isSelected: newValue,
            markup_price: value.price,
          };
        }),
      };
    });
    setdataCart(newArr);
  };
  const handlesetProduct = (newArr, dataCart, newValue, index) => {
    let dataFilter = [];
    dataFilter = dataCart.filter((v) => v.supplier_isSelected === true);
    newArr = dataCart.map((v, i) => {
      if (index === i) {
        return {
          supplier_name: v.supplier_name,
          supplier_isSelected: v.supplier_isSelected,
          supplier_city: v.supplier_city,
          supplier_id: v.supplier_id,
          product_data: v.product_data.map((value, i) => {
            return {
              cart_id: value.cart_id,
              code: value.code,
              quantity: value.quantity,
              product_id: value.product_id,
              name: value.name,
              images: value.images,
              is_from_supplier: value.is_from_supplier,
              supplier_id: value.supplier_id,
              supplier_name: value.supplier_name,
              supplier_city: value.supplier_city,
              variant: value.variant,
              price: value.price,
              isSelected: v.supplier_isSelected,
              markup_price: value.price,
            };
          }),
        };
      } else {
        return {
          supplier_name: v.supplier_name,
          supplier_isSelected: v.supplier_isSelected,
          supplier_city: v.supplier_city,
          supplier_id: v.supplier_id,
          product_data: v.product_data.map((value, i) => {
            return {
              cart_id: value.cart_id,
              code: value.code,
              quantity: value.quantity,
              product_id: value.product_id,
              name: value.name,
              images: value.images,
              is_from_supplier: value.is_from_supplier,
              supplier_id: value.supplier_id,
              supplier_name: value.supplier_name,
              supplier_city: value.supplier_city,
              variant: value.variant,
              price: value.price,
              isSelected: value.isSelected,
            };
          }),
        };
      }
    });
    if (dataFilter.length === newArr.length) {
      setChooseAll(true);
    } else {
      setChooseAll(false);
    }
    setdataCart(newArr);
  };

  const renderproductCart = ({item, index}) => {
    let _dataCart = [...dataCart];
    return (
      <View style={{marginTop: 20}}>
        <View style={{flexDirection: 'row'}}>
          <CheckBox
            value={dataCart[index].supplier_isSelected}
            onValueChange={(newValue) => {
              let dataLatest = [];
              _dataCart[index].supplier_isSelected = newValue;

              handlesetProduct(dataLatest, _dataCart, newValue, index);
            }}
          />
          <View>
            <View style={{padding: 3}}>
              <Text style={styles.txtsupplierName}>{item.supplier_name}</Text>
            </View>
            <View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View
                  style={{
                    paddingRight: 3,
                    paddingTop: 3,
                    paddingBottom: 3,
                  }}>
                  <MaterialCommunityIcons
                    name="map-marker-outline"
                    size={18}
                    color={'#979797'}
                  />
                </View>
                <Text style={styles.txtsupplierCity}>{item.supplier_city}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          {item.product_data.map((v, i) => {
            return (
              <View style={{flexDirection: 'row'}}>
                <CheckBox
                  value={dataCart[index].product_data[i].isSelected}
                  onValueChange={(newValue) => {
                    _dataCart[index].product_data[i].isSelected = newValue;
                    let dataLatest = [];
                    handlesetChildProduct(dataLatest, index, _dataCart);
                  }}
                />
                <View style={{flexDirection: 'row', flex: 1}}>
                  <View
                    style={{
                      padding: 5,
                      borderColor: 0.5,
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 1,
                      },
                      shadowOpacity: 0.22,
                      shadowRadius: 2.22,

                      elevation: 3,
                    }}>
                    <Image
                      source={{uri: v.images[0].url}}
                      style={{
                        width: Dimensions.get('screen').width * 0.16,
                        height: Dimensions.get('screen').height * 0.08,
                        borderRadius: 3,
                        resizeMode: 'cover',
                      }}
                    />
                  </View>
                  <View style={{padding: 5, flex: 1}}>
                    <View>
                      <Text style={styles.txtproductName}>{v.name}</Text>
                    </View>
                    <View>
                      <Text style={styles.txtvariantName}>{v.variant}</Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View>
                        <Text style={styles.txtproductPrice}>
                          {func.rupiah(v.price * v.quantity)}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          flex: 0.7,
                          justifyContent: 'space-around',
                        }}>
                        <Pressable
                          onPress={() => {
                            //handleremoveProduct(v);
                            let dataRemove = {
                              item: {
                                product: {
                                  id: v.product_id,
                                  code: v.code,
                                },
                              },
                              func: {reloadData: handleRefresh},
                            };
                            handleDeleteProduct(index, i);
                            dispatch(
                              MarketplaceActions.deleteCartDataRequest(
                                dataRemove,
                              ),
                            );
                          }}>
                          <FontAwesome
                            name="trash-o"
                            size={18}
                            color={'#9b9b9b'}
                          />
                        </Pressable>

                        <View style={{flexDirection: 'row'}}>
                          <Pressable
                            onPress={() => {
                              _dataCart[index].product_data[i].quantity =
                                _dataCart[index].product_data[i].quantity === 1
                                  ? _dataCart[index].product_data[i].quantity
                                  : _dataCart[index].product_data[i].quantity -
                                    1;
                              // dispatch(
                              //   MarketplaceActions.getCartDataSuccess(
                              //     _dataCart,
                              //   ),
                              // );
                              const data = {
                                item: {
                                  product: {
                                    id:
                                      _dataCart[index].product_data[i]
                                        .product_id,
                                    code: _dataCart[index].product_data[i].code,
                                  },
                                  quantity:
                                    _dataCart[index].product_data[i].quantity,
                                },
                                func: {reloadData: handleRefresh},
                              };

                              dispatch(
                                MarketplaceActions.updateCartDataRequest(data),
                              );
                              setdataCart(_dataCart);
                            }}>
                            <IconAntDesign
                              name="minussquareo"
                              size={15}
                              color={
                                _dataCart[index].product_data[i].quantity === 1
                                  ? '#9b9b9b'
                                  : '#1a69d5'
                              }
                            />
                          </Pressable>
                          <View
                            style={{
                              width: Dimensions.get('screen').width * 0.08,
                              borderBottomWidth: 0.5,
                            }}>
                            <Text style={styles.txtproductQuantity}>
                              {v.quantity}
                            </Text>
                          </View>
                          <Pressable
                            onPress={() => {
                              _dataCart[index].product_data[i].quantity =
                                _dataCart[index].product_data[i].quantity + 1;
                              // dispatch(
                              //   MarketplaceActions.getCartDataSuccess(
                              //     _dataCart,
                              //   ),
                              // );
                              const data = {
                                item: {
                                  product: {
                                    id:
                                      _dataCart[index].product_data[i]
                                        .product_id,
                                    code: _dataCart[index].product_data[i].code,
                                  },
                                  quantity:
                                    _dataCart[index].product_data[i].quantity,
                                },
                                func: {reloadData: handleRefresh},
                              };

                              dispatch(
                                MarketplaceActions.updateCartDataRequest(data),
                              );
                              setdataCart(_dataCart);
                            }}>
                            <IconAntDesign
                              name="plussquareo"
                              size={15}
                              color={'#1a69d5'}
                            />
                          </Pressable>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </View>
    );
  };
  const HeaderText = (item) => {
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </Pressable>
      </View>
    );
  };

  useEffect(() => {
    let totalHarga = 0;
    let total = 0;
    totalHarga = dataCart.map((v) => {
      return v.product_data.filter((value) => value.isSelected === true);
    });
    totalHarga = totalHarga.flatMap((v) => v);
    total = totalHarga.map((v) => v.price * v.quantity);
    total = total.reduce((a, b) => a + b, 0);
    setTotalCheckout(total);
  }, [dataCart]);
  useEffect(() => {
    if (cartData !== null) {
      const dataProduct = cartData.map((v, i) => {
        return {
          cart_id: v.id,
          code: v.product_variant_code,
          quantity: v.quantity,
          product_id: v.product_details.id,
          name: v.product_details.name,
          images: v.product_details.images,
          supplier_id: v.product_details.supplier_id,
          supplier_name: v.product_details.supplier_name,
          supplier_city: v.product_details.supplier_city,
          supplier_id: v.product_details.supplier_id,
          variant: v.product_details.variant,
          price: v.product_details.price,
          isSelected: true,
        };
      });

      const dataChain = _.chain(dataProduct)
        // Group the elements of Array based on `color` property
        .groupBy('supplier_name')
        // `key` is group's name (color), `value` is the array of objects
        .map((value, key) => ({supplier_name: key, product_data: value}))
        .value();

      const dataFinal = dataChain.map((v, i) => {
        const producDataManipulate = v.product_data.map((value, i) => {
          return {
            cart_id: value.cart_id,
            code: value.code,
            quantity: value.quantity,
            product_id: value.product_id,
            name: value.name,
            images: value.images,
            is_from_supplier: value.is_from_supplier,
            supplier_id: value.supplier_id,
            supplier_name: value.supplier_name,
            supplier_city: value.supplier_city,
            variant: value.variant,
            price: value.price,
            isSelected: value.isSelected,
            markup_price: value.price,
          };
        });

        return {
          supplier_name: v.supplier_name,
          supplier_isSelected: true,
          supplier_city: producDataManipulate[0].supplier_city,
          supplier_id: producDataManipulate[0].supplier_id,
          product_data: producDataManipulate,
        };
      });
      let dataFilter = [];
      dataFilter = dataFinal.filter((v) => v.supplier_isSelected === true);
      if (dataFilter.length === dataFinal.length) {
        setChooseAll(true);
      } else {
        setChooseAll(false);
      }
      console.log('datafinal', dataFinal);
      setdataCart(dataFinal);
    }
  }, [cartData]);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    }
    dispatch(MarketplaceActions.getCartDataRequest());
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };

  const toSupplierScreen = (item) => {
    navigation.navigate('MarketplaceSupplierScreen', {
      params: item,
    });
  };

  let {height, width} = Dimensions.get('window');

  return (
    <Container style={styles.container}>
      {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </Pressable>
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={styles.fixedHeaderText}>Keranjang</Text>
          </View>
        </View>
      {/* H E A D E R - - - END */}
      <ScrollView
        style={styles.scrollArea}>
        {/* B O D Y */}
          <SafeAreaView style={styles.body}>
            <View style={styles.bodyInner}>
                {/* C O N T E N T */}
                  <View style={styles.content}>
                    <View style={[styles.contentInner, {marginTop: 0}]}>

                      {/* Section 1 - Choose all */}
                      <View style={{flexDirection: 'row', paddingHorizontal: 12, marginBottom: 24}}>
                        <Pressable onPress = { () => this.selectHandler(i, item.checked) }>
                          <ButtonIcon 
                            icon="checkbox-blank-outline"
                            // icon={ item.checked == 1 ? "checkbox-marked-circle-outline" : "checkbox-blank-outline" }
                            text="Pilih Semua"
                            color={Colors.textGrey}
                            size={Fonts.size_26}
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                            }}
                            textStyle={{
                              color: Colors.textGreyDark,
                              paddingLeft: 6,
                            }}
                          />
                        </Pressable>
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 2 - Store & Product */}
                      <View style={styles.cartSection}>
                        <View style={styles.cartSectionHeader}>
                          <Pressable>
                            <ButtonIcon 
                              icon="checkbox-blank-outline"
                              color={Colors.textGrey}
                              size={Fonts.size_26}
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                              }}
                            />
                          </Pressable>
                          <View style={[SupStyle.headerInfo, {marginLeft: 3}]}>
                            <Pressable onPress={() => toSupplierScreen()} style={{}}>
                              <Text numberOfLines={1} style={[SupStyle.headerName, {marginTop: 0, paddingTop: 0}]}>Toko Segala Ada</Text>
                            </Pressable>

                            <View style={[SupStyle.headertStoreLocation]}>
                              <Image 
                                source={require('../../../../assets/image/location_pin.png')} 
                                style={[SupStyle.headertStoreLocationIcon]} />
                              <Text numberOfLines={1} style={[SupStyle.headerStoreLocationText]}>Menteng, Jakarta Pusat</Text>
                            </View>
                          </View>
                        </View>
                        <View style={styles.cartSectionBody}>
                          <FlatList
                            style={styles.sectionInfo}
                            data={[
                              {
                                ProductName         : 'Tesla model 5, made indonesia super speed echo friendly', 
                                ProductImage        : require('../../../../assets/image/honda-jazz-hero.png'),
                                ProductPrice        : '2.999.000.000', 
                                Quantity            : '1',
                                Varian1             : 'Green',
                                Varian2             : 'M',
                                key                 : '1'
                              },
                              {
                                ProductName         : 'Tesla model 6, made indonesia super speed echo friendly', 
                                ProductImage        : require('../../../../assets/image/honda-jazz-hero.png'),
                                ProductPrice        : '2.999.000.000', 
                                Quantity            : '2',
                                Varian1             : 'Blue',
                                Varian2             : 'S',
                                key                 : '2'
                              },
                              {
                                ProductName         : 'Tesla model 6, made indonesia super speed echo friendly', 
                                ProductImage        : require('../../../../assets/image/honda-jazz-hero.png'),
                                ProductPrice        : '2.999.000.000', 
                                Quantity            : '3',
                                Varian1             : 'Blue',
                                Varian2             : 'S',
                                key                 : '3'
                              },
                              {
                                ProductName         : 'Tesla model 6, made indonesia super speed echo friendly', 
                                ProductImage        : require('../../../../assets/image/honda-jazz-hero.png'),
                                ProductPrice        : '2.999.000.000', 
                                Quantity            : '4',
                                Varian1             : 'Blue',
                                Varian2             : 'S',
                                key                 : '4'
                              },
                            ]}
                            renderItem={({item}) => 
                            <ListItem key={item.key} style={[styles.cartRow]}>
                              <View style={styles.cartItem}>
                                <View style={styles.cartItemInfo}>
                                  <Pressable style={{marginTop: 80/2-28/2}}>
                                    <ButtonIcon 
                                      icon="checkbox-blank-outline"
                                      color={Colors.textGrey}
                                      size={Fonts.size_26}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>

                                  <ImageBackground source={item.ProductImage} style={[styles.cartItemInfoImage, {marginLeft: 8}]}>
                                  </ImageBackground>
                                  
                                  <View style={styles.cartItemInfoDetail}>
                                  
                                    <Text style={[styles.cartItemInfoDetailText]} numberOfLines={1}>{item.key}{item.ProductName}</Text>
                                    <Text style={[styles.cartItemInfoDetailText, {fontSize: Fonts.size_14+1, marginVertical: 5, color: Colors.textGrey}]}>{item.Varian1}, {item.Varian2}</Text>
                                    <Text style={[styles.cartItemInfoDetailText, {fontFamily: 'NotoSans-Bold',}]} numberOfLines={1}>Rp. {item.ProductPrice}</Text>
                                    
                                    <View style={{flexDirection: 'row', flex: 1, alignItems: 'center', marginTop: 12}}>
                                      <Pressable style={{marginLeft: 'auto', marginRight: 24}}>
                                        <ButtonIcon 
                                          icon="trash-can-outline"
                                          color={Colors.textGreyLight}
                                          size={Fonts.size_28+2}
                                          style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                          }}
                                        />
                                      </Pressable>
                                      <Pressable>
                                        <ButtonIcon 
                                          icon="minus"
                                          color={Colors.blueBold}
                                          size={Fonts.size_22}
                                          style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            height: Fonts.size_26,
                                            width: Fonts.size_26,
                                            borderRadius: 2,
                                            borderColor: Colors.blueBold,
                                            borderWidth: 2,
                                            borderStyle: 'solid',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                          }}
                                        />
                                      </Pressable>
                                      <View style={[styles.PlaceholderWrapper, {justifyContent: 'center', alignItems: 'center'}]}>
                                        {/* <FadeInView><SkeletonPlaceholder><View style={[{width: 24, height: 24}]} /></SkeletonPlaceholder></FadeInView> */}
                                        <Text style={[styles.cartItemInfoDetailText, {
                                          // backgroundColor: '#ddd',
                                          paddingHorizontal: 0,
                                          paddingTop: 0,
                                          paddingBottom: 0,
                                          width: 36,
                                          marginHorizontal: 6,
                                          textAlign: 'center',
                                          borderBottomColor: Colors.gray,
                                          borderBottomWidth: .4,
                                          borderStyle: 'solid',
                                        }]}>
                                          {item.Quantity}
                                        </Text>
                                      </View>
                                      <Pressable>
                                        <ButtonIcon 
                                          icon="plus"
                                          color={Colors.blueBold}
                                          size={Fonts.size_22}
                                          style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            height: Fonts.size_26,
                                            width: Fonts.size_26,
                                            borderRadius: 2,
                                            borderColor: Colors.blueBold,
                                            borderWidth: 2,
                                            borderStyle: 'solid',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                          }}
                                        />
                                      </Pressable>
                                    </View>
                                  </View>
                                </View>
                              </View>
                              <View style={[styles.cartItemDivider]}></View>
                            </ListItem>}
                          />
                        </View>
                      </View>

                    </View>
                  </View>
                {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
        {/* B O D Y - - - END */}
      </ScrollView>
      {/* F O O T E R */}
        <View style={[styles.fixedFooter, {backgroundColor: Colors.white, elevation: 1,}]}>
          <View style={{flex: 1}}>
            <Text style={[styles.cartItemInfoDetailText, {marginBottom: 0, fontSize: Fonts.size_16}]}>Total</Text>

            <View style={[styles.PlaceholderWrapper, {marginBottom: 6}]}>
              {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '90%', height: 24}]} /></SkeletonPlaceholder></FadeInView> */}
              <Text style={[styles.cartItemInfoDetailText, {
                fontFamily: 'NotoSans-Bold', 
                fontSize: Fonts.size_22, 
                color: Colors.blueBold}]} 
                numberOfLines={1}
              >
                Rp. 6.000.000.000
              </Text>
            </View>
          </View>
          <Button 
            onPress={() => _handleMarketplaceAddress()}
            text="Checkout Pesanan"
            style={{
              marginLeft: 'auto',
              backgroundColor: Colors.blueBold,
              borderWidth: 0,
              // paddingHorizontal: 12,
            }}
            textStyle={{
              color: Colors.white,
              letterSpacing: 1,
            }}
          />
        </View>
      {/* F O O T E R - - - END */}
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    cartData: state.marketplace.getCartData.data
      ? state.marketplace.getCartData.data.data
        ? state.marketplace.getCartData.data.data.cart
        : null
      : null,
    isSuccessCartUpdated: state.marketplace.updateCartData.success,
    fetchingcartData: state.marketplace.getCartData.fetching,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
