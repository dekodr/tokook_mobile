import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  txtproductName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
  },
  txtemptyCart: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 14,
    letterSpacing: 0.9,
    color: '#9b9b9b',
  },
  txtPilihSemua: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#43455c',
  },
  txtproductQuantity: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
    textAlign: 'center',
  },
  txtvariantName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 10,
    letterSpacing: 0.25,
    color: '#818181',
    marginTop: 5,
    marginBottom: 5,
  },
  txtproductPrice: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#4a4a4a',
  },
  txtsupplierName: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    letterSpacing: 1,
    color: '#4a4a4a',
  },
  txtsupplierCity: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 10,
    letterSpacing: 0.8,
    color: '#4a4a4a',
    textAlign: 'left',
  },
  cartSection: {
    // backgroundColor: '#777',
    paddingHorizontal: 12,
  },
  cartRow: {
    marginLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    borderBottomColor: 'rgba(0,0,0,0)',
    flexDirection: 'column',
  },
  cartSectionHeader: {
    // backgroundColor: '#999',
    flexDirection: 'row',
    // alignItems: 'center',
    marginBottom: 24,
  },
  cartSectionBody: {},
  cartItemDivider: {
    marginVertical: 24,
    flex: 1,
    width: '100%',
    height: .4,
    backgroundColor: Colors.borderGrey,
  },
  cartItem: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: '#ddd',
  },
  cartItemInfo: {
    flexDirection: 'row',
    flex: 1,
    // marginLeft: 12,
  },
  cartItemInfoImage: {
    borderRadius: 6,
    aspectRatio: 1,
    width: 80,
    overflow: 'hidden',
    resizeMode: 'cover',
    // marginLeft: 12,
  },
  cartItemInfoDetail: {
    marginLeft: 12,
    flex: 1,
    paddingVertical: 0,
    // backgroundColor: '#eee',
  },
  cartItemInfoDetailText: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16+1,
    // letterSpacing: 1,
    // marginBottom: 4,
  },

  container: {
    flex: 1,
    backgroundColor: '#EDECED',
  },
  tabbar: {
    backgroundColor: '#205493',
  },
  tab: {
    width: 110,
    height: 80,
  },
  icon: {
    backgroundColor: 'transparent',
    color: '#ffffff',
  },
  indicator: {
    width: 110,
    height: 80,
    backgroundColor: '#F6F7F8',
  },
  label: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'NotoSans-Regular',
    paddingTop: 5,
    color: '#F6F7F8',
    backgroundColor: 'transparent',
  },
  PlaceholderWrapper: {
    position: 'relative',
    overflow: 'hidden',
  },
  PlaceholderBox: {
    position: 'absolute',
    zIndex: -1,
    // zIndex: 2,
    // backgroundColor: '#ddd',
  },

  PagePlaceholderWrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  PagePlaceholderInner: {
    width: '100%',
    height: '100%',
  },

  HeaderShadow: {
    // backgroundColor: '#ddd',
    flexDirection: 'row',
    height: 24,
    marginBottom: 16,
  },
  HeaderShadowText: {
    width: '50%',
  },
  HeaderShadowLink: {
    marginLeft: 'auto',
    width: '25%',
  },

  CardRowShadow: {
    flexDirection: 'row',
    // height: 85,
  },
  CardTutorialShadow: {
    marginRight: 12,
    width: '100%',
    height: 85,
    borderRadius: 6,
    overflow: 'hidden',
  },
  CardProductShadow: {
    marginRight: 12,
    width: '100%',
    height: 285,
    borderRadius: 6,
    overflow: 'hidden',
  },

  // Row & Col
  fdRow: {
    flexDirection: 'row',
  },
  fdCol: {
    flexDirection: 'column',
  },
  mlAuto: {
    marginLeft: 'auto',
  },

  // Header Text
  HeaderText: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_18,
    letterSpacing: 1,
  },
  HeaderAllLink: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.blueBold,
    fontSize: Fonts.size_16,
    letterSpacing: 0.5,
  },

  container: {
    // backgroundColor: "#fff",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Main Area Scroll View
  scrollArea: {
    flex: 1,
  },

  body: {
    // paddingTop: StatusBar.currentHeight,
    width: width,
    position: 'relative',
    zIndex: 1,
    // padding: 20,
  },

  bodyInner: {
    marginTop: 24,
    position: 'relative',
    zIndex: 3,
    // paddingHorizontal: 20,
  },

  // Header section
  fixedHeader: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'relative',
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.4,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.05,
    shadowRadius: 2,
  },
  fixedHeaderPageIndicator: {
    height: 35,
    justifyContent: 'center',
  },
  fixedHeaderText: {
    color: Colors.white,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
    flex: 1,
    paddingVertical: 0,
    borderRadius: 2,
  },
  searchBarIcon: {
    height: 35,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  searchBarInput: {
    // backgroundColor: '#ddd',
    marginLeft: -8,
    flex: 1,
    // height: 35,
    paddingVertical: 0,
    // backgroundColor: 'transparent',
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    // letterSpacing: 1,
    // justifyContent: 'center',
  },
  searchBarInputPlaceholder: {
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  header: {
    width: '100%',
    height: 130,
    position: 'relative',
    backgroundColor: '#fff',
    // padding: 20,
  },
  headerTop: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    // position: 'absolute',
    // right: 20,
    // top: 0,
    padding: 20,
    // backgroundColor: '#fff',
  },
  icon: {
    width: 20,
    height: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  headerBottom: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    // left: 30,
    paddingHorizontal: 30,
    bottom: 0,
    paddingVertical: 20,
  },

  cartButtonWrapper: {
    position: 'relative',
    marginLeft: 6,
    // backgroundColor: '#ddd',
  },
  cartIndicator: {
    position: 'absolute',
    backgroundColor: Colors.blueBold,
    width: 18,
    height: 18,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    right: 0,
  },
  cartIndicatorInner: {
    height: 14,
    width: 14,
    borderRadius: 14,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIndicatorInnerText: {
    color: Colors.blueBold,
    fontSize: Fonts.size_14,
    fontFamily: 'NotoSans-Bold',
  },
  // Header Section - - - END

  // Footer Section
  fixedFooter: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 100,
    borderTopWidth: 4,
    borderTopColor: Colors.grayBlackWhite,
  },
  // Footer Section - - - END

  // Content Section
  content: {
    backgroundColor: '#fff',
    marginTop: 0,
    paddingHorizontal: 0,
    // padding: 30,
    paddingBottom: 0,

    // borderTopRightRadius: 25,
    // borderTopLeftRadius: 25,
    position: 'relative',
    zIndex: 3,
  },
  contentOutter: {
    width: '100%',
    paddingVertical: 60,
    backgroundColor: '#555',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'relative',
  },
  contentInner: {
    width: '100%',
    // paddingHorizontal: 30,
    // padding: 20,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 75,
  },
  cart: {
    paddingLeft: 10,
  },

  divider: {
    width: '100%',
    height: 5,
    backgroundColor: Colors.grayBlackWhite,
  },
  viewParentList: {
    borderRadius: 10,
    borderWidth: 0.3,
    margin: 3,
    shadowColor: '#4a4a4a',
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },
});

export default styles;
