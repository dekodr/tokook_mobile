import {Dimensions, StyleSheet} from 'react-native';
import {Colors} from '../../../../Themes';

const Page = StyleSheet.create({

  cartSection: {
    // backgroundColor: '#bbb',
    paddingHorizontal: 12,
    paddingVertical: 24,
  },
  cartSectionHeader: {
    flexDirection: 'row',
    marginBottom: 24,
  },
  cartSectionBody: {
    
  },
  cartItemDivider: {
    marginVertical: 24,
    flex: 1,
    height: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  cartItem: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: '#ddd',
  },
  cartItemInfo: {
    flexDirection: 'row',
    flex: 1,
    // marginLeft: 12,
  },
  cartItemInfoImage: {
    borderRadius: 6, 
    aspectRatio: 1, 
    width: 80,
    overflow: 'hidden',
    resizeMode: "cover",
    marginLeft: 12,
  },
  cartItemInfoDetail: {
    marginLeft: 12,
    flex: 1,
    paddingVertical: 0,
    // backgroundColor: '#eee',
  },
  cartItemInfoDetailText: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: 18,
    // letterSpacing: 1,
    marginBottom: 4,
  },

  container: {
    flex: 1,
    backgroundColor: '#EDECED'
  },
  tabbar: {
    backgroundColor: '#205493'
  },
  tab: {
    width: 110,
    height: 80
  },
  icon: {
    backgroundColor: 'transparent',
    color: '#ffffff',
  },
  indicator: {
    width: 110,
    height: 80,
    backgroundColor: '#F6F7F8'
  },
  label: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'NotoSans-Regular',
    paddingTop: 5,
    color: '#F6F7F8',
    backgroundColor: 'transparent'
  },
});

export default Page;