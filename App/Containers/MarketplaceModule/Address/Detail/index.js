import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  SafeAreaView,
  ImageBackground,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
import VerticalTabBar from '../../../../Components/SupplierTab';
import tc from '../../../TokoModule/Intro/styles';
import styles from '../../Intro/styles';
import page from './styles';
import SupStyle from '../../Supplier/Intro/styles';
import {Images, Colors} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';

import ButtonIcon from '../../../../Components/ButtonIcon';
import Button from '../../../../Components/Button';

// import TabViewVertical from 'react-native-tab-view';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { padEnd } from 'lodash-es';

function Address(props) {
  const {
	dispatch,
	navigation,
	getToko,
	bottomSheetRef,
	tokoId,
	userData,
	tokoData,
	setStep,
	step,
	tokoAddressData,
  } = props;
  const isVerified =
	userData.data.is_phone_verified !== null &&
	tokoData &&
	tokoAddressData !== null
	  ? tokoAddressData.listAddress.length
		? tokoAddressData.listAddress.length !== 0
		: false
	  : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
	? tokoCategory.length
	  ? tokoCategory.filter(data => data.id === selectedCategory.id)
	  : []
	: [];
  const isTokoAvailable = props.getToko
	? props.getToko.listToko
	  ? props.getToko.listToko.length === 0
		? false
		: true
	  : true
	: true;
  const isCurrentIdUndefined = tokoCategory
	? tokoCategory.length
	  ? getId.id === undefined
		? true
		: false
	  : false
	: false;
  const wait = timeout => {
	return new Promise(resolve => {
	  setTimeout(resolve, timeout);
	});
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  const handleGoToAllPage = () => {
    
  }

  const HeaderText = (item) => {
    return (
      <View 
        style={{paddingHorizontal: 12, flexDirection: 'row', alignItems: 'center'}}>
        <Text
          style={styles.HeaderText}>
          {item.text}
        </Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
            <Text
              style={styles.HeaderAllLink}>
              lihat semua
            </Text>
        </Pressable>
      </View>
    );
  };

  useEffect(() => {
	BackHandler.addEventListener('hardwareBackPress', handleGoBack);

	return () =>
	  BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
	setTokoCategory(
	  props.categoryData
		? props.categoryData.listCategory
		  ? props.categoryData.listCategory.length != 0
			? props.categoryData.listCategory
			: []
		  : []
		: [],
	);
  }, [props.categoryData]);

  useEffect(() => {
	if (newCategory) {
	  setIsCategoryEmpty(false);
	}
  }, [newCategory]);

  useEffect(() => {
	if (props.tokoId) {
	  props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
	}
	// func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };

  const toSupplierScreen = item => {
    navigation.navigate('MarketplaceSupplierScreen', {
      params: item,
    });
  };

  return (
    <Container style={styles.container}>
      {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </Pressable>
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={styles.fixedHeaderText}>Keranjang</Text>
          </View>
        </View>
      {/* H E A D E R - - - END */}
      <ScrollView
  		  style={styles.scrollArea}>
        {/* B O D Y */}
          <SafeAreaView style={styles.body}>
            <View style={styles.bodyInner}>
                {/* C O N T E N T */}
                  <View style={styles.content}>
                    <View style={[styles.contentInner, {marginTop: 0}]}>

                      {/* Section 1 - Choose all */}
                      <View style={{flexDirection: 'row', paddingHorizontal: 12, marginBottom: 24}}>
                        <Pressable onPress = { () => this.selectHandler(i, item.checked) }>
                          <ButtonIcon 
                            icon="checkbox-blank-outline"
                            // icon={ item.checked == 1 ? "checkbox-marked-circle-outline" : "checkbox-blank-outline" }
                            text="Pilih semua"
                            color={Colors.blueBold}
                            size={28}
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                            }}
                            textStyle={{
                              color: Colors.blueBold,
                              paddingLeft: 12,
                            }}
                          />
                        </Pressable>
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 2 - Store & Product */}
                      <View style={page.cartSection}>
                        <View style={page.cartSectionHeader}>
                          <Pressable>
                            <ButtonIcon 
                              icon="checkbox-blank-outline"
                              color={Colors.blueBold}
                              size={28}
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                              }}
                            />
                          </Pressable>
                          <View style={[SupStyle.headerInfo, {marginLeft: 8}]}>
                            <Pressable onPress={() => toSupplierScreen()}>
                              <Text numberOfLines={1} style={[SupStyle.headerName]}>Toko Segala Ada</Text>
                            </Pressable>
                            <View style={[SupStyle.headertStoreLocation]}>
                              <Image 
                                source={require('../../../../assets/image/location_pin.png')} 
                                style={[SupStyle.headertStoreLocationIcon]} />
                              <Text numberOfLines={1} style={[SupStyle.headerStoreLocationText]}>Menteng, Jakarta Pusat</Text>
                            </View>
                          </View>
                        </View>
                        <View style={page.cartSectionBody}>
                          <View style={page.cartItem}>
                            <View style={page.cartItemInfo}>
                              <Pressable style={{marginTop: 80/2-28/2}}>
                                <ButtonIcon 
                                  icon="checkbox-blank-outline"
                                  color={Colors.blueBold}
                                  size={28}
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                  }}
                                />
                              </Pressable>
                              <ImageBackground source={require('../../../../assets/image/honda-jazz-hero.png')} style={[page.cartItemInfoImage]}>
                              </ImageBackground>
                              <View style={page.cartItemInfoDetail}>
                                <Text style={page.cartItemInfoDetailText} numberOfLines={1}>Tesla model 4, made indonesia super speed echo friendly</Text>
                                <Text style={[page.cartItemInfoDetailText, {fontSize: 16}]}>Blue, S</Text>
                                <Text style={[page.cartItemInfoDetailText, {fontFamily: 'NotoSans-Bold',}]} numberOfLines={1}>Rp. 3.000.000.000</Text>
                                <View style={{flexDirection: 'row', flex: 1, alignItems: 'center', marginTop: 12}}>
                                  <Pressable style={{marginLeft: 'auto', marginRight: 24}}>
                                    <ButtonIcon 
                                      icon="trash-can"
                                      color={Colors.textGrey}
                                      size={32}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                  <Pressable>
                                    <ButtonIcon 
                                      icon="minus"
                                      color={Colors.blueBold}
                                      size={24}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: 32,
                                        width: 32,
                                        borderRadius: 2,
                                        borderColor: Colors.blueBold,
                                        borderWidth: 2,
                                        borderStyle: 'solid',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                  <Text style={[page.cartItemInfoDetailText, {
                                    paddingHorizontal: 12,
                                    marginHorizontal: 8,
                                    paddingTop: 8,
                                    paddingBottom: 0,
                                    borderBottomColor: Colors.gray,
                                    borderBottomWidth: 1,
                                    borderStyle: 'solid',
                                  }]}>
                                    2
                                  </Text>
                                  <Pressable>
                                    <ButtonIcon 
                                      icon="plus"
                                      color={Colors.blueBold}
                                      size={24}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: 32,
                                        width: 32,
                                        borderRadius: 2,
                                        borderColor: Colors.blueBold,
                                        borderWidth: 2,
                                        borderStyle: 'solid',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                </View>
                              </View>
                            </View>
                          </View>
                          <View style={page.cartItemDivider}></View>
                          <View style={page.cartItem}>
                            <View style={page.cartItemInfo}>
                              <Pressable style={{marginTop: 80/2-28/2}}>
                                <ButtonIcon 
                                  icon="checkbox-blank-outline"
                                  color={Colors.blueBold}
                                  size={28}
                                  style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                  }}
                                />
                              </Pressable>
                              <ImageBackground source={require('../../../../assets/image/honda-jazz-hero.png')} style={[page.cartItemInfoImage]}>
                              </ImageBackground>
                              <View style={page.cartItemInfoDetail}>
                                <Text style={page.cartItemInfoDetailText} numberOfLines={1}>Tesla model 4, made indonesia super speed echo friendly</Text>
                                <Text style={[page.cartItemInfoDetailText, {fontSize: 16}]}>Blue, S</Text>
                                <Text style={[page.cartItemInfoDetailText, {fontFamily: 'NotoSans-Bold',}]} numberOfLines={1}>Rp. 3.000.000.000</Text>
                                <View style={{flexDirection: 'row', flex: 1, alignItems: 'center', marginTop: 12}}>
                                  <Pressable style={{marginLeft: 'auto', marginRight: 24}}>
                                    <ButtonIcon 
                                      icon="trash-can"
                                      color={Colors.textGrey}
                                      size={32}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                  <Pressable>
                                    <ButtonIcon 
                                      icon="minus"
                                      color={Colors.blueBold}
                                      size={24}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: 32,
                                        width: 32,
                                        borderRadius: 2,
                                        borderColor: Colors.blueBold,
                                        borderWidth: 2,
                                        borderStyle: 'solid',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                  <Text style={[page.cartItemInfoDetailText, {
                                    paddingHorizontal: 12,
                                    marginHorizontal: 8,
                                    paddingTop: 8,
                                    paddingBottom: 0,
                                    borderBottomColor: Colors.gray,
                                    borderBottomWidth: 1,
                                    borderStyle: 'solid',
                                  }]}>
                                    2
                                  </Text>
                                  <Pressable>
                                    <ButtonIcon 
                                      icon="plus"
                                      color={Colors.blueBold}
                                      size={24}
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        height: 32,
                                        width: 32,
                                        borderRadius: 2,
                                        borderColor: Colors.blueBold,
                                        borderWidth: 2,
                                        borderStyle: 'solid',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                      }}
                                    />
                                  </Pressable>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>

                    </View>
                  </View>
                {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
        {/* B O D Y - - - END */}
      </ScrollView>
      {/* F O O T E R */}
        <View style={[styles.fixedFooter, {backgroundColor: Colors.white, elevation: 1,}]}>
          <View style={{flex: 1}}>
            <Text style={[page.cartItemInfoDetailText, {marginBottom: 0}]}>Total</Text>
            <Text style={[page.cartItemInfoDetailText, {
              fontFamily: 'NotoSans-Bold', 
              fontSize: 20, 
              color: Colors.blueBold}]} 
              numberOfLines={1}
            >
              Rp. 6.000.000.000
            </Text>
          </View>
          <Pressable style={{marginLeft: 'auto'}}>
            <Button 
              theme="primary"
              text="Checkout pesanan"
              size="medium"
              option
              style={{
                backgroundColor: Colors.blueBold,
                borderWidth: 0,
                paddingVertical: 18,
              }}
              textStyle={{
                color: Colors.white,
              }}
            />
          </Pressable>
        </View>
      {/* F O O T E R - - - END */}
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
	getToko: state.toko.tokoData.data,

	tokoId: state.toko.tokoData.data
	  ? state.toko.tokoData.data.listToko
		? state.toko.tokoData.data.listToko.length
		  ? state.toko.tokoData.data.listToko[0].id
		  : null
		: null
	  : null,
	categoryData: state.toko.categoryTokoData.data,
	userData: state.auth.userData,
	tokoData: state.toko.tokoData.data
	  ? state.toko.tokoData.data.listToko
		? state.toko.tokoData.data.listToko.length
		  ? state.toko.tokoData.data.listToko[0]
		  : null
		: null
	  : null,
	tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Address);