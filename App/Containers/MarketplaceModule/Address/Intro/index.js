import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Pressable,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  ImageBackground,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
import tc from '../../../TokoModule/Intro/styles';
import styles from '../../Intro/styles';
import page from './styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
import MarketplaceActions from './../../../../Redux/MarketPlaceRedux';
import Button from '../../../../Components/Button';
import {DeliveryOption} from '../../../../Components/Option';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

// import TabViewVertical from 'react-native-tab-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const formValidationInitial = {
  isSenderNameEmpty: false,
  isSenderPhoneEmpty: false,
  isSenderPhoneInvalid: false,
  isBuyerNameEmpty: false,
  isBuyerAddressEmpty: false,
  isBuyerPhoneEmpty: false,
  isBuyerPhoneInvalid: false,

  isBuyerEmailEmpty: false,
  isBuyerProvinceEmpty: false,
  isBuyerCityEmpty: false,
  isBuyerDistrictEmpty: false,
  isBuyerPostalCodeEmpty: false,
  isBuyerAddress1Empty: false,
  isBuyerAddress2Empty: false,
  isBuyerEmailInvalid: false,
};

function Address(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
  } = props;
  const formInitialitation = {
    senderName: tokoData != null ? tokoData.name : '',
    senderPhone: tokoData != null ? tokoData.whatsapp : '',
    buyerName: '',
    buyerAddress: '',
    buyerPhone: '',
    buyerEmail: 'aaa@gmail.com',
    buyerProvince: {name: 'Provinsi'},
    buyerCity: {name: 'Kota', type: ''},
    buyerDistrict: {name: 'Kecamatan'},
    buyerPostalCode: {name: 'Kode Pos'},
    buyerAddress1: '',
    buyerAddress2: '',
  };
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  console.log('toko ko', tokoData);
  const scrollViewRef = useRef();
  const [formValidation, setFormValidation] = useState(formValidationInitial);
  const [formShipping, setFormShipping] = useState(formInitialitation);
  const refRBSheet = useRef();
  const refRBSheetCity = useRef();
  const refRBSheetDistrict = useRef();
  const refRBSheetPostCode = useRef();
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);

  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter(data => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };

  const searchingPostalCode = searchText => {
    if (
      formShipping.buyerProvince.name != '' &&
      formShipping.buyerCity.name != '' &&
      formShipping.buyerDistrict.name != ''
    ) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        // setAddedAddressFormValidation({
        //   ...addedAddressFormValidation,
        //   isPostalCodeEmpty: false,
        // });
        setPostalCodeValue(searchText);

        let filteredData = postalCodeData.postalCode.filter(function(item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setPostalCodeValue(searchText);
    }
  };
  const handleAddressPicker = item => {
    if (item.postal_code !== undefined) {
      setFormValidation({...formValidation, isBuyerPostalCodeEmpty: false});
      setFormShipping({
        ...formShipping,
        buyerPostalCode: {...item, name: item.postal_code + ' - ' + item.urban},
      });

      refRBSheetPostCode.current.close();
    } else if (item.city_id !== undefined) {
      setFormValidation({...formValidation, isBuyerDistrictEmpty: false});

      setFormShipping({
        ...formShipping,
        buyerDistrict: item,
        buyerPostalCode: {...item, name: 'Kode Pos'},
      });
      dispatch(
        TokoActions.getPostalCodeRequest({
          cityName: formShipping.buyerCity.name,
          districtName: item.name,
        }),
      );
      dispatch(MarketplaceActions.setShippingDestinationData(item.name));
      refRBSheetDistrict.current.close();
    } else if (item.province_id !== undefined) {
      setFormValidation({...formValidation, isBuyerCityEmpty: false});

      setFormShipping({
        ...formShipping,
        buyerCity: item,
        buyerDistrict: {...item, name: 'Kecamatan'},
        buyerPostalCode: {...item, name: 'Kode Pos'},
      });
      dispatch(TokoActions.getDistrictRequest(item));

      refRBSheetCity.current.close();
    } else {
      setFormValidation({...formValidation, isBuyerProvinceEmpty: false});

      refRBSheet.current.close();
      dispatch(TokoActions.getCityRequest(item));
      setFormShipping({
        ...formShipping,
        buyerProvince: item,
        buyerCity: {...item, name: 'Kota', type: ''},
        buyerDistrict: {...item, name: 'Kecamatan'},
        buyerPostalCode: {...item, name: 'Kode Pos'},
      });
    }
  };
  const _renderItem = ({index, item}) => {
    return (
      <TouchableOpacity
        onPress={() => handleAddressPicker(item)}
        style={{
          borderBottomColor: Colors.gray,
          borderBottomWidth: 0.3,
          padding: 20,
        }}>
        <Text>
          {item.postal_code !== undefined
            ? item.postal_code + ' - ' + item.urban
            : item.province_id
            ? `${item.type} ${item.name}`
            : `${item.name}`}
        </Text>
      </TouchableOpacity>
    );
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    }
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };

  const _handleMarketplaceCheckout = item => {
    const validemail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const validPhoneNumber = /^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/;
    if (formShipping.senderName === '') {
      scrollViewRef.current.scrollTo(0);
      setFormValidation({...formValidation, isSenderNameEmpty: true});
    } else if (formShipping.senderPhone === '') {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isSenderPhoneEmpty: true});
    } else if (!validPhoneNumber.test(formShipping.senderPhone)) {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isSenderPhoneInvalid: true});
    } else if (formShipping.buyerName === '') {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isBuyerNameEmpty: true});
    } else if (formShipping.buyerPhone === '') {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isBuyerPhoneEmpty: true});
    } else if (!validPhoneNumber.test(formShipping.buyerPhone)) {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isBuyerPhoneInvalid: true});
    } else if (formShipping.buyerEmail === '') {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({...formValidation, isBuyerEmailEmpty: true});
    } else if (!validemail.test(formShipping.buyerEmail)) {
      scrollViewRef.current.scrollTo(0);

      setFormValidation({
        ...formValidation,

        isBuyerEmailInvalid: true,
      });
    } else if (formShipping.buyerProvince.name === 'Provinsi') {
      scrollViewRef.current.scrollToEnd({animated: true});
      setFormValidation({...formValidation, isBuyerProvinceEmpty: true});
    } else if (formShipping.buyerCity.name === 'Kota') {
      scrollViewRef.current.scrollToEnd({animated: true});

      setFormValidation({...formValidation, isBuyerCityEmpty: true});
    } else if (formShipping.buyerDistrict.name === 'Kecamatan') {
      scrollViewRef.current.scrollToEnd({animated: true});

      setFormValidation({...formValidation, isBuyerDistrictEmpty: true});
    } else if (formShipping.buyerPostalCode.name === 'Kode Pos') {
      scrollViewRef.current.scrollToEnd({animated: true});

      setFormValidation({...formValidation, isBuyerPostalCodeEmpty: true});
    } else if (formShipping.buyerAddress1 === '') {
      scrollViewRef.current.scrollToEnd({animated: true});

      setFormValidation({...formValidation, isBuyerAddress1Empty: true});
    } else {
      dispatch(MarketplaceActions.addAddressToCheckoutProceed(formShipping));
      dispatch(MarketplaceActions.getAllShippingFeeReset());
      navigation.navigate('MarketplaceCheckoutScreen', {
        params: item,
      });
    }
  };

  let {height, width} = Dimensions.get('window');

  return (
    <Container
      style={[
        styles.PlaceholderWrapper,
        {width: '100%', backgroundColor: '#fff'},
      ]}>
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </Pressable>
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={styles.fixedHeaderText}>Detail Pengiriman</Text>
          </View>
        </View>
        {/* H E A D E R - - - END */}

      {/* B O D Y */}
        <View style={[styles.scrollArea]}>
          <ScrollView ref={scrollViewRef} style={[styles.body, {flex: 1}]}>
            <View style={styles.bodyInner}>
              {/* C O N T E N T */}
              <View style={styles.content}>
                <View style={[styles.contentInner, {marginTop: 0}]}>
                  {/* Section 1 - Sender Information */}
                  <View style={{paddingHorizontal: 12, marginBottom: 24}}>
                    <Text style={page.sectionTitleText}>Kontak Pengirim</Text>
                    {/* Pembeli info */}
                    <TextInput
                      onChangeText={x => {
                        setFormValidation({
                          ...formValidation,
                          isSenderNameEmpty: false,
                        });
                        setFormShipping({...formShipping, senderName: x});
                      }}
                      value={formShipping.senderName}
                      style={[
                        page.input,
                        {
                          borderBottomColor:
                            formShipping.senderName.length > 0
                              ? Colors.mainBlue
                              : formValidation.isSenderNameEmpty
                              ? 'red'
                              : Colors.textGreyLight,
                        },
                      ]}
                      placeholder="Nama Pengirim"
                    />
                    {formValidation.isSenderNameEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nama pengirim harus diisi
                      </Text>
                    )}
                    <TextInput
                      onChangeText={x => {
                        setFormShipping({...formShipping, senderPhone: x});
                        setFormValidation({
                          ...formValidation,
                          isSenderPhoneEmpty: false,
                          isSenderPhoneInvalid: false,
                        });
                      }}
                      keyboardType={'decimal-pad'}
                      value={formShipping.senderPhone}
                      style={[
                        page.input,
                        {
                          borderBottomColor:
                            formShipping.senderPhone.length > 0
                              ? Colors.mainBlue
                              : formValidation.isSenderPhoneEmpty ||
                                formValidation.isSenderPhoneInvalid
                              ? 'red'
                              : Colors.textGreyLight,
                        },
                      ]}
                      placeholder="No. HP Pengirim"
                    />
                    {formValidation.isSenderPhoneEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nomor hp pengirim harus diisi
                      </Text>
                    )}
                    {formValidation.isSenderPhoneInvalid && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nomor hp pengirim tidak valid
                      </Text>
                    )}
                  </View>

                  <View style={styles.divider}></View>

                  {/* Section 2 - Receiver Information */}
                  <View style={{paddingHorizontal: 12, marginVertical: 24}}>
                    <Text style={page.sectionTitleText}>Alamat Pembeli</Text>

                    <TextInput
                      onChangeText={x => {
                        setFormShipping({...formShipping, buyerName: x});
                        setFormValidation({
                          ...formValidation,
                          isBuyerNameEmpty: false,
                        });
                      }}
                      value={formShipping.buyerName}
                      style={[
                        page.input,
                        {
                          borderBottomColor:
                            formShipping.buyerName.length > 0
                              ? Colors.mainBlue
                              : formValidation.isBuyerNameEmpty
                              ? 'red'
                              : Colors.textGreyLight,
                        },
                      ]}
                      placeholder="Nama Pembeli"
                    />
                    {formValidation.isBuyerNameEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nama pembeli harus diisi
                      </Text>
                    )}
                    <TextInput
                      onChangeText={x => {
                        setFormShipping({...formShipping, buyerPhone: x});
                        setFormValidation({
                          ...formValidation,
                          isBuyerPhoneEmpty: false,
                          isBuyerPhoneInvalid: false,
                        });
                      }}
                      keyboardType={'decimal-pad'}
                      value={formShipping.buyerPhone}
                      style={[
                        page.input,
                        {
                          borderBottomColor:
                            formShipping.buyerPhone.length > 0
                              ? Colors.mainBlue
                              : formValidation.isBuyerPhoneEmpty ||
                                formValidation.isBuyerPhoneInvalid
                              ? 'red'
                              : Colors.textGreyLight,
                        },
                      ]}
                      placeholder="No. HP Pembeli"
                    />
                    {formValidation.isBuyerPhoneEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nomor hp pembeli harus diisi
                      </Text>
                    )}
                    {formValidation.isBuyerPhoneInvalid && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Nomor hp pembeli tidak valid
                      </Text>
                    )}
                    {/* <TextInput
                      onChangeText={x => {
                        setFormShipping({...formShipping, buyerEmail: x});
                        setFormValidation({
                          ...formValidation,
                          isBuyerPhoneEmpty: false,
                          isBuyerEmailEmpty: false,
                          isBuyerEmailInvalid: false,
                        });
                      }}
                      keyboardType={'email-address'}
                      value={formShipping.buyerEmail}
                      style={page.input}
                      placeholder="Email Pembeli"
                    /> */}
                    {formValidation.isBuyerEmailEmpty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Email pembeli harus diisi
                      </Text>
                    )}
                    {formValidation.isBuyerEmailInvalid && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Email tidak valid
                      </Text>
                    )}
                    <Pressable
                      onPress={() => refRBSheet.current.open()}
                      style={{}}>
                      <View
                        style={[
                          page.input,
                          {
                            flexDirection: 'row',
                            alignItems: 'center',

                            borderBottomColor:
                              formShipping.buyerProvince.name != 'Provinsi'
                                ? Colors.mainBlue
                                : formValidation.isBuyerProvinceEmpty
                                ? 'red'
                                : Colors.textGreyLight,
                          },
                        ]}>
                        <Text style={page.inputText}>
                          {`${formShipping.buyerProvince.name}`}
                        </Text>
                        <View
                          style={{
                            marginLeft: 4,
                            transform: [{rotate: '180deg'}],
                          }}>
                          <MaterialCommunityIcons
                            onPress={() => refRBSheet.current.open()}
                            name={'triangle'}
                            color={Colors.blueBold}
                            size={Fonts.size_14}
                          />
                        </View>
                      </View>
                      {formValidation.isBuyerProvinceEmpty && (
                        <Text
                          style={[
                            c.Styles.txtInputMerchantErrors,
                            {marginTop: -12},
                          ]}>
                          Alamat provinsi pembeli harus diisi
                        </Text>
                      )}
                    </Pressable>
                    <Pressable
                      onPress={() => {
                        formShipping.buyerProvince.name === 'Provinsi'
                          ? Toast.show('Pilih provinsi terlebih dahulu')
                          : refRBSheetCity.current.open();
                      }}
                      style={{}}>
                      <View
                        style={[
                          page.input,
                          {
                            flexDirection: 'row',
                            alignItems: 'center',

                            borderBottomColor:
                              formShipping.buyerCity.name != 'Kota'
                                ? Colors.mainBlue
                                : formValidation.isBuyerCityEmpty
                                ? 'red'
                                : Colors.textGreyLight,
                          },
                        ]}>
                        <Text style={page.inputText}>
                          {`${formShipping.buyerCity.type} ${formShipping.buyerCity.name}`}
                        </Text>
                        <View
                          style={{
                            marginLeft: 4,
                            transform: [{rotate: '180deg'}],
                          }}>
                          <MaterialCommunityIcons
                            name={'triangle'}
                            color={Colors.blueBold}
                            size={Fonts.size_14}
                          />
                        </View>
                      </View>
                      {formValidation.isBuyerCityEmpty && (
                        <Text
                          style={[
                            c.Styles.txtInputMerchantErrors,
                            {marginTop: -12},
                          ]}>
                          Alamat kota pembeli harus diisi
                        </Text>
                      )}
                    </Pressable>
                    <Pressable
                      onPress={() => {
                        formShipping.buyerCity.name === 'Kota'
                          ? Toast.show('Pilih kota terlebih dahulu')
                          : refRBSheetDistrict.current.open();
                      }}
                      style={{}}>
                      <View
                        style={[
                          page.input,
                          {
                            flexDirection: 'row',
                            alignItems: 'center',
                            borderBottomColor:
                              formShipping.buyerDistrict.name != 'Kecamatan'
                                ? Colors.mainBlue
                                : formValidation.isBuyerDistrictEmpty
                                ? 'red'
                                : Colors.textGreyLight,
                          },
                        ]}>
                        <Text style={page.inputText}>
                          {`${formShipping.buyerDistrict.name}`}
                        </Text>
                        <View
                          style={{
                            marginLeft: 4,
                            transform: [{rotate: '180deg'}],
                          }}>
                          <MaterialCommunityIcons
                            name={'triangle'}
                            color={Colors.blueBold}
                            size={Fonts.size_14}
                          />
                        </View>
                      </View>
                      {formValidation.isBuyerDistrictEmpty && (
                        <Text
                          style={[
                            c.Styles.txtInputMerchantErrors,
                            {marginTop: -12},
                          ]}>
                          Alamat kecamatan pembeli harus diisi
                        </Text>
                      )}
                    </Pressable>
                    <Pressable
                      onPress={() => {
                        formShipping.buyerDistrict.name === 'Kecamatan'
                          ? Toast.show('Pilih kecamatan terlebih dahulu')
                          : refRBSheetPostCode.current.open();
                      }}
                      style={{}}>
                      <View
                        style={[
                          page.input,
                          {
                            flexDirection: 'row',
                            alignItems: 'center',

                            borderBottomColor:
                              formShipping.buyerPostalCode.name != 'Kode Pos'
                                ? Colors.mainBlue
                                : formValidation.isBuyerPostalCodeEmpty
                                ? 'red'
                                : Colors.textGreyLight,
                          },
                        ]}>
                        <Text style={page.inputText}>
                          {`${formShipping.buyerPostalCode.name}`}
                        </Text>
                        <View
                          style={{
                            marginLeft: 4,
                            transform: [{rotate: '180deg'}],
                          }}>
                          <MaterialCommunityIcons
                            name={'triangle'}
                            color={Colors.blueBold}
                            size={Fonts.size_14}
                          />
                        </View>
                      </View>
                      {formValidation.isBuyerPostalCodeEmpty && (
                        <Text
                          style={[
                            c.Styles.txtInputMerchantErrors,
                            {marginTop: -12},
                          ]}>
                          Alamat kode pos pembeli harus diisi
                        </Text>
                      )}
                    </Pressable>

                    <TextInput
                      onChangeText={x => {
                        setFormValidation({
                          ...formValidation,
                          isBuyerAddress1Empty: false,
                        });
                        setFormShipping({...formShipping, buyerAddress1: x});
                      }}
                      value={formShipping.buyerAddress1}
                      style={[
                        page.input,
                        {
                          borderBottomColor:
                            formShipping.buyerAddress1.length > 0
                              ? Colors.mainBlue
                              : formValidation.isBuyerAddress1Empty
                              ? 'red'
                              : Colors.textGreyLight,
                        },
                      ]}
                      placeholder="Alamat Lengkap"
                    />
                    {formValidation.isBuyerAddress1Empty && (
                      <Text
                        style={[
                          c.Styles.txtInputMerchantErrors,
                          {marginTop: -12},
                        ]}>
                        Alamat lengkap pembeli harus diisi
                      </Text>
                    )}
                    <TextInput
                      onChangeText={x =>
                        setFormShipping({...formShipping, buyerAddress2: x})
                      }
                      value={formShipping.buyerAddress2}
                      style={page.input}
                      placeholder="Detail Alamat (optional)"
                    />
                  </View>
                </View>
              </View>
              {/* C O N T E N T - - - END */}
            </View>
          </ScrollView>

          {/* F O O T E R */}
          <View
            style={[
              styles.fixedFooter,
              {backgroundColor: Colors.white, elevation: 1},
            ]}>
            <Pressable style={{width: '100%'}}>
              <Button
                onPress={() => _handleMarketplaceCheckout()}
                theme="primary"
                text="Checkout Pesanan"
                size="medium"
                // option
                style={{
                  backgroundColor: Colors.blueBold,
                  borderWidth: 0,
                  paddingVertical: 18,
                }}
                textStyle={{
                  color: Colors.white,
                }}
              />
            </Pressable>
          </View>
          {/* F O O T E R - - - END */}
        </View>
        {/* B O D Y - - - END */}

        {/* RBS - Provinsi Option */}
        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          height={height}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          }}>
          {/* Your Heres Here */}
          <View
            style={{
              paddingBottom: 20,
              borderBottomColor: Colors.borderGrey,
              borderBottomWidth: 1,
            }}>
            <Text
              style={{
                fontSize: Fonts.size_18,
                marginLeft: 24,
                fontFamily: 'NotoSans-Bold',
                color: Colors.textGreyDark,
                letterSpacing: 1,
              }}>
              Provinsi
            </Text>
          </View>
          <ScrollView>
            {/* Delivery Option */}
            <View>
              {provinceData != null && (
                <FlatList
                  data={provinceData.provincies}
                  renderItem={_renderItem}
                />
                // <DeliveryOption
                //   options={provinceData.provincies}
                //   onChange={option => {
                //     refRBSheet.current.close();
                //     console.log(option);
                //   }}
                // />
              )}
            </View>
          </ScrollView>
        </RBSheet>
        {/* RBS - District Option */}
        <RBSheet
          ref={refRBSheetDistrict}
          closeOnDragDown={true}
          height={height}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          }}>
          {/* Your Heres Here */}
          <View
            style={{
              paddingBottom: 20,
              borderBottomColor: Colors.borderGrey,
              borderBottomWidth: 1,
            }}>
            <Text
              style={{
                fontSize: Fonts.size_18,
                marginLeft: 24,
                fontFamily: 'NotoSans-Bold',
                color: Colors.textGreyDark,
                letterSpacing: 1,
              }}>
              Kecamatan
            </Text>
          </View>
          <ScrollView>
            {/* Delivery Option */}
            <View style={{paddingBottom: 48}}>
              {districtData != null && (
                <FlatList
                  data={districtData.districts}
                  renderItem={_renderItem}
                />
                // <DeliveryOption
                //   options={provinceData.provincies}
                //   onChange={option => {
                //     refRBSheet.current.close();
                //     console.log(option);
                //   }}
                // />
              )}
              {/* <DeliveryOption
                options={[
                  'Ambon',
                  'Bandung',
                  'Denpasar',
                  'Jakarta',
                  'Kupang',
                  'Makassar',
                  'Manado',
                  'Mataram',
                  'Medan',
                  'Pekanbaru',
                  'Samarinda',
                  'Serang',
                  'Surabaya',
                  'Tanjungselor',
                  'Yogyakarta',
                ]}
                onChange={option => {
                  refRBSheet.current.close();
                  console.log(option);
                }}
              /> */}
            </View>
          </ScrollView>
        </RBSheet>

        {/* RBS - City Option */}
        <RBSheet
          ref={refRBSheetCity}
          closeOnDragDown={true}
          height={height}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          }}>
          {/* Your Heres Here */}
          <View
            style={{
              paddingBottom: 20,
              borderBottomColor: Colors.borderGrey,
              borderBottomWidth: 1,
            }}>
            <Text
              style={{
                fontSize: Fonts.size_18,
                marginLeft: 24,
                fontFamily: 'NotoSans-Bold',
                color: Colors.textGreyDark,
                letterSpacing: 1,
              }}>
              Kota
            </Text>
          </View>
          <ScrollView>
            {/* Delivery Option */}
            <View style={{paddingBottom: 48}}>
              {cityData != null && (
                <FlatList data={cityData.cities} renderItem={_renderItem} />
                // <DeliveryOption
                //   options={provinceData.provincies}
                //   onChange={option => {
                //     refRBSheet.current.close();
                //     console.log(option);
                //   }}
                // />
              )}
              {/* <DeliveryOption
                options={[
                  'Ambon',
                  'Bandung',
                  'Denpasar',
                  'Jakarta',
                  'Kupang',
                  'Makassar',
                  'Manado',
                  'Mataram',
                  'Medan',
                  'Pekanbaru',
                  'Samarinda',
                  'Serang',
                  'Surabaya',
                  'Tanjungselor',
                  'Yogyakarta',
                ]}
                onChange={option => {
                  refRBSheet.current.close();
                  console.log(option);
                }}
              /> */}
            </View>
          </ScrollView>
        </RBSheet>

        {/* RBS - Postal Code Option */}
        <RBSheet
          ref={refRBSheetPostCode}
          closeOnDragDown={true}
          height={height}
          openDuration={250}
          closeDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: 'rgba(0,0,0,0.4)',
            },
            draggableIcon: {
              backgroundColor: '#d8d8d8',
            },
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            },
          }}>
          {/* Your Heres Here */}
          {/* <View
            style={{
              paddingBottom: 0,
              flexDirection: 'row',
              width: '100%',

              borderBottomWidth: 0.5,
            }}>
            <MaterialCommunityIcons
              name={'account-search'}
              color={Colors.gray}
              size={35}
            />
            <TextInput
              style={page.inputPostalCode}
              onChangeText={searchingPostalCode}
              keyboardType={'number-pad'}
              value={postalCodeValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Kode Pos'}
            />
          </View> */}
          <View
            style={{
              paddingBottom: 20,
              borderBottomColor: Colors.borderGrey,
              borderBottomWidth: 1,
            }}>
            <Text
              style={{
                fontSize: Fonts.size_18,
                marginLeft: 24,
                fontFamily: 'NotoSans-Bold',
                color: Colors.textGreyDark,
                letterSpacing: 1,
              }}>
              Kode Pos
            </Text>
          </View>
          <ScrollView>
            {postalCodeData != null && (
              <FlatList
                // style={{
                //   height: 400,
                //   width: '100%',
                //   backgroundColor: 'red',
                // }}
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                renderItem={_renderItem}
                data={
                  postalCodeFilteredData && postalCodeFilteredData.length > 0
                    ? postalCodeFilteredData
                    : postalCodeData.postalCode
                }
              />
            )}
          </ScrollView>
        </RBSheet>
      </Container>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Address);
