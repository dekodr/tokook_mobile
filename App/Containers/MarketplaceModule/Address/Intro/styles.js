import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const Page = StyleSheet.create({

  sectionTitleText: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: Fonts.size_18,
    letterSpacing: 1,
    marginBottom: 12,
  },
  sectionInfo: {

  },
  inputPostalCode:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
    marginBottom: 10,
    // backgroundColor: '#ddd',
    // borderBottomColor: Colors.borderGrey,
    // borderBottomWidth: 1,
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    marginRight: 12,
  },
  input: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 4,
    marginBottom: 22,
    // backgroundColor: '#ddd',
    borderBottomColor: Colors.borderGrey,
    borderBottomWidth: 0.6,
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    marginRight: 12,
  },
  inputText: {
    color: Colors.textGrey,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    marginRight: 12,
  }
});

export default Page;