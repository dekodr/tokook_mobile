import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  // SafeAreaView,
  Dimensions,
  TextInput,
  FlatList,
  ImageBackground,
  Image,
  RefreshControl,
  BackHandler,
  Animated,
  ActivityIndicator,
  PermissionsAndroid,
  Linking,
  Alert,
} from 'react-native';
import _ from 'lodash';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

import Swiper from 'react-native-swiper';
import * as c from '../../../../Components';
import styles from '../Intro/styles';
import page from './styles';
import SupStyle from '../../Supplier/Intro/styles';
import {Images, Colors, Fonts} from '../../../../Themes/';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../../Redux/TokoRedux';
import GlobalActions from './../../../../Redux/GlobalRedux';
import AuthActions from './../../../../Redux/AuthenticationRedux';
import AnimatedHeaderComponent from '../../../../Components/AnimatedHeader';
import Card from '../../../../Components/Card';
import ButtonIcon from '../../../../Components/ButtonIcon';
import {ProdukOption} from '../../../../Components/Option';
import ShowMore from '../../../../Components/ShowMore';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import Entypo from 'react-native-vector-icons/Entypo';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import CameraRoll from '@react-native-community/cameraroll';
import RNFetchBlob from 'rn-fetch-blob';
import Share from 'react-native-share';
import MarketplaceActions from '../../../../Redux/MarketPlaceRedux';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Zocial from 'react-native-vector-icons/Zocial';
import SafeAreaView from 'react-native-safe-area-view';
// import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import func from '../../../../Helpers/Utils';

function MarketplaceProduct(props) {
  const {
    dispatch,
    navigation,
    tokoId,
    userData,
    tokoData,
    tokoAddressData,
    marketplacedetailsProduct,
    cartData,
    provinceData,
    cityData,
    districtData,
    fetchingcityData,
    fetchingprovinceData,
    fetchingdistrictData,
    shippingFeeData,
    tokoAddress,
    shippingFee,
    shippingDestinationData,
    fetchingpostToCart,
  } = props;
  const initialProductForm = {
    product: {},
    option_data: [],
    variants: [],
  };
  const shareRef = useRef();
  const ongkirRef = useRef();
  const [stepOngkir, setstepOngkir] = useState(null);
  const [destinationShipping, setDestinationShipping] = useState(null);
  const [biayaOngkir, setBiayaOngkir] = useState(null);
  const [provinceValue, setProvinceValue] = useState(null);
  const [cityValue, setCityValue] = useState(null);
  const [editSelected, setEditSelected] = useState('');
  const [loadingAddress, setloadingAddress] = useState(false);
  const [imageCollapse, setimageCollapse] = useState([]);
  const [selectedVarian, setselectedVarian] = useState([]);
  const [message, setMessage] = useState(null);
  const [productForm, setProductForm] = useStateWithCallbackLazy(
    initialProductForm,
  );
  const [totalProduct, settotalProduct] = useState(0);
  const [facebookShareURL, setFacebookShareURL] = useState('https://tokook.id');
  const [postContent, setPostContent] = useState('');
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  console.log('isverified', isVerified, tokoAddress);
  useEffect(() => {
    if (item) {
      setTimeout(() => {
        dispatch(MarketplaceActions.getMarketplaceProductDetailsRequest(item));
      }, 1000);
    }
    dispatch(AuthActions.getUserRequest());
  }, [item]);
  useEffect(() => {
    if (tokoId) {
      dispatch(TokoActions.getTokoAddressRequest(tokoId));
    }
  }, [tokoId]);
  useEffect(() => {
    if (shippingFee !== null) {
      dispatch(MarketplaceActions.getAllShippingFeeRequest(shippingFee));
      setDestinationShipping(shippingDestinationData);
    } else if (tokoAddress && marketplacedetailsProduct) {
      let dataItems = {
        destination_id: tokoAddress.district_id,
        items: [
          {
            id: marketplacedetailsProduct.data.product.id,
            quantity: 1,
          },
        ],
      };
      dispatch(MarketplaceActions.getAllShippingFeeRequest(dataItems));
      setDestinationShipping(tokoAddress.district_details.name);
    }
  }, [tokoAddress, marketplacedetailsProduct, shippingFee]);
  useEffect(() => {
    dispatch(MarketplaceActions.getCartDataRequest());
  }, []);
  useEffect(() => {
    console.log('shippingData', shippingFeeData);
    let listKurir = [];
    if (shippingFeeData) {
      if (shippingFeeData.length === 1) {
        shippingFeeData.map((v, i) => {
          let biayaOngkir = func.rupiah(v.cost);
          console.log('biayaOngkir', biayaOngkir);
          setBiayaOngkir(biayaOngkir);
        });
      } else if (shippingFeeData.length > 1) {
        shippingFeeData.map((v, i) => {
          return (listKurir = v.cost);
        });
        listKurir = listKurir.sort(function(a, b) {
          return a - b;
        });
        let biayaOngkir =
          func.rupiah(listKurir[0]) +
          '-' +
          func.rupiah(listKurir[listKurir.length - 1]);
        setBiayaOngkir(biayaOngkir);
      }
    }
  }, [shippingFeeData]);
  useEffect(() => {
    console.log('selectedVarian', selectedVarian);
  }, [selectedVarian]);
  useEffect(() => {
    console.log('imagecollapse', imageCollapse);
  }, [imageCollapse]);
  useEffect(() => {
    if (marketplacedetailsProduct !== null) {
      setimageCollapse(marketplacedetailsProduct.data.product.images);
      let details = [];
      if (marketplacedetailsProduct.data.option_data) {
        marketplacedetailsProduct.data.option_data.map((v, i) => {
          details.push({
            id: v.values[0].id,
            name: v.values[0].name,
          });
        });
        setselectedVarian(details);
      }
      setPostContent(
        marketplacedetailsProduct.data.product.name +
          '\n' +
          marketplacedetailsProduct.data.product.description,
      );
      setMessage(
        `Hi, toko ini menjual ${marketplacedetailsProduct.data.product.name} \n \n ${marketplacedetailsProduct.data.product.description}`,
      );
      setProductForm({
        ...productForm,
        product: marketplacedetailsProduct.data.product,
        option_data: marketplacedetailsProduct.data.option_data
          ? marketplacedetailsProduct.data.option_data
          : null,
        variants: marketplacedetailsProduct.data.variants
          ? marketplacedetailsProduct.data.variants
          : null,
      });
    }
  }, [marketplacedetailsProduct]);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  const postOnFacebook = () => {

    const dataShare = {
      activity: 'share_product_from_marketplace',
      product_id: productForm.product.id,
    };
    dispatch(MarketplaceActions.postActivityLogRequest(dataShare));
    let facebookParameters = [];
    if (facebookShareURL)
      facebookParameters.push('u=' + encodeURI(facebookShareURL));
    if (postContent) facebookParameters.push('quote=' + encodeURI(postContent));
    const url =
      'https://www.facebook.com/sharer/sharer.php?' +
      facebookParameters.join('&');

    Linking.openURL(url)
      .then(data => {
        // alert('Facebook Opened');
      })
      .catch(() => {
        // alert('Something went wrong');
      });
  };

  const {params} = navigation.state.params;
  // const _shareLinkWithShareDialog = async () => {
  //   var tmp = this;
  //   const shareData = {
  //     contentType: 'link',
  //     contentUrl: 'https://tokook.id',
  //     contentTitle: 'INI TITLE',
  //     contentDescription: 'INI DESKRIPSI',
  //   };
  //   const canShow = await ShareDialog.canShow(shareData);
  //   if (canShow) {
  //     try {
  //       const {isCancelled, postId} = await ShareDialog.show(shareData);
  //       if (isCancelled) {
  //       } else {
  //       }
  //     } catch (error) {}
  //   }
  // };
  const item = params;
  const handleGoBack = () => {
    navigation.goBack();

    return true;
  };
  const openRBSheet = editSelected => {
    shareRef.current.open();
    setEditSelected(editSelected);
  };
  const handleRefresh = () => {
    dispatch(MarketplaceActions.getCartDataRequest());
  };
  const handleaddproducttoCart = () => {
    let varianName = [];
    let dataFiltered = [];
    if (productForm.variants === null) {
      const data = {
        item: {
          product: {
            id: productForm.product.id,
            code: productForm.product.code,
          },
          quantity: 1,
        },
        func: {reloadData: handleRefresh},
      };
      dispatch(MarketplaceActions.postToCartRequest(data));
    } else {
      if (selectedVarian.length > 1) {
        varianName = selectedVarian.map((v, i) => {
          return v.name;
        });
        varianName = varianName.join('-');
      } else {
        varianName = selectedVarian.map((v, i) => {
          return v.name;
        });
        varianName = varianName[0];
      }
      dataFiltered = productForm.variants.filter((v, i) => {
        return v.variant_name === varianName;
      });
      dataFiltered.map((v, i) => {
        const data = {
          item: {
            product: {
              id: productForm.product.id,
              code: v.code,
            },
            quantity: 1,
          },
          func: {reloadData: handleRefresh},
        };

        dispatch(MarketplaceActions.postToCartRequest(data));
      });
    }
  };
  const renderShareRBSheet = () => {
    switch (editSelected) {
      case 'instagram':
        return (
          <View>
            <Pressable
              onPress={() => _handleShare(Share.Social.INSTAGRAM_STORIES)}
              style={styles.txtShareModal}>
              <Text numberOfLines={1} style={styles.txtItem}>
                Bagikan Ke Story Kamu
              </Text>
            </Pressable>
            <Pressable
              onPress={() => _handleShare(Share.Social.INSTAGRAM)}
              style={styles.txtShareModal}>
              <Text numberOfLines={1} style={styles.txtItem}>
                Bagikan Ke Teman
              </Text>
            </Pressable>
          </View>
        );
        break;
    }
  };

  const renderAddress = ({item, index}) => {
    return (
      <Pressable
        onPress={() => {
          if (stepOngkir === 'province') {
            setloadingAddress(true);
            setProvinceValue(item.name);
            console.log('province', item.name);
            dispatch(TokoActions.getCityRequest(item));
            setstepOngkir('city');
          } else if (stepOngkir === 'city') {
            setCityValue(item.name);
            setloadingAddress(true);
            dispatch(TokoActions.getDistrictRequest(item));
            setstepOngkir('district');
          } else if (stepOngkir === 'district') {
            setDestinationShipping(item.name);
            ongkirRef.current.close();
            let dataItems = {
              destination_id: item.id,
              items: [
                {
                  id: productForm.product.id,
                  quantity: 1,
                },
              ],
            };
            dispatch(MarketplaceActions.setShippingDestinationData(item.name));
            dispatch(MarketplaceActions.setShippingFeeData(dataItems));
            dispatch(MarketplaceActions.getAllShippingFeeRequest(dataItems));
          }
        }}
        style={{
          padding: 15,
          borderTopWidth: index === 0 ? 0.5 : 0,
          borderBottomWidth: 0.5,
          borderBottomColor: '#c9c6c6',
          borderTopColor: '#c9c6c6',
        }}>
        <Text style={styles.txtnameAddress}>{item.name}</Text>
      </Pressable>
    );
  };

  const renderCourierList = ({item, index}) => {
    return (
      <View style={styles.viewCourierList}>
        <View>
          <Text style={styles.txtService}>
            {item.code.toUpperCase() +
              ' - ' +
              item.service +
              ` (${func.rupiah(item.cost)}) `}
          </Text>
        </View>
        <View>
          <Text style={styles.txtEdt}>Estimasi tiba {item.edt}</Text>
        </View>
      </View>
    );
  };

  const contentStepOngkir = () => {
    switch (stepOngkir) {
      case 'address':
        return (
          <View>
            <Pressable
              onPress={() => setstepOngkir('province')}
              style={styles.viewListCourier}>
              <Text style={styles.txtKirim}>Kirim ke</Text>
              <View style={styles.viewtxtDestination}>
                <Text style={styles.txtDestination}>
                  {destinationShipping ? destinationShipping : 'Pilih Tujuanmu'}
                </Text>
                <SimpleLineIcons
                  size={12}
                  color={'#43455c'}
                  name={'arrow-right'}
                />
              </View>
            </Pressable>
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={shippingFeeData}
              renderItem={renderCourierList}
            />
          </View>
        );
        break;
      case 'province':
        return (
          <View>
            {fetchingprovinceData ? (
              <ActivityIndicator size="small" color="#4a4a4a" />
            ) : (
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={provinceData.provincies}
                renderItem={renderAddress}
              />
            )}
          </View>
        );
        break;
      case 'city':
        return (
          <View>
            {fetchingcityData ? (
              <ActivityIndicator size="small" color="#4a4a4a" />
            ) : (
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={cityData.cities}
                renderItem={renderAddress}
              />
            )}
          </View>
        );
        break;
      case 'district':
        return (
          <View>
            {fetchingdistrictData ? (
              <ActivityIndicator size="small" color="#4a4a4a" />
            ) : (
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={districtData.districts}
                renderItem={renderAddress}
              />
            )}
          </View>
        );
        break;
    }
  };
  const _handleShare = platform => {
    const fs = RNFetchBlob.fs;
    let imagePath = null;
    RNFetchBlob.config({
      fileCache: true
    })
      .fetch("GET", productForm.product.images[0].url)
      // the image is now dowloaded to device's storage
      .then(resp => {
        // the image path you can use it directly with Image component
        imagePath = resp.path();
        return resp.readFile("base64");
      })
      .then(base64Data => {
        // here's base64 encoded image
        // console.log(base64Data);
        // console.log(productForm.product.images[0].url, "data:image/jpeg;base64,"+base64Data)
        const dataShare = {
          activity: 'share_product_from_marketplace',
          product_id: productForm.product.id,
        };
        dispatch(MarketplaceActions.postActivityLogRequest(dataShare));
        let options = {};
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>", platform)
        switch (platform) {

          default: {
            console.log("case whatsapp")
            options = {
              title: productForm.product.name,
              message: productForm.product.description,
              text: productForm.product.description,
              content: productForm.product.description,
              social: platform,
              url: "data:image/jpeg;base64,"+base64Data,
            };

           handleShare(options);
          }
          case 'instagram': {
            options = {
              title: productForm.product.name,
              message: productForm.product.description,
              text: productForm.product.description,
              content: productForm.product.description,
              social: platform,
              url: productForm.product.images[0].url
              // url: 'https://tokook.id/official/20907/sweater-wanita-atasan-wanita-go-knitwear-sweater-woman-dt3-2003-drak-blue',
            };

            handleShare(options);
          }

        }
        // remove the file from storage
        return fs.unlink(imagePath);
      });
    
  };

  const handleShare = options => {
    Share.open(options)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };
  const REMOTE_IMAGE_PATH =
    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/gift.png';

  const checkPermission = async () => {
    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download Photos',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          console.log('Storage Permission Granted.');
          downloadImage();
        } else {
          // If permission denied then show alert
          alert('Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = () => {
    // Main function to download the image

    // To add the time suffix in filename
    let imageUrl = [];
    let date = new Date();
    // console.log('cekimg bg', imageCollapse);
    imageCollapse.forEach((v, i) => {
      let image_URL = v.url;
      // Getting the extention of the file
      let ext = getExtention(image_URL);
      ext = '.' + ext[0];
      // console.log('image_URL', image_URL, ext);
      // Get config and fs from RNFetchBlob
      // config: To pass the downloading related options
      // fs: Directory path where we want our image to download
      const {config, fs} = RNFetchBlob;
      let PictureDir = fs.dirs.PictureDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          // Related to the Android only
          useDownloadManager: true,
          notification: true,
          path:
            PictureDir +
            '/image_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            ext,
          description: 'Image',
        },
      };
      config(options)
        .fetch('GET', image_URL)
        .then(res => {
          // Showing alert after successful downloading
          console.log('res -> ', JSON.stringify(res));
        });
    });
    // Image URL which we want to download
    //alert('Image Downloaded Successfully.');
  };

  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };
  // const  handleDownload = async () => {
  //   RNFetchBlob.config({
  //     fileCache: true,
  //     appendExt: 'png',
  //   })
  //     .fetch('GET', this.state.url)
  //     .then(res => {
  //       CameraRoll.saveToCameraRoll(res.data, 'photo')
  //         .then(res => console.log(res))
  //         .catch(err => console.log(err))
  //     })
  //     .catch(error => console.log(error);
  // };

  const OnPressCarousel = item => {
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };
  // const renderVarian = ({item, index}) => {
  //   return (

  //   );
  // };

  const _renderVarian = ({item, index, indexVarian}) => {
    return item.map((v, i) => {
      return (
        <Pressable
          onPress={() => {
            const _selectedVarian = [...selectedVarian];
            _selectedVarian[indexVarian].id = v.id;
            _selectedVarian[indexVarian].name = v.name;
            setselectedVarian(_selectedVarian);
          }}>
          <View
            style={
              selectedVarian[indexVarian].name === v.name
                ? styles.viewVarianNameSelected
                : styles.viewVarianName
            }>
            <Text
              style={
                selectedVarian[indexVarian].name === v.name
                  ? styles.txtvarianNameSelected
                  : styles.txtvarianName
              }>
              {v.name}
            </Text>
          </View>
        </Pressable>
      );
    });
  };

  const renderhorizontalItem = ({item, index}) => {
    return (
      <View style={{padding: 15}}>
        <Text style={styles.txttitleCatalog}>Pilih Varian {index + 1} :</Text>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal
          contentContainerStyle={{
            marginTop: 10,
            paddingBottom: 5,
            flexDirection: 'row',
          }}>
          <_renderVarian item={item.values} indexVarian={index} />
        </ScrollView>
      </View>
    );
  };
  const HeaderText = item => {
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        <Pressable
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </Pressable>
      </View>
    );
  };

  const FadeInView = props => {
    const [fadeAnim] = useState(new Animated.Value(1)); // Initial value for opacity: 0

    React.useEffect(() => {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: Fonts.skeleton_anim_time,
        delay: Fonts.skeleton_delay_anim_time,
      }).start();
    }, []);

    return (
      <Animated.View
        style={{
          ...props.style,
          opacity: fadeAnim,
        }}>
        {props.children}
      </Animated.View>
    );
  };

  // Render Carousel
  const isCarousel = React.useRef(null);
  // image slider
  const dataCarouselImageSlider = [
    {
      id: 1,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 2,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 3,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
  ];
  const renderCarouselImageItem = ({item, index}) => {
    return (
      <View>
        <View>
          <Image
            style={{
              width: Dimensions.get('screen').width * 1,
              height: Dimensions.get('screen').height * 0.4,
              resizeMode: 'cover',
            }}
            source={{uri: item.url}}
          />
        </View>
      </View>
      //<Card type="CardImageSlider" id={item.id} SliderImage={item.image} />
    );
  };
  const PaginationCarouselBanner = () => {
    return (
      <Pagination
        dotsLength={dataCarouselImageSlider.length}
        activeDotIndex={0}
        containerStyle={{}}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          marginHorizontal: -10,
          backgroundColor: Colors.blueBold,
        }}
        inactiveDotStyle={{
          backgroundColor: Colors.silver,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={0.8}
      />
    );
  };

  // Produk from the store
  const dataCarouselSP = [
    {
      id: 1,
      name: 'Tesla model 4, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 5, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 6, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselSPItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
          handlePress={'test'}
        />
      </Pressable>
    );
  };

  // For You
  const dataCarouselFY = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselFYItem = ({item, index}) => {
    return (
      <Pressable onPress={() => OnPressCarousel()} style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </Pressable>
    );
  };
  // Render Carousel - - - END

  const _handleMarketplaceSupplier = item => {
    navigation.navigate('MarketplaceSupplierScreen', {
      params: item,
    });
  };

  const toSupplierScreen = item => {
    navigation.navigate('MarketplaceSupplierScreen', {
      params: item,
    });
  };

  const _handleCart = item => {
    navigation.navigate('MarketplaceCartScreen', {
      params: item,
    });
  };

  const offsetHeader = useRef(new Animated.Value(0)).current;

  var offset = 0;
  const onScroll = event => {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const dif = currentOffset - (offset || 0);

    if (dif < 0) {
      console.log('up');
      Animated.event([{nativeEvent: {contentOffset: {y: offsetHeader}}}], {
        useNativeDriver: false,
      });
    } else {
      console.log('down');
      Animated.event([{nativeEvent: {contentOffset: {y: offsetHeader}}}], {
        useNativeDriver: false,
      });
    }

    offset = currentOffset;
  };

  const shareToInstagramStory = async () => {
    const shareOptions = {
      title: 'Share image to instastory',
      method: Share.InstagramStories.SHARE_BACKGROUND_IMAGE,
      backgroundImage: images.image1,
      social: Share.Social.INSTAGRAM_STORIES,
    };

    try {
      const ShareResponse = await Share.shareSingle(shareOptions);
      setResult(JSON.stringify(ShareResponse, null, 2));
    } catch (error) {
      console.log('Error =>', error);
      setResult('error: '.concat(getErrorString(error)));
    }
  };

  return (
    <Container>
      {marketplacedetailsProduct === null ? (
        <View
          style={{
            height: Dimensions.get('screen').height * 0.8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size={'small'} color={'#000'} />
        </View>
      ) : (
        <SafeAreaProvider style={styles.container}>
          <SafeAreaView
            style={{width: '100%', flex: 1}}
            forceInset={{top: 'always'}}>
            {/* H E A D E R */}
            <View
              style={[
                styles.fixedHeader,
                {
                  justifyContent: 'space-between',
                  backgroundColor: 'transparent',
                  zIndex: 2,
                  elevation: 0,
                  borderBottomColor: 'rgba(0,0,0,0)',
                  shadowColor: 'rgba(0,0,0,0)',
                },
              ]}>
              <Pressable onPress={handleGoBack}>
                <ButtonIcon
                  onPress={() => handleGoBack()}
                  icon="keyboard-backspace"
                  color="#fff"
                  size={Fonts.size_28}
                  style={{
                    backgroundColor: Colors.blueBold,
                    height: 48,
                    width: 48,
                    borderRadius: 48,
                    alignItems: 'center',
                    justifyContent: 'center',
                    // marginRight: -12,
                  }}
                />
              </Pressable>
              <Pressable>
                <View style={[styles.cartButtonWrapper]}>
                  <View style={[styles.cartIndicator]}>
                    <View style={[styles.cartIndicatorInner]}>
                      <Text style={styles.cartIndicatorInnerText}>
                        {cartData === null ? 0 : cartData.cart.length}
                      </Text>
                    </View>
                  </View>
                  <ButtonIcon
                    onPress={() => {
                      if (!isVerified) {
                        if (userData.data.is_phone_verified === null) {
                          let dataStep = {
                            modalVisible: true,
                            step: 1,
                          };
                          dispatch(GlobalActions.setModalVisible(dataStep));
                        } else if (tokoId === null) {
                          let dataStep = {
                            modalVisible: true,
                            step: 2,
                          };
                          dispatch(GlobalActions.setModalVisible(dataStep));
                        } else if (!tokoAddress) {
                          let dataStep = {
                            modalVisible: true,
                            step: 3,
                          };

                          dispatch(GlobalActions.setModalVisible(dataStep));
                        }
                        // bottomSheetRef.current.open();
                      } else {
                        _handleCart();
                      }
                    }}
                    icon="cart"
                    color="#fff"
                    size={Fonts.size_24}
                    style={{
                      backgroundColor: Colors.blueBold,
                      height: 48,
                      width: 48,
                      borderRadius: 48,
                      alignItems: 'center',
                      justifyContent: 'center',
                      // marginRight: -12,
                    }}
                  />
                </View>
              </Pressable>
            </View>
            {/* H E A D E R - - - END */}

            <AnimatedHeaderComponent animatedValue={offsetHeader} headerText={productForm.product.name} />

            <ScrollView
              showsVerticalScrollIndicator={false}
              style={[styles.scrollArea, {marginTop: -68}]}
              onScroll={Animated.event(
                [{nativeEvent: {contentOffset: {y: offsetHeader}}}],
                {useNativeDriver: false},
              )}>
              {/* B O D Y */}
              <View style={styles.body}>
                <View style={[styles.bodyInner, {marginTop: 0}]}>
                  {/* C O N T E N T */}
                  <View style={styles.content}>
                    <View style={[styles.contentInner]}>
                      {/* Section 1 - Image Slider */}
                      <View>
                        <Swiper
                          style={{
                            height: Dimensions.get('screen').height * 0.4,
                          }}>
                          {imageCollapse.map((v, i) => {
                            return (
                              <View>
                                <View>
                                  <Image
                                    style={{
                                      width: Dimensions.get('screen').width * 1,
                                      height:
                                        Dimensions.get('screen').height * 0.4,
                                      resizeMode: 'contain',
                                    }}
                                    source={{uri: v.url}}
                                  />
                                </View>
                              </View>
                            );
                          })}
                        </Swiper>
                      </View>
                      {/* Section 2 - Produk Title */}
                      <View
                        style={[
                          page.ProdukHeader,
                          {
                            paddingHorizontal: 12,
                            paddingTop: 12,
                            paddingBottom: 24,
                          },
                        ]}>
                        <View style={[styles.fdRow, {marginBottom: 8}]}>
                          <View
                            style={[
                              page.ProdukPrice,
                              styles.fdRow,
                              {alignItems: 'center'},
                            ]}>
                            <Text style={page.ProdukPriceTextMain}>
                              {productForm.product.price !== null
                                ? func.rupiah(productForm.product.price)
                                : 'Rp0'}
                            </Text>
                          </View>
                          <Pressable
                            onPress={() => {
                              func.handleShare({
                                message: message,
                              });
                              console.log('kepanggil');
                            }}
                            style={styles.mlAuto}>
                            <Entypo name="share" size={25} color={'#1a69d5'} />
                          </Pressable>
                        </View>
                        <View style={styles.row}>
                          <Text style={page.ProdukNameText}>
                            {productForm.product.name}
                          </Text>
                        </View>
                        <View style={[styles.fdRow, {marginTop: 40}]}>
                          <ButtonIcon
                            onPress={() => _handleShare(Share.Social.WHATSAPP)}
                            icon="whatsapp"
                            color={'#25d366'}
                            size={28}
                            style={{
                              backgroundColor: 'transparant',
                              borderRadius: 48,
                              width: 48,
                              height: 48,
                              borderColor: '#25d366',
                              borderWidth: 2,
                              borderStyle: 'solid',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}
                          />
                          <ButtonIcon
                            onPress={() => _handleShare(Share.Social.INSTAGRAM)}
                            icon="instagram"
                            color="#e84393"
                            size={28}
                            style={{
                              marginLeft: 5,
                              marginRight: 5,
                              backgroundColor: 'transparant',
                              borderRadius: 48,
                              width: 48,
                              height: 48,
                              borderColor: '#e84393',
                              borderWidth: 2,
                              borderStyle: 'solid',
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}
                          />
                          <Pressable
                            onPress={() => _handleShare(Share.Social.FACEBOOK)}
                            style={{
                              alignItems: 'flex-start',
                              justifyContent: 'center',
                              marginLeft: 5,
                              marginRight: 5,
                              borderWidth: 2,
                              borderRadius: 100,
                              width: 48,
                              borderColor: '#1778f2',
                              height: 48,
                            }}>
                            <View
                              style={{
                                alignSelf: 'center',
                              }}>
                              <FontAwesome
                                name={'facebook'}
                                color={'#1778f2'}
                                size={25}
                              />
                            </View>
                          </Pressable>
                          <Pressable
                            onPress={() => _handleShare(Share.Social.TELEGRAM)}
                            style={{
                              alignItems: 'flex-start',
                              justifyContent: 'center',
                              borderWidth: 2,
                              borderRadius: 100,
                              width: 48,
                              borderColor: '#1DA1F2',
                              height: 48,
                            }}>
                            <View
                              style={{
                                alignSelf: 'center',
                              }}>
                              <FontAwesome
                                name={'twitter'}
                                color={'#1DA1F2'}
                                size={25}
                              />
                            </View>
                          </Pressable>

                          <Pressable onPress={checkPermission}>
                            <View
                              style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignSelf: 'flex-end',
                                flex: 1,
                                // width: Dimensions.get('screen').width * 0.5,
                                marginLeft: 10,
                                borderRadius: 25,
                                height: 48,
                                paddingHorizontal: 16,
                                // padding: 10,
                                flexDirection: 'row',
                                borderWidth: 2,
                                borderColor: Colors.blueBold,
                              }}>
                              <FontAwesome
                                name="download"
                                size={18}
                                color={Colors.blueBold}
                              />
                              <Text
                                style={{
                                  fontFamily: 'NotoSans-Regular',
                                  fontSize: 12,
                                  letterSpacing: 0.8,
                                  color: Colors.blueBold,
                                  marginLeft: 5,
                                }}>
                                Download Foto
                              </Text>
                            </View>
                          </Pressable>
                        </View>
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 3 - Options */}
                      <View>
                        <FlatList
                          showsHorizontalScrollIndicator={false}
                          data={productForm.option_data}
                          renderItem={renderhorizontalItem}
                        />
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 4 - Ongkos Kirim */}
                      <Pressable
                        onPress={() => {
                          console.log(
                            'shippingFeeData',
                            shippingFeeData,
                            tokoAddress,
                          );
                          setstepOngkir(
                            shippingFeeData === null && tokoAddress === null
                              ? 'province'
                              : 'address',
                          );
                          ongkirRef.current.open();
                        }}
                        style={{padding: 15}}>
                        <View style={{flexDirection: 'row'}}>
                          <Feather
                            name="truck"
                            size={15}
                            color={c.Colors.mainBlue}
                            style={{marginRight: 10}}
                          />
                          <View style={{flex: 1}}>
                            <View style={{flexDirection: 'row'}}>
                              <Text style={styles.txttitleCatalog}>
                                Cek Harga Ongkir
                              </Text>
                              <Pressable
                                onPress={() => ongkirRef.current.open()}
                                style={{
                                  paddingTop: 3,
                                  paddingLeft: 10,
                                  paddingRight: 10,
                                }}>
                                <View style={{alignSelf: 'center'}}>
                                  <SimpleLineIcons
                                    size={12}
                                    color={'#43455c'}
                                    name={'arrow-down'}
                                  />
                                </View>
                              </Pressable>
                            </View>

                            <View
                              style={{
                                flexDirection: 'row',
                                marginTop: 10,
                              }}>
                              <View style={{width: '30%'}}>
                                <Text style={styles.txttitleOngkir}>
                                  Ongkos kirim :
                                </Text>
                              </View>
                              <View>
                                <Text style={styles.txtdescOngkir}>
                                  {biayaOngkir === null ? 'Rp0' : biayaOngkir}
                                </Text>
                              </View>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                marginTop: 10,
                                marginBottom: 10,
                              }}>
                              <View style={{width: '30%'}}>
                                <Text style={styles.txttitleOngkir}>
                                  Tujuan:
                                </Text>
                              </View>
                              <View>
                                <Text style={styles.txtdescOngkir}>
                                  {destinationShipping === null
                                    ? 'Pilih Tujuan Pengiriman'
                                    : destinationShipping}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </View>
                      </Pressable>

                      <View style={styles.divider}></View>

                      {/* Section 5 - Informasi Produk 1 */}
                      <View style={{padding: 15}}>
                        <Text style={styles.txttitleCatalog}>
                          Informasi Produk
                        </Text>
                        <View style={{marginTop: 15}}>
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            <View style={{width: '30%'}}>
                              <Text style={styles.txtvarianName}>Berat</Text>
                            </View>

                            <Text style={styles.txtInformation}>
                              {productForm.product.weight !== null
                                ? productForm.product.weight + ' gram'
                                : '0 gram'}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              marginTop: 10,
                            }}>
                            <View style={{width: '30%'}}>
                              <Text style={styles.txtvarianName}>Stok</Text>
                            </View>
                            <Text style={styles.txtInformation}>
                              {productForm.product.stock !== null
                                ? productForm.product.stock + ' buah'
                                : '0 buah'}
                            </Text>
                          </View>
                        </View>
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 6 - Informasi Produk 2 */}
                      <View
                        style={{paddingVertical: 24, paddingHorizontal: 12}}>
                        <Text
                          style={[page.UpperOptionText, {marginBottom: 12}]}>
                          Deskripsi Produk
                        </Text>
                        <View style={[styles.fdRow]}>
                          <Text>{productForm.product.description}</Text>
                        </View>
                      </View>

                      <View style={styles.divider}></View>

                      {/* Section 7 - Video Link */}
                      {/* <View
                        style={{paddingVertical: 24, paddingHorizontal: 12}}>
                        <Text
                          style={[page.UpperOptionText, {marginBottom: 12}]}>
                          Video Produk
                        </Text>
                        <View style={[styles.fdRow]}>
                          <Text
                            style={[
                              page.UpperInfoText2,
                              {
                                color: Colors.blueWhite,
                                textDecorationLine: 'underline',
                                textDecorationStyle: 'solid',
                                textDecorationColor: Colors.blueWhite,
                              },
                            ]}>
                            https://www.youtube.com/watch?v=bHugMs0znEI
                          </Text>
                        </View>
                      </View> */}

                      {/* <View style={styles.divider}></View> */}

                      {/* Section 8 - Supplier Store */}
                      {/* <View style={[SupStyle.header, {marginTop: 24}]}>
                        <Pressable onPress={() => toSupplierScreen()}>
                          <ImageBackground
                            source={require('../../../../assets/image/honda-jazz-hero.png')}
                            style={[SupStyle.headerProfile]}></ImageBackground>
                        </Pressable>
                        <View style={SupStyle.headerInfo}>
                          <Pressable onPress={() => toSupplierScreen()}>
                            <Text
                              numberOfLines={1}
                              style={[SupStyle.headerName]}>
                              Toko Segala Ada
                            </Text>
                          </Pressable>
                          <View style={[SupStyle.headertStoreLocation]}>
                            <Image
                              source={require('../../../../assets/image/location_pin.png')}
                              style={[SupStyle.headertStoreLocationIcon]}
                            />
                            <Text
                              numberOfLines={1}
                              style={[SupStyle.headerStoreLocationText]}>
                              Menteng, Jakarta Pusat
                            </Text>
                          </View>
                          <View
                            style={[
                              SupStyle.headerProdukTotal,
                              {marginTop: 6},
                            ]}>
                            <Text
                              numberOfLines={1}
                              style={[SupStyle.headerProdukTotalText]}>
                              50 Produk
                            </Text>
                          </View>
                        </View>
                      </View> */}

                      {/* <View style={styles.divider}></View> */}

                      {/* Section 9 - Selected Product From This Store */}
                      {/* <View style={{paddingVertical: 24}}>
                        <HeaderText text={'Produk Toko Ini'} />
                        <View style={c.Styles.SliderWrapper}>
                          <Carousel
                            ref={isCarousel}
                            //data={dataCarouselSP}
                            renderItem={renderCarouselSPItem}
                            sliderWidth={wp('100%')}
                            itemWidth={wp('45%')}
                            loop={true}
                            loopClonesPerSide={2}
                            inactiveSlideScale={1}
                            inactiveSlideOpacity={1}
                            enableMomentum={true}
                            activeSlideAlignment={'start'}
                            activeAnimationType={'spring'}
                            // activeAnimationOptions={{
                            //     friction: 4,
                            //     tension: 40
                            // }}
                            slideStyle={{overflow: 'visible'}}
                            autoplay={true}
                            autoplayDelay={0}
                            autoplayInterval={6000}
                          />
                        </View>
                      </View> */}

                      {/* <View style={styles.divider}></View> */}

                      {/* Section 10 - Selected Product for you */}
                      {/* <View style={{paddingVertical: 24}}>
                        <View
                          style={{
                            paddingHorizontal: 12,
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <Text style={styles.HeaderText}>
                            Produk Pilihan Untuk Kamu
                          </Text>
                        </View>
                        <View style={c.Styles.SliderWrapper}>
                          <Carousel
                            ref={isCarousel}
                            data={dataCarouselFY}
                            renderItem={renderCarouselFYItem}
                            sliderWidth={wp('100%')}
                            itemWidth={wp('45%')}
                            loop={true}
                            loopClonesPerSide={2}
                            inactiveSlideScale={1}
                            inactiveSlideOpacity={1}
                            enableMomentum={true}
                            activeSlideAlignment={'start'}
                            activeAnimationType={'spring'}
                            // activeAnimationOptions={{
                            //     friction: 4,
                            //     tension: 40
                            // }}
                            slideStyle={{overflow: 'visible'}}
                            autoplay={true}
                            autoplayDelay={0}
                            autoplayInterval={6000}
                          />
                        </View>
                      </View>
                     */}
                    </View>
                  </View>
                  {/* C O N T E N T - - - END */}
                </View>
              </View>
              {/* B O D Y - - - END */}
            </ScrollView>

            {/* F O O T E R */}
            <View
              style={[
                styles.fixedFooter,
                {backgroundColor: Colors.white, elevation: 1},
              ]}>
              <Pressable style={{flex: 2, marginRight: 6}}>
                <ButtonIcon
                  onPress={() => {
                    if (!isVerified) {
                      if (userData.data.is_phone_verified === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 1,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (tokoId === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 2,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (!tokoAddress) {
                        let dataStep = {
                          modalVisible: true,
                          step: 3,
                        };

                        dispatch(GlobalActions.setModalVisible(dataStep));
                      }
                      // bottomSheetRef.current.open();
                    } else {
                      navigation.navigate(
                        'MarketplaceAddProductToOwnTokoScreen',
                        {
                          item: marketplacedetailsProduct.data,
                        },
                      );
                    }
                  }}
                  icon="store-outline"
                  text="Toko Saya"
                  color={Colors.blueBold}
                  size={Fonts.size_28}
                  AddtoSomething
                  plusColor={Colors.blueBold}
                  style={{
                    borderRadius: 6,
                    width: '100%',
                    height: 48,
                    borderColor: Colors.blueBold,
                    borderWidth: 2,
                    borderStyle: 'solid',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  textStyle={{
                    color: Colors.blueBold,
                    fontFamily: 'NotoSans-Bold',
                    marginLeft: 15,
                  }}
                />
              </Pressable>
              <Pressable style={{flex: 2, marginLeft: 6}}>
                <ButtonIcon
                  onPress={() => {
                    console.log('isverified', isVerified);
                    if (!isVerified) {
                      if (userData.data.is_phone_verified === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 1,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (tokoId === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 2,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (!tokoAddress) {
                        let dataStep = {
                          modalVisible: true,
                          step: 3,
                        };

                        dispatch(GlobalActions.setModalVisible(dataStep));
                      }
                      // bottomSheetRef.current.open();
                    } else {
                      handleaddproducttoCart();
                    }
                  }}
                  loadingText={fetchingpostToCart}
                  icon={fetchingpostToCart ? null : 'cart-outline'}
                  text="Keranjang"
                  color={Colors.white}
                  size={Fonts.size_28}
                  AddtoSomething
                  plusColor={Colors.white}
                  style={{
                    borderRadius: 6,
                    width: '100%',
                    height: 48,
                    backgroundColor: Colors.blueBold,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  textStyle={{
                    color: Colors.white,
                    fontFamily: 'NotoSans-Bold',
                    marginLeft: 15,
                  }}
                />
              </Pressable>
            </View>
            {/* F O O T E R - - - END */}
          </SafeAreaView>
        </SafeAreaProvider>
      )}
      <RBSheet
        //onClose={handleCleanState}
        ref={shareRef}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.2}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <View>{renderShareRBSheet()}</View>
      </RBSheet>
      <RBSheet
        onClose={() => {
          //dispatch(MarketplaceActions.getAllShippingFeeReset());
          //setBiayaOngkir(null);
          setstepOngkir(tokoAddress === null ? 'province' : 'address');
        }}
        ref={ongkirRef}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.6}
        openDuration={250}>
        <View style={{padding: 15, flexDirection: 'row', alignItems: 'center'}}>
          {stepOngkir === 'address' ||
          (tokoAddress === null &&
            stepOngkir === 'province' &&
            shippingFeeData === null) ? null : (
            <Pressable
              onPress={() => {
                if (stepOngkir === 'province') {
                  setstepOngkir('address');
                } else if (stepOngkir === 'city') {
                  setstepOngkir('province');
                } else if (stepOngkir === 'district') {
                  setstepOngkir('city');
                }
              }}>
              <MaterialCommunityIcons
                name={'arrow-left'}
                size={20}
                color={'#111432'}
              />
            </Pressable>
          )}

          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 0.3,
              color: '#111432',
              marginLeft:
                stepOngkir === 'address' ||
                (tokoAddress === null &&
                  stepOngkir === 'province' &&
                  shippingFeeData === null)
                  ? 0
                  : 10,
            }}>
            {stepOngkir === 'address'
              ? 'Cek Harga Ongkir'
              : stepOngkir === 'province'
              ? 'Pilih Provinsi'
              : stepOngkir === 'city'
              ? provinceValue
              : cityValue}
          </Text>
        </View>
        <Content>
          <View>{contentStepOngkir()}</View>
        </Content>
      </RBSheet>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    marketplacedetailsProduct:
      state.marketplace.MarketplaceProductDetailsData.data,
    cartData: state.marketplace.getCartData.data
      ? state.marketplace.getCartData.data.data
        ? state.marketplace.getCartData.data.data
        : null
      : null,
    fetchingpostToCart: state.marketplace.postToCartData.fetching,
    shippingFee: state.marketplace.shippingFee.data,
    shippingDestinationData: state.marketplace.shippingDestinationData.data,
    shippingFeeData: state.marketplace.shippingFeeData.data
      ? state.marketplace.shippingFeeData.data.data
        ? state.marketplace.shippingFeeData.data.data.courierGroup[0]
            .courierList
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoAddress:
      state.toko.tokoAddressData.data !== null
        ? state.toko.tokoAddressData.data.listAddress
          ? state.toko.tokoAddressData.data.listAddress[0]
          : null
        : null,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    fetchingprovinceData: state.toko.provinceData.fetching,
    fetchingcityData: state.toko.cityData.fetching,
    fetchingdistrictData: state.toko.districtData.fetching,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceProduct);
