import {Dimensions, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../../../Themes';

const {width, height} = Dimensions.get('window');

const page = StyleSheet.create({
  txtService: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#9b9b9b',
  },

  txtEdt: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 10,
    letterSpacing: 0.25,
    color: '#4a4a4a',
    marginTop: 1,
  },
  txtnameAddress: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#9b9b9b',
  },
  txtKirim: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#9b9b9b',
  },
  txtDestination: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#43455c',
  },
  viewtxtDestination: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 0.3,
  },
  viewCourierList: {
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: '#c9c6c6',
  },
  viewListCourier: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 0.5,
    borderColor: '#c9c6c6',
    justifyContent: 'space-between',
    width: '100%',
    flexDirection: 'row',
  },
  viewVarianName: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 0.5,
    borderColor: '#9b9b9b',
    borderRadius: 20,
    marginRight: 8,
  },
  viewVarianNameSelected: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 0.5,
    borderColor: '#9b9b9b',
    borderRadius: 20,
    marginRight: 8,
    backgroundColor: '#1a69d5',
  },
  txtvarianNameSelected: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.8,
    color: '#fff',
  },
  txtvarianName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.8,
    color: '#9b9b9b',
  },
  txtInformation: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.8,
    color: '#111432',
  },
  txttitleCatalog: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    letterSpacing: 0.3,
    color: '#43455c',
  },

  ProdukPriceTextMain: {
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_26,
    color: Colors.blueBold,
    lineHeight: 28,
  },
  ProdukPriceTextSlash: {
    fontFamily: 'NotoSans-Bold',
    paddingLeft: 12,
    fontSize: Fonts.size_16,
    color: Colors.blueSky,
    lineHeight: 20,
    // textDecorationColor: Colors.redLight,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    width: '40%',
  },
  ProdukNameText: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_18,
    color: '#4a4a4a',
    lineHeight: 20,
    width: '90%',
  },

  UpperOptionText: {
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_18,
    color: Colors.textGreyDark,
    lineHeight: 22,
  },

  UpperInfoText: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    color: Colors.textGrey,
    lineHeight: 20,
  },
  UpperInfoText2: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    color: Colors.textGreyDark,
    lineHeight: 20,
  },

  PlaceholderWrapper: {
    position: 'relative',
    overflow: 'hidden',
  },
  PlaceholderBox: {
    position: 'absolute',
    zIndex: -1,
    // zIndex: 2,
    // backgroundColor: '#ddd',
  },

  // Row & Col
  fdRow: {
    flexDirection: 'row',
  },
  fdCol: {
    flexDirection: 'column',
  },
  mlAuto: {
    marginLeft: 'auto',
  },
  txtvarianNameSelected: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.8,
    color: '#fff',
  },
  txtvarianName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.8,
    color: '#9b9b9b',
  },
  viewVarianName: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 0.5,
    borderColor: '#9b9b9b',
    borderRadius: 20,
    marginRight: 8,
  },
  viewVarianNameSelected: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderWidth: 0.5,
    borderColor: '#9b9b9b',
    borderRadius: 20,
    marginRight: 8,
    backgroundColor: '#1a69d5',
  },
  // Header Text
  HeaderText: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    fontSize: 15,
    letterSpacing: 1,
  },
  HeaderAllLink: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.blueBold,
    fontSize: Fonts.size_16,
    letterSpacing: 0.5,
  },

  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Main Area Scroll View
  scrollArea: {
    flex: 1,
  },

  body: {
    // paddingTop: StatusBar.currentHeight,
    width: width,
    position: 'relative',
    zIndex: 1,
    // padding: 20,
  },

  bodyInner: {
    marginTop: 24,
    position: 'relative',
    zIndex: 3,
    // paddingHorizontal: 20,
  },

  // Header section
  fixedHeader: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'relative',
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.05,
    shadowRadius: 2,
  },
  fixedHeaderPageIndicator: {
    height: 48,
    justifyContent: 'center',
  },
  fixedHeaderText: {
    color: Colors.white,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
    flex: 1,
    paddingVertical: 0,
    borderRadius: 5,
  },
  searchBarIcon: {
    height: 48,
    width: 48,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  searchBarInput: {
    // backgroundColor: '#000',
    flex: 1,
    height: 48,
    paddingVertical: 0,
    backgroundColor: 'transparent',
    color: Colors.textGreyLight,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
    justifyContent: 'center',
  },
  searchBarInputPlaceholder: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    letterSpacing: 1,
  },
  header: {
    width: '100%',
    height: 130,
    position: 'relative',
    backgroundColor: '#fff',
    // padding: 20,
  },
  headerTop: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    // position: 'absolute',
    // right: 20,
    // top: 0,
    padding: 20,
    // backgroundColor: '#fff',
  },
  icon: {
    width: 20,
    height: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  headerBottom: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    // left: 30,
    paddingHorizontal: 30,
    bottom: 0,
    paddingVertical: 20,
  },

  cartButtonWrapper: {
    position: 'relative',
    // backgroundColor: '#ddd',
  },
  cartIndicator: {
    position: 'absolute',
    backgroundColor: Colors.blueBold,
    width: 23,
    height: 23,
    borderRadius: 23,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    right: -12,
  },
  cartIndicatorInner: {
    height: 18,
    width: 18,
    borderRadius: 18,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartIndicatorInnerText: {
    color: Colors.blueBold,
    fontSize: Fonts.size_14,
    fontFamily: 'NotoSans-Bold',
  },
  // Header Section - - - END

  // Footer Section
  fixedFooter: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 100,
    borderTopWidth: 6,
    borderTopColor: Colors.grayBlackWhite,
  },
  // Footer Section - - - END

  // Content Section
  content: {
    marginTop: 0,
    paddingHorizontal: 0,
    // padding: 30,
    paddingBottom: 0,

    // borderTopRightRadius: 25,
    // borderTopLeftRadius: 25,
    position: 'relative',
    zIndex: 3,
  },
  contentOutter: {
    width: '100%',
    paddingVertical: 60,
    backgroundColor: '#555',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'relative',
  },
  contentInner: {
    width: '100%',
    // paddingHorizontal: 30,
    // padding: 20,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 75,
  },
  cart: {
    paddingLeft: 10,
  },

  divider: {
    width: '100%',
    height: 6,
    backgroundColor: Colors.grayBlackWhite,
  },
});

export default page;
