import {StyleSheet} from 'react-native';
import {Colors} from '../../../Themes';

export default StyleSheet.create({
  mainContainer: {
    backgroundColor: Colors.mainBlue,
    height: '100%',
    width: '100%',
    paddingTop: 25,
    paddingRight: -12,
    paddingLeft: -12,
    flex: 1,
  },
  imgTop: {
    height: 25,
    width: 25,
  },
  textLabelEmailDesc: {
    marginTop: 25,
    color: Colors.white,
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'NoToSans',
    letterSpacing: 1.83,
    textAlign: 'center',
  },
  boldFont: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
