import React, {useEffect, useState} from 'react';

import {
  ScrollView,
  Text,
  Image,
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Linking,
  Modal,
  ActivityIndicator,
  BackHandler,
  Pressable,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import {Images, Colors} from '../../../Themes';
import {connect} from 'react-redux';
import AuthActions from './../../../Redux/AuthenticationRedux';
// Styles
import * as c from '../../../Components';
import styles from './styles';
import images from '../../../Themes/Images';
import moment from 'moment';

function LoginScreen(props) {
  const {getLoginEmailPostData, isFetching, navigation} = props;
  const [emailValue, setEmailValue] = useState('');
  const [errMsg, setErrMsg] = useState('');
  useEffect(() => {
    if (props.errSystem) {
      setErrMsg(props.errSystem.message);
      setTimeout(() => {
        setErrMsg('');
        props.dispatch(AuthActions.loginEmailPostReset());
      }, 2000);
    }
  }, [props.errSystem]);
  // useEffect(() => {
  //   //sementara
  //   props.navigation.navigate('TokoScreen');
  // }, []);
  // useEffect(() => {
  //   const backAction = () => {
  //     BackHandler.exitApp();
  //     return true;
  //   };

  //   const backHandler = BackHandler.addEventListener(
  //     'hardwareBackPress',
  //     backAction,
  //   );

  //   return () => backHandler.remove();
  // }, []);
  // useEffect(() => {
  //   if (isFetching) {
  //     setTimeout(() => {
  //       setTimeout(() => {
  //         props.dispatch(AuthActions.loginEmailPostReset());
  //         setErrMsg('sedang terjadi masalah. coba lagi');
  //       }, 100);
  //       setTimeout(() => {
  //         props.dispatch(AuthActions.loginEmailPostReset());
  //         setErrMsg('');
  //       }, 200);
  //     }, 10000);
  //   } else {
  //     console.log('success', true);
  //   }
  // }, [getLoginEmailPostData, isFetching]);

  const loginSubmit = () => {
    // if (emailValue === null) {
    //   setErrMsg('Email Belum Di Isi');
    //   setTimeout(() => {
    //     setErrMsg(null);
    //   }, 3000);
    // } else {
    validemail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (emailValue.length === 0) {
      setErrMsg('email tidak boleh kosong');
      setTimeout(() => {
        setErrMsg('');
      }, 2000);
    } else if (!validemail.test(emailValue)) {
      setErrMsg('email tidak valid');
      setTimeout(() => {
        setErrMsg('');
      }, 2000);
    } else {
      const getLastEmail =
        getLoginEmailPostData != null ? getLoginEmailPostData.user.email : '';
      const getLoginTime =
        getLoginEmailPostData != null ? getLoginEmailPostData.loginTime : null;
      const currentTime = moment().format('LLL');
      var date2 = new Date();

      console.log('cek date current', emailValue);
      console.log('cek email last', getLastEmail);
      console.log('current login', new Date().getTime());
      console.log('login time', new Date(getLoginTime).getTime());

      if (getLastEmail === emailValue) {
        console.log('masuk sama', true);
        if (new Date().getTime() > new Date(getLoginTime).getTime()) {
          console.log('waktu habisa', true);

          const data = {
            email: emailValue,
            source: 'login_or_register',
            type: 'email',
          };
          props.dispatch(AuthActions.loginEmailPostRequest(data));
        } else {
          console.log('waktu habisa', false);

          navigation.navigate('OtpScreen');
        }
      } else {
        console.log('masuk sama', false);
        const data = {
          email: emailValue,
          source: 'login_or_register',
          type: 'email',
        };
        props.dispatch(AuthActions.loginEmailPostRequest(data));
      }

      // }
    }
  };
  //   const B = children => {
  //     return <Text style={styles.boldFont}>{...children}</Text>;
  //   };
  const {navigate} = props.navigation;
  const handleWhatsapp = () => {
    Linking.openURL('whatsapp://send?phone=+6287878983919').catch(() =>
      Linking.openURL(
        'https://apps.apple.com/id/app/whatsapp-messenger/id310633997',
      ),
    );
  };
  const handleToc = () => {
    Linking.openURL('http://payok.id/terms-and-conditions/');
  };
  const handlePrivacyPolicy = () => {
    Linking.openURL('https://payok.id/privacy-policy/');
  };
  const B = props => (
    <Text
      onPress={props.onPress}
      style={{fontFamily: 'NotoSans-Bold', color: 'white'}}>
      {props.children}
    </Text>
  );
  useEffect(() => {
    if (isFetching) {
      props.dispatch(AuthActions.loginEmailPostLoadingReset());
    }
  }, []);
  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={{width: '100%', height: '70%'}}>
        <Image
          source={images.tokoOkBlueFeather}
          style={{alignSelf: 'center', width: 100, height: 150}}
        />
        <Text style={styles.textLabelEmailDesc}>
          Masukkan
          <B> E-mail</B> Untuk Melanjutkan
        </Text>
        <View
          style={{
            width: '90%',
            height: 40,
            backgroundColor: 'white',
            alignSelf: 'center',
            marginTop: 25,
            justifyContent: 'center',
            paddingLeft: 12,
            borderRadius: 4,
          }}>
          <TextInput
            keyboardType={'email-address'}
            style={{height: '100%', width: '100%', color: Colors.mainBlue}}
            onChangeText={text => {
              setEmailValue(text);
              setErrMsg('');
            }}
            placeholderTextColor={Colors.mainBlue}
            placeholder={'Email'}
            value={emailValue}
          />
        </View>

        <View
          style={{
            width: '95%',
            alignSelf: 'center',
          }}>
          <Text style={styles.textLabelEmailDesc}>
            Dengan melanjutkan, berarti kamu setuju dengan{' '}
            <B onPress={handleToc}>Syarat & Ketentuan</B> dan {'\n'}
            <B onPress={handlePrivacyPolicy}>Kebijakan Privasi</B>
          </Text>
        </View>

        <Pressable
          disabled={props.isFetching}
          onPress={loginSubmit}
          style={{
            marginTop: 25,
            alignSelf: 'center',
            width: '90%',
            height: 45,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 25,
          }}>
          {props.isFetching ? (
            <ActivityIndicator
              color={Colors.blueBold}
              animating={true}
              size="large"
            />
          ) : (
            <Text
              style={{
                fontSize: 17,
                color: Colors.blueBold,
                fontFamily: 'NotoSans-Bold',
              }}>
              Lanjutkan
            </Text>
          )}
        </Pressable>
      </View>

      <View
        style={{
          flexDirection: 'row',
          height: '30%',
          width: '85%',
          alignSelf: 'center',

          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: 16,
            fontFamily: 'NotoSans',
            bottom: 45,
            color: 'white',
            alignSelf: 'flex-end',
          }}>
          Butuh bantuan?
        </Text>

        <TouchableOpacity
          onPress={handleWhatsapp}
          style={{
            bottom: 40,
            marginTop: 25,
            marginLeft: 12,
            backgroundColor: 'white',
            height: 40,
            justifyContent: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 25,
            paddingLeft: 10,
            paddingRight: 10,
            alignSelf: 'flex-end',
          }}>
          <Image
            style={{
              resizeMode: 'contain',
              width: 25,
              height: 25,
              marginRight: 10,
            }}
            source={Images.waIcon}
          />
          <Text
            style={{marginTop: 0, fontFamily: 'NotoSans', letterSpacing: 1.79}}>
            Hubungi Kami
          </Text>
        </TouchableOpacity>
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={errMsg != '' ? true : false}
        //  onRequestClose={this.closeModal}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
          }}>
          <View
            style={{
              width: 300,
              height: 85,
              backgroundColor: 'white',
              borderRadius: 12,
            }}>
            <IconAntDesign
              // onPress={stepBack}
              color={'red'}
              name="warning"
              size={21}
              style={{alignSelf: 'center', marginTop: 8}}
            />
            <Text
              style={{
                alignSelf: 'center',
                marginTop: 12,
                fontFamily: 'NotoSans',
                letterSpacing: 0.98,
                textAlign: 'center',
                bottom: 5,
              }}>
              {errMsg}
            </Text>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}
const mapDispatchToProps = dispatch => ({
  dispatch,
});

const mapStateToProps = state => {
  return {
    businessType: state.toko.getBusinessTypeData.data,
    errSystem: state.auth.loginEmailPostData.error,
    isFetching: state.auth.loginEmailPostData.fetching,
    getLoginEmailPostData: state.auth.loginEmailPostData.data,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
