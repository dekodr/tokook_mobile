import React, {useEffect, useState} from 'react';
import {StackActions, NavigationActions} from 'react-navigation';
import {ActivityIndicator, ToastAndroid, View} from 'react-native';
import {connect} from 'react-redux';
import {Colors} from '../../../Themes';
import GlobalActions from '../../../Redux/GlobalRedux';

import AuthActions from '../../../Redux/AuthenticationRedux';
import TokoActions from '../../../Redux/TokoRedux';
import AsyncStorage from '@react-native-community/async-storage';
function StartupModule(props) {
  const access_token = props.checkAccessToken;

  const {dispatch, tokoId} = props;
  // useEffect(() => {
  //   if (!props.isLoginSuccess) {
  //     dispatch(GlobalActions.setTokenApiGloballyRequest(access_token));
  //     dispatch(GlobalActions.refreshTokenApiGloballyRequest(access_token));
  //   }
  // }, [access_token, props.isLoginSuccess]);
  useEffect(() => {
    dispatch(AuthActions.loginEmailPostLoadingReset());

    (async () => {
      AsyncStorage.getItem('isLogin').then(value => {
        if (value) {
          let dataStep = {
            modalVisible: false,
            step: 1,
          };
          dispatch(GlobalActions.setModalVisible(dataStep));
          AsyncStorage.getItem('bearerToken').then(bearer => {
            if (bearer) {
              dispatch(
                StackActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({
                      routeName: 'Dashboard',
                    }),
                  ],
                }),
              );
            }
          });

          // if (getToken != null) {
          //   dispatch(GlobalActions.setTokenApiGloballyRequest(access_token));
          //   dispatch(
          //     GlobalActions.refreshTokenApiGloballyRequest(access_token),
          //   );
          // }
        }
      });
    })();
  }, []);
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator
        color={Colors.blueBold}
        animating={true}
        size={'large'}
      />
    </View>
  );
}

const mapStateToProps = state => {
  return {
    app: state.app,
    checkAccessToken: state.app.tokenApi.data,
    isLoginSuccess: state.toko.tokoPostData.success,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StartupModule);
