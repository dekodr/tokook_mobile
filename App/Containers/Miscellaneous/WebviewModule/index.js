import React, {useEffect, useRef, useState} from 'react';
import {View, Text, ScrollView, Pressable, SafeAreaView} from 'react-native';

import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';
import styles from '../../MarketplaceModule/Intro/styles';
import {Images, Colors, Fonts} from '../../../Themes/';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {StackActions, NavigationActions} from 'react-navigation';

function Webview(props) {
  const {dispatch, navigation} = props;

  useEffect(() => {}, []);
  const handleGoBack = () => {
    dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          navigation.navigate({
            routeName: 'Order',
          }),
        ],
      }),
    );
  };
  return (
    <Container
      style={[
        styles.PlaceholderWrapper,
        {width: '100%', backgroundColor: '#fff'},
      ]}>
      <Container style={styles.container}>
        {/* H E A D E R */}
        <View style={styles.fixedHeader}>
          <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={Fonts.size_24}
            />
          </Pressable>
          <View style={styles.fixedHeaderPageIndicator}>
            <Text style={styles.fixedHeaderText}>Checkout</Text>
          </View>
        </View>
        {/* H E A D E R - - - END */}
        <ScrollView style={styles.scrollArea}>
          {/* B O D Y */}
          <SafeAreaView style={styles.body}>
            <View style={styles.bodyInner}>
              {/* C O N T E N T */}
              <View style={styles.content}>
                <View style={[styles.contentInner, {marginTop: 0}]}></View>
              </View>
              {/* C O N T E N T - - - END */}
            </View>
          </SafeAreaView>
          {/* B O D Y - - - END */}
        </ScrollView>
        {/* F O O T E R */}
        {/* F O O T E R - - - END */}
      </Container>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    getFinalCartData: state.marketplace.cartCheckoutData.data,
    getOwnerTokoCourierData: state.toko.ownerTokoCourierData.data,
    getCheckoutBuyerAndSenderData: state.marketplace.addressCheckoutData.data,
    getShippingServices: state.marketplace.shippingFeeData.data,
    getPaymentChannelData: state.marketplace.paymentChannelData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Webview);
