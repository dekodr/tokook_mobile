import {StyleSheet} from 'react-native';
import metrics from '../../../Themes/Metrics';

import {Colors} from './../../../Themes';
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  containerExclam: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  rectangleContainer: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    marginTop: 15,
    height: '30%',
    width: '95%',
    borderWidth: 2,
    borderColor: '#fff',
    backgroundColor: 'transparent',
    borderRadius: 3,
  },

  circle: {
    marginTop: -40,
    height: metrics.screenHeight / 3.5,
    width: metrics.screenWidth / 2.5,
    borderRadius: metrics.screenWidth / 2.5,
    borderColor: '#fff',
    borderWidth: 2,
    padding: 15,
    alignItems: 'center',
    bottom: 15,
  },
  rectangleKtp: {
    height: metrics.screenHeight / 6.5,
    width: metrics.screenWidth / 2.5,
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 5,
    marginTop: metrics.doubleBaseMargin * 2,
    marginBottom: 16,
  },
  containerPreview: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  camContainer: {
    width: '100%',
    backgroundColor: 'white',
  },
  previewContainer: {
    width: '100%',
    height: '100%',
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 15,
  },
  tagButton: {
    borderRadius: 35,
    backgroundColor: 'grey',
    padding: 15,
    alignItems: 'center',
    alignSelf: 'center',
  },
  flashButton: {
    borderRadius: 35,
    padding: 15,
    alignItems: 'center',
  },
  exclamButton: {
    backgroundColor: 'transparent',
    padding: 15,
    alignItems: 'center',
    bottom: 25,
    marginRight: 10,
  },
  cameraImgPreview: {
    flex: 1,
    resizeMode: 'cover',
  },
  uploadButton: {
    backgroundColor: Colors.mainBlue,
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    width: metrics.screenWidth / 2,
  },
  retryButton: {
    backgroundColor: '#C0C0C0',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    width: metrics.screenWidth / 2,
  },
  textInput: {
    color: 'white',
    fontSize: 20,
    marginTop: metrics.smallMargin,
    marginBottom: metrics.smallMargin,
  },
  imageKtp: {
    height: metrics.screenHeight / 3,
    width: metrics.screenWidth - 2 * metrics.doubleBaseMargin,
    marginLeft: metrics.baseMargin,
    marginRight: metrics.baseMargin,
  },
  containerSimulation: {
    backgroundColor: '#F4F4F4',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  okButton: {
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 0,
    width: metrics.screenWidth,
    height: 65,
  },
  okText: {
    fontSize: 22,
    color: 'white',
  },
  placeText: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    position: 'absolute',
    top: 25,
    margin: 15,
  },
  placeTextKtp: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});
