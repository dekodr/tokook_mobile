import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  PermissionsAndroid,
  Image,
  Text,
  StatusBar,
  Alert,
  SafeAreaView,
} from 'react-native';
import ImageResizer from 'react-native-image-resizer';

import * as c from '../../../Components';
import CameraActions from './../../../Redux/CameraRedux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';

import {RNCamera} from 'react-native-camera';
import styles from './styles';
import {ActivityIndicator} from 'react-native-paper';
import {Colors} from '../../../Themes';

console.disableYellowBox = true;
const CameraScreen = props => {
  const {dispatch, navigation, previewKtp, previewKtpSelfie} = props;
  const [flash, setFlash] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isFlip, setIsFlip] = useState(true);
  const camera = useRef(RNCamera);
  const getParams = navigation.getParam('params');

  async function takePicture() {
    setLoading(true);
    if (camera) {
      try {
        const options = {
          forceUpOrientation: true,
          fixOrientation: true,
          quality: 0.4,
          base64: false,
        };
        await camera.current.takePictureAsync(options).then(data => {
          ImageResizer.createResizedImage(data.uri, 1300, 1300, 'PNG', 100, 0)
            .then(({uri}) => {
              const dataPost = {data: uri, type: getParams.type};
              dispatch(CameraActions.takePictureKtpRequest(dataPost));
            })
            .catch(err => {
              console.log(err);
              return Alert.alert(
                'Unable to resize the photo',
                'Check the console for full the error message',
              );
            });
          setLoading(false);
        });
      } catch (error) {}
    } else {
      dispatch(
        CameraActions.cameraTakePictureFailed(
          'Error - Camera instance not found',
        ),
      );
    }
  }

  const retake = () => {
    if (getParams.type === 1) {
      dispatch(CameraActions.takePictureKtpReset());
    } else {
      dispatch(CameraActions.takePictureKtpSelfieReset());
    }
  };
  //   componentDidMount() {
  //     this.props.dispatch(CameraActions.cameraResetState());
  //   }

  //   compress = async imageUri => {
  //     // Untuk melakukan compress image
  //     const resultCompress = await CompressImage.createCustomCompressedImage(
  //       imageUri,
  //       "Compress/Images",
  //       2000,
  //       2000,
  //       80
  //     );
  //     return resultCompress;
  //   };

  const changeFlashMode = () => {
    setFlash(!flash);
  };
  const goBack = () => {
    navigation.goBack(null);
  };

  const base64PreviewKtpImage = previewKtp ? previewKtp : null;
  const base64PreviewKtpSelfieImage = previewKtpSelfie
    ? previewKtpSelfie
    : null;

  const viewPreview = (
    <View style={styles.previewContainer}>
      <Image
        resizeMode="stretch"
        style={styles.cameraImgPreview}
        source={{
          isStatic: true,
          uri:
            getParams.type === 1
              ? base64PreviewKtpImage
              : base64PreviewKtpSelfieImage,
        }}
      />
      <TouchableOpacity
        onPress={() => goBack()}
        style={{
          position: 'absolute',
          top: 25,
          left: 5,
          right: 0,
          flex: 1,
          bottom: 0,
        }}>
        <Icon name={'chevron-left'} size={35} color={'white'} />
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity style={styles.retryButton} onPress={() => retake()}>
          <Text style={styles.textInput}>ULANGI</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.uploadButton}
          onPress={() => {
            navigation.goBack(null);
          }}>
          <Text style={styles.textInput}>KONFIRMASI</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const viewCameraCapture = (
    <View style={{width: '100%', height: '100%'}}>
      <RNCamera
        ref={camera}
        flashMode={
          flash
            ? RNCamera.Constants.FlashMode.torch
            : RNCamera.Constants.FlashMode.off
        }
        style={{height: '100%', width: '100%'}}
        playSoundOnCapture={false}
        //Menggunakan kamera depan untuk foto selfie FO
        type={getParams.type == 2 && isFlip ? 'front' : 'back'}
        mirror={getParams.type == 2 && isFlip ? true : false}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        {getParams.type === 1 ? (
          <View style={styles.rectangleContainer} pointerEvents={'none'}>
            <View style={styles.rectangle}>
              <View>
                <Text style={styles.placeTextKtp}>
                  {'Posisikan KTP anda seperti ini'}
                </Text>
              </View>
            </View>
            <View></View>
          </View>
        ) : getParams.type === 2 ? (
          <View style={styles.rectangleContainer} pointerEvents={'none'}>
            <Text style={styles.placeText}>Posisikan wajah dan KTP anda</Text>
            <Text style={[{marginTop: 40}, styles.placeText]}>seperti ini</Text>

            <View style={styles.circle} />
            <View style={styles.rectangle} />
          </View>
        ) : (
          <View />
        )}
      </RNCamera>
      {getParams.type == 2 && (
        <TouchableOpacity
          onPress={() => {
            console.log('cek123', isFlip);
            setIsFlip(!isFlip);
          }}
          delayPressIn={0}
          style={{
            width: '15%',
            height: 100,
            position: 'absolute',
            top: 0,
            right: 0,
            top: 15,
            flexDirection: 'row',
          }}>
          <Icon
            name={!isFlip ? 'camera-rear' : 'camera-front'}
            size={35}
            color={Colors.mainBlue}
          />

          {/* <View
            style={{
              position: 'absolute',
              width: '50%',
              height: '40%',
              backgroundColor: isFlip ? 'white' : Colors.mainBlue,
              left: 12,
            }}></View>
          <View
            style={{
              width: '50%',
              height: '50%',
              backgroundColor: isFlip ? Colors.mainBlue : 'white',
            }}></View> */}
        </TouchableOpacity>
      )}

      <TouchableOpacity
        onPress={() => goBack()}
        style={{position: 'absolute', top: 25, left: 5}}>
        <Icon name={'chevron-left'} size={35} color={'white'} />
      </TouchableOpacity>
      {isLoading && (
        <View
          style={{
            position: 'absolute',
            top: 325,
            bottom: 0,
            left: 0,
            right: 0,
          }}>
          <ActivityIndicator color={Colors.mainBlue} size={'large'} />
          {/* <c.Loading /> */}
        </View>
      )}
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          alignSelf: 'center',
        }}>
        <TouchableOpacity
          style={styles.tagButton}
          disabled={isLoading ? true : false}
          onPress={() => takePicture()}>
          <Icon name="photo-camera" size={40} color="#FFFFFF" />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.flashButton}
          disabled={isLoading ? true : false}
          onPress={() => changeFlashMode()}>
          <Icon
            name={'flash-' + (flash ? 'on' : 'off')}
            size={24}
            color="#FFFFFF"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
  const resultUI =
    getParams.type === 1 ? base64PreviewKtpImage : base64PreviewKtpSelfieImage;
  return resultUI ? viewPreview : viewCameraCapture;
};

const mapStateToProps = state => {
  return {
    previewKtp: state.camera.ktp.ktpData ? state.camera.ktp.ktpData : '',
    previewKtpSelfie: state.camera.ktp.ktpSelfieData
      ? state.camera.ktp.ktpSelfieData
      : '',
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CameraScreen);
