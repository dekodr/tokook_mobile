import {StyleSheet} from 'react-native';
import {Colors} from '../../../Themes';

export default StyleSheet.create({
  viewinputwaNumber: {
    height: 40,
    width: '100%',
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderBottomWidth: 0.5,
    borderBottomColor: '#979797',
    marginTop: 8,
    marginBottom: 8,
  },
  txtinputwaNumber: {
    width: '70%',
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 0.9,
    color: '#000',
    marginLeft: 5,
  },
  txtbottomwaNumber: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 10,
    letterSpacing: 0.7,
  },
  txtkirimOtp: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    letterSpacing: 0.9,
    color: '#1a69d5',
  },
  txtresendOtp: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    letterSpacing: 0.9,
    color: '#1a69d5',
  },
  txttimeLeft: {
    fontFamily: 'NotoSans-ExtraBold',
    fontSize: 20,
    letterSpacing: 0.5,
    color: '#1a69d5',
  },
  mainContainer: {
    backgroundColor: Colors.mainBlue,
    height: '100%',
    width: '100%',
    paddingTop: 25,
    paddingRight: 25,
    paddingLeft: 25,
    flex: 1,
  },
  cellStyle: {
    borderBottomWidth: 3,
    borderColor: '#1a69d5',
    marginRight: 12,
    marginLeft: 12,
  },
  cellStyleFocused: {
    borderColor: Colors.whiteLight,
  },
  txtCellStyle: {
    fontSize: 24,
    color: '#1a69d5',
  },
  textStyleFocused: {
    fontSize: 24,
    color: '#1a69d5',
  },
  itemCenter: {
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
