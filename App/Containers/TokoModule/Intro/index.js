import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Linking,
  TextInput,
  BackHandler,
  Keyboard,
  KeyboardAvoidingView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  Container,
  Tab,
  Tabs,
  TabHeading,
  CheckBox,
  Button,
  Spinner,
  Content,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import AuthActions from './../../../Redux/AuthenticationRedux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Config from 'react-native-config';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import * as c from '../../../Components';
import RBSheet from 'react-native-raw-bottom-sheet';
import {Images} from '../../../Themes';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Product from '../Product';
import Category from '../Category';
import Foundation from 'react-native-vector-icons/Foundation';
import Admin from '../Admin/Intro';
import Order from '../Order/Intro';
import {connect} from 'react-redux';
import TokoActions from '../../../Redux/TokoRedux';
import * as func from '../../../Helpers/Utils';
import DropshipIntro from '../Dropship/Intro';
import FirebaseTypes from './../../../Redux/FirebaseRedux';
import AsyncStorage from '@react-native-community/async-storage';
import GlobalActions from './../../../Redux/GlobalRedux';
import styles from './styles';
const errorLinkMessage = 'link tidak valid';

function index(props) {
  const {
    dispatch,
    getToko,
    getOtpWa,
    userData,
    fcmData,
    isCreateTokoSuccess,
    tokoData,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    tokoAddressData,
    tokoAddress,
  } = props;
  const initialValidation = {
    isProvinceEmpty: false,
    isCityEmpty: false,
    isDistrictEmpty: false,
    isPostalCodeEmpty: false,
    isAddressEmpty: false,
    isDetailAddressEmpty: false,
  };

  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialValidation,
  );
  const [isAddedAddress, setIsAddedAddress] = useState(true);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [editSelected, setEditSelected] = useState(null);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState('');
  const [detailAddressValue, setDetailAddressValue] = useState('');
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [token, setToken] = useState('');
  const [timeLeft, setTimeLeft] = useState(0);
  const [errMsg, setErrMsg] = useState(null);
  const [code, setCode] = useState('');
  const [wanumber, setwaNumber] = useState('');
  const [otpWa, setOtpWa] = useState(
    props.getOtpWa ? (props.getOtpWa.data ? props.getOtpWa.data : {}) : {},
  );
  const [toko, setToko] = useState(
    props.getToko
      ? props.getToko.listToko
        ? props.getToko.listToko.length
          ? props.getToko.listToko[0]
          : {}
        : {}
      : {},
  );
  const [step, setStep] = useState(1);
  const [loadingStep, setloadingStep] = useState(false);
  const [formToko, setFormToko] = useState({
    tncCheck: true,
    link: null,
    name: null,
    whatsapp: null,
    businessTypeSelected: {
      id: 12,
      icon:
        'https://payok-toko-bucket.s3-ap-southeast-1.amazonaws.com/business-type/Others.png',
    },
    inputOtherBusinessType: null,
    tokopedia: null,
    shopee: null,
    blibli: null,
    instagram: null,
    facebook: null,
    line: null,
    youtube: null,
  });
  const [tabIndex, setTabIndex] = useState(0);
  const [isLocked, setIsLocked] = useState(false);
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
    isTokpedLinkInvalid: false,
    isShopeeLinkInvalid: false,
    isBlibliLinkInvalid: false,
    isInstagramLinkInvalid: false,
    isFacebookLinkInvalid: false,
    isLineLinkInvalid: false,
    isYoutubeLinkInvalid: false,
  });
  const [addTokoSuccess, setAddTokoSuccess] = useState(null);
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const refRBSheet = useRef();
  const refInputBusinessType = useRef();
  const message = `Hi, ${toko.name} baru membuat website baru nih. Lihat katalog di website nya dan pesan barang yang menarik dilink ini yah ${Config.TOKO_NAME}${toko.link}`;
  const tokoId = getToko
    ? getToko
      ? getToko
        ? getToko.length !== 0
          ? getToko.listToko.length !== 0
            ? getToko.listToko[0].id
            : 0
          : 0
        : 0
      : 0
    : 0;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;

  console.log('refInputBusinessType', refInputBusinessType);
  useEffect(() => {
    if (tokoId) {
      dispatch(AuthActions.loginOtpAfterSuccess());

      dispatch(TokoActions.getDetailTokoRequest(tokoId));
      dispatch(TokoActions.getProvinceRequest());
      dispatch(TokoActions.getBankRequest());
      dispatch(TokoActions.getTokoFinancialRequest(tokoId));
    }
  }, [tokoId]);
  // useEffect(() => {
  //   if (userData.success) {
  //     //props.dispatch(AuthActions.getUserRequest(props.access_token));
  //     if (userData.data.is_phone_verified && step < 2) {
  //       setStep(step + 1);
  //     } else if (tokoData && step < 3) {
  //       setStep(step + 1);
  //     }
  //   }
  // });
  useEffect(() => {
    if (fcmData != null) {
      if (fcmData.purpose === '3') {
        setTabIndex(2);
        dispatch(FirebaseTypes.fcmReset());
        dispatch(TokoActions.getTokoOrderRequest(tokoId));
      }
    }
  }, [fcmData]);

  useEffect(() => {
    props.dispatch(TokoActions.getBusinessTypeRequest(props.access_token));
    props.dispatch(TokoActions.getTokoRequest());
    dispatch(TokoActions.getSupplierRequest());
  }, []);
  useEffect(() => {
    if (getOtpWa.success) {
      const {token} = getOtpWa.data.data;
      setTimeLeft(30);
      setToken(token);
    }
  }, [getOtpWa.success]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }
    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // clear interval on re-render to avoid memory leaks

    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  useEffect(() => {
    dispatch(TokoActions.getDetailTokoRequest(tokoId));
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    dispatch(TokoActions.getProductTokoRequest(tokoId));
  }, [tokoId]);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, [tabIndex]);
  const refreshFirebaseToken = () => {
    (async () => {
      AsyncStorage.getItem('isLogin').then((value) => {
        if (value) {
          AsyncStorage.getItem('bearerToken').then((bearer) => {
            AsyncStorage.getItem('fcmToken').then((value) => {
              dispatch(GlobalActions.saveFirebaseTokenRequest(value));
            });
          });
        }
      });
    })();
  };
  useEffect(() => {
    setToko(
      props.getToko
        ? props.getToko.listToko
          ? props.getToko.listToko.length
            ? props.getToko.listToko[0]
            : {}
          : {}
        : {},
    );
  }, [props.getToko]);
  // useEffect(() => {
  //   if (provinceData != null) {
  //     setProvinceValue(provinceData.provincies[0]);
  //     props.dispatch(
  //       TokoActions.getCityRequest({id: provinceData.provincies[0].id}),
  //     );
  //   }
  // }, [provinceData]);
  //temporary
  // useEffect(() => {
  //   if (!isTokoAvailable) {
  //     refRBSheet.current.open();
  //   }
  // }, [isTokoAvailable]);

  useEffect(() => {
    setFormToko({...formToko, isBusinessTypeEmpty: false});
    if (step === 2 && formToko.businessTypeSelected.id === 12) {
      refInputBusinessType.current.focus();
    }
  }, [formToko.businessTypeSelected.id]);

  useEffect(() => {
    if (formToko.name) {
      setTokoValidation({...tokoValidation, isNameTokoEmpty: false});
    }
    if (formToko.link) {
      setTokoValidation({...tokoValidation, isLinkTokoEmpty: false});
    }
    if (formToko.whatsapp) {
      setTokoValidation({...tokoValidation, isWhatsappEmpty: false});
    }
    if (tokoValidation.isTokpedLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      });
    }
    if (tokoValidation.isShopeeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      });
    }
    if (tokoValidation.isBlibliLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      });
    }
    if (tokoValidation.isInstagramLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      });
    }
    if (tokoValidation.isFacebookLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      });
    }
    if (tokoValidation.isLineLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isLineLinkInvalid: !isLinkValid(formToko.line),
      });
    }
    if (tokoValidation.isYoutubeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
    }
  }, [formToko]);

  const isLinkValid = (link) => {
    if (typeof link === 'string' && link.length) {
      if (
        link.toLowerCase().includes('http') ||
        link.toLowerCase().includes('www')
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  const stepBack = () => {
    setStep(step - 1);
  };
  const handleBackButtonClick = () => {
    if (tabIndex !== 0) {
      setTabIndex(0);
      return true;
    }
  };
  const searchingPostalCode = (searchText) => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerCity(false);
        setOpenPickerProvince(false);
        setOpenPickerDistrict(false);

        setOpenPickerPostalCode(true);
        let filteredData = postalCodeData.postalCode.filter(function (item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setOpenPickerCity(false);
      setOpenPickerProvince(false);
      setOpenPickerDistrict(false);

      setPostalCodeValue(searchText);
    }
  };
  const handleAddAdressSubmit = () => {};
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() =>
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          )
        }
        style={{
          borderWidth: 0.1,
          borderColor: 'transparent',
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode ? item.postal_code : item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          props.dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const stepNext = () => {
    setTokoValidation({
      isNameTokoEmpty: !formToko.name ? true : false,
      isLinkTokoEmpty: !formToko.link ? true : false,
      //isWhatsappEmpty: !formToko.whatsapp ? true : false,
      isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      isLineLinkInvalid: !isLinkValid(formToko.line),
      isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
    });

    const {
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
      tncCheck,
    } = formToko;

    const data = {
      business_type_id: businessTypeSelected.id,
      name: name,
      link: link,
      whatsapp: whatsapp,
      instagram: instagram,
      facebook: facebook,
      line: line,
      youtube: youtube,
      tokopedia: tokopedia,
      shopee: shopee,
      blibli: blibli,
    };
    switch (step) {
      case 1: {
        const data = {
          item: {
            phone: wanumber,
            code: code,
            token: token,
          },
          func: {
            reloadData: () => {
              setloadingStep(false);
              props.dispatch(AuthActions.getUserRequest(props.access_token));
              setCode('');
              setwaNumber('');
              if (!isVerified) {
                if (!tokoData) {
                  setStep(2);
                } else if (!tokoAddress) {
                  setStep(3);
                } else {
                  refRBSheet.current.close();
                }
              }
            },
            reloadFailed: () => {
              setloadingStep(false);
            },
          },
        };
        setloadingStep(true);
        props.dispatch(AuthActions.whatsappVerificationRequest(data));
      }
      case 2: {
        // setStep(3);
        //Validate first
        if (name && link) {
          const data = {
            item: {
              name: name,
              link: link.toLowerCase(),
              whatsapp: wanumber,
            },
            func: {
              reloadData: () => {
                setloadingStep(false);
                props.dispatch(TokoActions.getDetailTokoRequest(tokoId));
                props.dispatch(TokoActions.getProvinceRequest());
                if (!isVerified) {
                  if (!tokoAddress) {
                    setStep(3);
                  } else {
                    refRBSheet.current.close();
                  }
                }
              },
              reloadFailed: () => {
                setloadingStep(false);
              },
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.tokoPostRequest(data));
        }
        break;
      }
      case 3: {
        if (provinceValue === null || provinceValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: true,
          });
        } else if (cityValue === null || cityValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: true,
          });
        } else if (districtValue === null || districtValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: true,
          });
        } else if (postalCodeValue === null || postalCodeValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isPostalCodeEmpty: true,
          });
        } else if (addressValue === null || addressValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isAddressEmpty: true,
          });
        } else {
          const postData = {
            tokoId: tokoId,
            func: {
              rb: refRBSheet,
            },
            purpose: 1,
            addressId: addressIdValue,
            purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
            detail: {
              province_id: provinceValue.id,
              city_id: cityValue.id,
              district_id: districtValue.id,
              postal_code: postalCodeValue,
              address: addressValue.concat(detailAddressValue),
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.postTokoAddressRequest(postData));
        }
        break;
      }

      default:
        break;
    }
  };

  const createMerchant = () => {
    const {
      tncCheck,
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
    } = formToko;

    const {
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
      isTokpedLinkInvalid,
      isBusinessTypeEmpty,
      isShopeeLinkInvalid,
      isBlibliLinkInvalid,
      isInstagramLinkInvalid,
      isFacebookLinkInvalid,
      isLineLinkInvalid,
      isYoutubeLinkInvalid,
    } = tokoValidation;
    return step == 1 ? (
      <View>
        <View
          style={[
            c.Styles.flexRow,
            c.Styles.alignItemsCenter,
            {marginTop: 20},
          ]}>
          <Text style={c.Styles.txtverifWA}>Verifikasi Whatsapp</Text>
        </View>
        <View style={styles.justifyCenter}>
          <View style={styles.viewinputwaNumber}>
            <View style={c.Styles.flexRow}>
              <View style={c.Styles.justifyCenter}>
                <c.Image
                  source={Images.waIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <TextInput
                keyboardType={'number-pad'}
                style={styles.txtinputwaNumber}
                onChangeText={(text) => {
                  setwaNumber(text);
                  setErrMsg(null);
                }}
                placeholderTextColor={'#bdbdbd'}
                placeholder={'Nomor Whatsapp'}
                value={wanumber}
              />
              <TouchableOpacity
                style={[c.Styles.justifyCenter]}
                onPress={timeLeft > 0 ? null : handlegetOtp}
                disabled={props.isFetchingOtp}>
                {getOtpWa.fetching ? (
                  <View
                    style={{
                      width: 50,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Spinner size={18} color={c.Colors.mainBlue} />
                  </View>
                ) : (
                  <Text
                    style={[
                      styles.txtkirimOtp,
                      c.Styles.alignCenter,
                      {opacity: timeLeft === 0 ? 1 : 0.4},
                    ]}>
                    Kirim OTP
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text style={styles.txtbottomwaNumber}>
              Agar pembeli bisa menghubungi kamu
            </Text>
          </View>
          <View style={[c.Styles.alignCenter, {marginTop: '10%', height: 100}]}>
            {getOtpWa.success ? (
              <View>
                <c.PinVerify
                  autoFocus={true}
                  cellStyle={styles.cellStyle}
                  cellStyleFocused={styles.cellStyleFocused}
                  textStyle={styles.txtCellStyle}
                  textStyleFocused={styles.textStyleFocused}
                  value={code}
                  editable={getOtpWa.success ? true : false}
                  onTextChange={(code) => setCode(code)}
                />
                <View
                  style={[
                    ,
                    {
                      marginTop: '10%',
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    },
                  ]}>
                  <TouchableOpacity
                    style={c.Styles.justifyCenter}
                    onPress={handlegetOtp}
                    disabled={timeLeft !== 0 ? true : false}>
                    <Text
                      style={[
                        styles.txtresendOtp,
                        {opacity: timeLeft === 0 ? 1 : 0.4},
                      ]}>
                      {' '}
                      Kirim Ulang OTP
                    </Text>
                  </TouchableOpacity>

                  <Text style={styles.txttimeLeft}>
                    00:{timeLeft < 10 ? '0' : ''}
                    {timeLeft}
                  </Text>
                </View>
              </View>
            ) : null}
          </View>

          <View
            style={{
              top: 30,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              //onPress={handleResendOtp}
              disabled={props.isFetchingOtp}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  opacity: timeLeft === 0 ? 1 : 0.4,
                  color: 'white',
                  marginTop: 2,
                }}>
                {' '}
                Kirim Ulang
              </Text>
            </TouchableOpacity>

            <Text
              style={{
                marginLeft: 6,
                fontFamily: 'NotoSans-ExtraBold',
                color: 'white',
                fontSize: 15,
                opacity: 1,
              }}>
              00:{timeLeft < 10 ? '0' : ''}
              {timeLeft}
            </Text>
          </View>
        </View>
      </View>
    ) : step == 2 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <Text style={c.Styles.txtCreateMerchant}>Buka Toko Online</Text>
        </View>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={c.Styles.viewTokoStepContent}
          showsVerticalScrollIndicator={false}>
          <Text style={c.Styles.lineHeightTwenty}>
            Fitur ini dibuat untuk kamu yang ingin memiliki website usaha kamu
            sendiri. Bagikan dan tingkatkan penjualan kamu.
          </Text>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={c.Styles.txtInputWithIcon}>
              <View style={c.Styles.marginRightTen}>
                <c.Image source={Images.toko} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  autoCapitalize="words"
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Nama Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      name: text,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: text ? false : true,
                    });
                  }}
                  value={name}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: name ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              Nama toko di website kamu
            </Text>
            {isNameTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Nama toko harus diisi
              </Text>
            )}
          </View>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image source={Images.at} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Link Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: text ? false : true,
                    });
                  }}
                  value={link}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: link ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              {Config.TOKO_NAME}
              <Text
                style={{
                  fontWeight: '600',
                  fontSize: 14,
                  letterSpacing: 1,
                  textTransform: 'lowercase',
                }}>
                {!link ? 'link-toko' : link}
              </Text>
            </Text>
            {isLinkTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Link toko harus diisi
              </Text>
            )}
          </View>
        </ScrollView>
      </View>
    ) : step == 3 ? (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'handled'}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 14,
            letterSpacing: 1,
            marginTop: 20,
          }}>
          Alamat Toko
        </Text>
        {tokoAddressData === null && !isAddedAddress && (
          <TouchableOpacity
            onPress={() => setIsAddedAddress(true)}
            delayPressIn={0}
            style={{
              marginTop: 25,
              justifyContent: 'center',
              alignSelf: 'center',
              width: '99%',
              height: 65,
              backgroundColor: '#ffffff',
              marginLeft: 0,
              marginRight: 0,
              alignItems: 'center',
              paddingLeft: 25,
              paddingRight: 25,

              borderColor: c.Colors.gray,
              borderWidth: 0.58,
            }}>
            <TouchableOpacity
              delayPressIn={0}
              style={{
                marginTop: 8,
                width: 25,
                height: 25,
                borderWidth: 1,
                borderColor: '#a9a9a9',
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <MaterialCommunityIcons name="plus" size={20} color="#a9a9a9" />
            </TouchableOpacity>
            <Text
              style={{
                marginTop: 8,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1.5,
                color: '#7b7b7b',
              }}>
              Tambah Alamat
            </Text>
          </TouchableOpacity>
        )}
        {isAddedAddress && (
          <View
            style={{
              width: '100%',
              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && cityData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && districtData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  keyboardShouldPersistTaps={'handled'}
                  nestedScrollEnabled
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
        {isUpdatedAddress && (
          <View
            style={{
              width: '100%',

              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            <TouchableOpacity
              onPress={() => setIsUpdatedAddress(false)}
              delayPressIn={0}
              style={{position: 'absolute', right: 8, top: 8}}>
              <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
            </TouchableOpacity>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && provinceData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && (
              <FlatList
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  nestedScrollEnabled
                  style={{height: 300}}
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
      </ScrollView>
    ) : null;
  };
  const lockTab = (isLocked) => {
    setIsLocked(isLocked);
  };

  const refreshToken = () =>
    (async () => {
      AsyncStorage.getItem('isLogin').then((value) => {
        if (value) {
          AsyncStorage.getItem('bearerToken').then((bearer) => {
            const bearerToken = JSON.parse(bearer);

            const passingTokenValue = bearerToken;
            dispatch(
              GlobalActions.setTokenApiGloballyRequest(passingTokenValue),
            );
            dispatch(
              GlobalActions.refreshTokenApiGloballyRequest(passingTokenValue),
            );
          });
        } else {
          dispatch(NavigationActions.navigate({routeName: 'LoginScreen'}));
        }
      });
    })();
  const handlegetOtp = () => {
    const data = {
      phone: wanumber,
      source: 'phone_verification',
      type: 'whatsapp',
    };
    Keyboard.dismiss();
    props.dispatch(AuthActions.getOtpPostRequest(data));
  };
  const _onChangeTab = (c) => {
    setTabIndex(c.i);
    switch (c.i) {
      case 0: {
        dispatch(TokoActions.getSupplierRequest());
        refreshFirebaseToken();
        dispatch(AuthActions.loginOtpAfterSuccess());

        break;
      }
      case 1: {
        dispatch(TokoActions.getProductTokoRequest(tokoId));
        dispatch(AuthActions.getUserRequest(props.access_token));
        refreshFirebaseToken();

        break;
      }
      // case 2: {
      //   dispatch(TokoActions.getCategoryTokoRequest(tokoId));
      //   dispatch(TokoActions.getTokoAddressRequest(tokoId));
      //   dispatch(TokoActions.getProductTokoRequest(tokoId));
      //   dispatch(AuthActions.getUserRequest(props.access_token));
      //   refreshFirebaseToken();

      //   break;
      // }
      case 2: {
        refreshFirebaseToken();
        dispatch(TokoActions.getTokoOrderRequest(tokoId));
        dispatch(TokoActions.getTokoAddressRequest(tokoId));
        dispatch(TokoActions.getTokoBankRequest(tokoId));
        dispatch(TokoActions.getBankRequest());
        dispatch(TokoActions.getProvinceRequest());
        dispatch(TokoActions.getProductTokoRequest(tokoId));
        dispatch(AuthActions.getUserRequest(props.access_token));

        break;
      }
      case 3: {
        refreshFirebaseToken();
        dispatch(TokoActions.getTokoOrderRequest(tokoId));

        dispatch(TokoActions.getDetailTokoRequest(tokoId));
        dispatch(AuthActions.getUserRequest(props.access_token));
        dispatch(TokoActions.getTokoFinancialRequest(tokoId));
        dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));

        break;
      }
      default:
        break;
    }
  };
  const handleCleanState = () => {
    // props.dispatch(TokoActions.postTokoBankReset());
    props.dispatch(TokoActions.postTokoAddressReset());

    // props.dispatch(TokoActions.getCityReset());
    setCityValue(null);
    setPostalCodeValue(null);
    setProvinceValue(null);
    setCityValue(null);
    setOpenPickerProvince(false);
    setOpenPickerCity(false);
    setOpenPickerDistrict(false);
    setOpenPickerPostalCode(false);
    setDistrictValue(null);
    setDetailAddressValue(null);
    setAddressIdValue(0);
    setAddressValue(null);
    // setIsAddedAddress(false), setIsUpdatedAddress(false);
    setAddedAddressFormValidation(initialValidation);
    // setIsAddedBank(false);
    // setOpenPickerBank(false);
    // // setBankNameValue(null);
    // setBankOwnerNameValue(null);
    // setBankIndex(-1);
    // setBankValue(null);
    // // setBankNameValue(null)
    // setIsUpdatedBank(false);
  };
  return (
    <Container>
      <c.Header
        c={c}
        iconright={true}
        materialicon={true}
        iconrightName="share-variant"
        iconrightStyle={{
          color: 'white',
          padding: 10,
          marginRight: 10,
        }}
        viewbodyStyle={[{flex: 5}, c.Styles.alignItemsEnd, c.Styles.justifyEnd]}
        iconrightSize={20}
        onPressRightIcon={() => {
          //kylie
          isTokoAvailable
            ? func.handleShare({message})
            : refRBSheet.current.open();
        }}
        style={[c.Styles.viewBgmainBlue, {paddingLeft: 25, height: 60}]}
        androidStatusBarColor={c.Colors.mainBlue}
        titleStyle={{letterSpacing: 1, fontSize: 19, alignSelf: 'flex-start'}}
        txtTitle={
          toko.name === null || tokoData === null ? 'Toko OK' : toko.name
        }
      />
      <Tabs
        style={{overflow: 'hidden'}}
        locked={isLocked}
        onChangeTab={_onChangeTab}
        page={tabIndex}
        tabBarUnderlineStyle={{
          bottom: -17,
          // marginHorizontal: Dimensions.get('window').width / 12,
          marginHorizontal: Dimensions.get('window').width / 9,

          // marginLeft: 45,
          borderLeftWidth: 7,
          borderLeftColor: 'transparent',
          borderRightWidth: 7,
          borderRightColor: 'transparent',
          borderBottomWidth: 10,
          borderBottomColor: '#fff',
          width: 0,
          height: 0,
          borderStyle: 'solid',
          backgroundColor: 'transparent',
        }}
        tabContainerStyle={{marginBottom: 20}}>
        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: c.Colors.mainBlue,
                flexDirection: 'column',
                height: 65,
              }}>
              <AntDesign name="shoppingcart" size={24} color="white" />
              <Text style={{color: '#fff', marginVertical: 5, fontSize: 13}}>
                Dropship
              </Text>
            </TabHeading>
          }>
          <DropshipIntro setStep={setStep} navigation={props.navigation} />
        </Tab>
        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: c.Colors.mainBlue,
                flexDirection: 'column',
                height: 65,
              }}>
              <AntDesign name="book" size={23} color="white" />
              <Text style={{color: '#fff', marginVertical: 5, fontSize: 13}}>
                Produk
              </Text>
            </TabHeading>
          }>
          <Product
            setStep={setStep}
            step={step}
            bottomSheetRef={refRBSheet}
            setTabIndex={setTabIndex}
            lockTab={lockTab}
            navigation={props.navigation}
          />
        </Tab>
        {/* <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: c.Colors.mainBlue,
                flexDirection: 'column',
                height: 65,
              }}>
              <AntDesign name="bars" size={24} color="white" />
              <Text style={{color: '#fff', marginVertical: 5, fontSize: 13}}>
                Kategori
              </Text>
            </TabHeading>
          }>
          <Category setStep={setStep} step={step} bottomSheetRef={refRBSheet} />
        </Tab>
        */}
        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: c.Colors.mainBlue,
                flexDirection: 'column',
                height: 65,
              }}>
              <Entypo size={24} name="text-document" color="#fff" />

              <Text style={{color: '#fff', marginVertical: 5, fontSize: 13}}>
                Order
              </Text>
            </TabHeading>
          }>
          <Order
            bottomSheetRef={refRBSheet}
            setTabIndex={setTabIndex}
            navigation={props.navigation}
            setStep={setStep}
            step={step}
          />
        </Tab>

        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: c.Colors.mainBlue,
                flexDirection: 'column',
                height: 65,
              }}>
              <AntDesign name="setting" size={24} color="white" />
              <Text style={{color: '#fff', marginVertical: 5, fontSize: 13}}>
                Admin
              </Text>
            </TabHeading>
          }>
          <Admin
            navigation={props.navigation}
            setStep={setStep}
            step={step}
            bottomSheetRef={refRBSheet}
          />
        </Tab>
      </Tabs>
      <RBSheet
        ref={refRBSheet}
        onClose={handleCleanState}
        closeOnDragDown={true}
        // onClose={() => {
        //   if (!isTokoAvailable) {
        //     refRBSheet.current.open();
        //   } else {
        //     // refRBSheet.current.close();
        //   }
        // }}
        keyboardAvoidingViewEnabled={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.8}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: 20,
            marginBottom: 50,
            height: '100%',
            justifyContent: 'space-between',
          }}>
          <KeyboardAvoidingView
            style={{flex: 1, flexDirection: 'column'}}
            keyboardVerticalOffset={-15}
            behavior="padding"
            enabled>
            {createMerchant()}
          </KeyboardAvoidingView>
          {step !== 4 ? (
            <Button
              disabled={step === 1 && code.length < 4 ? true : false}
              onPress={stepNext}
              style={[
                c.Styles.btnPrimary,
                {
                  marginTop: '10%',
                  marginBottom: '10%',
                  backgroundColor:
                    step === 1 && code.length < 4
                      ? '#c9c6c6'
                      : c.Colors.mainBlue,
                },
              ]}>
              {loadingStep ? (
                <Spinner size={18} color="#fff" />
              ) : (
                <Text style={c.Styles.txtCreateMerchantBtn}>
                  {step === 1
                    ? 'Verifikasi'
                    : step === 2
                    ? 'Buat Toko'
                    : step === 3
                    ? 'Selesai'
                    : null}
                </Text>
              )}
            </Button>
          ) : null}
        </Content>
      </RBSheet>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mapStateToProps = (state) => {
  return {
    getOtpWa: state.auth.getOtpPostData,
    userData: state.auth.userData,

    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,

    // getTokenWa : state.auth.getOtpPostData.data ?
    fcmData: state.firebase.fcmData.data,
    businessType: state.toko.getBusinessTypeData.data,
    getToko: state.toko.tokoData.data,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    isCreateTokoSuccess: state.toko.tokoPostData.success,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoAddress:
      state.toko.tokoAddressData.data != null
        ? state.toko.tokoAddressData.data.listAddress
          ? state.toko.tokoAddressData.data.listAddress.length != 0
            ? true
            : false
          : false
        : false,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
