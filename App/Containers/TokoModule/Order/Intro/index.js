import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {withNavigationFocus} from 'react-navigation';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import Config from 'react-native-config';
import {Button, Spinner, Card, Container, Content} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from './../../../../Redux/TokoRedux';
import MarketplaceActions from './../../../../Redux/MarketPlaceRedux';
import GlobalActions from './../../../../Redux/GlobalRedux';
import styles from './styles';
import {Images, Colors} from './../../../../Themes/';
import * as func from './../../../../Helpers/Utils';

import * as c from './../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
//import styles from '../../Intro/styles';

// const dataOrder = [
//   {
//     date: '23 Oct 2020',
//     total_harga: 200000,
//     data: [
//       {
//         nama_barang: 'Ayam',
//         jumlah_pesanan: 12,
//         merchant_name: 'Ayam Bakar Mpo Ad',
//         harga: 100000,
//         status_pesanan: 1,
//       },
//       {
//         nama_barang: 'Bebek Madu',
//         jumlah_pesanan: 10,
//         merchant_name: 'Bebek Madu Rasa',
//         harga: 50000,
//         status_pesanan: 2,
//       },
//       {
//         nama_barang: 'Sate Ayam',
//         jumlah_pesanan: 2,
//         merchant_name: 'Ayam Bakar Mpo Ad',
//         harga: 100000,
//         status_pesanan: 3,
//       },
//     ],
//   },
//   {
//     date: '23 Nov 2020',
//     total_harga: 250000,

//     data: [
//       {
//         nama_barang: 'Ayam 1',
//         jumlah_pesanan: 12,
//         merchant_name: 'Ayam Bakar Mpo Ad 1',
//         harga: 100000,
//         status_pesanan: 1,
//       },
//       {
//         nama_barang: 'Bebek Madu 1',
//         jumlah_pesanan: 10,
//         merchant_name: 'Bebek Madu Rasa 1',
//         harga: 50000,
//         status_pesanan: 2,
//       },
//       {
//         nama_barang: 'Sate Ayam 1',
//         jumlah_pesanan: 2,
//         merchant_name: 'Ayam Bakar Mpo Ad 1',
//         harga: 100000,
//         status_pesanan: 1,
//       },
//     ],
//   },
//   {
//     date: '23 Des 2020',
//     total_harga: 300000,

//     data: [
//       {
//         nama_barang: 'Ayam 2',
//         jumlah_pesanan: 12,
//         merchant_name: 'Ayam Bakar Mpo Ad 2',
//         harga: 100000,
//         status_pesanan: 1,
//       },
//       {
//         nama_barang: 'Bebek Madu 2',
//         jumlah_pesanan: 10,
//         merchant_name: 'Bebek Madu Rasa 2',
//         harga: 50000,
//         status_pesanan: 2,
//       },
//       {
//         nama_barang: 'Sate Ayam 2',
//         jumlah_pesanan: 2,
//         merchant_name: 'Ayam Bakar Mpo Ad 2',
//         harga: 100000,
//         status_pesanan: 1,
//       },
//     ],
//   },
// ];
function index(props) {
  const {
    tokoId,
    tokoOrderData,
    tokoOrderPrimary,
    toko,
    navigation,
    dispatch,
    supplierData,
    bottomSheetRef,
    setTabIndex,
    setStep,
    step,
    isTokoOrderDataFetching,
    userData,
    tokoData,
    tokoAddressData,
    tokoAddress,
    isFocused,
    fetchingTokoOrder,
  } = props;
  const [refreshing, setRefreshing] = useState(false);
  const [orderData, setorderData] = useState([
    {
      orderTotal: 0,
      orderName: 'Semua Pesanan',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pesanan Baru',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pesanan Diterima',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pembayaran Diterima',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pesanan Dikirim',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pesanan Selesai',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pesanan Ditolak',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Pembayaran Ditolak',
      orderData: [],
    },
    {
      orderTotal: 0,
      orderName: 'Siap Dicairkan',
      orderData: [],
    },
  ]);
  const [dataOrderan, setdataOrderan] = useState([]);
  const [selectedOrder, setSelectedOrder] = useState('Semua Pesanan');
  const [loadingData, setloadingData] = useState(true);
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const _handleNavigation = (item) => {
    dispatch(TokoActions.getTokoOrderDetailReset());
    dispatch(
      TokoActions.getTokoOrderDetailRequest({
        tokoId: tokoId,
        transactionId: item.id,
      }),
    );
    navigation.navigate('OrderDetailScreen', {
      params: item,
    });
  };
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  useEffect(() => {
    if (tokoId !== null) {
      dispatch(TokoActions.getTokoOrderRequest(tokoId));
      dispatch(TokoActions.getTokoAddressRequest(tokoId));
      dispatch(TokoActions.getTokoBankRequest(tokoId));
      dispatch(TokoActions.getDetailTokoRequest(tokoId));
      dispatch(TokoActions.getProductTokoRequest(tokoId));
    }
    dispatch(TokoActions.getBankRequest());
    dispatch(TokoActions.getProvinceRequest());

    dispatch(TokoActions.getTokoRequest());
  }, [tokoId]);
  useEffect(() => {
    let dataFilteran = orderData.filter((v) => v.orderName === selectedOrder);
    dataFilteran = dataFilteran.flatMap((v) => v.orderData);
    setdataOrderan(dataFilteran);
  }, [selectedOrder]);
  useEffect(() => {
    if (tokoOrderPrimary.data !== null) {
      let dataAll = tokoOrderPrimary.data.transactions.filter((v) => v);
      let dataNewOrder = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 1,
      );
      let dataOrderReceived = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 2,
      );
      let dataPaymentReceived = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 3,
      );
      let dataOrderSent = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 4,
      );
      let dataOrderDone = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 5,
      );
      let dataOrderRejected = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 6,
      );
      let dataPaymentRejected = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 7,
      );
      let dataReadytoThaw = tokoOrderPrimary.data.transactions.filter(
        (v) => v.status === 14,
      );

      let data = [
        {
          orderTotal: dataAll.length,
          orderName: 'Semua Pesanan',
          orderData: dataAll,
        },
        {
          orderTotal: dataNewOrder.length,
          orderName: 'Pesanan Baru',
          orderData: dataNewOrder,
        },
        {
          orderTotal: dataOrderReceived.length,
          orderName: 'Pesanan Diterima',
          orderData: dataOrderReceived,
        },
        {
          orderTotal: dataPaymentReceived.length,
          orderName: 'Pembayaran Diterima',
          orderData: dataPaymentReceived,
        },
        {
          orderTotal: dataOrderSent.length,
          orderName: 'Pesanan Dikirim',
          orderData: dataOrderSent,
        },
        {
          orderTotal: dataOrderDone.length,
          orderName: 'Pesanan Selesai',
          orderData: dataOrderDone,
        },
        {
          orderTotal: dataOrderRejected.length,
          orderName: 'Pesanan Ditolak',
          orderData: dataOrderRejected,
        },
        {
          orderTotal: dataPaymentRejected.length,
          orderName: 'Pembayaran Ditolak',
          orderData: dataPaymentRejected,
        },
        {
          orderTotal: dataReadytoThaw.length,
          orderName: 'Siap Dicairkan',
          orderData: dataReadytoThaw,
        },
      ];
      setorderData(data);
      let dataFilteran = data.filter((v) => v.orderName === selectedOrder);
      dataFilteran = dataFilteran.flatMap((v) => v.orderData);
      setdataOrderan(dataFilteran);
    }
    setloadingData(false);
  }, [tokoOrderPrimary.data]);

  const handleCopyLink = () => {
    Clipboard.setString(Config.TOKO_NAME + toko.link);
    Toast.show('Link disalin');
  };
  // useEffect(() => {
  //   props.dispatch(GlobalActions.setModalVisible(false));
  // }, []);
  const _OrderStatusHandler = ({status_pesanan}) => {
    switch (status_pesanan) {
      case 1:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c2dcff',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#1a69d5',
                letterSpacing: 0.7,
              }}>
              Pesanan Baru
            </Text>
          </View>
        );
        break;

      case 2:
        return (
          <View
            style={{
              width: '90%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Pesanan Diterima
            </Text>
          </View>
        );
        break;
      case 3:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 31:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 32:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 4:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Dikirim
            </Text>
          </View>
        );
        break;
      case 5:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Selesai
            </Text>
          </View>
        );
        break;
      case 6:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#d0021b',
                letterSpacing: 0.7,
              }}>
              Pesanan Ditolak
            </Text>
          </View>
        );
        break;
      case 7:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#d0021b',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Gagal
            </Text>
          </View>
        );
        break;
      case 14:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Siap Dicairkan
            </Text>
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const _renderListOrderItem = ({item, index, dataTransaction}) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: '#ffffff',
          marginTop: 12,
          flexDirection: 'row',
        }}>
        <View
          style={{
            width: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Card
            style={{
              width: 47,
              height: 47,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 0,
              marginRight: 0,
            }}>
            <Image
              style={{width: 38, height: 33}}
              source={
                item.details.length != 0
                  ? {uri: item.details[0].image}
                  : Images.noImg
              }
            />
          </Card>
        </View>
        <View
          style={{
            width: '47%',
            height: '100%',
            marginLeft: 8,
          }}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={{
              fontFamily: 'NotoSans-Bold',
              color: '#4a4a4a',
              fontSize: 11,
              letterSpacing: 1,
              marginTop: 1,
            }}>
            {item.details.length != 0 ? item.details[0].name : '-'}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              color: '#9b9b9b',
              fontSize: 9,
              letterSpacing: 0.7,
              marginTop: 3,
            }}>
            +{item.details.length.toString()} produk
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1,
              marginTop: 3,
            }}>
            {item.supplier ? item.supplier.name : item.toko.name}
          </Text>
        </View>
        <View
          style={{
            width: '35%',
            height: '100%',

            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              top: 0,
              color: '#4a4a4a',
              fontFamily: 'NotoSans',
              fontSize: 11,
              marginRight: 1,
              marginTop: 1,
            }}>
            {item.details.length != 0
              ? func.rupiah(item.details[0].price + item.shipping_fee)
              : func.rupiah(0)}
          </Text>

          <_OrderStatusHandler status_pesanan={item.status} />
        </View>
      </View>
    );
  };
  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleNavigation(item)}
        delayPressIn={0}
        style={{
          backgroundColor: 'white',

          width: '100%',
          padding: 8,
        }}>
        <Card
          style={{
            width: '100%',
            marginLeft: 0,
            marginRight: 0,
            padding: 10,
            backgroundColor: '#ffffff',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#9b9b9b',
                flex: 1,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 11,
                marginTop: 12,
              }}>
              {moment(item.date).format('ll')}
            </Text>
            <Text
              style={{
                fontSize: 11,
                color: '#4a4a4a',
                textAlign: 'right',
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                marginTop: 12,
              }}>
              {func.rupiah(item.amount + item.admin_fee)}
            </Text>
          </View>
          <View
            style={{
              marginTop: 15,
              width: '100%',
              borderWidth: 0.4,
              borderColor: '#c9c6c6',
            }}
          />
          {item.orders &&
            item.orders.map((data, index) => {
              return (
                <_renderListOrderItem
                  key={index}
                  item={data}
                  dataTransaction={item}
                />
              );
            })}
        </Card>
      </TouchableOpacity>
    );
  };
  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => setSelectedOrder(item.orderName)}
        style={
          selectedOrder === item.orderName
            ? {
                borderWidth: 1,
                borderColor: '#fff',
                justifyContent: 'center',
                backgroundColor: '#fff',
                width: Dimensions.get('screen').width * 0.18,
                padding: 5,
                margin: 5,
                borderRadius: 4,
              }
            : {
                borderWidth: 1,
                borderColor: '#fff',
                justifyContent: 'center',
                width: Dimensions.get('screen').width * 0.18,
                padding: 5,
                margin: 5,
                borderRadius: 4,
              }
        }>
        <View>
          <Text
            style={
              selectedOrder === item.orderName
                ? {
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 17,
                    letterSpacing: 0.5,
                    color: c.Colors.mainBlue,
                    textAlign: 'center',
                  }
                : {
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 17,
                    letterSpacing: 0.5,
                    color: '#fff',
                    textAlign: 'center',
                  }
            }>
            {item.orderTotal}
          </Text>
        </View>
        <View style={{width: '90%', alignSelf: 'center'}}>
          <Text
            style={
              selectedOrder === item.orderName
                ? {
                    textAlign: 'center',

                    fontFamily: 'NotoSans-Regular',
                    fontSize: 7,
                    letterSpacing: 0.75,
                    color: c.Colors.mainBlue,
                  }
                : {
                    textAlign: 'center',

                    fontFamily: 'NotoSans-Regular',
                    fontSize: 7,
                    letterSpacing: 0.75,
                    color: '#fff',
                  }
            }>
            {item.orderName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getTokoOrderRequest(tokoId));
    dispatch(TokoActions.getTokoAddressRequest(tokoId));
    dispatch(TokoActions.getTokoBankRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  return (
    <Container>
      {tokoId === null ? (
        <View>
          <View style={c.Styles.justifyCenter}>
            <View style={c.Styles.alignItemsCenter}>
              <c.Image
                source={require('../../../../assets/image/order.png')}
                style={{width: 300, height: 300, resizeMode: 'contain'}}
              />
            </View>
            <View style={[{width: '80%'}, c.Styles.alignCenter]}>
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1.43,
                  color: '#4a4a4a',
                  textAlign: 'center',
                }}>
                Kamu belum ada pesanan
              </Text>
            </View>
            <View
              style={[{width: '80%', marginTop: '5%'}, c.Styles.alignCenter]}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Regular',
                  letterSpacing: 1.13,
                  fontSize: 12,
                  color: '#4a4a4a',
                }}>
                {tokoData
                  ? 'Bagikan Link toko kamu ke teman - teman kamu lewat Whatsapp dan Social media'
                  : 'Untuk mulai mendapat pesanan ayo buat Toko kamu sekarang atau Cek katalog Marketplace kita !'}
              </Text>
              {tokoData ? (
                <View
                  style={{
                    borderBottomWidth: 0.8,
                    borderBottomColor: '#979797',
                    marginTop: '3%',
                    paddingVertical: '2%',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1.14,
                      color: '#1a69d5',
                    }}>
                    {(Config.TOKO_NAME + toko.link).length <= 32
                      ? Config.TOKO_NAME + toko.link
                      : (Config.TOKO_NAME + toko.link).substring(0, 32) + '...'}
                  </Text>
                  <View
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}>
                    <TouchableOpacity onPress={handleCopyLink} delayPressIn={0}>
                      <MaterialCommunityIcons
                        name="content-copy"
                        size={25}
                        color={c.Colors.grayWhite}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>
            {tokoData ? (
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    `whatsapp://send?text=${window.encodeURIComponent(
                      `Hi, ${
                        toko.name
                      }  baru membuat website baru nih \n \nYuk kunjungi toko nya disini ${
                        Config.TOKO_NAME + toko.link
                      }`,
                    )}`,
                  ).catch(() =>
                    Linking.openURL(
                      `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                    ),
                  )
                }
                style={styles.viewbtnWhatsapp}>
                <View
                  //onPress
                  style={styles.viewtxtWhatsapp}>
                  <MaterialCommunityIcons
                    name="whatsapp"
                    size={20}
                    color={'white'}
                  />
                  <Text style={styles.txtWhatsapp}>Bagikan lewat Whatsapp</Text>
                </View>
              </TouchableOpacity>
            ) : !isVerified ? (
              <View>
                <TouchableOpacity
                  onPress={() => {
                    if (!isVerified) {
                      if (userData.data.is_phone_verified === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 1,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (tokoId === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 2,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (!tokoAddress) {
                        let dataStep = {
                          modalVisible: true,
                          step: 3,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      }
                      // bottomSheetRef.current.open();
                    }
                  }}
                  style={styles.viewbtncreateToko}>
                  <View style={styles.viewtxtWhatsapp}>
                    <MaterialCommunityIcons
                      name="storefront"
                      size={20}
                      color={'white'}
                    />
                    <Text style={styles.txtWhatsapp}>Buat Toko Sekarang</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Dropship')}
                  style={styles.viewbtnKatalog}>
                  <View style={styles.viewtxtWhatsapp}>
                    <MaterialCommunityIcons
                      name="cart-outline"
                      size={20}
                      color={'white'}
                    />
                    <Text style={styles.txtWhatsapp}>
                      Cek Katalog Marketplace
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            ) : (
              <View />
            )}
          </View>
        </View>
      ) : (
        <View>
          {fetchingTokoOrder && tokoOrderPrimary.data === null ? (
            <View
              style={[
                c.Styles.justifyCenter,
                c.Styles.alignItemsCenter,
                {height: Dimensions.get('screen').height * 0.9},
              ]}>
              <ActivityIndicator size={'large'} color={c.Colors.mainBlue} />
            </View>
          ) : (
            <View>
              <View
                style={{
                  height: Dimensions.get('screen').height * 0.1,
                  backgroundColor: c.Colors.mainBlue,
                  padding: 5,
                }}>
                {orderData.length > 0 ? (
                  <FlatList
                    numColumns={1}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={orderData}
                    renderItem={renderItem}
                  />
                ) : null}
              </View>
              <ScrollView
                contentContainerStyle={{
                  paddingBottom: Dimensions.get('screen').height * 0.2,
                  marginBottom: Dimensions.get('screen').height * 0.2,
                }}
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={handleRefresh}
                  />
                }>
                {tokoOrderData ? (
                  dataOrderan.length > 0 ? (
                    <FlatList
                      style={{bottom: 5}}
                      numColumns={1}
                      data={dataOrderan}
                      renderItem={_renderItem}
                    />
                  ) : (
                    <View>
                      <View
                        style={[
                          c.Styles.justifyCenter,
                          {
                            height: Dimensions.get('screen').height * 0.6,
                          },
                        ]}>
                        <View style={c.Styles.alignItemsCenter}>
                          <c.Image
                            source={require('../../../../assets/image/order.png')}
                            style={{
                              width: 300,
                              height: 300,
                              resizeMode: 'contain',
                            }}
                          />
                        </View>
                        <View style={[{width: '80%'}, c.Styles.alignCenter]}>
                          <Text
                            style={{
                              fontSize: 15,
                              fontFamily: 'NotoSans-Bold',
                              letterSpacing: 1.43,
                              color: '#4a4a4a',
                              textAlign: 'center',
                            }}>
                            Ooops! kamu belum ada pesanan untuk kategori ini
                          </Text>
                        </View>
                      </View>
                    </View>
                  )
                ) : (
                  <View>
                    <View style={c.Styles.justifyCenter}>
                      <View style={c.Styles.alignItemsCenter}>
                        <c.Image
                          source={require('../../../../assets/image/order.png')}
                          style={{
                            width: 300,
                            height: 300,
                            resizeMode: 'contain',
                          }}
                        />
                      </View>
                      <View style={[{width: '80%'}, c.Styles.alignCenter]}>
                        <Text
                          style={{
                            fontSize: 15,
                            fontFamily: 'NotoSans-Bold',
                            letterSpacing: 1.43,
                            color: '#4a4a4a',
                            textAlign: 'center',
                          }}>
                          Kamu belum ada pesanan
                        </Text>
                      </View>
                      <View
                        style={[
                          {width: '80%', marginTop: '5%'},
                          c.Styles.alignCenter,
                        ]}>
                        <Text
                          style={{
                            fontFamily: 'NotoSans-Regular',
                            letterSpacing: 1.13,
                            fontSize: 12,
                            color: '#4a4a4a',
                          }}>
                          {tokoData
                            ? 'Bagikan Link toko kamu ke teman - teman kamu lewat Whatsapp dan Social media'
                            : 'Untuk mulai mendapat pesanan ayo buat Toko kamu sekarang atau Cek katalog Marketplace kita !'}
                        </Text>
                        {tokoData ? (
                          <View
                            style={{
                              borderBottomWidth: 0.8,
                              borderBottomColor: '#979797',
                              marginTop: '3%',
                              paddingVertical: '2%',
                            }}>
                            <Text
                              style={{
                                fontFamily: 'NotoSans-Bold',
                                fontSize: 12,
                                letterSpacing: 1.14,
                                color: '#1a69d5',
                              }}>
                              {(Config.TOKO_NAME + toko.link).length <= 32
                                ? Config.TOKO_NAME + toko.link
                                : (Config.TOKO_NAME + toko.link).substring(
                                    0,
                                    32,
                                  ) + '...'}
                            </Text>
                            <View
                              style={{
                                position: 'absolute',
                                right: 0,
                              }}>
                              <TouchableOpacity
                                onPress={handleCopyLink}
                                delayPressIn={0}>
                                <MaterialCommunityIcons
                                  name="content-copy"
                                  size={25}
                                  color={c.Colors.grayWhite}
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ) : null}
                      </View>
                      {tokoData ? (
                        <TouchableOpacity
                          onPress={() =>
                            Linking.openURL(
                              `whatsapp://send?text=${window.encodeURIComponent(
                                `Hi, ${
                                  toko.name
                                }  baru membuat website baru nih \n \nYuk kunjungi toko nya disini ${
                                  Config.TOKO_NAME + toko.link
                                }`,
                              )}`,
                            ).catch(() =>
                              Linking.openURL(
                                `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                              ),
                            )
                          }
                          style={styles.viewbtnWhatsapp}>
                          <View
                            //onPress
                            style={styles.viewtxtWhatsapp}>
                            <MaterialCommunityIcons
                              name="whatsapp"
                              size={20}
                              color={'white'}
                            />
                            <Text style={styles.txtWhatsapp}>
                              Bagikan lewat Whatsapp
                            </Text>
                          </View>
                        </TouchableOpacity>
                      ) : !isVerified ? (
                        <View>
                          <TouchableOpacity
                            onPress={() => {
                              if (!isVerified) {
                                if (userData.data.is_phone_verified === null) {
                                  let dataStep = {
                                    modalVisible: true,
                                    step: 1,
                                  };
                                  dispatch(
                                    GlobalActions.setModalVisible(dataStep),
                                  );
                                } else if (tokoId === null) {
                                  let dataStep = {
                                    modalVisible: true,
                                    step: 2,
                                  };
                                  dispatch(
                                    GlobalActions.setModalVisible(dataStep),
                                  );
                                } else if (!tokoAddress) {
                                  let dataStep = {
                                    modalVisible: true,
                                    step: 3,
                                  };
                                  dispatch(
                                    GlobalActions.setModalVisible(dataStep),
                                  );
                                }
                                // bottomSheetRef.current.open();
                              }
                            }}
                            style={styles.viewbtncreateToko}>
                            <View style={styles.viewtxtWhatsapp}>
                              <MaterialCommunityIcons
                                name="storefront"
                                size={20}
                                color={'white'}
                              />
                              <Text style={styles.txtWhatsapp}>
                                Buat Toko Sekarang
                              </Text>
                            </View>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => navigation.navigate('Dropship')}
                            style={styles.viewbtnKatalog}>
                            <View style={styles.viewtxtWhatsapp}>
                              <MaterialCommunityIcons
                                name="cart-outline"
                                size={20}
                                color={'white'}
                              />
                              <Text style={styles.txtWhatsapp}>
                                Cek Katalog Dropship
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      ) : (
                        <View />
                      )}
                    </View>
                  </View>
                )}
                {/* {tokoOrderData !== null && tokoOrderData.transactions.length < 1 ? (
      
    ) : null} */}
              </ScrollView>
            </View>
          )}
        </View>
      )}
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    isTokoOrderDataFetching: state.toko.tokoOrderData.fetching,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    tokoOrderPrimary: state.toko.tokoOrderData,
    fetchingTokoOrder: state.toko.tokoOrderData,
    tokoOrderData:
      state.toko.tokoOrderData.data != null
        ? state.toko.tokoOrderData.data.transactions.length
          ? state.toko.tokoOrderData.data.transactions.length != 0
            ? true
            : false
          : false
        : false,

    toko: state.toko.detailTokoData.data
      ? state.toko.detailTokoData.data.toko
        ? state.toko.detailTokoData.data.toko
        : {}
      : {},
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default withNavigationFocus(
  connect(mapStateToProps, mapDispatchToProps)(index),
);
