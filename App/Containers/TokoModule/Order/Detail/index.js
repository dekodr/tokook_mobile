import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  Clip,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import CheckBox from '@react-native-community/checkbox';
import Swiper from 'react-native-swiper';
import moment from 'moment';
import {TextInputMask} from 'react-native-masked-text';
import {Snackbar} from 'react-native-paper';
import {Button, Spinner, Card, Header, Left, Body, Right} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from './../../../../Redux/TokoRedux';

import {Images, Colors} from './../../../../Themes/';
import * as func from './../../../../Helpers/Utils';

import * as c from './../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
const initialOrderValidation = {
  isShippingCarrierEmpty: false,
  isShippingFeeEmpty: false,
  isShippingTraceNoEmpty: false,
  index: -1,
};
const dataOrder = [
  {
    nama_barang: 'Ayam',
    jumlah_pesanan: 12,
    merchant_name: 'Ayam Bakar Mpo Ad',
    harga: 100000,
    status_pesanan: 1,
  },
  {
    nama_barang: 'Bebek Madu',
    jumlah_pesanan: 10,
    merchant_name: 'Bebek Madu Rasa',
    harga: 50000,
    status_pesanan: 2,
  },
  {
    nama_barang: 'Sate Ayam',
    jumlah_pesanan: 2,
    merchant_name: 'Ayam Bakar Mpo Ad',
    harga: 100000,
    status_pesanan: 3,
  },

  {
    nama_barang: 'Ayam 1',
    jumlah_pesanan: 12,
    merchant_name: 'Ayam Bakar Mpo Ad 1',
    harga: 100000,
    status_pesanan: 1,
  },
  {
    nama_barang: 'Bebek Madu 1',
    jumlah_pesanan: 10,
    merchant_name: 'Bebek Madu Rasa 1',
    harga: 50000,
    status_pesanan: 2,
  },
  {
    nama_barang: 'Sate Ayam 1',
    jumlah_pesanan: 2,
    merchant_name: 'Ayam Bakar Mpo Ad 1',
    harga: 100000,
    status_pesanan: 1,
  },
  {
    nama_barang: 'Ayam 2',
    jumlah_pesanan: 12,
    merchant_name: 'Ayam Bakar Mpo Ad 2',
    harga: 100000,
    status_pesanan: 1,
  },
  {
    nama_barang: 'Bebek Madu 2',
    jumlah_pesanan: 10,
    merchant_name: 'Bebek Madu Rasa 2',
    harga: 50000,
    status_pesanan: 2,
  },
  {
    nama_barang: 'Sate Ayam 2',
    jumlah_pesanan: 2,
    merchant_name: 'Ayam Bakar Mpo Ad 2',
    harga: 100000,
    status_pesanan: 1,
  },
];

const dataOrderInfo = [
  {content: 'Toko', value: 'Ayam Goreng Amir'},
  {content: 'Invoice', value: 'INV/20201130/XX/XI/6784574'},
  {content: 'Status', value: 1},
];
const dataOrderProceed = [
  {content: 'Toko', value: 'Ayam Goreng Amir'},
  {content: 'Invoice', value: 'INV/20201130/XX/XI/6784574'},
  {content: 'Status', value: 2},
];
const dataOrderPaymentDone = [
  {content: 'Toko', value: 'Ayam Goreng Amir'},
  {content: 'Invoice', value: 'INV/20201130/XX/XI/6784574'},
  {content: 'Status', value: 3},
];
const dataOrderDone = [
  {content: 'Toko', value: 'Ayam Goreng Amir'},
  {content: 'Invoice', value: 'INV/20201130/XX/XI/6784574'},
  {content: 'Status', value: 4},
];
const dataOrderFinish = [
  {content: 'Toko', value: 'Ayam Goreng Amir'},
  {content: 'Invoice', value: 'INV/20201130/XX/XI/6784574'},
  {content: 'Status', value: 5},
];
const initialValidation = {
  isProvinceEmpty: false,
  isCityEmpty: false,
  isDistrictEmpty: false,
  isPostalCodeEmpty: false,
  isAddressEmpty: false,
  isDetailAddressEmpty: false,
};
const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
const OrderDetailScreen = function (props) {
  const {
    navigation,
    dispatch,
    supplierData,
    isTokoAddressExists,
    tokoId,
    tokoOrderDetailData,
    tokoOrderDetailDataFetching,
    tokoCourierData,
    tokoBankData,
    tokoAddressData,
    tokoData,
    isOrderProcessFetching,
    isOrderAcceptPaymentFetching,
    isSendOrderFetching,
    isFinishOrderFetching,
    isTokoBankExists,
    productData,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    isErrorTokoBankAlreadyRegistered,
    isSendOrderSuccess,
    bankData,
  } = props;
  const courierTxt = useRef(null);
  const orderListRef = useRef(null);
  const scrollParentRef = useRef(null);
  const [orderFormValidation, setOrderFormValidation] = useState(
    initialOrderValidation,
  );
  const [indexManipulate, setindexManipulate] = useState(-1);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialValidation,
  );
  const [addedBankFormValidation, setAddedBankFormValidation] = useState(
    initialBankValidation,
  );
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState(null);
  const [detailAddressValue, setDetailAddressValue] = useState(null);
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [snackBar, setSnackBar] = useState({show: false, message: ''});
  const [isAddedAddress, setIsAddedAddress] = useState(false);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);
  const [editSelected, setEditSelected] = useState(null);
  const [orderData, setOrderData] = useState(dataOrder);
  const [listOrder, setListOrder] = useState(null);
  const [dataKontak, setDataKontak] = useState(dataKontakInfo);
  const [orderInfo, setorderInfo] = useState(dataOrderInfo);
  const [swiperIndex, setSwiperIndex] = useState(0);
  const [orderListIndex, setOrderListIndex] = useState(-1);
  const [orderId, setOrderId] = useState(0);
  const [isShowCourier, setIsShowCourier] = useState(false);
  const swiper = useRef(null);
  const params = navigation.getParam('params');
  const setTabIndex = navigation.getParam('setTabIndex');
  const getStatusPemesanan = params.status;
  console.log('getStatusPemesanan', getStatusPemesanan);
  const refRBSheet = useRef(null);

  const dataKontakInfo = [
    {
      content: 'Name',
      value:
        tokoOrderDetailData != null &&
        tokoOrderDetailData.transaction.contact.name,
    },
    {
      content: 'Email',
      value:
        tokoOrderDetailData != null &&
        tokoOrderDetailData.transaction.contact.email,
    },
    {
      content: 'No Hp',
      value:
        tokoOrderDetailData != null &&
        tokoOrderDetailData.transaction.contact.phone,
    },
    {
      content: 'Alamat',
      value:
        tokoOrderDetailData != null &&
        tokoOrderDetailData.transaction.contact.address,
    },
  ];
  const handleAddAdressSubmit = () => {
    if (provinceValue === null || provinceValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isProvinceEmpty: true,
      });
    } else if (cityValue === null || cityValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isCityEmpty: true,
      });
    } else if (districtValue === null || districtValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isDistrictEmpty: true,
      });
    } else if (postalCodeValue === null || postalCodeValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isPostalCodeEmpty: true,
      });
    } else if (addressValue === null || addressValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isAddressEmpty: true,
      });
    } else {
      const postData = {
        tokoId: props.tokoId,
        func: {
          isAdded: setIsAddedAddress(),
          isUpdated: setIsUpdatedAddress(),
        },
        addressId: addressIdValue,
        purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
        detail: {
          province_id: provinceValue.id,
          city_id: cityValue.id,
          district_id: districtValue.id,
          postal_code: postalCodeValue,
          address: addressValue.concat(
            detailAddressValue === null ? '' : detailAddressValue,
          ),
        },
      };

      props.dispatch(TokoActions.postTokoAddressRequest(postData));
    }
  };
  const setValueSubmitBank = (purpose, value) => {
    switch (purpose) {
      case 1: {
        setIsAddedBank(value);

        break;
      }
      case 2: {
        setBankIndex(value);
        break;
      }
      default:
        null;
    }
  };
  const handleAddBankSubmit = () => {
    if (bankValue === null || bankValue === '') {
      setAddedBankFormValidation({
        ...addedBankFormValidation,
        isBankEmpty: true,
      });
    } else if (bankAccountNumValue === null || bankAccountNumValue === '') {
      setAddedBankFormValidation({
        ...addedBankFormValidation,
        isBankAccountNumEmpty: true,
      });
    } else if (bankOwnerNameValue === null || bankOwnerNameValue === '') {
      setAddedBankFormValidation({
        ...addedBankFormValidation,
        isBankOwnerEmpty: true,
      });
    } else {
      const postData = {
        tokoId: props.tokoId,
        rekId: rekId,
        setValueBank: setValueSubmitBank,
        // func: {updateBank: setBankIndex()},
        purpose: isAddedBank ? 1 : 2,
        detail: {
          bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
          holder_name: bankOwnerNameValue,
          rekening_number: bankAccountNumValue,
        },
      };

      props.dispatch(TokoActions.postTokoBankRequest(postData));
    }
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          props.dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const searchingPostalCode = (searchText) => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerPostalCode(true);

        let filteredData = postalCodeData.postalCode.filter(function (item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setPostalCodeValue(searchText);
    }
  };
  const _tokoAddressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          props.dispatch(TokoActions.getCityRequest(item.province_details));
          props.dispatch(TokoActions.getDistrictRequest(item.city_details));
          props.dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: item.city_details.name,
              districtName: item.district_details.name,
            }),
          );
          setAddressIdValue(item.id);
          setProvinceValue(item.province_details);
          setCityValue(item.city_details);
          setDistrictValue(item.district_details);
          setAddressValue(item.address);
          setDetailAddressValue(item.address2);
          setPostalCodeValue(item.postal_code);
          setIsUpdatedAddress(true);
        }}
        delayPressIn={0}
        style={{
          width: '100%',
          borderColor: c.Colors.gray,
          borderWidth: 0.58,
          marginTop: 17,
          paddingLeft: 18,
          paddingRight: 18,
          bottom: 1,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
            marginTop: 20,
          }}>
          {item.province_details.name}
        </Text>

        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.name}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.postal_code}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.fullAddress}
        </Text>
        <View style={{height: 25}} />
        {/* <View
          style={{
            borderRadius: 25,
            marginTop: 15,
            width: '100%',
            height: '50%',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '28%',
              height: '100%',
            }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              initialRegion={{
                latitude: 36.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0,
                longitudeDelta: 0.0,
              }}
              style={{
                width: '100%',
                height: '72%',
                alignSelf: 'center',
              }}>
              <MapView.Marker
                coordinate={{latitude: 37.78825, longitude: -122.4324}}
                title={'title'}
                description={'description'}
              />
            </MapView>
          </View>
          <View
            style={{
              width: '75%',
              height: '80%',
              paddingLeft: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#7b7b7b',
                letterSpacing: 1,
                fontSize: 9,
              }}>
              Jl. Blora No.32, RT.2/RW.6, Dukuh Atas, Menteng, Kec. Menteng,
              Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310
            </Text>
          </View>
        </View>
      */}
      </TouchableOpacity>
    );
  };
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          );
        }}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode
            ? item.postal_code
            : city
            ? item.type + ' ' + item.name
            : item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  const getTotalAmountNewOrder = () => {
    try {
      if (tokoOrderDetailData != null) {
        const filterDataAcceptOnly = listOrder.filter((x) => x.accept === true);
        let total = 0;
        filterDataAcceptOnly.forEach((item, i) => {
          console.log('item', item);
          if (item.supplier) {
            if (item.total_amount === undefined) {
              total +=
                Number(item.amount) +
                Number(item.shipping_fee) +
                Number(tokoOrderDetailData.transaction.admin_fee);
            } else {
              total +=
                Number(item.total_amount) +
                Number(item.shipping_fee) +
                Number(tokoOrderDetailData.transaction.admin_fee);
            }
          } else if (item.shipping_carrier.toLowerCase().includes('ambil')) {
            total += Number(item.amount);
          } else {
            if (item.total_amount === undefined && item.shipping_fee) {
              total +=
                Number(item.amount) +
                Number(item.shipping_fee) +
                Number(tokoOrderDetailData.transaction.admin_fee);
            } else if (item.total_amount === undefined) {
              total += item.amount;
            } else {
              total +=
                Number(item.total_amount) +
                Number(tokoOrderDetailData.transaction.admin_fee);
            }
          }
        });
        console.log('total', total);
        return func.rupiah(total);
      }
    } catch (err) {}
  };

  const getTotalAmountReceiptSender = (purpose) => {
    switch (purpose) {
      case 1: {
        try {
          if (tokoOrderDetailData != null) {
            const filterDataAcceptOnly = tokoOrderDetailData.transaction.orders.filter(
              (x) => x.status === 2,
            );
            let total = 0;
            filterDataAcceptOnly.forEach((item, i) => {
              total +=
                item.total_amount === undefined
                  ? item.amount +
                    item.shipping_fee +
                    tokoOrderDetailData.transaction.admin_fee
                  : item.total_amount;
            });
            return func.rupiah(total);
          }
        } catch (err) {}
        break;
      }
      case 2: {
        try {
          if (tokoOrderDetailData != null) {
            const filterDataAcceptOnly = tokoOrderDetailData.transaction.orders.filter(
              (x) => x.status === 2,
            );
            let total = 0;
            filterDataAcceptOnly.forEach((item, i) => {
              total +=
                item.total_amount === undefined
                  ? item.amount
                  : item.total_amount;
            });
            return func.rupiah(total);
          }
        } catch (err) {}
        break;
      }
      case 3: {
        try {
          if (tokoOrderDetailData != null) {
            const filterDataAcceptOnly = tokoOrderDetailData.transaction.orders.filter(
              (x) => x.status === 2,
            );
            let total = 0;
            filterDataAcceptOnly.forEach((item, i) => {
              total +=
                item.total_amount === undefined
                  ? item.shipping_fee
                  : item.total_amount;
            });
            return func.rupiah(total);
          }
        } catch (err) {}
        break;
      }
      default:
        null;
    }
  };
  const handleCallNumber = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL(
        `telprompt:${
          tokoOrderDetailData != null &&
          tokoOrderDetailData.transaction.contact.phone
        }`,
      );
    } else {
      Linking.openURL(
        `tel:${
          tokoOrderDetailData != null &&
          tokoOrderDetailData.transaction.contact.phone
        }`,
      );
    }
  };
  const handleGoBack = () => {
    dispatch(TokoActions.getTokoOrderRequest(tokoId));

    // setTabIndex(2);
    navigation.goBack();
  };
  useEffect(() => {
    if (isSendOrderSuccess) {
      let dataLatest = listOrder;
      if (dataLatest !== null) {
        dataLatest[indexManipulate].status = 4;
        setListOrder(dataLatest);
      }
    }
  }, [isSendOrderSuccess]);

  useEffect(() => {
    if (tokoOrderDetailData != null) {
      let dataLatest = [];
      tokoOrderDetailData.transaction.orders.map((v, i) => {
        dataLatest.push({
          id: v.id,
          transaction_id: v.transaction_id,
          toko_id: v.toko_id,
          invoice_no: v.invoice_no,
          amount: v.amount,
          admin_fee: v.admin_fee,
          customer_name: v.customer_name,
          customer_phone: v.customer_phone,
          customer_email: v.customer_email,
          status: v.status,
          country: v.country,
          province_id: v.province_id,
          city_id: v.city_id,
          district_id: v.district,
          postal_code: v.postal_code,
          address: v.address,
          address2: v.address2,
          date: v.date,
          shipping_method: v.shipping_method,
          shipping_carrier: v.shipping_carrier,
          shipping_date: v.shipping_date,
          shipping_fee: v.shipping_fee,
          shipping_traceno: v.shipping_traceno,
          payment_channel: v.payment_channel,
          payment_method: v.payment_method,
          payment_date: v.payment_date,
          supplier_id: v.supplier_id,
          is_tokotalk: v.is_tokotalk,
          supplier: v.supplier,
          toko: v.toko,
          province_details: v.province_details,
          city_details: v.city_details,
          district_details: v.district_details,
          details: v.details,
          shipping_tracking_web: v.shipping_tracking_web,
        });
      });
      setListOrder(dataLatest);
    }
  }, [tokoOrderDetailData]);
  const handleStatusOrder = (id, value, item) => {
    var temp = [...listOrder];
    let statusValue = null;
    temp.map((t) => {
      if (t.id == id) {
        statusValue = {...t, accept: value};

        return statusValue;
      }
    });
    temp.push(statusValue);
    const result = Object.values(
      temp.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}),
    );
    setListOrder(result);
  };
  const handleWAOrderMessage = () => {
    let tokoBankDefault =
      tokoBankData != null &&
      tokoBankData.listRekening.filter((x) => x.default === true);

    let orderData = tokoOrderDetailData.transaction.orders;
    let orderCheckoutUrl = tokoOrderDetailData.transaction.payment_url;
    let adminFee = tokoOrderDetailData.transaction.admin_fee;
    let filterOrderAcceptOnly = orderData.filter((x) => x.status === 2);
    let messageBody = filterOrderAcceptOnly.map((t, index) => {
      let detailOrders = t.details.map((v, index) => {
        const indexNum = index + 1;
        let messageDetailValue = `${indexNum}. ${v.name}${'\n'}${
          func.rupiah(v.price) + ' /pcs'
        }${'\n'}${v.quantity}PCS${'\n'}`;
        return messageDetailValue.split(null).join('-');
      });
      let messageValue = `${'\n'}Invoice id : ${
        t.invoice_no
      }${'\n'}Shipping : ${t.shipping_carrier}${'\n'}${detailOrders.join('')}
      `;

      return messageValue.split(null).join('-');
    });
    let listBankMessage =
      tokoBankData != null &&
      tokoBankData.listRekening.map((tbank, index) => {
        let bankMessage = `${'-'} ${tbank.bank_details.name} :  ${
          tbank.holder_name
        } -  ${tbank.rekening_number}${'\n'}`;
        return bankMessage.split(null).join('-');
      });
    let messageHeader = `Terimakasih sudah belanja di toko ${
      tokoData.listToko[0].name
    }${'\n'}Tanggal : ${moment(orderData.date).format('ll')}${'\n'}Kirim ke ${
      tokoOrderDetailData.transaction.contact.name
    }(${tokoOrderDetailData.transaction.contact.phone})${'\n'}${
      tokoOrderDetailData.transaction.contact.address
    }`;
    let messageFooter = `${'\n'}TOTAL ${getTotalAmountReceiptSender(
      2,
    )}${'\n'}SHIPPING ${getTotalAmountReceiptSender(
      3,
    )}${'\n'}ADMIN FEE ${func.rupiah(
      adminFee != null ? adminFee : 0,
    )}${'\n'}Grand Total ${func.rupiah(
      tokoOrderDetailData.transaction.admin_fee +
        tokoOrderDetailData.transaction.amount,
    )}${'\n\n'}Untuk Pembayaran, klik di link berikut : ${'\n'}${orderCheckoutUrl}`;

    Linking.openURL(
      `whatsapp://send?text=${encodeURIComponent(
        messageHeader + '\n\n' + messageBody.join('') + messageFooter,
      )}&phone=${
        tokoOrderDetailData != null &&
        tokoOrderDetailData.transaction.contact.phone
      }
      `,
    ).catch(() =>
      Linking.openURL(
        `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
      ),
    );
  };
  const handleShowCourierOrder = (id, value, item) => {
    var temp = [...listOrder];
    let courierValue = null;
    temp.map((t) => {
      if (t.id == orderId) {
        courierValue = {
          ...t,
          shipping_name: value,
          shipping_carrier: item.code,
          shipping_fee: item.id === 23 ? 0 : t.shipping_fee,
        };

        return courierValue;
      }
    });
    temp.push(courierValue);
    const result = Object.values(
      temp.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}),
    );
    setListOrder(result);
    setIsShowCourier(false);

    setOrderId(0);
    setOrderListIndex(-1);
  };
  const handleShippingFeeOrder = (item, value) => {
    var temp = [...listOrder];
    let courierValue = null;
    temp.map((t) => {
      if (t.id == item.id) {
        courierValue = {
          ...t,
          shipping_fee: value,
          total_amount: Number(item.amount) + Number(value),
        };

        return courierValue;
      }
    });
    temp.push(courierValue);
    const result = Object.values(
      temp.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}),
    );
    setListOrder(result);
  };
  const handleShippingTraceNoOrder = (id, value, item) => {
    var temp = [...listOrder];
    let traceNoValue = null;
    temp.map((t) => {
      if (t.id == id) {
        traceNoValue = {...t, shipping_traceno: value};

        return traceNoValue;
      }
    });
    temp.push(traceNoValue);
    const result = Object.values(
      temp.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}),
    );
    setListOrder(result);
  };
  const _renderBankItem = ({index, item}) => {
    if (bankIndex === index) {
      return (
        <View
          style={{
            width: '100%',

            borderColor: c.Colors.gray,
            borderWidth: 0.58,
            marginTop: 17,
            paddingLeft: 18,
            paddingRight: 18,
          }}>
          <TouchableOpacity
            onPress={() => {
              props.dispatch(TokoActions.postTokoBankReset());

              setBankIndex(-1);
            }}
            delayPressIn={0}
            style={{position: 'absolute', right: 8, top: 8}}>
            <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1.2,
              marginTop: 25,
            }}>
            Nama Bank
          </Text>
          <TouchableOpacity
            onPress={() => {
              props.dispatch(TokoActions.postTokoBankReset());

              if (bank) {
                setOpenPickerBank(false);
              } else {
                setOpenPickerBank(true);
              }
            }}
            delayPressIn={0}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankEmpty
                ? 'red'
                : bankNameValue.name == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <Text
              style={{
                bottom: 3,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
              }}>
              {bankNameValue.name != null || bankNameValue.name != ''
                ? bankNameValue.name
                : 'Nama Bank'}
            </Text>
            <MaterialCommunityIcons
              style={{bottom: 5}}
              name={!bank ? 'menu-down' : 'menu-up'}
              size={25}
              color="#1a69d5"
            />
          </TouchableOpacity>
          {/* {addedBankFormValidation.isBankEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib memilih Nama Bank
            </Text>
          )} */}
          {bank && (
            <FlatList
              nestedScrollEnabled
              style={{height: 300}}
              renderItem={_addressRenderItem}
              data={bankData.banks}
            />
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                ? 'red'
                : bankAccountNumValue == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              keyboardType={'number-pad'}
              onChangeText={(text) => {
                props.dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankAccountNumEmpty: false,
                });
                setBankAccountNumValue(text);
              }}
              value={bankAccountNumValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nomor Rekening'}
            />
          </View>
          {addedBankFormValidation.isBankAccountNumEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nomor rekening
            </Text>
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                ? 'red'
                : bankOwnerNameValue == null || bankOwnerNameValue == ''
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              onChangeText={(text) => {
                props.dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankOwnerEmpty: false,
                });

                setBankOwnerNameValue(text);
              }}
              value={bankOwnerNameValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nama Pemilik'}
            />
          </View>
          {addedBankFormValidation.isBankOwnerEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nama pemilik rekening
            </Text>
          )}

          <View style={{height: 14}} />
          {isErrorTokoBankAlreadyRegistered && (
            <Text
              style={[
                c.Styles.txtInputDesc,
                {color: 'red', alignSelf: 'center', top: 15},
              ]}>
              {isErrorTokoBankAlreadyRegistered.message}
            </Text>
          )}
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <View style={{width: '23%'}}>
              <Button
                style={{
                  height: 55,
                  width: 55,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: c.Colors.mainBlue,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}
                onPress={() => handleAddBankSubmit(3)}>
                <Image
                  style={{width: 25, height: 30}}
                  source={Images.trashIcon}
                />
              </Button>
            </View>
            <TouchableOpacity
              style={{
                top: 1,
                borderRadius: 25,
                height: 50,
                alignSelf: 'center',
                width: '75%',
                backgroundColor: '#1a69d5',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              delayPressIn={0}
              onPress={handleAddBankSubmit}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: '#ffffff',
                  letterSpacing: 1,
                  fontSize: 14,
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{height: 25}} />
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            setIsAddedBank(false);
            props.dispatch(TokoActions.postTokoBankReset());

            setRekId(item.id);
            setBankIndex(index);
            setIsUpdatedBank(true);
            setBankOwnerNameValue(item.holder_name);
            setBankAccountNumValue(item.rekening_number);
            setBankNameValue(item.bank_details);
            setBankValue(item.bank_details.name);
          }}
          delayPressIn={0}
          style={{
            width: '100%',
            height: 100,
            borderColor: '#c9c6c6',
            borderWidth: 0.5,
            marginTop: 14,
            justifyContent: 'center',
            paddingLeft: 25,
            bottom: 5,
          }}>
          <TouchableWithoutFeedback>
            <TouchableOpacity
              //kylie
              onPress={() => {
                const dataPost = {tokoId: props.tokoId, bankId: item.id};
                props.dispatch(
                  TokoActions.updateDefaultRekeningTokoRequest(dataPost),
                );
              }}
              delayPressIn={0}
              style={{
                marginTop: 8,
                marginRight: 8,
                position: 'absolute',
                right: 0,
                top: 0,
              }}>
              <Image
                style={{width: 20, height: 20}}
                source={
                  item.default ? Images.starBlueIcon : Images.starWhiteIcon
                }
              />
              {/* <MaterialCommunityIcons name="star" size={20} color={item.default?c.Colors.mainBlue:'#f4f4f4'} /> */}
            </TouchableOpacity>
          </TouchableWithoutFeedback>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.bank_details.name === null || item.bank_details.name === ''
              ? '-'
              : item.bank_details.name}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.rekening_number === null || item.rekening_number === ''
              ? '-'
              : item.rekening_number}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.holder_name === null || item.holder_name === ''
              ? '-'
              : item.holder_name}
          </Text>
        </TouchableOpacity>
      );
    }
  };
  const handleShowSnackBar = (message) => {
    setSnackBar({show: true, message: message});
  };
  const handleCopyLink = (item) => {
    Clipboard.setString(item.value);
    handleShowSnackBar(`${item.content} Berhasil disalin!`);
  };
  const handleCopyInvoice = () => {
    Clipboard.setString(tokoOrderDetailData.transaction.invoice_no);
    Toast.show('No Invoice disalin');
  };
  const _handleNavigation = (item) => {
    navigation.navigate('DropshipDetailScreen', {
      params: item,
    });
  };
  const handleProcessOrder = () => {
    let isAllVerified = false;

    let dataTransformOrder = listOrder.map((obj, index) => {
      console.log(obj);
      const indexPlus = index + 1;
      if (
        obj.shipping_carrier === 'ambil sendiri - Ambil di tempat' ||
        obj.shipping_carrier === 'AMBIL SENDIRI-AMBIL DI TEMPAT'
      ) {
        isAllVerified = true;
        return {
          order_id: obj.id,
          shipping_carrier: obj.shipping_carrier,
          shipping_fee: 0,
          accept: obj.accept,
        };
      } else if (
        obj.shipping_carrier === null &&
        obj.shipping_fee === null &&
        obj.accept === true
      ) {
        console.log('jalanin 2');
        courierTxt.current.focus();
        scrollParentRef.current.scrollTo({
          x: indexPlus,
          y: 500 * indexPlus,
          animated: true,
        });
        scrollParentRef.current.scrollTo({
          x: index,
          y: 300 * indexPlus,
          animated: true,
        });
        orderListRef.current.scrollToEnd();
        setOrderFormValidation({
          ...orderFormValidation,
          isShippingCarrierEmpty: true,
          isShippingFeeEmpty: true,
        });
      } else if (obj.shipping_carrier === null) {
        courierTxt.current.focus();
        scrollParentRef.current.scrollTo({
          x: indexPlus,
          y: 500 * indexPlus,
          animated: true,
        });
        scrollParentRef.current.scrollTo({
          x: index,
          y: 300 * indexPlus,
          animated: true,
        });
        orderListRef.current.scrollToEnd();
        setOrderFormValidation({
          ...orderFormValidation,
          isShippingCarrierEmpty: true,
        });
      } else if (obj.shipping_fee === null) {
        courierTxt.current.focus();
        scrollParentRef.current.scrollTo({
          x: indexPlus,
          y: 500 * indexPlus,
          animated: true,
        });
        scrollParentRef.current.scrollTo({
          x: index,
          y: 300 * indexPlus,
          animated: true,
        });
        orderListRef.current.scrollToEnd();
        setOrderFormValidation({
          ...orderFormValidation,
          isShippingFeeEmpty: true,
        });
      } else if (obj.shipping_carrier === null && obj.accept === true) {
        console.log('jalanin 3');
        setOrderFormValidation({
          ...orderFormValidation,
          isShippingCarrierEmpty: true,
        });
      } else if (obj.shipping_fee === null && obj.accept === true) {
        console.log('jalanin 4');
        setOrderFormValidation({
          ...orderFormValidation,
          isShippingFeeEmpty: true,
        });
      } else {
        isAllVerified = true;
        return {
          order_id: obj.id,
          shipping_carrier: obj.shipping_carrier,
          shipping_fee: obj.shipping_fee,
          accept: obj.accept,
        };
      }
    });
    const dataPost = {
      tokoId: tokoId,
      transactionId: params.id,
      func: swiper,
      detail: {
        transaction_id: tokoOrderDetailData.transaction.id,
        orders: dataTransformOrder,
      },
    };
    let cekIsUndefined = dataTransformOrder.filter((x) => x === undefined);

    if (isAllVerified && cekIsUndefined.length === 0) {
      dispatch(TokoActions.postTokoOrderProceedRequest(dataPost));
    }
  };
  const handleAcceptPayment = (purpose) => {
    const dataPost = {
      tokoId: tokoId,
      transactionId: params.id,
      purpose: purpose,
      func: {swiper: swiper, rbSheet: refRBSheet},
      detail: {
        transaction_id: tokoOrderDetailData.transaction.id,
      },
    };
    dispatch(TokoActions.postTokoAcceptPaymentRequest(dataPost));
  };
  const handlesetListOrder = (value) => {
    setListOrder(value);
  };
  const handleSendOrder = (item, index) => {
    console.log(
      'item',
      item,
      !item.shipping_carrier.toLowerCase().includes('ambil'),
    );
    if (
      item.shipping_traceno === null &&
      !item.shipping_carrier.toLowerCase().includes('ambil')
    ) {
      setOrderFormValidation({
        ...orderFormValidation,
        isShippingTraceNoEmpty: true,
        index: index,
      });
    } else {
      setindexManipulate(index);
      const dataPost = {
        tokoId: tokoId,
        detail: {
          transaction_id: params.id,
          order_id: item.id,
          shipping_traceno: item.shipping_carrier
            .toLowerCase()
            .includes('ambil')
            ? 'random'
            : item.shipping_traceno,
        },
        handlesetListOrders: handlesetListOrder,
        listOrder: listOrder,
        index: index,
      };
      dispatch(TokoActions.postTokoSendOrderRequest(dataPost));
    }
  };
  const handleFinishOrder = (item) => {
    const dataPost = {
      tokoId: tokoId,
      detail: {
        transaction_id: params.id,
        order_id: item.id,
      },
    };
    dispatch(TokoActions.postTokoFinishOrderRequest(dataPost));
  };
  useEffect(() => {
    dispatch(TokoActions.getTokoCourierRequest());
    if (getStatusPemesanan === 1) {
      setSwiperIndex(1);
    }
    if (getStatusPemesanan === 2) {
      setSwiperIndex(2);
    }
    if (
      getStatusPemesanan === 3 ||
      getStatusPemesanan === 4 ||
      getStatusPemesanan === 31 ||
      getStatusPemesanan === 32 ||
      getStatusPemesanan === 5 ||
      getStatusPemesanan === 6 ||
      getStatusPemesanan === 7 ||
      getStatusPemesanan === 14
    ) {
      setSwiperIndex(3);
    }
  }, [params]);
  const _courierListRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => handleShowCourierOrder(item.id, item.name, item)}
        delayPressIn={0}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  const _orderListRenderItem = ({item, index}) => {
    console.log('item');
    const dataOrderInfo = [
      {
        content: 'Toko',
        value: item.supplier ? item.supplier.name : item.toko.name,
      },
      {content: 'Invoice', value: item.invoice_no},
      {content: 'Status', value: item.status, url: item.shipping_tracking_web},
    ];
    return (
      <Card
        style={{
          marginTop: 0,
          paddingTop: 20,
          width: '100%',
          backgroundColor: '#ffffff',
          marginLeft: 0,
          marginRight: 0,
          marginBottom: 0,
          paddingTop: 30,
          borderWidth: 1,
        }}>
        {item.supplier ? (
          <TouchableOpacity
            delayPressIn={0}
            style={{
              position: 'absolute',
              right: 20,
              top: 10,
              borderRadius: 5,
            }}>
            <Image
              style={{width: 27, height: 27}}
              source={Images.purpleStoreIcon}
            />
          </TouchableOpacity>
        ) : null}

        <FlatList
          keyExtractor={(item, index) => index.toString()}
          style={{paddingLeft: 25, paddingRight: 25}}
          data={dataOrderInfo}
          renderItem={_renderDataOrderInfoItem}
        />
        {/* <FlatList data={dataKontak} renderItem={_renderDataKontakItem} />
         */}
        <View
          style={{
            alignSelf: 'center',
            marginTop: 15,
            width: '88%',
            borderWidth: 0.2,
            borderColor: '#c9c6c6',
          }}
        />
        {item.details &&
          item.details.map((data, index) => {
            return <_renderListOrderItem index={index} item={data} />;
          })}
        {/* <FlatList
          style={{paddingLeft: 25, paddingRight: 25}}
          data={orderData}
          renderItem={_renderListOrderItem}
        /> */}
        <View style={{width: '100%', flexDirection: 'row', marginTop: 25}}>
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,

              alignItems: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#9b9b9b',
                fontSize: 12,
                marginRight: 54,
              }}>
              Total Harga
            </Text>
          </View>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#4a4a4a',
              fontSize: 12,
              position: 'absolute',
              right: 29,
            }}>
            {func.rupiah(item.amount)}
          </Text>
        </View>

        <Card
          style={{
            width: '93%',
            marginTop: 40,
            padding: 15,

            alignSelf: 'center',
          }}>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <Image
              style={{height: 20, width: 33, resizeMode: 'cover'}}
              source={Images.truckIcn}
            />
            <Text
              style={{
                color: '#4a4a4a',
                fontSize: 14,
                fontFamily: 'NotoSans-Bold',
                marginLeft: 10,
              }}>
              Pilih Pengiriman
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              console.log(item);
              if (isShowCourier) {
                setIsShowCourier(false);
              } else {
                setOrderListIndex(index);
                setIsShowCourier(true);
                setOrderId(item.id);
              }
            }}
            disabled={
              swiperIndex === 2 ||
              swiperIndex === 3 ||
              swiperIndex === 31 ||
              swiperIndex === 32 ||
              item.shipping_carrier.toLowerCase().includes('ambil') ||
              !item.accept ||
              item.supplier_id != null ||
              item.shipping_fee
                ? true
                : false
            }
            delayPressIn={0}
            style={{
              marginTop: 23,
              flexDirection: 'row',
              borderBottomWidth: 0.5,
              borderBottomColor: item.shipping_carrier
                .toLowerCase()
                .includes('ambil')
                ? '#979797'
                : !orderFormValidation.isShippingCarrierEmpty &&
                  item.shipping_carrier === null &&
                  item.accept === true
                ? '#979797'
                : orderFormValidation.isShippingCarrierEmpty &&
                  item.shipping_carrier === null &&
                  item.accept === true
                ? 'red'
                : Colors.mainBlue,
            }}>
            <Text
              ref={courierTxt}
              style={{
                bottom: 5,
                fontFamily: 'NotoSans',
                fontSize: 14,
                letterSpacing: 0.7,
                color: '#9b9b9b',
              }}>
              {item.shipping_name === undefined &&
              item.shipping_carrier === null
                ? 'Pilih Kurir'
                : item.shipping_name
                ? item.shipping_name
                : item.shipping_carrier}
            </Text>
            <MaterialCommunityIcons
              style={{bottom: 8}}
              color={'#9b9b9b'}
              size={25}
              name={
                isShowCourier && orderListIndex === index
                  ? 'menu-up'
                  : 'menu-down'
              }
            />
          </TouchableOpacity>
          {isShowCourier && orderListIndex === index && (
            <FlatList
              contentContainerStyle={{flexGrow: 1}}
              keyExtractor={(item, index) => index.toString()}
              nestedScrollEnabled
              renderItem={_courierListRenderItem}
              data={tokoCourierData.courierList}
            />
          )}
          {orderFormValidation.isShippingCarrierEmpty &&
            item.shipping_carrier === null &&
            item.accept === true && (
              <Text
                style={[
                  c.Styles.txtInputDesc,
                  {color: 'red', marginTop: isShowCourier ? 10 : 0},
                ]}>
                Wajib memilih Kurir
              </Text>
            )}

          <View
            style={{
              marginTop: 6,
              flexDirection: 'row',
              borderBottomWidth: 0.5,
              borderBottomColor:
                !orderFormValidation.isShippingFeeEmpty &&
                item.shipping_fee === null &&
                item.accept === true
                  ? '#979797'
                  : orderFormValidation.isShippingFeeEmpty &&
                    item.shipping_fee === null &&
                    item.accept === true
                  ? 'red'
                  : Colors.mainBlue,
            }}>
            <TextInputMask
              keyboardType={'number-pad'}
              type={'money'}
              options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp',
                suffixUnit: '',
              }}
              includeRawValueInChangeText={true}
              editable={false}
              value={
                item.shipping_carrier.toLowerCase().includes('ambil')
                  ? 0
                  : swiperIndex === 2 ||
                    swiperIndex === 3 ||
                    swiperIndex === 31 ||
                    swiperIndex === 32
                  ? item.shipping_fee != null
                    ? item.shipping_fee.toString()
                    : item.shipping_fee
                  : item.shipping_fee
              }
              onChangeText={(x, rawText) => {
                handleShippingFeeOrder(
                  item,
                  rawText.length === 0 ||
                    isNaN(rawText) ||
                    item.shipping_carrier.toLowerCase().includes('ambil')
                    ? 0
                    : rawText,
                );
              }}
              placeholder={'Tarif Pengiriman'}
              style={{width: '100%'}}
            />
          </View>
          {item.shipping_carrier.toLowerCase().includes('ambil')
            ? null
            : orderFormValidation.isShippingFeeEmpty &&
              item.shipping_fee === null &&
              item.accept === true && (
                <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                  Wajib mengisi Tarif Pengiriman
                </Text>
              )}
          {(swiperIndex === 3 && item.status != 6) ||
            (swiperIndex === 31 && item.status != 6) ||
            (swiperIndex === 32 && item.status != 6 && (
              <View
                style={{
                  flexDirection: 'row',
                  borderBottomWidth: 0.5,
                  marginTop: 6,
                  borderBottomColor:
                    item.shipping_carrier.toLowerCase().includes('ambil') ||
                    item.status === 7
                      ? 'transparent'
                      : !orderFormValidation.isShippingTraceNoEmpty &&
                        item.shipping_traceno === null
                      ? '#979797'
                      : orderFormValidation.isShippingTraceNoEmpty &&
                        item.shipping_traceno === null &&
                        index === orderFormValidation.index
                      ? 'red'
                      : Colors.mainBlue,
                }}>
                {item.shipping_carrier.toLowerCase().includes('ambil') ||
                item.status === 7 ? null : (
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{width: '100%'}}
                    onPress={() => {
                      console.log(item);
                      handleCopyLink({
                        content: 'No. Resi',
                        value: item.shipping_traceno.toString(),
                      });
                    }}>
                    <TextInput
                      onPress={() => {
                        handleCopyLink({
                          content: 'No. Resi',
                          value: item.shipping_traceno.toString(),
                        });
                      }}
                      editable={
                        item.status === 4 ||
                        item.status === 5 ||
                        item.status === 7 ||
                        item.status === 14
                          ? false
                          : true
                      }
                      value={
                        swiperIndex === 3 ||
                        swiperIndex === 31 ||
                        swiperIndex === 32
                          ? item.shipping_traceno != null
                            ? item.shipping_traceno.toString()
                            : item.shipping_traceno
                          : item.shipping_traceno
                      }
                      onChangeText={(x) =>
                        handleShippingTraceNoOrder(item.id, x)
                      }
                      placeholder={'No. Resi'}
                    />
                  </TouchableOpacity>
                )}
                {(item.status === 4 &&
                  !item.shipping_carrier.toLowerCase().includes('ambil')) ||
                (item.status === 5 &&
                  !item.shipping_carrier.toLowerCase().includes('ambil')) ||
                (item.status === 14 &&
                  !item.shipping_carrier.toLowerCase().includes('ambil')) ? (
                  <TouchableOpacity
                    style={{
                      position: 'absolute',
                      right: 0,
                      justifyContent: 'center',
                      height: '100%',
                    }}
                    onPress={() => Linking.openURL(item.shipping_tracking_web)}>
                    <Text
                      style={{
                        textDecorationLine: 'underline',
                        fontFamily: 'NotoSans-Regular',
                        fontSize: 10,
                        letterSpacing: 0.9,
                        color: c.Colors.mainBlue,
                        marginLeft: 5,
                      }}>
                      LACAK
                    </Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            ))}
          {item.shipping_carrier.toLowerCase().includes('ambil')
            ? null
            : orderFormValidation.isShippingTraceNoEmpty &&
              item.shipping_traceno === null &&
              index === orderFormValidation.index && (
                <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                  Wajib mengisi Nomor Resi
                </Text>
              )}
        </Card>
        <View
          style={{
            alignSelf: 'center',
            marginTop: 15,
            width: '90%',
            borderWidth: 0.6,
            borderColor: '#c9c6c6',
          }}
        />
        <View
          style={{
            marginTop: 15,
            flexDirection: 'row',
            paddingLeft: 25,
            paddingRight: 25,
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'center',
              fontFamily: 'NotoSans',
              fontSize: 15,
              color: '#4a4a4a',
            }}>
            Total
          </Text>
          {/* //kylie */}
          <Text
            style={{
              flex: 1,
              textAlign: 'right',
              fontFamily: 'NotoSans-Bold',
              fontSize: 15,
              color: '#1a69d5',
            }}>
            {item.status === 1 && item.supplier
              ? item.total_amount === undefined
                ? func.rupiah(Number(item.amount) + Number(item.shipping_fee))
                : func.rupiah(
                    Number(item.total_amount) + Number(item.shipping_fee),
                  )
              : item.status === 1 && !item.supplier
              ? item.total_amount === undefined
                ? func.rupiah(Number(item.amount) + Number(item.shipping_fee))
                : item.shipping_carrier.toLowerCase().includes('ambil')
                ? func.rupiah(Number(item.amount))
                : func.rupiah(Number(item.total_amount))
              : item.status === 2
              ? func.rupiah(Number(item.amount) + Number(item.shipping_fee))
              : func.rupiah(Number(item.amount) + Number(item.shipping_fee))}
          </Text>
        </View>
        {swiperIndex != 2 && swiperIndex != 3 && (
          <View
            style={{
              marginTop: 15,
              width: '93%',
              alignSelf: 'center',
              borderRadius: 35,
              height: 60,
              width: '93%',
              // borderWidth: 1,
              // borderColor: Colors.mainBlue,
              flexDirection: 'row',
            }}>
            <CheckBox
              tintColors={{true: Colors.mainBlue, false: 'red'}}
              onTintColor="red"
              onCheckColor="black"
              style={{alignSelf: 'center'}}
              disabled={false}
              value={item.accept ? true : false}
              onValueChange={(newValue) => handleStatusOrder(item.id, newValue)}
            />
            <Text
              style={{
                alignSelf: 'center',
                textAlign: 'center',
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 14,
                color: 'black',
                marginLeft: 5,
              }}>
              Terima Pesanan
            </Text>
            {/* <TouchableOpacity
              onPress={() => handleStatusOrder(item.id, false)}
              delayPressIn={0}
              style={{
                borderTopLeftRadius: 35,
                borderBottomLeftRadius: 35,

                width: '50%',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor:
                  item.accept != undefined
                    ? item.accept === false
                      ? Colors.mainBlue
                      : Colors.white
                    : Colors.white,
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 14,
                  color:
                    item.accept != undefined
                      ? item.accept === false
                        ? Colors.white
                        : Colors.mainBlue
                      : Colors.mainBlue,
                }}>
                Tolak
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleStatusOrder(item.id, true)}
              delayPressIn={0}
              style={{
                borderTopRightRadius: 35,
                borderBottomRightRadius: 35,

                width: '50%',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor:
                  item.accept != undefined
                    ? item.accept === true
                      ? Colors.mainBlue
                      : Colors.white
                    : Colors.mainBlue,
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 14,
                  color:
                    item.accept != undefined
                      ? item.accept === true
                        ? Colors.white
                        : Colors.mainBlue
                      : Colors.white,
                }}>
                Terima
              </Text>
            </TouchableOpacity>
          */}
          </View>
        )}
        {swiperIndex === 3 &&
          swiperIndex === 31 &&
          swiperIndex === 32 &&
          item.status != 4 &&
          item.status != 5 &&
          item.status != 6 &&
          item.status != 7 &&
          item.status != 14 && (
            <View
              style={{
                marginTop: 15,
                alignSelf: 'center',
                borderRadius: 35,
                height: 60,
                width: '93%',
                borderWidth: 1,
                borderColor: Colors.mainBlue,
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                disabled={isSendOrderFetching ? true : false}
                onPress={() => handleSendOrder(item, index)}
                delayPressIn={0}
                style={{
                  borderRadius: 35,

                  width: '100%',
                  height: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.mainBlue,
                }}>
                {isSendOrderFetching ? (
                  <ActivityIndicator color={'white'} size={'large'} />
                ) : (
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      letterSpacing: 1,
                      fontSize: 14,
                      color: Colors.white,
                    }}>
                    Kirim Barang
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          )}
        {item.status === 4 && (
          <View
            style={{
              marginTop: 15,
              alignSelf: 'center',
              borderRadius: 35,
              height: 60,
              width: '93%',
              borderWidth: 1,
              borderColor: Colors.mainBlue,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              disabled={isFinishOrderFetching ? true : false}
              onPress={() => handleFinishOrder(item)}
              delayPressIn={0}
              style={{
                borderRadius: 35,

                width: '100%',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors.mainBlue,
              }}>
              {isFinishOrderFetching ? (
                <ActivityIndicator color={'white'} size={'large'} />
              ) : (
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    letterSpacing: 1,
                    fontSize: 14,
                    color: Colors.white,
                  }}>
                  Pesanan Selesai
                </Text>
              )}
            </TouchableOpacity>
          </View>
        )}
        <View style={{height: 25}} />
      </Card>
    );
  };
  const handleSaveOrderItem = (data) => {
    const dataTemp = {
      order_id: data.orderId,
      shipping_carrier: data.shippingCarrier,
      shipping_fee: data.shippingFee,
      accept: data.isAccept,
    };
    const postDataTemp = [{...dataTemp, ...dataTemp}];
  };
  const _OrderStatusHandler = ({status_pesanan}) => {
    switch (status_pesanan) {
      case 1:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c2dcff',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#1a69d5',
                letterSpacing: 0.7,
              }}>
              Pesanan Baru
            </Text>
          </View>
        );
        break;

      case 2:
        return (
          <View
            style={{
              width: '87%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Pesanan Diterima
            </Text>
          </View>
        );
        break;
      case 3:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#d0021b',
                letterSpacing: 0.7,
              }}>
              Pesanan Ditolak
            </Text>
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const _renderListOrderItem = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: '#ffffff',
          marginTop: 12,
          flexDirection: 'row',
          paddingLeft: 25,
          paddingRight: 25,
        }}>
        {/* <View
          style={{
            position: 'absolute',
            right: 0,
            left: 0,
            top: 9,
            bottom: 0,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{fontFamily: 'NotoSans', fontSize: 12, color: '#9b9b9b'}}>
              {func.rupiah(item.price)}
          
          </Text>
        </View> */}

        <View
          style={{
            width: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Card
            style={{
              width: 47,
              height: 47,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{width: 38, height: 33}}
              source={
                item
                  ? item.image
                    ? {uri: item.image}
                    : Images.noImg
                  : Images.noImg
              }
            />
          </Card>
        </View>
        {item.source_url ? (
          <TouchableOpacity
            onPress={() => {
              console.log(tokoOrderDetailData.transaction.orders, index);
              let data = {
                link: item.source_url,
                name: item.name,
              };
              navigation.navigate('DropshipDetailScreen', {
                params: data,
              });
            }}
            delayPressIn={0}
            style={{
              position: 'absolute',
              right: 20,
              borderWidth: 0.75,
              borderColor: c.Colors.mainBlue,
              borderRadius: 5,
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 9,
                color: c.Colors.mainBlue,
                textAlign: 'center',
                marginTop: 3,
                marginBottom: 3,
                marginLeft: 10,
                marginRight: 10,
              }}>
              Beli Barang
            </Text>
          </TouchableOpacity>
        ) : null}

        <View
          style={{
            width: '53%',
            marginLeft: 8,
            marginTop: 5,
            justifyContent: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: Colors.blueBlue,
                fontSize: 12,
                letterSpacing: 0.7,
              }}>
              {item.quantity}x
            </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#4a4a4a',
                fontSize: 11,
                letterSpacing: 1,
                marginLeft: 5,
              }}>
              {' ' + item.name}
            </Text>
          </View>
          {item.variant_name ? (
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Regular',
                  letterSpacing: 0.28,
                  color: '#818181',
                  fontSize: 10,
                }}>
                {item.variant_name}
              </Text>
            </View>
          ) : null}
        </View>
        <View
          style={{
            width: '29%',
            alignItems: 'flex-end',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              top: 0,
              color: '#4a4a4a',
              fontFamily: 'NotoSans',
              fontSize: 11,
              marginRight: 1,
              marginTop: 6,
              textAlign: 'center',
            }}>
            {func.rupiah(item.price * item.quantity)}
          </Text>
        </View>
      </View>
    );
  };
  const checkIfAllOrdersRejected =
    tokoOrderDetailData != null &&
    tokoOrderDetailData.transaction.orders.filter((x) => x.status === 2);
  const _renderDataKontakItem = ({item, index}) => {
    return (
      <View style={{flexDirection: 'row', marginTop: 12}}>
        <View style={{width: '30%'}}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'NotoSans',
              letterSpacing: 1,
              color: '#9b9b9b',
            }}>
            {item.content}
          </Text>
        </View>

        <View style={{width: '70%'}}>
          <Text
            onPress={() => handleCopyLink(item)}
            style={{
              fontSize: 14,
              fontFamily: 'NotoSans',
              letterSpacing: 1,
              color: '#4a4a4a',
            }}>
            {item.value}
          </Text>
        </View>
      </View>
    );
  };
  const openRBSheet = (editSelected) => {
    refRBSheet.current.open();
    setEditSelected(editSelected);
  };
  const renderOrderContent = () => {
    switch (editSelected) {
      case 'accept':
        return (
          <View>
            <Image
              style={{
                marginTop: 25,
                width: 100,
                height: 100,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={Images.creditcardIcn}
            />
            <Text
              style={{
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#4a4a4a',
                alignSelf: 'center',
                marginTop: 25,
              }}>
              Apakah anda yakin pembayaran sudah diterima ?
            </Text>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 35,
              }}>
              <TouchableOpacity
                onPress={() => refRBSheet.current.close()}
                delayPressIn={0}
                style={{
                  width: '35%',
                  height: 47,
                  borderColor: Colors.mainBlue,
                  borderWidth: 0.5,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 5,
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-SemiBold',
                    fontSize: 14,
                    letterSpacing: 1,
                    color: '#4a4a4a',
                    alignSelf: 'center',
                  }}>
                  Batalkan
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={isOrderAcceptPaymentFetching ? true : false}
                delayPressIn={0}
                onPress={() => handleAcceptPayment(1)}
                style={{
                  width: '35%',
                  height: 47,
                  borderColor: Colors.mainBlue,
                  borderWidth: 0.5,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.mainBlue,
                  margin: 5,
                }}>
                {isOrderAcceptPaymentFetching ? (
                  <ActivityIndicator color={'white'} size={'large'} />
                ) : (
                  <Text
                    style={{
                      fontFamily: 'NotoSans-SemiBold',
                      fontSize: 14,
                      letterSpacing: 1,
                      color: 'white',
                      alignSelf: 'center',
                    }}>
                    Diterima
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        );
        break;
      case 'decline':
        return (
          <View>
            <Image
              style={{
                marginTop: 25,
                width: 100,
                height: 100,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={Images.creditcardIcn}
            />
            <Text
              style={{
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#4a4a4a',
                alignSelf: 'center',
                marginTop: 25,
              }}>
              Apakah anda yakin pembayaran belum diterima ?
            </Text>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 35,
              }}>
              <TouchableOpacity
                onPress={() => refRBSheet.current.close()}
                delayPressIn={0}
                style={{
                  width: '35%',
                  height: 47,
                  borderColor: Colors.mainBlue,
                  borderWidth: 0.5,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 5,
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-SemiBold',
                    fontSize: 14,
                    letterSpacing: 1,
                    color: '#4a4a4a',
                    alignSelf: 'center',
                  }}>
                  Batalkan
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={isOrderAcceptPaymentFetching ? true : false}
                delayPressIn={0}
                onPress={() => handleAcceptPayment(2)}
                style={{
                  width: '35%',
                  height: 47,
                  borderColor: Colors.mainBlue,
                  borderWidth: 0.5,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Colors.mainBlue,
                  margin: 5,
                }}>
                {isOrderAcceptPaymentFetching ? (
                  <ActivityIndicator color={'white'} size={'large'} />
                ) : (
                  <Text
                    style={{
                      fontFamily: 'NotoSans-SemiBold',
                      fontSize: 14,
                      letterSpacing: 1,
                      color: 'white',
                      alignSelf: 'center',
                    }}>
                    Ya
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        );
        break;
      case 'toko-address':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Alamat Toko
              </Text>
              {tokoAddressData.listAddress.length === 0 && !isAddedAddress && (
                <TouchableOpacity
                  onPress={() => setIsAddedAddress(true)}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Alamat
                  </Text>
                </TouchableOpacity>
              )}
              {isAddedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && provinceData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && cityData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && districtData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        style={{height: 300}}
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {isUpdatedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsUpdatedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        style={{height: 300}}
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {!isUpdatedAddress && (
                <FlatList
                  data={tokoAddressData.listAddress.slice(0, 1)}
                  renderItem={_tokoAddressRenderItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      case 'rek-bank':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Rekening Bank
              </Text>

              {isAddedBank ? (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedBank(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 25,
                    }}>
                    Nama Bank
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      props.dispatch(TokoActions.postTokoBankReset());

                      if (bank) {
                        setOpenPickerBank(false);
                      } else {
                        setOpenPickerBank(true);
                      }
                    }}
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankEmpty
                        ? 'red'
                        : bankValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {bankValue != null ? bankValue.name : 'Nama Bank'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!bank ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedBankFormValidation.isBankEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Nama Bank
                    </Text>
                  )}
                  {bank && bankData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={bankData.banks}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                        ? 'red'
                        : bankAccountNumValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      keyboardType={'number-pad'}
                      onChangeText={(text) => {
                        props.dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankAccountNumEmpty: false,
                        });
                        setBankAccountNumValue(text);
                      }}
                      value={bankAccountNumValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nomor Rekening'}
                    />
                  </View>
                  {addedBankFormValidation.isBankAccountNumEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nomor rekening
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                        ? 'red'
                        : bankOwnerNameValue == null || bankOwnerNameValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => {
                        props.dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankOwnerEmpty: false,
                        });

                        setBankOwnerNameValue(text);
                      }}
                      value={bankOwnerNameValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nama Pemilik'}
                    />
                  </View>
                  {addedBankFormValidation.isBankOwnerEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nama pemilik rekening
                    </Text>
                  )}

                  <View style={{height: 14}} />
                  {isErrorTokoBankAlreadyRegistered && (
                    <Text
                      style={[
                        c.Styles.txtInputDesc,
                        {color: 'red', alignSelf: 'center', top: 15},
                      ]}>
                      {isErrorTokoBankAlreadyRegistered.message}
                    </Text>
                  )}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 30,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddBankSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    setBankAccountNumValue(null);
                    setBankValue(null);
                    setBankOwnerNameValue(null);
                    setIsAddedBank(true);
                  }}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,

                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Rekening
                  </Text>
                </TouchableOpacity>
              )}
              {tokoBankData != null && (
                <FlatList
                  extraData={useState}
                  nestedScrollEnabled
                  data={tokoBankData.listRekening}
                  style={{marginTop: 12}}
                  renderItem={_renderBankItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      default:
        null;
    }
  };
  const _handleOrderInfoValue = ({item, index}) => {
    switch (index) {
      case 0:
        return (
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'NotoSans-Bold',
              letterSpacing: 1,
              color: '#4a4a4a',
            }}>
            {item.value}
          </Text>
        );
        break;
      case 1:
        return (
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'NotoSans',
              letterSpacing: 1,
              color: '#4a4a4a',
            }}>
            {item.value}
          </Text>
        );
        break;
      case 2:
        return (
          <View>
            {item.value === 1 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '36%',
                    height: 20,
                    backgroundColor: '#c2dcff',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      color: 'white',
                      fontSize: 10,
                      color: '#1a69d5',
                      letterSpacing: 0.7,
                    }}>
                    Pesanan Baru
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
             */}
              </View>
            ) : item.value === 2 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '47%',
                    height: 20,
                    backgroundColor: '#c3ffd9',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#2b814a',
                      letterSpacing: 0.7,
                    }}>
                    Pesanan Diterima
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
              */}
              </View>
            ) : item.value === 3 || item.value === 31 || item.value === 32 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '58%',
                    height: 20,
                    backgroundColor: '#c3ffd9',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#2b814a',
                      letterSpacing: 0.7,
                    }}>
                    Pembayaran Diterima
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
              */}
              </View>
            ) : item.value === 4 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '40%',
                    height: 20,
                    backgroundColor: '#c3ffd9',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#2b814a',
                      letterSpacing: 0.7,
                    }}>
                    Barang Dikirim
                  </Text>
                </View>

                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
              */}
              </View>
            ) : item.value === 5 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '43%',
                    height: 20,
                    backgroundColor: '#c3ffd9',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#2b814a',
                      letterSpacing: 0.7,
                    }}>
                    Pesanan Selesai
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
             */}
              </View>
            ) : item.value === 6 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '47%',
                    height: 20,
                    backgroundColor: '#ffc2c2',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#d0021b',
                      letterSpacing: 0.7,
                    }}>
                    Pesanan Ditolak
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
             */}
              </View>
            ) : item.value === 7 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '58%',
                    height: 20,
                    backgroundColor: '#ffc2c2',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#d0021b',
                      letterSpacing: 0.7,
                    }}>
                    Pembayaran Ditolak
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
             */}
              </View>
            ) : item.value === 14 ? (
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    marginTop: 4,
                    width: '43%',
                    height: 20,
                    backgroundColor: '#c3ffd9',
                    borderRadius: 5,
                    bottom: 4,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 10,
                      color: '#2b814a',
                      letterSpacing: 0.7,
                    }}>
                    Siap Dicairkan
                  </Text>
                </View>
                {/* <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#1a69d5',
                    marginLeft: 15,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: 'NotoSans',
                      letterSpacing: 1,
                      color: '#1a69d5',
                      top: 7,
                    }}>
                    LIHAT
                  </Text>
                </TouchableOpacity>
             */}
              </View>
            ) : null}
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const handleCleanState = () => {
    props.dispatch(TokoActions.postTokoBankReset());
    props.dispatch(TokoActions.postTokoAddressReset());

    props.dispatch(TokoActions.getCityReset());
    setPostalCodeValue(null);
    setProvinceValue(null);
    setCityValue(null);
    setOpenPickerProvince(false);
    setDistrictValue(null);
    setDetailAddressValue(null);
    setAddressIdValue(0);
    setAddressValue(null);
    setIsAddedAddress(false), setIsUpdatedAddress(false);
    setAddedAddressFormValidation(initialValidation);
    setIsAddedBank(false);
    setOpenPickerBank(false);
    setBankNameValue(null);
    setBankOwnerNameValue(null);
    setBankValue(null);
    setIsUpdatedBank(false);
  };
  const _renderDataOrderInfoItem = ({item, index}) => {
    return (
      <View style={{flexDirection: 'row', marginTop: 12}}>
        <View style={{width: '30%'}}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'NotoSans',
              letterSpacing: 1,
              color: '#9b9b9b',
            }}>
            {item.content}
          </Text>
        </View>

        <View style={{width: '70%'}}>
          <_handleOrderInfoValue item={item} index={index} />
        </View>
      </View>
    );
  };
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
      }}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: 15,
          paddingRight: 15,
          backgroundColor: Colors.mainBlue,
        }}>
        <TouchableOpacity style={{flexDirection: 'row'}} onPress={handleGoBack}>
          <Image
            style={{
              resizeMode: 'contain',
              width: 20,
              height: 50,
              marginRight: 10,
            }}
            source={Images.arrowBack}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: 'white',
            marginLeft: 7,
            alignSelf: 'center',
            fontFamily: 'NotoSans-Bold',
            fontSize: 14,
            letterSpacing: 2.57,
            width: '50%',
          }}
          numberOfLines={1}
          ellipsizeMode="tail">
          {'Detail Pesanan'}
        </Text>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => {
            tokoOrderDetailData.transaction !== null
              ? tokoOrderDetailData.transaction.invoice_url !== null
                ? Linking.openURL(tokoOrderDetailData.transaction.invoice_url)
                : Toast.show('Maaf pdf untuk detail pesanan ini tidak tersedia')
              : Toast.show('Maaf pdf untuk detail pesanan ini tidak tersedia');
          }}
          style={{
            width: 70,
            height: 25,
            backgroundColor: Colors.white,
            borderRadius: 25,
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute',
            right: 10,
          }}>
          <MaterialCommunityIcons
            style={{marginLeft: 10}}
            size={15}
            color={'#1a69d5'}
            name={'arrow-collapse-down'}
          />
          <Text
            style={{
              flex: 1,
              marginLeft: 10,
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1,
              color: '#1a69d5',
            }}>
            PDF
          </Text>
        </TouchableOpacity>

        {/* <TouchableOpacity
          delayPressIn={0}
          style={{position: 'absolute', right: 18}}>
          <MaterialCommunityIcons
            style={{marginLeft: 10}}
            size={24}
            color={'white'}
            name={'share-variant'}
          />
        </TouchableOpacity> */}
      </View>

      <Swiper
        index={swiperIndex}
        ref={swiper}
        loop={false}
        onIndexChanged={(newIndex) => {
          setTimeout(() => {
            setSwiperIndex(newIndex);
          }, 1);
        }}
        scrollEnabled={false}
        showsPagination={false}>
        {/* Auth */}
        <View
          style={{
            width: '100%',
            height: '100%',
          }}>
          <ActivityIndicator
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 80,
            }}
            color={Colors.mainBlue}
            size={'large'}
          />
        </View>
        {/* order detail step 1 */}
        {tokoOrderDetailData === null ? (
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 80,
              }}
              color={Colors.mainBlue}
              size={'large'}
            />
          </View>
        ) : (
          <View>
            <View style={{height: '75%'}}>
              <ScrollView
                removeClippedSubviews={false}
                ref={scrollParentRef}
                style={{flexGrow: 1}}
                scrollEnabled={true}>
                <Card>
                  <View
                    style={{
                      width: '100%',
                      height: 80,
                      backgroundColor: '#ffffff',
                      marginLeft: 0,
                      marginRight: 0,
                      alignItems: 'center',
                      paddingLeft: 25,
                      paddingRight: 25,
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        paddingBottom: 15,
                        width: '100%',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        {tokoOrderDetailData.transaction.invoice_no}
                      </Text>
                      <View
                        style={{
                          position: 'absolute',
                          right: 0,
                        }}>
                        <TouchableOpacity
                          onPress={handleCopyInvoice}
                          delayPressIn={0}>
                          <MaterialCommunityIcons
                            name="content-copy"
                            size={25}
                            color={c.Colors.grayWhite}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  <View style={{marginBottom: 10}}>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Tanggal
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {moment(
                          tokoOrderDetailData != null &&
                            tokoOrderDetailData.transaction.date,
                        ).format('ll')}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Harga Barang
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null &&
                          func.rupiah(tokoOrderDetailData.transaction.amount)}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Biaya Admin
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null
                          ? tokoOrderDetailData.transaction.admin_fee != null
                            ? func.rupiah(
                                tokoOrderDetailData.transaction.admin_fee,
                              )
                            : func.rupiah(0)
                          : func.rupiah(0)}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Total Harga
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null &&
                          func.rupiah(
                            tokoOrderDetailData.transaction.admin_fee +
                              tokoOrderDetailData.transaction.amount,
                          )}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Metode Pembayaran
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData.transaction.payment_method ===
                        'QRIS'
                          ? 'Qris'
                          : 'Transfer Bank'}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                          textTransform: 'capitalize',
                        }}>
                        Pembayaran
                      </Text>
                      <View
                        style={{
                          marginTop: 4,
                          width: '30%',
                          height: 20,
                          backgroundColor:
                            tokoOrderDetailData.transaction.payment_status ===
                            'PAID'
                              ? '#c3ffd9'
                              : '#ffc2c2',
                          borderRadius: 5,
                          bottom: 4,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 10,
                            color:
                              tokoOrderDetailData.transaction.payment_status ===
                              'PAID'
                                ? '#2b814a'
                                : '#d0021b',
                            letterSpacing: 0.7,
                          }}>
                          {tokoOrderDetailData != null &&
                          tokoOrderDetailData.transaction.payment_status ===
                            'PAID'
                            ? 'Lunas'
                            : 'Belum Lunas'}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Card>
                <Card
                  style={{
                    marginTop: 0,
                    width: '100%',
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    paddingLeft: 25,
                    paddingRight: 25,
                    marginRight: 5,
                  }}>
                  <View
                    style={{
                      height: 65,
                      flexDirection: 'row',
                      marginRight: 5,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        flex: 1,
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 14,
                        letterSpacing: 1,
                        color: '#4a4a4a',
                      }}>
                      Kontak Pembeli
                    </Text>

                    {/* <TouchableOpacity
          delayPressIn={0}
          style={{
            position: 'absolute',
            right: 93,
          }}>
          <MaterialCommunityIcons
            style={{marginLeft: 10}}
            size={20}
            color={'#1a69d5'}
            name={'map-marker'}
          />
        </TouchableOpacity> */}
                    <TouchableOpacity
                      onPress={() =>
                        Linking.openURL(
                          `whatsapp://send?&phone=${
                            tokoOrderDetailData != null &&
                            tokoOrderDetailData.transaction.contact.phone
                          }`,
                        ).catch(() =>
                          Linking.openURL(
                            `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                          ),
                        )
                      }
                      delayPressIn={0}
                      style={{
                        position: 'absolute',
                        right: 45,
                      }}>
                      <MaterialCommunityIcons
                        style={{marginLeft: 10}}
                        size={20}
                        color={'#1a69d5'}
                        name={'whatsapp'}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => handleCallNumber()}
                      delayPressIn={0}
                      style={{
                        position: 'absolute',
                        right: 0,
                      }}>
                      <MaterialCommunityIcons
                        style={{marginLeft: 10}}
                        size={20}
                        color={'#1a69d5'}
                        name={'phone'}
                      />
                    </TouchableOpacity>
                  </View>
                  {tokoOrderDetailData != null && (
                    <FlatList
                      keyExtractor={(item, index) => index.toString()}
                      data={dataKontakInfo}
                      renderItem={_renderDataKontakItem}
                    />
                  )}

                  <View style={{height: 25}} />
                </Card>
                <FlatList
                  nestedScrollEnabled
                  ref={orderListRef}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={listOrder}
                  data={tokoOrderDetailData != null && listOrder}
                  renderItem={_orderListRenderItem}
                />
              </ScrollView>
            </View>
            <View style={{height: '25%'}}>
              <Card
                style={{
                  width: '93%',
                  alignSelf: 'center',
                  marginTop: 22,
                }}>
                <View
                  style={{
                    marginTop: 15,
                    flexDirection: 'row',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'left',
                      fontFamily: 'NotoSans',
                      fontSize: 17,
                      color: '#4a4a4a',
                    }}>
                    Total Harga
                  </Text>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 15,
                      color: '#1a69d5',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(
                        tokoOrderDetailData.transaction.admin_fee +
                          tokoOrderDetailData.transaction.amount,
                      )}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    height: 55,
                    marginTop: 15,

                    borderTopWidth: 0.2,
                    borderTopColor: '#c9c6c6',
                  }}>
                  {/* <TouchableOpacity
          delayPressIn={0}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: '50%',
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-SemiBold',
              color: '#4a4a4a',
              fontSize: 15,
              letterSpacing: 1.5,
            }}>
            Tolak
          </Text>
        </TouchableOpacity>
       */}
                  <TouchableOpacity
                    disabled={isOrderProcessFetching ? true : false}
                    delayPressIn={0}
                    onPress={
                      // swiper.current.scrollBy(1);
                      handleProcessOrder
                      // setorderInfo(dataOrderProceed);
                    }
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '100%',
                      backgroundColor: '#1a69d5',
                    }}>
                    {isOrderProcessFetching ? (
                      <ActivityIndicator color={'white'} size={'large'} />
                    ) : (
                      <Text
                        style={{
                          fontFamily: 'NotoSans-SemiBold',
                          color: 'white',
                          fontSize: 15,
                          letterSpacing: 1.5,
                        }}>
                        Proses Pesanan
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              </Card>
            </View>
          </View>
        )}
        {/* order detail step 2 */}
        {tokoOrderDetailData === null ? (
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 80,
              }}
              color={Colors.mainBlue}
              size={'large'}
            />
          </View>
        ) : (
          <View>
            <View
              style={{height: checkIfAllOrdersRejected != 0 ? '70%' : '100%'}}>
              <ScrollView>
                <Card>
                  <View
                    style={{
                      width: '100%',
                      height: 80,
                      backgroundColor: '#ffffff',
                      marginLeft: 0,
                      marginRight: 0,
                      alignItems: 'center',
                      paddingLeft: 25,
                      paddingRight: 25,
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        paddingBottom: 15,
                        width: '100%',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        {tokoOrderDetailData.transaction.invoice_no}
                      </Text>
                      <View
                        style={{
                          position: 'absolute',
                          right: 0,
                        }}>
                        <TouchableOpacity
                          onPress={handleCopyInvoice}
                          delayPressIn={0}>
                          <MaterialCommunityIcons
                            name="content-copy"
                            size={25}
                            color={c.Colors.grayWhite}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  <View style={{marginBottom: 10}}>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Tanggal
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {moment(
                          tokoOrderDetailData != null &&
                            tokoOrderDetailData.transaction.date,
                        ).format('ll')}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Harga Barang
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null &&
                          func.rupiah(tokoOrderDetailData.transaction.amount)}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Biaya Admin
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null
                          ? tokoOrderDetailData.transaction.admin_fee != null
                            ? func.rupiah(
                                tokoOrderDetailData.transaction.admin_fee,
                              )
                            : func.rupiah(0)
                          : func.rupiah(0)}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Total Harga
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData != null &&
                          func.rupiah(
                            tokoOrderDetailData.transaction.admin_fee +
                              tokoOrderDetailData.transaction.amount,
                          )}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                        }}>
                        Metode Pembayaran
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#9b9b9b',
                        }}>
                        {tokoOrderDetailData.transaction.payment_method ===
                        'QRIS'
                          ? 'Qris'
                          : 'Transfer Bank'}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        height: 40,
                        backgroundColor: '#ffffff',
                        marginLeft: 0,
                        marginRight: 0,
                        alignItems: 'center',
                        paddingLeft: 25,
                        paddingRight: 25,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          flex: 1,
                          fontFamily: 'NotoSans',
                          fontSize: 12,
                          letterSpacing: 1,
                          color: '#4a4a4a',
                          textTransform: 'capitalize',
                        }}>
                        Pembayaran
                      </Text>
                      <View
                        style={{
                          marginTop: 4,
                          width: '30%',
                          height: 20,
                          backgroundColor:
                            tokoOrderDetailData.transaction.payment_status ===
                            'PAID'
                              ? '#c3ffd9'
                              : '#ffc2c2',
                          borderRadius: 5,
                          bottom: 4,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 10,
                            color:
                              tokoOrderDetailData.transaction.payment_status ===
                              'PAID'
                                ? '#2b814a'
                                : '#d0021b',
                            letterSpacing: 0.7,
                          }}>
                          {tokoOrderDetailData != null &&
                          tokoOrderDetailData.transaction.payment_status ===
                            'PAID'
                            ? 'Lunas'
                            : 'Belum Lunas'}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Card>
                <Card
                  style={{
                    marginTop: 0,
                    width: '100%',
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    paddingLeft: 25,
                    paddingRight: 25,
                    marginRight: 5,
                  }}>
                  <View
                    style={{
                      height: 65,
                      flexDirection: 'row',
                      marginRight: 5,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        flex: 1,
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 14,
                        letterSpacing: 1,
                        color: '#4a4a4a',
                      }}>
                      Kontak Pembeli
                    </Text>

                    {/* <TouchableOpacity
                delayPressIn={0}
                style={{
                  position: 'absolute',
                  right: 93,
                }}>
                <MaterialCommunityIcons
                  style={{marginLeft: 10}}
                  size={20}
                  color={'#1a69d5'}
                  name={'map-marker'}
                />
              </TouchableOpacity> */}
                    <TouchableOpacity
                      onPress={() =>
                        Linking.openURL(
                          `whatsapp://send?&phone=${
                            tokoOrderDetailData != null &&
                            tokoOrderDetailData.transaction.contact.phone
                          }`,
                        ).catch(() =>
                          Linking.openURL(
                            `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                          ),
                        )
                      }
                      delayPressIn={0}
                      style={{
                        position: 'absolute',
                        right: 45,
                      }}>
                      <MaterialCommunityIcons
                        style={{marginLeft: 10}}
                        size={20}
                        color={'#1a69d5'}
                        name={'whatsapp'}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => handleCallNumber()}
                      delayPressIn={0}
                      style={{
                        position: 'absolute',
                        right: 0,
                      }}>
                      <MaterialCommunityIcons
                        style={{marginLeft: 10}}
                        size={20}
                        color={'#1a69d5'}
                        name={'phone'}
                      />
                    </TouchableOpacity>
                  </View>
                  {tokoOrderDetailData != null && (
                    <FlatList
                      keyExtractor={(item, index) => index.toString()}
                      data={dataKontakInfo}
                      renderItem={_renderDataKontakItem}
                    />
                  )}
                  <View style={{height: 25}} />
                </Card>
                <View
                  style={{
                    marginTop: 0,
                    width: '100%',
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,

                    marginRight: 5,
                  }}>
                  <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    extraData={listOrder}
                    data={tokoOrderDetailData != null && listOrder}
                    renderItem={_orderListRenderItem}
                  />
                  {/* <FlatList data={dataKontak} renderItem={_renderDataKontakItem} />
                   */}
                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: 15,
                      width: '88%',
                      borderWidth: 0.2,
                      borderColor: '#c9c6c6',
                    }}
                  />
                  {/* <FlatList
              style={{paddingLeft: 25, paddingRight: 25}}
              data={orderData}
              renderItem={_renderListOrderItem}
            /> */}
                  {/* <View style={{width: '100%', flexDirection: 'row', marginTop: 25}}>
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,

                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    color: '#9b9b9b',
                    fontSize: 12,
                    marginRight: 54,
                  }}>
                  Total Harga
                </Text>
              </View>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#4a4a4a',
                  fontSize: 12,
                  position: 'absolute',
                  right: 29,
                }}>
                Rp 55.000
              </Text>
            </View>
          */}
                  {/* <Card
              style={{
                width: '93%',
                marginTop: 40,
                padding: 15,
                alignSelf: 'center',
              }}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <Image
                  style={{height: 20, width: 30}}
                  source={Images.blibliCircleIcon}
                />
                <Text
                  style={{
                    color: '#4a4a4a',
                    fontSize: 14,
                    fontFamily: 'NotoSans-Bold',
                    marginLeft: 10,
                  }}>
                  Pilih Pengiriman
                </Text>
              </View>
              <TouchableOpacity
                delayPressIn={0}
                style={{
                  marginTop: 23,
                  flexDirection: 'row',
                  borderBottomWidth: 0.2,
                  borderBottomColor: '#979797',
                }}>
                <Text
                  style={{
                    bottom: 5,
                    fontFamily: 'NotoSans',
                    fontSize: 14,
                    letterSpacing: 0.7,
                    color: '#9b9b9b',
                  }}>
                  Pilih kurir
                </Text>
                <MaterialCommunityIcons
                  style={{bottom: 8}}
                  color={'#9b9b9b'}
                  size={25}
                  name={'menu-down'}
                />
              </TouchableOpacity>
              <View
                style={{
                  marginTop: 6,
                  flexDirection: 'row',
                  borderBottomWidth: 0.2,
                  borderBottomColor: '#979797',
                }}>
                <TextInput
                  placeholder={'Harga Pengiriman'}
                  style={{width: '100%'}}
                />
              </View>
              <View style={{height: 8}} />
            </Card>
            */}
                  {/* <View
              style={{
                alignSelf: 'center',
                marginTop: 15,
                width: '90%',
                borderWidth: 0.6,
                borderColor: '#c9c6c6',
              }}
            />
            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                paddingLeft: 25,
                paddingRight: 25,
              }}>
              <Text
                style={{
                  flex: 1,
                  textAlign: 'center',
                  fontFamily: 'NotoSans',
                  fontSize: 15,
                  color: '#4a4a4a',
                }}>
                Total
              </Text>
              <Text
                style={{
                  flex: 1,
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 15,
                  color: '#1a69d5',
                }}>
                Rp 67.000
              </Text>
            </View>
           */}

                  <View style={{height: 25}} />
                </View>
              </ScrollView>
            </View>
            {checkIfAllOrdersRejected != 0 && (
              <View
                style={{
                  height: '30%',
                }}>
                {tokoOrderDetailData != null &&
                  tokoOrderDetailData.status != 6 && (
                    <Card
                      style={{
                        width: '93%',
                        alignSelf: 'center',
                      }}>
                      <View
                        style={{
                          marginTop: 15,
                          flexDirection: 'row',
                          paddingLeft: 20,
                          paddingRight: 20,
                        }}>
                        <Text
                          style={{
                            flex: 1,
                            textAlign: 'left',
                            fontFamily: 'NotoSans',
                            fontSize: 17,
                            color: '#4a4a4a',
                          }}>
                          Total Harga
                        </Text>
                        <Text
                          style={{
                            flex: 1,
                            textAlign: 'right',
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 15,
                            color: '#1a69d5',
                          }}>
                          {tokoOrderDetailData != null &&
                            func.rupiah(
                              tokoOrderDetailData.transaction.admin_fee +
                                tokoOrderDetailData.transaction.amount,
                            )}
                        </Text>
                      </View>
                      <TouchableOpacity
                        onPress={
                          () => {
                            let checkIfAllOrdersRejected = tokoOrderDetailData.transaction.orders.filter(
                              (x) => x.status === 2,
                            );
                            if (checkIfAllOrdersRejected.length != 0) {
                              // if (!isTokoBankExists) {
                              //   openRBSheet('rek-bank');
                              // } else
                              if (!isTokoAddressExists) {
                                openRBSheet('toko-address');
                              } else {
                                handleWAOrderMessage();
                              }
                            } else {
                              ToastAndroid.show(
                                'Tagihan tidak dapat kami kirimkan, dikarenakan tidak ada pesanan yang diterima',
                                ToastAndroid.SHORT,
                              );
                            }
                          }
                          //
                        }
                        delayPressIn={0}
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          height: 55,
                          marginTop: 15,
                          backgroundColor: '#25d366',
                          borderTopWidth: 0.2,
                          borderTopColor: '#c9c6c6',
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <FontAwesome5
                          style={{margin: 10}}
                          color={'white'}
                          name={'whatsapp'}
                          size={25}
                        />
                        <Text
                          style={{
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 14,
                            letterSpacing: 1,
                            color: '#ffffff',
                          }}>
                          Kirim Tagihan
                        </Text>
                      </TouchableOpacity>

                      <View
                        style={{
                          flexDirection: 'row',
                          width: '100%',
                          height: 55,

                          borderTopWidth: 0.2,
                          borderTopColor: '#c9c6c6',
                        }}>
                        <TouchableOpacity
                          onPress={
                            () => openRBSheet('decline')
                            // swiper.current.scrollBy(1);

                            // setorderInfo(dataOrderPaymentDone);
                          }
                          delayPressIn={0}
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '50%',
                          }}>
                          <Text
                            style={{
                              fontFamily: 'NotoSans-SemiBold',
                              color: '#4a4a4a',
                              fontSize: 15,
                              letterSpacing: 1.5,
                            }}>
                            Batal
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={
                            () =>
                              Linking.openURL(
                                tokoOrderDetailData != null &&
                                  tokoOrderDetailData.transaction.payment_url,
                              ).catch(() => null)

                            // openRBSheet('accept')
                            // handleAcceptPayment(1)
                            // swiper.current.scrollBy(1);

                            // setorderInfo(dataOrderPaymentDone);
                          }
                          delayPressIn={0}
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '50%',
                            backgroundColor: '#1a69d5',
                          }}>
                          <Text
                            style={{
                              fontFamily: 'NotoSans-SemiBold',
                              color: 'white',
                              fontSize: 15,
                              letterSpacing: 1.5,
                              textAlign: 'center',
                            }}>
                            Bayar
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </Card>
                  )}
              </View>
            )}
          </View>
        )}
        {/* order detail step 3 */}
        {tokoOrderDetailData === null ? (
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 80,
              }}
              color={Colors.mainBlue}
              size={'large'}
            />
          </View>
        ) : (
          <ScrollView>
            <Card>
              <View
                style={{
                  width: '100%',
                  height: 80,
                  backgroundColor: '#ffffff',
                  marginLeft: 0,
                  marginRight: 0,
                  alignItems: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    borderBottomWidth: 0.5,
                    paddingBottom: 15,
                    width: '100%',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    {tokoOrderDetailData.transaction.invoice_no}
                  </Text>
                  <View
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}>
                    <TouchableOpacity
                      onPress={handleCopyInvoice}
                      delayPressIn={0}>
                      <MaterialCommunityIcons
                        name="content-copy"
                        size={25}
                        color={c.Colors.grayWhite}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Tanggal
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {moment(
                      tokoOrderDetailData != null &&
                        tokoOrderDetailData.transaction.date,
                    ).format('ll')}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Harga Barang
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(tokoOrderDetailData.transaction.amount)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Biaya Admin
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null
                      ? tokoOrderDetailData.transaction.admin_fee != null
                        ? func.rupiah(tokoOrderDetailData.transaction.admin_fee)
                        : func.rupiah(0)
                      : func.rupiah(0)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Total Harga
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(
                        tokoOrderDetailData.transaction.admin_fee +
                          tokoOrderDetailData.transaction.amount,
                      )}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Metode Pembayaran
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData.transaction.payment_method === 'QRIS'
                      ? 'Qris'
                      : 'Transfer Bank'}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                      textTransform: 'capitalize',
                    }}>
                    Pembayaran
                  </Text>
                  <View
                    style={{
                      marginTop: 4,
                      width: '30%',
                      height: 20,
                      backgroundColor:
                        tokoOrderDetailData.transaction.payment_status ===
                        'PAID'
                          ? '#c3ffd9'
                          : '#ffc2c2',
                      borderRadius: 5,
                      bottom: 4,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 10,
                        color:
                          tokoOrderDetailData.transaction.payment_status ===
                          'PAID'
                            ? '#2b814a'
                            : '#d0021b',
                        letterSpacing: 0.7,
                      }}>
                      {tokoOrderDetailData != null &&
                      tokoOrderDetailData.transaction.payment_status === 'PAID'
                        ? 'Lunas'
                        : 'Belum Lunas'}
                    </Text>
                  </View>
                </View>
              </View>
            </Card>
            <Card
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,
                paddingLeft: 25,
                paddingRight: 25,
                marginRight: 5,
              }}>
              <View
                style={{
                  height: 65,
                  flexDirection: 'row',
                  marginRight: 5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 14,
                    letterSpacing: 1,
                    color: '#4a4a4a',
                  }}>
                  Kontak Pembeli
                </Text>

                {/* <TouchableOpacity
                delayPressIn={0}
                style={{
                  position: 'absolute',
                  right: 93,
                }}>
                <MaterialCommunityIcons
                  style={{marginLeft: 10}}
                  size={20}
                  color={'#1a69d5'}
                  name={'map-marker'}
                />
              </TouchableOpacity> */}
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(
                      `whatsapp://send?&phone=${
                        tokoOrderDetailData != null &&
                        tokoOrderDetailData.transaction.contact.phone
                      }`,
                    ).catch(() =>
                      Linking.openURL(
                        `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                      ),
                    )
                  }
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 45,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'whatsapp'}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => handleCallNumber()}
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 0,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'phone'}
                  />
                </TouchableOpacity>
              </View>
              {tokoOrderDetailData != null && (
                <FlatList
                  keyExtractor={(item, index) => index.toString()}
                  data={dataKontakInfo}
                  renderItem={_renderDataKontakItem}
                />
              )}
              <View style={{height: 25}} />
            </Card>
            <View
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,

                marginRight: 5,
              }}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                extraData={listOrder}
                data={tokoOrderDetailData != null && listOrder}
                renderItem={_orderListRenderItem}
              />
              {/* <FlatList data={dataKontak} renderItem={_renderDataKontakItem} />
               */}
              {/* <View
              style={{
                alignSelf: 'center',
                marginTop: 15,
                width: '88%',
                borderWidth: 0.2,
                borderColor: '#c9c6c6',
              }}
            /> */}

              {/* <View style={{width: '100%', flexDirection: 'row', marginTop: 25}}>
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,

                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    color: '#9b9b9b',
                    fontSize: 12,
                    marginRight: 54,
                  }}>
                  Total Harga
                </Text>
              </View>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#4a4a4a',
                  fontSize: 12,
                  position: 'absolute',
                  right: 29,
                }}>
                Rp 55.000
              </Text>
            </View>
            */}
              {/* <View
              style={{
                alignSelf: 'center',
                marginTop: 15,
                width: '90%',
                borderWidth: 0.6,
                borderColor: '#c9c6c6',
              }}
            /> */}
              {/* <TouchableOpacity
              onPress={() => {
                swiper.current.scrollBy(1);

                setorderInfo(dataOrderDone);
              }}
              delayPressIn={0}
              style={{
                borderRadius: 25,
                marginTop: 20,
                width: '93%',
                height: 50,
                backgroundColor: Colors.mainBlue,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: 'white',
                  fontSize: 14,
                  letterSpacing: 1.2,
                }}>
                Kirim Barang
              </Text>
            </TouchableOpacity>
           */}
              <View style={{height: 25}} />
            </View>
          </ScrollView>
        )}
        {/* order detail step 4 */}
        {tokoOrderDetailData === null ? (
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 80,
              }}
              color={Colors.mainBlue}
              size={'large'}
            />
          </View>
        ) : (
          <ScrollView>
            <Card>
              <View
                style={{
                  width: '100%',
                  height: 80,
                  backgroundColor: '#ffffff',
                  marginLeft: 0,
                  marginRight: 0,
                  alignItems: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    borderBottomWidth: 0.5,
                    paddingBottom: 15,
                    width: '100%',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    {tokoOrderDetailData.transaction.invoice_no}
                  </Text>
                  <View
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}>
                    <TouchableOpacity
                      onPress={handleCopyInvoice}
                      delayPressIn={0}>
                      <MaterialCommunityIcons
                        name="content-copy"
                        size={25}
                        color={c.Colors.grayWhite}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Tanggal
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {moment(
                      tokoOrderDetailData != null &&
                        tokoOrderDetailData.transaction.date,
                    ).format('ll')}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Harga Barang
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(tokoOrderDetailData.transaction.amount)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Biaya Admin
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null
                      ? tokoOrderDetailData.transaction.admin_fee != null
                        ? func.rupiah(tokoOrderDetailData.transaction.admin_fee)
                        : func.rupiah(0)
                      : func.rupiah(0)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Total Harga
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(
                        tokoOrderDetailData.transaction.admin_fee +
                          tokoOrderDetailData.transaction.amount,
                      )}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Metode Pembayaran
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData.transaction.payment_method === 'QRIS'
                      ? 'Qris'
                      : 'Transfer Bank'}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                      textTransform: 'capitalize',
                    }}>
                    Pembayaran
                  </Text>
                  <View
                    style={{
                      marginTop: 4,
                      width: '30%',
                      height: 20,
                      backgroundColor:
                        tokoOrderDetailData.transaction.payment_status ===
                        'PAID'
                          ? '#c3ffd9'
                          : '#ffc2c2',
                      borderRadius: 5,
                      bottom: 4,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 10,
                        color:
                          tokoOrderDetailData.transaction.payment_status ===
                          'PAID'
                            ? '#2b814a'
                            : '#d0021b',
                        letterSpacing: 0.7,
                      }}>
                      {tokoOrderDetailData != null &&
                      tokoOrderDetailData.transaction.payment_status === 'PAID'
                        ? 'Lunas'
                        : 'Belum Lunas'}
                    </Text>
                  </View>
                </View>
              </View>
            </Card>
            <Card
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,
                paddingLeft: 25,
                paddingRight: 25,
                marginRight: 5,
              }}>
              <View
                style={{
                  height: 65,
                  flexDirection: 'row',
                  marginRight: 5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 14,
                    letterSpacing: 1,
                    color: '#4a4a4a',
                  }}>
                  Kontak Pembeli
                </Text>

                {/* <TouchableOpacity
                delayPressIn={0}
                style={{
                  position: 'absolute',
                  right: 93,
                }}>
                <MaterialCommunityIcons
                  style={{marginLeft: 10}}
                  size={20}
                  color={'#1a69d5'}
                  name={'map-marker'}
                />
              </TouchableOpacity> */}
                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(
                      `whatsapp://send?&phone=${tokoOrderDetailData.transaction.contact.phone}`,
                    ).catch(() =>
                      Linking.openURL(
                        `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                      ),
                    )
                  }
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 45,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'whatsapp'}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => handleCallNumber()}
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 0,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'phone'}
                  />
                </TouchableOpacity>
              </View>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={dataKontak}
                renderItem={_renderDataKontakItem}
              />
              <View style={{height: 25}} />
            </Card>
            <Card
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,

                marginRight: 5,
              }}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                style={{paddingLeft: 25, paddingRight: 25}}
                data={orderInfo}
                renderItem={_renderDataOrderInfoItem}
              />
              {/* <FlatList data={dataKontak} renderItem={_renderDataKontakItem} />
               */}
              <View
                style={{
                  alignSelf: 'center',
                  marginTop: 15,
                  width: '88%',
                  borderWidth: 0.2,
                  borderColor: '#c9c6c6',
                }}
              />
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                style={{paddingLeft: 25, paddingRight: 25}}
                data={orderData}
                renderItem={_renderListOrderItem}
              />
              <View
                style={{width: '100%', flexDirection: 'row', marginTop: 25}}>
                <View
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,

                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      color: '#9b9b9b',
                      fontSize: 12,
                      marginRight: 54,
                    }}>
                    Total Harga
                  </Text>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    color: '#4a4a4a',
                    fontSize: 12,
                    position: 'absolute',
                    right: 29,
                  }}>
                  Rp 55.000
                </Text>
              </View>
              <Card
                style={{
                  width: '93%',
                  marginTop: 40,
                  padding: 15,
                  alignSelf: 'center',
                }}>
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <Image
                    style={{height: 20, width: 30}}
                    source={Images.blibliCircleIcon}
                  />
                  <Text
                    style={{
                      color: '#4a4a4a',
                      fontSize: 14,
                      fontFamily: 'NotoSans-Bold',
                      marginLeft: 10,
                    }}>
                    Pilih Pengiriman
                  </Text>
                </View>
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    marginTop: 23,
                    flexDirection: 'row',
                    borderBottomWidth: 0.2,
                    borderBottomColor: '#979797',
                  }}>
                  <Text
                    style={{
                      bottom: 5,
                      fontFamily: 'NotoSans',
                      fontSize: 14,
                      letterSpacing: 0.7,
                      color: '#9b9b9b',
                    }}>
                    Pilih kurir
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 8}}
                    color={'#9b9b9b'}
                    size={25}
                    name={'menu-down'}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    marginTop: 6,
                    flexDirection: 'row',
                    borderBottomWidth: 0.2,
                    borderBottomColor: '#979797',
                  }}>
                  <TextInput
                    placeholder={'Harga Pengiriman'}
                    style={{width: '100%'}}
                  />
                </View>
                <View style={{height: 8}} />
              </Card>
              <View
                style={{
                  alignSelf: 'center',
                  marginTop: 15,
                  width: '90%',
                  borderWidth: 0.6,
                  borderColor: '#c9c6c6',
                }}
              />
              <View
                style={{
                  marginTop: 15,
                  flexDirection: 'row',
                  paddingLeft: 25,
                  paddingRight: 25,
                }}>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'center',
                    fontFamily: 'NotoSans',
                    fontSize: 15,
                    color: '#4a4a4a',
                  }}>
                  Total
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    color: '#1a69d5',
                  }}>
                  Rp 67.000
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  swiper.current.scrollBy(1);
                  setorderInfo(dataOrderFinish);
                }}
                delayPressIn={0}
                style={{
                  borderRadius: 25,
                  marginTop: 20,
                  width: '93%',
                  height: 50,
                  backgroundColor: Colors.mainBlue,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: 'white',
                    fontSize: 14,
                    letterSpacing: 1.2,
                  }}>
                  Pesanan Selesai
                </Text>
              </TouchableOpacity>
              <View style={{height: 25}} />
            </Card>
          </ScrollView>
        )}
        {/* order detail step 5 */}
        {tokoOrderDetailData === null ? (
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <ActivityIndicator
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 80,
              }}
              color={Colors.mainBlue}
              size={'large'}
            />
          </View>
        ) : (
          <ScrollView>
            <Card>
              <View
                style={{
                  width: '100%',
                  height: 80,
                  backgroundColor: '#ffffff',
                  marginLeft: 0,
                  marginRight: 0,
                  alignItems: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    borderBottomWidth: 0.5,
                    paddingBottom: 15,
                    width: '100%',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    {tokoOrderDetailData.transaction.invoice_no}
                  </Text>
                  <View
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}>
                    <TouchableOpacity
                      onPress={handleCopyInvoice}
                      delayPressIn={0}>
                      <MaterialCommunityIcons
                        name="content-copy"
                        size={25}
                        color={c.Colors.grayWhite}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Tanggal
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {moment(
                      tokoOrderDetailData != null &&
                        tokoOrderDetailData.transaction.date,
                    ).format('ll')}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Harga Barang
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(tokoOrderDetailData.transaction.amount)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Biaya Admin
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null
                      ? tokoOrderDetailData.transaction.admin_fee != null
                        ? func.rupiah(tokoOrderDetailData.transaction.admin_fee)
                        : func.rupiah(0)
                      : func.rupiah(0)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Total Harga
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData != null &&
                      func.rupiah(
                        tokoOrderDetailData.transaction.admin_fee +
                          tokoOrderDetailData.transaction.amount,
                      )}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                    }}>
                    Metode Pembayaran
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#9b9b9b',
                    }}>
                    {tokoOrderDetailData.transaction.payment_method === 'QRIS'
                      ? 'Qris'
                      : 'Transfer Bank'}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    height: 40,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#4a4a4a',
                      textTransform: 'capitalize',
                    }}>
                    Pembayaran
                  </Text>
                  <View
                    style={{
                      marginTop: 4,
                      width: '30%',
                      height: 20,
                      backgroundColor:
                        tokoOrderDetailData.transaction.payment_status ===
                        'PAID'
                          ? '#c3ffd9'
                          : '#ffc2c2',
                      borderRadius: 5,
                      bottom: 4,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 10,
                        color:
                          tokoOrderDetailData.transaction.payment_status ===
                          'PAID'
                            ? '#2b814a'
                            : '#d0021b',
                        letterSpacing: 0.7,
                      }}>
                      {tokoOrderDetailData != null &&
                      tokoOrderDetailData.transaction.payment_status === 'PAID'
                        ? 'Lunas'
                        : 'Belum Lunas'}
                    </Text>
                  </View>
                </View>
              </View>
            </Card>
            <Card
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,
                paddingLeft: 25,
                paddingRight: 25,
                marginRight: 5,
              }}>
              <View
                style={{
                  height: 65,
                  flexDirection: 'row',
                  marginRight: 5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 14,
                    letterSpacing: 1,
                    color: '#4a4a4a',
                  }}>
                  Kontak Pembeli
                </Text>

                {/* <TouchableOpacity
                delayPressIn={0}
                style={{
                  position: 'absolute',
                  right: 93,
                }}>
                <MaterialCommunityIcons
                  style={{marginLeft: 10}}
                  size={20}
                  color={'#1a69d5'}
                  name={'map-marker'}
                />
              </TouchableOpacity> */}
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 45,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'whatsapp'}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    position: 'absolute',
                    right: 0,
                  }}>
                  <MaterialCommunityIcons
                    style={{marginLeft: 10}}
                    size={20}
                    color={'#1a69d5'}
                    name={'phone'}
                  />
                </TouchableOpacity>
              </View>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={dataKontak}
                renderItem={_renderDataKontakItem}
              />
              <View style={{height: 25}} />
            </Card>
            <Card
              style={{
                marginTop: 0,
                width: '100%',
                backgroundColor: '#ffffff',
                marginLeft: 0,
                marginRight: 0,

                marginRight: 5,
              }}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                style={{paddingLeft: 25, paddingRight: 25}}
                data={orderInfo}
                renderItem={_renderDataOrderInfoItem}
              />
              {/* <FlatList data={dataKontak} renderItem={_renderDataKontakItem} />
               */}
              <View
                style={{
                  alignSelf: 'center',
                  marginTop: 15,
                  width: '88%',
                  borderWidth: 0.2,
                  borderColor: '#c9c6c6',
                }}
              />
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                style={{paddingLeft: 25, paddingRight: 25}}
                data={orderData}
                renderItem={_renderListOrderItem}
              />
              <View
                style={{width: '100%', flexDirection: 'row', marginTop: 25}}>
                <View
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,

                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      color: '#9b9b9b',
                      fontSize: 12,
                      marginRight: 54,
                    }}>
                    Total Harga
                  </Text>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    color: '#4a4a4a',
                    fontSize: 12,
                    position: 'absolute',
                    right: 29,
                  }}>
                  Rp 55.000
                </Text>
              </View>
              <Card
                style={{
                  width: '93%',
                  marginTop: 40,
                  padding: 15,
                  alignSelf: 'center',
                }}>
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <Image
                    style={{height: 20, width: 30}}
                    source={Images.blibliCircleIcon}
                  />
                  <Text
                    style={{
                      color: '#4a4a4a',
                      fontSize: 14,
                      fontFamily: 'NotoSans-Bold',
                      marginLeft: 10,
                    }}>
                    Pilih Pengiriman
                  </Text>
                </View>
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    marginTop: 23,
                    flexDirection: 'row',
                    borderBottomWidth: 0.2,
                    borderBottomColor: '#979797',
                  }}>
                  <Text
                    style={{
                      bottom: 5,
                      fontFamily: 'NotoSans',
                      fontSize: 14,
                      letterSpacing: 0.7,
                      color: '#9b9b9b',
                    }}>
                    Pilih kurir
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 8}}
                    color={'#9b9b9b'}
                    size={25}
                    name={'menu-down'}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    marginTop: 6,
                    flexDirection: 'row',
                    borderBottomWidth: 0.2,
                    borderBottomColor: '#979797',
                  }}>
                  <TextInput
                    placeholder={'Harga Pengiriman'}
                    style={{width: '100%'}}
                  />
                </View>
                <View style={{height: 8}} />
              </Card>
              <View
                style={{
                  alignSelf: 'center',
                  marginTop: 15,
                  width: '90%',
                  borderWidth: 0.6,
                  borderColor: '#c9c6c6',
                }}
              />
              <View
                style={{
                  marginTop: 15,
                  flexDirection: 'row',
                  paddingLeft: 25,
                  paddingRight: 25,
                }}>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'center',
                    fontFamily: 'NotoSans',
                    fontSize: 15,
                    color: '#4a4a4a',
                  }}>
                  Total
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    color: '#1a69d5',
                  }}>
                  Rp 50000
                </Text>
              </View>
              <View style={{height: 25}} />
            </Card>
          </ScrollView>
        )}
      </Swiper>
      <RBSheet
        onClose={handleCleanState}
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            height:
              editSelected === 'decline' || editSelected === 'accept'
                ? Dimensions.get('window').height * 0.5
                : Dimensions.get('window').height * 0.9,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <View
          style={{
            paddingHorizontal: 20,
            marginBottom: 50,
            height: '100%',
          }}>
          <KeyboardAvoidingView
            style={{flex: 1, flexDirection: 'column'}}
            keyboardVerticalOffset={-15}
            behavior="padding"
            enabled>
            {renderOrderContent()}
          </KeyboardAvoidingView>
        </View>
      </RBSheet>
      {snackBar.show && (
        <Snackbar
          visible={snackBar.show}
          style={{zIndex: 10}}
          duration={3000}
          onDismiss={() => setSnackBar({show: false, message: ''})}
          action={{
            label: 'OK',
            onPress: () => setSnackBar({show: false, message: ''}),
          }}>
          {snackBar.message}
        </Snackbar>
      )}
    </View>
  );
};

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    isErrorTokoBankAlreadyRegistered: state.toko.postTokoBankData.error,

    bankData: state.toko.bankData.data,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,
    tokoBankData: state.toko.tokoBankData.data,

    isTokoAddressExists:
      state.toko.tokoAddressData.data != null
        ? state.toko.tokoAddressData.data.listAddress.length != 0
          ? true
          : false
        : false,
    isTokoBankExists:
      state.toko.tokoBankData.data != null
        ? state.toko.tokoBankData.data.listRekening.length != 0
          ? true
          : false
        : false,
    isOrderProcessFetching: state.toko.postTokoOrderProceedData.fetching,
    isOrderAcceptPaymentFetching: state.toko.postTokoAcceptPaymentData.fetching,
    isSendOrderFetching: state.toko.postTokoSendOrderData.fetching,
    isFinishOrderFetching: state.toko.postTokoFinishOrderData.fetching,
    tokoData: state.toko.tokoData.data,
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoCourierData: state.toko.tokoCourierData.data,
    tokoOrderDetailData: state.toko.tokoOrderDetailData.data,
    tokoOrderDetailDataFetching: state.toko.tokoOrderDetailData.fetching,

    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    isSendOrderSuccess: state.toko.postTokoSendOrderData.success,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailScreen);
