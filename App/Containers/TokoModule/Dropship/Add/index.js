import React, {useEffect, useRef, useState, useCallback} from 'react';
import {
  Body,
  Card,
  Container,
  Content,
  Header,
  Left,
  Right,
  Tab,
  Tabs,
  Button,
  View,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import CheckBox from '@react-native-community/checkbox';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {Header as H} from 'react-navigation-stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import _ from 'lodash';
import TokoActions from '../../../../Redux/TokoRedux';
import {
  BackHandler,
  Text,
  Image,
  KeyboardAvoidingView,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {
  FlatList,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import * as c from '../../../../Components';
import {connect} from 'react-redux';
import {Images} from '../../../../Themes';
import * as func from '../../../../Helpers/Utils';
import styles from './styles';
import {T} from 'ramda';
const initialProductForm = {
  name: null,
  description: null,

  //without varian
  capitalPrice: null,
  price: null,
  discount: null,
  stock: null,
  weight: null,
  source_url: null,
  video_url: null,
  //with varian
  isVarian: false,
  images: [],
  category: null,
  addCategory: false,
  newCategory: null,
  categorySelected: null,
  inputOtherBusinessType: null,
  //   isEdit: false,
  //   productId: null,
};
const initialValidation = {
  isImageEmpty: false,
  isNameEmpty: false,
  isPriceEmpty: false,
  isCategoryEmpty: false,
  isVarianEmpty: false,
};
const initialFilter = {
  isFiltered: false,
  categoryId: null,
  productFiltered: [],
};
const dataNull = [{}];
function AddProductDropship(props) {
  const {
    navigation,
    dispatch,
    categoryData,
    tokoId,
    AddNewCategoryId,
    checkSuccessAddNewCategory,
    productData,
    getToko,
    checkSuccessScrap,
  } = props;
  const getParams = navigation.state.params.params;
  console.log('item', getParams);
  const [productForm, setProductForm] = useStateWithCallbackLazy(
    initialProductForm,
  );
  const scrollParentRef = useRef(null);
  const [productValidation, setProductValidation] = useState(initialValidation);
  const [tabIndex, setTabIndex] = useState(0);
  const [filterProduct, setFilterProduct] = useState(initialFilter);
  const [rupiahChoosed, setrupiahChoosed] = useState(false);
  const [percentChoosed, setpercentChoosed] = useState(true);

  const [rupiahsChoosed, setrupiahsChoosed] = useState(false);
  const [percentsChoosed, setpercentsChoosed] = useState(true);
  const [valuetambahHarga, setvaluetambahHarga] = useState(null);

  const [ketName, setketName] = useState('');
  const scrollviewRef = useRef(null);
  const [loadingVarian, setloadingVarian] = useState(false);

  const [imagesTemp, setImagesTemp] = useState([]);
  const [imagesVarian, setimagesVarian] = useState([]);
  const [deletedImages, setDeletedImages] = useState([]);
  const [addVarian, setaddVarian] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [key, setKey] = useState(null);
  const [dataVarian, setdataVarian] = useState([]);
  const [dataVariasi, setdataVariasi] = useState([]);
  const [dataVariasiAwal, setdataVariasiAwal] = useState([]);
  const [varian, setVarian] = useState([]);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [categoryName, setCategoryName] = useState(null);
  const [ketVariasi, setketVariasi] = useState([
    {
      img: null,
      name: null,
      price: null,
      capitalPrice: null,
      stock: null,

      status: true,
      isVarian: false,
    },
  ]);
  const isTokoAvailable = getToko
    ? getToko.listToko
      ? getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  let varianData = [];
  let dataTest = {};
  const saveVariant = () => {
    setloadingVarian(true);
    for (var i = 0; i < ketVariasi.length; i++) {
      setaddVarian(false);

      varianData[i] = {
        name: productForm.varianName
          ? productForm.varianName
          : 'Tanpa Keterangan',
        options: [...ketVariasi],
      };
    }
    varianData = varianData.filter(
      (v, i, a) => a.findIndex((t) => t.name === v.name) === i,
    );
    setTimeout(() => {
      setloadingVarian(false);
      setisEdit(false);
      if (dataVarian.length < 1) {
        setdataVarian(varianData);
      } else if (addVarian && isEdit) {
        let dataEdit = [varianData[0], ...dataVarian];
        let dataFilter = varianData;
        varianData = dataEdit.filter(
          (v, i, a) => a.findIndex((t) => t.name === v.name) === i,
        );
        dataFilter = filterImage(varianData);
        filterData(varianData, dataFilter);
        console.log('variandData', varianData);
        setdataVarian(varianData);
      } else {
        setdataVarian([...dataVarian, varianData[0]]);
      }
      setisEdit(false);
      setProductForm({...productForm, varianName: null});
      setketVariasi([
        {
          img: null,
          name: null,
          price: null,
          capitalPrice: null,
          stock: null,

          status: false,
        },
      ]);
    }, 200);
  };

  const filterData = (dataFinal, dataFilter) => {
    let uploadImageVarian = [];
    dataFinal.forEach((value, index) => {
      dataFilter.forEach((df, index) => {
        df.options.forEach((v, i) => {
          const formVarian = new FormData();
          formVarian.append('image', {
            uri: v.img.uri,
            name: v.img.name,
            type: v.img.type,
          });

          uploadImageVarian = {
            data: {
              tokoId: tokoId,
              item: formVarian,
              func: {
                reloadData: (latest) => {},
              },
            },
            dataFinal: dataFinal,
            name: v.name,
            parentName: value.name,
          };
          console.log('formVarian', formVarian, uploadImageVarian);
          dispatch(TokoActions.uploadProductImageRequest(uploadImageVarian));
        });
      });
    });
  };
  const filterImage = (arr) => {
    console.log('array', arr);
    return arr.map((v) => {
      return {
        name: v.name,
        options: v.options.filter(
          (item) => typeof item.img === 'object' && item.img !== null,
        ),
      };
    });
  };

  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    dispatch(TokoActions.getProductTokoRequest(tokoId));
    setFilterProduct(initialFilter);
    setProductForm(initialProductForm);
    setProductValidation(initialValidation);
    navigation.goBack(null);
    setCategoryName(null);
    setImagesTemp([]);
    setdataVarian([]);
    setdataVariasi([]);
  };
  const handleAddTokoCategory = () => {
    if (productForm.newCategory === '') {
      Toast.show('Isikan Nama Kategori Terlebih Dahulu !');
    } else {
      const data = {
        tokoId: props.tokoId,
        item: {name: productForm.newCategory},
        func: {
          reloadData: (response) => {
            setProductForm({
              ...productForm,
              addCategory: false,
              categorySelected: response.productCategory.id,
            });
            setProductValidation({
              ...productValidation,
              isCategoryEmpty: false,
            });
            dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
          },
        },
      };
      dispatch(TokoActions.postCategoryTokoRequest(data));
    }
  };
  const handleCategory = (item) => {
    setProductForm({
      ...productForm,
      categorySelected: item,
      addCategory: false,
    });
    setProductValidation({...productValidation, isCategoryEmpty: false});
  };

  const categoryButton = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleCategory(item.id);
          setCategoryName(item.name);
        }}
        key={index}>
        <View
          style={{
            backgroundColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : 'white',
            borderColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                productForm.categorySelected === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight:
                productForm.categorySelected === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const handleAddNewProduct = () => {
    console.log('addnew product');
    setProductValidation({
      isImageEmpty: imagesTemp.length === 0 ? true : false,
      isNameEmpty: !productForm.name ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? true
          : false,
      isCategoryEmpty: !productForm.categorySelected ? true : false,
      isVarianEmpty:
        productForm.isVarian && dataVarian.length < 1 ? true : false,
    });

    if (
      productData.image.length &&
      productForm.name &&
      productForm.price &&
      categoryName
    ) {
      if (productForm.newCategory != '' && productForm.addCategory) {
        handleAddTokoCategory();
      } else if (productForm.isVarian && dataVarian.length < 1) {
      } else {
        let urlImages = [];
        let urlVarian = [];

        imagesTemp.forEach((img, i) => {
          const form = new FormData();
          form.append('image', {
            uri: img ? img.uri : Images.noImg,
            name: img.name,
            type: img.type,
          });
          console.log('form', form);
          const formVarian = new FormData();
          let uploadImageVarian = {};
          if (imagesVarian.length > 0) {
            imagesVarian.forEach((img, i) => {
              formVarian.append('image', {
                uri: img ? img.uri : '',
                name: img.name,
                type: img.type,
              });
              uploadImageVarian = {
                tokoId: tokoId,
                item: formVarian,
                func: {
                  reloadData: (url) => {
                    console.log('url', url);
                    urlVarian[i] = url;
                  },
                },
              };
            });
          }
          let dataMap = [];
          dataMap = varian.map((o) => {
            return {
              name: o.name,
              options: o.options.map((v) => {
                return {
                  name: v.name,
                  image: urlVarian,
                };
              }),
            };
          });
          const uploadData = {
            tokoId: tokoId,
            item: form,
            func: {
              reloadData: (url) => {
                console.log(url);
                urlImages[i] = url;
                const dataWithoutVarian = {
                  id: tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    price:
                      typeof productForm.price === 'string'
                        ? Number(productForm.price.replace(/[Rp.]/g, ''))
                        : productForm.price,
                    capital_price:
                      typeof productForm.capitalPrice === 'string'
                        ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                        : productForm.capitalPrice,
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,

                    source_url: productForm.source_url,
                    weight:
                      productForm.weight === null ? 100 : productForm.weight,
                    stock: productForm.stock === null ? 1 : productForm.stock,
                  },
                  isVarian: productForm.isVarian,
                  func: {
                    reloadData: handleRefresh,
                    reloadFailed: setloadingVarian,
                  },
                };
                const dataWithVarian = {
                  id: tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,

                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                    weight: parseInt(productForm.weight),
                    source_url: productForm.source_url,
                    variants: {
                      models: dataMap,
                      details: dataVariasi,
                    },
                  },
                  isVarian: productForm.isVarian,
                  func: {
                    reloadData: handleRefresh,
                    reloadFailed: setloadingVarian,
                  },
                };
                if (i >= imagesTemp.length - 1 && i >= urlImages.length - 1) {
                  console.log('variants', dataWithVarian);
                  dispatch(
                    TokoActions.postProductTokoRequest(
                      tabIndex === 1 ? dataWithVarian : dataWithoutVarian,
                    ),
                  );
                }
              },
            },
          };
          if (imagesVarian.length > 0) {
            console.log('kepanggil');
            dispatch(TokoActions.uploadProductImageRequest(uploadImageVarian));
          }
          dispatch(TokoActions.uploadProductImageRequest(uploadData));
        });
      }
    } else if (
      imagesTemp.length === 0 ||
      !productForm.categorySelected ||
      !productForm.name
    ) {
      scrollParentRef.current.scrollTo({
        x: 10,
        y: 10 * 1,
        animated: true,
      });
    } else {
      scrollParentRef.current.scrollTo({
        x: 500,
        y: 500 * 1,
        animated: true,
      });
    }
  };
  const handleAddProduct = () => {
    console.log('tabIndex', tabIndex);
    setProductValidation({
      isImageEmpty: imagesTemp.length === 0 ? true : false,
      isNameEmpty: !productForm.name ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? true
          : false,
      isCategoryEmpty: !productForm.categorySelected ? true : false,
      isVarianEmpty:
        productForm.isVarian && dataVarian.length < 1 ? true : false,
    });

    if (
      imagesTemp.length > 0 &&
      productForm.name &&
      ((productForm.price === null && !productForm.isVarian) ||
      (productForm.price === 'Rp0' && !productForm.isVarian) ||
      (productForm.price === '' && !productForm.isVarian) ||
      (productForm.price == 0 && !productForm.isVarian)
        ? false
        : true) &&
      productForm.categorySelected
    ) {
      if (productForm.newCategory != '' && productForm.addCategory) {
        handleAddTokoCategory();
      } else if (tabIndex === 1 && dataVarian.length < 1) {
        Toast.show('Masukkan varian terlebih dahulu');
      } else {
        setloadingVarian(true);
        let dataMap = [];
        let urlVarian = [];
        let urlImages = [];
        productData.images.forEach((v, i) => {
          urlImages[i] = v.url;
        });
        // dataMap = varian.map((o) => {
        //   return {
        //     name: o.name,
        //     options: o.options.map((v) => {
        //       return {
        //         name: v.name,
        //         image: urlVarian,
        //       };
        //     }),
        //   };
        // });
        const dataWithoutVarian = {
          id: tokoId,
          item: {
            name: productForm.name,
            description: productForm.description,
            price:
              typeof productForm.price === 'string'
                ? Number(productForm.price.replace(/[Rp.]/g, ''))
                : productForm.price,
            capital_price:
              typeof productForm.capitalPrice === 'string'
                ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                : productForm.capitalPrice,
            category_id:
              AddNewCategoryId != null
                ? AddNewCategoryId.productCategory != undefined
                  ? AddNewCategoryId.productCategory.id
                  : productForm.categorySelected
                : productForm.categorySelected,
            images: urlImages
              ? urlImages.length > 5
                ? urlImages.slice(0, 5)
                : urlImages
              : null,

            source_url: productForm.source_url,
            weight: productForm.weight === null ? 100 : productForm.weight,
            stock: productForm.stock === null ? 1 : productForm.stock,
            supplier_id: getParams.id,
          },
          isVarian: productForm.isVarian,
          func: {
            reloadData: handleRefresh,
            reloadFailed: setloadingVarian,
          },
        };
        const dataWithVarian = {
          id: tokoId,
          item: {
            name: productForm.name,
            description: productForm.description,

            category_id:
              AddNewCategoryId != null
                ? AddNewCategoryId.productCategory != undefined
                  ? AddNewCategoryId.productCategory.id
                  : productForm.categorySelected
                : productForm.categorySelected,
            images: urlImages
              ? urlImages.length > 5
                ? urlImages.slice(0, 5)
                : urlImages
              : null,
            weight: parseInt(productForm.weight),
            source_url: productForm.source_url,
            supplier_id: getParams.id,
            variants: {
              models: dataVarian,
              details: dataVariasi,
            },
          },
          isVarian: productForm.isVarian,
          func: {
            reloadData: handleRefresh,
            reloadFailed: setloadingVarian,
          },
        };
        console.log('lala');
        dispatch(
          TokoActions.postProductTokoRequest(
            tabIndex === 1 ? dataWithVarian : dataWithoutVarian,
          ),
        );
      }
    } else if (
      imagesTemp.length === 0 ||
      !productForm.categorySelected ||
      !productForm.name
    ) {
      scrollParentRef.current.scrollTo({
        x: 10,
        y: 10 * 1,
        animated: true,
      });
    } else {
      scrollParentRef.current.scrollTo({
        x: 500,
        y: 500 * 1,
        animated: true,
      });
    }
  };
  const addHandler = () => {
    const _ketVariasi = [...ketVariasi];
    _ketVariasi.push({
      img: null,
      name: null,
      price: null,
      capitalPrice: null,
      stock: null,

      status: false,
    });
    setketVariasi(_ketVariasi);
  };
  const deleteHandler = (key) => {
    const _ketVariasi = ketVariasi.filter((input, index) => index != key);
    setketVariasi(_ketVariasi);
  };
  const _renderHeader = (item, index) => {
    return (
      <View>
        <ScrollView>
          <View style={styles.viewheaderlistVarian}>
            <View
              style={{
                width: Dimensions.get('screen').width * 0.15,
              }}>
              <Text style={styles.headerlistVarian}>Aktif</Text>
            </View>
            <View
              style={{
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 3,
              }}>
              <Text style={styles.headerlistVarian}>Variasi</Text>
            </View>
            <View
              style={{
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 5,
              }}>
              <Text style={styles.headerlistVarian}>Harga Jual *</Text>
            </View>
            <View
              style={{
                width: Dimensions.get('screen').width * 0.26,
                marginRight: 3,
              }}>
              <Text style={styles.headerlistVarian}>Harga Modal</Text>
            </View>
            <View
              style={{
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 3,
              }}>
              <Text style={styles.headerlistVarian}>Stok</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };
  const _renderItem = (item, index) => {
    const data = item.item;
    // console.log(
    //   parseInt(valuetambahHarga) + parseInt(dataVariasi[item.index].price),
    //   'lala',
    // );
    // const finalPrice = (total) => {
    //   console.log('total', total);
    // };
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() =>
          scrollviewRef.current.scrollTo({x: 0, y: 0, animated: true})
        }>
        <ScrollView horizontal>
          <View style={styles.viewlistVarian}>
            <View
              pointerEvents="none"
              style={{
                width: Dimensions.get('screen').width * 0.15,
                marginRight: 5,
              }}>
              <CheckBox
                disabled={false}
                value={data.status}
                onValueChange={(newValue) => {
                  const _dataVariasi = [...dataVariasi];
                  _dataVariasi[item.index].status = newValue;
                  setdataVariasi(_dataVariasi);
                }}
              />
            </View>
            <View
              pointerEvents="none"
              style={{
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 5,
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Regular',
                  fontSize: 11,
                  letterSpacing: 0.76,
                  color: '#4a4a4a',
                }}>
                {data.code}
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View
                style={{
                  width: Dimensions.get('screen').width * 0.25,
                  marginRight: 5,
                  justifyContent: 'center',
                }}>
                <TextInput
                  returnKeyType="next"
                  //onSubmitEditing={() => this.description.focus()}
                  keyboardType="number-pad"
                  style={
                    data.price
                      ? [styles.txtinputExist, {paddingVertical: 2}]
                      : [
                          styles.txtinputNull,

                          {paddingHorizontal: 30, paddingVertical: 2},
                        ]
                  }
                  onChangeText={(text) => {
                    let total = text.replace(/\D/g, '');
                    const _dataVariasi = [...dataVariasi];
                    dataVariasi[item.index].price = total;
                    setdataVariasi(_dataVariasi);
                  }}
                  value={func.rupiah(dataVariasi[item.index].price)}
                  placeholderTextColor={'#bdbdbd'}
                  placeholder={'0'}
                />
                {data.price ? null : (
                  <Text
                    style={[
                      styles.txtinputExist,
                      {
                        position: 'absolute',
                        borderWidth: 0,
                      },
                    ]}>
                    Rp
                  </Text>
                )}
              </View>
            </TouchableWithoutFeedback>
            <View
              pointerEvents="none"
              style={{
                justifyContent: 'center',
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 5,
              }}>
              <TextInput
                //onSubmitEditing={() => this.price.focus()}
                editable={false}
                returnKeyType="next"
                keyboardType="number-pad"
                style={
                  data.capital_price
                    ? [
                        styles.txtinputExist,
                        {paddingVertical: 2, color: '#bdbdbd'},
                      ]
                    : [
                        styles.txtinputNull,
                        {
                          paddingHorizontal: 30,
                          paddingVertical: 2,
                          color: '#bdbdbd',
                        },
                      ]
                }
                // onChangeText={(text) => {
                //   let total = text.replace(/\D/g, '');
                //   const _dataVariasi = [...dataVariasi];

                //   _dataVariasi[item.index].capital_price = total;
                //   setdataVariasi(_dataVariasi);
                // }}
                value={
                  data.capital_price
                    ? func.rupiah(data.capital_price)
                    : data.price
                }
                placeholderTextColor={'#bdbdbd'}
                placeholder={'0'}
              />
              {data.capital_price ? null : (
                <Text
                  style={[
                    styles.txtinputExist,
                    {
                      position: 'absolute',
                      borderWidth: 0,
                    },
                  ]}>
                  Rp
                </Text>
              )}
            </View>
            <View
              style={{
                justifyContent: 'center',
                width: Dimensions.get('screen').width * 0.25,
                marginRight: 5,
              }}>
              <TextInput
                keyboardType="number-pad"
                returnKeyType="next"
                //onSubmitEditing={() => this.description.focus()}
                style={
                  data.stock
                    ? [styles.txtinputExist, {paddingVertical: 2}]
                    : [styles.txtinputNull, {paddingVertical: 2}]
                }
                onChangeText={(text) => {
                  console.log(text);
                  const _dataVariasi = [...dataVariasi];

                  _dataVariasi[item.index].stock = text;
                  setdataVariasi(_dataVariasi);
                }}
                value={data.stock}
                placeholder="Stok"
                placeholderTextColor={'#bdbdbd'}
              />
            </View>
          </View>
        </ScrollView>
      </TouchableOpacity>
    );
  };
  const productImageList = ({item, index}) => {
    return (
      <TouchableOpacity style={styles.contentView}>
        <Card style={styles.addPhoto}>
          <Image
            style={styles.imgPhoto}
            source={item.url ? {uri: item.url} : Images.noImg}
          />
        </Card>
      </TouchableOpacity>
    );
  };

  const handlePickImage = (source) => {
    if (source === 'gallery') {
      ImagePicker.openPicker({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
        multiple: addVarian && key !== null ? false : true,
        mediaType: 'photo',
      }).then((image) => {
        console.log('image', image);
        let dataImg = {
          name: image.modificationDate,
          uri: image.path,
          type: image.mime,
        };
        let dataImages = {};
        if (key !== null) {
          inputImage(dataImg, key);
        } else {
          setProductValidation({
            ...productValidation,
            isImageEmpty: false,
          });

          dataImages = image.flatMap((v) => {
            return {
              name: v.modificationDate,
              uri: v.path,
              type: v.mime,
            };
          });
          let dataFilter = [...imagesTemp, dataImages];
          dataFilter = dataFilter.flatMap((v) => {
            return v;
          });
          console.log('dataFilter', dataFilter);
          setImagesTemp(dataFilter);
        }

        setShowUploadModal(false);
      });
    } else if (source === 'camera') {
      ImagePicker.openCamera({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
        mediaType: 'photo',
        multiple: true,
      }).then((image) => {
        console.log('image', image);
        let dataImg = {
          name: image.modificationDate,
          uri: image.path,
          type: image.mime,
        };
        setProductValidation({
          ...productValidation,
          isImageEmpty: false,
        });
        setImagesTemp([...imagesTemp, dataImg]);

        setShowUploadModal(false);
      });
    }
  };

  const handleRemoveImage = (item) => {
    console.log('itemdeleted', item);
    const filtered = imagesTemp.filter((e) => {
      return e !== item;
    });
    console.log('filtered', filtered);
    setImagesTemp(filtered);

    // if (productForm.isEdit && item.hasOwnProperty('priority')) {
    //   productForm.isEdit && setDeletedImages([...deletedImages, item.id]);
    // }
  };
  const productImageListFooter = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => setShowUploadModal(true)}
        style={{paddingTop: 20, paddingBottom: 20, paddingRight: 20}}>
        <Card style={styles.addPhoto}>
          <Image style={styles.imgnoPhoto} source={Images.noImg} />
        </Card>
      </TouchableOpacity>
    );
  };
  useEffect(() => {
    console.log(ketVariasi);
  }, [ketVariasi]);
  const inputHandler = (text, key) => {
    const _ketVariasi = [...ketVariasi];
    console.log('ketVariasi', _ketVariasi);
    _ketVariasi[key].name = text;
    _ketVariasi[key].key = key;

    setketVariasi(_ketVariasi);
  };
  const inputImage = (img, key) => {
    const _ketVariasi = [...ketVariasi];
    _ketVariasi[key].img = img;
    setketVariasi(_ketVariasi);
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    console.log('productForm', productForm);
  }, [productForm]);
  useEffect(() => {
    if (productData.length != 0) {
      console.log('productData', productData, productData.variants);
      setProductForm({
        ...productForm,
        name: productData.name,
        price: productData.price,
        capitalPrice: productData.price,
        weight: productData.weight,
        stock: productData.stock ? productData.stock : 1,
        description: productData.description,
        isVarian:
          productData.variants === undefined || productData.variants === null
            ? false
            : true,
        source_url: productData.source_url,
      });
      if (productData.variants) {
        if (productData.variants !== null) {
          let modelVariants = [];
          modelVariants = productData.variants.models.flatMap((v) => {
            return {
              name: v.name,
              options: v.options.filter(
                (v, i, a) => a.findIndex((t) => t.name === v.name) === i,
              ),
            };
          });
          let details = [];
          productData.variants.details.flatMap((v) => {
            details.push({
              code: v.code ? v.code : '',
              stock: v.stock ? v.stock.toString() : '1',
              status: v.status ? v.status : true,
              price: v.price ? v.price.toString() : '0',
              capital_price: v.capital_price
                ? v.capital_price
                : v.price.toString(),
            });
          });
          setdataVariasi(details);
          setdataVarian(modelVariants);
        }
      }
      setTabIndex(productData.variants !== undefined ? 1 : 0);
      setImagesTemp(productData.images);
    }
  }, [productData]);
  useEffect(() => {
    console.log('dataVariasi', dataVariasi);
  }, [dataVariasi]);
  useEffect(() => {
    console.log('datavariasiawal', dataVariasiAwal);
  }, [dataVariasiAwal]);
  useEffect(() => {
    if (checkSuccessAddNewCategory) {
      console.log('kepanggil successadnewcategory');
      if (checkSuccessScrap === false) {
        let urlImages = [];
        let urlVarian = [];

        imagesTemp.forEach((img, i) => {
          const form = new FormData();
          form.append('image', {
            uri: img ? img.uri : Images.noImg,
            name: img.name,
            type: img.type,
          });
          console.log('form', form);
          const formVarian = new FormData();
          let uploadImageVarian = {};
          if (imagesVarian.length > 0) {
            imagesVarian.forEach((img, i) => {
              formVarian.append('image', {
                uri: img ? img.uri : '',
                name: img.name,
                type: img.type,
              });
              uploadImageVarian = {
                tokoId: tokoId,
                item: formVarian,
                func: {
                  reloadData: (url) => {
                    console.log('url', url);
                    urlVarian[i] = url;
                  },
                },
              };
            });
          }
          let dataMap = [];
          dataMap = varian.map((o) => {
            return {
              name: o.name,
              options: o.options.map((v) => {
                return {
                  name: v.name,
                  image: urlVarian,
                };
              }),
            };
          });
          const uploadData = {
            tokoId: tokoId,
            item: form,
            func: {
              reloadData: (url) => {
                console.log(url);
                urlImages[i] = url;
                const dataWithoutVarian = {
                  id: tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    price:
                      typeof productForm.price === 'string'
                        ? Number(productForm.price.replace(/[Rp.]/g, ''))
                        : productForm.price,
                    capital_price:
                      typeof productForm.capitalPrice === 'string'
                        ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                        : productForm.capitalPrice,
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                    supplier_id: getParams.id,
                    source_url: productForm.source_url,
                    weight:
                      productForm.weight === null ? 100 : productForm.weight,
                    stock: productForm.stock === null ? 1 : productForm.stock,
                  },
                  isVarian: productForm.isVarian,
                  func: {
                    reloadData: handleRefresh,
                    reloadFailed: setloadingVarian,
                  },
                };
                const dataWithVarian = {
                  id: tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    supplier_id: getParams.id,
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                    weight: parseInt(productForm.weight),
                    source_url: productForm.source_url,
                    variants: {
                      models: dataMap,
                      details: dataVariasi,
                    },
                  },
                  isVarian: productForm.isVarian,
                  func: {
                    reloadData: handleRefresh,
                    reloadFailed: setloadingVarian,
                  },
                };
                console.log('urlVarian', urlVarian);
                if (i >= imagesTemp.length - 1 && i >= urlImages.length - 1) {
                  console.log('variants', dataWithVarian);
                  dispatch(
                    TokoActions.postProductTokoRequest(
                      tabIndex === 1 ? dataWithVarian : dataWithoutVarian,
                    ),
                  );
                }
              },
            },
          };
          if (imagesVarian.length > 0) {
            dispatch(TokoActions.uploadProductImageRequest(uploadImageVarian));
          }
          dispatch(TokoActions.uploadProductImageRequest(uploadData));
        });
      } else {
        setloadingVarian(true);
        let dataMap = [];
        let urlVarian = [];
        let urlImages = [];
        productData.images.forEach((v, i) => {
          urlImages[i] = v.url;
        });
        // dataMap = varian.map((o) => {
        //   return {
        //     name: o.name,
        //     options: o.options.map((v) => {
        //       return {
        //         name: v.name,
        //         image: urlVarian,
        //       };
        //     }),
        //   };
        // });
        const dataWithoutVarian = {
          id: tokoId,
          item: {
            name: productForm.name,
            description: productForm.description,
            price:
              typeof productForm.price === 'string'
                ? Number(productForm.price.replace(/[Rp.]/g, ''))
                : productForm.price,
            capital_price:
              typeof productForm.capitalPrice === 'string'
                ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                : productForm.capitalPrice,
            category_id:
              AddNewCategoryId != null
                ? AddNewCategoryId.productCategory != undefined
                  ? AddNewCategoryId.productCategory.id
                  : productForm.categorySelected
                : productForm.categorySelected,
            images: urlImages
              ? urlImages.length > 5
                ? urlImages.slice(0, 5)
                : urlImages
              : null,
            supplier_id: getParams.id,
            source_url: productForm.source_url,
            weight: productForm.weight === null ? 100 : productForm.weight,
            stock: productForm.stock === null ? 1 : productForm.stock,
          },
          isVarian: productForm.isVarian,
          func: {reloadData: handleRefresh, reloadFailed: setloadingVarian},
        };
        const dataWithVarian = {
          id: tokoId,
          item: {
            name: productForm.name,
            description: productForm.description,

            category_id:
              AddNewCategoryId != null
                ? AddNewCategoryId.productCategory != undefined
                  ? AddNewCategoryId.productCategory.id
                  : productForm.categorySelected
                : productForm.categorySelected,
            images: urlImages
              ? urlImages.length > 5
                ? urlImages.slice(0, 5)
                : urlImages
              : null,
            weight: parseInt(productForm.weight),
            supplier_id: getParams.id,
            source_url: productForm.source_url,
            variants: {
              models: dataVarian,
              details: dataVariasi,
            },
          },
          isVarian: productForm.isVarian,
          func: {reloadData: handleRefresh, reloadFailed: setloadingVarian},
        };
        dispatch(
          TokoActions.postProductTokoRequest(
            tabIndex === 1 ? dataWithVarian : dataWithoutVarian,
          ),
        );
      }
    }
  }, [checkSuccessAddNewCategory]);
  useEffect(() => {
    let dataMap = [];
    let dataImagesVarian = [];
    let dataOptions = [];

    // let variasiData = [];

    if (dataVarian) {
      dataOptions = dataVarian.map((o) => {
        return o.options;
      });
      dataImagesVarian = dataVarian.flatMap((o) => {
        return o.options;
      });
      dataImagesVarian = dataImagesVarian.flatMap((o) => {
        if (o.img !== null) {
          return o.img;
        } else {
          return [];
        }
      });
      dataMap = dataVarian.map((o) => {
        return {
          name: o.name,
          options: o.options.map((v) => {
            return {
              name: v.name,
              image: v.img ? v.img.uri : '',
            };
          }),
        };
      });
      // console.log(dataMap);
    }
    const fn = _.spread(_.union);
    const details = [];
    if (dataVarian) {
      let allVarianName = [];
      let allVarianDetails = [];
      dataVarian.map((dv, i) => {
        let tempVarian = [];
        dv.options.map((o) => tempVarian.push(o.name));
        allVarianName.push(tempVarian);
        allVarianDetails.push(dv.options);
      });
      let f = (a, b) =>
        [].concat(...a.map((a) => b.map((b) => [].concat(a, b))));
      let cartesian = (a, b, ...c) => (b ? cartesian(f(a, b), ...c) : a);
      const mappingVarian =
        allVarianName.length > 1
          ? cartesian(...allVarianName)
          : cartesian([...allVarianName]);
      if (mappingVarian.length > 1 && allVarianName.length > 1) {
        mappingVarian.map((mv) => {
          const getData = _.find(fn(allVarianDetails), {name: mv[0]});
          const code = allVarianName.length > 1 ? mv.join('-') : mv;
          details.push({
            code,
            stock: parseInt(getData.stock),
            status: true,
            price: parseInt(getData.price),
            capital_price: parseInt(getData.capitalPrice),
          });
        });
      } else {
        dataOptions.flatMap((v) => {
          v.map((o) => {
            details.push({
              code: o.name,
              stock: parseInt(o.stock),
              status: true,
              price: parseInt(o.price),
              capital_price: parseInt(o.capitalPrice),
            });
          });
        });
      }
    }
    let dataLast = [...dataVariasi, ...details];
    let counts = _.countBy(dataLast, 'code');
    (counts = _.filter(dataLast, (x) => counts[x.code] > 1)),
      (counts = counts.filter(
        (v, i, a) => a.findIndex((t) => t.code === v.code) === i,
      ));
    if (dataVariasi.length > 0) {
      setdataVariasi(counts);
    }
    console.log('counts', counts);

    setVarian(dataMap);
    setimagesVarian(dataImagesVarian);
  }, [dataVarian]);
  useEffect(() => {
    console.log('imagesTemp', imagesTemp);
  }, [imagesTemp]);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  return (
    <Container>
      <Header
        androidStatusBarColor={c.Colors.mainBlue}
        style={{backgroundColor: c.Colors.mainBlue}}>
        <Left>
          <TouchableOpacity onPress={handleGoBack}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <Text style={styles.txtTitle}>Tambah Produk</Text>
        </Body>
        <Right />
      </Header>
      {productForm.name === null ? (
        <View
          style={{
            justifyContent: 'center',
            alignContent: 'center',
            height: Dimensions.get('screen').height * 0.8,
          }}>
          <ActivityIndicator color="#000" size="large" />
        </View>
      ) : (
        <ScrollView ref={scrollParentRef} keyboardShouldPersistTaps={'always'}>
          <Modal
            onDismiss={() => setKey(null)}
            onModalHide={() => setKey(null)}
            onBackdropPress={() => setShowUploadModal(false)}
            isVisible={showUploadModal}
            style={{margin: 20}}>
            <View
              style={[
                c.Styles.paddingTwenty,
                {
                  borderRadius: 5,
                  backgroundColor: 'white',
                },
              ]}>
              <View style={[c.Styles.paddingTwenty]}>
                <Text style={[c.Styles.txtBold]}>Pilih Gambar Dari</Text>
              </View>
              <TouchableOpacity
                delayPressIn={0}
                onPress={() => {
                  handlePickImage('gallery');
                }}
                style={[c.Styles.btnImagePicker, c.Styles.flexRow, ,]}>
                <View style={[c.Styles.flexRow, {alignItems: 'center'}]}>
                  <MaterialCommunityIcons
                    name="image-outline"
                    size={25}
                    color={c.Colors.mainBlue}
                  />
                  <c.Text
                    style={[
                      c.Styles.paddingTen,
                      c.Styles.txtTncDefault,
                      c.Styles.txtmainBlue,
                    ]}
                    txt="Gallery"
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                delayPressIn={0}
                onPress={() => {
                  handlePickImage('camera');
                }}
                style={[c.Styles.btnImagePicker, c.Styles.flexRow]}>
                <View style={[c.Styles.flexRow, {alignItems: 'center'}]}>
                  <MaterialCommunityIcons
                    name="camera-outline"
                    size={25}
                    color={c.Colors.mainBlue}
                  />
                  <c.Text
                    style={[
                      c.Styles.paddingTen,
                      c.Styles.txtTncDefault,
                      c.Styles.txtmainBlue,
                    ]}
                    txt="Camera"
                  />
                </View>
              </TouchableOpacity>
            </View>
          </Modal>
          <ScrollView
            //contentContainerStyle={styles.contentView}
            keyboardShouldPersistTaps={'always'}>
            <View style={{paddingLeft: 20}}>
              <FlatList
                renderItem={productImageList}
                data={imagesTemp}
                showsHorizontalScrollIndicator={false}
                horizontal
                ListFooterComponent={
                  imagesTemp.length < 1 && productImageListFooter
                }
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

            <View style={styles.itemPadding}>
              {productValidation.isImageEmpty && (
                <Text style={[styles.txtketaddPhoto, {color: 'red'}]}>
                  Wajib masukkan 1 foto produk (sisanya optional)
                </Text>
              )}
            </View>
            <View style={[styles.viewForm, styles.itemPadding]}>
              <Text style={styles.txttitleForm}>Nama Produk *</Text>
              <TextInput
                returnKeyType="next"
                //onSubmitEditing={() => this.description.focus()}
                style={
                  productForm.name ? styles.txtinputExist : styles.txtinputNull
                }
                onChangeText={(text) => {
                  setProductValidation({
                    ...productValidation,
                    isNameEmpty: text ? false : true,
                  });
                  setProductForm({...productForm, name: text});
                }}
                value={productForm.name}
                placeholder="Masukkan Nama Produk"
                placeholderTextColor={'#bdbdbd'}
              />
              {productValidation.isNameEmpty && (
                <Text style={[styles.txtketaddPhoto, {color: 'red'}]}>
                  Wajib diisi
                </Text>
              )}
            </View>
            <View style={[styles.viewForm, styles.itemPadding]}>
              <Text style={styles.txttitleForm}>Pilih Kategori</Text>
              <TextInput
                //ref={refAddNewCategory}
                // editable={productForm.addCategory}
                placeholder={
                  categoryData.length != 0
                    ? 'Buat Kategori Baru Atau Pilih Dibawah'
                    : 'Buat Kategori Baru'
                }
                // blurOnSubmit={true}
                value={
                  categoryName != null ? categoryName : productForm.newCategory
                }
                placeholderTextColor={c.Colors.grayWhite}
                onChangeText={(text) => {
                  setCategoryName(text);
                  setProductForm({
                    ...productForm,
                    newCategory: text,
                    addCategory: true,
                    categorySelected: true,
                  });
                }}
                style={
                  categoryName != null
                    ? styles.txtinputExist
                    : styles.txtinputNull
                }></TextInput>
              <FlatList
                renderItem={categoryButton}
                data={categoryData ? categoryData : null}
                showsHorizontalScrollIndicator={false}
                horizontal
                contentContainerStyle={[styles.viewForm]}
                keyExtractor={(item, index) => index.toString()}
              />
              {productValidation.isCategoryEmpty && (
                <Text style={[styles.txtketaddPhoto, {color: 'red'}]}>
                  Wajib memilih kategori
                </Text>
              )}
            </View>
            <View style={[styles.viewForm, styles.itemPadding]}>
              <Text style={styles.txttitleForm}>Deskripsi Produk</Text>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={[
                    c.Styles.txtInputBordered,
                    c.Styles.borderGrey,
                    {paddingVertical: 10},
                  ]}
                  placeholder="Masukkan Deskripsi Produk"
                  multiline={true}
                  numberOfLines={4}
                  textAlignVertical="top"
                  value={productForm.description}
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    setProductForm({
                      ...productForm,
                      description: text,
                    });
                  }}
                />
              </View>
            </View>
            <View style={[styles.viewForm, styles.itemPadding]}>
              <Text style={styles.txttitleForm}>Berat (gram)</Text>
              <TextInput
                keyboardType="number-pad"
                returnKeyType="next"
                //onSubmitEditing={() => this.description.focus()}
                style={
                  productForm.weight
                    ? styles.txtinputExist
                    : styles.txtinputNull
                }
                onChangeText={(text) => {
                  setProductForm({...productForm, weight: text});
                }}
                value={
                  productForm.weight
                    ? productForm.weight.toString()
                    : productForm.weight
                }
                placeholder="Berat produk (gram)"
                placeholderTextColor={'#bdbdbd'}
              />
            </View>
            <View style={[styles.viewForm]}>
              <Tabs
                initialPage={tabIndex}
                page={tabIndex}
                onChangeTab={(c) => {
                  console.log(c.i);
                  setProductForm({
                    ...productForm,
                    isVarian: c.i == 0 ? false : true,
                  });
                  setTabIndex(c.i);
                }}
                tabBarUnderlineStyle={{backgroundColor: '#1a69d5', height: 3}}>
                <Tab
                  heading="Tanpa Varian"
                  activeTextStyle={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 0.97,
                    color: '#4a4a4a',
                  }}
                  textStyle={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 0.97,
                    color: '#4a4a4a',
                  }}
                  tabStyle={{backgroundColor: '#fff'}}
                  activeTabStyle={{backgroundColor: '#fff'}}>
                  <View style={[styles.itemPadding, {paddingBottom: 20}]}>
                    <View style={styles.viewForm}>
                      <Text style={styles.txttitleForm}>Harga Modal</Text>
                      <View
                        style={{
                          justifyContent: 'center',
                        }}>
                        <TextInput
                          //onSubmitEditing={() => this.price.focus()}
                          editable={false}
                          returnKeyType="next"
                          keyboardType="number-pad"
                          style={
                            productForm.capitalPrice
                              ? [styles.txtinputExist, {color: '#bdbdbd'}]
                              : [
                                  styles.txtinputNull,
                                  {paddingHorizontal: 30, color: '#bdbdbd'},
                                ]
                          }
                          onChangeText={(text) => {
                            let total = text.replace(/\D/g, '');
                            setProductForm({
                              ...productForm,
                              capitalPrice: total,
                            });
                          }}
                          value={
                            productForm.capitalPrice
                              ? func.rupiah(productForm.capitalPrice)
                              : productForm.capitalPrice
                          }
                          placeholderTextColor={'#bdbdbd'}
                          placeholder={'0'}
                        />
                        {productForm.capitalPrice ? null : (
                          <Text
                            style={[
                              styles.txtinputExist,
                              {
                                position: 'absolute',
                                borderWidth: 0,
                              },
                            ]}>
                            Rp
                          </Text>
                        )}
                      </View>
                    </View>
                    <View style={styles.viewForm}>
                      <Text style={styles.txttitleForm}>Harga Jual *</Text>
                      <View
                        style={{
                          justifyContent: 'center',
                        }}>
                        <TextInput
                          //ref={(input) => (this.price = input)}
                          //onSubmitEditing={() => this.discount.focus()}
                          keyboardType="number-pad"
                          style={
                            productForm.price
                              ? [styles.txtinputExist]
                              : [styles.txtinputNull, {paddingHorizontal: 30}]
                          }
                          onChangeText={(text) => {
                            let total = text.replace(/\D/g, '');
                            setProductValidation({
                              ...productValidation,
                              isPriceEmpty: total ? false : true,
                            });
                            setProductForm({...productForm, price: total});
                          }}
                          value={
                            productForm.price
                              ? func.rupiah(productForm.price)
                              : productForm.price
                          }
                          placeholderTextColor={'#bdbdbd'}
                          placeholder={'0'}
                        />
                        {productForm.price ? null : (
                          <Text
                            style={[
                              styles.txtinputExist,
                              {
                                position: 'absolute',
                                borderWidth: 0,
                              },
                            ]}>
                            Rp
                          </Text>
                        )}
                      </View>
                      {productValidation.isPriceEmpty && (
                        <Text style={[styles.txtketaddPhoto, {color: 'red'}]}>
                          Wajib diisi
                        </Text>
                      )}
                    </View>
                    {/* <View style={styles.viewForm}>
                      <Text style={styles.txttitleForm}>Diskon</Text>
                      <View
                        style={{
                          justifyContent: 'center',
                        }}>
                        <TextInput
                          // ref={(input) => (this.discount = input)}
                          returnKeyType="next"
                          style={
                            productForm.discount || percentChoosed
                              ? [styles.txtinputExist]
                              : [styles.txtinputNull, {paddingHorizontal: 30}]
                          }
                          onChangeText={(text) => {
                            let total = text.replace(/\D/g, '');
                            setProductForm({
                              ...productForm,
                              discount: total,
                            });
                          }}
                          value={
                            productForm.discount && !percentChoosed
                              ? func.rupiah(productForm.discount)
                              : productForm.discount
                          }
                          placeholder={percentChoosed ? '10%' : '0'}
                          placeholderTextColor={'#bdbdbd'}
                        />
                        {productForm.discount || percentChoosed ? null : (
                          <Text
                            style={[
                              styles.txtinputExist,
                              {
                                position: 'absolute',
                                borderWidth: 0,
                              },
                            ]}>
                            Rp
                          </Text>
                        )}
                        <View
                          style={{
                            position: 'absolute',
                            right: 0,
                            flexDirection: 'row',
                          }}>
                          <TouchableOpacity
                            onPress={() => {
                              setpercentChoosed(true);
                              setrupiahChoosed(false);
                              setProductForm({
                                ...productForm,
                                discount: null,
                              });
                            }}
                            style={
                              percentChoosed
                                ? styles.discountChoosed
                                : styles.discountnotChoosed
                            }>
                            <Text
                              style={
                                percentChoosed
                                  ? styles.txtdiscountChoosed
                                  : styles.txtdiscountnotChoosed
                              }>
                              %
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => {
                              setrupiahChoosed(true);
                              setpercentChoosed(false);
                              setProductForm({
                                ...productForm,
                                discount: null,
                              });
                            }}
                            style={
                              rupiahChoosed
                                ? styles.discountChoosed
                                : styles.discountnotChoosed
                            }>
                            <Text
                              style={
                                rupiahChoosed
                                  ? styles.txtdiscountChoosed
                                  : styles.txtdiscountnotChoosed
                              }>
                              Rp
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View> */}
                    <View style={styles.viewForm}>
                      <Text style={styles.txttitleForm}>Stok</Text>
                      <TextInput
                        keyboardType="number-pad"
                        returnKeyType="next"
                        //onSubmitEditing={() => this.description.focus()}
                        style={
                          productForm.stock
                            ? styles.txtinputExist
                            : styles.txtinputNull
                        }
                        onChangeText={(text) => {
                          setProductForm({...productForm, stock: text});
                        }}
                        value={
                          productForm.stock
                            ? productForm.stock.toString()
                            : productForm.stock
                        }
                        placeholder="Stok"
                        placeholderTextColor={'#bdbdbd'}
                      />
                    </View>

                    <View style={[styles.viewForm, {marginBottom: 80}]}>
                      <Text style={styles.txttitleForm}>
                        Video Produk (Link Youtube)
                      </Text>
                      <TextInput
                        returnKeyType="next"
                        //onSubmitEditing={() => this.description.focus()}
                        style={
                          productForm.source_url
                            ? styles.txtinputExist
                            : styles.txtinputNull
                        }
                        onChangeText={(text) => {
                          setProductForm({...productForm, productLink: text});
                        }}
                        value={productForm.video_url}
                        placeholder="https://youtube.com/"
                        placeholderTextColor={'#bdbdbd'}
                      />
                    </View>
                  </View>
                </Tab>
                <Tab
                  heading="Varian"
                  activeTextStyle={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 0.97,
                    color: '#4a4a4a',
                  }}
                  textStyle={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 0.97,
                    color: '#4a4a4a',
                  }}
                  tabStyle={{backgroundColor: '#fff'}}
                  activeTabStyle={{backgroundColor: '#fff'}}>
                  <View style={{padding: 25}}>
                    {dataVarian.length > 0 ? (
                      <View
                        style={{
                          marginBottom: 5,
                        }}>
                        <Text style={styles.txttitleForm}>
                          Tambah harga jual untuk semua barang
                        </Text>
                        <View
                          style={{
                            justifyContent: 'center',
                          }}>
                          <TextInput
                            // ref={(input) => (this.discount = input)}
                            keyboardType="number-pad"
                            returnKeyType="next"
                            style={
                              valuetambahHarga || percentsChoosed
                                ? [styles.txtinputExist]
                                : [styles.txtinputNull, {paddingHorizontal: 30}]
                            }
                            onChangeText={(text) => {
                              let total = text.replace(/\D/g, '');
                              console.log('total', total);
                              setvaluetambahHarga(total);
                              let dataLast = [...dataVariasi];
                              const _dataVariasi = [...dataVariasi];
                              _.map(_dataVariasi, function (x) {
                                var result =
                                  (parseInt(total) * x.capital_price) / 100;
                                let totalhargaRupiah =
                                  Number(x.capital_price) + Number(total);
                                let totalhargaPercent = isNaN(result)
                                  ? Number(x.capital_price)
                                  : Number(x.capital_price) + result;
                                x.price =
                                  valuetambahHarga === null
                                    ? Number(x.capital_price)
                                    : rupiahsChoosed
                                    ? totalhargaRupiah
                                    : totalhargaPercent;
                                return x;
                              });
                              console.log(_dataVariasi, dataLast);
                              setdataVariasi(_dataVariasi);
                            }}
                            value={
                              valuetambahHarga && !percentsChoosed
                                ? func.rupiah(valuetambahHarga)
                                : valuetambahHarga
                            }
                            placeholder={percentsChoosed ? '10%' : '0'}
                            placeholderTextColor={'#bdbdbd'}
                          />
                          {valuetambahHarga || percentsChoosed ? null : (
                            <Text
                              style={[
                                styles.txtinputExist,
                                {
                                  position: 'absolute',
                                  borderWidth: 0,
                                },
                              ]}>
                              Rp
                            </Text>
                          )}
                          <View
                            style={{
                              position: 'absolute',
                              right: 0,
                              flexDirection: 'row',
                            }}>
                            <TouchableOpacity
                              onPress={() => {
                                console.log('dataVariasiAwal', dataVariasiAwal);
                                setpercentsChoosed(true);
                                setrupiahsChoosed(false);
                                setvaluetambahHarga(null);
                                // setdataVariasi(dataVariasiAwal);
                              }}
                              style={
                                percentsChoosed
                                  ? styles.discountChoosed
                                  : styles.discountnotChoosed
                              }>
                              <Text
                                style={
                                  percentsChoosed
                                    ? styles.txtdiscountChoosed
                                    : styles.txtdiscountnotChoosed
                                }>
                                %
                              </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() => {
                                console.log('dataVariasiAwal', dataVariasiAwal);
                                setrupiahsChoosed(true);
                                setpercentsChoosed(false);
                                setvaluetambahHarga(null);
                                //setdataVariasi(dataVariasiAwal);
                              }}
                              style={
                                rupiahsChoosed
                                  ? styles.discountChoosed
                                  : styles.discountnotChoosed
                              }>
                              <Text
                                style={
                                  rupiahsChoosed
                                    ? styles.txtdiscountChoosed
                                    : styles.txtdiscountnotChoosed
                                }>
                                Rp
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ) : null}
                    <View
                      style={[
                        styles.viewForm,
                        {
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          marginBottom: 5,
                        },
                      ]}>
                      <Text
                        style={{
                          fontFamily: 'NotoSans-Regular',
                          fontSize: 10,
                          letterSpacing: 0.76,
                          color: c.Colors.mainBlue,
                        }}>
                        Variasi
                      </Text>
                      <TouchableOpacity onPress={() => setaddVarian(true)}>
                        <Text
                          style={{
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 12,
                            letterSpacing: 0.93,
                            color: c.Colors.mainBlue,
                          }}>
                          {' '}
                          + Tambah Varian
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
                        borderWidth: productValidation.isVarianEmpty
                          ? 0.8
                          : 0.5,
                        borderColor: productValidation.isVarianEmpty
                          ? 'red'
                          : '#979797',
                      }}>
                      {addVarian ? (
                        <View
                          style={{
                            borderWidth: productValidation.isVarianEmpty
                              ? 0.8
                              : 0.5,
                            borderColor: productValidation.isVarianEmpty
                              ? 'red'
                              : '#979797',
                            margin: 20,
                          }}>
                          <View style={{padding: 10}}>
                            <View style={{marginTop: 10}}>
                              <Text
                                style={{
                                  fontFamily: 'NotoSans-Regular',
                                  fontSize: 10,
                                  letterSpacing: 0.75,
                                  color: c.Colors.mainBlue,
                                  margin: 5,
                                }}>
                                Nama Variasi
                              </Text>
                              <TextInput
                                returnKeyType="next"
                                //onSubmitEditing={() => this.description.focus()}
                                style={
                                  productForm.varianName === null
                                    ? [styles.txtinputExist, {color: 'red'}]
                                    : productForm.varianName
                                    ? styles.txtinputExist
                                    : styles.txtinputNull
                                }
                                onChangeText={(text) => {
                                  setProductValidation({
                                    ...productValidation,
                                    isVarianEmpty: text ? false : true,
                                  });
                                  setProductForm({
                                    ...productForm,
                                    varianName: text,
                                  });
                                }}
                                value={productForm.varianName}
                                placeholder="cth: Warna"
                                placeholderTextColor={'#bdbdbd'}
                              />
                            </View>
                            <View style={{marginTop: 10}}>
                              <Text
                                style={{
                                  fontFamily: 'NotoSans-Regular',
                                  fontSize: 10,
                                  letterSpacing: 0.75,
                                  color: c.Colors.mainBlue,
                                  margin: 5,
                                }}>
                                Keterangan Variasi
                              </Text>
                              <TouchableOpacity
                                onPress={addHandler}
                                style={{
                                  borderWidth: 0.5,
                                  borderColor: c.Colors.mainBlue,
                                }}>
                                <Text
                                  style={{
                                    fontFamily: 'NotoSans-Regular',
                                    fontSize: 12,
                                    letterSpacing: 0.95,
                                    color: c.Colors.mainBlue,
                                    textAlign: 'center',
                                    margin: 10,
                                  }}>
                                  {' '}
                                  + Tambahkan
                                </Text>
                              </TouchableOpacity>
                              <View style={{marginTop: 5, marginBottom: 10}}>
                                {ketVariasi.map((data, i) => (
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      marginTop: 5,
                                    }}>
                                    {dataVarian.length > 0 ? null : (
                                      <TouchableOpacity
                                        onPress={() => {
                                          setKey(i);
                                          setShowUploadModal(true);
                                        }}
                                        style={{
                                          backgroundColor: '#d8d8d8',
                                          padding: data.img != null ? 0 : 12,
                                        }}>
                                        <Image
                                          source={
                                            data.img != null
                                              ? {uri: data.img.uri}
                                              : Images.noImg
                                          }
                                          style={{
                                            width: data.img != null ? 42 : 18,
                                            height: data.img != null ? 42 : 18,
                                            resizeMode: 'contain',
                                          }}
                                        />
                                      </TouchableOpacity>
                                    )}

                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent:
                                          dataVarian.length > 0
                                            ? 'space-between'
                                            : 'space-around',
                                        width:
                                          dataVarian.length > 0
                                            ? '100%'
                                            : '85%',
                                      }}>
                                      <View
                                        style={{
                                          justifyContent: 'center',
                                          width: '85%',
                                        }}>
                                        <TextInput
                                          placeholder={'cth: Merah'}
                                          value={data.name}
                                          style={
                                            ketVariasi[i].name
                                              ? [
                                                  styles.txtinputExist,
                                                  {paddingVertical: 3},
                                                ]
                                              : [
                                                  styles.txtinputNull,
                                                  {paddingVertical: 3},
                                                ]
                                          }
                                          onChangeText={(text) => {
                                            console.log(ketVariasi, text);
                                            inputHandler(text, i);
                                          }}
                                        />
                                      </View>
                                      <TouchableOpacity
                                        disabled={
                                          ketVariasi.length < 2 ? true : false
                                        }
                                        onPress={() => deleteHandler(i)}>
                                        <FontAwesome
                                          size={20}
                                          color="#979797"
                                          name={'trash-o'}
                                        />
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                ))}
                                <View style={{marginTop: 20}}>
                                  <TouchableOpacity
                                    onPress={
                                      productForm.varianName === null
                                        ? Toast.show('Mohon lengkapi data')
                                        : saveVariant
                                    }
                                    style={styles.saveButton}>
                                    {loadingVarian ? (
                                      <ActivityIndicator />
                                    ) : (
                                      <Text
                                        style={styles.txtsaveketvariasiButton}>
                                        Simpan
                                      </Text>
                                    )}
                                  </TouchableOpacity>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      ) : null}
                      {dataVarian.length > 0 ? (
                        <View>
                          <View
                            style={{
                              width: '90%',
                              alignSelf: 'center',
                              borderBottomWidth: 0.5,
                              borderBottomColor: '#979797',
                            }}>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                width: '70%',
                                marginTop: 10,
                                marginBottom: 10,
                              }}>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: 'NotoSans-Regular',
                                    fontSize: 10,
                                    letterSpacing: 0.75,
                                    color: c.Colors.mainBlue,
                                    margin: 5,
                                  }}>
                                  Nama Variasi
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontFamily: 'NotoSans-Regular',
                                    fontSize: 10,
                                    letterSpacing: 0.75,
                                    color: c.Colors.mainBlue,
                                    margin: 5,
                                  }}>
                                  Keterangan Variasi
                                </Text>
                              </View>
                            </View>
                          </View>
                          {dataVarian.map((v, i) => {
                            return (
                              <View
                                style={{
                                  width: '90%',
                                  alignSelf: 'center',
                                  flexDirection: 'row',
                                }}>
                                <View style={{width: '35%'}}>
                                  <Text
                                    style={{
                                      fontFamily: 'NotoSans-Regular',
                                      fontSize: 10,
                                      letterSpacing: 0.73,
                                      color: '#4a4a4a',
                                      margin: 5,
                                      marginTop: 10,
                                      marginBottom: 10,
                                    }}>
                                    {v.name}
                                  </Text>
                                </View>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    width: '65%',
                                  }}>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                    }}>
                                    <Text
                                      numberOfLines={1}
                                      style={{width: '90%'}}>
                                      {v.options.map((data, i) => {
                                        return (
                                          <Text
                                            style={{
                                              fontFamily: 'NotoSans-Regular',
                                              fontSize: 10,
                                              letterSpacing: 0.73,
                                              color: '#4a4a4a',
                                              marginTop: 10,
                                              marginBottom: 10,
                                            }}>
                                            {data.name + ', '}
                                          </Text>
                                        );
                                      })}
                                    </Text>
                                  </View>
                                  <TouchableOpacity
                                    onPress={() => {
                                      let ketVariasi = v.options.map(
                                        (data) => data,
                                      );
                                      console.log('ketvariasi', ketVariasi);
                                      setaddVarian(true);
                                      setisEdit(true);
                                      setProductForm({
                                        ...productForm,
                                        varianName: v.name,
                                      });
                                      setketVariasi(ketVariasi);
                                    }}>
                                    <MaterialCommunityIcons
                                      name="pencil"
                                      size={18}
                                      color={'#9b9b9b'}
                                    />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            );
                          })}
                        </View>
                      ) : null}
                    </View>

                    {dataVarian.length > 0 ? (
                      <ScrollView
                        horizontal
                        pointerEvents="none"
                        showsHorizontalScrollIndicator={false}
                        ref={scrollviewRef}
                        style={{
                          borderWidth: 0.5,
                          borderColor: '#979797',
                          marginTop: 20,
                          paddingBottom: 20,
                        }}>
                        <FlatList
                          activationDistance={5}
                          ListHeaderComponent={_renderHeader}
                          keyExtractor={(item, index) => index.toString()}
                          showsHorizontalScrollIndicator={false}
                          renderItem={_renderItem}
                          data={dataVariasi}
                          keyExtractor={(item, index) => index.toString()}
                        />
                      </ScrollView>
                    ) : null}

                    {/* <View style={styles.viewForm}>
                      <Text style={styles.txttitleForm}>Diskon</Text>
                      <View
                        style={{
                          justifyContent: 'center',
                        }}>
                        <TextInput
                          // ref={(input) => (this.discount = input)}
                          returnKeyType="next"
                          style={
                            productForm.discount || percentChoosed
                              ? [styles.txtinputExist]
                              : [styles.txtinputNull, {paddingHorizontal: 30}]
                          }
                          onChangeText={(text) => {
                            let total = text.replace(/\D/g, '');
                            setProductForm({
                              ...productForm,
                              discount: total,
                            });
                          }}
                          value={
                            productForm.discount && !percentChoosed
                              ? func.rupiah(productForm.discount)
                              : productForm.discount
                          }
                          placeholder={percentChoosed ? '10%' : '0'}
                          placeholderTextColor={'#bdbdbd'}
                        />
                        {productForm.discount || percentChoosed ? null : (
                          <Text
                            style={[
                              styles.txtinputExist,
                              {
                                position: 'absolute',
                                borderWidth: 0,
                              },
                            ]}>
                            Rp
                          </Text>
                        )}
                        <View
                          style={{
                            position: 'absolute',
                            right: 0,
                            flexDirection: 'row',
                          }}>
                          <TouchableOpacity
                            onPress={() => {
                              setpercentChoosed(true);
                              setrupiahChoosed(false);
                              setProductForm({
                                ...productForm,
                                discount: null,
                              });
                            }}
                            style={
                              percentChoosed
                                ? styles.discountChoosed
                                : styles.discountnotChoosed
                            }>
                            <Text
                              style={
                                percentChoosed
                                  ? styles.txtdiscountChoosed
                                  : styles.txtdiscountnotChoosed
                              }>
                              %
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => {
                              setrupiahChoosed(true);
                              setpercentChoosed(false);
                              setProductForm({
                                ...productForm,
                                discount: null,
                              });
                            }}
                            style={
                              rupiahChoosed
                                ? styles.discountChoosed
                                : styles.discountnotChoosed
                            }>
                            <Text
                              style={
                                rupiahChoosed
                                  ? styles.txtdiscountChoosed
                                  : styles.txtdiscountnotChoosed
                              }>
                              Rp
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View> */}
                    <View style={[styles.viewForm, {marginBottom: 80}]}>
                      <Text style={styles.txttitleForm}>
                        Video Produk (Link Youtube)
                      </Text>
                      <TextInput
                        returnKeyType="next"
                        //onSubmitEditing={() => this.description.focus()}
                        style={
                          productForm.source_url
                            ? styles.txtinputExist
                            : styles.txtinputNull
                        }
                        onChangeText={(text) => {
                          setProductForm({...productForm, productLink: text});
                        }}
                        value={productForm.video_url}
                        placeholder="https://youtube.com/"
                        placeholderTextColor={'#bdbdbd'}
                      />
                    </View>
                  </View>
                </Tab>
              </Tabs>
            </View>
          </ScrollView>
        </ScrollView>
      )}
      {productForm.name === null ? null : (
        <View
          style={{
            position: 'absolute',
            flex: 1,
            bottom: 10,
            width: '100%',
            paddingHorizontal: 20,
          }}>
          <Button
            onPress={
              checkSuccessScrap === false
                ? handleAddNewProduct
                : handleAddProduct
            }
            // onPress={handleNewProduct}
            style={[c.Styles.btnPrimary]}>
            {loadingVarian ? (
              <ActivityIndicator color="#fff" />
            ) : (
              <Text style={styles.txtsaveButton}>Simpan</Text>
            )}
          </Button>
        </View>
      )}
    </Container>
  );
}
const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    AddNewCategoryId: state.toko.postCategoryTokoData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    checkSuccessAddNewCategory: state.toko.postCategoryTokoData.success,
    categoryData: state.toko.categoryTokoData.data
      ? state.toko.categoryTokoData.data.listCategory
        ? state.toko.categoryTokoData.data.listCategory
        : []
      : [],
    productData: state.marketplace.productByUrlData.data,
    checkSuccessScrap: state.marketplace.productByUrlData.success,
    getToko: state.toko.tokoData.data,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddProductDropship);
