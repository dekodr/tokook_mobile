import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {withNavigationFocus} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {Button, Spinner, Card} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from './../../../../Redux/TokoRedux';
import AuthActions from './../../../../Redux/AuthenticationRedux';
import GlobalActions from './../../../../Redux/GlobalRedux';
import FirebaseTypes from '../../../../Redux/FirebaseRedux';
import {Images, Colors} from './../../../../Themes/';
import * as func from './../../../../Helpers/Utils';

import * as c from './../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';

const DropshipIntroScreen = function(props) {
  const access_token = props.checkAccessToken;

  const {
    navigation,
    dispatch,
    supplierData,
    isFocused,
    fcmData,
    tokoId,
    fcmToken,
  } = props;
  const [refreshing, setRefreshing] = useState(false);
  const _handleNavigation = item => {
    navigation.navigate('DropshipDetailScreen', {
      params: item,
    });
  };
  useEffect(() => {
    dispatch(TokoActions.getSupplierRequest());
    dispatch(AuthActions.loginOtpAfterSuccess());
    dispatch(TokoActions.getTokoRequest());
    dispatch(AuthActions.getUserRequest(access_token));
  }, []);

  useEffect(() => {
    dispatch(GlobalActions.saveFirebaseTokenRequest(fcmToken));

    dispatch(TokoActions.getTokoRequest());
  }, []);

  useEffect(() => {
    if (fcmData != null) {
      if (fcmData.purpose === '3') {
        navigation.navigate('Order');
        dispatch(FirebaseTypes.fcmReset());
        dispatch(TokoActions.getTokoOrderRequest(tokoId));
      }
    }
  }, [fcmData]);
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getSupplierRequest());
    wait(500).then(() => setRefreshing(false));
  };
  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleNavigation(item)}
        delayPressIn={0}
        style={{
          backgroundColor: 'white',
          height: 260,

          width: '49.5%',
          padding: 8,
        }}>
        <Card
          style={{
            width: '100%',
            height: '100%',
            padding: 10,
          }}>
          <Image
            style={{
              width: '100%',
              height: 150,
              alignSelf: 'center',
              resizeMode: 'contain',
            }}
            source={
              item
                ? item.photo
                  ? {uri: item.photo}
                  : Images.noImg
                : Images.noImg
            }
          />
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1,
              color: '#111432',
              marginTop: 15,
            }}>
            {item.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              bottom: 20,
              width: '100%',
              marginLeft: 10,
            }}>
            <FontAwesome5
              style={{marginTop: 1}}
              name="map-marker-alt"
              size={12}
              color="#0b997e"
            />

            <Text
              style={{
                color: '#0b997e',
                fontSize: 10.5,
                marginLeft: 5,
                fontFamily: 'NotoSans',
                letterSpacing: 1,
              }}>
              {item.location}
            </Text>
          </View>
        </Card>
      </TouchableOpacity>
    );
  };
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.white,
      }}>
      <FlatList
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={handleRefresh} />
        }
        style={{bottom: 5}}
        numColumns={2}
        data={supplierData}
        renderItem={_renderItem}
        // ListFooterComponent={() => {
        //   return supplierData != null ? (
        //     <Button
        //       //onPress={() => navigation.navigate('AddProduct')}
        //       onPress={() =>
        //         Linking.openURL(
        //           'https://docs.google.com/forms/d/e/1FAIpQLSd2ifLd5fwwDGkZ30_GzPP9radSuKN2nbVOQ5NswjNmFyB7HA/viewform?vc=0&c=0&w=1&flr=0&gxids=7628',
        //         )
        //       }
        //       style={[
        //         c.Styles.btnPrimary,
        //         {width: '95%', alignSelf: 'center'},
        //       ]}>
        //       <Text
        //         style={[
        //           c.Styles.txtCreateMerchantBtn,
        //           {fontWeight: 'bold', letterSpacing: 1},
        //         ]}>
        //         Klaim Komisi
        //       </Text>
        //     </Button>
        //   ) : (
        //     <View />
        //   );
        // }}
      />
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,

    checkProductAvail: state.toko.productTokoData.data
      ? state.toko.productTokoData.data.listProduct
        ? state.toko.productTokoData.data.listProduct.length !== 0
        : true
      : false,
    getToko: state.toko.tokoData.data,
    userData: state.auth.userData,
    checkSuccessAddNewCategory: state.toko.postCategoryTokoData.success,
    AddNewCategoryId: state.toko.postCategoryTokoData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    toko: state.toko.detailTokoData.data
      ? state.toko.detailTokoData.data.toko
        ? state.toko.detailTokoData.data.toko
        : {}
      : {},
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,

    categoryData: state.toko.categoryTokoData.data
      ? state.toko.categoryTokoData.data.listCategory
        ? state.toko.categoryTokoData.data.listCategory
        : []
      : [],
    productPrimary: state.toko.productTokoData,
    productData: state.toko.productTokoData.data
      ? state.toko.productTokoData.data.listProduct
        ? state.toko.productTokoData.data.listProduct.length != 0
          ? state.toko.productTokoData.data.listProduct
          : []
        : []
      : [],
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoAddress:
      state.toko.tokoAddressData.data !== null
        ? state.toko.tokoAddressData.data.length
          ? state.toko.tokoAddressData.data.length !== 0
          : null
        : null,
    fcmData: state.firebase.fcmData.data,
    fcmToken: state.app.fcmToken,
    checkAccessToken: state.app.tokenApi.data,
  };
};
export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(DropshipIntroScreen),
);
