import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  ToastAndroid,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  KeyboardAvoidingView,
  RefreshControl,
  Keyboard,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import Config from 'react-native-config';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Button, Content, Spinner} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {TextInputMask} from 'react-native-masked-text';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import MarketPlaceActions from '../../../../Redux/MarketPlaceRedux';
import GlobalActions from '../../../../Redux/GlobalRedux';
import TokoActions from '../../../../Redux/TokoRedux';
import AuthActions from '../../../../Redux/AuthenticationRedux';
import {Images, Colors} from '../../../../Themes';
import * as func from '../../../../Helpers/Utils';

import AntDesign from 'react-native-vector-icons/AntDesign';

import {WebView} from 'react-native-webview';
import * as c from '../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
const webURL = 'https://toko.ly/tokookofficial/home';
const phoneNumber = '+6287878983919';
const webURLProduct = 'https://toko.ly/tokookofficial/products';
const initialProductForm = {
  name: null,
  desc: null,
  price: null,
  capitalPrice: null,
  images: [],
  category: null,
  addCategory: false,
  newCategory: null,
  categorySelected: null,
  inputOtherBusinessType: null,
  isEdit: false,
  productId: null,
};

const initialAddProductValidation = {
  isNameEmpty: false,
  isPriceEmpty: false,
  iscapitalPriceEmpty: false,
  isCategoryEmpty: false,
};
const initialFilter = {
  isFiltered: false,
  categoryId: null,
  productFiltered: [],
};
const initialValidation = {
  isImageEmpty: false,
  isNameEmpty: false,
  isPriceEmpty: false,
  iscapitalPriceEmpty: false,
  isCategoryEmpty: false,
};

const DropshipDetailScreen = function(props) {
  const {
    navigation,
    categoryData,
    dispatch,
    productData,
    productScrapLoading,
    AddNewCategoryId,
    checkSuccessAddNewCategory,
    checkSuccessScrap,
    getToko,
    getOtpWa,
    userData,
    tokoData,
    tokoAddressData,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    tokoId,
    tokoAddress,
  } = props;
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
    isTokpedLinkInvalid: false,
    isShopeeLinkInvalid: false,
    isBlibliLinkInvalid: false,
    isInstagramLinkInvalid: false,
    isFacebookLinkInvalid: false,
    isLineLinkInvalid: false,
    isYoutubeLinkInvalid: false,
  });
  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialValidation,
  );
  const [isAddedAddress, setIsAddedAddress] = useState(true);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [editSelected, setEditSelected] = useState(null);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState('');
  const [detailAddressValue, setDetailAddressValue] = useState('');
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [token, setToken] = useState('');
  const [timeLeft, setTimeLeft] = useState(0);
  const [errMsg, setErrMsg] = useState(null);
  const [code, setCode] = useState('');
  const [wanumber, setwaNumber] = useState('');
  const [loadingRBSheet, setLoadingRBSheet] = useState(false);
  const [visible, setVisible] = useState(false);
  const [actionType, setActionType] = useState(null);
  const [tokoCategory, setTokoCategory] = useState(null);
  const [imagesTemp, setImagesTemp] = useState([]);
  const [filterProduct, setFilterProduct] = useState(initialFilter);
  const [productForm, setProductForm] = useStateWithCallbackLazy(
    initialProductForm,
  );

  const [name, setName] = useState(null);
  const [step, setStep] = useState(1);
  const [loadingStep, setloadingStep] = useState(false);
  const [formToko, setFormToko] = useState({
    tncCheck: true,
    link: null,
    name: null,
    whatsapp: null,
    businessTypeSelected: {
      id: 12,
      icon:
        'https://payok-toko-bucket.s3-ap-southeast-1.amazonaws.com/business-type/Others.png',
    },
    inputOtherBusinessType: null,
    tokopedia: null,
    shopee: null,
    blibli: null,
    instagram: null,
    facebook: null,
    line: null,
    youtube: null,
  });
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [productValidation, setProductValidation] = useState(initialValidation);
  const refAddNewCategory = useRef();
  const refAddProduct = useRef();
  const refRBSheetAddToko = useRef();
  const [refreshing, setRefreshing] = useState(false);
  const [addProductValidation, setAddProductValidation] = useState(
    initialAddProductValidation,
  );
  const [webviewConfig, setWebviewConfig] = useState({
    canGoBack: false,
    loading: false,
    canGoForward: false,
    url: '',
    urlProduk: false,
    title: '',
  });
  const refInputBusinessType = useRef();
  const [categoryName, setCategoryName] = useState(null);
  const getParams = navigation.getParam('params');
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const webview = useRef(null);
  const refRBSheet = useRef(null);
  const [loadingAddCategory, setLoadingAddCategory] = useState(false);
  const isMatch = (searchOnString, searchText) => {
    searchText = searchText.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    return (
      searchOnString.match(new RegExp('\\b' + searchText + '\\b', 'i')) != null
    );
  };
  useEffect(() => {
    if (getOtpWa.success) {
      const {token} = getOtpWa.data.data;
      setTimeLeft(30);
      setToken(token);
    }
  }, [getOtpWa.success]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }
    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // clear interval on re-render to avoid memory leaks

    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getDetailTokoRequest(props.tokoId));
      props.dispatch(TokoActions.getProvinceRequest());
      props.dispatch(TokoActions.getBankRequest());
    }
  }, [props.tokoId]);
  useEffect(() => {
    props.dispatch(TokoActions.getProvinceRequest());
  }, []);
  // useEffect(() => {
  //   if (userData.success) {
  //     console.log(tokoData);
  //     //props.dispatch(AuthActions.getUserRequest(props.access_token));
  //     if (userData.data.is_phone_verified && step < 2) {
  //       setStep(step + 1);
  //     } else if (tokoData && step < 3) {
  //       setStep(step + 1);
  //     }
  //   } else {
  //     props.dispatch(AuthActions.getUserRequest(props.access_token));
  //   }
  // });
  // useEffect(() => {
  //   if (productData.length != 0) {
  //     if (isTokoAvailable) {
  //       const checkCategoryExists = categoryData.listCategory.filter(
  //         (x) => x.name === productData.categories[0],
  //       );
  //       if (checkCategoryExists.length === 0) {
  //         setCategoryName(
  //           productData.categories ? productData.categories[0] : '',
  //         );
  //         setProductForm({
  //           ...productForm,
  //           name: productData.name,
  //           price: productData.price,
  //           desc: productData.description_combined,
  //           newCategory: productData.categories[0],

  //           addCategory: true,
  //           categorySelected: true,
  //         });
  //       } else {
  //         setCategoryName(
  //           checkCategoryExists ? checkCategoryExists[0].name : '',
  //         );
  //         setProductForm({
  //           ...productForm,
  //           name: productData.name,
  //           price: productData.price,
  //           desc: productData.description_combined,
  //           categorySelected: checkCategoryExists[0].id,
  //         });
  //       }
  //     }
  //   }
  // }, [productData]);
  const handleGoBack = () => {
    // console.log('cek url kita', webviewConfig.url);
    // if (webURL === webviewConfig.url) {
    //   props.navigation.navigate({routeName: 'TokoScreen'});
    // } else {

    if (webviewConfig.url.includes('home') || webviewConfig.url === '') {
      navigation.goBack();
      return true;
    } else {
      webview.current.goBack();
      // webview.current.reload();

      return true;
    }
    // }
    //
  };
  useEffect(() => {
    dispatch(AuthActions.getUserRequest(props.access_token));
  }, []);
  useEffect(() => {
    if (tokoId) {
      dispatch(TokoActions.getCategoryTokoRequest(tokoId));
      dispatch(TokoActions.getProductTokoRequest(tokoId));
      dispatch(TokoActions.getTokoAddressRequest(tokoId));
    }
  }, [tokoId]);
  const handlegetOtp = () => {
    const data = {
      phone: wanumber,
      source: 'phone_verification',
      type: 'whatsapp',
    };
    Keyboard.dismiss();
    props.dispatch(AuthActions.getOtpPostRequest(data));
  };
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const handleDeleteProduct = () => {
    setLoadingRBSheet(true);
    const data = {
      tokoId: props.tokoId,
      productId: productForm.productId,
      func: {callback: handleRefresh},
    };
    dispatch(TokoActions.deleteProductTokoRequest(data));
  };
  const handleNewProduct = () => {
    setLoadingRBSheet(false);
    setProductForm(initialProductForm);
    refRBSheet.current.open();
  };
  const addProductList = () => (
    <View
      style={[
        c.Styles.borderBottom,
        {
          paddingHorizontal: 20,
          paddingVertical: 10,
          borderBottomColor: '#dddddd',
          borderBottomWidth: 0.75,
        },
      ]}>
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => handleNewProduct}
        style={{flexDirection: 'row', alignItems: 'center'}}>
        <View
          style={{
            width: 50,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 5,
            borderColor: 'lightgrey',
            borderWidth: 1,
            backgroundColor: 'white',
            marginRight: 10,
          }}>
          <AntDesign name="pluscircleo" size={25} color={c.Colors.grayDark} />
        </View>
        <Text style={{fontWeight: 'bold', color: c.Colors.grayWhiteDark}}>
          Tambah Produk
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => setTabIndex(2)}
        style={{flexDirection: 'row', alignItems: 'center'}}>
        <View
          style={{
            width: 50,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 5,
            borderColor: 'lightgrey',
            borderWidth: 1,
            backgroundColor: 'white',
            marginRight: 10,
            marginTop: 12,
          }}>
          <AntDesign name="pluscircleo" size={25} color={c.Colors.grayDark} />
        </View>
        <Text style={{fontWeight: 'bold', color: c.Colors.grayWhiteDark}}>
          Tambah Produk Dropship
        </Text>
      </TouchableOpacity>
    </View>
  );
  const productImageListFooter = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => setShowUploadModal(true)}>
        <c.Image
          source={{
            uri:
              'https://payok-assets-bucket.s3-ap-southeast-1.amazonaws.com/mobile/upload_photo.png',
          }}
          style={{
            width: 100,
            height: 100,
            borderRadius: 5,
            marginVertical: 10,
            marginRight: 10,
          }}
        />
      </TouchableOpacity>
    );
  };
  const productImageListNew = ({item, index}) => {
    return (
      <View>
        <c.Image
          source={item.url ? {uri: item.url} : Images.noImg}
          style={{
            width: 100,
            height: 100,
            borderRadius: 5,
            marginVertical: 10,
            marginRight: 10,
            borderColor: c.Colors.grayDark,
            borderWidth: 0.5,
          }}
        />
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => handleRemoveImage(item)}
          style={{
            padding: 5,
            position: 'absolute',
            right: 5,
            borderRadius: 25,
            backgroundColor: c.Colors.gray,
          }}>
          <MaterialCommunityIcons name="close-thick" size={17} color="white" />
        </TouchableOpacity>
      </View>
    );
  };
  const categoryButton = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          handleCategory(item.id);
          setCategoryName(item.name);
        }}>
        <View
          style={{
            backgroundColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : 'white',
            borderColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                productForm.categorySelected === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight:
                productForm.categorySelected === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const categoryFooter = ({item, index}) => {
    return (
      <TouchableOpacity delayPressIn={0} onPress={handleAddNewCategory}>
        <View
          style={{
            backgroundColor: productForm.addCategory
              ? c.Colors.mainBlue
              : 'white',
            borderColor: productForm.addCategory
              ? c.Colors.mainBlue
              : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color: productForm.addCategory
                ? c.Colors.white
                : c.Colors.grayWhite,
              fontWeight: productForm.addCategory ? 'bold' : '400',
            }}>
            + Tambah Kategori
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const handleCategory = item => {
    setProductForm({
      ...productForm,
      categorySelected: item,
      addCategory: false,
    });
    setAddProductValidation({...addProductValidation, isCategoryEmpty: false});
  };

  const handleFilterCategory = item => {
    if (filterProduct.categoryId !== item) {
      const productFiltered = listTokoProduct.filter(el => {
        return el.category_id == item;
      });
      setFilterProduct({
        ...filterProduct,
        productFiltered: productFiltered,
        isFiltered: true,
        categoryId: item,
      });
    } else {
      setFilterProduct({productFiltered: [], isFiltered: false});
    }
  };
  const categoryFilter = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => handleFilterCategory(item.id)}>
        <View
          style={{
            backgroundColor:
              filterProduct.categoryId === item.id
                ? c.Colors.mainBlue
                : 'white',
            borderColor:
              filterProduct.categoryId === item.id
                ? c.Colors.mainBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                filterProduct.categoryId === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight: filterProduct.categoryId === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const handleAddNewCategory = () => {
    setProductForm(
      {
        ...productForm,
        addCategory: true,
        categorySelected: null,
        newCategory: '',
      },
      () => {
        refAddNewCategory.current.focus();
      },
    );
  };
  const productImageList = ({item, index}) => {
    return (
      <View>
        <c.Image
          source={item ? {uri: item} : Images.noImg}
          style={{
            width: 100,
            height: 100,
            borderRadius: 5,
            marginVertical: 10,
            marginRight: 10,
            borderColor: c.Colors.grayDark,
            borderWidth: 0.5,
          }}
        />
      </View>
    );
  };
  const handleAddTokoCategory = () => {
    if (productForm.newCategory === '') {
      ToastAndroid.show(
        'Isikan Nama Kategori Terlebih Dahulu !',
        ToastAndroid.SHORT,
      );
    } else {
      setLoadingAddCategory(true);
      const data = {
        tokoId: props.tokoId,
        item: {name: productForm.newCategory},
        func: {
          reloadData: response => {
            setLoadingAddCategory(false);
            setProductForm({
              ...productForm,
              addCategory: false,
              categorySelected: response.productCategory.id,
            });
            setAddProductValidation({
              ...addProductValidation,
              isCategoryEmpty: false,
            });
            dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
          },
        },
      };
      dispatch(TokoActions.postCategoryTokoRequest(data));
    }
  };
  useEffect(() => {
    setFormToko({...formToko, isBusinessTypeEmpty: false});
    if (step === 2 && formToko.businessTypeSelected.id === 12) {
      refInputBusinessType.current.focus();
    }
  }, [formToko.businessTypeSelected.id]);

  useEffect(() => {
    if (formToko.name) {
      setTokoValidation({...tokoValidation, isNameTokoEmpty: false});
    }
    if (formToko.link) {
      setTokoValidation({...tokoValidation, isLinkTokoEmpty: false});
    }
    if (formToko.whatsapp) {
      setTokoValidation({...tokoValidation, isWhatsappEmpty: false});
    }
    if (tokoValidation.isTokpedLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      });
    }
    if (tokoValidation.isShopeeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      });
    }
    if (tokoValidation.isBlibliLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      });
    }
    if (tokoValidation.isInstagramLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      });
    }
    if (tokoValidation.isFacebookLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      });
    }
    if (tokoValidation.isLineLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isLineLinkInvalid: !isLinkValid(formToko.line),
      });
    }
    if (tokoValidation.isYoutubeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
    }
  }, [formToko]);

  const isLinkValid = link => {
    if (typeof link === 'string' && link.length) {
      if (
        link.toLowerCase().includes('http') ||
        link.toLowerCase().includes('www')
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  const stepBack = () => {
    setStep(step - 1);
  };
  const handleBackButtonClick = () => {
    if (tabIndex !== 0) {
      setTabIndex(0);
      return true;
    }
  };

  const stepNext = () => {
    setTokoValidation({
      isNameTokoEmpty: !formToko.name ? true : false,
      isLinkTokoEmpty: !formToko.link ? true : false,
      //isWhatsappEmpty: !formToko.whatsapp ? true : false,
      isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      isLineLinkInvalid: !isLinkValid(formToko.line),
      isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
    });

    const {
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
      tncCheck,
    } = formToko;

    const data = {
      business_type_id: businessTypeSelected.id,
      name: name,
      link: link,
      whatsapp: whatsapp,
      instagram: instagram,
      facebook: facebook,
      line: line,
      youtube: youtube,
      tokopedia: tokopedia,
      shopee: shopee,
      blibli: blibli,
    };
    switch (step) {
      case 1: {
        const data = {
          item: {
            phone: wanumber,
            code: code,
            token: token,
          },
          func: {
            reloadData: () => {
              setloadingStep(false);
              props.dispatch(AuthActions.getUserRequest(props.access_token));
              if (!isVerified) {
                if (!tokoData) {
                  setStep(2);
                } else if (!tokoAddress) {
                  setStep(3);
                } else {
                  refRBSheet.current.close();
                }
              }
            },
            reloadFailed: () => {
              setloadingStep(false);
            },
          },
        };
        setloadingStep(true);
        props.dispatch(AuthActions.whatsappVerificationRequest(data));
      }
      case 2: {
        // setStep(3);
        //Validate first
        if (name && link) {
          const data = {
            item: {
              name: name,
              link: link.toLowerCase(),
              whatsapp: wanumber,
            },
            func: {
              reloadData: () => {
                setStep(3);
                setloadingStep(false);
                props.dispatch(TokoActions.getDetailTokoRequest(props.tokoId));
                props.dispatch(TokoActions.getProvinceRequest());
                props.dispatch(TokoActions.getBankRequest());
              },
              reloadFailed: () => {
                setloadingStep(false);
              },
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.tokoPostRequest(data));
        }
        break;
      }
      case 3: {
        if (provinceValue === null || provinceValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: true,
          });
        } else if (cityValue === null || cityValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: true,
          });
        } else if (districtValue === null || districtValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: true,
          });
        } else if (postalCodeValue === null || postalCodeValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isPostalCodeEmpty: true,
          });
        } else if (addressValue === null || addressValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isAddressEmpty: true,
          });
        } else {
          const postData = {
            tokoId: props.tokoId,
            func: {
              rb: refRBSheet,
            },
            purpose: 1,
            addressId: addressIdValue,
            purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
            detail: {
              province_id: provinceValue.id,
              city_id: cityValue.id,
              district_id: districtValue.id,
              postal_code: postalCodeValue,
              address: addressValue.concat(detailAddressValue),
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.postTokoAddressRequest(postData));
        }
        break;
      }

      default:
        break;
    }
  };

  const handleAddProduct = () => {
    console.log('handleaddproduct');
    //let priceFinal = Number(productForm.price.replace(/[Rp.]/g, ''));
    setAddProductValidation({
      isImageEmpty: productData.image
        ? productData.image.length === 0
          ? true
          : false
        : false,
      isNameEmpty:
        productForm.name === '' || productForm.name === null ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0 ||
        productForm.price === '0'
          ? true
          : false,
      iscapitalPriceEmpty:
        productForm.capitalPrice === null ||
        productForm.capitalPrice === 'Rp0' ||
        productForm.capitalPrice === '' ||
        productForm.capitalPrice == 0
          ? true
          : false,
      isCategoryEmpty:
        categoryName === '' ||
        categoryName === null ||
        categoryName === undefined
          ? true
          : !productForm.categorySelected
          ? true
          : false,
    });
    if (
      productData.image.length &&
      productForm.name &&
      productForm.price &&
      categoryName
    ) {
      if (productForm.newCategory !== '' && productForm.addCategory) {
        setLoadingRBSheet(true);
        handleAddTokoCategory();
      } else {
        setLoadingRBSheet(true);
        const data = {
          id: props.tokoId,
          item: {
            supplier_id: getParams.id,
            source_url: webviewConfig.url,

            name:
              productForm.name === null ? productData.name : productForm.name,
            description:
              productForm.desc === null
                ? productData.description_combined
                : productForm.desc,
            price:
              typeof productForm.price === 'string'
                ? Number(productForm.price.replace(/[Rp.]/g, ''))
                : productForm.price,
            capital_price:
              productForm.capitalPrice === null
                ? typeof productData.price === 'string'
                  ? Number(productData.price.replace(/[Rp.]/g, ''))
                  : productData.price
                : typeof productForm.capitalPrice === 'string'
                ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                : productForm.capitalPrice,
            category_id:
              AddNewCategoryId != null
                ? AddNewCategoryId.productCategory != undefined
                  ? AddNewCategoryId.productCategory.id
                  : productForm.categorySelected
                : productForm.categorySelected,
            images: productData.image
              ? productData.image.length > 5
                ? productData.image.slice(0, 5)
                : productData.image
              : null,
          },
          func: {
            reloadData: () => {
              setProductForm(initialProductForm);
              setLoadingRBSheet(false);
              refRBSheet.current.close();
            },
          },
        };

        dispatch(TokoActions.postProductTokoRequest(data));
      }
    }
  };
  const handleAddNewProduct = () => {
    console.log('handleAddNewProduct');
    setProductValidation({
      isImageEmpty: imagesTemp.length === 0 ? true : false,
      isNameEmpty: !productForm.name ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? true
          : false,
      isCategoryEmpty: !productForm.categorySelected ? true : false,
    });

    if (
      imagesTemp.length > 0 &&
      productForm.name &&
      (productForm.price === null ||
      productForm.price === 'Rp0' ||
      productForm.price === '' ||
      productForm.price == 0
        ? false
        : true) &&
      productForm.categorySelected &&
      productForm
    ) {
      if (productForm.newCategory != '' && productForm.addCategory) {
        setLoadingRBSheet(true);

        handleAddTokoCategory();
      } else {
        setLoadingRBSheet(true);

        let urlImages = [];

        imagesTemp.forEach((img, i) => {
          const form = new FormData();
          form.append('image', {
            uri: img.uri ? img.uri : Images.noImg,
            name: img.name,
            type: img.type,
          });

          const uploadData = {
            tokoId: tokoId,
            item: form,
            func: {
              reloadData: url => {
                urlImages[i] = url;

                const data = {
                  id: props.tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    price: Number(productForm.price.replace(/[Rp.]/g, '')),
                    capital_price:
                      typeof productForm.capitalPrice === 'string'
                        ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                        : productForm.capitalPrice,
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                  },
                  func: {reloadData: handleRefresh},
                };
                if (i >= imagesTemp.length - 1 && i >= urlImages.length - 1) {
                  setTimeout(() => {
                    dispatch(TokoActions.postProductTokoRequest(data));
                  }, 1000);
                }
              },
            },
          };
          dispatch(TokoActions.uploadProductImageRequest(uploadData));
          // ImageResizer.createResizedImage(img.uri, 800, 800, 'JPEG', 800)
          //   .then(({uri}) => {

          //   })
          //   .catch((err) => {
          //     console.log(err);
          //     return Alert.alert(
          //       'Unable to resize the photo',
          //       'Check the console for full the error message',
          //     );
          //   });
          // func.handleUploadTokoProduct(props.dispatch, props.idToko, formData, (url)=> {
          //     urlImages.push(url)

          //     if (i >= imagesTemp.length - 1) {
          //         func.handleAddTokoProduct(props.dispatch, props.idToko, data, (success) => {
          //             if (success) {
          //                 handleShowSnackBar('Berhasil menambahkan produk')
          //                 handleRefresh()
          //             }
          //         })
          //     }

          // })
        });
      }
    }
  };
  const handlePickImage = source => {
    if (source === 'gallery') {
      ImagePicker.openPicker({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
      }).then(image => {
        setImagesTemp([
          ...imagesTemp,
          {
            id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    } else if (source === 'camera') {
      ImagePicker.openCamera({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
      }).then(image => {
        setImagesTemp([
          ...imagesTemp,
          {
            id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    }
  };
  const searchingPostalCode = searchText => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerCity(false);
        setOpenPickerProvince(false);
        setOpenPickerDistrict(false);

        setOpenPickerPostalCode(true);
        let filteredData = postalCodeData.postalCode.filter(function(item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setOpenPickerCity(false);
      setOpenPickerProvince(false);
      setOpenPickerDistrict(false);

      setPostalCodeValue(searchText);
    }
  };
  const handleAddAdressSubmit = () => {};
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() =>
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          )
        }
        style={{
          borderWidth: 0.1,
          borderColor: 'transparent',
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode ? item.postal_code : item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          props.dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          props.dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const handleRemoveImage = item => {
    const filtered = imagesTemp.filter(e => {
      return e.id !== item.id;
    });

    setImagesTemp(filtered);

    if (productForm.isEdit && item.hasOwnProperty('priority')) {
      productForm.isEdit && setDeletedImages([...deletedImages, item.id]);
    }
  };
  const handleCleanTemp = () => {
    setCategoryName(null);
    setImagesTemp([]);
    setProductForm({...productForm, isEdit: false});
    setDeletedImages([]);
    setNewImages([]);
    setProductValidation(initialValidation);
    dispatch(TokoActions.postCategoryTokoReset());
    setProductForm(initialProductForm);
    setAddProductValidation(initialAddProductValidation);
  };
  const handleCloseRBSheet = () => {
    refRBSheet.current.close();
    setProductForm(initialProductForm);
    setAddProductValidation(initialAddProductValidation);
    dispatch(TokoActions.postCategoryTokoReset());
  };
  const handleCleanState = () => {
    // props.dispatch(TokoActions.postTokoBankReset());
    props.dispatch(TokoActions.postTokoAddressReset());

    setCityValue(null);
    setPostalCodeValue(null);
    setProvinceValue(null);
    setCityValue(null);
    setOpenPickerProvince(false);
    setOpenPickerCity(false);
    setOpenPickerDistrict(false);
    setOpenPickerPostalCode(false);
    setDistrictValue(null);
    setDetailAddressValue(null);
    setAddressIdValue(0);
    setAddressValue(null);
    // setIsAddedAddress(false), setIsUpdatedAddress(false);
    setAddedAddressFormValidation(initialValidation);
    // setIsAddedBank(false);
    // setOpenPickerBank(false);
    // // setBankNameValue(null);
    // setBankOwnerNameValue(null);
    // setBankIndex(-1);
    // setBankValue(null);
    // // setBankNameValue(null)
    // setIsUpdatedBank(false);
  };
  const renderBottomSheetContent = () => {
    switch (actionType) {
      case 'add': {
        return (
          <View style={{paddingHorizontal: 5, marginBottom: 20}}>
            <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
              <Text style={[c.Styles.txtCreateMerchant, {fontWeight: 'bold'}]}>
                Tambah Produk
              </Text>
            </View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent, {flexGrow: 1}]}
              showsVerticalScrollIndicator={false}>
              <View style={[c.Styles.flexRow]}>
                <FlatList
                  renderItem={productImageList}
                  data={
                    productData.image
                      ? productData.image.length > 5
                        ? productData.image.slice(0, 5)
                        : productData.image
                      : null
                  }
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Nama Produk *</Text>
                <View style={c.Styles.flexOne}>
                  <TextInput
                    style={[c.Styles.txtInputBordered, c.Styles.borderGrey]}
                    placeholder="Masukkan Nama Produk"
                    placeholderTextColor={c.Colors.gray}
                    value={productForm.name}
                    onChangeText={text => {
                      setProductForm({...productForm, name: text});
                    }}
                  />
                </View>
                <View style={c.Styles.viewCheck}>
                  {addProductValidation.isNameEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib diisi
                    </Text>
                  )}
                </View>
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Deskripsi Produk</Text>
                <View style={c.Styles.flexOne}>
                  <TextInput
                    style={[
                      c.Styles.txtInputBordered,
                      c.Styles.borderGrey,
                      {paddingVertical: 10},
                    ]}
                    placeholder="Masukkan Deskripsi Produk"
                    multiline={true}
                    numberOfLines={5}
                    textAlignVertical="top"
                    value={
                      productForm.desc === null
                        ? productData.description_combined.replace('\n', '\n')
                        : productForm.desc
                    }
                    placeholderTextColor={c.Colors.gray}
                    onChangeText={text => {
                      setProductForm({...productForm, desc: text});
                    }}
                  />
                </View>
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Harga Modal</Text>
                <View style={c.Styles.flexOne}>
                  <TextInputMask
                    style={[
                      c.Styles.txtInputBordered,
                      c.Styles.borderGrey,
                      {color: '#c9c6c6'},
                    ]}
                    type={'money'}
                    editable={false}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={
                      productForm.capitalPrice
                        ? productForm.capitalPrice
                        : productData.price
                    }
                    onChangeText={text => {
                      setProductForm({
                        ...productForm,
                        capitalPrice: text,
                      });
                    }}
                  />
                </View>
                {productValidation.iscapitalPriceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib diisi
                  </Text>
                )}
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Harga Jual *</Text>
                <View style={c.Styles.flexOne}>
                  <TextInputMask
                    style={[c.Styles.txtInputBordered, c.Styles.borderGrey]}
                    type={'money'}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={productForm.price ? productForm.price : null}
                    onChangeText={text => {
                      setProductForm({...productForm, price: text});
                    }}
                  />
                </View>
                {addProductValidation.isPriceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib diisi
                  </Text>
                )}
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <View
                  style={[
                    c.Styles.borderBottom,
                    c.Styles.marginBottomTen,
                    {
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor:
                        productForm.newCategory ||
                        categoryName != null ||
                        categoryData.length != 0
                          ? '#ccc'
                          : 'red',
                    },
                  ]}>
                  <TextInput
                    ref={refAddNewCategory}
                    // editable={productForm.addCategory}
                    placeholder={
                      // productForm.addCategory
                      //   ? 'Buat Kategori Baru'
                      //   :
                      categoryData.listCategory.length != 0
                        ? 'Buat Kategori Baru Atau Pilih Dibawah'
                        : 'Buat Kategori Baru'
                    }
                    // blurOnSubmit={true}
                    value={
                      categoryName != null
                        ? categoryName
                        : productForm.newCategory
                    }
                    placeholderTextColor={
                      productForm.newCategory ||
                      categoryName != null ||
                      categoryData.length != 0
                        ? c.Colors.grayWhite
                        : 'red'
                    }
                    onChangeText={text => {
                      setCategoryName(text);

                      setProductForm({
                        ...productForm,
                        newCategory: text,
                        addCategory: true,
                        categorySelected: true,
                      });
                    }}
                    style={[
                      {
                        paddingHorizontal: 0,
                        paddingBottom: 5,
                        paddingTop: 0,
                        flex: 1,
                        color: '#000',
                      },
                    ]}></TextInput>
                  {/* {!productData.length && categoryName ===null  && (
                    <Text
                      style={[
                        c.Styles.txtInputDesc,
                        {color: 'red', marginTop: 5},
                      ]}>
                      Wajib membuat kategori
                    </Text>
                  )} */}
                </View>
                <FlatList
                  renderItem={categoryButton}
                  extraData={productForm.categorySelected}
                  data={categoryData ? categoryData.listCategory : null}
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  // ListFooterComponent={categoryFooter}
                  keyExtractor={(item, index) => index.toString()}
                />
                {addProductValidation.isCategoryEmpty && (
                  <Text
                    style={[
                      c.Styles.txtInputDesc,
                      {color: 'red', marginTop: 5},
                    ]}>
                    Wajib memilih kategori
                  </Text>
                )}
              </View>
            </ScrollView>
            <View
              style={[
                c.Styles.flexRow,
                c.Styles.justifyspaceBetween,
                {bottom: 14},
              ]}>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnOutline]}
                  onPress={handleCloseRBSheet}>
                  <Text
                    style={[
                      {
                        fontWeight: 'bold',
                        letterSpacing: 1,
                        color: c.Colors.mainBlue,
                      },
                    ]}>
                    Batal
                  </Text>
                </Button>
              </View>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnPrimary]}
                  onPress={handleAddProduct}>
                  <Text
                    style={[
                      c.Styles.txtCreateMerchantBtn,
                      {fontWeight: 'bold', letterSpacing: 1},
                    ]}>
                    Simpan
                  </Text>
                </Button>
              </View>
            </View>
            {/* <View style={[c.Styles.flexRow, c.Styles.justifyspaceBetween]}>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnOutline]}
                  onPress={() =>
                    // productForm.isEdit
                    //   ? handleDeleteProduct(productForm.productId)
                    //   :
                    refRBSheet.current.close()
                  }>
                  <Text
                    style={[
                      {
                        fontWeight: 'bold',
                        letterSpacing: 1,
                        color: c.Colors.mainBlue,
                      },
                    ]}>
                    {productForm.isEdit ? 'Hapus' : 'Batal'}
                  </Text>
                </Button>
              </View>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  onPress={() => refRBSheet.current.close()}
                  style={[c.Styles.btnPrimary]}>
                  <Text
                    style={[
                      c.Styles.txtCreateMerchantBtn,
                      {fontWeight: 'bold', letterSpacing: 1},
                    ]}>
                    Simpan
                  </Text>
                </Button>
              </View>
            </View> */}
          </View>
        );
        break;
      }
      case 'addNew': {
        return (
          <View style={{paddingHorizontal: 5, marginBottom: 20}}>
            <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
              <Text style={[c.Styles.txtCreateMerchant, {fontWeight: 'bold'}]}>
                {'Tambah Produk'}
              </Text>
            </View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <FlatList
                renderItem={productImageListNew}
                data={imagesTemp}
                showsHorizontalScrollIndicator={false}
                horizontal
                ListFooterComponent={
                  imagesTemp.length < 5 && productImageListFooter
                }
                keyExtractor={(item, index) => index.toString()}
              />
              {productValidation.isImageEmpty && (
                <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                  Wajib masukkan 1 foto produk (sisanya optional)
                </Text>
              )}
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Nama Produk *</Text>
                <View style={c.Styles.flexOne}>
                  <TextInput
                    style={[c.Styles.txtInputBordered, c.Styles.borderGrey]}
                    placeholder="Masukkan Nama Produk"
                    placeholderTextColor={c.Colors.gray}
                    value={productForm.name}
                    onChangeText={text => {
                      setProductForm({...productForm, name: text});
                    }}
                  />
                </View>
                <View style={c.Styles.viewCheck}>
                  {productValidation.isNameEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib diisi
                    </Text>
                  )}
                </View>
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Deskripsi Produk</Text>
                <View style={c.Styles.flexOne}>
                  <TextInput
                    style={[
                      c.Styles.txtInputBordered,
                      c.Styles.borderGrey,
                      {paddingVertical: 10},
                    ]}
                    placeholder="Masukkan Deskripsi Produk"
                    multiline={true}
                    numberOfLines={4}
                    textAlignVertical="top"
                    value={productForm.description}
                    placeholderTextColor={c.Colors.gray}
                    onChangeText={text => {
                      setProductForm({
                        ...productForm,
                        description: text,
                      });
                    }}
                  />
                </View>
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Harga Modal</Text>
                <View style={c.Styles.flexOne}>
                  <TextInputMask
                    style={[c.Styles.txtInputBordered, c.Styles.borderGrey]}
                    type={'money'}
                    editable={false}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={
                      productForm.capitalPrice ? productForm.capitalPrice : null
                    }
                    onChangeText={text => {
                      setProductForm({
                        ...productForm,
                        capitalPrice: text,
                      });
                    }}
                  />
                </View>
                {productValidation.iscapitalPriceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib diisi
                  </Text>
                )}
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <Text style={[c.Styles.txtInputTitle]}>Harga Jual *</Text>
                <View style={c.Styles.flexOne}>
                  <TextInputMask
                    style={[c.Styles.txtInputBordered, c.Styles.borderGrey]}
                    type={'money'}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={productForm.price ? productForm.price : null}
                    onChangeText={text => {
                      setProductForm({...productForm, price: text});
                    }}
                  />
                </View>
                {productValidation.isPriceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib diisi
                  </Text>
                )}
              </View>
              <View style={c.Styles.viewInputWrapper}>
                <View
                  style={[
                    c.Styles.borderBottom,
                    c.Styles.marginBottomTen,
                    {
                      flexDirection: 'row',
                      alignItems: 'center',
                      borderBottomColor:
                        productForm.newCategory ||
                        categoryName != null ||
                        categoryData.length != 0
                          ? '#ccc'
                          : 'red',
                    },
                  ]}>
                  <TextInput
                    ref={refAddNewCategory}
                    // editable={productForm.addCategory}
                    placeholder={
                      categoryData.length != 0
                        ? 'Buat Kategori Baru Atau Pilih Dibawah'
                        : 'Buat Kategori Baru'
                    }
                    // blurOnSubmit={true}
                    value={
                      categoryName != null
                        ? categoryName
                        : productForm.newCategory
                    }
                    placeholderTextColor={
                      productForm.newCategory ||
                      categoryName != null ||
                      categoryData.length != 0
                        ? c.Colors.grayWhite
                        : 'red'
                    }
                    onChangeText={text => {
                      setProductForm({
                        ...productForm,
                        newCategory: text,
                        addCategory: true,
                        categorySelected: true,
                      });
                    }}
                    style={[
                      {
                        paddingHorizontal: 0,
                        paddingBottom: 5,
                        paddingTop: 0,
                        flex: 1,
                        color: '#000',
                      },
                    ]}></TextInput>
                </View>
                <FlatList
                  renderItem={categoryButton}
                  data={categoryData ? categoryData.listCategory : null}
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  // ListFooterComponent={categoryFooter}
                  keyExtractor={(item, index) => index.toString()}
                />
                {productValidation.isCategoryEmpty && (
                  <Text
                    style={[
                      c.Styles.txtInputDesc,
                      {color: 'red', marginTop: 5},
                    ]}>
                    Wajib memilih kategori
                  </Text>
                )}
              </View>
            </ScrollView>

            <View style={[c.Styles.flexRow, c.Styles.justifyspaceBetween]}>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnOutline]}
                  onPress={
                    productForm.isEdit
                      ? () => setShowDeleteModal(true)
                      : handleCloseRBSheet
                  }>
                  <Text
                    style={[
                      {
                        fontWeight: 'bold',
                        letterSpacing: 1,
                        color: c.Colors.mainBlue,
                      },
                    ]}>
                    {productForm.isEdit ? 'Hapus' : 'Batal'}
                  </Text>
                </Button>
              </View>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnPrimary]}
                  onPress={handleAddNewProduct}>
                  <Text
                    style={[
                      c.Styles.txtCreateMerchantBtn,
                      {fontWeight: 'bold', letterSpacing: 1},
                    ]}>
                    Simpan
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        );
        break;
      }
      case 'addToko': {
        const {
          tncCheck,
          name,
          link,
          whatsapp,
          tokopedia,
          shopee,
          blibli,
          instagram,
          facebook,
          line,
          youtube,
          businessTypeSelected,
          inputOtherBusinessType,
          bottomSheetRef,
        } = formToko;
        const {
          isLinkTokoEmpty,
          isNameTokoEmpty,
          isWhatsappEmpty,
          isTokpedLinkInvalid,
          isBusinessTypeEmpty,
          isShopeeLinkInvalid,
          isBlibliLinkInvalid,
          isInstagramLinkInvalid,
          isFacebookLinkInvalid,
          isLineLinkInvalid,
          isYoutubeLinkInvalid,
        } = tokoValidation;
        return step == 1 ? (
          <View>
            <View
              style={[
                c.Styles.flexRow,
                c.Styles.alignItemsCenter,
                {marginTop: 20},
              ]}>
              <Text style={c.Styles.txtverifWA}>Verifikasi Whatsapp</Text>
            </View>
            <View style={styles.justifyCenter}>
              <View style={styles.viewinputwaNumber}>
                <View style={c.Styles.flexRow}>
                  <View style={c.Styles.justifyCenter}>
                    <c.Image
                      source={Images.waIcon}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <TextInput
                    keyboardType={'number-pad'}
                    style={styles.txtinputwaNumber}
                    onChangeText={text => {
                      setwaNumber(text);
                      setErrMsg(null);
                    }}
                    placeholderTextColor={'#bdbdbd'}
                    placeholder={'Nomor Whatsapp'}
                    value={wanumber}
                  />
                  <TouchableOpacity
                    style={[c.Styles.justifyCenter]}
                    onPress={timeLeft > 0 ? null : handlegetOtp}
                    disabled={props.isFetchingOtp}>
                    <Text
                      style={[
                        styles.txtkirimOtp,
                        {opacity: timeLeft === 0 ? 1 : 0.4},
                      ]}>
                      Kirim OTP
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <Text style={styles.txtbottomwaNumber}>
                  Agar pembeli bisa menghubungi kamu
                </Text>
              </View>
              <View
                style={[c.Styles.alignCenter, {marginTop: '10%', height: 100}]}>
                {getOtpWa.success ? (
                  <View>
                    <c.PinVerify
                      autoFocus={true}
                      cellStyle={styles.cellStyle}
                      cellStyleFocused={styles.cellStyleFocused}
                      textStyle={styles.txtCellStyle}
                      textStyleFocused={styles.textStyleFocused}
                      value={code}
                      editable={getOtpWa.success ? true : false}
                      onTextChange={code => setCode(code)}
                    />
                    <View
                      style={[
                        ,
                        {
                          marginTop: '10%',
                          flexDirection: 'row',
                          justifyContent: 'space-around',
                        },
                      ]}>
                      <TouchableOpacity
                        style={c.Styles.justifyCenter}
                        onPress={handlegetOtp}
                        disabled={timeLeft !== 0 ? true : false}>
                        <Text
                          style={[
                            styles.txtresendOtp,
                            {opacity: timeLeft === 0 ? 1 : 0.4},
                          ]}>
                          {' '}
                          Kirim Ulang OTP
                        </Text>
                      </TouchableOpacity>

                      <Text style={styles.txttimeLeft}>
                        00:{timeLeft < 10 ? '0' : ''}
                        {timeLeft}
                      </Text>
                    </View>
                  </View>
                ) : null}
              </View>

              <View
                style={{
                  top: 30,
                  flexDirection: 'row',
                }}>
                <TouchableOpacity
                  //onPress={handleResendOtp}
                  disabled={props.isFetchingOtp}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      opacity: timeLeft === 0 ? 1 : 0.4,
                      color: 'white',
                      marginTop: 2,
                    }}>
                    {' '}
                    Kirim Ulang
                  </Text>
                </TouchableOpacity>

                <Text
                  style={{
                    marginLeft: 6,
                    fontFamily: 'NotoSans-ExtraBold',
                    color: 'white',
                    fontSize: 15,
                    opacity: 1,
                  }}>
                  00:{timeLeft < 10 ? '0' : ''}
                  {timeLeft}
                </Text>
              </View>
            </View>
          </View>
        ) : step == 2 ? (
          <View>
            <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
              <Text style={c.Styles.txtCreateMerchant}>Buka Toko Online</Text>
            </View>
            <ScrollView
              style={c.Styles.viewTokoStepContent}
              showsVerticalScrollIndicator={false}>
              <Text style={c.Styles.lineHeightTwenty}>
                Fitur ini dibuat untuk kamu yang ingin memiliki website usaha
                kamu sendiri. Bagikan dan tingkatkan penjualan kamu.
              </Text>
              <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
                <View style={c.Styles.txtInputWithIcon}>
                  <View style={c.Styles.marginRightTen}>
                    <c.Image
                      source={Images.toko}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      autoCapitalize="words"
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Nama Toko"
                      placeholderTextColor={c.Colors.gray}
                      onChangeText={text => {
                        let lowercasetext = text.toLowerCase();
                        setFormToko({
                          ...formToko,
                          name: text,
                          link: lowercasetext.split(' ').join('-'),
                        });
                        setTokoValidation({
                          ...tokoValidation,
                          isNameTokoEmpty: text ? false : true,
                        });
                      }}
                      value={name}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isNameTokoEmpty: name ? false : true,
                        })
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  Nama toko di website kamu
                </Text>
                {isNameTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Nama toko harus diisi
                  </Text>
                )}
              </View>
              <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={Images.at}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Link Toko"
                      placeholderTextColor={c.Colors.gray}
                      onChangeText={text => {
                        let lowercasetext = text.toLowerCase();
                        setFormToko({
                          ...formToko,
                          link: lowercasetext.split(' ').join('-'),
                        });
                        setTokoValidation({
                          ...tokoValidation,
                          isLinkTokoEmpty: text ? false : true,
                        });
                      }}
                      value={link}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isLinkTokoEmpty: link ? false : true,
                        })
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  {Config.TOKO_NAME}
                  <Text
                    style={{
                      fontWeight: '600',
                      fontSize: 14,
                      letterSpacing: 1,
                      textTransform: 'lowercase',
                    }}>
                    {!link ? 'link-toko' : link}
                  </Text>
                </Text>
                {isLinkTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Link toko harus diisi
                  </Text>
                )}
              </View>
            </ScrollView>
          </View>
        ) : step == 3 ? (
          <View>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 14,
                letterSpacing: 1,
                marginTop: 20,
              }}>
              Alamat Toko
            </Text>
            {tokoAddressData === null && !isAddedAddress && (
              <TouchableOpacity
                onPress={() => setIsAddedAddress(true)}
                delayPressIn={0}
                style={{
                  marginTop: 25,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  width: '99%',
                  height: 65,
                  backgroundColor: '#ffffff',
                  marginLeft: 0,
                  marginRight: 0,
                  alignItems: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,

                  borderColor: c.Colors.gray,
                  borderWidth: 0.58,
                }}>
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    marginTop: 8,
                    width: 25,
                    height: 25,
                    borderWidth: 1,
                    borderColor: '#a9a9a9',
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    name="plus"
                    size={20}
                    color="#a9a9a9"
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    marginTop: 8,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.5,
                    color: '#7b7b7b',
                  }}>
                  Tambah Alamat
                </Text>
              </TouchableOpacity>
            )}
            {isAddedAddress && (
              <View
                style={{
                  width: '100%',
                  borderColor: c.Colors.gray,
                  borderWidth: 0.58,
                  marginTop: 17,
                  paddingLeft: 18,
                  paddingRight: 18,
                }}>
                {provinceValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 25,
                    }}>
                    Provinsi
                  </Text>
                )}

                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    if (province) {
                      setOpenPickerProvince(false);
                      setOpenPickerCity(false);
                      setOpenPickerDistrict(false);
                      setOpenPickerPostalCode(false);
                    } else {
                      setOpenPickerProvince(true);
                      setOpenPickerCity(false);
                      setOpenPickerDistrict(false);
                      setOpenPickerPostalCode(false);
                    }
                  }}
                  style={{
                    marginTop: provinceValue != null ? 8 : 50,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                      ? 'red'
                      : provinceValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      marginLeft: 2,
                    }}>
                    {provinceValue != null ? provinceValue.name : 'Provinsi'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!province ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isProvinceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Provinsi
                  </Text>
                )}

                {province && provinceData != null && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={provinceData.provincies}
                  />
                )}
                {cityValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 32,
                    }}>
                    Kota
                  </Text>
                )}
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    if (provinceValue === null) {
                      Toast.show('Pilih Provinsi Terlebih Dahulu !');
                    } else {
                      if (city) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(true);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      }
                    }
                  }}
                  style={{
                    marginTop: cityValue != null ? 8 : 32,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isCityEmpty
                      ? 'red'
                      : cityValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      marginLeft: 2,

                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                    }}>
                    {cityValue != null ? cityValue.name : 'Kota'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!city ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isCityEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Kota
                  </Text>
                )}
                {city && cityData != null && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={cityData.cities}
                  />
                )}
                {districtValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 32,
                    }}>
                    Kecamatan
                  </Text>
                )}
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    if (provinceValue === null) {
                      Toast.show('Pilih Provinsi Terlebih Dahulu !');
                    } else if (cityValue === null) {
                      Toast.show('Pilih Kota Terlebih Dahulu !');
                    } else {
                      if (district) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(true);
                        setOpenPickerPostalCode(false);
                      }
                    }
                  }}
                  style={{
                    marginTop: districtValue != null ? 8 : 32,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                      ? 'red'
                      : districtValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      marginLeft: 2,
                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                    }}>
                    {districtValue != null ? districtValue.name : 'Kecamatan'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!district ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isDistrictEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Kecamatan
                  </Text>
                )}
                {district && districtData != null && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={districtData.districts}
                  />
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                      ? 'red'
                      : postalCodeValue == null || postalCodeValue === ''
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    onChangeText={searchingPostalCode}
                    keyboardType={'number-pad'}
                    value={postalCodeValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Kode Pos'}
                  />
                  {postalCodeValue != null && postalCode && (
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => setOpenPickerPostalCode(false)}
                      style={{bottom: 9, position: 'absolute', right: 0}}>
                      <MaterialCommunityIcons
                        name="check"
                        size={25}
                        color="#1a69d5"
                      />
                    </TouchableOpacity>
                  )}
                </View>
                {addedAddressFormValidation.isPostalCodeEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi Kode Pos
                  </Text>
                )}
                {postalCode &&
                  provinceValue != null &&
                  cityValue != null &&
                  districtValue != null && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={
                        postalCodeFilteredData &&
                        postalCodeFilteredData.length > 0
                          ? postalCodeFilteredData
                          : postalCodeData.postalCode
                      }
                    />
                  )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isAddressEmpty
                      ? 'red'
                      : addressValue == null || addressValue == ''
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    multiline={true}
                    onChangeText={text => {
                      setAddressValue(text);
                    }}
                    value={addressValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Alamat Lengkap'}
                  />
                </View>
                {addedAddressFormValidation.isAddressEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi Alamat Lengkap
                  </Text>
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor:
                      detailAddressValue === null || detailAddressValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    onChangeText={text => setDetailAddressValue(text)}
                    value={detailAddressValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Detail Alamat (optional)'}
                  />
                </View>

                <View style={{height: 25}} />
              </View>
            )}
            {isUpdatedAddress && (
              <View
                style={{
                  width: '100%',

                  borderColor: c.Colors.gray,
                  borderWidth: 0.58,
                  marginTop: 17,
                  paddingLeft: 18,
                  paddingRight: 18,
                }}>
                <TouchableOpacity
                  onPress={() => setIsUpdatedAddress(false)}
                  delayPressIn={0}
                  style={{position: 'absolute', right: 8, top: 8}}>
                  <MaterialCommunityIcons
                    name="close"
                    size={22}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {provinceValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 25,
                    }}>
                    Provinsi
                  </Text>
                )}

                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    setOpenPickerProvince(true);
                  }}
                  style={{
                    marginTop: provinceValue != null ? 8 : 50,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                      ? 'red'
                      : provinceValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      marginLeft: 2,
                    }}>
                    {provinceValue != null ? provinceValue.name : 'Provinsi'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!province ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isProvinceEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Provinsi
                  </Text>
                )}

                {province && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={provinceData.provincies}
                  />
                )}
                {cityValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 32,
                    }}>
                    Kota
                  </Text>
                )}
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    if (provinceValue === null) {
                      Toast.show('Pilih Provinsi Terlebih Dahulu !');
                    } else {
                      setOpenPickerCity(true);
                    }
                  }}
                  style={{
                    marginTop: cityValue != null ? 8 : 32,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isCityEmpty
                      ? 'red'
                      : cityValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      marginLeft: 2,

                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                    }}>
                    {cityValue != null ? cityValue.name : 'Kota'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!city ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isCityEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Kota
                  </Text>
                )}
                {city && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={cityData.cities}
                  />
                )}
                {districtValue != null && (
                  <Text
                    style={{
                      marginLeft: 2,

                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 32,
                    }}>
                    Kecamatan
                  </Text>
                )}
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    if (provinceValue === null) {
                      Toast.show('Pilih Provinsi Terlebih Dahulu !');
                    } else if (cityValue === null) {
                      Toast.show('Pilih Kota Terlebih Dahulu !');
                    } else {
                      setOpenPickerDistrict(true);
                    }
                  }}
                  style={{
                    marginTop: districtValue != null ? 8 : 32,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                      ? 'red'
                      : districtValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      marginLeft: 2,
                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                    }}>
                    {districtValue != null ? districtValue.name : 'Kecamatan'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!district ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedAddressFormValidation.isDistrictEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Kecamatan
                  </Text>
                )}
                {district && (
                  <FlatList
                    nestedScrollEnabled
                    style={{height: 300}}
                    renderItem={_addressRenderItem}
                    data={districtData.districts}
                  />
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                      ? 'red'
                      : postalCodeValue == null || postalCodeValue === ''
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    onChangeText={searchingPostalCode}
                    keyboardType={'number-pad'}
                    value={postalCodeValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Kode Pos'}
                  />
                  {postalCodeValue != null && postalCode && (
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => setOpenPickerPostalCode(false)}
                      style={{bottom: 9, position: 'absolute', right: 0}}>
                      <MaterialCommunityIcons
                        name="check"
                        size={25}
                        color="#1a69d5"
                      />
                    </TouchableOpacity>
                  )}
                </View>
                {addedAddressFormValidation.isPostalCodeEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi Kode Pos
                  </Text>
                )}
                {postalCode &&
                  provinceValue != null &&
                  cityValue != null &&
                  districtValue != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={
                        postalCodeFilteredData &&
                        postalCodeFilteredData.length > 0
                          ? postalCodeFilteredData
                          : postalCodeData.postalCode
                      }
                    />
                  )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedAddressFormValidation.isAddressEmpty
                      ? 'red'
                      : addressValue == null || addressValue == ''
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    multiline={true}
                    onChangeText={text => {
                      setAddressValue(text);
                    }}
                    value={addressValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Alamat Lengkap'}
                  />
                </View>
                {addedAddressFormValidation.isAddressEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi Alamat Lengkap
                  </Text>
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor:
                      detailAddressValue === null || detailAddressValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    onChangeText={text => setDetailAddressValue(text)}
                    value={detailAddressValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Detail Alamat (optional)'}
                  />
                </View>

                <View style={{height: 25}} />
              </View>
            )}
          </View>
        ) : null;
      }
      default:
        return null;
        break;
    }
  };

  const _onNavigationStateChange = webViewState => {
    setWebviewConfig({
      ...webViewState.nativeEvent,
      urlProduk: webViewState.nativeEvent.url.includes('products'),
    });
    // setWebviewConfig([...webViewConfig,webViewState.nativeEvent.url.replace(/[^0-9]/g, ''));
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, [webviewConfig]);

  useEffect(() => {
    if (webviewConfig.urlProduk) {
      // setWebviewConfig({...webviewConfig, urlProduk:true})
      dispatch(MarketPlaceActions.getProductByUrlRequest(webviewConfig.url));
    } else {
      // setWebviewConfig({...webviewConfig, urlProduk:false})
    }
  }, [webviewConfig]);
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    dispatch(TokoActions.getProductTokoRequest(props.tokoId));
    refRBSheet.current.close();
    setFilterProduct(initialFilter);
    setProductForm(initialProductForm);
    wait(500).then(() => setRefreshing(false));
  };
  // useEffect(() => {
  //   if (checkSuccessAddNewCategory) {
  //     console.log('kepanggil detail');
  //     setLoadingRBSheet(true);
  //     const data = {
  //       id: props.tokoId,
  //       item: {
  //         supplier_id: getParams.id,
  //         source_url: webviewConfig.url,

  //         name: productForm.name === null ? productData.name : productForm.name,
  //         description:
  //           productForm.desc === null
  //             ? productData.description_combined
  //             : productForm.desc,

  //         price:
  //           productForm.price === null
  //             ? typeof productData.price === 'string'
  //               ? Number(productData.price.replace(/[Rp.]/g, ''))
  //               : productData.price
  //             : typeof productForm.price === 'string'
  //             ? Number(productForm.price.replace(/[Rp.]/g, ''))
  //             : productForm.price,
  //         category_id:
  //           AddNewCategoryId != null
  //             ? AddNewCategoryId.productCategory != undefined
  //               ? AddNewCategoryId.productCategory.id
  //               : productForm.categorySelected
  //             : productForm.categorySelected,
  //         images: productData.image
  //           ? productData.image.length > 5
  //             ? productData.image.slice(0, 5)
  //             : productData.image
  //           : null,
  //       },
  //       func: {
  //         reloadData: () => {
  //           setProductForm(initialProductForm);
  //           setLoadingRBSheet(false);
  //           refRBSheet.current.close();
  //         },
  //       },
  //     };
  //     dispatch(TokoActions.postProductTokoRequest(data));
  //   }
  // }, [checkSuccessAddNewCategory]);
  const openRBSheet = actionType => {
    setLoadingRBSheet(false);
    if (webviewConfig.urlProduk) {
      dispatch(MarketPlaceActions.getProductByUrlRequest(webviewConfig.url));

      setActionType(actionType);
      refRBSheet.current.open();
    }
  };
  const injectScript = `
  (function() {
    function wrap(fn) {
      return function wrapper() {
        var res = fn.apply(this, arguments);
        window.ReactNativeWebView.postMessage('navigationStateChange');
        return res;
      }
    }

    history.pushState = wrap(history.pushState);
    history.replaceState = wrap(history.replaceState);
    window.addEventListener('popstate', function() {
      window.ReactNativeWebView.postMessage('navigationStateChange');
    });
  })();

  true;
`;
  const ActivityIndicatorElement = () => {
    return (
      <View
        style={{
          flex: 1,
          position: 'absolute',
          marginLeft: 'auto',
          marginRight: 'auto',
          marginTop: 'auto',
          marginBottom: 'auto',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          justifyContent: 'center',
        }}>
        <ActivityIndicator color={Colors.mainBlue} size="large" />
      </View>
    );
  };
  const tokoAvail = () => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: Colors.mainBlue,
        }}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          <TouchableOpacity
            style={{flexDirection: 'row'}}
            onPress={handleGoBack}>
            <Image
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 50,
                marginRight: 10,
              }}
              source={Images.arrowBack}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: 'white',
              marginLeft: 7,
              alignSelf: 'center',
              fontFamily: 'NotoSans-Bold',
              fontSize: 14,
              letterSpacing: 2.57,
              width: '50%',
            }}
            numberOfLines={1}
            ellipsizeMode="tail">
            {getParams.name}
          </Text>

          {/* <TouchableOpacity
          onPress={() => openRBSheet('add')}
          disabled={
            !webviewConfig.urlProduct && !productScrapLoading ? false : true
          }
          style={{
            flexDirection: 'row',
            borderRadius: 7,
            justifyContent: 'center',
            alignItems: 'center',
            right: 15,
            position: 'absolute',
            paddingHorizontal: 5,
            paddingVertical: 3,
            backgroundColor: webviewConfig.urlProduk ? '#fff' : '#0c4faa',
            // backgroundColor:'#0c4faa'
          }}>
          <Ionicons name={'add'} color="#1a69d5" size={23} />
          <c.Image
            source={Images.magazine}
            style={{width: 15, height: 19, marginRight: 5, marginLeft: 3}}
            resizeMode="contain"
          />
        </TouchableOpacity> */}
        </View>
        <KeyboardAvoidingView
          behavior={'padding'}
          enabled
          contentContainerStyle={{flex: 1}}
          keyboardVerticalOffset={20}
          style={{flexGrow: 1}}>
          <WebView
            automaticallyAdjustContentInsets={false}
            cacheEnabled
            userAgent="Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Mobile Safari/537.36"
       
            injectedJavaScript={injectScript}
            onMessage={_onNavigationStateChange}
            source={{uri: getParams.link}}
            style={{width: visible ? 0 : '100%', height: visible ? 0 : '90%'}}
            ref={webview}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            onLoadStart={() => setVisible(true)}
            onLoad={() => setVisible(false)}
          />
        </KeyboardAvoidingView>

        {visible || productScrapLoading ? <ActivityIndicatorElement /> : null}
        {webviewConfig.urlProduk ? (
          <View
            style={{
              height: '10%',
              backgroundColor: '#fff',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              width: '100%',
            }}>
            <View
              style={{
                width: '96.3%',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  navigation.navigate('DropshipAddScreen', {
                    params: getParams,
                  });
                }}
                // onPress={() => {
                //   if (checkSuccessScrap === false) {
                //     openRBSheet('addNew');
                //   } else {
                //     openRBSheet('add');
                //   }
                // }}
                style={{
                  alignSelf: 'center',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: c.Colors.mainBlue,
                  borderColor: c.Colors.mainBlue,
                  borderWidth: 1,
                  borderRadius: 10,
                  width: '100%',
                }}>
                <c.Image
                  source={Images.bookWhite}
                  style={{
                    width: 15,
                    height: 19,
                    marginRight: 5,
                    marginLeft: 20,
                  }}
                  resizeMode="contain"
                />
                <Text style={{color: 'white'}}>Tambahkan ke Katalog</Text>
                <Text style={{color: c.Colors.mainBlue}}>cc</Text>
              </Button>
            </View>
          </View>
        ) : null}
      </View>
    );
  };
  const tokoUnavail = () => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: Colors.mainBlue,
        }}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          <TouchableOpacity
            style={{flexDirection: 'row'}}
            onPress={handleGoBack}>
            <Image
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 50,
                marginRight: 10,
              }}
              source={Images.arrowBack}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: 'white',
              marginLeft: 7,
              alignSelf: 'center',
              fontFamily: 'NotoSans-Bold',
              fontSize: 14,
              letterSpacing: 2.57,
              width: '50%',
            }}
            numberOfLines={1}
            ellipsizeMode="tail">
            {getParams.name}
          </Text>

          {/* <TouchableOpacity
                        onPress={() => openRBSheet('add')}
                        disabled={
                          !webviewConfig.urlProduct && !productScrapLoading ? false : true
                        }
                        style={{
                          flexDirection: 'row',
                          borderRadius: 7,
                          justifyContent: 'center',
                          alignItems: 'center',
                          right: 15,
                          position: 'absolute',
                          paddingHorizontal: 5,
                          paddingVertical: 3,
                          backgroundColor: webviewConfig.urlProduk ? '#fff' : '#0c4faa',
                          // backgroundColor:'#0c4faa'
                        }}>
                        <Ionicons name={'add'} color="#1a69d5" size={23} />
                        <c.Image
                          source={Images.magazine}
                          style={{width: 15, height: 19, marginRight: 5, marginLeft: 3}}
                          resizeMode="contain"
                        />
                      </TouchableOpacity> */}
        </View>
        <KeyboardAvoidingView
          behavior={'padding'}
          enabled
          contentContainerStyle={{flex: 1}}
          keyboardVerticalOffset={20}
          style={{flexGrow: 1}}>
          <WebView
            automaticallyAdjustContentInsets={false}
            cacheEnabled
            userAgent="Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Mobile Safari/537.36"
       
            injectedJavaScript={injectScript}
            onMessage={_onNavigationStateChange}
            source={{uri: getParams.link}}
            style={{width: visible ? 0 : '100%', height: visible ? 0 : '90%'}}
            ref={webview}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            onLoadStart={() => setVisible(true)}
            onLoad={() => setVisible(false)}
          />
        </KeyboardAvoidingView>
        {visible || productScrapLoading ? <ActivityIndicatorElement /> : null}
        {webviewConfig.urlProduk ? (
          <View
            style={{
              height: '10%',
              backgroundColor: '#fff',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              width: '100%',
            }}>
            <View
              style={{
                width: '96.3%',
                alignItems: 'center',
              }}>
              <Button
                onPress={() => {
                  if (!isVerified) {
                    if (userData.data.is_phone_verified === null) {
                      let dataStep = {
                        modalVisible: true,
                        step: 1,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    } else if (tokoId === null) {
                      let dataStep = {
                        modalVisible: true,
                        step: 2,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    } else if (!tokoAddress) {
                      let dataStep = {
                        modalVisible: true,
                        step: 3,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    }
                  }
                }}
                style={{
                  alignSelf: 'center',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: c.Colors.mainBlue,
                  borderColor: c.Colors.mainBlue,
                  borderWidth: 1,
                  borderRadius: 10,
                  width: '100%',
                }}>
                <c.Image
                  source={Images.bookWhite}
                  style={{
                    width: 15,
                    height: 19,
                    marginRight: 5,
                    marginLeft: 20,
                  }}
                  resizeMode="contain"
                />
                <Text style={{color: 'white'}}>Tambahkan ke Katalog</Text>
                <Text style={{color: c.Colors.mainBlue}}>cc</Text>
              </Button>
            </View>
          </View>
        ) : null}
      </View>
    );
  };
  return isTokoAvailable && isVerified ? tokoAvail() : tokoUnavail();
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getOtpWa: state.auth.getOtpPostData,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,
    checkSuccessAddNewCategory: state.toko.postCategoryTokoData.success,
    AddNewCategoryId: state.toko.postCategoryTokoData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    isFetchingLogout: state.auth.deleteAuthToken.fetching,
    getToko: state.toko.tokoData.data,
    detailTokoData: state.toko.detailTokoData.data,
    categoryData: state.toko.categoryTokoData.data,
    productData: state.marketplace.productByUrlData.data,
    productScrapLoading: state.marketplace.productByUrlData.fetching,
    checkSuccessScrap: state.marketplace.productByUrlData.success,
    businessType: state.toko.getBusinessTypeData.data,
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoAddress:
      state.toko.tokoAddressData.data != null
        ? state.toko.tokoAddressData.data.listAddress
          ? state.toko.tokoAddressData.data.listAddress.length != 0
            ? true
            : false
          : false
        : false,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DropshipDetailScreen);
