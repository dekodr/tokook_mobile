import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
} from 'react-native';
import CameraActions from './../../../../../Redux/CameraRedux';
import moment from 'moment';
import OrderIntro from '../../../Order/Intro';
import {Button, Spinner, Card, Content} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from '../../../../../Redux/TokoRedux';

import {Images, Colors} from '../../../../../Themes';
import * as func from '../../../../../Helpers/Utils';

import * as c from '../../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import {ThemeColors} from 'react-navigation';
const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
const initialValidation = {
  isKycNameEmpty: false,
  isInputKycError: false,
  isKycNumberEmpty: false,
  isKycPhotoEmpty: false,
  isKycSelfiePhotoEmpty: false,
};
const FinanceKYCScreen = props => {
  const [addedKycFormValidation, setAddedKycFormValidation] = useState(
    initialValidation,
  );
  const [kycNameValue, setKycNameValue] = useState(null);
  const [kycNumberValue, setKycNumberValue] = useState(0);
  const [kycPhotoValue, setKycPhotoValue] = useState(null);
  const [kycPSelfiePhotoValue, setKycSelfiePhotoValue] = useState(null);
  const {
    navigation,
    dispatch,
    isFetchingDataVerif,
    previewKtp,
    previewKtpSelfie,
  } = props;
  console.log('props', props);
  const scrollParentRef = useRef(null);
  const handleSubmitKyc = () => {
    if (kycNameValue === null || kycNameValue === '') {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycNameEmpty: true,
      });
      scrollParentRef.current.scrollTo(0);
    } else if (kycNumberValue === 0) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycNumberEmpty: true,
      });
      scrollParentRef.current.scrollTo(0);
    } else if (kycNumberValue.length < 16) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isInputKycError: true,
      });
      scrollParentRef.current.scrollTo(0);
    } else if (previewKtp === null) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycPhotoEmpty: true,
      });
    } else if (previewKtpSelfie === null) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycSelfiePhotoEmpty: true,
      });
    } else {
      // form.append('identity_image', {
      //   uri: previewKtp,
      //   name: previewKtp + kycNameValue,
      //   type: 'JPEG',
      // });
      // form.append('selfie_image', {
      //   uri: previewKtpSelfie,
      //   name: previewKtpSelfie + kycNameValue,
      //   type: 'JPEG',
      // });
      // form.append('fullname', kycNameValue);
      // form.append('identity_number', kycNumberValue);
      const form = new FormData();

      form.append('identity_image', {
        uri: previewKtp,
        name: previewKtp + kycNameValue,
        type: 'image/jpeg',
      });
      form.append('selfie_image', {
        uri: previewKtpSelfie,
        name: previewKtpSelfie + kycNameValue,
        type: 'image/jpeg',
      });
      form.append('fullname', kycNameValue);
      form.append('identity_number', kycNumberValue);

      dispatch(TokoActions.postKycTokoRequest(form));
    }
  };
  useEffect(() => {
    const backAction = () => {
      console.log('navigations', navigation.state.params);
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  console.log('navigations', navigation.state.params);
  const handleWhatsapp = () => {
    Linking.openURL('whatsapp://send?phone=+6287878983919').catch(() =>
      Linking.openURL(
        'https://apps.apple.com/id/app/whatsapp-messenger/id310633997',
      ),
    );
  };
  useEffect(() => {
    if (previewKtp != null) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycPhotoEmpty: false,
      });
    }
    if (previewKtpSelfie != null) {
      setAddedKycFormValidation({
        ...addedKycFormValidation,
        isKycSelfiePhotoEmpty: false,
      });
    }
  }, [previewKtp, previewKtpSelfie]);
  const B = props => (
    <Text
      onPress={props.onPress}
      style={{
        fontFamily: 'NotoSans-Bold',
        color: '#4a4a4a',
        fontSize: 12,
        letterSpacing: 1.3,
      }}>
      {props.children}
    </Text>
  );
  return (
    <View
      style={{
        width: '100%',

        backgroundColor: Colors.white,
      }}>
      <View
        style={{
          height: 50,
          backgroundColor: Colors.mainBlue,
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{width: 25, height: 17, marginLeft: 25}}
            source={Images.arrowBack}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: 'white',
            marginLeft: 25,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          KYC
        </Text>
      </View>
      <ScrollView
        ref={scrollParentRef}
        style={{
          width: '100%',
          height: '100%',
        }}>
        <View
          style={{
            width: '88%',
            alignSelf: 'center',
            marginTop: 25,
            flexDirection: 'row',

            borderBottomWidth: 0.5,
            borderBottomColor: addedKycFormValidation.isKycNameEmpty
              ? 'red'
              : kycNameValue == null || kycNameValue == ''
              ? '#c9c6c6'
              : '#1a69d5',
          }}>
          <TextInput
            style={{
              bottom: -5,
              fontFamily: 'NotoSans',
              fontSize: 12,
              letterSpacing: 1,
              color: '#7b7b7b',
              width: '100%',
            }}
            onChangeText={text => {
              setKycNameValue(text);
              setAddedKycFormValidation({
                ...addedKycFormValidation,
                isKycNameEmpty: false,
              });
            }}
            value={kycNameValue}
            placeholderTextColor={'#7b7b7b'}
            placeholder={'Nama Lengkap (sesuai KTP)'}
          />
        </View>
        {addedKycFormValidation.isKycNameEmpty && (
          <Text
            style={[
              c.Styles.txtInputDesc,
              {color: 'red', width: '88%', alignSelf: 'center'},
            ]}>
            Wajib mengisi Nama Lengkap
          </Text>
        )}
        <View
          style={{
            width: '88%',
            alignSelf: 'center',
            marginTop: 15,
            flexDirection: 'row',

            borderBottomWidth: 0.5,
            borderBottomColor:
              addedKycFormValidation.isKycNumberEmpty ||
              addedKycFormValidation.isInputKycError
                ? 'red'
                : kycNumberValue == null || kycNumberValue == ''
                ? '#c9c6c6'
                : '#1a69d5',
          }}>
          <TextInput
            style={{
              bottom: -5,
              fontFamily: 'NotoSans',
              fontSize: 12,
              letterSpacing: 1,
              color: '#7b7b7b',
              width: '100%',
            }}
            onChangeText={text => {
              setKycNumberValue(text.replace(/[^0-9]/g, ''));
              setAddedKycFormValidation({
                ...addedKycFormValidation,
                isKycNumberEmpty: false,
                isInputKycError: false,
              });
            }}
            maxLength={16}
            keyboardType={'decimal-pad'}
            value={kycNumberValue}
            placeholderTextColor={'#7b7b7b'}
            placeholder={'NIK'}
          />
        </View>
        {!addedKycFormValidation.isInputKycError && (
          <Text
            style={[
              c.Styles.txtInputDesc,
              {
                color:
                  kycNumberValue === 0 || kycNumberValue.length < 16
                    ? 'red'
                    : Colors.mainBlue,
                width: '94%',
                textAlign: 'right',
              },
            ]}>
            {kycNumberValue === 0 ? 0 : kycNumberValue.length} / {16}
          </Text>
        )}

        {addedKycFormValidation.isInputKycError ? (
          <Text
            style={[
              c.Styles.txtInputDesc,
              {color: 'red', width: '88%', alignSelf: 'center'},
            ]}>
            {kycNumberValue.length < 16 && kycNumberValue.length > 0
              ? 'NIK Harus 16 Karakter'
              : 'Wajib mengisi NIK Lengkap'}
          </Text>
        ) : null}

        <Text
          style={{
            fontFamily: 'NotoSans',
            fontSize: 12,
            color: '#4a4a4a',
            letterSpacing: 1.3,
            textAlign: 'center',
            paddingLeft: 30,
            paddingRight: 30,
            marginTop: 30,
          }}>
          Kirim
          <B> foto KTP </B>
          kamu (pastikan semua data KTP yang ada di foto dapat terbaca)
        </Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('CameraScreen', {params: {type: 1}})
          }
          delayPressIn={0}
          style={{
            alignSelf: 'center',
            width: '78%',
            height: 190,
            backgroundColor: '#d8d8d8',
            marginTop: 17,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
          }}>
          <Image
            style={{
              width: '90%',
              height: 150,
              resizeMode: previewKtp ? null : 'contain',
            }}
            source={
              previewKtp ? {isStatic: true, uri: previewKtp} : Images.ktpIcon
            }
          />
        </TouchableOpacity>
        {addedKycFormValidation.isKycPhotoEmpty && (
          <Text
            style={[
              c.Styles.txtInputDesc,
              {
                color: 'red',
                width: '88%',
                alignSelf: 'center',
                marginLeft: 50,
              },
            ]}>
            Wajib menyertakan Foto KTP
          </Text>
        )}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('CameraScreen', {params: {type: 1}});
            dispatch(CameraActions.takePictureKtpReset());
            dispatch(CameraActions.takePictureKtpSelfieReset());
          }}
          style={{marginTop: 12, alignItems: 'center'}}
          delayPressIn={0}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1,
              color: Colors.mainBlue,
            }}>
            Ambil ulang foto
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'NotoSans',
            fontSize: 12,
            color: '#4a4a4a',
            letterSpacing: 1.3,
            textAlign: 'center',
            paddingLeft: 30,
            paddingRight: 30,
            marginTop: 30,
          }}>
          Kirim <B>foto Selfie</B> kamu dan KTP kamu
        </Text>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('CameraScreen', {params: {type: 2}})
          }
          delayPressIn={0}
          style={{
            alignSelf: 'center',
            width: '78%',
            height: 190,
            backgroundColor: '#d8d8d8',
            marginTop: 17,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 12,
          }}>
          <Image
            style={{
              width: '90%',
              height: 150,
              resizeMode: previewKtpSelfie ? null : 'contain',
            }}
            source={
              previewKtpSelfie
                ? {isStatic: true, uri: previewKtpSelfie}
                : Images.selfieWithKtpIcon
            }
          />
        </TouchableOpacity>
        {addedKycFormValidation.isKycSelfiePhotoEmpty && (
          <Text
            style={[
              c.Styles.txtInputDesc,
              {
                color: 'red',
                width: '88%',
                alignSelf: 'center',
                marginLeft: 50,
              },
            ]}>
            Wajib menyertakan Foto Selfie dengan KTP
          </Text>
        )}
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('CameraScreen', {params: {type: 2}});
            dispatch(CameraActions.takePictureKtpSelfieReset());
          }}
          style={{marginTop: 12, alignItems: 'center'}}
          delayPressIn={0}>
          <Text
            style={{
              textAlign: 'center',
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1,
              color: Colors.mainBlue,
            }}>
            Ambil ulang foto
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={isFetchingDataVerif ? true : false}
          onPress={() => handleSubmitKyc()}
          delayPressIn={0}
          style={{
            marginTop: 50,
            width: '88%',
            alignSelf: 'center',
            alignItems: 'center',
            backgroundColor: Colors.mainBlue,
            borderRadius: 30,
          }}>
          {isFetchingDataVerif ? (
            <ActivityIndicator
              style={{
                padding: 13,
              }}
              color={'white'}
              size={'large'}
            />
          ) : (
            <Text
              style={{
                padding: 20,
                color: Colors.white,
                fontSize: 14,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1.2,
              }}>
              Verifikasi data
            </Text>
          )}
        </TouchableOpacity>
        <View
          style={{
            marginTop: 4,
            padding: 12,
            width: '100%',
            marginLeft: 4,
          }}>
          <View style={{flexDirection: 'row'}}>
            {/* <Image
                style={{height: 30, width: 30, resizeMode: 'contain'}}
                source={images.waIcon}
              /> */}
            <Text
              style={{
                color: '#3C5060',

                letterSpacing: 1,
                textAlign: 'center',
                fontFamily: 'NotoSans',
                fontSize: 15,
                marginTop: 3,
                marginLeft: 5,
              }}>
              Butuh bantuan ?
            </Text>
            <TouchableOpacity
              onPress={() => handleWhatsapp()}
              style={{
                marginTop: 3,
                marginLeft: 8,
                borderBottomWidth: 1,
                borderBottomColor: Colors.mainBlue,
              }}>
              <Text
                style={{
                  color: Colors.mainBlue,

                  letterSpacing: 1.24,
                  textAlign: 'center',
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 15,
                }}>
                klik disini
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{height: 50}} />
      </ScrollView>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    previewKtp: state.camera.ktp.ktpData,
    previewKtpSelfie: state.camera.ktp.ktpSelfieData,

    tokoOrderPrimary: state.toko.tokoOrderData,
    tokoBankData: state.toko.tokoBankData.data,

    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    isErrorTokoBankAlreadyRegistered: state.toko.postTokoBankData.error,
    bankData: state.toko.bankData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    getToko: state.toko.tokoData.data,
    isFetchingDataVerif: state.toko.postKycTokoData.fetching,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FinanceKYCScreen);
