import {StyleSheet} from 'react-native';
import {Images, Colors} from '../../../../../Themes/';
export default StyleSheet.create({
  mainContainer: {
    backgroundColor: Colors.mainBlue,
    height: '100%',
    width: '100%',
    paddingTop: 25,
    paddingRight: 25,
    paddingLeft: 25,
    flex: 1,
  },
  cellStyle: {
    borderBottomWidth: 3,
    borderColor: Colors.whiteLight,
    marginRight: 12,
    marginLeft: 12,
  },
  cellStyleFocused: {
    borderColor: Colors.whiteLight,
  },
  txtCellStyle: {
    fontSize: 24,
    color: Colors.mainBlue,
  },
  textStyleFocused: {
    fontSize: 24,
    color: 'white',
  },
  itemCenter: {
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
