import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ToastAndroid,
  RefreshControl,
} from 'react-native';
import AuthActions from '../../../../../Redux/AuthenticationRedux';

import styles from './styles';
import {TextInputMask} from 'react-native-masked-text';

import moment from 'moment';
import OrderIntro from '../../../Order/Intro';
import {Button, Spinner, Card, Content} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from '../../../../../Redux/TokoRedux';
import GlobalActions from '../../../../../Redux/GlobalRedux';
import {Images, Colors} from '../../../../../Themes/';
import * as func from '../../../../../Helpers/Utils';

import * as c from '../../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';

const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
const initialValidation = {
  isKYCPendingNReject: false,
  isTokoBankEmpty: false,
  isSalesEmpty: false,
  isSalesOutOfSaving: false,
  isCommissionEmpty: false,
  iscommissionOutOfSaving: false,
  isOutOfWithdrawMinimum: false,
};
const bantuanURL = 'https://web.tokook.id/bantuan/';
const phoneNumber = '+6287878983919';
const FinanceBankTransferScreen = props => {
  const {
    navigation,
    tokoBankData,
    dispatch,
    supplierData,
    tokoOrderPrimary,
    isErrorTokoBankAlreadyRegistered,
    tokoId,
    bankData,
    getKycStatusData,
    getFinancialTokoData,
    userData,
    getTokenAuthEmail,
    authLoginData,
    isFetchingWithdrawDisburst,
    getErrMsg,
    isSuccessOtpwithdrawDisbursment,
    getErrMsgOtpDisburstment,
    getErrMsgPostDisburstment,
    tokoData,
    tokoAddressData,
  } = props;
  const refRBSheet = useRef(null);
  const scrollViewRefParent = useRef(null);
  const [
    addedBankTransferFormValidation,
    setAddedBankTransferFormValidation,
  ] = useState(initialValidation);
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addedBankFormValidation, setAddedBankFormValidation] = useState(
    initialBankValidation,
  );
  const [isAddedAllSales, setIsAddedAllSales] = useState(false);
  const [isAddedAllCommission, setIsAddedAllCommission] = useState(false);

  const [contentSelected, setContentSelected] = useState(null);
  const [isAddedAddress, setIsAddedAddress] = useState(false);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);

  const [errMsgWithdraw, setErrMsgWithdraw] = useState(null);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [salesValue, setSalesValue] = useState(0);
  const [commissionValue, setCommissionValue] = useState(0);
  const [code, setCode] = useState('');
  const [errMsg, setErrMsg] = useState(null);
  const [timeLeft, setTimeLeft] = useState(30);
  const pinverifRef = useRef(null);
  const salesInputRef = useRef(null);
  const commissionInputRef = useRef(null);
  const isTokoBankExists =
    tokoBankData != null
      ? tokoBankData.listRekening.length != 0
        ? true
        : false
      : false;
  const isVerified =
    userData.is_phone_verified !== null && tokoData && tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  console.log('ceds', isTokoBankExists);
  const getKycStatus = getKycStatusData != null ? getKycStatusData.kyc : null;
  const getDefaultBank =
    tokoBankData !== null
      ? tokoBankData.listRekening.filter(x => x.default === true)
      : [];
  const getNonDefaultBank =
    tokoBankData !== null
      ? tokoBankData.listRekening.filter(x => x.default === false)
      : [];
  useEffect(() => {
    if (props.errSystemReOtp) {
      setErrMsg(props.errSystemReOtp.message);
      setTimeout(() => {
        setErrMsg(null);
        // props.dispatch(AuthActions.loginEmailPostReset());
      }, 3000);
    }
  }, [props.errSystemReOtp]);
  useEffect(() => {
    if (getErrMsg != null) {
      console.log('fff', getErrMsg.message);
      setErrMsgWithdraw(getErrMsg.message);
    }
  }, [getErrMsg.message]);
  useEffect(() => {
    if (getErrMsgOtpDisburstment != null) {
      setErrMsgWithdraw(getErrMsgOtpDisburstment.message);
    }
  }, [getErrMsgOtpDisburstment]);
  useEffect(() => {
    if (getErrMsgPostDisburstment != null) {
      setErrMsgWithdraw(getErrMsgPostDisburstment.message);
    }
  }, [getErrMsgPostDisburstment]);
  useEffect(() => {
    if (isSuccessOtpwithdrawDisbursment) {
      setTimeLeft(30);
    }
  }, [isSuccessOtpwithdrawDisbursment]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
      // dispatch(AuthActions.otpWithdrawDisbursmentPostReset());
    }

    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);

  // useEffect(() => {
  //   if (props.errSystem.message) {
  //     setErrMsg(props.errSystem.message);
  //     setTimeout(() => {
  //       setErrMsg(null);
  //       props.dispatch(AuthActions.otpPostReset());
  //     }, 3000);
  //   }
  // }, [props.errSystem.message]);
  const handleResendOtp = () => {
    if (timeLeft === 0) {
      const data = {
        email: props.userLogin.data.email
          ? props.userLogin.data.email
          : props.userLogin.data.user.email,
        source: 'withdraw_request',
        type: 'email',
      };
      props.dispatch(AuthActions.otpWithdrawDisbursmentPostRequest(data));
    } else null;
  };
  const resetAllState = () => {
    // setCommissionValue(0);
    // setSalesValue(0);
    setCode('');
  };
  const withdrawDataDetail = [
    {
      id: 1,
      content: 'Penjualan',
      value: salesValue === 0 ? 0 : salesValue,
    },
    {
      id: 2,

      content: 'Komisi',
      value: commissionValue === 0 ? 0 : commissionValue,
    },

    {
      id: 3,

      content: 'Biaya Admin',
      value:
        isTokoBankExists === true
          ? getDefaultBank[0].bank_details.admin_fee
          : 0,
    },
  ];
  const B = props => (
    <Text
      onPress={props.onPress}
      style={{
        fontFamily: 'NotoSans-Bold',
        color: props.color,
        fontSize: 12,
        letterSpacing: 1.3,
      }}>
      {props.children}
    </Text>
  );

  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleOnPressPickerItem(5, {
            item,
            index,
          });
        }}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  useEffect(() => {
    const backAction = () => {
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setAddedBankTransferFormValidation({
            ...addedBankTransferFormValidation,
            isTokoBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const _OrderStatusHandler = ({status_pesanan}) => {
    switch (status_pesanan) {
      case 1:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c2dcff',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#1a69d5',
                letterSpacing: 0.7,
              }}>
              Pesanan Baru
            </Text>
          </View>
        );
        break;

      case 2:
        return (
          <View
            style={{
              width: '90%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Pesanan Diterima
            </Text>
          </View>
        );
        break;
      case 3:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 4:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Dikirim
            </Text>
          </View>
        );
        break;
      case 5:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Selesai
            </Text>
          </View>
        );
        break;
      case 6:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#d0021b',
                letterSpacing: 0.7,
              }}>
              Pesanan Ditolak
            </Text>
          </View>
        );
        break;
      case 7:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#d0021b',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Ditolak
            </Text>
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const handleSubmitTransfer = () => {
    const salesValueData =
      getFinancialTokoData.data != null
        ? getFinancialTokoData.data.earnings.sales
        : 0;
    const commissionValueData =
      getFinancialTokoData.data != null
        ? getFinancialTokoData.data.earnings.commission
        : 0;
    console.log('yuhuu sales', Number(salesValue));
    console.log('yuhuu sales data', salesValueData);
    console.log('commission Value', commissionValue);
    if (salesValue > salesValueData) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isSalesOutOfSaving: true,
      });
      scrollViewRefParent.current.scrollTo(0);
    } else if (commissionValue > commissionValueData) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isCommissionOutOfSaving: true,
      });
      scrollViewRefParent.current.scrollTo(0);
    } else if (
      Number(salesValue) + Number(commissionValue) <
      // -
      // Number(withdrawDataDetail[2].value)
      getFinancialTokoData.data.earnings.minimumWithdraw
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isOutOfWithdrawMinimum: true,
      });
    } else if (!isTokoBankExists) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isTokoBankEmpty: true,
      });
    } else if (
      getKycStatus === null ||
      getKycStatus.status === 'pending' ||
      getKycStatus.status === 'rejected'
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isKYCPendingNReject: true,
      });
      scrollViewRefParent.current.scrollToEnd();
    } else if (
      !isNaN(Number(commissionValue)) ||
      !isNaN(Number(salesValue)) ||
      Number(salesValue) != 0 ||
      Number(commissionValue) != 0
    ) {
      handleGetOtp(1);
      _openRBSheet('whatsapp-otp');
    } else if (
      isNaN(Number(commissionValue)) ||
      isNaN(Number(salesValue)) ||
      Number(commissionValue) === 0 ||
      Number(salesValue) === 0
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isSalesEmpty: true,
      });
      scrollViewRefParent.current.scrollTo(0);
    } else if (!isTokoBankExists) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isTokoBankEmpty: true,
      });
    } else if (
      getKycStatus === null ||
      getKycStatus.status === 'pending' ||
      getKycStatus.status === 'rejected'
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isKYCPendingNReject: true,
      });
      scrollViewRefParent.current.scrollToEnd();
    } else if (
      !isNaN(Number(commissionValue)) ||
      !isNaN(Number(salesValue)) ||
      Number(salesValue) != 0 ||
      Number(commissionValue) != 0
    ) {
      handleGetOtp(1);
      _openRBSheet('whatsapp-otp');
    } else if (
      commissionValue === 0 ||
      (commissionValue === '' && Number(salesValue) === 0 && salesValue === '')
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isCommissionEmpty: true,
      });
      scrollViewRefParent.current.scrollTo(0);
    } else if (!isTokoBankExists) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isTokoBankEmpty: true,
      });
    } else if (
      getKycStatus === null ||
      getKycStatus.status === 'pending' ||
      getKycStatus.status === 'rejected'
    ) {
      setAddedBankTransferFormValidation({
        ...addedBankTransferFormValidation,
        isKYCPendingNReject: true,
      });
      scrollViewRefParent.current.scrollToEnd();
    } else {
      handleGetOtp(1);
      _openRBSheet('whatsapp-otp');
    }
  };
  const setValueSubmitBank = (purpose, value) => {
    switch (purpose) {
      case 1: {
        setIsAddedBank(value);

        break;
      }
      case 2: {
        setBankIndex(value);
        break;
      }
      default:
        null;
    }
  };
  const handleAddBankSubmit = value => {
    if (value === 3) {
      const postData = {
        tokoId: props.tokoId,
        rekId: rekId,
        setValueBank: setValueSubmitBank,
        // func: {updateBank: setBankIndex()},
        purpose: 3,
        detail: {
          bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
          holder_name: bankOwnerNameValue,
          rekening_number: bankAccountNumValue,
        },
      };

      props.dispatch(TokoActions.postTokoBankRequest(postData));
    } else {
      if (bankValue === null || bankValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankEmpty: true,
        });
      } else if (bankAccountNumValue === null || bankAccountNumValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankAccountNumEmpty: true,
        });
      } else if (bankOwnerNameValue === null || bankOwnerNameValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankOwnerEmpty: true,
        });
      } else {
        const postData = {
          tokoId: props.tokoId,
          rekId: rekId,
          setValueBank: setValueSubmitBank,
          // func: {updateBank: setBankIndex()},
          purpose: isAddedBank || !isTokoBankExists ? 1 : 2,
          detail: {
            bank_id:
              isAddedBank || !isTokoBankExists
                ? bankValue.id
                : bankNameValue.id,
            holder_name: bankOwnerNameValue,
            rekening_number: bankAccountNumValue,
          },
        };

        props.dispatch(TokoActions.postTokoBankRequest(postData));
      }
    }
  };

  const _renderBankItem = ({index, item}) => {
    if (bankIndex === item.id) {
      return (
        <View
          style={{
            width: '100%',

            borderColor: c.Colors.gray,
            borderWidth: 0.58,
            marginTop: 17,
            paddingLeft: 18,
            paddingRight: 18,
          }}>
          <TouchableOpacity
            onPress={() => {
              setAddedBankFormValidation({
                ...addedBankFormValidation,
                isBankAccountNumEmpty: false,
                isBankEmpty: false,
                isBankOwnerEmpty: false,
                isOutOfWithdrawMinimum: false,
              });
              props.dispatch(TokoActions.postTokoBankReset());
              setBankIndex(-1);
            }}
            delayPressIn={0}
            style={{position: 'absolute', right: 8, top: 8}}>
            <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1.2,
              marginTop: 25,
            }}>
            Nama Bank
          </Text>
          <TouchableOpacity
            onPress={() => {
              props.dispatch(TokoActions.postTokoBankReset());

              if (bank) {
                setOpenPickerBank(false);
              } else {
                setOpenPickerBank(true);
              }
            }}
            delayPressIn={0}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankEmpty
                ? 'red'
                : bankNameValue.name == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <Text
              style={{
                bottom: 3,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
              }}>
              {bankNameValue.name != null || bankNameValue.name != ''
                ? bankNameValue.name
                : 'Nama Bank'}
            </Text>
            <MaterialCommunityIcons
              style={{bottom: 5}}
              name={!bank ? 'menu-down' : 'menu-up'}
              size={25}
              color="#1a69d5"
            />
          </TouchableOpacity>
          {/* {addedBankFormValidation.isBankEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib memilih Nama Bank
            </Text>
          )} */}
          {bank && (
            <FlatList
              nestedScrollEnabled
              renderItem={_addressRenderItem}
              data={bankData.banks}
            />
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                ? 'red'
                : bankAccountNumValue == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              keyboardType={'number-pad'}
              onChangeText={text => {
                props.dispatch(TokoActions.postTokoBankReset());
                setAddedBankTransferFormValidation({
                  ...addedBankTransferFormValidation,
                  isTokoBankEmpty: false,
                  isKYCPendingNReject: false,
                  isOutOfWithdrawMinimum: false,
                });
                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankAccountNumEmpty: false,
                  isTokoBankEmpty: false,
                });
                setBankAccountNumValue(text);
              }}
              value={bankAccountNumValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nomor Rekening'}
            />
          </View>
          {addedBankFormValidation.isBankAccountNumEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nomor rekening
            </Text>
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                ? 'red'
                : bankOwnerNameValue == null || bankOwnerNameValue == ''
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              onChangeText={text => {
                props.dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankOwnerEmpty: false,
                });
                setAddedBankTransferFormValidation({
                  ...addedBankTransferFormValidation,
                  isTokoBankEmpty: false,
                  isKYCPendingNReject: false,
                  isOutOfWithdrawMinimum: false,
                });
                setBankOwnerNameValue(text);
              }}
              value={bankOwnerNameValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nama Pemilik'}
            />
          </View>
          {addedBankFormValidation.isBankOwnerEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nama pemilik rekening
            </Text>
          )}

          <View style={{height: 14}} />
          {isErrorTokoBankAlreadyRegistered && (
            <Text
              style={[
                c.Styles.txtInputDesc,
                {color: 'red', alignSelf: 'center', top: 15},
              ]}>
              {isErrorTokoBankAlreadyRegistered.message}
            </Text>
          )}
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <View style={{width: '23%'}}>
              <Button
                style={{
                  height: 55,
                  width: 55,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: '#484848',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}
                onPress={() => handleAddBankSubmit(3)}>
                <Image
                  style={{width: 25, height: 30}}
                  source={Images.trashIcon}
                />
              </Button>
            </View>
            <TouchableOpacity
              style={{
                top: 1,
                borderRadius: 25,
                height: 50,
                alignSelf: 'center',
                width: '75%',
                backgroundColor: '#1a69d5',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              delayPressIn={0}
              onPress={handleAddBankSubmit}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: '#ffffff',
                  letterSpacing: 1,
                  fontSize: 14,
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{height: 25}} />
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          delayPressIn={0}
          style={{
            width: '100%',
            height: 100,
            borderColor: '#c9c6c6',
            borderWidth: 0.5,
            marginTop: 14,
            justifyContent: 'center',
            paddingLeft: 25,
            bottom: 5,
          }}>
          <TouchableWithoutFeedback>
            <TouchableOpacity
              //kylie
              onPress={() => {
                const dataPost = {tokoId: props.tokoId, bankId: item.id};
                props.dispatch(
                  TokoActions.updateDefaultRekeningTokoRequest(dataPost),
                );
              }}
              delayPressIn={0}
              style={{
                marginTop: 8,
                marginRight: 8,
                position: 'absolute',
                right: 0,
                top: 0,
              }}>
              <Image
                style={{width: 20, height: 20}}
                source={
                  item.default ? Images.starBlueIcon : Images.starWhiteIcon
                }
              />
              {/* <MaterialCommunityIcons name="star" size={20} color={item.default?c.Colors.mainBlue:'#f4f4f4'} /> */}
            </TouchableOpacity>
          </TouchableWithoutFeedback>
          <TouchableOpacity
            style={{top: 9, width: '95%'}}
            onPress={() => {
              props.dispatch(TokoActions.postTokoBankReset());
              setAddedBankFormValidation({
                ...addedBankFormValidation,
                isBankAccountNumEmpty: false,
                isBankEmpty: false,
                isBankOwnerEmpty: false,
              });
              setIsAddedBank(false);
              setRekId(item.id);
              setBankIndex(item.id);
              setIsUpdatedBank(true);
              setBankOwnerNameValue(item.holder_name);
              setBankAccountNumValue(item.rekening_number);
              setBankNameValue(item.bank_details);
              setBankValue(item.bank_details.name);
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.bank_details.name === null || item.bank_details.name === ''
                ? '-'
                : item.bank_details.name}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.rekening_number === null || item.rekening_number === ''
                ? '-'
                : item.rekening_number}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.holder_name === null || item.holder_name === ''
                ? '-'
                : item.holder_name}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }
  };
  const handleGetOtp = type => {
    switch (type) {
      case 1:
        {
          const data = {
            email: props.userLogin.data.email
              ? props.userLogin.data.email
              : props.userLogin.data.user.email,
            source: 'withdraw_request',
            type: 'email',
          };
          props.dispatch(AuthActions.otpWithdrawDisbursmentPostRequest(data));
        }
        break;
      case 2:
        {
          if (timeLeft === 0) {
            const data = {
              email: props.userLogin.data.email
                ? props.userLogin.data.email
                : props.userLogin.data.user.email,
              source: 'login_or_register',
              type: 'email',
            };
            props.dispatch(AuthActions.otpWithdrawDisbursmentPostRequest(data));
          } else null;
        }
        break;
      default:
        null;
    }
  };
  const handleCleanState = () => {
    // dispatch(AuthActions.otpWithdrawDisbursmentPostReset());
    props.dispatch(TokoActions.postWithdrawDisbursmentTokoReset());
    props.dispatch(TokoActions.postTokoBankReset());
    props.dispatch(TokoActions.postTokoAddressReset());
    setErrMsgWithdraw(null);
    props.dispatch(TokoActions.getCityReset());
    setIsAddedBank(false);
    setOpenPickerBank(false);
    setSalesValue(0);
    setCommissionValue(0);
    // setBankNameValue(null);
    setBankOwnerNameValue(null);
    setBankIndex(-1);
    setBankValue(null);
    // setBankNameValue(null)
    setIsUpdatedBank(false);
    setCode('');
  };
  const _renderListOrderItem = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: '#ffffff',
          marginTop: 12,
          flexDirection: 'row',
        }}>
        <View
          style={{
            width: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Card
            style={{
              width: 47,
              height: 47,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 0,
              marginRight: 0,
            }}>
            <Image
              style={{width: 38, height: 33}}
              source={
                item.details.length != 0
                  ? {uri: item.details[0].image}
                  : Images.noImg
              }
            />
          </Card>
        </View>
        <View
          style={{
            width: '47%',
            height: '100%',
            marginLeft: 8,
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              color: '#4a4a4a',
              fontSize: 11,
              letterSpacing: 1,
              marginTop: 1,
            }}>
            {item.details.length.toString()} produk
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1,
              marginTop: 3,
            }}>
            {item.supplier === null ? item.toko.name : item.supplier.name}
          </Text>
        </View>
        <View
          style={{
            width: '35%',
            height: '100%',

            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              top: 0,
              color: '#4a4a4a',
              fontFamily: 'NotoSans',
              fontSize: 11,
              marginRight: 1,
              marginTop: 1,
            }}>
            {item.details.length != 0
              ? func.rupiah(item.details[0].price)
              : func.rupiah(0)}
          </Text>

          <_OrderStatusHandler status_pesanan={item.status} />
        </View>
      </View>
    );
  };
  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleNavigation(item)}
        delayPressIn={0}
        style={{
          width: '100%',
          padding: 8,
        }}>
        <Card
          style={{
            width: '100%',
            marginLeft: 0,
            marginRight: 0,

            padding: 15,

            backgroundColor: '#ffffff',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#9b9b9b',
                flex: 1,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 11,
                marginTop: 12,
              }}>
              INV/20201130/XX/XI/6784574
            </Text>
            {item.status === 1 ? (
              <Text
                style={{
                  fontSize: 11,
                  color: '#0f977c',
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                Komisi
              </Text>
            ) : (
              <Text
                style={{
                  fontSize: 11,
                  color: Colors.mainBlue,
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                Penjualan
              </Text>
            )}
          </View>

          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#9b9b9b',
                flex: 1,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 11,
                marginTop: 12,
              }}>
              {moment(item.date).format('ll')}
            </Text>
            <Text
              style={{
                fontSize: 11,
                color: '#4a4a4a',
                textAlign: 'right',
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                marginTop: 12,
              }}>
              {func.rupiah(item.amount)}
            </Text>
          </View>
          <View
            style={{
              marginTop: 15,
              width: '100%',
              borderWidth: 0.4,
              borderColor: '#c9c6c6',
            }}
          />
          {item.orders &&
            item.orders.map((data, index) => {
              return <_renderListOrderItem key={index} item={data} />;
            })}
        </Card>
      </TouchableOpacity>
    );
  };
  const _renderItemDefaultBank = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        disabled
        style={{
          width: '100%',
          height: 100,
          borderColor: Colors.mainBlue,
          borderWidth: 1.5,
          borderRadius: 6,
          marginTop: 14,
          justifyContent: 'center',
          paddingLeft: 25,
          bottom: 5,
        }}>
        <TouchableWithoutFeedback>
          <TouchableOpacity
            disabled
            onPress={() => {
              const dataPost = {tokoId: props.tokoId, bankId: item.id};
              props.dispatch(
                TokoActions.updateDefaultRekeningTokoRequest(dataPost),
              );
            }}
            delayPressIn={0}
            style={{
              marginTop: 8,
              marginRight: 8,
              position: 'absolute',
              right: 0,
              top: 0,
            }}>
            <Image
              style={{width: 20, height: 20}}
              source={item.default ? Images.starBlueIcon : Images.starWhiteIcon}
            />
            {/* <MaterialCommunityIcons name="star" size={20} color={item.default?c.Colors.mainBlue:'#f4f4f4'} /> */}
          </TouchableOpacity>
        </TouchableWithoutFeedback>
        <TouchableOpacity
          style={{top: 9, width: '95%'}}
          onPress={() => {
            props.dispatch(TokoActions.postTokoBankReset());

            setIsAddedBank(false);
            setRekId(item.id);
            setBankIndex(item.id);
            setIsUpdatedBank(true);
            setBankOwnerNameValue(item.holder_name);
            setBankAccountNumValue(item.rekening_number);
            setBankNameValue(item.bank_details);
            setBankValue(item.bank_details.name);
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.bank_details.name === null || item.bank_details.name === ''
              ? '-'
              : item.bank_details.name}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.rekening_number === null || item.rekening_number === ''
              ? '-'
              : item.rekening_number}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#111432',
              fontSize: 12,
              letterSpacing: 1,
            }}>
            {item.holder_name === null || item.holder_name === ''
              ? '-'
              : item.holder_name}
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };
  const handleKycStatus = () => {
    if (getKycStatus === null) {
      return (
        <Card
          style={{
            backgroundColor: '#e3efff',
            width: '100%',
            height: Dimensions.get('window').height * 0.21,
            marginLeft: 0,
            marginRight: 0,
            padding: 25,
            marginTop: 0,
          }}>
          <View style={{width: '100%', height: '20%'}}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 13,
                letterSpacing: 1.2,
                color: '#4a4a4a',
              }}>
              Verifikasi Akun Anda
            </Text>
            <Text
              style={{
                marginTop: 9,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1.2,
                color: '#4a4a4a',
              }}>
              Hey kamu perlu melakukan KYC dulu sebelum dapat menarik uang ke
              rekening kamu sendiri
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('FinanceKYCScreen')}
            delayPressIn={0}
            style={{
              alignItems: 'center',
              backgroundColor: Colors.mainBlue,
              width: '38%',
              borderRadius: 25,
              position: 'absolute',
              bottom: 15,
              left: 20,
              flexDirection: 'row',
            }}>
            <Image
              style={{width: 25, height: 25, marginLeft: 10}}
              source={Images.checklistCircle}
            />
            <Text
              style={{
                fontFamily: 'NotoSans',
                letterSpacing: 1.1,
                fontSize: 11,
                color: '#ffffff',
                padding: 10,
              }}>
              Verifikasi
            </Text>
          </TouchableOpacity>
          <Image
            style={{
              position: 'absolute',
              width: '80%',
              height: '100%',
              alignSelf: 'center',
              bottom: 0,
              right: 0,
            }}
            source={Images.peopleShadow}
          />
        </Card>
      );
    } else {
      switch (getKycStatus.status) {
        case 'pending':
          return (
            <Card
              style={{
                backgroundColor: '#feffe3',
                width: '100%',
                height: Dimensions.get('window').height * 0.21,
                marginLeft: 0,
                marginRight: 0,
                padding: 25,
                marginTop: 0,
              }}>
              <View style={{width: '100%', height: '20%'}}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 13,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Verifikasi Akun Anda sedang diproses
                </Text>
                <Text
                  style={{
                    marginTop: 9,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Akun kamu akan diproses dalam waktu paling lama 2 hari kerja
                </Text>
              </View>
              <Image
                style={{
                  position: 'absolute',
                  width: '50%',
                  height: '100%',
                  alignSelf: 'center',
                  bottom: 10,
                  right: 0,
                }}
                source={Images.clockShadow}
              />
              <TouchableOpacity
                onPress={() => Linking.openURL(bantuanURL)}
                delayPressIn={0}
                style={{
                  alignItems: 'center',
                  backgroundColor: Colors.white,
                  width: '38%',
                  borderRadius: 25,
                  position: 'absolute',
                  bottom: 15,
                  right: 190,
                  flexDirection: 'row',
                  borderColor: Colors.mainBlue,
                  borderWidth: 1,
                }}>
                <Image
                  style={{width: 25, height: 25, marginLeft: 10}}
                  source={Images.questionmarkIcon}
                />
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    letterSpacing: 1.1,
                    fontSize: 11,
                    color: Colors.mainBlue,
                    padding: 10,
                  }}>
                  Bantuan
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(`whatsapp://send?phone=${phoneNumber}`)
                }
                delayPressIn={0}
                style={{
                  alignItems: 'center',
                  backgroundColor: Colors.white,
                  width: '44%',
                  borderRadius: 25,
                  position: 'absolute',
                  bottom: 15,
                  right: 10,
                  flexDirection: 'row',
                  borderColor: Colors.mainBlue,
                  borderWidth: 1,
                }}>
                <Image
                  style={{width: 25, height: 25, marginLeft: 10}}
                  source={Images.waIcon}
                />
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    letterSpacing: 1.1,
                    fontSize: 11,
                    color: Colors.mainBlue,
                    padding: 10,
                  }}>
                  Hubungi Kami
                </Text>
              </TouchableOpacity>
            </Card>
          );
          break;

        case 'rejected':
          return (
            <Card
              style={{
                backgroundColor: '#ffe3e3',
                width: '100%',
                height: Dimensions.get('window').height * 0.21,
                marginLeft: 0,
                marginRight: 0,
                padding: 25,
                marginTop: 0,
              }}>
              <TouchableOpacity
                onPress={() => Linking.openURL(bantuanURL)}
                delayPressIn={0}
                style={{
                  alignItems: 'center',
                  borderRadius: 25,
                  position: 'absolute',
                  right: 12,

                  top: 12,
                  flexDirection: 'row',
                }}>
                <Image
                  style={{width: 25, height: 25, marginLeft: 10}}
                  source={Images.questionmarkIcon}
                />
              </TouchableOpacity>

              <View style={{width: '100%', height: '20%'}}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 13,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Verifikasi Gagal{' '}
                </Text>
                <Text
                  style={{
                    marginTop: 9,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Verifikasi data kamu gagal karena “
                  <B color={'#d0021b'}>{getKycStatus.rejected_reason}</B>”
                </Text>
              </View>
              <Image
                style={{
                  resizeMode: 'contain',
                  position: 'absolute',
                  width: '60%',
                  height: '100%',
                  alignSelf: 'center',
                  bottom: 0,
                  right: 0,
                }}
                source={Images.peopleShadow}
              />

              <TouchableOpacity
                onPress={() => navigation.navigate('FinanceKYCScreen')}
                delayPressIn={0}
                style={{
                  alignItems: 'center',
                  backgroundColor: Colors.mainBlue,
                  width: '38%',
                  borderRadius: 25,
                  position: 'absolute',
                  bottom: 15,
                  right: 190,
                  flexDirection: 'row',
                }}>
                <Image
                  style={{width: 25, height: 25, marginLeft: 10}}
                  source={Images.checklistCircle}
                />
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    letterSpacing: 1.1,
                    fontSize: 11,
                    color: '#ffffff',
                    padding: 10,
                  }}>
                  Verifikasi
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(`whatsapp://send?phone=${phoneNumber}`)
                }
                delayPressIn={0}
                style={{
                  alignItems: 'center',
                  backgroundColor: Colors.white,
                  width: '44%',
                  borderRadius: 25,
                  position: 'absolute',
                  bottom: 15,
                  right: 10,
                  flexDirection: 'row',
                  borderColor: Colors.mainBlue,
                  borderWidth: 1,
                }}>
                <Image
                  style={{width: 25, height: 25, marginLeft: 10}}
                  source={Images.waIcon}
                />
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    letterSpacing: 1.1,
                    fontSize: 11,
                    color: Colors.mainBlue,
                    padding: 10,
                  }}>
                  Hubungi Kami
                </Text>
              </TouchableOpacity>
            </Card>
          );
          break;
        case 'verified':
          return (
            <Card
              style={{
                backgroundColor: '#e3efff',
                width: '100%',
                height: Dimensions.get('window').height * 0.13,
                marginLeft: 0,
                marginRight: 0,
                padding: 25,
                marginTop: 0,
              }}>
              <View style={{width: '100%', height: '20%'}}>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 13,
                      letterSpacing: 1.2,
                      color: '#4a4a4a',
                    }}>
                    {getKycStatus.fullname}
                  </Text>
                  <MaterialCommunityIcons
                    style={{marginLeft: 5, bottom: 2}}
                    color={Colors.mainBlue}
                    name={'check-circle'}
                    size={22}
                  />
                </View>

                <Text
                  style={{
                    marginTop: 9,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Data kamu sudah terverifikasi
                </Text>
              </View>
              <Image
                style={{
                  resizeMode: 'contain',
                  position: 'absolute',
                  width: '170%',
                  height: '170%',
                  alignSelf: 'center',
                  bottom: 0,
                  right: -210,
                }}
                source={Images.peopleShadow}
              />
            </Card>
          );
          break;
        default:
          null;
      }
    }
  };
  useEffect(() => {
    if (tokoId) {
      dispatch(TokoActions.getTokoBankRequest(tokoId));
      dispatch(TokoActions.getTokoFinancialRequest(tokoId));
      dispatch(TokoActions.getKycStatusTokoRequest());
      dispatch(TokoActions.getBankRequest());
    }
  }, [tokoId]);
  const handleRefresh = () => {
    dispatch(TokoActions.getTokoFinancialRequest(tokoId));
    dispatch(TokoActions.getKycStatusTokoRequest());
    dispatch(TokoActions.getBankRequest());
    dispatch(TokoActions.getTokoBankRequest(tokoId));
  };
  const _renderWithDrawDetail = ({item, index}) => {
    return (
      <View style={{flexDirection: 'row', paddingTop: 12}}>
        <Text
          style={{
            flex: 1,
            fontSize: 12,
            fontFamily: 'NotoSans',
            letterSpacing: 1.1,
            color: item.id === 3 ? 'red' : '#1a69d5',
          }}>
          {item.content}
        </Text>
        <Text
          style={{
            flex: 1,
            textAlign: 'right',
            fontFamily: 'NotoSans',
            fontSize: 12,
            letterSpacing: 1.1,
            color: '#4a4a4a',
          }}>
          {func.rupiah(item.value)}
        </Text>
      </View>
    );
  };
  const _openRBSheet = value => {
    setContentSelected(value);
    refRBSheet.current.open();
  };
  const _renderBottomSheetContent = () => {
    switch (contentSelected) {
      case 'whatsapp-otp':
        return (
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
            style={{width: '100%', flex: 1}}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 12,
                letterSpacing: 1.3,
                marginTop: 10,
              }}>
              Detail Penarikan
            </Text>
            <FlatList
              style={{flexGrow: 0, marginTop: 10}}
              data={withdrawDataDetail}
              renderItem={_renderWithDrawDetail}
            />
            <View
              style={{
                marginTop: 20,
                width: '100%',
                borderWidth: 0.3,
                borderColor: '#979797',
              }}
            />
            <View
              style={{
                marginTop: 12,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  color: Colors.mainBlue,
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  letterSpacing: 1.1,
                }}>
                Total Uang Masuk Rekening
              </Text>
              {/* {commissionValue !=0|| salesValue != 0 && ( */}
              <Text
                style={{
                  textAlign: 'right',
                  color: '#4a4a4a',
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  letterSpacing: 1.1,
                  flex: 1,
                }}>
                {func.rupiah(
                  Number(salesValue) +
                    Number(commissionValue) -
                    Number(withdrawDataDetail[2].value),
                )}
              </Text>
              {/* )} */}
            </View>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1.1,
                color: '#4a4a4a',
                marginTop: 40,
              }}>
              Rekening Tujuan
            </Text>

            <FlatList
              extraData={useState}
              nestedScrollEnabled
              data={getDefaultBank}
              style={{flexGrow: 0, marginTop: 12}}
              renderItem={({item, index}) =>
                _renderItemDefaultBank({item, index})
              }
            />
            <Text
              style={{
                marginTop: 35,
                color: Colors.mainBlue,
                fontSize: 30,
                textAlign: 'center',
                fontFamily: 'NotoSans-ExtraBold',
                letterSpacing: 1.98,
              }}>
              Masukkan OTP
            </Text>
            <Text
              style={{
                marginTop: 4,
                color: Colors.mainBlue,

                textAlign: 'center',
                fontFamily: 'NotoSans',
                letterSpacing: 1.98,
              }}>
              {'Kode verifikasi telah dikirim ke'}{' '}
              <B
                color={Colors.mainBlue}
                style={{fontFamily: 'NotoSans-Bold', color: 'white'}}>
                {userData.email}
              </B>
            </Text>
            <View style={[styles.itemCenter]}>
              <View
                onPress={() => pinverifRef.focus()}
                style={{
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <c.PinVerify
                  ref={pinverifRef}
                  cellStyle={styles.cellStyle}
                  cellStyleFocused={styles.cellStyleFocused}
                  textStyle={styles.txtCellStyle}
                  textStyleFocused={styles.textStyleFocused}
                  value={code}
                  onTextChange={code => {
                    setErrMsgWithdraw(false);
                    setCode(code);
                  }}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 23,
                }}>
                <TouchableOpacity
                  onPress={handleResendOtp}
                  disabled={props.isFetchingOtp}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      opacity: timeLeft === 0 ? 1 : 0.4,
                      color: '#9b9b9b',
                      marginTop: 2,
                    }}>
                    {' '}
                    Kirim Ulang OTP
                  </Text>
                </TouchableOpacity>

                <Text
                  style={{
                    marginLeft: 6,
                    fontFamily: 'NotoSans-ExtraBold',
                    color: Colors.mainBlue,
                    fontSize: 15,
                    opacity: 1,
                  }}>
                  00:{timeLeft < 10 ? '0' : ''}
                  {timeLeft}
                </Text>
              </View>
              {errMsgWithdraw && (
                <Text
                  style={[
                    c.Styles.txtInputDesc,
                    {color: 'red', marginTop: 10},
                  ]}>
                  {errMsgWithdraw}
                </Text>
              )}

              <TouchableOpacity
                // disabled={props.isFetching}
                disabled={
                  !isFetchingWithdrawDisburst && code != '' && code.length === 4
                    ? false
                    : true
                }
                onPress={() => {
                  const dataPost = {
                    tokoId: tokoId,
                    func: {rbSheet: refRBSheet, resetState: resetAllState},
                    detail: {
                      rekening_id: getDefaultBank[0].id,
                      sales_amount: salesValue,
                      commission_amount: commissionValue,
                      code: code,
                      token:
                        authLoginData != null
                          ? authLoginData.login.token
                          : authLoginData.login.token,
                      type: 'email',
                    },
                  };
                  props.dispatch(
                    TokoActions.postWithdrawDisbursmentTokoRequest(dataPost),
                  );
                }}
                style={{
                  marginTop: 20,
                  alignSelf: 'center',
                  width: '100%',
                  height: 55,
                  backgroundColor: code != '' ? Colors.mainBlue : '#9b9b9b',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 25,
                }}>
                {props.isFetching ||
                props.isFetchingOtp ||
                isFetchingWithdrawDisburst ? (
                  <ActivityIndicator
                    color={'white'}
                    animating={true}
                    size="large"
                  />
                ) : (
                  <Text
                    style={{
                      fontSize: 17,
                      color: 'white',
                      fontFamily: 'NotoSans-Bold',
                    }}>
                    Transfer ke Rekening
                  </Text>
                )}
              </TouchableOpacity>
            </View>
            <View style={{height: 40}} />
          </ScrollView>
        );
        break;
      default:
        null;
    }
  };
  return (
    <View
      style={{
        width: '100%',

        backgroundColor: '#f4f4f4',
      }}>
      <View
        style={{
          height: 50,
          backgroundColor: Colors.mainBlue,
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity delayPressIn={0} onPress={() => navigation.goBack()}>
          <Image
            style={{width: 25, height: 17, marginLeft: 25}}
            source={Images.arrowBack}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: 'white',
            marginLeft: 25,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          Transfer ke Rekening
        </Text>
      </View>
      <ScrollView
        ref={scrollViewRefParent}
        style={{
          width: '100%',
        }}
        refreshControl={
          <RefreshControl
            refreshing={getFinancialTokoData.fetching}
            onRefresh={handleRefresh}
          />
        }>
        {handleKycStatus()}
        <Card
          style={{
            width: '100%',
            height: Dimensions.get('window').height * 0.4,
            marginLeft: 0,
            marginRight: 0,
            padding: 25,
            marginTop: 0,
          }}>
          <View style={{width: '100%', height: '20%'}}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                fontSize: 14,
                letterSpacing: 1,
                color: '#4a4a4a',
              }}>
              Pilih sumber uang dan masukkan nominal yang ingin ditarik
            </Text>
          </View>
          <View style={{width: '100%', height: '80%'}}>
            <View
              style={{
                width: '100%',
                height: '50%',
                padding: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  height: '25%',
                  flexDirection: 'row',
                  // paddingLeft: 10,
                  // paddingRight: 10,
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: Colors.mainBlue,
                  }}>
                  Penjualan
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Saldo:{' '}
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.sales
                      : 0,
                  )}
                </Text>
              </View>
              <View
                style={{
                  height: '50%',

                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor:
                    addedBankTransferFormValidation.isSalesEmpty ||
                    addedBankTransferFormValidation.isSalesOutOfSaving
                      ? 'red'
                      : salesValue === 0
                      ? '#979797'
                      : Colors.mainBlue,
                }}>
                <Text
                  style={{
                    alignSelf: 'flex-end',

                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.4,
                    color: '#4a4a4a',
                    bottom: 4,
                  }}>
                  Rp
                </Text>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '120%',

                    width: '70%',
                  }}>
                  <TextInputMask
                    includeRawValueInChangeText={true}
                    ref={salesInputRef}
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 18,
                      letterSpacing: 1.4,
                      color: '#4a4a4a',
                      width: '100%',
                      height: '100%',
                      marginTop: 4,
                    }}
                    placeholder={'0'}
                    type={'money'}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: '',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={salesValue}
                    onChangeText={(maskedText, rawText) => {
                      setIsAddedAllSales(false);
                      setSalesValue(isNaN(rawText) ? 0 : rawText);
                      setAddedBankTransferFormValidation({
                        ...addedBankFormValidation,
                        isSalesOutOfSaving: false,
                        isSalesEmpty: false,
                        isOutOfWithdrawMinimum: false,
                        isKYCPendingNReject: false,
                      });
                    }}
                  />
                </View>
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    setAddedBankTransferFormValidation({
                      ...addedBankFormValidation,
                      isSalesOutOfSaving: false,
                      isSalesEmpty: false,
                    });
                    setIsAddedAllSales(!isAddedAllSales);
                    setSalesValue(
                      getFinancialTokoData.data != null
                        ? getFinancialTokoData.data.earnings.sales
                        : 0,
                    );
                  }}
                  style={{
                    backgroundColor: isAddedAllSales
                      ? Colors.mainBlue
                      : 'white',
                    borderRadius: 25,
                    borderWidth: isAddedAllSales ? 0 : 1,
                    borderColor: '#9b9b9b',
                    alignItems: 'center',
                    height: '70%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute',
                    right: 0,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      padding: 15,
                      bottom: 1,
                      fontSize: 10,
                      color: isAddedAllSales ? 'white' : '#9b9b9b',
                      letterSpacing: 1,
                      fontFamily: 'NotoSans',
                    }}>
                    Semua
                  </Text>
                </TouchableOpacity>
              </View>
              {addedBankTransferFormValidation.isSalesEmpty ? (
                <Text
                  style={[
                    c.Styles.txtInputDesc,
                    {
                      color: 'red',
                      width: '88%',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Wajib mengisi Penjualan
                </Text>
              ) : addedBankTransferFormValidation.isSalesOutOfSaving ? (
                <Text
                  style={[
                    c.Styles.txtInputDesc,
                    {
                      color: 'red',
                      width: '88%',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Saldo Anda Tidak Mencukupi
                </Text>
              ) : (
                <Text />
              )}
            </View>
            <View
              style={{
                width: '100%',
                height: '50%',
                padding: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  height: '25%',
                  flexDirection: 'row',
                  // paddingLeft: 10,
                  // paddingRight: 10,
                }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: Colors.mainBlue,
                  }}>
                  Komisi
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: '#4a4a4a',
                  }}>
                  Saldo:{' '}
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.commission
                      : 0,
                  )}
                </Text>
              </View>
              <View
                style={{
                  height: '50%',

                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor:
                    addedBankTransferFormValidation.isCommissionEmpty ||
                    addedBankTransferFormValidation.isCommissionOutOfSaving
                      ? 'red'
                      : commissionValue === 0
                      ? '#979797'
                      : Colors.mainBlue,
                }}>
                <Text
                  style={{
                    alignSelf: 'flex-end',

                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.4,
                    color: '#4a4a4a',
                    bottom: 4,
                  }}>
                  Rp
                </Text>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '120%',

                    width: '70%',
                    marginTop: 4,
                  }}>
                  <TextInputMask
                    ref={commissionInputRef}
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 18,
                      letterSpacing: 1.4,
                      color: '#4a4a4a',
                      width: '100%',
                      height: '100%',
                    }}
                    includeRawValueInChangeText={true}
                    placeholder={'0'}
                    type={'money'}
                    options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: '',
                      suffixUnit: '',
                    }}
                    // onSubmitEditing={Keyboard.dismiss}
                    value={commissionValue}
                    onChangeText={(maskedText, rawText) => {
                      setIsAddedAllCommission(false);
                      setCommissionValue(isNaN(rawText) ? 0 : rawText);
                      setAddedBankTransferFormValidation({
                        ...addedBankFormValidation,
                        isCommissionOutOfSaving: false,
                        isCommissionEmpty: false,
                        isOutOfWithdrawMinimum: false,
                        isKYCPendingNReject: false,
                      });
                    }}
                  />
                </View>
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => {
                    setAddedBankTransferFormValidation({
                      ...addedBankFormValidation,
                      isCommissionOutOfSaving: false,
                      isCommissionEmpty: false,
                    });
                    setIsAddedAllCommission(!isAddedAllCommission);
                    setCommissionValue(
                      getFinancialTokoData.data != null
                        ? getFinancialTokoData.data.earnings.commission
                        : 0,
                    );
                  }}
                  style={{
                    backgroundColor: isAddedAllCommission
                      ? Colors.mainBlue
                      : 'white',
                    borderRadius: 25,
                    borderWidth: isAddedAllCommission ? 0 : 1,
                    borderColor: '#9b9b9b',
                    alignItems: 'center',
                    height: '70%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute',
                    right: 0,
                    bottom: 7,
                  }}>
                  <Text
                    style={{
                      padding: 15,
                      bottom: 1,
                      fontSize: 10,
                      color: isAddedAllCommission ? 'white' : '#9b9b9b',
                      letterSpacing: 1,
                      fontFamily: 'NotoSans',
                    }}>
                    Semua
                  </Text>
                </TouchableOpacity>
              </View>
              {addedBankTransferFormValidation.isCommissionEmpty ? (
                <Text
                  style={[
                    c.Styles.txtInputDesc,
                    {
                      color: 'red',
                      width: '88%',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Wajib mengisi komisi
                </Text>
              ) : addedBankTransferFormValidation.isCommissionOutOfSaving ? (
                <Text
                  style={[
                    c.Styles.txtInputDesc,
                    {
                      color: 'red',
                      width: '88%',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Saldo Anda Tidak Mencukupi
                </Text>
              ) : (
                <Text />
              )}
            </View>
          </View>
        </Card>
        <Card
          style={{
            width: '100%',
            marginLeft: 0,
            marginRight: 0,
            padding: 25,
            marginTop: 0,
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1.2,
              color: '#4a4a4a',
            }}>
            Pilih Rekening
          </Text>

          <ScrollView
            style={[c.Styles.viewTokoStepContent]}
            showsVerticalScrollIndicator={false}>
            <FlatList
              extraData={useState}
              nestedScrollEnabled
              data={getDefaultBank}
              style={{marginTop: 12, padding: 5}}
              renderItem={_renderBankItem}
              keyExtractor={(item, index) => index.toString()}
            />
            {isTokoBankExists && (
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                  textAlign: 'center',
                }}>
                Transfer ke rekening ini dikenakan biaya transfer sebesar{' '}
                {func.rupiah(
                  isTokoBankExists === true
                    ? getDefaultBank[0].bank_details.admin_fee
                    : 0,
                )}
              </Text>
            )}

            {isAddedBank || !isTokoBankExists ? (
              <View
                style={{
                  width: '100%',

                  borderColor: c.Colors.gray,
                  borderWidth: 0.58,
                  marginTop: 17,
                  paddingLeft: 18,
                  paddingRight: 18,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    setAddedBankFormValidation({
                      ...addedBankFormValidation,
                      isBankAccountNumEmpty: false,
                      isBankEmpty: false,
                      isBankOwnerEmpty: false,
                    });
                    setIsAddedBank(false);
                  }}
                  delayPressIn={0}
                  style={{position: 'absolute', right: 8, top: 8}}>
                  <MaterialCommunityIcons
                    name="close"
                    size={22}
                    color="#1a69d5"
                  />
                </TouchableOpacity>

                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    color: '#1a69d5',
                    fontSize: 10,
                    letterSpacing: 1.2,
                    marginTop: 25,
                  }}>
                  Nama Bank
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    props.dispatch(TokoActions.postTokoBankReset());

                    if (bank) {
                      setOpenPickerBank(false);
                    } else {
                      setOpenPickerBank(true);
                    }
                  }}
                  delayPressIn={0}
                  style={{
                    marginTop: 8,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedBankFormValidation.isBankEmpty
                      ? 'red'
                      : bankValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <Text
                    style={{
                      bottom: 3,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                    }}>
                    {bankValue != null ? bankValue.name : 'Nama Bank'}
                  </Text>
                  <MaterialCommunityIcons
                    style={{bottom: 5}}
                    name={!bank ? 'menu-down' : 'menu-up'}
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
                {addedBankFormValidation.isBankEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib memilih Nama Bank
                  </Text>
                )}
                {bank && (
                  <FlatList
                    nestedScrollEnabled
                    renderItem={_addressRenderItem}
                    data={bankData.banks}
                  />
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                      ? 'red'
                      : bankAccountNumValue == null
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    keyboardType={'number-pad'}
                    onChangeText={text => {
                      props.dispatch(TokoActions.postTokoBankReset());
                      setAddedBankTransferFormValidation({
                        ...addedBankTransferFormValidation,
                        isTokoBankEmpty: false,
                        isKYCPendingNReject: false,
                      });
                      setAddedBankFormValidation({
                        ...addedBankFormValidation,
                        isBankAccountNumEmpty: false,
                        isTokoBankEmpty: false,
                      });
                      setBankAccountNumValue(text);
                    }}
                    value={bankAccountNumValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Nomor Rekening'}
                  />
                </View>
                {addedBankFormValidation.isBankAccountNumEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi nomor rekening
                  </Text>
                )}
                <View
                  style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    width: '100%',
                    borderBottomWidth: 0.5,
                    borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                      ? 'red'
                      : bankOwnerNameValue == null || bankOwnerNameValue == ''
                      ? '#c9c6c6'
                      : '#1a69d5',
                  }}>
                  <TextInput
                    style={{
                      bottom: -5,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1,
                      color: '#7b7b7b',
                      width: '100%',
                    }}
                    onChangeText={text => {
                      props.dispatch(TokoActions.postTokoBankReset());
                      setAddedBankTransferFormValidation({
                        ...addedBankTransferFormValidation,
                        isTokoBankEmpty: false,
                        isKYCPendingNReject: false,
                      });
                      setAddedBankFormValidation({
                        ...addedBankFormValidation,
                        isBankOwnerEmpty: false,
                      });

                      setBankOwnerNameValue(text);
                    }}
                    value={bankOwnerNameValue}
                    placeholderTextColor={'#7b7b7b'}
                    placeholder={'Nama Pemilik'}
                  />
                </View>
                {addedBankFormValidation.isBankOwnerEmpty && (
                  <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                    Wajib mengisi nama pemilik rekening
                  </Text>
                )}

                <View style={{height: 14}} />
                {isErrorTokoBankAlreadyRegistered ||
                  (addedBankTransferFormValidation.isTokoBankEmpty && (
                    <Text
                      style={[
                        c.Styles.txtInputDesc,
                        {color: 'red', alignSelf: 'center', top: 15},
                      ]}>
                      {addedBankTransferFormValidation.isTokoBankEmpty
                        ? 'Isikan Bank Terlebih Dahulu'
                        : isErrorTokoBankAlreadyRegistered.message}
                    </Text>
                  ))}
                <TouchableOpacity
                  style={{
                    borderRadius: 25,
                    marginTop: 30,
                    height: 50,
                    alignSelf: 'center',
                    width: '95%',
                    backgroundColor: '#1a69d5',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  delayPressIn={0}
                  onPress={() => {
                    if (!isVerified) {
                      if (userData.is_phone_verified === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 1,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (tokoId === null) {
                        let dataStep = {
                          modalVisible: true,
                          step: 2,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      } else if (!tokoAddress) {
                        let dataStep = {
                          modalVisible: true,
                          step: 3,
                        };
                        dispatch(GlobalActions.setModalVisible(dataStep));
                      }
                      // bottomSheetRef.current.open();
                    } else {
                      handleAddBankSubmit();
                    }
                  }}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      color: '#ffffff',
                      letterSpacing: 1,
                      fontSize: 14,
                    }}>
                    Simpan
                  </Text>
                </TouchableOpacity>

                <View style={{height: 25}} />
              </View>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  setAddedBankFormValidation({
                    ...addedBankFormValidation,
                    isBankAccountNumEmpty: false,
                    isBankEmpty: false,
                    isBankOwnerEmpty: false,
                  });
                  setBankAccountNumValue(null);
                  setBankValue(null);
                  setBankOwnerNameValue(null);
                  setIsAddedBank(true);
                  setIsUpdatedBank(false);
                  setBankIndex(-1);
                  props.dispatch(TokoActions.postTokoBankReset());
                }}
                delayPressIn={0}
                style={{
                  marginTop: 25,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  width: '99%',
                  height: 65,
                  backgroundColor: '#ffffff',
                  marginLeft: 0,
                  marginRight: 0,
                  alignItems: 'center',
                  paddingLeft: 25,
                  paddingRight: 25,

                  borderColor: c.Colors.gray,
                  borderWidth: 0.58,
                }}>
                <TouchableOpacity
                  delayPressIn={0}
                  style={{
                    marginTop: 8,
                    width: 25,
                    height: 25,
                    borderWidth: 1,
                    borderColor: '#a9a9a9',
                    borderRadius: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <MaterialCommunityIcons
                    name="plus"
                    size={20}
                    color="#a9a9a9"
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    marginTop: 8,

                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.5,
                    color: '#7b7b7b',
                  }}>
                  Tambah Rekening
                </Text>
              </TouchableOpacity>
            )}
            {tokoBankData != null && (
              <FlatList
                nestedScrollEnabled
                data={getNonDefaultBank}
                style={{marginTop: 12}}
                renderItem={_renderBankItem}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
            {addedBankTransferFormValidation.isKYCPendingNReject && (
              //jenner
              <Text
                style={[
                  c.Styles.txtInputDesc,
                  {color: 'red', textAlign: 'center', marginTop: 12},
                ]}>
                {getKycStatus === null
                  ? 'Silahkan Verifikasi Akun Anda Terlebih Dahulu'
                  : getKycStatus.status === 'pending'
                  ? 'Verifikasi Akun Anda sedang diproses'
                  : getKycStatus.status === 'rejected'
                  ? `Verifikasi Gagal, karena ${getKycStatus.rejected_reason}`
                  : ''}
              </Text>
            )}
            {addedBankTransferFormValidation.isOutOfWithdrawMinimum && (
              <Text
                style={[
                  c.Styles.txtInputDesc,
                  {color: 'red', marginTop: 10, textAlign: 'center'},
                ]}>
                {`Minimal penarikan adalah ${func.rupiah(
                  getFinancialTokoData.data.earnings.minimumWithdraw,
                )}`}
              </Text>
            )}
            <TouchableOpacity
              onPress={() => {
                if (!isVerified) {
                  if (userData.is_phone_verified === null) {
                    let dataStep = {
                      modalVisible: true,
                      step: 1,
                    };
                    dispatch(GlobalActions.setModalVisible(dataStep));
                  } else if (tokoId === null) {
                    let dataStep = {
                      modalVisible: true,
                      step: 2,
                    };
                    dispatch(GlobalActions.setModalVisible(dataStep));
                  } else if (!tokoAddress) {
                    let dataStep = {
                      modalVisible: true,
                      step: 3,
                    };
                    dispatch(GlobalActions.setModalVisible(dataStep));
                  }
                  // bottomSheetRef.current.open();
                } else {
                  handleSubmitTransfer();
                }
              }}
              delayPressIn={0}
              style={{
                marginTop: 25,
                alignItems: 'center',
                backgroundColor: '#1a69d5',

                borderRadius: 30,
              }}>
              <Text
                style={{
                  padding: 17,
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 14,
                  letterSpacing: 1.1,
                  color: Colors.white,
                }}>
                Transfer ke Rekening
              </Text>
            </TouchableOpacity>
            <View style={{height: 50}} />
          </ScrollView>
        </Card>
      </ScrollView>
      <RBSheet
        onClose={handleCleanState}
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        keyboardAvoidingViewEnabled={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.9}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: 20,
            marginBottom: 50,
            height: '100%',
            justifyContent: 'space-between',
          }}>
          <KeyboardAvoidingView
            style={{flex: 1, flexDirection: 'column'}}
            keyboardVerticalOffset={50}
            behavior="padding"
            enabled>
            {_renderBottomSheetContent()}
          </KeyboardAvoidingView>
        </Content>
      </RBSheet>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getFinancialTokoData: state.toko.tokoFinancialData,
    userData: state.auth.userData.data,
    isFetchingWithdrawDisburst:
      state.toko.postWithdrawDisbursmentTokoData.fetching,

    tokoOrderPrimary: state.toko.tokoOrderData,
    tokoBankData: state.toko.tokoBankData.data,

    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    isErrorTokoBankAlreadyRegistered: state.toko.postTokoBankData.error,
    bankData: state.toko.bankData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    getToko: state.toko.tokoData.data,
    getKycStatusData: state.toko.getKycStatusTokoData.data,

    userLogin: state.auth.loginEmailPostData,
    authLoginData: state.auth.otpWithdrawDisbursmentPostData.data,
    isSuccessOtpwithdrawDisbursment:
      state.auth.otpWithdrawDisbursmentPostData.success,
    errSystem: state.auth.otpPostData.error,
    errSystemReOtp: state.auth.otpWithdrawDisbursmentPostData.error,
    isFetchingOtp: state.auth.otpWithdrawDisbursmentPostData.fetching,
    isFetching: state.auth.otpPostData.fetching,
    getTokenAuthEmail: state.auth.otpPostData.data,
    getErrMsg: state.toko.postWithdrawDisbursmentTokoData.error,
    getErrMsgOtpDisburstment: state.auth.otpWithdrawDisbursmentPostData.error,
    getErrMsgPostDisburstment: state.toko.postWithdrawDisbursmentTokoData.error,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FinanceBankTransferScreen);
