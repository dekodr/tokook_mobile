import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  TouchableWithoutFeedback,
  RefreshControl,
} from 'react-native';
import moment from 'moment';
import OrderIntro from '../../../Order/Intro';
import {
  Button,
  Spinner,
  Card,
  Container,
  Tab,
  Tabs,
  TabHeading,
  CheckBox,
  Content,
} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from '../../../../../Redux/TokoRedux';

import {Images, Colors} from '../../../../../Themes';
import * as func from '../../../../../Helpers/Utils';

import * as c from '../../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import DraggableFlatList from 'react-native-draggable-flatlist';

const FinanceTransactionScreen = props => {
  const {
    navigation,
    dispatch,
    supplierData,
    tokoOrderPrimary,
    tokoId,
    withdrawHistoryData,
    withdrawHistoryFetching,
  } = props;
  const [isExpand, setisExpand] = useState(false);
  const [listIndex, setIndex] = useState(-1);
  const [tabIndex, setTabIndex] = useState(0);
  const [dateIndex, setDateIndex] = useState(-1);
  const [withDrawHistoryDatas, setWithdrawHistoryDatas] = useState(null);
  const dateData = [
    {
      id: 1,
      value: 2019,
    },
    {
      id: 2,
      value: 2020,
    },
    {
      id: 3,
      value: 2021,
    },
  ];
  const isHistoryDataExists =
    withdrawHistoryData !== null
      ? withdrawHistoryData.withdrawHistories
        ? withdrawHistoryData.withdrawHistories.length != 0
          ? true
          : false
        : false
      : false;
  const isFromBankTf = navigation.getParam('isFromBankTf');
  console.log('vgbv', isFromBankTf);
  useEffect(() => {
    dispatch(TokoActions.getWithdrawHistoryTokoRequest(tokoId));
  }, []);
  useEffect(() => {
    const backAction = () => {
      if (isFromBankTf) {
        navigation.pop(2);
      } else {
        navigation.goBack();
      }
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    if (withdrawHistoryData != null) {
      const dataTransform = withdrawHistoryData.withdrawHistories.map(
        (i, x) => {
          return {
            ...i,
            isExpand: false,
          };
        },
      );
      setWithdrawHistoryDatas(dataTransform);
      console.log('cek data transform', dataTransform);
    }
  }, [withdrawHistoryData]);
  const _TransferStatusHandler = status => {
    switch (status) {
      case 'pending':
        return (
          <View
            style={{
              marginTop: 15,
              width: '32%',
              height: 20,
              backgroundColor: '#c2dcff',
              borderRadius: 5,
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#1a69d5',
                letterSpacing: 0.7,
              }}>
              Sedang Diproses
            </Text>
          </View>
        );
        break;

      case 'approved':
        return (
          <View
            style={{
              marginTop: 15,

              width: '18%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Berhasil
            </Text>
          </View>
        );
        break;
      case 'rejected':
        return (
          <View
            style={{
              marginTop: 15,

              width: '15%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#d0021b',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Gagal
            </Text>
          </View>
        );
        break;

      default:
        return null;
    }
  };
  const _OrderStatusHandler = ({status_pesanan}) => {
    switch (status_pesanan) {
      case 1:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c2dcff',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#1a69d5',
                letterSpacing: 0.7,
              }}>
              Pesanan Baru
            </Text>
          </View>
        );
        break;

      case 2:
        return (
          <View
            style={{
              width: '90%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Pesanan Diterima
            </Text>
          </View>
        );
        break;
      case 3:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 4:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Dikirim
            </Text>
          </View>
        );
        break;
      case 5:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Selesai
            </Text>
          </View>
        );
        break;
      case 6:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#d0021b',
                letterSpacing: 0.7,
              }}>
              Pesanan Ditolak
            </Text>
          </View>
        );
        break;
      case 7:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#d0021b',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Ditolak
            </Text>
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const categoryFilter = ({item, index}) => {
    return (
      <TouchableWithoutFeedback
        delayPressIn={0}
        onPress={() => setDateIndex(index)}>
        <View
          style={{
            backgroundColor: dateIndex === index ? c.Colors.mainBlue : 'white',
            borderColor:
              dateIndex === index ? c.Colors.mainBlue : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color: dateIndex === index ? c.Colors.white : c.Colors.grayWhite,
              fontWeight: dateIndex === index ? 'bold' : '400',
            }}>
            {item.value}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };
  const _renderTransferContent = () => {
    return (
      <View style={{width: '100%', height: '100%', backgroundColor: '#f4f4f4'}}>
        {withdrawHistoryData != null && (
          <FlatList
            style={{marginTop: 17}}
            numColumns={1}
            data={withDrawHistoryDatas}
            renderItem={_renderItemTransferList}
          />
        )}

        <View style={{height: 30}} />
      </View>
    );
  };
  const _renderMutationContent = () => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: '#f4f4f4',
          justifyContent: 'space-between',
        }}>
        <View
          style={[
            c.Styles.borderBottom,
            {
              flexDirection: 'row',
              minHeight: 46,
              marginTop: 30,
              marginLeft: 15,
            },
          ]}>
          <DraggableFlatList
            renderItem={categoryFilter}
            data={dateData}
            showsHorizontalScrollIndicator={false}
            horizontal
            nestedScrollEnabled={true}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>

        <FlatList
          numColumns={1}
          data={tokoOrderPrimary.data.transactions}
          renderItem={_renderItemMutationList}
        />
      </View>
    );
  };
  const _renderListOrderItem = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: '#ffffff',
          marginTop: 12,
          flexDirection: 'row',
        }}>
        <View
          style={{
            width: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Card
            style={{
              width: 47,
              height: 47,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 0,
              marginRight: 0,
            }}>
            <Image
              style={{width: 38, height: 33}}
              source={
                item.details.length != 0
                  ? {uri: item.details[0].image}
                  : Images.noImg
              }
            />
          </Card>
        </View>
        <View
          style={{
            width: '47%',
            height: '100%',
            marginLeft: 8,
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              color: '#4a4a4a',
              fontSize: 11,
              letterSpacing: 1,
              marginTop: 1,
            }}>
            {item.details.length.toString()} produk
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1,
              marginTop: 3,
            }}>
            {item.supplier === null ? item.toko.name : item.supplier.name}
          </Text>
        </View>
        <View
          style={{
            width: '35%',
            height: '100%',

            alignItems: 'flex-end',
          }}>
          <Text
            style={{
              top: 0,
              color: '#4a4a4a',
              fontFamily: 'NotoSans',
              fontSize: 11,
              marginRight: 1,
              marginTop: 1,
            }}>
            {item.details.length != 0
              ? func.rupiah(item.details[0].price)
              : func.rupiah(0)}
          </Text>

          <_OrderStatusHandler status_pesanan={item.status} />
        </View>
      </View>
    );
  };
  const _handleChangeIsExpanded = (id, value) => {
    var temp = [...withDrawHistoryDatas];
    let isExpandedValue = null;
    temp.map(t => {
      if (t.id == id) {
        isExpandedValue = {...t, isExpand: value};

        return isExpandedValue;
      }
      return t;
    });
    temp.push(isExpandedValue);
    const result = Array.from(
      temp.reduce((a, o) => a.set(o.id, o), new Map()).values(),
    );

    setWithdrawHistoryDatas(result);
  };
  const _renderItemTransferList = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={
          () => {
            _handleChangeIsExpanded(item.id, !item.isExpand);
          }
          // setIndex(listIndex === index ? -1 : index)
        }
        delayPressIn={0}
        style={{
          width: '100%',
          padding: 8,
        }}>
        <Card
          style={{
            width: '100%',
            marginLeft: 0,
            marginRight: 0,

            padding: 15,

            backgroundColor: '#ffffff',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#9b9b9b',
                flex: 1,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 11,
                marginTop: 12,
              }}>
              {`${moment(item.request_date).format('ll') +
                ' ' +
                moment(item.request_date).format('h:mm')}`}
            </Text>
            {_TransferStatusHandler(item.status)}
          </View>
          <View
            style={{
              marginTop: 15,
              width: '100%',
              borderWidth: 0.4,
              borderColor: '#c9c6c6',
            }}
          />
          {item.rekening_details != null ? (
            <View
              style={{
                width: '100%',

                flexDirection: 'row',
                paddingTop: 15,
              }}>
              <View style={{flexDirection: 'column'}}>
                <Text
                  style={{
                    marginTop: 5,

                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                  }}>
                  {item.rekening_details.bank_name}
                </Text>
                <Text
                  style={{
                    marginTop: 5,
                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                  }}>
                  {item.rekening_details.rekening_number}
                </Text>
                <Text
                  style={{
                    marginTop: 5,

                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                  }}>
                  {item.rekening_details.holder_name}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  position: 'absolute',
                  right: 0,
                  alignSelf: 'center',
                  top: 25,
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    marginRight: 10,
                    alignSelf: 'flex-end',
                  }}>
                  {func.rupiah(item.total_amount - item.fee)}
                </Text>

                <MaterialCommunityIcons
                  style={{
                    marginTop: 10,
                    top: 3,
                  }}
                  name={item.isExpand != true ? 'chevron-down' : 'chevron-up'}
                  size={25}
                  color={c.Colors.mainBlue}
                />
              </View>
            </View>
          ) : (
            <View />
          )}
          {item.isExpand && (
            <View style={{paddingTop: 25}}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    marginTop: 5,
                    flex: 1,
                    fontFamily: 'NotoSans',
                    fontSize: 10,
                    letterSpacing: 1.05,
                    color: Colors.mainBlue,
                  }}>
                  Penjualan
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    marginTop: 5,
                  }}>
                  {func.rupiah(item.sales_amount)}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text
                  style={{
                    flex: 1,
                    marginTop: 5,

                    fontFamily: 'NotoSans',
                    fontSize: 10,
                    letterSpacing: 1.05,
                    color: Colors.mainBlue,
                  }}>
                  Komisi
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    marginTop: 5,
                  }}>
                  {func.rupiah(item.commission_amount)}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text
                  style={{
                    flex: 1,
                    marginTop: 5,

                    fontFamily: 'NotoSans',
                    fontSize: 10,
                    letterSpacing: 1.05,
                    color: 'red',
                  }}>
                  Biaya Admin
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    marginTop: 5,
                  }}>
                  {func.rupiah(item.fee)}
                </Text>
              </View>
              <View
                style={{
                  marginTop: 15,
                  width: '100%',
                  borderWidth: 0.4,
                  borderColor: '#c9c6c6',
                }}
              />
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Text
                  style={{
                    flex: 1,
                    marginTop: 5,

                    fontFamily: 'NotoSans',
                    fontSize: 10,
                    letterSpacing: 1.05,
                    color: Colors.mainBlue,
                  }}>
                  Total Uang Masuk Rekening
                </Text>
                <Text
                  style={{
                    flex: 1,
                    textAlign: 'right',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    marginTop: 5,
                  }}>
                  {func.rupiah(item.total_amount - item.fee)}
                </Text>
              </View>
            </View>
          )}
        </Card>
      </TouchableOpacity>
    );
  };
  const _renderItemMutationList = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => _handleNavigation(item)}
        delayPressIn={0}
        style={{
          width: '100%',
          padding: 8,
          paddingTop: -8,
        }}>
        <Card
          style={{
            width: '100%',
            marginLeft: 0,
            marginRight: 0,

            padding: 15,

            backgroundColor: '#ffffff',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#9b9b9b',
                flex: 1,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1,
                fontSize: 11,
                marginTop: 12,
              }}>
              22 Oct 2020 14:25
            </Text>
          </View>
          <View
            style={{
              marginTop: 15,
              width: '100%',
              borderWidth: 0.4,
              borderColor: '#c9c6c6',
            }}
          />
          <View
            style={{
              width: '100%',

              flexDirection: 'row',
              padding: 15,
            }}>
            <View
              style={{
                flexDirection: 'column',
                width: '100%',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  marginTop: 5,
                  flexDirection: 'row',
                  width: '100%',
                }}>
                <Text
                  style={{
                    color: '#9b9b9b',
                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    flex: 1,
                  }}>
                  Penjualan
                </Text>
                <Text
                  style={{
                    color: '#4a4a4a',
                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    textAlign: 'right',
                    flex: 1,
                  }}>
                  Rp 2.200.000
                </Text>
              </View>
              <View
                style={{
                  marginTop: 15,
                  flexDirection: 'row',
                  width: '100%',
                }}>
                <Text
                  style={{
                    color: '#9b9b9b',
                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    flex: 1,
                  }}>
                  Komisi
                </Text>
                <Text
                  style={{
                    color: '#4a4a4a',
                    fontFamily: 'NotoSans',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    textAlign: 'right',
                    flex: 1,
                  }}>
                  Rp 2.200.000
                </Text>
              </View>
              <View
                style={{
                  marginTop: 15,
                  flexDirection: 'row',
                  width: '100%',
                }}>
                <Text
                  style={{
                    color: '#9b9b9b',
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 13,
                    letterSpacing: 1.3,
                    flex: 1,
                    color: '#9b9b9b',
                  }}>
                  Total
                </Text>
                <Text
                  style={{
                    color: Colors.mainBlue,
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 11,
                    letterSpacing: 1.05,
                    textAlign: 'right',
                    flex: 1,
                  }}>
                  Rp 2.200.000
                </Text>
              </View>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  };
  const _onChangeTab = c => {
    setTabIndex(c.i);
    switch (c.i) {
      case 0: {
        null;
        break;
      }
      case 1: {
        null;
        break;
      }

      default:
        break;
    }
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getWithdrawHistoryTokoRequest(tokoId));
  };

  const historyWithdrawExist = () => {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={withdrawHistoryFetching}
            onRefresh={handleRefresh}
          />
        }>
        {_renderTransferContent()}
      </ScrollView>
    );
  };
  const historyWithdrawEmpty = () => {
    return (
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: '94%',

          alignItems: 'center',
        }}>
        <Image
          style={{
            width: '70%',
            height: '40%',
            resizeMode: 'contain',
            marginTop: 17,
          }}
          source={Images.goldCoinIcon}
        />
        <Text
          style={{
            width: '50%',
            fontFamily: 'NotoSans-Bold',
            letterSpacing: 1.1,
            fontSize: 15,
            color: '#4a4a4a',
            textAlign: 'center',
          }}>
          Kamu belum pernah melakukan penarikan
        </Text>
        <Text
          style={{
            width: '90%',
            fontFamily: 'NotoSans',
            letterSpacing: 1.3,
            fontSize: 12,
            color: '#4a4a4a',
            textAlign: 'center',
            marginTop: 14,
          }}>
          Ayo jual barang yang banyak biar kamu bisa sering-sering melakukan
          penarikan!
        </Text>
      </View>
    );
  };

  return (
    <View
      style={{
        width: '100%',
        backgroundColor: '#f4f4f4',
        height: '100%',
      }}>
      <View
        style={{
          height: 50,
          backgroundColor: Colors.mainBlue,
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() =>
            isFromBankTf ? navigation.pop(2) : navigation.goBack()
          }>
          <Image
            style={{width: 25, height: 17, marginLeft: 25}}
            source={Images.arrowBack}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: 'white',
            marginLeft: 25,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          Riwayat Transfer
        </Text>
      </View>
      {isHistoryDataExists ? historyWithdrawExist() : historyWithdrawEmpty()}

      {/* <Tabs
        onChangeTab={_onChangeTab}
        style={{overflow: 'hidden'}}
        tabBarUnderlineStyle={{
          backgroundColor: Colors.mainBlue,
          height: 5,
          bottom: -17,
        }}>
        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: 'white',
                flexDirection: 'column',
                height: 65,
              }}>
              <Text
                style={{
                  color: tabIndex === 0 ? Colors.mainBlue : '#9b9b9b',
                  marginVertical: 5,
                  fontSize: 13,
                }}>
                Transfer
              </Text>
            </TabHeading>
          }>
          <_renderTransferContent />
        </Tab>

        <Tab
          heading={
            <TabHeading
              style={{
                backgroundColor: 'white',

                flexDirection: 'column',
                height: 65,
              }}>
              <Text
                style={{
                  color: tabIndex === 1 ? Colors.mainBlue : '#9b9b9b',
                  marginVertical: 5,
                  fontSize: 13,
                }}>
                Mutasi
              </Text>
            </TabHeading>
          }>
          <_renderMutationContent />
        </Tab>
      </Tabs>
   */}
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    withdrawHistoryData: state.toko.getWithdrawHistoryTokoData.data,
    withdrawHistoryFetching: state.toko.getWithdrawHistoryTokoData.fetching,
    tokoOrderPrimary: state.toko.tokoOrderData,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FinanceTransactionScreen);
