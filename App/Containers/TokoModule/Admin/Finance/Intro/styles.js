import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  viewbtnWhatsapp: {
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#25d366',
    borderWidth: 0.2,
    borderRadius: 25,
    backgroundColor: '#25d366',
    marginTop: 30,
  },
  viewbtncreateToko: {
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',

    borderWidth: 0.2,
    borderRadius: 25,
    backgroundColor: '#1a69d5',
    marginTop: 30,
  },
  viewbtnKatalog: {
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
    borderWidth: 0.2,
    borderRadius: 25,
    backgroundColor: '#c72920',
    marginTop: 10,
  },
  viewtxtWhatsapp: {
    width: '70%',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  txtWhatsapp: {
    textAlign: 'left',
    fontFamily: 'NotoSans-Bold',
    fontSize: 14,
    marginLeft: '10%',
    letterSpacing: 0.99,
    color: 'white',
  },
});
