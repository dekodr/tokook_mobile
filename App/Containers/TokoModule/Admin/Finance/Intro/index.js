import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import GlobalActions from './../../../../../Redux/GlobalRedux';
import Toast from 'react-native-simple-toast';
import CountDown from 'react-native-countdown-component';
import Config from 'react-native-config';
import moment from 'moment';
import OrderIntro from '../../../Order/Intro';
import {Button, Spinner, Card, Content} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from '../../../../../Redux/TokoRedux';
import styles from './styles';
import {Images, Colors} from '../../../../../Themes';
import * as func from '../../../../../Helpers/Utils';

import * as c from '../../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';

const FinanceIntroScreen = props => {
  const {
    navigation,
    dispatch,
    supplierData,
    tokoOrderPrimary,
    getFinancialTokoData,
    tokoId,
    supplierFetchingData,
    tokoData,
    toko,
    tokoAddressData,
    userData,
  } = props;
  const isFinancialDataExists =
    getFinancialTokoData.data != null
      ? getFinancialTokoData.data.orders
        ? getFinancialTokoData.data.orders.length != 0
          ? true
          : false
        : false
      : false;

  // const getTimeValue = item => {
  //   var dateFuture = new Date(
  //     new Date(item.complaint_deadline).getFullYear() + 1,
  //     0,
  //     1,
  //   );
  //   var dateNow = new Date();

  //   var seconds = Math.floor((dateFuture - dateNow) / 1000);
  //   var minutes = Math.floor(seconds / 60);
  //   var hours = Math.floor(minutes / 60);
  //   var days = Math.floor(hours / 24);

  //   hours = hours - days * 24;
  //   minutes = minutes - days * 24 * 60 - hours * 60;
  //   seconds = seconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60;

  //   setInterval(() => {
  //     console.log('cantik', true);
  //     return `${hours} : ${minutes} : ${seconds}`;
  //   }, 1000);
  // };
  useEffect(() => {
    if (tokoId) {
      dispatch(TokoActions.getTokoFinancialRequest(tokoId));

      dispatch(TokoActions.getKycStatusTokoRequest());
    }
  }, [tokoId]);
  // useEffect(() => {
  //   if (getFinancialTokoData != null) {
  //     {
  //       setInterval(
  //         () => {
  //           console.log('cek interval', getTimeValue());

  //           getTimeValue();
  //         },

  //         1000,
  //       );
  //     }
  //   }
  // }, [getFinancialTokoData]);
  const B = props => (
    <Text
      onPress={props.onPress}
      style={{
        fontFamily: 'NotoSans-Bold',
        color: '#1a69d5',
        fontSize: 10,
        letterSpacing: 1.3,
      }}>
      {props.children}
    </Text>
  );
  useEffect(() => {
    const backAction = () => {
      navigation.goBack(null);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const handleCopyLink = () => {
    Clipboard.setString(Config.TOKO_NAME + toko.link);
    Toast.show('Link disalin');
  };
  const _OrderStatusHandler = status_pesanan => {
    console.log('chimii', status_pesanan);
    switch (status_pesanan) {
      case 14:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Siap Dicairkan
            </Text>
          </View>
        );
        break;

      case 2:
        return (
          <View
            style={{
              width: '90%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: 'white',
                fontSize: 10,
                color: '#2b814a',
                letterSpacing: 0.7,
              }}>
              Pesanan Diterima
            </Text>
          </View>
        );
        break;
      case 3:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Diterima
            </Text>
          </View>
        );
        break;
      case 4:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Dikirim
            </Text>
          </View>
        );
        break;
      case 5:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#c3ffd9',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#2b814a',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pesanan Selesai
            </Text>
          </View>
        );
        break;
      case 6:
        return (
          <View
            style={{
              width: '85%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',

                fontSize: 10,
                color: '#d0021b',
                letterSpacing: 0.7,
              }}>
              Pesanan Ditolak
            </Text>
          </View>
        );
        break;
      case 7:
        return (
          <View
            style={{
              width: '115%',
              height: 20,
              backgroundColor: '#ffc2c2',
              borderRadius: 5,
              position: 'absolute',
              bottom: 4,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                color: '#d0021b',

                fontSize: 10,
                letterSpacing: 0.7,
              }}>
              Pembayaran Ditolak
            </Text>
          </View>
        );
        break;
      default:
        return null;
    }
  };
  const _renderListOrderItem = ({item, index, status, complaint_deadline}) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: complaint_deadline ? '#F4F4F4' : '#ffffff',
          marginTop: 12,
          flexDirection: 'row',
        }}>
        <View
          style={{
            width: '15%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Card
            style={{
              width: 47,
              height: 47,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 0,
              marginRight: 0,
            }}>
            <Image
              style={{width: 38, height: 33}}
              source={item.length != 0 ? {uri: item.image} : Images.noImg}
            />
          </Card>
        </View>
        <View
          style={{
            width: '47%',
            height: '100%',
            marginLeft: 8,
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              color: '#4a4a4a',
              fontSize: 11,
              letterSpacing: 1,
              marginTop: 1,
            }}>
            {item.quantity} produk
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1,
              marginTop: 3,
            }}>
            {item.name}
          </Text>
        </View>
        <View
          style={{
            width: '35%',
            height: '60%',

            alignItems: 'flex-end',
          }}>
          {/* <Text
            style={{
              top: 12,
              color: '#4a4a4a',
              fontFamily: 'NotoSans',
              fontSize: 11,
              marginRight: 1,
              marginTop: 1,
            }}>
            {item.length != 0 ? func.rupiah(item.price) : func.rupiah(0)}
          </Text> */}
          {_OrderStatusHandler(status)}
        </View>
      </View>
    );
  };

  const _renderItem = ({item, index}) => {
    if (item.complaint_deadline != undefined) {
      const nowContinueBuyGold = new Date();
      nowContinueBuyGold.setHours(nowContinueBuyGold.getHours());

      const deadlineContinueBuy = new Date(item.complaint_deadline);
      deadlineContinueBuy.setHours(deadlineContinueBuy.getHours() + 2);
      const total_hoursContinueBuy =
        deadlineContinueBuy.getTime() - nowContinueBuyGold.getTime();

      //
      const start = new Date();
      const end = new Date(item.complaint_deadline);
      const range = moment(item.complaint_deadline)
        .endOf('day')
        .fromNow();
      console.log('moment gw', moment(item.complaint_deadline).format('LL'));
      const now = moment(new Date());
      const expiration = moment(item.complaint_deadline);

      // get the difference between the moments
      const diff = expiration.diff(now);

      //express as a duration
      const diffDuration = moment.duration(diff);

      // display
      console.log('Days:', diffDuration.days());
      console.log('Hours:', diffDuration.hours());
      console.log('Minutes:', diffDuration.minutes());
      const getDays =
        diffDuration.days() < 10
          ? '0' + diffDuration.days()
          : diffDuration.days();
      const getHours =
        diffDuration.hours() < 10
          ? '0' + diffDuration.hours()
          : diffDuration.hours();
      const getMinutes =
        diffDuration.minutes() < 10
          ? '0' + diffDuration.minutes()
          : diffDuration.minutes();

      //

      var diffr = moment.duration(
        moment(item.complaint_deadline).diff(moment(new Date())),
      );
      //difference of the expiry date-time given and current date-time
      var hours = parseInt(diffr.asHours());
      var minutes = parseInt(diffr.minutes());
      var days = parseInt(diffr.asDays());
      var d = days * 60 * 60 + hours * 60 * 60 + minutes * 60;
      return (
        <TouchableOpacity
          disabled
          // onPress={() => _handleNavigation(item)}
          delayPressIn={0}
          style={{
            width: '100%',
            padding: 8,
          }}>
          <Card
            style={{
              width: '100%',
              marginLeft: 0,
              marginRight: 0,

              padding: 15,

              backgroundColor: '#f4f4f4',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#9b9b9b',
                  flex: 1,
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 11,
                  marginTop: 12,
                }}>
                {item.invoice_no}
              </Text>

              <Text
                style={{
                  fontSize: 11,
                  color: item.type === 'Komisi' ? '#0f977c' : Colors.mainBlue,
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                {item.type}
              </Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#9b9b9b',
                  flex: 1,
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 11,
                  marginTop: 12,
                }}>
                {moment(item.date).format('ll')}
              </Text>
              <Text
                style={{
                  fontSize: 11,
                  color: '#4a4a4a',
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                {func.rupiah(item.actualEarnings)}
              </Text>
            </View>
            <View
              style={{
                marginTop: 15,
                width: '100%',
                borderWidth: 0.4,
                borderColor: '#c9c6c6',
              }}
            />
            {item.details &&
              item.details.map((data, index) => {
                return (
                  <_renderListOrderItem
                    complaint_deadline={
                      item.complaint_deadline != undefined ? true : false
                    }
                    status={item.status}
                    key={index}
                    item={data}
                  />
                );
              })}

            {item.status === 5 && (
              <View>
                <View
                  style={{
                    marginTop: 15,
                    width: '100%',
                    borderWidth: 0.4,
                    borderColor: '#c9c6c6',
                  }}
                />
                <View style={{flexDirection: 'column'}}>
                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      textAlign: 'center',
                      fontSize: 10,
                      letterSpacing: 1.1,
                      color: '#4a4a4a',
                      marginTop: 12,
                    }}>
                    Jika pembeli tidak komplain, uang akan masuk ke saldo anda
                    dalam waktu
                    <B>
                      {' '}
                      {` ${getDays} Hari :  ${getHours} Jam : ${getMinutes} Menit`}
                    </B>{' '}
                  </Text>
                  {/* <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',

                      marginTop: 6,
                    }}>
                    <CountDown
                      size={10}
                      until={d}
                      digitStyle={{
                        backgroundColor: Colors.white,
                      }}
                      digitTxtStyle={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 10,
                        letterSpacing: 1.5,
                        color: '#153F93',
                      }}
                      separatorStyle={{color: '#153F93'}}
                      timeToShow={['D', 'H', 'M']}
                      timeLabels={{h: null, m: null, s: null}}
                      timeLabelStyle={{
                        fontSize: 10,
                        fontFamily: 'NotoSans',
                        letterSpacing: 1.29,
                        color: Colors.grayWhite,
                        textAlign: 'center',
                      }}
                      showSeparator
                    />
                  </View> */}
                </View>
                {/* <Text
                  style={{
                    fontFamily: 'NotoSans',
                    textAlign: 'center',
                    fontSize: 10,
                    letterSpacing: 1.1,
                    color: '#4a4a4a',
                  }}>
                  uang akan masuk ke saldo anda
                </Text> */}
              </View>
            )}
          </Card>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          disabled
          // onPress={() => _handleNavigation(item)}
          delayPressIn={0}
          style={{
            width: '100%',
            padding: 8,
          }}>
          <Card
            style={{
              width: '100%',
              marginLeft: 0,
              marginRight: 0,

              padding: 15,

              backgroundColor: '#ffffff',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#9b9b9b',
                  flex: 1,
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 11,
                  marginTop: 12,
                }}>
                {item.invoice_no}
              </Text>

              <Text
                style={{
                  fontSize: 11,
                  color: item.type === 'Komisi' ? '#0f977c' : Colors.mainBlue,
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                {item.type}
              </Text>
            </View>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#9b9b9b',
                  flex: 1,
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  fontSize: 11,
                  marginTop: 12,
                }}>
                {moment(item.date).format('ll')}
              </Text>
              <Text
                style={{
                  fontSize: 11,
                  color: '#4a4a4a',
                  textAlign: 'right',
                  fontFamily: 'NotoSans-Bold',
                  letterSpacing: 1,
                  marginTop: 12,
                }}>
                {func.rupiah(item.actualEarnings)}
              </Text>
            </View>
            <View
              style={{
                marginTop: 15,
                width: '100%',
                borderWidth: 0.4,
                borderColor: '#c9c6c6',
              }}
            />
            {item.details &&
              item.details.map((data, index) => {
                return (
                  <_renderListOrderItem
                    status={item.status}
                    key={index}
                    item={data}
                  />
                );
              })}
          </Card>
        </TouchableOpacity>
      );
    }
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getTokoFinancialRequest(tokoId));
  };
  const financeListEmpty = () => {
    return (
      <View>
        <View style={c.Styles.justifyCenter}>
          <View style={c.Styles.alignItemsCenter}>
            <c.Image
              source={Images.orderIcn}
              style={{
                width: 300,
                height: 240,
                resizeMode: 'contain',
                marginTop: 25,
              }}
            />
          </View>
          <View style={[{width: '80%'}, c.Styles.alignCenter]}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'NotoSans-Bold',
                letterSpacing: 1.43,
                color: '#4a4a4a',
                textAlign: 'center',
              }}>
              Kamu belum ada pesanan
            </Text>
          </View>
          <View style={[{width: '80%', marginTop: '5%'}, c.Styles.alignCenter]}>
            <Text
              style={{
                fontFamily: 'NotoSans-Regular',
                letterSpacing: 1.13,
                fontSize: 12,
                color: '#4a4a4a',
              }}>
              {tokoData
                ? 'Bagikan Link toko kamu ke teman - teman kamu lewat Whatsapp dan Social media'
                : 'Untuk mulai mendapat pesanan ayo buat Toko kamu sekarang atau Cek katalog Marketplace kita !'}
            </Text>
            {tokoData ? (
              <View
                style={{
                  borderBottomWidth: 0.8,
                  borderBottomColor: '#979797',
                  marginTop: '3%',
                  paddingVertical: '2%',
                }}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 1.14,
                    color: '#1a69d5',
                  }}>
                  {(Config.TOKO_NAME + toko.link).length <= 32
                    ? Config.TOKO_NAME + toko.link
                    : (Config.TOKO_NAME + toko.link).substring(0, 32) + '...'}
                </Text>
                <View
                  style={{
                    position: 'absolute',
                    right: 0,
                  }}>
                  <TouchableOpacity onPress={handleCopyLink} delayPressIn={0}>
                    <MaterialCommunityIcons
                      name="content-copy"
                      size={25}
                      color={c.Colors.grayWhite}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ) : null}
          </View>
          {tokoData ? (
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(
                  `whatsapp://send?text=${window.encodeURIComponent(
                    `Hi, ${
                      toko.name
                    }  baru membuat website baru nih \n \nYuk kunjungi toko nya disini ${Config.TOKO_NAME +
                      toko.link}`,
                  )}`,
                ).catch(() =>
                  Linking.openURL(
                    `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                  ),
                )
              }
              style={styles.viewbtnWhatsapp}>
              <View
                //onPress
                style={styles.viewtxtWhatsapp}>
                <MaterialCommunityIcons
                  name="whatsapp"
                  size={20}
                  color={'white'}
                />
                <Text style={styles.txtWhatsapp}>Bagikan lewat Whatsapp</Text>
              </View>
            </TouchableOpacity>
          ) : !isVerified ? (
            <View>
              <TouchableOpacity
                onPress={() => {
                  if (!isVerified) {
                    if (userData.data.is_phone_verified === null) {
                      let dataStep = {
                        modalVisible: true,
                        step: 1,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    } else if (tokoId === null) {
                      let dataStep = {
                        modalVisible: true,
                        step: 2,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    } else if (!tokoAddress) {
                      let dataStep = {
                        modalVisible: true,
                        step: 3,
                      };
                      dispatch(GlobalActions.setModalVisible(dataStep));
                    }
                    // bottomSheetRef.current.open();
                  }
                }}
                style={styles.viewbtncreateToko}>
                <View style={styles.viewtxtWhatsapp}>
                  <MaterialCommunityIcons
                    name="storefront"
                    size={20}
                    color={'white'}
                  />
                  <Text style={styles.txtWhatsapp}>Buat Toko Sekarang</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.navigate('Dropship')}
                style={styles.viewbtnKatalog}>
                <View style={styles.viewtxtWhatsapp}>
                  <MaterialCommunityIcons
                    name="cart-outline"
                    size={20}
                    color={'white'}
                  />
                  <Text style={styles.txtWhatsapp}>
                    Cek Katalog Marketplace
                  </Text>
                </View>
              </TouchableOpacity>
              <View style={{height: 40}} />
            </View>
          ) : (
            <View />
          )}
        </View>
      </View>
    );
  };
  console.log('getFinancialTokoData', getFinancialTokoData);
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: isFinancialDataExists ? '#f4f4f4' : 'white',
      }}>
      <View
        style={{
          height: 50,
          backgroundColor: Colors.mainBlue,
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => navigation.goBack(null)}>
          <Image
            style={{width: 25, height: 17, marginLeft: 25}}
            source={Images.arrowBack}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: 'white',
            marginLeft: 25,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          Finansial
        </Text>
        <TouchableOpacity
          delayPressIn={0}
          style={{
            right: 0,
            position: 'absolute',
            marginRight: 15,
            width: 50,
            height: 50,

            alignItems: 'flex-end',
            justifyContent: 'center',
          }}
          onPress={() => navigation.navigate('FinanceTransactionScreen')}>
          <Image
            style={{
              width: 15,
              height: 15,
            }}
            source={Images.clockWhite}
          />
        </TouchableOpacity>
      </View>
      {getFinancialTokoData.data === null ? (
        <Content>
          <View
            style={{
              width: '100%',
              height: 225,
              backgroundColor: Colors.mainBlue,
            }}>
            <View
              style={{
                width: '100%',
                height: '40%',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1.2,
                  color: 'white',
                  marginTop: 17,
                }}>
                Total
              </Text>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 20,
                  letterSpacing: 2,
                  color: 'white',
                  marginTop: 5,
                }}>
                {func.rupiah(
                  getFinancialTokoData.data != null
                    ? getFinancialTokoData.data.earnings.total
                    : 0,
                )}
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#145fc6',
                  width: '85%',
                  bottom: 0,
                  position: 'absolute',
                }}
              />
            </View>
            <View
              style={{
                width: '100%',
                height: '50%',
                alignItems: 'center',
                marginTop: 12,
                backgroundColor: Colors.mainBlue,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: '50%',
                  height: '100%',
                  borderRightWidth: 1,
                  borderRightColor: '#145fc6',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    marginTop: 12,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.3,
                    color: 'white',
                  }}>
                  Penjualan
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.5,
                    color: 'white',
                    marginTop: 6,
                    paddingBottom: 10,
                  }}>
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.sales
                      : 0,
                  )}
                </Text>
              </View>
              <View
                style={{
                  width: '50%',
                  height: '100%',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    marginTop: 12,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.3,
                    color: 'white',
                  }}>
                  Komisi
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.5,
                    color: 'white',
                    marginTop: 6,
                    paddingBottom: 10,
                  }}>
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.commission
                      : 0,
                  )}
                </Text>
              </View>
            </View>
          </View>
          {financeListEmpty()}
          <TouchableOpacity
            delayPressIn={0}
            style={{
              position: 'absolute',

              height: 60,
              width: '93%',
              top: 180,

              right: 14,
              left: 14,
            }}>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                navigation.navigate('FinanceBankTransferScreen');
              }}
              style={{
                height: '100%',
                alignItems: 'center',
                marginLeft: 0,
                marginRight: 0,
                fontSize: 15,
                letterSpacing: 1.55,
                borderWidth: 0.1,
                borderRadius: 5,
                backgroundColor: 'white',

                justifyContent: 'center',
                flexDirection: 'row',
                width: '100%',
              }}>
              <Image
                style={{width: 23, height: 23, marginRight: 15, top: -3}}
                source={Images.downloadIcon}
              />
              <Text
                style={{
                  color: '#4a4a4a',
                  fontFamily: 'NotoSans',
                  letterSpacing: 1.2,
                }}>
                Tarik Saldo
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </Content>
      ) : (
        <View style={{flex: 1}}>
          <View
            style={{
              width: '100%',
              height: 225,
              backgroundColor: Colors.mainBlue,
            }}>
            <View
              style={{
                width: '100%',
                height: '40%',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1.2,
                  color: 'white',
                  marginTop: 17,
                }}>
                Total
              </Text>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 20,
                  letterSpacing: 2,
                  color: 'white',
                  marginTop: 5,
                }}>
                {func.rupiah(
                  getFinancialTokoData.data != null
                    ? getFinancialTokoData.data.earnings.total
                    : 0,
                )}
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#145fc6',
                  width: '85%',
                  bottom: 0,
                  position: 'absolute',
                }}
              />
            </View>
            <View
              style={{
                width: '100%',
                height: '50%',
                alignItems: 'center',
                marginTop: 12,
                backgroundColor: Colors.mainBlue,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: '50%',
                  height: '100%',
                  borderRightWidth: 1,
                  borderRightColor: '#145fc6',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    marginTop: 12,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.3,
                    color: 'white',
                  }}>
                  Penjualan
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.5,
                    color: 'white',
                    marginTop: 6,
                    paddingBottom: 10,
                  }}>
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.sales
                      : 0,
                  )}
                </Text>
              </View>
              <View
                style={{
                  width: '50%',
                  height: '100%',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    marginTop: 12,
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.3,
                    color: 'white',
                  }}>
                  Komisi
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 15,
                    letterSpacing: 1.5,
                    color: 'white',
                    marginTop: 6,
                    paddingBottom: 10,
                  }}>
                  {func.rupiah(
                    getFinancialTokoData.data != null
                      ? getFinancialTokoData.data.earnings.commission
                      : 0,
                  )}
                </Text>
              </View>
            </View>
          </View>
          <ScrollView
            style={{
              height: '90%',
              flex: 1,
              marginTop: 35,
            }}
            refreshControl={
              <RefreshControl
                refreshing={supplierFetchingData}
                onRefresh={handleRefresh}
              />
            }>
            <Content contentContainerStyle={{flex: 1}}>
              <FlatList
                nestedScrollEnabled
                numColumns={1}
                data={getFinancialTokoData.data.orders}
                renderItem={_renderItem}
              />
            </Content>
          </ScrollView>
          <TouchableOpacity
            delayPressIn={0}
            style={{
              position: 'absolute',

              height: 60,
              width: '93%',
              top: 180,

              right: 14,
              left: 14,
            }}>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                navigation.navigate('FinanceBankTransferScreen');
              }}
              style={{
                height: '100%',
                alignItems: 'center',
                marginLeft: 0,
                marginRight: 0,
                fontSize: 15,
                letterSpacing: 1.55,
                borderWidth: 0.1,
                borderRadius: 5,
                backgroundColor: 'white',

                justifyContent: 'center',
                flexDirection: 'row',
                width: '100%',
              }}>
              <Image
                style={{width: 23, height: 23, marginRight: 15, top: -3}}
                source={Images.downloadIcon}
              />
              <Text
                style={{
                  color: '#4a4a4a',
                  fontFamily: 'NotoSans',
                  letterSpacing: 1.2,
                }}>
                Tarik Saldo
              </Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    getFinancialTokoData: state.toko.tokoFinancialData,

    tokoOrderPrimary: state.toko.tokoOrderData,

    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    toko: state.toko.detailTokoData.data
      ? state.toko.detailTokoData.data.toko
        ? state.toko.detailTokoData.data.toko
        : {}
      : {},
    supplierFetchingData: state.toko.supplierData.fetching,
    userData: state.auth.userData,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FinanceIntroScreen);
