import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  FlatList,
  Pressable,
  Image,
  RefreshControl,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import InAppReview from 'react-native-in-app-review';
import {Picker} from '@react-native-picker/picker';
import {Button, Spinner, Card, Content, Container} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import MapStyle from './../../../../Fixtures/MapStyle.json';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import TokoActions from '../../../../Redux/TokoRedux';
import AuthActions from '../../../../Redux/AuthenticationRedux';
import {Images, Colors} from '../../../../Themes/';
import {Snackbar} from 'react-native-paper';
import Modal from 'react-native-modal';
import Config from 'react-native-config';
import * as func from '../../../../Helpers/Utils';
import * as c from '../../../../Components';
import ImagePicker from 'react-native-image-crop-picker';
import Foundation from 'react-native-vector-icons/Foundation';
import styles from './styles';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
let version = DeviceInfo.getVersion();
const webURL = 'https://dev.tokook.id/';
const phoneNumber = '+6287878983919';
const bantuanURL = 'https://web.tokook.id/bantuan/';

const errorLinkMessage = 'link tidak valid';
const bankData = [
  {
    id: 1,
    bank_name: 'Mandiri',
    bank_account: 5665654343,
    owner: 'Gwen Stefani',
  },
  {id: 2, bank_name: 'BCA', bank_account: 3456767767, owner: 'Munch Le Bawl'},

  {id: 3, bank_name: 'BNI', bank_account: 8986564346, owner: 'Lynx De'},
];
const initialValidation = {
  isProvinceEmpty: false,
  isCityEmpty: false,
  isDistrictEmpty: false,
  isPostalCodeEmpty: false,
  isAddressEmpty: false,
  isDetailAddressEmpty: false,
  isErrorTokoBankAlreadyRegistered: false,
};
const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
function index(props) {
  // const [bank, setBank] = useState(bankData);
  const {
    toko,
    tokoId,
    listBusinessType,
    productData,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    tokoAddressData,
    bankData,
    tokoBankData,
    bottomSheetRef,
    isErrorTokoBankAlreadyRegistered,
    setStep,
    tokoAddress,
    userData,
    tokoData,
    navigation,
    dispatch,
    getFinancialEarning,
    getKycStatusData,
    useRef,
    getOtpWa,
  } = props;
  const _handleNavigation = () => {
    navigation.navigate('CategoryScreen');
  };
  const getKycData = getKycStatusData != null ? getKycStatusData.kyc : null;
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialValidation,
  );
  const [addedBankFormValidation, setAddedBankFormValidation] = useState(
    initialBankValidation,
  );
  const [code, setCode] = useState('');
  const [loadingStep, setloadingStep] = useState(false);
  const [isAddedAddress, setIsAddedAddress] = useState(false);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [editSelected, setEditSelected] = useState(null);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState(null);
  const [detailAddressValue, setDetailAddressValue] = useState(null);
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [wanumber, setwaNumber] = useState('');
  const [timeLeft, setTimeLeft] = useState(0);
  const [errMsg, setErrMsg] = useState(null);
  const [token, setToken] = useState('');
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
    isTokpedLinkInvalid: false,
    isShopeeLinkInvalid: false,
    isBlibliLinkInvalid: false,
    isInstagramLinkInvalid: false,
    isFacebookLinkInvalid: false,
    isLineLinkInvalid: false,
    isYoutubeLinkInvalid: false,
  });
  const [formToko, setFormToko] = useState({
    business_type_id: null,
    name: null,
    link: null,
    whatsapp: null,
    instagram: null,
    facebook: null,
    line: null,
    youtube: null,
    tokopedia: null,
    shopee: null,
    blibli: null,
    inputOtherBusinessType: null,
  });
  const [message, setMessage] = useState(null);
  const [messageShareToko, setMessageShareToko] = useState(null);
  const refRBSheet = useRef();
  const refVerifwa = useRef();
  const refInputBusinessType = useRef(null);
  const [imagesTemp, setImagesTemp] = useState([]);
  const [snackBar, setSnackBar] = useState({show: false, message: ''});
  const [loadingLogout, setLoadingLogout] = useState(false);
  const handlegetOtp = () => {
    const data = {
      phone: wanumber,
      source: 'phone_verification',
      type: 'whatsapp',
    };
    setCode('');
    props.dispatch(AuthActions.getOtpPostRequest(data));
  };
  const handlewhatsappVerification = () => {
    const data = {
      item: {
        phone: wanumber,
        code: code,
        token: token,
      },
      func: {
        reloadData: () => {
          setloadingStep(false);
          props.dispatch(AuthActions.getUserRequest(props.access_token));
          setCode('');
          setwaNumber('');
          console.log('berhasil');
          setTimeLeft(0);
          refVerifwa.current.close();
        },
        reloadFailed: () => {
          setloadingStep(false);
        },
      },
    };
    setloadingStep(true);
    props.dispatch(AuthActions.whatsappVerificationRequest(data));
  };
  useEffect(() => {
    if (
      // Give you result if version of device supported to rate app or not!
      InAppReview.isAvailable()
    ) {
    }
    if (props.tokoId) {
      dispatch(TokoActions.getDetailTokoRequest(tokoId));
      dispatch(TokoActions.getProvinceRequest());
      dispatch(TokoActions.getBankRequest());
    }
  }, []);
  useEffect(() => {
    if (getOtpWa.success) {
      const {token} = getOtpWa.data.data;
      setTimeLeft(30);
      setToken(token);
    }
  }, [getOtpWa.success]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }
    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // clear interval on re-render to avoid memory leaks

    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  useEffect(() => {
    if (formToko.name) {
      setTokoValidation({...tokoValidation, isNameTokoEmpty: false});
    }
    if (formToko.link) {
      setTokoValidation({...tokoValidation, isLinkTokoEmpty: false});
    }
    if (formToko.whatsapp) {
      setTokoValidation({...tokoValidation, isWhatsappEmpty: false});
    }
    if (tokoValidation.isTokpedLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      });
    }
    if (tokoValidation.isShopeeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      });
    }
    if (tokoValidation.isBlibliLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      });
    }
    if (tokoValidation.isInstagramLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      });
    }
    if (tokoValidation.isFacebookLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      });
    }
    if (tokoValidation.isLineLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isLineLinkInvalid: !isLinkValid(formToko.line),
      });
    }
    if (tokoValidation.isYoutubeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
    }
  }, [formToko]);

  useEffect(() => {
    if (imagesTemp.length > 0) {
      imagesTemp.forEach((img, i) => {
        const formData = new FormData();
        formData.append('image', img);
        let uploadData = [];
        uploadData = {
          data: {
            tokoId: props.tokoId,
            item: formData,
            func: {
              reloadData: (url) => {
                const data = {
                  tokoId: props.tokoId,
                  item: {
                    icon: url,
                  },
                  func: {reloadData: reload},
                };
                dispatch(TokoActions.tokoUpdateRequest(data));
              },
            },
          },
        };

        dispatch(TokoActions.uploadProductImageRequest(uploadData));

        // func.handleUploadTokoProduct(props.dispatch, props.idToko, formData, (url)=> {

        //     if (i >= imagesTemp.length - 1) {
        //         func.handleUpdateToko(props.dispatch, props.idToko, {icon: url}, (success)=> {
        //             setUpdateSuccess(success)
        //             reload()
        //         })
        //     }

        // }, i)
      });
    }
  }, [imagesTemp]);

  useEffect(() => {
    setFormToko({
      ...formToko,
      business_type_id: toko.business_type_id,
      name: toko.name,
      link: toko.link,
      whatsapp: toko.whatsapp,
      instagram: toko.instagram,
      facebook: toko.facebook,
      line: toko.line,
      youtube: toko.youtube,
      tokopedia: toko.tokopedia,
      shopee: toko.shopee,
      blibli: toko.blibli,
      business_type: toko.business_type,
    });
    setMessageShareToko(
      `Hi, ${
        toko.name
      } sedang menjual perlengkapan yang kamu butuhkan , yuk kunjungi website kami dilink ini yah ${
        Config.TOKO_NAME + toko.link
      }`,
    );
  }, [toko]);
  useEffect(() => {
    if (productData && toko) {
      setMessage(
        productData.length === 0
          ? `Hi, ${
              toko.name
            }  baru membuat website baru nih  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }`
          : productData.length === 1
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}   \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }  `
          : productData.length === 2
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}  \n- ${
              productData[1].name
            }  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }  `
          : productData.length >= 3
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}   \n- ${productData[1].name}  \n- ${
              productData[2].name
            }  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }`
          : null,
      );
    }
  }, [productData, toko]);
  useEffect(() => {
    dispatch(TokoActions.getDetailTokoRequest(tokoId));
    dispatch(TokoActions.getTokoAddressRequest(tokoId));
    dispatch(TokoActions.getTokoBankRequest(tokoId));
    dispatch(TokoActions.getKycStatusTokoRequest());
  }, [tokoId]);

  const handleCallNumber = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL(`telprompt:${phoneNumber}`);
    } else {
      Linking.openURL(`tel:${phoneNumber}`);
    }
  };
  const setValueSubmitBank = (purpose, value) => {
    switch (purpose) {
      case 1: {
        setIsAddedBank(value);

        break;
      }
      case 2: {
        setBankIndex(value);
        break;
      }
      default:
        null;
    }
  };
  // useEffect(() => {
  //   if (provinceData != null) {
  //     setProvinceValue(provinceData.provincies[0]);
  //     props.dispatch(
  //       TokoActions.getCityRequest({id: provinceData.provincies[0].id}),
  //     );
  //   }
  // }, [provinceData]);
  // useEffect(() => {
  //   if (cityData != null) {
  //     setCityValue(cityData.cities[0]);
  //   }
  // }, [cityData]);
  // useEffect(() => {
  //   if (districtData != null) {
  //     setDistrictValue(districtData.districts[0]);
  //   }
  // }, [districtData]);
  const handleShowSnackBar = (message) => {
    setSnackBar({show: true, message: message});
  };
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getDetailTokoRequest(tokoId));
    dispatch(TokoActions.getTokoAddressRequest(tokoId));
    dispatch(TokoActions.getTokoBankRequest(tokoId));
    dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
    dispatch(TokoActions.getTokoFinancialRequest(tokoId));

    wait(500).then(() => setRefreshing(false));
  };
  const handleCopyLink = () => {
    Clipboard.setString(Config.TOKO_NAME + toko.link);
    handleShowSnackBar('Link disalin!');
  };
  const openRBSheet = (editSelected) => {
    refRBSheet.current.open();
    setEditSelected(editSelected);
  };
  const handleFormToko = (key, value) => {
    setFormToko({...formToko, [key]: value});
  };
  const handlePickImage = (source) => {
    if (source === 'gallery') {
      ImagePicker.openPicker({
        width: 500,
        height: 500,
        cropping: true,
      }).then((image) => {
        setImagesTemp([
          {
            id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    } else if (source === 'camera') {
      ImagePicker.openCamera({
        width: 500,
        height: 500,
        cropping: true,
      }).then((image) => {
        setImagesTemp([
          {
            id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    }
  };
  const handleLogout = () => {
    setLoadingLogout(true);
    dispatch(AuthActions.deleteAuthTokenUserRequest());
  };
  const reload = () => {
    refRBSheet.current.close();
    dispatch(TokoActions.getDetailTokoRequest(tokoId));
  };
  const handleAddAdressSubmit = () => {
    if (provinceValue === null || provinceValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isProvinceEmpty: true,
      });
    } else if (cityValue === null || cityValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isCityEmpty: true,
      });
    } else if (districtValue === null || districtValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isDistrictEmpty: true,
      });
    } else if (postalCodeValue === null || postalCodeValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isPostalCodeEmpty: true,
      });
    } else if (addressValue === null || addressValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isAddressEmpty: true,
      });
    } else {
      const postData = {
        tokoId: props.tokoId,
        func: {
          isAdded: setIsAddedAddress(),
          isUpdated: setIsUpdatedAddress(),
        },
        addressId: addressIdValue,
        purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
        detail: {
          province_id: provinceValue.id,
          city_id: cityValue.id,
          district_id: districtValue.id,
          postal_code: postalCodeValue,
          address: addressValue.concat(
            detailAddressValue === null ? '' : detailAddressValue,
          ),
        },
      };

      dispatch(TokoActions.postTokoAddressRequest(postData));
    }
  };
  const handleAddBankSubmit = (value) => {
    if (value === 3) {
      const postData = {
        tokoId: props.tokoId,
        rekId: rekId,
        setValueBank: setValueSubmitBank,
        // func: {updateBank: setBankIndex()},
        purpose: 3,
        detail: {
          bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
          holder_name: bankOwnerNameValue,
          rekening_number: bankAccountNumValue,
        },
      };

      dispatch(TokoActions.postTokoBankRequest(postData));
    } else {
      if (bankValue === null || bankValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankEmpty: true,
        });
      } else if (bankAccountNumValue === null || bankAccountNumValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankAccountNumEmpty: true,
        });
      } else if (bankOwnerNameValue === null || bankOwnerNameValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankOwnerEmpty: true,
        });
      } else {
        const postData = {
          tokoId: props.tokoId,
          rekId: rekId,
          setValueBank: setValueSubmitBank,
          // func: {updateBank: setBankIndex()},
          purpose: isAddedBank ? 1 : 2,
          detail: {
            bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
            holder_name: bankOwnerNameValue,
            rekening_number: bankAccountNumValue,
          },
        };

        dispatch(TokoActions.postTokoBankRequest(postData));
      }
    }
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const searchingPostalCode = (searchText) => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerCity(false);
        setOpenPickerProvince(false);
        setOpenPickerDistrict(false);

        setOpenPickerPostalCode(true);
        let filteredData = postalCodeData.postalCode.filter(function (item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setOpenPickerCity(false);
      setOpenPickerProvince(false);
      setOpenPickerDistrict(false);

      setPostalCodeValue(searchText);
    }
  };
  const _tokoAddressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(TokoActions.getCityRequest(item.province_details));
          dispatch(TokoActions.getDistrictRequest(item.city_details));
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: item.city_details.name,
              districtName: item.district_details.name,
            }),
          );
          setAddressIdValue(item.id);
          setProvinceValue(item.province_details);
          setCityValue(item.city_details);
          setDistrictValue(item.district_details);
          setAddressValue(item.address);
          setDetailAddressValue(item.address2);
          setPostalCodeValue(item.postal_code);
          setIsUpdatedAddress(true);
        }}
        delayPressIn={0}
        style={{
          width: '100%',
          borderColor: c.Colors.gray,
          borderWidth: 0.58,
          marginTop: 17,
          paddingLeft: 18,
          paddingRight: 18,
          bottom: 1,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
            marginTop: 20,
          }}>
          {item.province_details.name}
        </Text>

        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.name}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.postal_code}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.fullAddress}
        </Text>
        <View style={{height: 25}} />
        {/* <View
          style={{
            borderRadius: 25,
            marginTop: 15,
            width: '100%',
            height: '50%',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '28%',
              height: '100%',
            }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              initialRegion={{
                latitude: 36.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0,
                longitudeDelta: 0.0,
              }}
              style={{
                width: '100%',
                height: '72%',
                alignSelf: 'center',
              }}>
              <MapView.Marker
                coordinate={{latitude: 37.78825, longitude: -122.4324}}
                title={'title'}
                description={'description'}
              />
            </MapView>
          </View>
          <View
            style={{
              width: '75%',
              height: '80%',
              paddingLeft: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#7b7b7b',
                letterSpacing: 1,
                fontSize: 9,
              }}>
              Jl. Blora No.32, RT.2/RW.6, Dukuh Atas, Menteng, Kec. Menteng,
              Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310
            </Text>
          </View>
        </View>
      */}
      </TouchableOpacity>
    );
  };
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          );
        }}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode
            ? item.postal_code
            : city
            ? item.type + ' ' + item.name
            : item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  const handleUpdateToko = () => {
    if (editSelected === 'delete-toko') {
      setEditSelected(null);
      const data = {
        tokoId: props.tokoId,
        func: {reloadData: reload},
      };
      dispatch(TokoActions.deleteTokoRequest(data));
    } else {
      const {
        isLinkTokoEmpty,
        isNameTokoEmpty,
        isWhatsappEmpty,
      } = tokoValidation;
      const data = {
        tokoId: props.tokoId,
        item:
          editSelected === 'toko'
            ? {
                name: formToko.name,
                link: formToko.link.toLowerCase(),
                whatsapp:
                  formToko.whatsapp.substring(0, 2) == 62
                    ? '0' +
                      formToko.whatsapp.substring(2, formToko.whatsapp.length)
                    : formToko.whatsapp,
              }
            : editSelected === 'category'
            ? {
                business_type_id: formToko.business_type_id,
              }
            : editSelected === 'e-commerce'
            ? {
                tokopedia: formToko.tokopedia,
                shopee: formToko.shopee,
                blibli: formToko.blibli,
              }
            : editSelected === 'socmed'
            ? {
                instagram: formToko.instagram,
                facebook: formToko.facebook,
                line: formToko.line,
                youtube: formToko.youtube,
              }
            : null,
        func: {reloadData: reload},
      };
      setTokoValidation({
        isNameTokoEmpty: !formToko.name ? true : false,
        isLinkTokoEmpty: !formToko.link ? true : false,
        isWhatsappEmpty: !formToko.whatsapp ? true : false,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
        isLineLinkInvalid: !isLinkValid(formToko.line),
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
      if (editSelected === 'e-commerce') {
        if (
          isLinkValid(formToko.tokopedia) &&
          isLinkValid(formToko.shopee) &&
          isLinkValid(formToko.blibli)
        ) {
          setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      } else if (editSelected === 'socmed') {
        if (
          isLinkValid(formToko.instagram) &&
          isLinkValid(formToko.facebook) &&
          isLinkValid(formToko.line) &&
          isLinkValid(formToko.youtube)
        ) {
          setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      } else {
        if (formToko.name && formToko.link && formToko.whatsapp) {
          setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      }
    }
  };
  const _renderBankItem = ({index, item}) => {
    if (bankIndex === index) {
      return (
        <View
          style={{
            width: '100%',

            borderColor: c.Colors.gray,
            borderWidth: 0.58,
            marginTop: 17,
            paddingLeft: 18,
            paddingRight: 18,
          }}>
          <TouchableOpacity
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());
              setBankIndex(-1);
            }}
            delayPressIn={0}
            style={{position: 'absolute', right: 8, top: 8}}>
            <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1.2,
              marginTop: 25,
            }}>
            Nama Bank
          </Text>
          <TouchableOpacity
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());

              if (bank) {
                setOpenPickerBank(false);
              } else {
                setOpenPickerBank(true);
              }
            }}
            delayPressIn={0}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankEmpty
                ? 'red'
                : bankNameValue.name == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <Text
              style={{
                bottom: 3,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
              }}>
              {bankNameValue.name != null || bankNameValue.name != ''
                ? bankNameValue.name
                : 'Nama Bank'}
            </Text>
            <MaterialCommunityIcons
              style={{bottom: 5}}
              name={!bank ? 'menu-down' : 'menu-up'}
              size={25}
              color="#1a69d5"
            />
          </TouchableOpacity>
          {/* {addedBankFormValidation.isBankEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib memilih Nama Bank
            </Text>
          )} */}
          {bank && (
            <FlatList
              nestedScrollEnabled
              style={{height: 300}}
              renderItem={_addressRenderItem}
              data={bankData.banks}
            />
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                ? 'red'
                : bankAccountNumValue == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              keyboardType={'number-pad'}
              onChangeText={(text) => {
                dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankAccountNumEmpty: false,
                });
                setBankAccountNumValue(text);
              }}
              value={bankAccountNumValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nomor Rekening'}
            />
          </View>
          {addedBankFormValidation.isBankAccountNumEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nomor rekening
            </Text>
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                ? 'red'
                : bankOwnerNameValue == null || bankOwnerNameValue == ''
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              onChangeText={(text) => {
                dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankOwnerEmpty: false,
                });

                setBankOwnerNameValue(text);
              }}
              value={bankOwnerNameValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nama Pemilik'}
            />
          </View>
          {addedBankFormValidation.isBankOwnerEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nama pemilik rekening
            </Text>
          )}

          <View style={{height: 14}} />
          {isErrorTokoBankAlreadyRegistered && (
            <Text
              style={[
                c.Styles.txtInputDesc,
                {color: 'red', alignSelf: 'center', top: 15},
              ]}>
              {isErrorTokoBankAlreadyRegistered.message}
            </Text>
          )}
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <View style={{width: '23%'}}>
              <Button
                style={{
                  height: 55,
                  width: 55,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: '#484848',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}
                onPress={() => handleAddBankSubmit(3)}>
                <Image
                  style={{width: 25, height: 30}}
                  source={Images.trashIcon}
                />
              </Button>
            </View>
            <TouchableOpacity
              style={{
                top: 1,
                borderRadius: 25,
                height: 50,
                alignSelf: 'center',
                width: '75%',
                backgroundColor: '#1a69d5',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              delayPressIn={0}
              onPress={handleAddBankSubmit}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: '#ffffff',
                  letterSpacing: 1,
                  fontSize: 14,
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{height: 25}} />
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          delayPressIn={0}
          style={{
            width: '100%',
            height: 100,
            borderColor: '#c9c6c6',
            borderWidth: 0.5,
            marginTop: 14,
            justifyContent: 'center',
            paddingLeft: 25,
            bottom: 5,
          }}>
          <TouchableWithoutFeedback>
            <TouchableOpacity
              //kylie
              onPress={() => {
                const dataPost = {tokoId: props.tokoId, bankId: item.id};
                dispatch(
                  TokoActions.updateDefaultRekeningTokoRequest(dataPost),
                );
              }}
              delayPressIn={0}
              style={{
                marginTop: 8,
                marginRight: 8,
                position: 'absolute',
                right: 0,
                top: 0,
              }}>
              <Image
                style={{width: 20, height: 20}}
                source={
                  item.default ? Images.starBlueIcon : Images.starWhiteIcon
                }
              />
              {/* <MaterialCommunityIcons name="star" size={20} color={item.default?c.Colors.mainBlue:'#f4f4f4'} /> */}
            </TouchableOpacity>
          </TouchableWithoutFeedback>
          <TouchableOpacity
            style={{top: 9, width: '95%'}}
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());

              setIsAddedBank(false);
              setRekId(item.id);
              setBankIndex(index);
              setIsUpdatedBank(true);
              setBankOwnerNameValue(item.holder_name);
              setBankAccountNumValue(item.rekening_number);
              setBankNameValue(item.bank_details);
              setBankValue(item.bank_details.name);
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.bank_details.name === null || item.bank_details.name === ''
                ? '-'
                : item.bank_details.name}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.rekening_number === null || item.rekening_number === ''
                ? '-'
                : item.rekening_number}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.holder_name === null || item.holder_name === ''
                ? '-'
                : item.holder_name}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }
  };
  const urlencode = (str) => {
    str = (str + '').toString();

    // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
    // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
    const url = encodeURIComponent(str)
      .split('!')
      .join('%21')
      .replace("'", '%27')
      .split('(')
      .join('%28')
      .split(')')
      .join('%29')
      .split('*')
      .join('%2A')
      .split('%26')
      .join('&')
      .split('%20')
      .join('+');
    return url;
  };
  const isLinkValid = (link) => {
    if (typeof link === 'string' && link.length) {
      if (
        link.toLowerCase().includes('http') ||
        link.toLowerCase().includes('www')
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };
  const handleSelectedBusinessType = (item) => {
    setFormToko({
      ...formToko,
      business_type_id: item.id,
      business_type: item,
    });
  };
  const renderEditMerchant = () => {
    const {
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
      isTokpedLinkInvalid,
      isShopeeLinkInvalid,
      isBlibliLinkInvalid,
      isInstagramLinkInvalid,
      isFacebookLinkInvalid,
      isLineLinkInvalid,
      isYoutubeLinkInvalid,
    } = tokoValidation;
    switch (editSelected) {
      case 'toko':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={[c.Styles.marginTopFive]}>
                <View style={c.Styles.txtInputWithIcon}>
                  <View style={c.Styles.marginRightTen}>
                    <c.Image
                      source={require('../../../../assets/image/toko.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      autoCapitalize="words"
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Nama Toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.name}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isNameTokoEmpty: formToko.name ? false : true,
                        })
                      }
                      onChangeText={(text) => {
                        setFormToko({
                          ...formToko,
                          name: text,
                          link: text.split(' ').join('-'),
                        });
                      }}
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  Nama toko di website kamu
                </Text>
                {isNameTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Nama toko harus diisi
                  </Text>
                )}
              </View>
              <View style={[c.Styles.marginTopTen]}>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/at.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Link Toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.link}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isLinkTokoEmpty: formToko.link ? false : true,
                        })
                      }
                      onChangeText={(value) =>
                        handleFormToko('link', value.split(' ').join('-'))
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  tokook.id/
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 14,
                      letterSpacing: 1,
                      textTransform: 'lowercase',
                    }}>
                    {formToko.link !== null ? formToko.link : 'link-toko'}
                  </Text>
                </Text>
                {isLinkTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Link toko harus diisi
                  </Text>
                )}
              </View>
              <View style={[c.Styles.marginTopTen]}>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/whatsapp.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      editable={false}
                      keyboardType="numeric"
                      // style={c.Styles.txtInputCreateMerchant}
                      placeholder="Nomor Whatsapp"
                      editable={false}
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.whatsapp}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isWhatsappEmpty: formToko.whatsapp ? false : true,
                        })
                      }
                      onChangeText={(value) =>
                        handleFormToko('whatsapp', value)
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  Agar pembeli bisa menghubungi kamu
                </Text>
                {isWhatsappEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Nomor whatsapp harus diisi
                  </Text>
                )}
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'category':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View
                style={[
                  c.Styles.flexRow,
                  c.Styles.viewheaderDay,
                  c.Styles.justifyspaceBetween,
                ]}>
                <View>
                  <View
                    style={[
                      c.Styles.paddingTen,
                      {
                        borderRadius: 100,
                        backgroundColor: c.Colors.blueWhite,
                      },
                    ]}>
                    <c.Image
                      source={
                        formToko.business_type
                          ? {
                              uri:
                                formToko.business_type &&
                                formToko.business_type.icon,
                            }
                          : Images.noImg
                      }
                      style={c.Styles.imgaddNotificationToTransaction}
                    />
                  </View>
                </View>
                <TouchableOpacity style={{width: '80%'}}>
                  <View style={[c.Styles.borderBottom, {marginLeft: 5}]}>
                    <TextInput
                      onChangeText={(value) =>
                        handleFormToko('inputOtherBusinessType', value)
                      }
                      ref={refInputBusinessType}
                      editable={false}
                      placeholder="Masukkan jenis usaha kamu"
                      style={[c.Styles.txtcategoryNotification]}>
                      {formToko.business_type_id !== 12
                        ? formToko.business_type
                          ? formToko.business_type.name
                          : null
                        : // : formToko.inputOtherBusinessType}
                          'Lainnya'}
                    </TextInput>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={[c.Styles.viewTokoStep2WrapCategory]}>
                {listBusinessType.length
                  ? listBusinessType.map((item) => {
                      return (
                        <TouchableOpacity
                          onPress={() => handleSelectedBusinessType(item)}
                          key={item.id}
                          style={[
                            c.Styles.justifyCenter,
                            c.Styles.alignItemsCenter,
                            c.Styles.marginTopFifteen,
                            c.Styles.marginBottomFifteen,
                            {width: '33.33%'},
                          ]}>
                          <View
                            style={[
                              c.Styles.paddingTen,
                              {
                                borderRadius: 100,
                                backgroundColor: c.Colors.blueWhite,
                              },
                            ]}>
                            <c.Image
                              source={
                                item.icon ? {uri: item.icon} : Images.noImg
                              }
                              style={c.Styles.imgaddNotificationToTransaction}
                            />
                          </View>
                          <Text
                            style={{
                              marginTop: 8,
                              textAlign: 'center',
                              fontSize: 12,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      );
                    })
                  : null}
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'toko-address':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Alamat Toko
              </Text>
              {tokoAddressData.listAddress.length === 0 && !isAddedAddress && (
                <TouchableOpacity
                  onPress={() => setIsAddedAddress(true)}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Alamat
                  </Text>
                </TouchableOpacity>
              )}
              {isAddedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && provinceData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                    {/* bb */}
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && cityData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && districtData != null && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        style={{height: 300}}
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {isUpdatedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsUpdatedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        style={{height: 300}}
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {!isUpdatedAddress && (
                <FlatList
                  data={tokoAddressData.listAddress.slice(0, 1)}
                  renderItem={_tokoAddressRenderItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      case 'rek-bank':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Rekening Bank
              </Text>

              {isAddedBank ? (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedBank(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 25,
                    }}>
                    Nama Bank
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      dispatch(TokoActions.postTokoBankReset());

                      if (bank) {
                        setOpenPickerBank(false);
                      } else {
                        setOpenPickerBank(true);
                      }
                    }}
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankEmpty
                        ? 'red'
                        : bankValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {bankValue != null ? bankValue.name : 'Nama Bank'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!bank ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedBankFormValidation.isBankEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Nama Bank
                    </Text>
                  )}
                  {bank && (
                    <FlatList
                      nestedScrollEnabled
                      style={{height: 300}}
                      renderItem={_addressRenderItem}
                      data={bankData.banks}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                        ? 'red'
                        : bankAccountNumValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      keyboardType={'number-pad'}
                      onChangeText={(text) => {
                        dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankAccountNumEmpty: false,
                        });
                        setBankAccountNumValue(text);
                      }}
                      value={bankAccountNumValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nomor Rekening'}
                    />
                  </View>
                  {addedBankFormValidation.isBankAccountNumEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nomor rekening
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                        ? 'red'
                        : bankOwnerNameValue == null || bankOwnerNameValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => {
                        dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankOwnerEmpty: false,
                        });

                        setBankOwnerNameValue(text);
                      }}
                      value={bankOwnerNameValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nama Pemilik'}
                    />
                  </View>
                  {addedBankFormValidation.isBankOwnerEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nama pemilik rekening
                    </Text>
                  )}

                  <View style={{height: 14}} />
                  {isErrorTokoBankAlreadyRegistered && (
                    <Text
                      style={[
                        c.Styles.txtInputDesc,
                        {color: 'red', alignSelf: 'center', top: 15},
                      ]}>
                      {isErrorTokoBankAlreadyRegistered.message}
                    </Text>
                  )}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 30,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddBankSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    setBankAccountNumValue(null);
                    setBankValue(null);
                    setBankOwnerNameValue(null);
                    setIsAddedBank(true);
                    setIsUpdatedBank(false);
                    setBankIndex(-1);
                    dispatch(TokoActions.postTokoBankReset());
                  }}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,

                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Rekening
                  </Text>
                </TouchableOpacity>
              )}
              {tokoBankData != null && (
                <FlatList
                  extraData={useState}
                  nestedScrollEnabled
                  data={tokoBankData.listRekening}
                  style={{marginTop: 12}}
                  renderItem={_renderBankItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      case 'e-commerce':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={c.Styles.viewCreateMerchantSectionWrapper}>
                <Text style={c.Styles.txtBold}>E-Commerce</Text>
                <Text style={c.Styles.lineHeightTwenty}>
                  Untuk customer kamu jika ingin membeli dan membayar melalui
                  toko kamu lewat e-commerce.
                </Text>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/toped-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.tokopedia.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.tokopedia}
                      onChangeText={(value) =>
                        handleFormToko('tokopedia', value)
                      }
                    />
                  </View>
                </View>
                {isTokpedLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/shopee-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.shopee.co.id/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.shopee}
                      onChangeText={(value) => handleFormToko('shopee', value)}
                    />
                  </View>
                  {isShopeeLinkInvalid ? (
                    <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                      <Text style={c.Styles.txtInputMerchantErrors}>
                        {errorLinkMessage}
                      </Text>
                    </View>
                  ) : null}
                </View>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/blibli-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.blibli.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.blibli}
                      onChangeText={(value) => handleFormToko('blibli', value)}
                    />
                  </View>
                </View>
                {isBlibliLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'socmed':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={c.Styles.viewCreateMerchantSectionWrapper}>
                <Text style={c.Styles.txtBold}>Social Media</Text>
                <Text>Supaya calon pembeli lebih mengenal kamu.</Text>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/instagram.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.instagram.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.instagram}
                      onChangeText={(value) =>
                        handleFormToko('instagram', value)
                      }
                    />
                  </View>
                </View>
                {isInstagramLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/facebook.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.facebook.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.facebook}
                      onChangeText={(value) =>
                        handleFormToko('facebook', value)
                      }
                    />
                  </View>
                </View>
                {isFacebookLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/line.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.line.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.line}
                      onChangeText={(value) => handleFormToko('line', value)}
                    />
                  </View>
                </View>
                {isLineLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../../assets/image/youtube.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.youtube.com/user/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.youtube}
                      onChangeText={(value) => handleFormToko('youtube', value)}
                    />
                  </View>
                </View>
                {isYoutubeLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'delete-toko':
        return (
          <View style={[c.Styles.justifyCenter, c.Styles.alignItemsCenter]}>
            <c.Image
              source={require('../../../../assets/image/store.png')}
              style={{width: 80, height: 80, marginVertical: 20}}
            />
            <Text
              style={{
                fontSize: 12,
                fontFamily: 'NotoSans-Regular',
                letterSpacing: 0.91,
                color: '#4a4a4a',
              }}>
              Apakah anda yakin ingin menghapus toko ini?
            </Text>
          </View>
        );
        break;
      default:
        return (
          <View>
            <Spinner color={c.Colors.mainBlue} />
          </View>
        );
        break;
    }
  };
  const handleCleanState = () => {
    dispatch(TokoActions.postTokoBankReset());
    dispatch(TokoActions.postTokoAddressReset());

    // props.dispatch(TokoActions.getCityReset());
    setPostalCodeValue(null);
    setProvinceValue(null);
    setCityValue(null);
    setOpenPickerProvince(false);
    setOpenPickerCity(false);
    setOpenPickerDistrict(false);
    setOpenPickerPostalCode(false);
    setDistrictValue(null);
    setDetailAddressValue(null);
    setAddressIdValue(0);
    setAddressValue(null);
    setIsAddedAddress(false), setIsUpdatedAddress(false);
    setAddedAddressFormValidation(initialValidation);
    setIsAddedBank(false);
    setOpenPickerBank(false);
    // setBankNameValue(null);
    setBankOwnerNameValue(null);
    setBankIndex(-1);
    setBankValue(null);
    // setBankNameValue(null)
    setIsUpdatedBank(false);
  };
  return (
    <Container>
      <ScrollView
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        <View>
          <View style={styles.viewHeader}>
            {getKycData !== null ? (
              <View style={{paddingLeft: 5}}>
                <Text style={styles.txtFullname}>
                  {getKycData !== null ? getKycData.fullname : ''}
                </Text>
              </View>
            ) : (
              <TouchableOpacity
                delayPressIn={0}
                onPress={() => navigation.navigate('FinanceKYC')}
                style={{flexDirection: 'row', paddingLeft: 5}}>
                <View>
                  <Text style={styles.txtFullname}>Masukkan Nama kamu</Text>
                </View>
                <View style={{marginLeft: 10, justifyContent: 'center'}}>
                  <SimpleLineIcons name="arrow-right" size={15} color="#fff" />
                </View>
              </TouchableOpacity>
            )}
            <View style={styles.viewItem}>
              <View style={{flexDirection: 'row', padding: 5}}>
                <View
                  style={{
                    width: '8%',
                  }}>
                  <Foundation name={'mail'} size={18} color={'#fff'} />
                </View>
                <Text style={styles.txtitemHeader}>{userData.data.email}</Text>
              </View>

              {userData.data.is_phone_verified ? (
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      paddingLeft: 5,
                      width: '9%',
                    }}>
                    <FontAwesome name={'phone'} size={18} color={'#fff'} />
                  </View>
                  <Text style={styles.txtitemHeader}>
                    {userData.data.phone}
                  </Text>
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => refVerifwa.current.open()}
                  delayPressIn={0}
                  style={{width: '65%'}}>
                  <View style={styles.viewWhatsapp}>
                    <View style={{width: '12%'}}>
                      <MaterialCommunityIcons
                        name="whatsapp"
                        size={18}
                        color={'#fff'}
                      />
                    </View>

                    <Text style={styles.txtWhatsapp}>
                      Verifikasi Nomor Whatsapp
                    </Text>
                    <View style={{justifyContent: 'center', marginLeft: 5}}>
                      <SimpleLineIcons
                        name="arrow-right"
                        size={8}
                        color="#fff"
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            </View>

            <Pressable
              onPress={() => navigation.navigate('FinanceScreen')}
              delayPressIn={0}
              style={{
                width: '100%',
                position: 'absolute',
                bottom: -10,
                alignSelf: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#36beff',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 4,
                  },
                  width: '100%',
                  alignSelf: 'center',
                  padding: 15,
                  shadowOpacity: 0.3,
                  shadowRadius: 4.65,
                  elevation: 8,
                  borderRadius: 4,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      width: '90%',
                      flexDirection: 'row',
                    }}>
                    <View>
                      <Image
                        source={Images.walletImg}
                        style={{width: 50, height: 50}}
                      />
                    </View>
                    <View style={{marginLeft: 20, justifyContent: 'center'}}>
                      <View>
                        <Text
                          style={{
                            letterSpacing: 1.145,
                            fontFamily: 'NotoSans-Regular',
                            fontSize: 12,
                            color: '#fff',
                          }}>
                          Hasil Penjualan Kamu
                        </Text>
                      </View>
                      <View>
                        <Text
                          style={{
                            letterSpacing: 1.72,
                            fontFamily: 'NotoSans-Bold',
                            fontSize: 18,
                            color: '#fff',
                          }}>
                          {func.rupiah(
                            getFinancialEarning != null
                              ? getFinancialEarning.earnings.total
                              : 0,
                          )}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={c.Styles.justifyCenter}>
                    <SimpleLineIcons
                      name="arrow-right"
                      size={25}
                      color="#fff"
                    />
                  </View>
                </View>
              </View>
            </Pressable>
          </View>
       
       

          <View>
            <View style={styles.viewlistBantuan}>
              <Text style={styles.txtHelp}>Bantuan</Text>
            </View>
            <TouchableOpacity
            delayPressIn={0}
            onPress={() =>
              Linking.openURL(
                'https://docs.google.com/forms/d/e/1FAIpQLSd2ifLd5fwwDGkZ30_GzPP9radSuKN2nbVOQ5NswjNmFyB7HA/viewform?vc=0&c=0&w=1&flr=0&gxids=7628',
                )
            }
            style={styles.viewlistItem}>
            <View style={{width: '10%'}}>
              <MaterialCommunityIcons
                name={'form-select'}
                size={30}
                color={c.Colors.mainBlue}
              />
            </View>

            <Text style={styles.txtlistItem}>Klaim Komisi</Text>
          </TouchableOpacity>
     
            <TouchableOpacity
              delayPressIn={0}
              onPress={() =>
                Linking.openURL(
                  'https://m.youtube.com/channel/UCc59aag1a-4NBegrByx-vXg',
                )
              }
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <MaterialCommunityIcons
                  name="youtube"
                  size={30}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Tutorial</Text>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => Linking.openURL(bantuanURL)}
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <MaterialIcons
                  name="help-outline"
                  size={30}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Cara Penggunaan</Text>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() =>
                Linking.openURL(
                  toko.name === undefined
                    ? `whatsapp://send?text=${`Halo admin toko OK, saya pengguna baru Toko OK, ingin bertanya `}&phone=${phoneNumber}`
                    : `whatsapp://send?text=${`Halo admin toko OK, saya dari toko ${toko.name}, ingin bertanya `}&phone=${phoneNumber}`,
                ).catch(() =>
                  Linking.openURL(
                    `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                  ),
                )
              }
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <MaterialCommunityIcons
                  name="whatsapp"
                  size={30}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Hubungi Kami</Text>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={handleCallNumber}
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <MaterialCommunityIcons
                  name="phone"
                  size={30}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Telepon Kami</Text>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() =>
                Linking.openURL(
                  'https://docs.google.com/forms/d/14BkUqaQwLvNjNOUBzVyeCXtmpH_jksqRaP27E7lrZV4/edit',
                )
              }
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <MaterialCommunityIcons
                  name="send"
                  size={28}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Kirim Saran</Text>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={
                () =>
                  Linking.openURL(
                    `https://play.google.com/store/apps/details?id=com.tokook.id`,
                  ).catch(() =>
                    Linking.openURL(
                      `https://play.google.com/store/apps/details?id=com.tokook.id`,
                    ),
                  )
                // InAppReview.RequestInAppReview()
              }
              style={styles.viewlistItem}>
              <View style={{width: '10%'}}>
                <SimpleLineIcons
                  name="star"
                  size={25}
                  color={c.Colors.mainBlue}
                />
              </View>

              <Text style={styles.txtlistItem}>Rate Kita</Text>
            </TouchableOpacity>

            <View style={styles.viewVersion}>
              <Text style={styles.txtVersion}>{'V' + version}</Text>
            </View>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() =>
                Alert.alert('', 'Apakah anda ingin pindah akun ?', [
                  {
                    text: 'NO',
                    onPress: () => console.log('No Pressed'),
                    style: 'cancel',
                  },
                  {text: 'YES', onPress: () => handleLogout()},
                ])
              }
              style={styles.viewLogout}>
              <View style={{width: '10%'}}>
                <MaterialCommunityIcons
                  name="logout"
                  size={25}
                  color={'white'}
                />
              </View>

              <Text style={[c.Styles.marginLeftTen, c.Styles.txtListAdmin]}>
                Pindah Akun
              </Text>
            </TouchableOpacity>
          </View>
          <RBSheet
            onClose={handleCleanState}
            ref={refVerifwa}
            closeOnDragDown={true}
            closeOnPressMask={true}
            dragFromTopOnly={true}
            height={Dimensions.get('window').height * 0.6}
            openDuration={250}
            customStyles={{
              container: {
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
              },
              draggableIcon: {
                backgroundColor: '#D8D8D8',
                width: 59.5,
              },
            }}>
            <Content
              contentContainerStyle={{
                paddingHorizontal: 20,
                height: '100%',
              }}
              keyboardShouldPersistTaps={'always'}>
              <View>
                <View
                  style={[
                    c.Styles.flexRow,
                    c.Styles.alignItemsCenter,
                    {marginTop: 20},
                  ]}>
                  <Text style={c.Styles.txtverifWA}>Verifikasi Whatsapp</Text>
                </View>
                <View style={styles.justifyCenter}>
                  <View style={styles.viewinputwaNumber}>
                    <View style={c.Styles.flexRow}>
                      <View style={c.Styles.justifyCenter}>
                        <c.Image
                          source={Images.waIcon}
                          style={c.Styles.textInputIcon}
                        />
                      </View>
                      <TextInput
                        keyboardType={'number-pad'}
                        style={styles.txtinputwaNumber}
                        onChangeText={(text) => {
                          setwaNumber(text);
                          setErrMsg(null);
                        }}
                        placeholderTextColor={'#bdbdbd'}
                        placeholder={'Nomor Whatsapp'}
                        value={wanumber}
                      />
                      <TouchableOpacity
                        style={[c.Styles.justifyCenter]}
                        onPress={timeLeft > 0 ? null : handlegetOtp}
                        disabled={props.isFetchingOtp}>
                        {getOtpWa.fetching ? (
                          <View
                            style={{
                              width: 50,
                              alignSelf: 'center',
                              alignItems: 'center',
                            }}>
                            <Spinner size={18} color={c.Colors.mainBlue} />
                          </View>
                        ) : (
                          <Text
                            style={[
                              styles.txtkirimOtp,
                              c.Styles.alignCenter,
                              {opacity: timeLeft === 0 ? 1 : 0.4},
                            ]}>
                            Kirim OTP
                          </Text>
                        )}
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.txtbottomwaNumber}>
                      Agar pembeli bisa menghubungi kamu
                    </Text>
                  </View>
                  <View
                    style={[
                      c.Styles.alignCenter,
                      {marginTop: '10%', height: 100},
                    ]}>
                    {getOtpWa.success ? (
                      <View>
                        <c.PinVerify
                          autoFocus={true}
                          cellStyle={styles.cellStyle}
                          cellStyleFocused={styles.cellStyleFocused}
                          textStyle={styles.txtCellStyle}
                          textStyleFocused={styles.textStyleFocused}
                          value={code}
                          editable={getOtpWa.success ? true : false}
                          onTextChange={(code) => setCode(code)}
                        />
                        <View
                          style={[
                            {
                              marginTop: '10%',
                              flexDirection: 'row',
                              justifyContent: 'space-around',
                            },
                          ]}>
                          <TouchableOpacity
                            style={c.Styles.justifyCenter}
                            onPress={handlegetOtp}
                            disabled={timeLeft !== 0 ? true : false}>
                            <Text
                              style={[
                                styles.txtresendOtp,
                                {opacity: timeLeft === 0 ? 1 : 0.4},
                              ]}>
                              {' '}
                              Kirim Ulang OTP
                            </Text>
                          </TouchableOpacity>

                          <Text style={styles.txttimeLeft}>
                            00:{timeLeft < 10 ? '0' : ''}
                            {timeLeft}
                          </Text>
                        </View>
                      </View>
                    ) : null}
                  </View>

                  <View
                    style={{
                      top: 30,
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      //onPress={handleResendOtp}
                      disabled={props.isFetchingOtp}>
                      <Text
                        style={{
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          opacity: timeLeft === 0 ? 1 : 0.4,
                          color: 'white',
                          marginTop: 2,
                        }}>
                        {' '}
                        Kirim Ulang
                      </Text>
                    </TouchableOpacity>

                    <Text
                      style={{
                        marginLeft: 6,
                        fontFamily: 'NotoSans-ExtraBold',
                        color: 'white',
                        fontSize: 15,
                        opacity: 1,
                      }}>
                      00:{timeLeft < 10 ? '0' : ''}
                      {timeLeft}
                    </Text>
                  </View>
                </View>
                <Button
                  disabled={code.length < 4 ? true : false}
                  onPress={handlewhatsappVerification}
                  style={[
                    c.Styles.btnPrimary,
                    {
                      marginTop: '10%',
                      marginBottom: '10%',
                      backgroundColor:
                        code.length < 4 ? '#c9c6c6' : c.Colors.mainBlue,
                    },
                  ]}>
                  {loadingStep ? (
                    <Spinner size={18} color="#fff" />
                  ) : (
                    <Text style={c.Styles.txtCreateMerchantBtn}>
                      Verifikasi
                    </Text>
                  )}
                </Button>
              </View>
            </Content>
          </RBSheet>
        </View>
      </ScrollView>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    getFinancialEarning: state.toko.tokoFinancialData.data,
    getToko: state.toko.tokoData.data,
    isErrorTokoBankAlreadyRegistered: state.toko.postTokoBankData.error,
    tokoBankData: state.toko.tokoBankData.data,
    bankData: state.toko.bankData.data,
    tokoAddressData: state.toko.tokoAddressData.data,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,

    productData: state.toko.productTokoData.data
      ? state.toko.productTokoData.data.listProduct
        ? state.toko.productTokoData.data.listProduct
        : null
      : null,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    isFetchingLogout: state.auth.deleteAuthToken.fetching,
    getToko: state.toko.tokoData.data,
    toko: state.toko.detailTokoData.data
      ? state.toko.detailTokoData.data.toko
        ? state.toko.detailTokoData.data.toko
        : {}
      : {},
    listBusinessType: state.toko.getBusinessTypeData.data
      ? state.toko.getBusinessTypeData.data.businessType
        ? state.toko.getBusinessTypeData.data.businessType.length
          ? state.toko.getBusinessTypeData.data.businessType
          : []
        : []
      : [],
    tokoAddress:
      state.toko.tokoAddressData.data !== null
        ? state.toko.tokoAddressData.data.length
          ? state.toko.tokoAddressData.data.length !== 0
          : null
        : null,
    userData: state.auth.userData,
    getKycStatusData: state.toko.getKycStatusTokoData.data,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    getOtpWa: state.auth.getOtpPostData,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(index);
