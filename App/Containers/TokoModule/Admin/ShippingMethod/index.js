import React, {useEffect, useRef, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
  Image,
  BackHandler,
  FlatList,
  Switch,
  ActivityIndicator,
  TouchableWithoutFeedback,
  RefreshControl,
} from 'react-native';
import moment from 'moment';

import {
  Button,
  Spinner,
  Card,
  Container,
  Tab,
  Tabs,
  TabHeading,
  Content,
} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Clipboard from '@react-native-community/clipboard';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import TokoActions from '../../../../Redux/TokoRedux';
import CheckBox from '@react-native-community/checkbox';
import {Images, Colors} from '../../../../Themes';
import * as func from '../../../../Helpers/Utils';

import * as c from '../../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import DraggableFlatList from 'react-native-draggable-flatlist';

const ShippingMethodScreen = (props) => {
  const {
    navigation,
    dispatch,
    supplierData,
    tokoOrderPrimary,
    ownerTokoCourierData,
    tokoId,
    ownerTokoCourierFetching,
  } = props;
  console.log('props', props);
  const getOwnerTokoCourierData =
    ownerTokoCourierData === null ? [] : ownerTokoCourierData.courierList;
  const [tabIndex, setTabIndex] = useState(0);
  const [isCOD, setIsCOD] = useState(true);
  const [dateIndex, setDateIndex] = useState(-1);
  const [listIndex, setListIndex] = useState(-1);
  const [listCourier, setListCourier] = useState(null);
  useEffect(() => {
    const backAction = () => {
      dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
      navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  useEffect(() => {
    if (getOwnerTokoCourierData) {
      setListCourier(getOwnerTokoCourierData);
    }
  }, [getOwnerTokoCourierData]);
  useEffect(() => {
    dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
  }, []);
  const filterCODData =
    listCourier != null ? listCourier.filter((x) => x.id === 12) : [];
  const filterWithoutCODData =
    listCourier != null ? listCourier.filter((x) => x.id != 12) : [];

  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity>
        <View
          style={{
            padding: 15,
          }}>
          <TouchableOpacity
            disabled
            delayPressIn={0}
            onPress={() =>
              setListIndex(
                index != listIndex ? index : index === listIndex ? -1 : index,
              )
            }
            style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
            {/* <View style={{width: '10%'}}>
              <Switch
                // onValueChange={val => this.setStatus(item.id, val, item)}

                style={
                  item.is_active === false && {marginLeft: 8, width: '100%'}
                }
                value={item.is_active}
                trackColor={{false: 'rgb(155, 155, 155)', true: '#B6D4F7'}}
                thumbColor={item.is_active === false ? '#EDECED' : '#598FDC'}
              />
            </View> */}

            <Card
              style={{
                width: 80,

                padding: 13,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {item.image === null ? (
                <Image
                  style={{width: 50, height: 50}}
                  source={Images.peopleCarrier}
                />
              ) : (
                <Image
                  style={{width: 50, height: 50}}
                  source={{uri: item.image}}
                />
              )}
            </Card>
            <View style={{width: '70%', flexDirection: 'column', padding: 5}}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: '#4a4a4a',
                  letterSpacing: 1.05,
                  marginLeft: 15,
                  fontSize: 11,
                }}>
                {item.name}
              </Text>
              {/* <Text
                style={{
                  fontSize: 9,
                  fontFamily: 'NotoSans',
                  color: Colors.mainBlue,
                  letterSpacing: 1.1,
                  marginLeft: 15,
                  margin: 5,
                }}>
                {item.desc}
              </Text> */}
            </View>
          </TouchableOpacity>
          <View
            style={{
              width: '100%',
              borderWidth: 0.4,
              borderColor: '#c9c6c6',
              marginTop: 15,
              padding: 12,
            }}>
            {item.services &&
              item.services.map((data, index) => {
                return (
                  <_renderShippingMethDetailItem
                    key={index}
                    courier_code={item.code}
                    parent_id={item.id}
                    index={index}
                    item={data}
                    itemParent={item}
                  />
                );
              })}
          </View>
          {listIndex === index &&
            item.details &&
            item.details.map((data, index) => {
              return <_renderShippingMethDetailItem key={index} item={data} />;
            })}
        </View>
      </TouchableOpacity>
    );
  };
  const handlingOwnerTokoCourierStatus = (
    newValue,
    item,
    index,
    courier_code,
    parent_id,
    itemParent,
  ) => {
    if (newValue) {
      const dataPost = {
        tokoId: tokoId,
        purpose: 1,
        detail: {
          courier_id: item.courier_id,
          courier_code: courier_code,
          service: item.service,
        },
      };
      dispatch(TokoActions.handlingOwnerTokoCourierStatusRequest(dataPost));
    } else {
      const dataPost = {
        tokoId: tokoId,
        purpose: 2,
        detail: {
          courier_id: item.courier_id,
          courier_code: courier_code,
          service: item.service,
        },
      };
      dispatch(TokoActions.handlingOwnerTokoCourierStatusRequest(dataPost));
    }
    var temp = [...listCourier];
    let statusValue = null;
    let childValue = null;
    let newVal = null;

    temp.map((t) => {
      if (t.id == parent_id) {
        childValue = t.services.map((e) => {
          if (e.id == item.id) {
            newVal = {...e, active: newValue};
          }

          return e;
          // return {...e, ...newVal};
        });
        var newData = [...childValue, newVal];

        const result = Array.from(
          newData.reduce((a, o) => a.set(o.id, o), new Map()).values(),
        );
        // console.log('cek temp data kuy index', t.services[index]);
        const merging = t;
        statusValue = {...merging, services: result};
        return statusValue;
      }
    });
    temp.push(statusValue);
    const result = Object.values(
      temp.reduce((acc, cur) => Object.assign(acc, {[cur.id]: cur}), {}),
    );
    setListCourier(result);
  };

  const _renderShippingMethDetailItem = ({
    item,
    index,
    courier_code,
    parent_id,
    itemParent,
  }) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: '#ffffff',
        }}>
        <View style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}>
          <CheckBox
            tintColors={{true: Colors.mainBlue, false: '#979797'}}
            onTintColor="red"
            onCheckColor="black"
            style={{alignSelf: 'center'}}
            disabled={false}
            value={item.active ? true : false}
            onValueChange={(newValue) =>
              handlingOwnerTokoCourierStatus(
                newValue,
                item,
                index,
                courier_code,
                parent_id,
                itemParent,
              )
            }
          />
          <Text
            style={{
              margin: 8,
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 0.5,
              color: '#4a4a4a',
              width: '67%',
            }}>
            {item.service}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans-Regular',
              fontSize: 12,
              letterSpacing: 0.5,
              color: '#4a4a4a',
              position: 'absolute',
              right: 10,
            }}>
            {item.etd === null || item.etd === '' ? '0' : item.etd} Hari
          </Text>
        </View>

        {/* <FlatList data={item} renderItem={_renderShippingListItem} /> */}
        {/* {_handleDetailPaymentMethStatus(item)} */}
      </View>
    );
  };
  const _handleDetailPaymentMethStatus = (item) => {
    switch (item.id) {
      case 1:
        {
          return (
            <FlatList
              data={item.shipping_type}
              renderItem={_renderShippingListItem}
              keyExtractor={(item, index) => index.toString()}
            />
          );
        }
        break;
      case 2:
        {
          return (
            <View style={{padding: 15}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{width: '10%', marginLeft: 9}}>
                  <Switch
                    // onValueChange={val => this.setStatus(item.id, val, item)}

                    style={
                      item.avail_COD === false && {marginLeft: 8, width: '100%'}
                    }
                    value={item.avail_COD}
                    trackColor={{false: 'rgb(155, 155, 155)', true: '#B6D4F7'}}
                    thumbColor={
                      item.avail_COD === false ? '#EDECED' : '#598FDC'
                    }
                  />
                </View>
                <Text
                  style={{
                    marginLeft: 9,
                    fontFamily: 'NotoSans-Bold',
                    color: '#4a4a4a',
                    fontSize: 12,
                    letterSpacing: 0.45,
                  }}>
                  {item.title}
                </Text>
              </View>
              <Text
                style={{
                  marginTop: 7,
                  textAlign: 'justify',
                  padding: 5,
                  fontFamily: 'NotoSans',
                  color: '#4a4a4a',
                  fontSize: 12,
                  letterSpacing: 0.45,
                }}>
                {item.desc}
              </Text>
            </View>
          );
        }
        break;
      case 3:
        {
          return (
            <View style={{padding: 15}}>
              <Text
                style={{
                  marginLeft: 4,
                  fontFamily: 'NotoSans-Bold',
                  color: '#4a4a4a',
                  fontSize: 12,
                  letterSpacing: 0.45,
                }}>
                {item.title}
              </Text>

              <Text
                style={{
                  textAlign: 'justify',
                  padding: 5,

                  fontFamily: 'NotoSans',
                  color: '#4a4a4a',
                  fontSize: 12,
                  letterSpacing: 0.45,
                }}>
                {item.desc}
              </Text>
            </View>
          );
        }
        break;
    }
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
  };
  const _renderShippingListItem = ({item, index}) => {
    return (
      <View style={{flexDirection: 'row', padding: 5, alignItems: 'center'}}>
        <CheckBox
          tintColors={{true: Colors.mainBlue, false: '#979797'}}
          onTintColor="red"
          onCheckColor="black"
          style={{alignSelf: 'center'}}
          disabled={false}
          value={item.accept ? true : false}
          onValueChange={(newValue) => handleStatusOrder(item.id, newValue)}
        />
        <Text
          style={{
            margin: 8,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 0.5,
            color: '#4a4a4a',
            width: '12%',
          }}>
          {item.name}
        </Text>
        <Text>{item.value}</Text>
      </View>
    );
  };

  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: '#f4f4f4',
      }}>
      <View
        style={{
          height: 50,
          backgroundColor: Colors.mainBlue,
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => {
            dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
            navigation.goBack();
          }}>
          <Image
            style={{width: 25, height: 17, marginLeft: 25}}
            source={Images.arrowBack}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: 'white',
            marginLeft: 25,
            fontFamily: 'NotoSans-Bold',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          Metode pengiriman
        </Text>
      </View>
      {listCourier != null && (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={ownerTokoCourierFetching}
              onRefresh={handleRefresh}
            />
          }>
          {/* //. */}
          <Card
            style={{
              backgroundColor: 'white',
              width: '95%',
              alignSelf: 'center',
              marginTop: 15,
            }}>
            <FlatList
              contentContainerStyle={{padding: 10, bottom: 15}}
              data={filterCODData}
              renderItem={_renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </Card>

          <Card
            style={{
              backgroundColor: 'white',
              width: '95%',
              alignSelf: 'center',
            }}>
            <FlatList
              contentContainerStyle={{padding: 10, bottom: 15}}
              data={filterWithoutCODData}
              renderItem={_renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </Card>
          <View style={{height: 30}} />
        </ScrollView>
      )}
    </View>
  );
};

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    ownerTokoCourierData: state.toko.ownerTokoCourierData.data,
    tokoOrderPrimary: state.toko.tokoOrderData,
    ownerTokoCourierFetching: state.toko.ownerTokoCourierData.fetching,
    supplierData: state.toko.supplierData
      ? state.toko.supplierData.data
        ? state.toko.supplierData.data.suppliers
        : null
      : null,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShippingMethodScreen);
