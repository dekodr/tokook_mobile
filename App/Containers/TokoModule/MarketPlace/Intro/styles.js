import {Dimensions, StyleSheet} from 'react-native';
import {Colors} from '../../../../Themes/';

const {width, height} = Dimensions.get('window');

const Styles = StyleSheet.create({
  // Row & Col
  fdRow: {
    flexDirection: 'row',
  },
  fdCol: {
    flexDirection: 'column',
  },
  mlAuto: {
    marginLeft: 'auto',
  },

  // Header Text
  HeaderText: {
    fontFamily: 'NotoSans-Bold',
    color: '#111432',
    fontSize: 18,
    letterSpacing: 1,
  },
  HeaderAllLink: {
    fontFamily: 'NotoSans-Bold',
    color: Colors.blueBold,
    fontSize: 16,
    letterSpacing: 0.5,
  },

  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Main Area Scroll View
  scrollArea: {
    backgroundColor: '#fff',
    flex: 1,
  },

  body: {
    // paddingTop: StatusBar.currentHeight,
    width: width,
    position: 'relative',
    zIndex: 1,
    // padding: 20,
  },

  bodyInner: {
    marginTop: 24,
    position: 'relative',
    zIndex: 3,
    // paddingHorizontal: 20,
  },

  // Header section
  fixedHeader: {
    width: '100%',
    paddingHorizontal: 12,
    paddingVertical: 10,
    position: 'relative',
    backgroundColor: Colors.blueBold,
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
    flex: 1,
    paddingVertical: 0,
    borderRadius: 5,
  },
  searchBarIcon: {
    height: 48,
    width: 48,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  searchBarInput: {
    flex: 1,
    height: 32,
    paddingVertical: 8,
    backgroundColor: 'transparent',
    color: Colors.charcoal,
  },
  header: {
    width: '100%',
    height: 130,
    position: 'relative',
    backgroundColor: '#fff',
    // padding: 20,
  },
  headerTop: {
    width: '100%',
    height: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    // position: 'absolute',
    // right: 20,
    // top: 0,
    padding: 20,
    // backgroundColor: '#fff',
  },
  icon: {
    width: 20,
    height: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  headerBottom: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    // left: 30,
    paddingHorizontal: 30,
    bottom: 0,
    paddingVertical: 20,
  },
  // Header Section - - - END

  // Content Section
  content: {
    marginTop: 0,
    paddingHorizontal: 0,
    // padding: 30,
    paddingBottom: 0,

    // borderTopRightRadius: 25,
    // borderTopLeftRadius: 25,
    position: 'relative',
    zIndex: 3,
  },
  contentOutter: {
    width: '100%',
    paddingVertical: 60,
    backgroundColor: '#555',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'relative',
  },
  contentInner: {
    width: '100%',
    // paddingHorizontal: 30,
    // padding: 20,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 75,
  },

  divider: {
    width: '100%',
    height: 6,
    backgroundColor: Colors.grayBlackWhite,
  },
});

export default Styles;
