import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  useWindowDimensions,
  BackHandler,
  StyleSheet,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../../Components';
import Card from '../../../../Components/Card';
// import Button from '../../../Components/Button';
import ButtonIcon from '../../../../Components/ButtonIcon';
import tc from '../../Intro/styles';
import styles from './styles';
import {Images, Colors} from '../../../../Themes';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from '../../../../Redux/TokoRedux';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

function Marketplace(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
    marketplaceproductdetailsdata,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter((data) => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    if (marketplaceproductdetailsData !== null) {
      console.log(
        'marketplaceproductdetailsData',
        marketplaceproductdetailsData,
      );
    }
  }, [marketplaceproductdetailsData]);
  const handleGoToAllPage = () => {};

  // Render Carousel
  const isCarousel = React.useRef(null);

  // Tutorial
  const dataCarouselTutorial = [
    {
      id: 1,
      title: 'Cara mulai',
      bg: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 2,
      title: 'mulai berjualan',
      bg: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 3,
      title: 'di Toko OK!',
      bg: require('../../../../assets/image/honda-jazz-hero.png'),
    },
  ];
  const renderCarouselTutorialItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 12}}>
        <Card
          type="CardTutorial"
          id={item.id}
          TutorialName={item.title}
          TutorialBgImage={item.bg}
          handlePress={'test'}
        />
      </TouchableOpacity>
    );
  };

  // Free Ongkir
  const dataCarouselFO = [
    {
      id: 1,
      name: 'Tesla model 4, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 5, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 6, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselFOItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
          handlePress={'test'}
        />
      </TouchableOpacity>
    );
  };

  // Most Wanted Product
  const dataCarouselMW = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselMWItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </TouchableOpacity>
    );
  };

  // bannercarousel
  const dataCarouselBanner = [
    {
      id: 1,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 2,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
    {
      id: 3,
      image: require('../../../../assets/image/honda-jazz-hero.png'),
    },
  ];
  const renderCarouselBannerItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 0}}>
        <Card type="CardBanner" id={item.id} BannerImage={item.image} />
      </TouchableOpacity>
    );
  };
  const PaginationCarouselBanner = () => {
    return (
      <Pagination
        dotsLength={dataCarouselBanner.length}
        activeDotIndex={0}
        containerStyle={{}}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          marginHorizontal: 2,
          backgroundColor: Colors.blueBold,
        }}
        inactiveDotStyle={{
          backgroundColor: Colors.silver,
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={0.8}
      />
    );
  };

  // Trending
  const dataCarouselTR = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselTRItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </TouchableOpacity>
    );
  };

  // For You
  const dataCarouselFY = [
    {
      id: 1,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 2,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
    {
      id: 3,
      name: 'Tesla model 3, made indonesia super speed echo friendly',
      thumbnail: require('../../../../assets/image/honda-jazz-hero.png'),
      price: '3.000.000.000',
      store: 'Mangga Djaja, Menteng, Jakarta Poesat',
    },
  ];
  const renderCarouselFYItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => OnPressCarousel()}
        style={{paddingRight: 12}}>
        <Card
          type="CardProduct"
          id={item.id}
          ProductName={item.name}
          ProductThumbnail={item.thumbnail}
          ProductPrice={item.price}
          ProductStoreLocation={item.store}
        />
      </TouchableOpacity>
    );
  };

  // Render Carousel - - - END

  const _handleMarketplaceDetail = (item) => {
    navigation.navigate('MarketplaceDetailScreen', {
      params: item,
    });
  };

  const _handleMarketplaceProduct = (item) => {
    console.log('press');
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

  const OnPressCarousel = (item) => {
    navigation.navigate('MarketplaceProductScreen', {
      params: item,
    });
  };

  const HeaderText = (item) => {
    // console.log("----", item.text);
    return (
      <View
        style={{
          paddingHorizontal: 12,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Text style={styles.HeaderText}>{item.text}</Text>
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => _handleMarketplaceDetail()}
          style={{
            marginLeft: 'auto',
          }}>
          <Text style={styles.HeaderAllLink}>lihat semua</Text>
        </TouchableOpacity>
      </View>
    );
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    }
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };
  // console.log(CarouselOne);
  return (
    <Container style={styles.container}>
      {/* H E A D E R */}
      <View style={styles.fixedHeader}>
        <View style={styles.searchBar}>
          <View style={styles.searchBarIcon}>
            <MaterialCommunityIcons
              name={'magnify'}
              color={Colors.blueBold}
              size={24}
            />
          </View>
          <TextInput
            placeholder="Cari barang..."
            style={styles.searchBarInput}
            underlineColorAndroid="transparent"
          />
        </View>
        <ButtonIcon icon="cart" color="#fff" size={24} />
      </View>
      {/* H E A D E R - - - END */}
      <ScrollView style={styles.scrollArea}>
        {/* B O D Y */}
        <SafeAreaView style={styles.body}>
          <View style={styles.bodyInner}>
            {/* C O N T E N T */}
            <View style={styles.content}>
              <View style={[styles.contentInner]}>
                {/* Section 1 - Tutorial */}
                <View style={{marginBottom: 25}}>
                  <HeaderText text={'Tutorial pakai Toko OK!'} />
                  <View style={c.Styles.SliderWrapper}>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselTutorial}
                      renderItem={renderCarouselTutorialItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('70%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={6000}
                    />
                  </View>
                </View>

                {/* Section 2 - Free Ongkir */}
                <View style={{marginBottom: 25}}>
                  <HeaderText text={'Produk Gratis Ongkir'} />
                  <View style={c.Styles.SliderWrapper}>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselFO}
                      renderItem={renderCarouselFOItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('45%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={6000}
                    />
                  </View>
                </View>

                {/* Section 3 - Terlaris */}
                <View style={{marginBottom: 25}}>
                  <HeaderText text={'Produk Terlaris'} />
                  <View style={c.Styles.SliderWrapper}>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselMW}
                      renderItem={renderCarouselMWItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('45%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={6000}
                    />
                  </View>
                </View>

                {/* Section 4 - Banner */}
                <View style={{marginBottom: 25}}>
                  <View
                    style={{
                      paddingHorizontal: 12,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text style={styles.HeaderText}>Promo</Text>
                  </View>
                  <View
                    style={
                      ([c.Styles.SliderWrapper],
                      {paddingHorizontal: 0, paddingTop: 15})
                    }>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselBanner}
                      renderItem={renderCarouselBannerItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('100%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={1000}
                    />
                    <View
                      style={{
                        position: 'absolute',
                        alignSelf: 'center',
                        bottom: -15,
                      }}>
                      <PaginationCarouselBanner />
                    </View>
                  </View>
                </View>

                {/* Section 5 - Trending */}
                <View style={{marginBottom: 25}}>
                  <HeaderText text={'Produk Trending'} />
                  <View style={c.Styles.SliderWrapper}>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselTR}
                      renderItem={renderCarouselTRItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('45%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={6000}
                    />
                  </View>
                </View>

                {/* Section 6 - For You */}
                <View style={{marginBottom: 50}}>
                  <HeaderText text={'Produk Buat Kamu'} />
                  <View style={c.Styles.SliderWrapper}>
                    <Carousel
                      ref={isCarousel}
                      data={dataCarouselFY}
                      renderItem={renderCarouselFYItem}
                      sliderWidth={wp('100%')}
                      itemWidth={wp('45%')}
                      loop={true}
                      loopClonesPerSide={2}
                      inactiveSlideScale={1}
                      inactiveSlideOpacity={1}
                      enableMomentum={true}
                      activeSlideAlignment={'start'}
                      activeAnimationType={'spring'}
                      // activeAnimationOptions={{
                      //     friction: 4,
                      //     tension: 40
                      // }}
                      slideStyle={{overflow: 'visible'}}
                      autoplay={true}
                      autoplayDelay={0}
                      autoplayInterval={6000}
                    />
                  </View>
                </View>
              </View>
            </View>
            {/* C O N T E N T - - - END*/}
          </View>
        </SafeAreaView>
        {/* B O D Y - - - END */}
      </ScrollView>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
    marketplaceproductdetailsData:
      state.marketplace.MarketplaceProductDetailsData,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Marketplace);
