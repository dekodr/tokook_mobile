import React, {useEffect, useRef, useState} from 'react';

import {View, Text} from 'react-native';

import {connect} from 'react-redux';

const marketplaceCategory = function(props) {
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        alignItems: 'center',

        justifyContent: 'center',
      }}>
      <Text>Under Development</Text>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    tokoId:
      state.toko.tokoData.data.length === 0
        ? 0
        : state.toko.tokoData.data.listToko[0].id,
    isFetchingLogout: state.auth.deleteAuthToken.fetching,
    getToko: state.toko.tokoData.data,
    detailTokoData: state.toko.detailTokoData.data,
    categoryData: state.toko.categoryTokoData.data,
    productData: state.marketplace.productByUrlData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(marketplaceCategory);
