import React, {useEffect, useState, useRef, useCallback} from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TextInput,
  Alert,
  FlatList,
  TouchableOpacity,
  Share,
  RefreshControl,
  KeyboardAvoidingView,
  ToastAndroid,
  Image,
  Linking,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
import {withNavigationFocus} from 'react-navigation';
import AuthActions from './../../../Redux/AuthenticationRedux';
import {Content, Container} from 'native-base';
import TokoActions from './../../../Redux/TokoRedux';
import GlobalActions from './../../../Redux/GlobalRedux';
import {Button, Spinner, Card} from 'native-base';
import * as func from '../../../Helpers/Utils';
import Config from 'react-native-config';
// import * as func from '../../../helpers/handle';
import RBSheet from 'react-native-raw-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import Feather from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import * as c from '../../../Components';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import {connect} from 'react-redux';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {TextInputMask} from 'react-native-masked-text';
import styles from './styles';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {Images} from '../../../Themes';
import Toast from 'react-native-simple-toast';
import {T} from 'ramda';
import Styles from '../../../Components/Styles';
import Clipboard from '@react-native-community/clipboard';
const errorLinkMessage = 'link tidak valid';
const initialProductForm = {
  name: null,
  description: null,
  price: null,
  capitalPrice: null,
  images: [],
  category: null,
  addCategory: false,
  newCategory: null,
  categorySelected: null,
  inputOtherBusinessType: null,
  isEdit: false,
  productId: null,
};

const initialFilter = {
  isFiltered: false,
  categoryId: null,
  productFiltered: [],
};

const initialValidation = {
  isImageEmpty: false,
  isNameEmpty: false,
  isPriceEmpty: false,
  isCategoryEmpty: false,
};
const initialAddressValidation = {
  isProvinceEmpty: false,
  isCityEmpty: false,
  isDistrictEmpty: false,
  isPostalCodeEmpty: false,
  isAddressEmpty: false,
  isDetailAddressEmpty: false,
  isErrorTokoBankAlreadyRegistered: false,
};
const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
function index(props) {
  const {
    dispatch,
    tokoId,
    tokoData,
    categoryData,
    productData,
    lockTab,
    AddNewCategoryId,
    checkSuccessAddNewCategory,
    navigation,
    setTabIndex,
    toko,
    userData,
    tokoAddressData,
    checkProductAvail,
    productPrimary,
    tokoAddress,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    bankData,
    tokoBankData,
    isErrorTokoBankAlreadyRegistered,
    getOtpWa,
    tokoFetching,
    tokoAllData,
    productAllData,
    refRBSheet,
    isFetchingUpdateTokoData,
  } = props;
  const refShareToko = useRef();
  const refSettingsToko = useRef();
  const refisTokoAvailable = useRef();
  const refisTokoUnavailable = useRef();
  const [timeLeft, setTimeLeft] = useState(0);
  const _handleNavigation = () => {
    refSettingsToko.current.close();
    setTimeout(() => {
      navigation.push('CategoryScreen');
    }, 500);
  };
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const [categoryName, setCategoryName] = useState(null);
  const refAddProduct = useRef();
  const refAddNewCategory = useRef();
  const [productForm, setProductForm] = useStateWithCallbackLazy(
    initialProductForm,
  );
  const [formToko, setFormToko] = useState({
    business_type_id: null,
    name: null,
    link: null,
    whatsapp: null,
    instagram: null,
    facebook: null,
    line: null,
    youtube: null,
    tokopedia: null,
    shopee: null,
    blibli: null,
    inputOtherBusinessType: null,
  });
  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialAddressValidation,
  );
  const [addedBankFormValidation, setAddedBankFormValidation] = useState(
    initialBankValidation,
  );
  const [wanumber, setwaNumber] = useState('');
  const [token, setToken] = useState('');
  const [loadingStep, setloadingStep] = useState(false);
  const [isAddedAddress, setIsAddedAddress] = useState(false);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [isAddedBank, setIsAddedBank] = useState(false);
  const [step, setStep] = useState(0);
  const [isUpdatedBank, setIsUpdatedBank] = useState(false);
  const [bankIndex, setBankIndex] = useState(-1);
  const [rekId, setRekId] = useState(0);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [bank, setOpenPickerBank] = useState(false);
  const [bankValue, setBankValue] = useState(null);
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState(null);
  const [detailAddressValue, setDetailAddressValue] = useState(null);
  const [bankAccountNumValue, setBankAccountNumValue] = useState(null);
  const [bankOwnerNameValue, setBankOwnerNameValue] = useState(null);
  const [bankNameValue, setBankNameValue] = useState(null);
  const [messageShareToko, setMessageShareToko] = useState(null);
  const [editSelected, setEditSelected] = useState(null);
  const [message, setMessage] = useState(null);
  const [productValidation, setProductValidation] = useState(initialValidation);
  const [loadingAddCategory, setLoadingAddCategory] = useState(false);
  const [loadingRBSheet, setLoadingRBSheet] = useState(false);
  const [filterProduct, setFilterProduct] = useState(initialFilter);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [snackBar, setSnackBar] = useState({show: false, message: ''});
  const [refreshing, setRefreshing] = useState(false);
  const [imagesTemp, setImagesTemp] = useState([]);
  const [newImages, setNewImages] = useStateWithCallbackLazy([]);
  const [deletedImages, setDeletedImages] = useState([]);
  const [shareModal, setshareModal] = useState(false);
  const [code, setCode] = useState('');
  const [errMsg, setErrMsg] = useState(null);
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
    isTokpedLinkInvalid: false,
    isShopeeLinkInvalid: false,
    isBlibliLinkInvalid: false,
    isInstagramLinkInvalid: false,
    isFacebookLinkInvalid: false,
    isLineLinkInvalid: false,
    isYoutubeLinkInvalid: false,
  });
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const addressVerified =
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
          ? true
          : false
        : false
      : false;
  useEffect(() => {
    console.log('tokoId', tokoId);
  }, [tokoData]);
  const stepNext = () => {
    setTokoValidation({
      isNameTokoEmpty: !formToko.name ? true : false,
      isLinkTokoEmpty: !formToko.link ? true : false,
      //isWhatsappEmpty: !formToko.whatsapp ? true : false,
      isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      isLineLinkInvalid: !isLinkValid(formToko.line),
      isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
    });

    const {
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
      tncCheck,
    } = formToko;

    switch (step) {
      case 1: {
        const data = {
          item: {
            phone: wanumber,
            code: code,
            token: token,
          },
          func: {
            reloadData: () => {
              setloadingStep(false);
              props.dispatch(AuthActions.getUserRequest(props.access_token));
              setCode('');
              setwaNumber('');
              if (!isVerified) {
                if (!tokoData) {
                  setStep(2);
                } else if (!tokoAddress) {
                  setIsAddedAddress(true);
                  setStep(3);
                } else {
                  refisTokoUnavailable.current.close();
                }
              }
            },
            reloadFailed: () => {
              setloadingStep(false);
            },
          },
        };
        setloadingStep(true);
        props.dispatch(AuthActions.whatsappVerificationRequest(data));
      }
      case 2: {
        // setStep(3);
        //Validate first
        if (name && link) {
          const data = {
            item: {
              name: name,
              link: link.toLowerCase(),
              whatsapp: userData.is_phone_verified ? userData.phone : wanumber,
            },
            func: {
              reloadData: () => {
                setloadingStep(false);
                props.dispatch(TokoActions.getDetailTokoRequest(tokoId));
                props.dispatch(TokoActions.getProvinceRequest());
                if (!isVerified) {
                  if (!tokoAddress) {
                    setIsAddedAddress(true);
                    setStep(3);
                  } else {
                    refisTokoUnavailable.current.close();
                  }
                }
              },
              reloadFailed: () => {
                setloadingStep(false);
              },
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.tokoPostRequest(data));
        }
        break;
      }
      case 3: {
        if (provinceValue === null || provinceValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: true,
          });
        } else if (cityValue === null || cityValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: true,
          });
        } else if (districtValue === null || districtValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: true,
          });
        } else if (postalCodeValue === null || postalCodeValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isPostalCodeEmpty: true,
          });
        } else if (addressValue === null || addressValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isAddressEmpty: true,
          });
        } else {
          const postData = {
            tokoId: tokoId,
            func: {
              rb: refisTokoUnavailable,
            },
            purpose: 1,
            addressId: addressIdValue,
            purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
            detail: {
              province_id: provinceValue.id,
              city_id: cityValue.id,
              district_id: districtValue.id,
              postal_code: postalCodeValue,
              address: addressValue.concat(detailAddressValue),
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.postTokoAddressRequest(postData));
        }
        break;
      }

      default:
        break;
    }
  };
  const handleCopyLink = () => {
    Clipboard.setString(Config.TOKO_NAME + toko.link);
    Toast.show('Link disalin!');
  };
  const handlegetOtp = () => {
    const data = {
      phone: userData.is_phone_verified ? userData.phone : wanumber,
      source: 'phone_verification',
      type: 'whatsapp',
    };
    Keyboard.dismiss();
    props.dispatch(AuthActions.getOtpPostRequest(data));
  };
  const handleFormToko = (key, value) => {
    setFormToko({...formToko, [key]: value});
  };
  const openRBSheet = (editSelected) => {
    refisTokoAvailable.current.open();
    setEditSelected(editSelected);
  };
  const setValueSubmitBank = (purpose, value) => {
    switch (purpose) {
      case 1: {
        setIsAddedBank(value);

        break;
      }
      case 2: {
        setBankIndex(value);
        break;
      }
      default:
        null;
    }
  };

  // //handle update toko when success cases
  // useEffect(() => {
  //   if (isSuccessUpdateTokoData) {
  //     openRBSheet(editSelected);
  //   }
  // }, [isSuccessUpdateTokoData]);
  const createMerchant = () => {
    const {
      tncCheck,
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
    } = formToko;

    const {
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
      isTokpedLinkInvalid,
      isBusinessTypeEmpty,
      isShopeeLinkInvalid,
      isBlibliLinkInvalid,
      isInstagramLinkInvalid,
      isFacebookLinkInvalid,
      isLineLinkInvalid,
      isYoutubeLinkInvalid,
    } = tokoValidation;
    return step == 1 ? (
      <View>
        <View
          style={[
            c.Styles.flexRow,
            c.Styles.alignItemsCenter,
            {marginTop: 20},
          ]}>
          <Text style={c.Styles.txtverifWA}>Verifikasi Whatsapp</Text>
        </View>
        <View style={styles.justifyCenter}>
          <View style={styles.viewinputwaNumber}>
            <View style={c.Styles.flexRow}>
              <View style={c.Styles.justifyCenter}>
                <c.Image
                  source={Images.waIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <TextInput
                keyboardType={'number-pad'}
                style={styles.txtinputwaNumber}
                onChangeText={(text) => {
                  setwaNumber(text);
                  setErrMsg(null);
                }}
                placeholderTextColor={'#bdbdbd'}
                placeholder={'Nomor Whatsapp'}
                value={wanumber}
              />
              <TouchableOpacity
                style={[c.Styles.justifyCenter]}
                onPress={timeLeft > 0 ? null : handlegetOtp}
                disabled={props.isFetchingOtp}>
                {getOtpWa.fetching ? (
                  <View
                    style={{
                      width: 50,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Spinner size={18} color={c.Colors.mainBlue} />
                  </View>
                ) : (
                  <Text
                    style={[
                      styles.txtkirimOtp,
                      c.Styles.alignCenter,
                      {opacity: timeLeft === 0 ? 1 : 0.4},
                    ]}>
                    Kirim OTP
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text style={styles.txtbottomwaNumber}>
              Agar pembeli bisa menghubungi kamu
            </Text>
          </View>
          <View style={[c.Styles.alignCenter, {marginTop: '10%', height: 100}]}>
            {getOtpWa.success ? (
              <View>
                <c.PinVerify
                  autoFocus={true}
                  cellStyle={styles.cellStyle}
                  cellStyleFocused={styles.cellStyleFocused}
                  textStyle={styles.txtCellStyle}
                  textStyleFocused={styles.textStyleFocused}
                  value={code}
                  editable={getOtpWa.success ? true : false}
                  onTextChange={(code) => setCode(code)}
                />
                <View
                  style={[
                    ,
                    {
                      marginTop: '10%',
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    },
                  ]}>
                  <TouchableOpacity
                    style={c.Styles.justifyCenter}
                    onPress={handlegetOtp}
                    disabled={timeLeft !== 0 ? true : false}>
                    <Text
                      style={[
                        styles.txtresendOtp,
                        {opacity: timeLeft === 0 ? 1 : 0.4},
                      ]}>
                      {' '}
                      Kirim Ulang OTP
                    </Text>
                  </TouchableOpacity>

                  <Text style={styles.txttimeLeft}>
                    00:{timeLeft < 10 ? '0' : ''}
                    {timeLeft}
                  </Text>
                </View>
              </View>
            ) : null}
          </View>

          <View
            style={{
              top: 30,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              //onPress={handleResendOtp}
              disabled={props.isFetchingOtp}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  opacity: timeLeft === 0 ? 1 : 0.4,
                  color: 'white',
                  marginTop: 2,
                }}>
                {' '}
                Kirim Ulang
              </Text>
            </TouchableOpacity>

            <Text
              style={{
                marginLeft: 6,
                fontFamily: 'NotoSans-ExtraBold',
                color: 'white',
                fontSize: 15,
                opacity: 1,
              }}>
              00:{timeLeft < 10 ? '0' : ''}
              {timeLeft}
            </Text>
          </View>
        </View>
      </View>
    ) : step == 2 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <Text style={c.Styles.txtCreateMerchant}>Buka Toko Online</Text>
        </View>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={c.Styles.viewTokoStepContent}
          showsVerticalScrollIndicator={false}>
          <Text style={c.Styles.lineHeightTwenty}>
            Fitur ini dibuat untuk kamu yang ingin memiliki website usaha kamu
            sendiri. Bagikan dan tingkatkan penjualan kamu.
          </Text>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={c.Styles.txtInputWithIcon}>
              <View style={c.Styles.marginRightTen}>
                <c.Image source={Images.toko} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  autoCapitalize="words"
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Nama Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      name: text,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: text ? false : true,
                    });
                  }}
                  value={name}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: name ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              Nama toko di website kamu
            </Text>
            {isNameTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Nama toko harus diisi
              </Text>
            )}
          </View>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image source={Images.at} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Link Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: text ? false : true,
                    });
                  }}
                  value={link}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: link ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              {Config.TOKO_NAME}
              <Text
                style={{
                  fontWeight: '600',
                  fontSize: 14,
                  letterSpacing: 1,
                  textTransform: 'lowercase',
                }}>
                {!link ? 'link-toko' : link}
              </Text>
            </Text>
            {isLinkTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Link toko harus diisi
              </Text>
            )}
          </View>
        </ScrollView>
      </View>
    ) : step == 3 ? (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'handled'}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 14,
            letterSpacing: 1,
            marginTop: 20,
          }}>
          Alamat Toko
        </Text>

        {isAddedAddress && (
          <View
            style={{
              width: '100%',
              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && cityData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && districtData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  keyboardShouldPersistTaps={'handled'}
                  nestedScrollEnabled
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
        {isUpdatedAddress && (
          <View
            style={{
              width: '100%',

              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            <TouchableOpacity
              onPress={() => setIsUpdatedAddress(false)}
              delayPressIn={0}
              style={{position: 'absolute', right: 8, top: 8}}>
              <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
            </TouchableOpacity>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && provinceData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && (
              <FlatList
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  nestedScrollEnabled
                  style={{height: 300}}
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
      </ScrollView>
    ) : null;
  };
  const isLinkValid = (link) => {
    if (typeof link === 'string' && link.length) {
      if (
        link.toLowerCase().includes('http') ||
        link.toLowerCase().includes('www')
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };
  const _renderBankItem = ({index, item}) => {
    if (bankIndex === index) {
      return (
        <View
          style={{
            width: '100%',

            borderColor: c.Colors.gray,
            borderWidth: 0.58,
            marginTop: 17,
            paddingLeft: 18,
            paddingRight: 18,
          }}>
          <TouchableOpacity
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());
              setBankIndex(-1);
            }}
            delayPressIn={0}
            style={{position: 'absolute', right: 8, top: 8}}>
            <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'NotoSans',
              color: '#1a69d5',
              fontSize: 10,
              letterSpacing: 1.2,
              marginTop: 25,
            }}>
            Nama Bank
          </Text>
          <TouchableOpacity
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());

              if (bank) {
                setOpenPickerBank(false);
              } else {
                setOpenPickerBank(true);
              }
            }}
            delayPressIn={0}
            style={{
              marginTop: 8,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankEmpty
                ? 'red'
                : bankNameValue.name == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <Text
              style={{
                bottom: 3,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
              }}>
              {bankNameValue.name != null || bankNameValue.name != ''
                ? bankNameValue.name
                : 'Nama Bank'}
            </Text>
            <MaterialCommunityIcons
              style={{bottom: 5}}
              name={!bank ? 'menu-down' : 'menu-up'}
              size={25}
              color="#1a69d5"
            />
          </TouchableOpacity>
          {/* {addedBankFormValidation.isBankEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib memilih Nama Bank
            </Text>
          )} */}
          {bank && (
            <FlatList
              nestedScrollEnabled
              style={{height: 300}}
              renderItem={_addressRenderItem}
              data={bankData.banks}
            />
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                ? 'red'
                : bankAccountNumValue == null
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              keyboardType={'number-pad'}
              onChangeText={(text) => {
                dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankAccountNumEmpty: false,
                });
                setBankAccountNumValue(text);
              }}
              value={bankAccountNumValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nomor Rekening'}
            />
          </View>
          {addedBankFormValidation.isBankAccountNumEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nomor rekening
            </Text>
          )}
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              width: '100%',
              borderBottomWidth: 0.5,
              borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                ? 'red'
                : bankOwnerNameValue == null || bankOwnerNameValue == ''
                ? '#c9c6c6'
                : '#1a69d5',
            }}>
            <TextInput
              style={{
                bottom: -5,
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1,
                color: '#7b7b7b',
                width: '100%',
              }}
              onChangeText={(text) => {
                dispatch(TokoActions.postTokoBankReset());

                setAddedBankFormValidation({
                  ...addedBankFormValidation,
                  isBankOwnerEmpty: false,
                });

                setBankOwnerNameValue(text);
              }}
              value={bankOwnerNameValue}
              placeholderTextColor={'#7b7b7b'}
              placeholder={'Nama Pemilik'}
            />
          </View>
          {addedBankFormValidation.isBankOwnerEmpty && (
            <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
              Wajib mengisi nama pemilik rekening
            </Text>
          )}

          <View style={{height: 14}} />
          {isErrorTokoBankAlreadyRegistered && (
            <Text
              style={[
                c.Styles.txtInputDesc,
                {color: 'red', alignSelf: 'center', top: 15},
              ]}>
              {isErrorTokoBankAlreadyRegistered.message}
            </Text>
          )}
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <View style={{width: '23%'}}>
              <Button
                style={{
                  height: 55,
                  width: 55,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: '#484848',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                }}
                onPress={() => handleAddBankSubmit(3)}>
                <Image
                  style={{width: 25, height: 30}}
                  source={Images.trashIcon}
                />
              </Button>
            </View>
            <TouchableOpacity
              style={{
                top: 1,
                borderRadius: 25,
                height: 50,
                alignSelf: 'center',
                width: '75%',
                backgroundColor: '#1a69d5',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              delayPressIn={0}
              onPress={handleAddBankSubmit}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  color: '#ffffff',
                  letterSpacing: 1,
                  fontSize: 14,
                }}>
                Simpan
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{height: 25}} />
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          delayPressIn={0}
          style={{
            width: '100%',
            height: 100,
            borderColor: '#c9c6c6',
            borderWidth: 0.5,
            marginTop: 14,
            justifyContent: 'center',
            paddingLeft: 25,
            bottom: 5,
          }}>
          <TouchableWithoutFeedback>
            <TouchableOpacity
              //kylie
              onPress={() => {
                const dataPost = {tokoId: props.tokoId, bankId: item.id};
                dispatch(
                  TokoActions.updateDefaultRekeningTokoRequest(dataPost),
                );
              }}
              delayPressIn={0}
              style={{
                marginTop: 8,
                marginRight: 8,
                position: 'absolute',
                right: 0,
                top: 0,
              }}>
              <Image
                style={{width: 20, height: 20}}
                source={
                  item.default ? Images.starBlueIcon : Images.starWhiteIcon
                }
              />
              {/* <MaterialCommunityIcons name="star" size={20} color={item.default?c.Colors.mainBlue:'#f4f4f4'} /> */}
            </TouchableOpacity>
          </TouchableWithoutFeedback>
          <TouchableOpacity
            style={{top: 9, width: '95%'}}
            onPress={() => {
              dispatch(TokoActions.postTokoBankReset());

              setIsAddedBank(false);
              setRekId(item.id);
              setBankIndex(index);
              setIsUpdatedBank(true);
              setBankOwnerNameValue(item.holder_name);
              setBankAccountNumValue(item.rekening_number);
              setBankNameValue(item.bank_details);
              setBankValue(item.bank_details.name);
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.bank_details.name === null || item.bank_details.name === ''
                ? '-'
                : item.bank_details.name}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.rekening_number === null || item.rekening_number === ''
                ? '-'
                : item.rekening_number}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#111432',
                fontSize: 12,
                letterSpacing: 1,
              }}>
              {item.holder_name === null || item.holder_name === ''
                ? '-'
                : item.holder_name}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }
  };
  const handleUpdateToko = () => {
    if (editSelected === 'delete-toko') {
      setEditSelected(null);
      const data = {
        tokoId: props.tokoId,
        func: {reloadData: reload},
      };
      dispatch(TokoActions.deleteTokoRequest(data));
    } else {
      const {
        isLinkTokoEmpty,
        isNameTokoEmpty,
        isWhatsappEmpty,
      } = tokoValidation;
      const data = {
        tokoId: props.tokoId,
        item:
          editSelected === 'toko'
            ? {
                name: formToko.name,
                link: formToko.link.toLowerCase(),
                whatsapp:
                  formToko.whatsapp.substring(0, 2) == 62
                    ? '0' +
                      formToko.whatsapp.substring(2, formToko.whatsapp.length)
                    : formToko.whatsapp,
              }
            : editSelected === 'category'
            ? {
                business_type_id: formToko.business_type_id,
              }
            : editSelected === 'e-commerce'
            ? {
                tokopedia: formToko.tokopedia,
                shopee: formToko.shopee,
                blibli: formToko.blibli,
              }
            : editSelected === 'socmed'
            ? {
                instagram: formToko.instagram,
                facebook: formToko.facebook,
                line: formToko.line,
                youtube: formToko.youtube,
              }
            : null,
        func: {reloadData: reload},
      };
      setTokoValidation({
        isNameTokoEmpty: !formToko.name ? true : false,
        isLinkTokoEmpty: !formToko.link ? true : false,
        isWhatsappEmpty: !formToko.whatsapp ? true : false,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
        isLineLinkInvalid: !isLinkValid(formToko.line),
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
      if (editSelected === 'e-commerce') {
        if (
          isLinkValid(formToko.tokopedia) &&
          isLinkValid(formToko.shopee) &&
          isLinkValid(formToko.blibli)
        ) {
          // setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      } else if (editSelected === 'socmed') {
        if (
          isLinkValid(formToko.instagram) &&
          isLinkValid(formToko.facebook) &&
          isLinkValid(formToko.line) &&
          isLinkValid(formToko.youtube)
        ) {
          // setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      } else {
        if (formToko.name && formToko.link && formToko.whatsapp) {
          // setEditSelected(null);
          dispatch(TokoActions.tokoUpdateRequest(data));
        }
      }
    }
  };
  const handleAddAdressSubmit = () => {
    if (provinceValue === null || provinceValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isProvinceEmpty: true,
      });
    } else if (cityValue === null || cityValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isCityEmpty: true,
      });
    } else if (districtValue === null || districtValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isDistrictEmpty: true,
      });
    } else if (postalCodeValue === null || postalCodeValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isPostalCodeEmpty: true,
      });
    } else if (addressValue === null || addressValue === '') {
      setAddedAddressFormValidation({
        ...addedAddressFormValidation,
        isAddressEmpty: true,
      });
    } else {
      const postData = {
        tokoId: props.tokoId,
        func: {
          isAdded: setIsAddedAddress(),
          isUpdated: setIsUpdatedAddress(),
        },
        addressId: addressIdValue,
        purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
        detail: {
          province_id: provinceValue.id,
          city_id: cityValue.id,
          district_id: districtValue.id,
          postal_code: postalCodeValue,
          address: addressValue.concat(
            detailAddressValue === null ? '' : detailAddressValue,
          ),
        },
      };

      dispatch(TokoActions.postTokoAddressRequest(postData));
    }
  };
  const handleAddBankSubmit = (value) => {
    if (value === 3) {
      const postData = {
        tokoId: props.tokoId,
        rekId: rekId,
        setValueBank: setValueSubmitBank,
        // func: {updateBank: setBankIndex()},
        purpose: 3,
        detail: {
          bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
          holder_name: bankOwnerNameValue,
          rekening_number: bankAccountNumValue,
        },
      };

      dispatch(TokoActions.postTokoBankRequest(postData));
    } else {
      if (bankValue === null || bankValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankEmpty: true,
        });
      } else if (bankAccountNumValue === null || bankAccountNumValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankAccountNumEmpty: true,
        });
      } else if (bankOwnerNameValue === null || bankOwnerNameValue === '') {
        setAddedBankFormValidation({
          ...addedBankFormValidation,
          isBankOwnerEmpty: true,
        });
      } else {
        const postData = {
          tokoId: props.tokoId,
          rekId: rekId,
          setValueBank: setValueSubmitBank,
          // func: {updateBank: setBankIndex()},
          purpose: isAddedBank ? 1 : 2,
          detail: {
            bank_id: isAddedBank ? bankValue.id : bankNameValue.id,
            holder_name: bankOwnerNameValue,
            rekening_number: bankAccountNumValue,
          },
        };

        dispatch(TokoActions.postTokoBankRequest(postData));
      }
    }
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const searchingPostalCode = (searchText) => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerCity(false);
        setOpenPickerProvince(false);
        setOpenPickerDistrict(false);

        setOpenPickerPostalCode(true);
        let filteredData = postalCodeData.postalCode.filter(function (item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setOpenPickerCity(false);
      setOpenPickerProvince(false);
      setOpenPickerDistrict(false);

      setPostalCodeValue(searchText);
    }
  };
  const _tokoAddressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(TokoActions.getCityRequest(item.province_details));
          dispatch(TokoActions.getDistrictRequest(item.city_details));
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: item.city_details.name,
              districtName: item.district_details.name,
            }),
          );
          setAddressIdValue(item.id);
          setProvinceValue(item.province_details);
          setCityValue(item.city_details);
          setDistrictValue(item.district_details);
          setAddressValue(item.address);
          setDetailAddressValue(item.address2);
          setPostalCodeValue(item.postal_code);
          setIsUpdatedAddress(true);
        }}
        delayPressIn={0}
        style={{
          width: '100%',
          borderColor: c.Colors.gray,
          borderWidth: 0.58,
          marginTop: 17,
          paddingLeft: 18,
          paddingRight: 18,
          bottom: 1,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
            marginTop: 20,
          }}>
          {item.province_details.name}
        </Text>

        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.name}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.postal_code}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.fullAddress}
        </Text>
        <View style={{height: 25}} />
        {/* <View
          style={{
            borderRadius: 25,
            marginTop: 15,
            width: '100%',
            height: '50%',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '28%',
              height: '100%',
            }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              initialRegion={{
                latitude: 36.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0,
                longitudeDelta: 0.0,
              }}
              style={{
                width: '100%',
                height: '72%',
                alignSelf: 'center',
              }}>
              <MapView.Marker
                coordinate={{latitude: 37.78825, longitude: -122.4324}}
                title={'title'}
                description={'description'}
              />
            </MapView>
          </View>
          <View
            style={{
              width: '75%',
              height: '80%',
              paddingLeft: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#7b7b7b',
                letterSpacing: 1,
                fontSize: 9,
              }}>
              Jl. Blora No.32, RT.2/RW.6, Dukuh Atas, Menteng, Kec. Menteng,
              Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310
            </Text>
          </View>
        </View>
      */}
      </TouchableOpacity>
    );
  };
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          );
        }}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode
            ? item.postal_code
            : city
            ? item.type + ' ' + item.name
            : item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  const renderEditMerchant = () => {
    const {
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
      isTokpedLinkInvalid,
      isShopeeLinkInvalid,
      isBlibliLinkInvalid,
      isInstagramLinkInvalid,
      isFacebookLinkInvalid,
      isLineLinkInvalid,
      isYoutubeLinkInvalid,
    } = tokoValidation;
    switch (editSelected) {
      case 'toko':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={[c.Styles.marginTopFive]}>
                <View style={c.Styles.txtInputWithIcon}>
                  <View style={c.Styles.marginRightTen}>
                    <c.Image
                      source={require('../../../assets/image/toko.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      autoCapitalize="words"
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Nama Toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.name}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isNameTokoEmpty: formToko.name ? false : true,
                        })
                      }
                      onChangeText={(text) => {
                        setFormToko({
                          ...formToko,
                          name: text,
                          link: text.split(' ').join('-'),
                        });
                      }}
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  Nama toko di website kamu
                </Text>
                {isNameTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Nama toko harus diisi
                  </Text>
                )}
              </View>
              <View style={[c.Styles.marginTopTen]}>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/at.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="Link Toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.link}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isLinkTokoEmpty: formToko.link ? false : true,
                        })
                      }
                      onChangeText={(value) =>
                        handleFormToko('link', value.split(' ').join('-'))
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  tokook.id/
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 14,
                      letterSpacing: 1,
                      textTransform: 'lowercase',
                    }}>
                    {formToko.link !== null ? formToko.link : 'link-toko'}
                  </Text>
                </Text>
                {isLinkTokoEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Link toko harus diisi
                  </Text>
                )}
              </View>
              <View style={[c.Styles.marginTopTen]}>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/whatsapp.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      editable={false}
                      keyboardType="numeric"
                      // style={c.Styles.txtInputCreateMerchant}
                      placeholder="Nomor Whatsapp"
                      editable={false}
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.whatsapp}
                      onBlur={() =>
                        setTokoValidation({
                          ...tokoValidation,
                          isWhatsappEmpty: formToko.whatsapp ? false : true,
                        })
                      }
                      onChangeText={(value) =>
                        handleFormToko('whatsapp', value)
                      }
                    />
                  </View>
                </View>
                <Text style={[c.Styles.txtInputDesc]}>
                  Agar pembeli bisa menghubungi kamu
                </Text>
                {isWhatsappEmpty && (
                  <Text style={c.Styles.txtInputMerchantErrors}>
                    Nomor whatsapp harus diisi
                  </Text>
                )}
              </View>
              <View
                style={[{marginTop: 25}, c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnPrimary]}
                  onPress={() => handleUpdateToko()}>
                  {isFetchingUpdateTokoData ? (
                    <View
                      style={{
                        width: 50,
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Spinner size={18} color={'white'} />
                    </View>
                  ) : (
                    <Text
                      style={[
                        c.Styles.txtCreateMerchantBtn,
                        {fontWeight: 'bold', letterSpacing: 1},
                      ]}>
                      {editSelected === 'delete-toko' ? 'Hapus' : 'Simpan'}
                    </Text>
                  )}
                </Button>
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'category':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View
                style={[
                  c.Styles.flexRow,
                  c.Styles.viewheaderDay,
                  c.Styles.justifyspaceBetween,
                ]}>
                <View>
                  <View
                    style={[
                      c.Styles.paddingTen,
                      {
                        borderRadius: 100,
                        backgroundColor: c.Colors.blueWhite,
                      },
                    ]}>
                    <c.Image
                      source={
                        formToko.business_type
                          ? {
                              uri:
                                formToko.business_type &&
                                formToko.business_type.icon,
                            }
                          : Images.noImg
                      }
                      style={c.Styles.imgaddNotificationToTransaction}
                    />
                  </View>
                </View>
                <TouchableOpacity style={{width: '80%'}}>
                  <View style={[c.Styles.borderBottom, {marginLeft: 5}]}>
                    <TextInput
                      onChangeText={(value) =>
                        handleFormToko('inputOtherBusinessType', value)
                      }
                      ref={refInputBusinessType}
                      editable={false}
                      placeholder="Masukkan jenis usaha kamu"
                      style={[c.Styles.txtcategoryNotification]}>
                      {formToko.business_type_id !== 12
                        ? formToko.business_type
                          ? formToko.business_type.name
                          : null
                        : // : formToko.inputOtherBusinessType}
                          'Lainnya'}
                    </TextInput>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={[c.Styles.viewTokoStep2WrapCategory]}>
                {listBusinessType.length
                  ? listBusinessType.map((item) => {
                      return (
                        <TouchableOpacity
                          onPress={() => handleSelectedBusinessType(item)}
                          key={item.id}
                          style={[
                            c.Styles.justifyCenter,
                            c.Styles.alignItemsCenter,
                            c.Styles.marginTopFifteen,
                            c.Styles.marginBottomFifteen,
                            {width: '33.33%'},
                          ]}>
                          <View
                            style={[
                              c.Styles.paddingTen,
                              {
                                borderRadius: 100,
                                backgroundColor: c.Colors.blueWhite,
                              },
                            ]}>
                            <c.Image
                              source={
                                item.icon ? {uri: item.icon} : Images.noImg
                              }
                              style={c.Styles.imgaddNotificationToTransaction}
                            />
                          </View>
                          <Text
                            style={{
                              marginTop: 8,
                              textAlign: 'center',
                              fontSize: 12,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      );
                    })
                  : null}
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'toko-address':
        return (
          <View>
            <ScrollView
              keyboardShouldPersistTaps={'always'}
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Alamat Toko
              </Text>
              {tokoAddressData.listAddress.length === 0 && !isAddedAddress && (
                <TouchableOpacity
                  onPress={() => setIsAddedAddress(true)}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,
                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Alamat
                  </Text>
                </TouchableOpacity>
              )}
              {isAddedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && provinceData != null && (
                    <FlatList
                      keyboardShouldPersistTaps={'handled'}
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                    {/* bb */}
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && cityData != null && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && districtData != null && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {isUpdatedAddress && (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsUpdatedAddress(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {provinceValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 25,
                      }}>
                      Provinsi
                    </Text>
                  )}

                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (province) {
                        setOpenPickerProvince(false);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      } else {
                        setOpenPickerProvince(true);
                        setOpenPickerCity(false);
                        setOpenPickerDistrict(false);
                        setOpenPickerPostalCode(false);
                      }
                    }}
                    style={{
                      marginTop: provinceValue != null ? 8 : 50,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                        ? 'red'
                        : provinceValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    {/* <Picker
  selectedValue={language}
 mode={'dropdown'}
  style={{height: '100%', width: "100%",backgroundColor:'red'}}
  onValueChange={(itemValue, itemIndex) =>
    setLanguage(itemValue)
 
  }>
 {language.map((item, index) => {
        return (<Picker.Item label={item} value={index} key={index}/>) 
    })}
</Picker> */}
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        marginLeft: 2,
                      }}>
                      {provinceValue != null ? provinceValue.name : 'Provinsi'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!province ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isProvinceEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Provinsi
                    </Text>
                  )}

                  {province && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={provinceData.provincies}
                    />
                  )}
                  {cityValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kota
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else {
                        if (city) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(true);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: cityValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isCityEmpty
                        ? 'red'
                        : cityValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,

                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {cityValue != null
                        ? `${cityValue.type} ${cityValue.name}`
                        : 'Kota'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!city ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isCityEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kota
                    </Text>
                  )}
                  {city && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={cityData.cities}
                    />
                  )}
                  {districtValue != null && (
                    <Text
                      style={{
                        marginLeft: 2,

                        fontFamily: 'NotoSans',
                        color: '#1a69d5',
                        fontSize: 10,
                        letterSpacing: 1.2,
                        marginTop: 32,
                      }}>
                      Kecamatan
                    </Text>
                  )}
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => {
                      if (provinceValue === null) {
                        Toast.show('Pilih Provinsi Terlebih Dahulu !');
                      } else if (cityValue === null) {
                        Toast.show('Pilih Kota Terlebih Dahulu !');
                      } else {
                        if (district) {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(false);
                          setOpenPickerPostalCode(false);
                        } else {
                          setOpenPickerProvince(false);
                          setOpenPickerCity(false);
                          setOpenPickerDistrict(true);
                          setOpenPickerPostalCode(false);
                        }
                      }
                    }}
                    style={{
                      marginTop: districtValue != null ? 8 : 32,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                        ? 'red'
                        : districtValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        marginLeft: 2,
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {districtValue != null ? districtValue.name : 'Kecamatan'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!district ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedAddressFormValidation.isDistrictEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Kecamatan
                    </Text>
                  )}
                  {district && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={districtData.districts}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                        ? 'red'
                        : postalCodeValue == null || postalCodeValue === ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={searchingPostalCode}
                      keyboardType={'number-pad'}
                      value={postalCodeValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Kode Pos'}
                    />
                    {postalCodeValue != null && postalCode && (
                      <TouchableOpacity
                        delayPressIn={0}
                        onPress={() => setOpenPickerPostalCode(false)}
                        style={{bottom: 9, position: 'absolute', right: 0}}>
                        <MaterialCommunityIcons
                          name="check"
                          size={25}
                          color="#1a69d5"
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {addedAddressFormValidation.isPostalCodeEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Kode Pos
                    </Text>
                  )}
                  {postalCode &&
                    provinceValue != null &&
                    cityValue != null &&
                    districtValue != null && (
                      <FlatList
                        nestedScrollEnabled
                        renderItem={_addressRenderItem}
                        data={
                          postalCodeFilteredData &&
                          postalCodeFilteredData.length > 0
                            ? postalCodeFilteredData
                            : postalCodeData.postalCode
                        }
                      />
                    )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedAddressFormValidation.isAddressEmpty
                        ? 'red'
                        : addressValue == null || addressValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      multiline={true}
                      onChangeText={(text) => {
                        setAddressValue(text);
                      }}
                      value={addressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Alamat Lengkap'}
                    />
                  </View>
                  {addedAddressFormValidation.isAddressEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi Alamat Lengkap
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor:
                        detailAddressValue === null || detailAddressValue === ''
                          ? '#c9c6c6'
                          : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => setDetailAddressValue(text)}
                      value={detailAddressValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Detail Alamat (optional)'}
                    />
                  </View>

                  {/* <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 32,
                  }}>
                  <Card
                    style={{
                      height: 60,
                      width: '17%',
                      borderWidth: 0.5,
                      borderColor: '#c9c6c6',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="map-marker"
                      size={35}
                      color={'#1a69d5'}
                    />
                  </Card>

                  <View
                    style={{
                      height: '95%',

                      marginLeft: 12,
                      flexDirection: 'row',
                      width: '78%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: '#c9c6c6',
                    }}>
                    <TextInput
                      style={{
                        bottom: -12,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={text => null}
                      value={null}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Lokasi'}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    color: '#7b7b7b',
                    letterSpacing: 1,
                    fontSize: 9,
                    marginTop: 5,
                  }}>
                  Pilih lokasi yang sesuai dengan alamat yang kamu isi di atas
                </Text> */}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 25,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddAdressSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              )}
              {!isUpdatedAddress && (
                <FlatList
                  data={tokoAddressData.listAddress.slice(0, 1)}
                  renderItem={_tokoAddressRenderItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      case 'rek-bank':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <Text
                style={{
                  fontFamily: 'NotoSans',
                  color: '#111432',
                  fontSize: 14,
                  letterSpacing: 1,
                  marginTop: 20,
                }}>
                Rekening Bank
              </Text>

              {isAddedBank ? (
                <View
                  style={{
                    width: '100%',

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                    marginTop: 17,
                    paddingLeft: 18,
                    paddingRight: 18,
                  }}>
                  <TouchableOpacity
                    onPress={() => setIsAddedBank(false)}
                    delayPressIn={0}
                    style={{position: 'absolute', right: 8, top: 8}}>
                    <MaterialCommunityIcons
                      name="close"
                      size={22}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontFamily: 'NotoSans',
                      color: '#1a69d5',
                      fontSize: 10,
                      letterSpacing: 1.2,
                      marginTop: 25,
                    }}>
                    Nama Bank
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      dispatch(TokoActions.postTokoBankReset());

                      if (bank) {
                        setOpenPickerBank(false);
                      } else {
                        setOpenPickerBank(true);
                      }
                    }}
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankEmpty
                        ? 'red'
                        : bankValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <Text
                      style={{
                        bottom: 3,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                      }}>
                      {bankValue != null ? bankValue.name : 'Nama Bank'}
                    </Text>
                    <MaterialCommunityIcons
                      style={{bottom: 5}}
                      name={!bank ? 'menu-down' : 'menu-up'}
                      size={25}
                      color="#1a69d5"
                    />
                  </TouchableOpacity>
                  {addedBankFormValidation.isBankEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib memilih Nama Bank
                    </Text>
                  )}
                  {bank && (
                    <FlatList
                      nestedScrollEnabled
                      renderItem={_addressRenderItem}
                      data={bankData.banks}
                    />
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankAccountNumEmpty
                        ? 'red'
                        : bankAccountNumValue == null
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      keyboardType={'number-pad'}
                      onChangeText={(text) => {
                        dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankAccountNumEmpty: false,
                        });
                        setBankAccountNumValue(text);
                      }}
                      value={bankAccountNumValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nomor Rekening'}
                    />
                  </View>
                  {addedBankFormValidation.isBankAccountNumEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nomor rekening
                    </Text>
                  )}
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: 'row',
                      width: '100%',
                      borderBottomWidth: 0.5,
                      borderBottomColor: addedBankFormValidation.isBankOwnerEmpty
                        ? 'red'
                        : bankOwnerNameValue == null || bankOwnerNameValue == ''
                        ? '#c9c6c6'
                        : '#1a69d5',
                    }}>
                    <TextInput
                      style={{
                        bottom: -5,
                        fontFamily: 'NotoSans',
                        fontSize: 12,
                        letterSpacing: 1,
                        color: '#7b7b7b',
                        width: '100%',
                      }}
                      onChangeText={(text) => {
                        dispatch(TokoActions.postTokoBankReset());

                        setAddedBankFormValidation({
                          ...addedBankFormValidation,
                          isBankOwnerEmpty: false,
                        });

                        setBankOwnerNameValue(text);
                      }}
                      value={bankOwnerNameValue}
                      placeholderTextColor={'#7b7b7b'}
                      placeholder={'Nama Pemilik'}
                    />
                  </View>
                  {addedBankFormValidation.isBankOwnerEmpty && (
                    <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                      Wajib mengisi nama pemilik rekening
                    </Text>
                  )}

                  <View style={{height: 14}} />
                  {isErrorTokoBankAlreadyRegistered && (
                    <Text
                      style={[
                        c.Styles.txtInputDesc,
                        {color: 'red', alignSelf: 'center', top: 15},
                      ]}>
                      {isErrorTokoBankAlreadyRegistered.message}
                    </Text>
                  )}
                  <TouchableOpacity
                    style={{
                      borderRadius: 25,
                      marginTop: 30,
                      height: 50,
                      alignSelf: 'center',
                      width: '95%',
                      backgroundColor: '#1a69d5',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    delayPressIn={0}
                    onPress={handleAddBankSubmit}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        color: '#ffffff',
                        letterSpacing: 1,
                        fontSize: 14,
                      }}>
                      Simpan
                    </Text>
                  </TouchableOpacity>

                  <View style={{height: 25}} />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    setBankAccountNumValue(null);
                    setBankValue(null);
                    setBankOwnerNameValue(null);
                    setIsAddedBank(true);
                    setIsUpdatedBank(false);
                    setBankIndex(-1);
                    dispatch(TokoActions.postTokoBankReset());
                  }}
                  delayPressIn={0}
                  style={{
                    marginTop: 25,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    width: '99%',
                    height: 65,
                    backgroundColor: '#ffffff',
                    marginLeft: 0,
                    marginRight: 0,
                    alignItems: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,

                    borderColor: c.Colors.gray,
                    borderWidth: 0.58,
                  }}>
                  <TouchableOpacity
                    delayPressIn={0}
                    style={{
                      marginTop: 8,
                      width: 25,
                      height: 25,
                      borderWidth: 1,
                      borderColor: '#a9a9a9',
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialCommunityIcons
                      name="plus"
                      size={20}
                      color="#a9a9a9"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      marginTop: 8,

                      fontFamily: 'NotoSans',
                      fontSize: 12,
                      letterSpacing: 1.5,
                      color: '#7b7b7b',
                    }}>
                    Tambah Rekening
                  </Text>
                </TouchableOpacity>
              )}
              {tokoBankData != null && (
                <FlatList
                  extraData={useState}
                  nestedScrollEnabled
                  data={tokoBankData.listRekening}
                  style={{marginTop: 12}}
                  renderItem={_renderBankItem}
                />
              )}

              <View style={{height: 25}} />
            </ScrollView>
          </View>
        );
        break;
      case 'e-commerce':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={c.Styles.viewCreateMerchantSectionWrapper}>
                <Text style={c.Styles.txtBold}>E-Commerce</Text>
                <Text style={c.Styles.lineHeightTwenty}>
                  Untuk customer kamu jika ingin membeli dan membayar melalui
                  toko kamu lewat e-commerce.
                </Text>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/toped-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.tokopedia.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.tokopedia}
                      onChangeText={(value) =>
                        handleFormToko('tokopedia', value)
                      }
                    />
                  </View>
                </View>
                {isTokpedLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/shopee-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.shopee.co.id/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.shopee}
                      onChangeText={(value) => handleFormToko('shopee', value)}
                    />
                  </View>
                  {isShopeeLinkInvalid ? (
                    <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                      <Text style={c.Styles.txtInputMerchantErrors}>
                        {errorLinkMessage}
                      </Text>
                    </View>
                  ) : null}
                </View>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/blibli-circle.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.blibli.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.blibli}
                      onChangeText={(value) => handleFormToko('blibli', value)}
                    />
                  </View>
                </View>
                {isBlibliLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
              </View>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnPrimary]}
                  onPress={() => handleUpdateToko()}>
                  {isFetchingUpdateTokoData ? (
                    <View
                      style={{
                        width: 50,
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Spinner size={18} color={'white'} />
                    </View>
                  ) : (
                    <Text
                      style={[
                        c.Styles.txtCreateMerchantBtn,
                        {fontWeight: 'bold', letterSpacing: 1},
                      ]}>
                      {editSelected === 'delete-toko' ? 'Hapus' : 'Simpan'}
                    </Text>
                  )}
                </Button>
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'socmed':
        return (
          <View>
            <ScrollView
              style={[c.Styles.viewTokoStepContent]}
              showsVerticalScrollIndicator={false}>
              <View style={c.Styles.viewCreateMerchantSectionWrapper}>
                <Text style={c.Styles.txtBold}>Social Media</Text>
                <Text>Supaya calon pembeli lebih mengenal kamu.</Text>
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/instagram.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.instagram.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.instagram}
                      onChangeText={(value) =>
                        handleFormToko('instagram', value)
                      }
                    />
                  </View>
                </View>
                {isInstagramLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/facebook.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.facebook.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.facebook}
                      onChangeText={(value) =>
                        handleFormToko('facebook', value)
                      }
                    />
                  </View>
                </View>
                {isFacebookLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/line.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.line.com/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.line}
                      onChangeText={(value) => handleFormToko('line', value)}
                    />
                  </View>
                </View>
                {isLineLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
                <View style={[c.Styles.txtInputWithIcon]}>
                  <View style={[c.Styles.marginRightTen]}>
                    <c.Image
                      source={require('../../../assets/image/youtube.png')}
                      style={c.Styles.textInputIcon}
                    />
                  </View>
                  <View style={c.Styles.flexOne}>
                    <TextInput
                      style={c.Styles.txtInputCreateMerchant}
                      placeholder="https://www.youtube.com/user/nama-toko"
                      placeholderTextColor={c.Colors.gray}
                      value={formToko.youtube}
                      onChangeText={(value) => handleFormToko('youtube', value)}
                    />
                  </View>
                </View>
                {isYoutubeLinkInvalid ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      {errorLinkMessage}
                    </Text>
                  </View>
                ) : null}
              </View>
              <View style={[c.Styles.flexOne, c.Styles.pxFive]}>
                <Button
                  style={[c.Styles.btnPrimary]}
                  onPress={() => handleUpdateToko()}>
                  {isFetchingUpdateTokoData ? (
                    <View
                      style={{
                        width: 50,
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Spinner size={18} color={'white'} />
                    </View>
                  ) : (
                    <Text
                      style={[
                        c.Styles.txtCreateMerchantBtn,
                        {fontWeight: 'bold', letterSpacing: 1},
                      ]}>
                      {editSelected === 'delete-toko' ? 'Hapus' : 'Simpan'}
                    </Text>
                  )}
                </Button>
              </View>
            </ScrollView>
          </View>
        );
        break;
      case 'delete-toko':
        return (
          <View style={[c.Styles.justifyCenter, c.Styles.alignItemsCenter]}>
            <c.Image
              source={require('../../../assets/image/store.png')}
              style={{width: 80, height: 80, marginVertical: 20}}
            />
            <Text
              style={{
                fontSize: 12,
                fontFamily: 'NotoSans-Regular',
                letterSpacing: 0.91,
                color: '#4a4a4a',
              }}>
              Apakah anda yakin ingin menghapus toko ini?
            </Text>
          </View>
        );
        break;
      default:
        return (
          <View>
            <Spinner color={c.Colors.mainBlue} />
          </View>
        );
        break;
    }
  };
  useEffect(() => {
    if (getOtpWa.success) {
      const {token} = getOtpWa.data.data;
      setTimeLeft(30);
      setToken(token);
    }
  }, [getOtpWa.success]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }
    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // clear interval on re-render to avoid memory leaks

    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  useEffect(() => {
    if (formToko.name) {
      setTokoValidation({...tokoValidation, isNameTokoEmpty: false});
    }
    if (formToko.link) {
      setTokoValidation({...tokoValidation, isLinkTokoEmpty: false});
    }
    if (formToko.whatsapp) {
      setTokoValidation({...tokoValidation, isWhatsappEmpty: false});
    }
    if (tokoValidation.isTokpedLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      });
    }
    if (tokoValidation.isShopeeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      });
    }
    if (tokoValidation.isBlibliLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      });
    }
    if (tokoValidation.isInstagramLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      });
    }
    if (tokoValidation.isFacebookLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      });
    }
    if (tokoValidation.isLineLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isLineLinkInvalid: !isLinkValid(formToko.line),
      });
    }
    if (tokoValidation.isYoutubeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
    }
  }, [formToko]);
  useEffect(() => {
    setFormToko({
      ...formToko,
      business_type_id: toko.business_type_id,
      name: toko.name,
      link: toko.link,
      whatsapp: toko.whatsapp,
      instagram: toko.instagram,
      facebook: toko.facebook,
      line: toko.line,
      youtube: toko.youtube,
      tokopedia: toko.tokopedia,
      shopee: toko.shopee,
      blibli: toko.blibli,
      business_type: toko.business_type,
    });
    setMessageShareToko(
      `Hi, ${
        toko.name
      } sedang menjual perlengkapan yang kamu butuhkan , yuk kunjungi website kami dilink ini yah ${
        Config.TOKO_NAME + toko.link
      }`,
    );
  }, [toko]);
  useEffect(() => {
    if (productData && toko) {
      setMessage(
        productData.length === 0
          ? `Hi, ${
              toko.name
            }  baru membuat website baru nih  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }`
          : productData.length === 1
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}   \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }  `
          : productData.length === 2
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}  \n- ${
              productData[1].name
            }  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }  `
          : productData.length >= 3
          ? `Hi, ${
              toko.name
            } sedang menjual produk yang kamu pasti suka. Seperti 
       \n- ${productData[0].name}   \n- ${productData[1].name}  \n- ${
              productData[2].name
            }  \n \nYuk kunjungi toko nya disini ${
              Config.TOKO_NAME + toko.link
            }`
          : null,
      );
    }
  }, [productData, toko]);
  useEffect(() => {
    if (tokoId !== null) {
      dispatch(TokoActions.getTokoFinancialRequest(tokoId));
      dispatch(TokoActions.getCategoryTokoRequest(tokoId));
      dispatch(TokoActions.getProductTokoRequest(tokoId));
      dispatch(TokoActions.getTokoBankRequest(tokoId));
      dispatch(TokoActions.getDetailTokoRequest(tokoId));
      dispatch(TokoActions.getOwnerTokoCourierRequest(tokoId));
      dispatch(TokoActions.getCategoryTokoRequest(tokoId));
      dispatch(TokoActions.getProductTokoRequest(tokoId));
      dispatch(TokoActions.getTokoAddressRequest(tokoId));
    }
    dispatch(TokoActions.getBusinessTypeRequest(props.access_token));
    dispatch(TokoActions.getTokoRequest());
    dispatch(TokoActions.getSupplierRequest());
    dispatch(AuthActions.getUserRequest(props.access_token));

    dispatch(AuthActions.loginOtpAfterSuccess());

    dispatch(TokoActions.getProvinceRequest());
    dispatch(TokoActions.getBankRequest());
  }, [tokoId]);
  useEffect(() => {
    if (imagesTemp.length > 0) {
      imagesTemp.forEach((img, i) => {
        const formData = new FormData();
        formData.append('image', img);
        let uploadData = [];
        uploadData = {
          data: {
            tokoId: props.tokoId,
            item: formData,
            func: {
              reloadData: (url) => {
                const data = {
                  tokoId: props.tokoId,
                  item: {
                    icon: url,
                  },
                  func: {reloadData: reload},
                };
                dispatch(TokoActions.tokoUpdateRequest(data));
              },
            },
          },
        };

        dispatch(TokoActions.uploadProductImageRequest(uploadData));

        // func.handleUploadTokoProduct(props.dispatch, props.idToko, formData, (url)=> {

        //     if (i >= imagesTemp.length - 1) {
        //         func.handleUpdateToko(props.dispatch, props.idToko, {icon: url}, (success)=> {
        //             setUpdateSuccess(success)
        //             reload()
        //         })
        //     }

        // }, i)
      });
    }
  }, [imagesTemp]);
  // useEffect(() => {
  //   if (tokoId) {
  //     dispatch(TokoActions.getCategoryTokoRequest(tokoId));
  //     dispatch(TokoActions.getProductTokoRequest(tokoId));
  //     dispatch(TokoActions.getTokoAddressRequest(tokoId));
  //     dispatch(AuthActions.loginOtpAfterSuccess());
  //     dispatch(TokoActions.getTokoBankRequest(tokoId));
  //     dispatch(TokoActions.getDetailTokoRequest(tokoId));
  //     dispatch(TokoActions.getProvinceRequest());
  //     dispatch(TokoActions.getBankRequest());
  //     dispatch(TokoActions.getTokoFinancialRequest(tokoId));
  //     dispatch(TokoActions.getCategoryTokoRequest(tokoId));
  //     dispatch(TokoActions.getProductTokoRequest(tokoId));
  //   }
  // }, [tokoId]);
  useEffect(() => {
    console.log('navigation', navigation);
  }, [navigation]);
  // useEffect(() => {
  //   if (userData) {
  //     console.log(userData.data);

  //     if (userData.data.is_phone_verified === null) {
  //       setStep(1);
  //     }
  //   }
  // }, [userData]);
  // useEffect(() => {
  //   if (checkSuccessAddNew y && !productForm.isEdit) {
  //     setLoadingRBSheet(true);

  //     let urlImages = [];

  //     imagesTemp.forEach((img, i) => {
  //       const form = new FormData();

  //       form.append('image', {
  //         uri: img ? img.uri : Images.noImg,
  //         name: img.name,
  //         type: img.type,
  //       });
  //       const uploadData = {
  //         tokoId: tokoId,
  //         item: form,
  //         func: {
  //           reloadData: (url) => {
  //             urlImages[i] = url;

  //             const data = {
  //               id: props.tokoId,
  //               item: {
  //                 name: productForm.name,
  //                 description: productForm.description,
  //                 price: Number(productForm.price.replace(/[Rp.]/g, '')),
  //                 capital_price:
  //                   typeof productForm.capitalPrice === 'string'
  //                     ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
  //                     : productForm.capitalPrice,
  //                 category_id:
  //                   AddNewCategoryId != null
  //                     ? AddNewCategoryId.productCategory != undefined
  //                       ? AddNewCategoryId.productCategory.id
  //                       : productForm.categorySelected
  //                     : productForm.categorySelected,
  //                 images: urlImages,
  //               },
  //               isVarian: isVarian,
  //               func: {reloadData: handleRefresh},
  //             };
  //             if (i >= imagesTemp.length - 1 && i >= urlImages.length - 1) {
  //               setTimeout(() => {
  //                 dispatch(TokoActions.postProductTokoRequest(data));
  //               }, 1000);
  //             }
  //           },
  //         },
  //       };

  //       dispatch(TokoActions.uploadProductImageRequest(uploadData));
  //     });
  //   } else if (checkSuccessAddNewCategory && productForm.isEdit) {
  //     let uploadImages = imagesTemp.filter((item) => {
  //       return item.hasOwnProperty('type');
  //     });

  //     if (uploadImages.length > 0) {
  //       if (
  //         imagesTemp.length > 0 &&
  //         productForm.name &&
  //         productForm.price &&
  //         productForm.categorySelected
  //       ) {
  //         setLoadingRBSheet(true);
  //         let urlImages = [];
  //         uploadImages.forEach((img, i) => {
  //           const form = new FormData();
  //           form.append('image', {
  //             uri: img ? img.uri : Images.noImg,
  //             name: img.name,
  //             type: img.type,
  //           });

  //           const uploadData = {
  //             tokoId: tokoId,
  //             item: form,
  //             func: {
  //               reloadData: (url) => {
  //                 urlImages[i] = url;

  //                 const data = {
  //                   id: props.tokoId,
  //                   productId: productForm.productId,
  //                   item: {
  //                     name: productForm.name,
  //                     description: productForm.description,
  //                     price:
  //                       typeof productForm.price === 'string'
  //                         ? Number(productForm.price.replace(/[Rp.]/g, ''))
  //                         : productForm.price,
  //                     capital_price:
  //                       typeof productForm.capitalPrice === 'string'
  //                         ? Number(
  //                             productForm.capitalPrice.replace(/[Rp.]/g, ''),
  //                           )
  //                         : productForm.capitalPrice,
  //                     category_id:
  //                       AddNewCategoryId != null
  //                         ? AddNewCategoryId.productCategory != undefined
  //                           ? AddNewCategoryId.productCategory.id
  //                           : productForm.categorySelected
  //                         : productForm.categorySelected,
  //                     deletedImages: deletedImages,
  //                     newImages: urlImages,
  //                   },
  //                   func: {
  //                     callback: () => {
  //                       refAddProduct.current.close();
  //                       handleRefresh();
  //                     },
  //                   },
  //                 };

  //                 if (
  //                   i >= uploadImages.length - 1 &&
  //                   i >= urlImages.length - 1
  //                 ) {
  //                   dispatch(TokoActions.updateProductTokoRequest(data));
  //                 }
  //               },
  //             },
  //           };

  //           dispatch(TokoActions.uploadProductImageRequest(uploadData));
  //         });
  //       }
  //     } else {
  //       if (
  //         imagesTemp.length > 0 &&
  //         productForm.name &&
  //         productForm.price &&
  //         productForm.categorySelected
  //       ) {
  //         setLoadingRBSheet(true);
  //         const data = {
  //           id: props.tokoId,
  //           productId: productForm.productId,
  //           item: {
  //             name: productForm.name,
  //             description: productForm.description,
  //             price:
  //               typeof productForm.price === 'string'
  //                 ? Number(productForm.price.replace(/[Rp.]/g, ''))
  //                 : productForm.price,
  //             capital_price:
  //               typeof productForm.capitalPrice === 'string'
  //                 ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
  //                 : productForm.capitalPrice,
  //             category_id:
  //               AddNewCategoryId != null
  //                 ? AddNewCategoryId.productCategory != undefined
  //                   ? AddNewCategoryId.productCategory.id
  //                   : productForm.categorySelected
  //                 : productForm.categorySelected,
  //             deletedImages: deletedImages,
  //           },
  //           func: {
  //             callback: () => {
  //               refAddProduct.current.close();
  //               handleRefresh();
  //             },
  //           },
  //         };
  //         dispatch(TokoActions.updateProductTokoRequest(data));
  //       }
  //     }
  //   }
  // }, [checkSuccessAddNewCategory]);

  useEffect(() => {
    if (productForm.addCategory === true) {
      refAddNewCategory.current.focus();
    }
  }, [productForm.addCategory]);

  useEffect(() => {
    if (productForm.name) {
      setProductValidation({...productValidation, isNameEmpty: false});
    }
    if (productForm.price) {
      setProductValidation({...productValidation, isPriceEmpty: false});
    }
  }, [productForm]);

  useEffect(() => {
    if (imagesTemp.length) {
      setProductValidation({...productValidation, isImageEmpty: false});
    }
  }, [imagesTemp]);

  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };

  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    dispatch(TokoActions.getProductTokoRequest(tokoId));
    setFilterProduct(initialFilter);
    setProductForm(initialProductForm);
    setLoadingRBSheet(false);
    wait(500).then(() => setRefreshing(false));
  };
  const handleRefreshAddProduct = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    dispatch(TokoActions.getProductTokoRequest(tokoId));

    setFilterProduct(initialFilter);
    setProductForm(initialProductForm);
    wait(500).then(() => setRefreshing(false));
  };
  const handleCategory = (item) => {
    setProductForm({
      ...productForm,
      categorySelected: item,
      addCategory: false,
    });
    setProductValidation({...productValidation, isCategoryEmpty: false});
  };

  const handleFilterCategory = (item) => {
    if (filterProduct.categoryId !== item) {
      const productFiltered = productData.filter((el) => {
        return el.category_id == item;
      });
      setFilterProduct({
        ...filterProduct,
        productFiltered: productFiltered,
        isFiltered: true,
        categoryId: item,
      });
    } else {
      setFilterProduct({productFiltered: [], isFiltered: false});
    }
  };

  const handleAddTokoCategory = () => {
    if (productForm.newCategory === '') {
      ToastAndroid.show(
        'Isikan Nama Kategori Terlebih Dahulu !',
        ToastAndroid.SHORT,
      );
    } else {
      setLoadingAddCategory(true);
      const data = {
        tokoId: props.tokoId,
        item: {name: productForm.newCategory},
        func: {
          reloadData: (response) => {
            setLoadingAddCategory(false);
            setProductForm({
              ...productForm,
              addCategory: false,
              categorySelected: response.productCategory.id,
            });
            setProductValidation({
              ...productValidation,
              isCategoryEmpty: false,
            });
            dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
          },
        },
      };
      dispatch(TokoActions.postCategoryTokoRequest(data));
    }
  };

  const handleDeleteProduct = () => {
    setLoadingRBSheet(true);
    const data = {
      tokoId: props.tokoId,
      productId: productForm.productId,
      func: {callback: handleRefresh},
    };
    setProductForm(initialProductForm);
    dispatch(TokoActions.deleteProductTokoRequest(data));
  };

  const handleNewProduct = () => {
    setLoadingRBSheet(false);
    setProductForm(initialProductForm);
    refAddProduct.current.open();
  };

  const handleminiUpdate = (item, type, value) => {
    const data = {
      id: tokoId,
      productId: item.id,
      item: {},
      func: {
        reloadData: handleRefresh,
      },
    };
    data.item[type] = value;
    console.log('data', data);
    dispatch(TokoActions.miniUpdateProductTokoRequest(data));
  };

  const handleUpdateProduct = (item, type, value) => {
    setLoadingRBSheet(false);
    if (type === 'edit') {
      setProductForm({
        ...productForm,
        isEdit: true,
        name: item.name,
        description: item.description,
        images: item.images,
        price: item.price,
        capitalPrice: item.capital_price,
        categorySelected: item.category_id,
        productId: item.id,
        supplier_id: item.supplier_id,
      });
      setImagesTemp(item.images);
      refAddProduct.current.open();

      setCategoryName(item.category.name);
    } else {
      setLoadingRBSheet(true);
      const data = {
        id: props.tokoId,
        productId: item.id,
        item: {},
        func: {
          callback: () => {
            refAddProduct.current.close();
            handleRefresh();
          },
        },
      };
      data.item[type] = value;

      dispatch(TokoActions.updateProductTokoRequest(data));
    }
  };
  const handleAddProduct = () => {
    setProductValidation({
      isImageEmpty: imagesTemp.length === 0 ? true : false,
      isNameEmpty: !productForm.name ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? true
          : false,
      isCategoryEmpty: !productForm.categorySelected ? true : false,
    });

    if (
      imagesTemp.length > 0 &&
      productForm.name &&
      (productForm.price === null ||
      productForm.price === 'Rp0' ||
      productForm.price === '' ||
      productForm.price == 0
        ? false
        : true) &&
      productForm.categorySelected
    ) {
      if (productForm.newCategory != '' && productForm.addCategory) {
        setLoadingRBSheet(true);

        handleAddTokoCategory();
      } else {
        setLoadingRBSheet(true);

        let urlImages = [];

        imagesTemp.forEach((img, i) => {
          const form = new FormData();
          form.append('image', {
            uri: img ? img.uri : Images.noImg,
            name: img.name,
            type: img.type,
          });

          const uploadData = {
            tokoId: tokoId,
            item: form,
            func: {
              reloadData: (url) => {
                urlImages[i] = url;
                const data = {
                  id: props.tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    price: Number(productForm.price.replace(/[Rp.]/g, '')),
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                  },
                  func: {reloadData: handleRefreshAddProduct},
                };
                const dataPost = {
                  id: props.tokoId,
                  item: {
                    name: productForm.name,
                    description: productForm.description,
                    price: Number(productForm.price.replace(/[Rp.]/g, '')),
                    capital_price:
                      typeof productForm.capitalPrice === 'string'
                        ? Number(productForm.capitalPrice.replace(/[Rp.]/g, ''))
                        : productForm.capitalPrice,
                    category_id:
                      AddNewCategoryId != null
                        ? AddNewCategoryId.productCategory != undefined
                          ? AddNewCategoryId.productCategory.id
                          : productForm.categorySelected
                        : productForm.categorySelected,
                    images: urlImages,
                  },
                  func: {reloadData: handleRefresh},
                };
                if (i >= imagesTemp.length - 1 && i >= urlImages.length - 1) {
                  setTimeout(() => {
                    dispatch(TokoActions.postProductTokoRequest(dataPost));
                  }, 1000);
                }
              },
            },
          };

          dispatch(TokoActions.uploadProductImageRequest(uploadData));

          // func.handleUploadTokoProduct(props.dispatch, props.idToko, formData, (url)=> {
          //     urlImages.push(url)

          //     if (i >= imagesTemp.length - 1) {
          //         func.handleAddTokoProduct(props.dispatch, props.idToko, data, (success) => {
          //             if (success) {
          //                 handleShowSnackBar('Berhasil menambahkan produk')
          //                 handleRefresh()
          //             }
          //         })
          //     }

          // })
        });
      }
    }
  };
  const handleSaveProduct = () => {
    setProductValidation({
      isImageEmpty: imagesTemp.length === 0 ? true : false,
      isNameEmpty: !productForm.name ? true : false,
      isPriceEmpty:
        productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? true
          : false,
      isCategoryEmpty: !productForm.categorySelected ? true : false,
    });

    let uploadImages = imagesTemp.filter((item) => {
      return item.hasOwnProperty('type');
    });

    if (uploadImages.length > 0) {
      if (
        imagesTemp.length > 0 &&
        productForm.name &&
        (productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? false
          : true) &&
        productForm.categorySelected
      ) {
        if (productForm.newCategory != '' && productForm.addCategory) {
          setLoadingRBSheet(true);
          handleAddTokoCategory();
        } else {
          setLoadingRBSheet(true);
          let urlImages = [];
          uploadImages.forEach((img, i) => {
            const form = new FormData();
            form.append('image', {
              uri: img ? img.uri : Images.noImg,
              name: img.name,
              type: img.type,
            });

            const uploadData = {
              tokoId: tokoId,
              item: form,
              func: {
                reloadData: (url) => {
                  urlImages[i] = url;

                  const data = {
                    id: props.tokoId,
                    productId: productForm.productId,
                    item: {
                      name: productForm.name,
                      description: productForm.description,
                      price:
                        typeof productForm.price === 'string'
                          ? Number(productForm.price.replace(/[Rp.]/g, ''))
                          : productForm.price,
                      category_id: productForm.categorySelected,
                      deletedImages: deletedImages,
                      newImages: urlImages,
                    },
                    func: {
                      callback: () => {
                        refAddProduct.current.close();
                        handleRefresh();
                      },
                    },
                  };

                  if (
                    i >= uploadImages.length - 1 &&
                    i >= urlImages.length - 1
                  ) {
                    dispatch(TokoActions.updateProductTokoRequest(data));
                  }
                },
              },
            };

            dispatch(TokoActions.uploadProductImageRequest(uploadData));
          });
        }
      }
    } else {
      if (
        imagesTemp.length > 0 &&
        productForm.name &&
        (productForm.price === null ||
        productForm.price === 'Rp0' ||
        productForm.price === '' ||
        productForm.price == 0
          ? false
          : true) &&
        productForm.categorySelected
      ) {
        if (productForm.newCategory !== '' && productForm.addCategory) {
          setLoadingRBSheet(true);
          handleAddTokoCategory();
        } else {
          setLoadingRBSheet(true);
          const data = {
            id: props.tokoId,
            productId: productForm.productId,
            item: {
              name: productForm.name,
              description: productForm.description,
              price:
                typeof productForm.price === 'string'
                  ? Number(productForm.price.replace(/[Rp.]/g, ''))
                  : productForm.price,
              category_id: productForm.categorySelected,
              deletedImages: deletedImages,
            },
            func: {
              callback: () => {
                refAddProduct.current.close();
                handleRefresh();
              },
            },
          };

          dispatch(TokoActions.updateProductTokoRequest(data));
        }
      }
    }
  };

  const handleShare = async (item) => {
    try {
      const result = await Share.share({
        message:
          item.description === null
            ? item.name +
              '\n' +
              func.rupiah(item.price) +
              '\n' +
              `Lihat Foto : ${Config.TOKO_NAME}${toko.link}/${item.id}/${item.safe_url} ` +
              '\n' +
              '\n' +
              ''
            : item.name +
              '\n' +
              func.rupiah(item.price) +
              '\n' +
              `Lihat Foto : ${Config.TOKO_NAME}${toko.link}/${item.id}/${item.safe_url} ` +
              '\n' +
              '\n' +
              item.description,
      });
    } catch (error) {
      alert(error.message);
    }
  };

  const categoryButton = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleCategory(item.id);
          setCategoryName(item.name);
        }}
        key={index}>
        <View
          style={{
            backgroundColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : 'white',
            borderColor:
              productForm.categorySelected === item.id
                ? c.Colors.mainBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                productForm.categorySelected === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight:
                productForm.categorySelected === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const categoryFooter = () => {
    return (
      <TouchableOpacity delayPressIn={0} onPress={handleAddNewCategory}>
        <View
          style={{
            backgroundColor: productForm.addCategory
              ? c.Colors.mainBlue
              : 'white',
            borderColor: productForm.addCategory
              ? c.Colors.mainBlue
              : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color: productForm.addCategory
                ? c.Colors.white
                : c.Colors.grayWhite,
              fontWeight: productForm.addCategory ? 'bold' : '400',
            }}>
            + Tambah Kategori
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const categoryFilter = ({item, index}) => {
    return (
      <TouchableWithoutFeedback
        delayPressIn={0}
        onPress={() => handleFilterCategory(item.id)}>
        <View
          style={{
            backgroundColor:
              filterProduct.categoryId === item.id
                ? c.Colors.mainBlue
                : 'white',
            borderColor:
              filterProduct.categoryId === item.id
                ? c.Colors.mainBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 7,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                filterProduct.categoryId === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight: filterProduct.categoryId === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const handleAddNewCategory = () => {
    setProductForm(
      {
        ...productForm,
        addCategory: true,
        categorySelected: null,
        newCategory: '',
      },
      () => {
        refAddNewCategory.current.focus();
      },
    );
  };
  const reload = () => {
    refisTokoUnavailable.current.close();
    dispatch(TokoActions.getDetailTokoRequest(tokoId));
  };
  const handlePickImage = (source) => {
    if (source === 'gallery') {
      ImagePicker.openPicker({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
        mediaType: 'photo',
      }).then((image) => {
        setImagesTemp([
          ...imagesTemp,
          {
            // id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    } else if (source === 'camera') {
      ImagePicker.openCamera({
        cropping: true,
        maxHeight: '100%',
        maxWidth: '100%',
        compressImageMaxWidth: 1179,
        compressImageMaxHeight: 579,
        includeExif: true,
        mediaType: 'photo',
      }).then((image) => {
        setImagesTemp([
          ...imagesTemp,
          {
            // id: image.modificationDate,
            name: image.modificationDate,
            uri: image.path,
            type: image.mime,
          },
        ]);
        setShowUploadModal(false);
      });
    }
  };

  const handleRemoveImage = (item) => {
    const filtered = imagesTemp.filter((e) => {
      return e.id !== item.id;
    });

    setImagesTemp(filtered);

    if (productForm.isEdit && item.hasOwnProperty('priority')) {
      productForm.isEdit && setDeletedImages([...deletedImages, item.id]);
    }
  };

  const handleCleanState = () => {
    setCityValue(null);
    setPostalCodeValue(null);
    setProvinceValue(null);
    setCityValue(null);
    setOpenPickerProvince(false);
    setOpenPickerCity(false);
    setOpenPickerDistrict(false);
    setOpenPickerPostalCode(false);
    setDistrictValue(null);
    setDetailAddressValue(null);
    setAddressIdValue(0);
    setAddressValue(null);
    setAddedAddressFormValidation(initialValidation);
  };
  const handleCleanTemp = () => {
    console.log('kepanggil');
    setCategoryName(null);
    setImagesTemp([]);
    setProductForm({...productForm, isEdit: false});
    setDeletedImages([]);
    setNewImages([]);
    setloadingStep(false);
    setProductValidation(initialValidation);
    dispatch(TokoActions.postCategoryTokoReset());
  };

  const productList = ({item, index}) => {
    const displayImage = item.images.find((el) => {
      return el.priority === 1 ? el.priority === 1 : el;
    });
    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(TokoActions.getDetailProductReset());
          dispatch(
            TokoActions.getDetailProductRequest({
              tokoId: tokoId,
              productId: item.id,
            }),
          );

          navigation.navigate('ProductEditScreen', {
            item: item,
          });
        }}
        //onPress={() => handleUpdateProduct(item, 'edit')}
        delayPressIn={0}
        key={index}>
        <View
          key={item.id}
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderBottomColor: '#dddddd',
            borderBottomWidth: 0.75,
          }}>
          <c.Image
            style={{
              width: 75,
              height: 75,
              marginRight: 13,
              borderRadius: 5,
              borderWidth: 0.2,
              borderColor: c.Colors.gray,
            }}
            source={{
              uri: displayImage
                ? displayImage.url
                : 'https://payok-assets-bucket.s3-ap-southeast-1.amazonaws.com/mobile/upload_photo.png',
            }}
          />

          <View style={{flex: 1}}>
            <Text style={{fontWeight: 'bold', color: '#636363', fontSize: 14}}>
              {item.name}
            </Text>
            <Text
              style={{
                color: c.Colors.grayWhiteDark,
                fontSize: 13,
                marginVertical: 2,
              }}>
              {func.rupiah(item.price)}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 5,
                alignItems: 'center',
              }}>
              {item.category !== null ? (
                <View
                  style={{
                    paddingVertical: 3,
                    paddingHorizontal: 7,
                    marginRight: 10,
                    borderColor: c.Colors.mainBlue,
                    borderWidth: 1,
                    borderRadius: 3,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 13,
                      color: c.Colors.mainBlue,
                    }}>
                    {item.category != null && item.category.name}
                  </Text>
                </View>
              ) : null}

              {item.status === 'active' ? (
                <View
                  style={{
                    paddingVertical: 3,
                    paddingHorizontal: 7,
                    marginRight: 10,
                    borderColor: c.Colors.greenReport,
                    borderWidth: 1,
                    borderRadius: 3,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 13,
                      color: c.Colors.greenReport,
                    }}>
                    Aktif
                  </Text>
                </View>
              ) : (
                <View
                  style={{
                    paddingVertical: 3,
                    paddingHorizontal: 7,
                    marginRight: 10,
                    borderColor: c.Colors.grayWhite,
                    borderWidth: 1,
                    borderRadius: 3,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 13,
                      color: c.Colors.grayWhite,
                    }}>
                    Non-Aktif
                  </Text>
                </View>
              )}
              <View style={{flexDirection: 'row'}}>
                {item.supplier_id != null && (
                  <Image
                    style={{width: 27, height: 27}}
                    source={Images.purpleStoreIcon}
                  />
                )}
                {item.featured && (
                  <MaterialCommunityIcons
                    style={{marginLeft: 2}}
                    name="star"
                    size={27}
                    color={c.Colors.mainBlue}
                  />
                )}
              </View>
            </View>
          </View>
          <Menu>
            <MenuTrigger
              children={
                <MaterialCommunityIcons
                  name="dots-vertical"
                  size={21}
                  style={{padding: 5}}
                />
              }
            />
            <MenuOptions>
              <MenuOption
                onSelect={() =>
                  item.featured
                    ? handleminiUpdate(item, 'featured', false)
                    : handleminiUpdate(item, 'featured', true)
                }
                value={1}
                style={[
                  c.Styles.borderBottom,
                  {flexDirection: 'row', padding: 10},
                ]}>
                <MaterialCommunityIcons
                  name="star"
                  size={21}
                  color={item.featured ? c.Colors.grayWhite : c.Colors.mainBlue}
                  style={{marginRight: 10}}
                />
                {item.featured ? (
                  <Text>Non-utamakan</Text>
                ) : (
                  <Text>Utamakan</Text>
                )}
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  item.status === 'active'
                    ? handleminiUpdate(item, 'status', 'inactive')
                    : handleminiUpdate(item, 'status', 'active')
                }
                style={[
                  c.Styles.borderBottom,
                  {flexDirection: 'row', padding: 10},
                ]}>
                <MaterialCommunityIcons
                  name="eye"
                  size={21}
                  color={c.Colors.grayWhite}
                  style={{marginRight: 10}}
                />
                <Text>
                  {item.status === 'active' ? 'Non-aktifkan' : 'Aktifkan'}
                </Text>
              </MenuOption>
              <MenuOption
                onSelect={() => handleShare(item)}
                style={[
                  c.Styles.borderBottom,
                  {flexDirection: 'row', padding: 10},
                ]}>
                <MaterialCommunityIcons
                  name="share-variant"
                  size={21}
                  color={c.Colors.grayWhite}
                  style={{marginRight: 10}}
                />
                <Text>Bagikan</Text>
              </MenuOption>
              <MenuOption
                onSelect={() => {
                  dispatch(TokoActions.getDetailProductReset());
                  dispatch(
                    TokoActions.getDetailProductRequest({
                      tokoId: tokoId,
                      productId: item.id,
                    }),
                  );

                  navigation.navigate('ProductEditScreen', {
                    item: item,
                  });
                }}
                style={[
                  c.Styles.borderBottom,
                  {flexDirection: 'row', padding: 10},
                ]}>
                <MaterialCommunityIcons
                  name="pencil"
                  size={21}
                  color={c.Colors.grayWhite}
                  style={{marginRight: 10}}
                />
                <Text>Edit</Text>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </View>
      </TouchableOpacity>
    );
  };

  const addProductList = () => (
    <View
      style={{
        width: '100%',
        height: Dimensions.get('window').height * 1,
        backgroundColor: 'red',
        backgroundColor: '#ffffff',
        paddingLeft: 25,
        paddingRight: 25,
      }}></View>
  );

  const productImageList = ({item, index}) => {
    return (
      <View>
        <c.Image
          source={{uri: item.uri || item.url}}
          style={{
            width: 100,
            height: 100,
            borderRadius: 5,
            marginVertical: 10,
            marginRight: 10,
            borderColor: c.Colors.grayDark,
            borderWidth: 0.5,
          }}
        />
        <TouchableOpacity
          delayPressIn={0}
          onPress={() => handleRemoveImage(item)}
          style={{
            padding: 5,
            position: 'absolute',
            right: 5,
            borderRadius: 25,
            backgroundColor: c.Colors.gray,
          }}>
          <MaterialCommunityIcons name="close-thick" size={17} color="white" />
        </TouchableOpacity>
      </View>
    );
  };

  const productImageListFooter = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => setShowUploadModal(true)}>
        <c.Image
          source={{
            uri:
              'https://payok-assets-bucket.s3-ap-southeast-1.amazonaws.com/mobile/upload_photo.png',
          }}
          style={{
            width: 100,
            height: 100,
            borderRadius: 5,
            marginVertical: 10,
            marginRight: 10,
          }}
        />
      </TouchableOpacity>
    );
  };
  const productAvail = () => {
    return (
      <View style={c.Styles.flexOne}>
        {snackBar.show && (
          <Snackbar
            visible={snackBar.show}
            duration={3000}
            style={{zIndex: 10}}
            onDismiss={() => setSnackBar({show: false, message: ''})}
            action={{
              label: 'OK',
              onPress: () => setSnackBar({show: false, message: ''}),
            }}>
            {snackBar.message}
          </Snackbar>
        )}
        <View
          style={[
            c.Styles.borderBottom,
            {flexDirection: 'row', minHeight: 46},
          ]}>
          <View style={{justifyContent: 'center', marginHorizontal: 10}}>
            {/* <AntDesign name="search1" size={25} color="grey" /> */}
          </View>
          <TouchableOpacity
            onPress={_handleNavigation}
            style={{justifyContent: 'center', marginRight: 10}}>
            <Foundation name={'list-bullet'} color={'#c9c6c6'} size={25} />
          </TouchableOpacity>

          {productData ? (
            productData.length ? (
              <DraggableFlatList
                style={[
                  c.Styles.flexRow,
                  {paddingVertical: 4.5, marginRight: 20},
                ]}
                renderItem={categoryFilter}
                data={categoryData ? categoryData : null}
                showsHorizontalScrollIndicator={false}
                horizontal
                nestedScrollEnabled={true}
                keyExtractor={(item, index) => index.toString()}
              />
            ) : null
          ) : null}
        </View>
        <ScrollView
          keyboardShouldPersistTaps={'always'}
          scrollEnabled={
            productData ? (productData.length ? true : false) : false
          }>
          <View>
            {productData ? (
              productData.length ? (
                <View>
                  <FlatList
                    refreshControl={
                      <RefreshControl
                        refreshing={refreshing}
                        onRefresh={handleRefresh}
                      />
                    }
                    renderItem={productList}
                    data={
                      filterProduct.isFiltered
                        ? filterProduct.productFiltered
                        : productData
                    }
                    keyExtractor={(item, index) => index.toString()}
                    style={{marginBottom: 75}}
                  />
                </View>
              ) : (
                !filterProduct.isFiltered && addProductList()
              )
            ) : (
              <View>
                <Spinner color={c.Colors.mainBlue} />
              </View>
            )}
          </View>

          {filterProduct.isFiltered &&
          filterProduct.productFiltered.length === 0 ? (
            <View
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                height: Dimensions.get('window').height * 0.5,
              }}>
              <View
                style={{
                  backgroundColor: c.Colors.mainBlue,
                  display: 'flex',
                  width: 75,
                  height: 75,
                  borderRadius: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Ionicons
                  name="file-tray-outline"
                  size={35}
                  color={c.Colors.white}
                />
              </View>
              <View style={{marginVertical: 20, alignItems: 'center'}}>
                <Text
                  style={{fontWeight: 'bold', fontSize: 15, marginBottom: 5}}>
                  Oops!
                </Text>
                <Text style={{color: c.Colors.grayWhiteDark}}>
                  Tidak ada produk di kategori ini.
                </Text>
              </View>
            </View>
          ) : null}
        </ScrollView>
      </View>
    );
  };
  const productUnavail = () => {
    return (
      <Content
        contentContainerStyle={{
          width: '100%',
          backgroundColor: '#ffffff',
          paddingLeft: 25,
          paddingRight: 25,
          paddingTop: 15,
          paddingBottom: 15,
        }}>
        <Modal
          onBackdropPress={() => setShowUploadModal(false)}
          isVisible={showUploadModal}
          deviceHeight={Math.max(
            Dimensions.get('window').height,
            Dimensions.get('screen').height,
          )}
          style={{margin: 20}}>
          <View
            style={[
              c.Styles.paddingTwenty,
              {
                borderRadius: 5,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}>
            <View style={[c.Styles.paddingTwenty]}>
              <Text style={[c.Styles.txtBold]}>Pilih Gambar Dari</Text>
            </View>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => handlePickImage('gallery')}
              style={[c.Styles.btnImagePicker, c.Styles.flexRow]}>
              <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                <MaterialCommunityIcons
                  name="image-outline"
                  size={25}
                  color={c.Colors.mainBlue}
                />
                <c.Text
                  style={[
                    c.Styles.paddingTen,
                    c.Styles.txtTncDefault,
                    c.Styles.txtmainBlue,
                  ]}
                  txt="Gallery"
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => handlePickImage('camera')}
              style={[c.Styles.btnImagePicker, c.Styles.flexRow]}>
              <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                <MaterialCommunityIcons
                  name="camera-outline"
                  size={25}
                  color={c.Colors.mainBlue}
                />
                <c.Text
                  style={[
                    c.Styles.paddingTen,
                    c.Styles.txtTncDefault,
                    c.Styles.txtmainBlue,
                  ]}
                  txt="Camera"
                />
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <View
          style={{
            width: '100%',
            borderWidth: 1,
            borderColor: isVerified ? '#c9c6c6' : '#1a69d5',
            borderRadius: 5,
            padding: 15,
          }}>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              fontSize: 16,
              letterSpacing: 1.3,
              color: isVerified ? '#c9c6c6' : '#1a69d5',
            }}>
            {tokoData ? 'Lengkapi data toko kamu' : 'STEP 1'}
          </Text>
          <Text
            style={{
              fontFamily: 'NotoSans-Bold',
              fontSize: 12,
              letterSpacing: 1,
              color: isVerified ? '#c9c6c6' : '#4a4a4a',
              marginTop: 5,
            }}>
            {(tokoData && userData.data.is_phone_verified === null) ||
            (tokoData === null && userData.data.is_phone_verified === null)
              ? 'Verifikasi nomor whatsapp kamu'
              : tokoData && !addressVerified
              ? 'Masukkan alamat kamu'
              : 'Buat toko kamu sekarang!'}
          </Text>
          <View style={{paddingTop: 5, paddingBottom: 5}}>
            <View style={[c.Styles.flexRow, {marginTop: 5}]}>
              <MaterialCommunityIcons
                name="whatsapp"
                size={25}
                color={
                  isVerified
                    ? '#c9c6c6'
                    : userData.data.is_phone_verified === null
                    ? '#c9c6c6'
                    : c.Colors.mainBlue
                }
              />
              <Ionicons
                size={25}
                name="md-remove-outline"
                color={
                  isVerified
                    ? '#c9c6c6'
                    : userData.data.is_phone_verified === null
                    ? '#c9c6c6'
                    : c.Colors.mainBlue
                }
              />
              <MaterialCommunityIcons
                size={25}
                name="storefront"
                color={
                  isVerified
                    ? '#c9c6c6'
                    : tokoData
                    ? c.Colors.mainBlue
                    : '#c9c6c6'
                }
              />
              <Ionicons
                size={25}
                name="md-remove-outline"
                color={
                  isVerified
                    ? '#c9c6c6'
                    : tokoData
                    ? c.Colors.mainBlue
                    : '#c9c6c6'
                }
              />
              <SimpleLineIcons
                size={25}
                name="location-pin"
                color={
                  isVerified
                    ? '#c9c6c6'
                    : tokoAddressData !== null
                    ? tokoAddressData.listAddress.length
                      ? c.Colors.mainBlue
                      : '#c9c6c6'
                    : '#c9c6c6'
                }
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              if (!isVerified) {
                if (userData.data.is_phone_verified === null) {
                  let dataStep = {
                    modalVisible: true,
                    step: 1,
                  };
                  dispatch(GlobalActions.setModalVisible(dataStep));
                } else if (tokoId === null) {
                  let dataStep = {
                    modalVisible: true,
                    step: 2,
                  };
                  dispatch(GlobalActions.setModalVisible(dataStep));
                } else if (!tokoAddress) {
                  let dataStep = {
                    modalVisible: true,
                    step: 3,
                  };
                  setIsAddedAddress(true);
                  dispatch(GlobalActions.setModalVisible(dataStep));
                }
                // bottomSheetRef.current.open();
              }
            }}
            disabled={isVerified ? true : false}
            delayPressIn={0}
            style={{
              width:
                (tokoData && userData.data.is_phone_verified === null) ||
                (tokoData === null && userData.data.is_phone_verified === null)
                  ? '30%'
                  : tokoData && !addressVerified
                  ? '50%'
                  : '30%',
              height: 35,
              backgroundColor: isVerified ? '#c9c6c6' : '#1a69d5',
              borderRadius: 25,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 5,
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 12,
                letterSpacing: 1,
                color: !isVerified ? '#ffffff' : '#c9c6c6',

                alignSelf: 'center',
              }}>
              {(tokoData && userData.data.is_phone_verified === null) ||
              (tokoData === null && userData.data.is_phone_verified === null)
                ? 'Verifikasi'
                : tokoData && !addressVerified
                ? 'Masukkan alamat'
                : 'Buat toko'}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            borderColor: isVerified ? c.Colors.mainBlue : 'transparent',
            borderWidth: 1,
            backgroundColor: isVerified ? '#fff' : '#f4f4f4',
            borderRadius: 5,
            padding: 15,
            marginTop: '5%',
          }}>
          <View>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 16,
                letterSpacing: 1.3,
                color: isVerified ? c.Colors.mainBlue : '#9b9b9b',
              }}>
              STEP 2
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 12,
                letterSpacing: 1,
                color: isVerified ? '#4a4a4a' : '#9b9b9b',

                marginTop: 8,
              }}>
              Tambah produk kamu sendiri
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              isVerified ? navigation.navigate('ProductAddScreen') : null;
            }}
            style={{
              borderWidth: 1,
              borderColor: isVerified ? '#c9c6c6' : '#c9c6c6',
              marginTop: '3%',
              borderRadius: 4,
            }}>
            <View style={[c.Styles.flexRow, {padding: 15}]}>
              <Card style={{backgroundColor: '#fff', padding: 17}}>
                <Image
                  style={{width: 35, height: 35, alignSelf: 'center'}}
                  source={Images.noImg}
                />
              </Card>
              <View style={{justifyContent: 'space-around', marginLeft: '5%'}}>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    color: isVerified ? '#9b9b9b' : '#9b9b9b',
                  }}>
                  Nama barang
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    color: isVerified ? '#9b9b9b' : '#9b9b9b',
                  }}>
                  Rp 0
                </Text>
                <View>
                  <Text
                    style={{
                      fontFamily: 'NotoSans-Bold',
                      fontSize: 12,
                      color: isVerified ? '#9b9b9b' : '#9b9b9b',
                      borderWidth: 1,
                      borderColor: isVerified ? '#9b9b9b' : '#9b9b9b',
                      padding: 2,
                      borderRadius: 5,
                      textAlign: 'center',
                    }}>
                    Kategori
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <View>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 12,
                letterSpacing: 1,
                color: isVerified ? '#4a4a4a' : '#9b9b9b',

                marginTop: 22,
              }}>
              Bingung mau jual apa?
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans',
                fontSize: 12,
                letterSpacing: 1.2,
                color: isVerified ? '#4a4a4a' : '#9b9b9b',

                marginTop: 2,
              }}>
              Pilih dari katalog Toko OK dan Marketplace sekarang
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              isVerified ? navigation.navigate('Dropship') : null;
            }}
            style={{
              borderWidth: 1,
              borderColor: isVerified ? '#c9c6c6' : '#c9c6c6',
              borderRadius: 4,
              marginTop: '5%',
            }}>
            <Image
              style={{
                width: 290,
                height: 150,
                alignSelf: 'center',
                resizeMode: 'contain',
              }}
              source={Images.dropshipimglogo}
            />
          </TouchableOpacity>
        </View>
      </Content>
    );
  };
  const tokoAvail = () => {
    return checkProductAvail ? productAvail() : productUnavail();
  };
  const tokoUnavail = () => {
    return (
      <Content
        contentContainerStyle={{
          width: '100%',
          height: '100%',
        }}>
        <View
          style={{
            backgroundColor: '#ffffff',
            paddingLeft: 25,
            paddingRight: 25,
            paddingTop: 15,
            paddingBottom: 15,
          }}>
          <View
            style={{
              width: '100%',
              borderWidth: 1,
              borderColor: isVerified ? '#c9c6c6' : '#1a69d5',
              borderRadius: 5,
              padding: 15,
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 16,
                letterSpacing: 1.3,
                color: isVerified ? '#c9c6c6' : '#1a69d5',
              }}>
              {tokoData ? 'Lengkapi data toko kamu' : 'STEP 1'}
            </Text>
            <Text
              style={{
                fontFamily: 'NotoSans-Bold',
                fontSize: 12,
                letterSpacing: 1,
                color: isVerified ? '#c9c6c6' : '#4a4a4a',
                marginTop: 5,
              }}>
              {(tokoData && userData.data.is_phone_verified === null) ||
              (tokoData === null && userData.data.is_phone_verified === null)
                ? 'Verifikasi nomor whatsapp kamu'
                : tokoData && !addressVerified
                ? 'Masukkan alamat kamu'
                : 'Buat toko kamu sekarang!'}
            </Text>
            <View style={{paddingTop: 5, paddingBottom: 5}}>
              <View style={[c.Styles.flexRow, {marginTop: 5}]}>
                <MaterialCommunityIcons
                  name="whatsapp"
                  size={25}
                  color={
                    isVerified
                      ? '#c9c6c6'
                      : userData.data.is_phone_verified === null
                      ? '#c9c6c6'
                      : c.Colors.mainBlue
                  }
                />
                <Ionicons
                  size={25}
                  name="md-remove-outline"
                  color={
                    isVerified
                      ? '#c9c6c6'
                      : userData.data.is_phone_verified === null
                      ? '#c9c6c6'
                      : c.Colors.mainBlue
                  }
                />
                <MaterialCommunityIcons
                  size={25}
                  name="storefront"
                  color={
                    isVerified
                      ? '#c9c6c6'
                      : tokoData
                      ? c.Colors.mainBlue
                      : '#c9c6c6'
                  }
                />
                <Ionicons
                  size={25}
                  name="md-remove-outline"
                  color={
                    isVerified
                      ? '#c9c6c6'
                      : tokoAddressData !== null
                      ? tokoAddressData.listAddress.length
                        ? c.Colors.mainBlue
                        : '#c9c6c6'
                      : '#c9c6c6'
                  }
                />
                <SimpleLineIcons
                  size={25}
                  name="location-pin"
                  color={
                    isVerified
                      ? '#c9c6c6'
                      : tokoAddressData !== null
                      ? tokoAddressData.listAddress.length
                        ? c.Colors.mainBlue
                        : '#c9c6c6'
                      : '#c9c6c6'
                  }
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                if (!isVerified) {
                  if (userData.data.is_phone_verified === null) {
                    let dataStep = {
                      modalVisible: true,
                      step: 1,
                    };

                    dispatch(GlobalActions.setModalVisible(dataStep));
                  } else if (tokoId === null) {
                    let dataStep = {
                      modalVisible: true,
                      step: 2,
                    };

                    dispatch(GlobalActions.setModalVisible(dataStep));
                  } else if (!tokoAddress) {
                    let dataStep = {
                      modalVisible: true,
                      step: 3,
                    };
                    setIsAddedAddress(true);

                    dispatch(GlobalActions.setModalVisible(dataStep));
                  }
                  //bottomSheetRef.current.open();
                }
              }}
              disabled={isVerified ? true : false}
              delayPressIn={0}
              style={{
                width:
                  (tokoData && userData.data.is_phone_verified === null) ||
                  (tokoData === null &&
                    userData.data.is_phone_verified === null)
                    ? '30%'
                    : tokoData && !addressVerified
                    ? '50%'
                    : '30%',
                height: 35,
                backgroundColor: isVerified ? '#c9c6c6' : '#1a69d5',
                borderRadius: 25,
                alignItems: 'center',
                justifyContent: 'center',

                marginTop: 5,
              }}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: !isVerified ? '#ffffff' : '#c9c6c6',

                  alignSelf: 'center',
                }}>
                {(tokoData && userData.data.is_phone_verified === null) ||
                (tokoData === null && userData.data.is_phone_verified === null)
                  ? 'Verifikasi'
                  : tokoData && !addressVerified
                  ? 'Masukkan alamat'
                  : 'Buat toko'}
              </Text>
            </TouchableOpacity>
          </View>
          {productData.length > 0 ? (
            productAvail()
          ) : (
            <View
              style={{
                borderColor: isVerified ? c.Colors.mainBlue : 'transparent',
                borderWidth: 1,
                backgroundColor: isVerified ? '#fff' : '#f4f4f4',
                borderRadius: 5,
                padding: 15,
                marginTop: '5%',
              }}>
              <View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 16,
                    letterSpacing: 1.3,
                    color: isVerified ? c.Colors.mainBlue : '#9b9b9b',
                  }}>
                  STEP 2
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 1,
                    color: isVerified ? '#4a4a4a' : '#9b9b9b',

                    marginTop: 8,
                  }}>
                  Tambah produk kamu sendiri
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  isVerified ? navigation.navigate('ProductAddScreen') : null;
                }}
                style={{
                  borderWidth: 1,
                  borderColor: isVerified ? '#c9c6c6' : '#c9c6c6',
                  marginTop: '3%',
                  borderRadius: 4,
                }}>
                <View style={[c.Styles.flexRow, {padding: 15}]}>
                  <Card style={{backgroundColor: '#fff', padding: 17}}>
                    <Image
                      style={{width: 35, height: 35, alignSelf: 'center'}}
                      source={Images.noImg}
                    />
                  </Card>
                  <View
                    style={{justifyContent: 'space-around', marginLeft: '5%'}}>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 12,
                        color: isVerified ? '#9b9b9b' : '#9b9b9b',
                      }}>
                      Nama barang
                    </Text>
                    <Text
                      style={{
                        fontFamily: 'NotoSans-Bold',
                        fontSize: 12,
                        color: isVerified ? '#9b9b9b' : '#9b9b9b',
                      }}>
                      Rp 0
                    </Text>
                    <View>
                      <Text
                        style={{
                          fontFamily: 'NotoSans-Bold',
                          fontSize: 12,
                          color: isVerified ? '#9b9b9b' : '#9b9b9b',
                          borderWidth: 1,
                          borderColor: isVerified ? '#9b9b9b' : '#9b9b9b',
                          padding: 2,
                          borderRadius: 5,
                          textAlign: 'center',
                        }}>
                        Kategori
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              <View>
                <Text
                  style={{
                    fontFamily: 'NotoSans-Bold',
                    fontSize: 12,
                    letterSpacing: 1,
                    color: isVerified ? '#4a4a4a' : '#9b9b9b',

                    marginTop: 22,
                  }}>
                  Bingung mau jual apa?
                </Text>
                <Text
                  style={{
                    fontFamily: 'NotoSans',
                    fontSize: 12,
                    letterSpacing: 1.2,
                    color: isVerified ? '#4a4a4a' : '#9b9b9b',

                    marginTop: 2,
                  }}>
                  Pilih dari katalog Toko OK dan Marketplace sekarang
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  isVerified ? navigation.navigate('Dropship') : null;
                }}
                style={{
                  borderWidth: 1,
                  borderColor: isVerified ? '#c9c6c6' : '#c9c6c6',
                  borderRadius: 4,
                  marginTop: '5%',
                }}>
                <Image
                  style={{
                    width: 290,
                    height: 150,
                    alignSelf: 'center',
                    resizeMode: 'contain',
                  }}
                  source={Images.dropshipimglogo}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {/* <TouchableOpacity onPress={() => bottomSheetRef.current.open()}>
          <Text>Click Me</Text>
        </TouchableOpacity> */}
        <View>
          {isTokoAvailable && isVerified ? (
            <View>
              <View
                style={[
                  c.Styles.borderBottom,
                  {flexDirection: 'row', minHeight: 46},
                ]}>
                <View style={{justifyContent: 'center', marginHorizontal: 10}}>
                  {/* <AntDesign name="search1" size={25} color="grey" /> */}
                </View>
                <TouchableOpacity
                  onPress={_handleNavigation}
                  style={{justifyContent: 'center', marginRight: 10}}>
                  <Foundation
                    name={'list-bullet'}
                    color={'#c9c6c6'}
                    size={25}
                  />
                </TouchableOpacity>
                {productData ? (
                  productData.length ? (
                    <DraggableFlatList
                      style={[
                        c.Styles.flexRow,
                        {paddingVertical: 4.5, marginRight: 20},
                      ]}
                      renderItem={categoryFilter}
                      data={categoryData ? categoryData : null}
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      nestedScrollEnabled={true}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  ) : null
                ) : null}
              </View>

              {productData ? (
                productData.length ? (
                  <View>
                    <FlatList
                      refreshControl={
                        <RefreshControl
                          refreshing={refreshing}
                          onRefresh={handleRefresh}
                        />
                      }
                      renderItem={productList}
                      data={
                        filterProduct.isFiltered
                          ? filterProduct.productFiltered
                          : productData
                      }
                      keyExtractor={(item, index) => index.toString()}
                      style={{marginBottom: 75}}
                    />
                  </View>
                ) : (
                  !filterProduct.isFiltered && addProductList()
                )
              ) : (
                <View>
                  <Spinner color={c.Colors.mainBlue} />
                </View>
              )}
            </View>
          ) : null}
        </View>
      </Content>
    );
  };
  const handleCloseRBSheet = () => {
    refAddProduct.current.close();
    setProductValidation(initialValidation);
  };

  return (
    <Container>
      {tokoFetching && tokoData === null && (
        <View
          style={[
            c.Styles.justifyCenter,
            c.Styles.alignItemsCenter,
            {height: Dimensions.get('screen').height * 0.9},
          ]}>
          <ActivityIndicator size={'large'} color={c.Colors.mainBlue} />
        </View>
      )}
      {tokoId === null ? (
        <ScrollView>
          <View style={{flex: 1}}>{tokoUnavail()}</View>
        </ScrollView>
      ) : (
        <View>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
              />
            }
            keyboardShouldPersistTaps={'handled'}>
            <View>
              <Modal
                onBackdropPress={() => setShowUploadModal(false)}
                isVisible={showUploadModal}
                deviceHeight={Math.max(
                  Dimensions.get('window').height,
                  Dimensions.get('screen').height,
                )}
                style={{margin: 20}}>
                <View
                  style={[
                    c.Styles.paddingTwenty,
                    {
                      borderRadius: 5,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                  ]}>
                  <View style={[c.Styles.paddingTwenty]}>
                    <Text style={[c.Styles.txtBold]}>Pilih Gambar Dari</Text>
                  </View>
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => handlePickImage('gallery')}
                    style={[c.Styles.btnImagePicker, c.Styles.flexRow]}>
                    <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                      <MaterialCommunityIcons
                        name="image-outline"
                        size={25}
                        color={c.Colors.mainBlue}
                      />
                      <c.Text
                        style={[
                          c.Styles.paddingTen,
                          c.Styles.txtTncDefault,
                          c.Styles.txtmainBlue,
                        ]}
                        txt="Gallery"
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    delayPressIn={0}
                    onPress={() => handlePickImage('camera')}
                    style={[c.Styles.btnImagePicker, c.Styles.flexRow]}>
                    <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                      <MaterialCommunityIcons
                        name="camera-outline"
                        size={25}
                        color={c.Colors.mainBlue}
                      />
                      <c.Text
                        style={[
                          c.Styles.paddingTen,
                          c.Styles.txtTncDefault,
                          c.Styles.txtmainBlue,
                        ]}
                        txt="Camera"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </Modal>

              <RBSheet
                onClose={handleCleanState}
                ref={refShareToko}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={Dimensions.get('window').height * 0.4}
                openDuration={250}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 25,
                    borderTopRightRadius: 25,
                  },
                  draggableIcon: {
                    backgroundColor: '#D8D8D8',
                    width: 59.5,
                  },
                }}>
                <View>
                  <View style={styles.viewBagikanToko}>
                    <Text style={styles.txtShare}>Bagikan Toko</Text>
                  </View>
                  <View style={styles.viewFirstItem}>
                    <View
                      style={[
                        styles.viewFirstItemList,
                        {
                          borderBottomWidth: 0.5,
                          borderBottomColor: '#c9c6c6',
                        },
                      ]}>
                      <View style={styles.viewLeft}>
                        <TouchableOpacity
                          delayPressIn={0}
                          onPress={() =>
                            Linking.openURL(Config.TOKO_NAME + toko.link)
                          }
                          style={styles.viewLeftItem}>
                          <MaterialCommunityIcons
                            name="web"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                          <Text numberOfLines={1} style={styles.txtLink}>
                            {(Config.TOKO_NAME + toko.link).length <= 32
                              ? Config.TOKO_NAME + toko.link
                              : (Config.TOKO_NAME + toko.link).substring(
                                  0,
                                  32,
                                ) + '...'}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={handleCopyLink}
                          delayPressIn={0}>
                          <MaterialCommunityIcons
                            name="content-copy"
                            size={25}
                            color={c.Colors.grayWhite}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.viewFirstItemList}>
                      <View style={[styles.viewLeft]}>
                        <View style={styles.viewLeftItem}>
                          <MaterialCommunityIcons
                            name="eye"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                          <Text numberOfLines={1} style={styles.txtItem}>
                            Link dilihat
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.txtDilihat}>
                            {toko.total_visit}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() =>
                        func.handleShare({
                          message: messageShareToko,
                        })
                      }
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <MaterialCommunityIcons
                            name="share-variant"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                          <Text numberOfLines={1} style={styles.txtItem}>
                            Bagikan Toko Anda
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() =>
                        Linking.openURL(
                          `whatsapp://send?text=${window.encodeURIComponent(
                            message,
                          )}`,
                        ).catch(() =>
                          Linking.openURL(
                            `https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US`,
                          ),
                        )
                      }
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <MaterialCommunityIcons
                            name="whatsapp"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                          <Text numberOfLines={1} style={styles.txtItem}>
                            Bagikan Lewat Whatsapp
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </RBSheet>
              <RBSheet
                onClose={handleCleanState}
                ref={refSettingsToko}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={Dimensions.get('window').height * 0.6}
                openDuration={250}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 25,
                    borderTopRightRadius: 25,
                  },
                  draggableIcon: {
                    backgroundColor: '#D8D8D8',
                    width: 59.5,
                  },
                }}>
                <View>
                  <View style={styles.viewBagikanToko}>
                    <Text style={styles.txtShare}>Pengaturan Toko</Text>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => openRBSheet('toko-address')}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <MaterialCommunityIcons
                              name="map-marker"
                              size={25}
                              color={c.Colors.mainBlue}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Alamat Toko
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => {
                        refSettingsToko.current.close();
                        setTimeout(() => {
                          navigation.push('ShippingMethodScreen');
                        }, 500);
                      }}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <Feather
                              name="truck"
                              size={25}
                              color={c.Colors.mainBlue}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Metode Pengiriman
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => openRBSheet('rek-bank')}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <MaterialCommunityIcons
                              name="credit-card"
                              size={25}
                              color={c.Colors.mainBlue}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Rekening Toko
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={_handleNavigation}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <Foundation
                              name={'list-bullet'}
                              color={c.Colors.mainBlue}
                              size={25}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Kategori Penjualan
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => openRBSheet('e-commerce')}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <MaterialCommunityIcons
                              name="shopping-outline"
                              size={25}
                              color={c.Colors.mainBlue}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Link E-commerce
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.viewborderBottom}>
                    <TouchableOpacity
                      delayPressIn={0}
                      onPress={() => openRBSheet('socmed')}
                      style={styles.viewFirstItemList}>
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            width: '100%',
                          }}>
                          <View style={{width: '10%'}}>
                            <MaterialCommunityIcons
                              name="instagram"
                              size={25}
                              color={c.Colors.mainBlue}
                            />
                          </View>
                          <View style={{width: '90%'}}>
                            <Text numberOfLines={1} style={styles.txtItem}>
                              Link Social media
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </RBSheet>
              <RBSheet
                onClose={handleCleanState}
                ref={refisTokoAvailable}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={Dimensions.get('window').height * 0.8}
                openDuration={250}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 25,
                    borderTopRightRadius: 25,
                  },
                  draggableIcon: {
                    backgroundColor: '#D8D8D8',
                    width: 59.5,
                  },
                }}>
                <Content
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    marginBottom: 50,
                    height: '100%',
                    justifyContent: 'space-between',
                    paddingHorizontal: 20,
                  }}>
                  {renderEditMerchant()}
                </Content>
              </RBSheet>
              <RBSheet
                onClose={handleCleanState}
                ref={refisTokoUnavailable}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={Dimensions.get('window').height * 0.8}
                openDuration={250}
                customStyles={{
                  container: {
                    borderTopLeftRadius: 25,
                    borderTopRightRadius: 25,
                  },
                  draggableIcon: {
                    backgroundColor: '#D8D8D8',
                    width: 59.5,
                  },
                }}>
                <Content
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    marginBottom: 50,
                    height: '100%',
                    paddingHorizontal: 20,
                    justifyContent: 'space-between',
                  }}>
                  {createMerchant()}
                  {step !== 4 ? (
                    <Button
                      disabled={step === 1 && code.length < 4 ? true : false}
                      onPress={stepNext}
                      style={[
                        c.Styles.btnPrimary,
                        {
                          paddingHorizontal: 20,
                          marginTop: '10%',
                          marginBottom: '10%',
                          backgroundColor:
                            step === 1 && code.length < 4
                              ? '#c9c6c6'
                              : c.Colors.mainBlue,
                        },
                      ]}>
                      {loadingStep ? (
                        <Spinner size={18} color="#fff" />
                      ) : (
                        <Text style={c.Styles.txtCreateMerchantBtn}>
                          {step === 1
                            ? 'Verifikasi'
                            : step === 2
                            ? 'Buat Toko'
                            : step === 3
                            ? 'Selesai'
                            : null}
                        </Text>
                      )}
                    </Button>
                  ) : null}
                </Content>
              </RBSheet>
              {tokoData !== null ? (
                <View style={styles.viewHeader}>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      justifyContent: 'space-between',
                      padding: 20,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '70%',
                        justifyContent: 'space-around',
                      }}>
                      <View>
                        <TouchableOpacity
                          delayPressIn={0}
                          onPress={() => setShowUploadModal(true)}
                          activeOpacity={0.75}>
                          <View style={[c.Styles.flexRow]}>
                            {toko.icon !== null ? (
                              <c.Image
                                source={
                                  toko.icon ? {uri: toko.icon} : Images.noImg
                                }
                                style={
                                  toko.icon
                                    ? {
                                        width: 60,
                                        height: 60,
                                        borderRadius: 50,
                                        borderColor: c.Colors.gray,
                                        borderWidth: 0.3,
                                        backgroundColor: '#fff',
                                      }
                                    : {
                                        width: 20,
                                        height: 20,
                                        margin: 10,
                                        borderColor: c.Colors.gray,
                                        borderWidth: 0.3,
                                        backgroundColor: '#fff',
                                      }
                                }
                              />
                            ) : (
                              <View
                                style={[
                                  {
                                    borderRadius: 100,
                                    backgroundColor: '#fff',
                                    width: 60,
                                    height: 60,

                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  },
                                ]}>
                                <Text
                                  style={{
                                    fontSize: 21,
                                    fontWeight: 'bold',
                                    color: c.Colors.mainBlue,
                                  }}>
                                  {toko.name.charAt(0)}
                                </Text>
                              </View>
                            )}

                            <View
                              style={{
                                backgroundColor: c.Colors.mainBlue,
                                padding: 5,
                                borderRadius: 50,
                                borderWidth: 0.5,
                                borderColor: c.Colors.mainBlue,
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                                shadowColor: '#000',
                                shadowOffset: {
                                  width: 0,
                                  height: 4,
                                },
                                shadowOpacity: 0.3,
                                shadowRadius: 4.65,
                                elevation: 8,
                              }}>
                              <MaterialCommunityIcons
                                name="pencil"
                                color={'#fff'}
                              />
                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={{marginLeft: 20}}>
                        <View>
                          <Text style={styles.txttokoTitle}>{toko.name}</Text>
                        </View>
                        <View>
                          <Text style={styles.txttokoPhone}>
                            {toko.whatsapp}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.txttokoUrl}>
                            {(Config.TOKO_NAME + toko.link).length <= 32
                              ? Config.TOKO_NAME + toko.link
                              : (Config.TOKO_NAME + toko.link).substring(
                                  0,
                                  32,
                                ) + '...'}
                          </Text>
                        </View>
                      </View>
                    </View>

                    <TouchableOpacity
                      onPress={() => openRBSheet('toko')}
                      style={{justifyContent: 'center'}}>
                      <MaterialCommunityIcons
                        size={25}
                        name="pencil"
                        color={'#fff'}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      width: '95%',
                      alignSelf: 'center',

                      paddingTop: 10,
                      paddingBottom: 10,
                    }}>
                    <TouchableOpacity
                      onPress={() => refShareToko.current.open()}
                      delayPressIn={0}
                      style={styles.viewsettingsHeader}>
                      <View style={styles.viewsettingsItem}>
                        <View>
                          <MaterialCommunityIcons
                            name="share-variant"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                        </View>
                        <View>
                          <Text style={styles.txtSettings}>Bagikan Toko</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => refSettingsToko.current.open()}
                      delayPressIn={0}
                      style={styles.viewsettingsHeader}>
                      <View style={styles.viewsettingsItem}>
                        <View>
                          <Ionicons
                            name="md-settings-sharp"
                            size={25}
                            color={c.Colors.mainBlue}
                          />
                        </View>
                        <View>
                          <Text style={styles.txtSettings}>
                            Pengaturan Toko
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>

            {isTokoAvailable && isVerified ? tokoAvail() : tokoUnavail()}
          </ScrollView>
        </View>
      )}
      {isTokoAvailable && checkProductAvail ? (
        <View
          style={{
            position: 'absolute',
            flex: 1,
            bottom: 10,
            right: 0,
            paddingHorizontal: 20,
          }}>
          <Button
            onPress={() => {
              dispatch(TokoActions.getDetailProductReset());
              navigation.navigate('ProductAddScreen');
            }}
            // onPress={handleNewProduct}
            style={{
              borderRadius: 100,
              backgroundColor: c.Colors.mainBlue,
              padding: 10,
            }}>
            <Fontisto color={'#fff'} name={'plus-a'} size={25} />
          </Button>
        </View>
      ) : null}
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
const mapStateToProps = (state) => {
  return {
    getOtpWa: state.auth.getOtpPostData,
    tokoBankData: state.toko.tokoBankData.data,
    bankData: state.toko.bankData.data,
    tokoAddressData: state.toko.tokoAddressData.data,
    provinceData: state.toko.provinceData.data,
    cityData: state.toko.cityData.data,
    districtData: state.toko.districtData.data,
    postalCodeData: state.toko.postalCodeData.data,
    checkProductAvail: state.toko.productTokoData.data
      ? state.toko.productTokoData.data.listProduct
        ? state.toko.productTokoData.data.listProduct.length !== 0
        : true
      : false,
    getToko: state.toko.tokoData.data,
    userData: state.auth.userData,
    checkSuccessAddNewCategory: state.toko.postCategoryTokoData.success,
    AddNewCategoryId: state.toko.postCategoryTokoData.data,
    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    tokoAllData: state.toko.detailTokoData.data,
    tokoFetching: state.toko.tokoData.fetching,
    toko: state.toko.detailTokoData.data
      ? state.toko.detailTokoData.data.toko
        ? state.toko.detailTokoData.data.toko
        : {}
      : {},
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,

    categoryData: state.toko.categoryTokoData.data
      ? state.toko.categoryTokoData.data.listCategory
        ? state.toko.categoryTokoData.data.listCategory
        : []
      : [],
    productPrimary: state.toko.productTokoData,
    productAllData: state.toko.productTokoData.data,
    productData: state.toko.productTokoData.data
      ? state.toko.productTokoData.data.listProduct
        ? state.toko.productTokoData.data.listProduct.length != 0
          ? state.toko.productTokoData.data.listProduct
          : []
        : []
      : [],
    tokoAddressData: state.toko.tokoAddressData.data,
    tokoAddress:
      state.toko.tokoAddressData.data !== null
        ? state.toko.tokoAddressData.data.length
          ? state.toko.tokoAddressData.data.length !== 0
          : null
        : null,
    isErrorTokoBankAlreadyRegistered: state.toko.postTokoBankData.error,
    isFetchingUpdateTokoData: state.toko.tokoUpdateData.fetching,
  };
};
export default withNavigationFocus(
  connect(mapStateToProps, mapDispatchToProps)(index),
);
