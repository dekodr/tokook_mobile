import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  BackHandler,
} from 'react-native';
import {
  ListItem,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Spinner,
  Container,
  Header,
  Content,
  Title,
} from 'native-base';

import * as c from '../../../Components';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import RBSheet from 'react-native-raw-bottom-sheet';
import {connect} from 'react-redux';
import {useStateWithCallbackLazy} from 'use-state-with-callback';
import TokoActions from './../../../Redux/TokoRedux';
function Category(props) {
  const {
    dispatch,
    navigation,
    getToko,
    bottomSheetRef,
    tokoId,
    userData,
    tokoData,
    setStep,
    step,
    tokoAddressData,
  } = props;
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const refRBSheet = useRef(null);
  const [isCategoryEmpty, setIsCategoryEmpty] = useState(false);
  const [newCategory, setNewCategory] = useState('');
  const [scrollOffset, setscrollOffset] = useState(0);
  const [text, setText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState({});
  const [selectedNewCategory, setSelectedNewCategory] = useState({});
  const [moveSelectedCategory, setMoveSelectedCategory] = useState({});
  const [tokoCategory, setTokoCategory] = useStateWithCallbackLazy(null);
  const [actionType, setActionType] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const getId = tokoCategory
    ? tokoCategory.length
      ? tokoCategory.filter(data => data.id === selectedCategory.id)
      : []
    : [];
  const isTokoAvailable = props.getToko
    ? props.getToko.listToko
      ? props.getToko.listToko.length === 0
        ? false
        : true
      : true
    : true;
  const isCurrentIdUndefined = tokoCategory
    ? tokoCategory.length
      ? getId.id === undefined
        ? true
        : false
      : false
    : false;
  const wait = timeout => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  };
  const handleRefresh = () => {
    dispatch(TokoActions.getCategoryTokoRequest(tokoId));
    wait(500).then(() => setRefreshing(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleGoBack);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', handleGoBack);
  }, []);
  useEffect(() => {
    setTokoCategory(
      props.categoryData
        ? props.categoryData.listCategory
          ? props.categoryData.listCategory.length != 0
            ? props.categoryData.listCategory
            : []
          : []
        : [],
    );
  }, [props.categoryData]);

  useEffect(() => {
    if (newCategory) {
      setIsCategoryEmpty(false);
    }
  }, [newCategory]);

  const handleAddTokoCategory = () => {
    if (newCategory) {
      const data = {
        tokoId: props.tokoId,
        item: {name: newCategory},
        func: {reloadData: reload, reloadFailed: setActionType},
      };
      setActionType(null);
      dispatch(TokoActions.postCategoryTokoRequest(data));
    } else {
      setIsCategoryEmpty(true);
    }
  };
  const handleUpdateTokoCategory = () => {
    if (selectedCategory.name) {
      const data = {
        tokoId: props.tokoId,
        categoryId: selectedCategory.id,
        item: {name: selectedCategory.name, icon: selectedCategory.icon},
        func: {reloadData: reload, reloadFailed: setActionType},
      };
      setActionType(null);
      dispatch(TokoActions.updateCategoryTokoRequest(data));
    } else {
      setIsCategoryEmpty(true);
    }
  };
  const handleUpdatePositionTokoCategory = (from, to) => {
    const data = {
      tokoId: props.tokoId,
      item: {from: from, to: to},
    };
    setActionType(null);
    dispatch(TokoActions.updatePositionCategoryTokoRequest(data));
  };
  const handleDraggableCategory = (data, from, to) => {
    const params = {
      from,
      to,
    };
    setTokoCategory(data, () => {
      handleUpdatePositionTokoCategory(from, to);
    });
  };
  const handleDeleteTokoCategory = () => {
    const getId = tokoCategory.filter(data => data.id === selectedCategory.id);
    const isCurrentIdUndefined = getId.id === undefined ? true : false;
    if (selectedCategory.totalProducts > 0) {
      if (
        tokoCategory.filter(data => data.id !== selectedCategory.id).length !=
          0 &&
        selectedNewCategory.id !== undefined
      ) {
        const data = {
          tokoId: props.tokoId,
          categoryId: selectedCategory.id,
          item: moveSelectedCategory
            ? {change_id: moveSelectedCategory.id}
            : null,
          func: {reloadData: reload},
        };
        setActionType(null);
        dispatch(TokoActions.deleteCategoryTokoRequest(data));
      }
    } else {
      const data = {
        tokoId: props.tokoId,
        categoryId: selectedCategory.id,
        item: moveSelectedCategory
          ? {change_id: moveSelectedCategory.id}
          : null,
        func: {reloadData: reload},
      };
      setActionType(null);
      dispatch(TokoActions.deleteCategoryTokoRequest(data));
    }
  };

  useEffect(() => {
    if (props.tokoId) {
      props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
    }
    // func.handleGetAllTokoCategory(setTokoCategory, props.dispatch, props.idToko)
  }, []);

  const reload = () => {
    refRBSheet.current.close();
    setIsCategoryEmpty(false);
    setNewCategory('');
    setSelectedCategory({});
    props.dispatch(TokoActions.getCategoryTokoRequest(props.tokoId));
  };

  const handleCloseRBSheet = () => {
    refRBSheet.current.close();
    setNewCategory('');
    setSelectedCategory({});
  };

  const renderDraggableItem = ({item, drag}) => {
    return (
      <View style={[c.Styles.borderBottom]}>
        <ListItem
          onLongPress={drag}
          onPress={() => openRBSheet('edit', item)}
          icon
          noBorder>
          <Left>
            <TouchableOpacity onPressIn={drag}>
              <MaterialCommunityIcons
                name="menu"
                size={26}
                color={c.Colors.gray}
              />
            </TouchableOpacity>
          </Left>
          <Body
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingRight: 18,
            }}>
            <Text style={{fontWeight: '100'}}>{item.name}</Text>
            <Text style={{fontWeight: '100', color: c.Colors.gray}}>
              {item.totalProducts} produk
            </Text>
          </Body>
          <Right>
            <TouchableOpacity onPress={() => openRBSheet('edit', item)}>
              <MaterialCommunityIcons
                name="pencil"
                size={18}
                color={c.Colors.mainBlue}
              />
            </TouchableOpacity>
          </Right>
        </ListItem>
      </View>
    );
  };
  const openRBSheet = (actionType, item) => {
    setMoveSelectedCategory({});
    if (actionType === 'edit') {
      setSelectedCategory(item);
    }
    setActionType(actionType);
    refRBSheet.current.open();
  };

  const categoryButton = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          setMoveSelectedCategory(item);
          setSelectedNewCategory(item);
        }}>
        <View
          style={{
            backgroundColor:
              moveSelectedCategory.id === item.id ? c.Colors.mainBlue : 'white',
            borderColor:
              moveSelectedCategory.id === item.id
                ? c.Colors.newBlue
                : c.Colors.grayDark,
            marginRight: 5,
            paddingVertical: 8,
            paddingHorizontal: 20,
            borderWidth: 1,
            borderRadius: 25,
            alignItems: 'center',
            fontSize: 13,
          }}>
          <Text
            style={{
              fontSize: 13,
              letterSpacing: 1,
              color:
                moveSelectedCategory.id === item.id
                  ? c.Colors.white
                  : c.Colors.grayWhite,
              fontWeight: moveSelectedCategory.id === item.id ? 'bold' : '400',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  const renderActionContent = () => {
    switch (actionType) {
      case 'add': {
        return (
          <React.Fragment>
            <View
              style={[
                c.Styles.flexRow,
                c.Styles.alignItemsCenter,
                c.Styles.borderBottom,
              ]}>
              <TextInput
                value={newCategory}
                onChangeText={text => setNewCategory(text)}
                autoCapitalize="words"
                style={c.Styles.txtInputCreateMerchant}
                placeholder="Nama Kategori"
                placeholderTextColor={c.Colors.gray}
                onBlur={() => setIsCategoryEmpty(newCategory ? false : true)}
              />
            </View>

            {isCategoryEmpty ? (
              <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                <Text style={c.Styles.txtInputMerchantErrors}>
                  Nama kategori harus diisi
                </Text>
              </View>
            ) : null}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 20,
              }}>
              <TouchableOpacity
                onPress={handleCloseRBSheet}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="close-thick"
                  size={24}
                  color={c.Colors.gray}
                />
                <Text style={{color: c.Colors.gray}}>Batal</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleAddTokoCategory}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="check-bold"
                  size={24}
                  color={c.Colors.mainBlue}
                />
                <Text style={{color: c.Colors.mainBlue}}>Simpan</Text>
              </TouchableOpacity>
            </View>
          </React.Fragment>
        );
        break;
      }
      case 'edit': {
        return (
          <React.Fragment>
            <View
              style={[
                c.Styles.flexRow,
                c.Styles.alignItemsCenter,
                c.Styles.borderBottom,
              ]}>
              <TextInput
                autoCapitalize="words"
                style={c.Styles.txtInputCreateMerchant}
                placeholder="Nama Kategori"
                placeholderTextColor={c.Colors.gray}
                value={selectedCategory.name}
                onBlur={() =>
                  setIsCategoryEmpty(selectedCategory.name ? false : true)
                }
                onChangeText={text => {
                  setIsCategoryEmpty(false);
                  setSelectedCategory({...selectedCategory, name: text});
                }}
              />
            </View>
            {isCategoryEmpty ? (
              <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                <Text style={c.Styles.txtInputMerchantErrors}>
                  Nama kategori harus diisi
                </Text>
              </View>
            ) : null}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
              }}>
              <TouchableOpacity
                onPress={() => openRBSheet('del')}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="trash-can-outline"
                  size={24}
                  color={c.Colors.gray}
                />
                <Text style={{color: c.Colors.gray}}>Hapus</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleCloseRBSheet}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="close-thick"
                  size={24}
                  color={c.Colors.gray}
                />
                <Text style={{color: c.Colors.gray}}>Batal</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleUpdateTokoCategory}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="check-bold"
                  size={24}
                  color={c.Colors.mainBlue}
                />
                <Text style={{color: c.Colors.mainBlue}}>Simpan</Text>
              </TouchableOpacity>
            </View>
          </React.Fragment>
        );
        break;
      }
      case 'del': {
        return (
          <React.Fragment>
            {selectedCategory.totalProducts > 0 ? (
              <React.Fragment>
                <View
                  style={[
                    c.Styles.flexRow,
                    c.Styles.alignItemsCenter,
                    c.Styles.borderBottom,
                  ]}>
                  <Text
                    style={[
                      c.Styles.txtInputCreateMerchant,
                      {marginVertical: 10},
                    ]}>
                    {selectedCategory.name}
                  </Text>
                </View>
                <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                  <Text
                    style={[
                      c.Styles.txtInputCreateMerchant,
                      {marginVertical: 10, textAlign: 'center'},
                    ]}>
                    Jika anda ingin menghapus kategori ini, semua{' '}
                    <Text style={{color: c.Colors.mainBlue}}>
                      {selectedCategory.totalProducts} produk
                    </Text>{' '}
                    harus dipindahkan ke kategori lain
                  </Text>
                </View>

                <View
                  style={[
                    c.Styles.flexRow,
                    c.Styles.alignItemsCenter,
                    c.Styles.borderBottom,
                  ]}>
                  <Text
                    style={[
                      c.Styles.txtInputCreateMerchant,
                      {marginVertical: 10},
                    ]}>
                    Pilih Kategori
                  </Text>
                </View>
                {tokoCategory.filter(data => data.id !== selectedCategory.id)
                  .length === 0 ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      Belum ada pilihan untuk kategori pengganti
                    </Text>
                  </View>
                ) : null}
                <View
                  style={[
                    c.Styles.flexRow,
                    c.Styles.alignItemsCenter,
                    {marginTop: 10},
                  ]}>
                  <FlatList
                    renderItem={categoryButton}
                    data={tokoCategory.filter(
                      data => data.id !== selectedCategory.id,
                    )}
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>

                {selectedNewCategory.id === undefined ? (
                  <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
                    <Text style={c.Styles.txtInputMerchantErrors}>
                      Belum memilih kategori pengganti
                    </Text>
                  </View>
                ) : null}
              </React.Fragment>
            ) : null}

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 20,
              }}>
              <TouchableOpacity
                onPress={handleCloseRBSheet}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="close-thick"
                  size={24}
                  color={c.Colors.gray}
                />
                <Text style={{color: c.Colors.gray}}>Batal</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleDeleteTokoCategory}
                style={c.Styles.alignItemsCenter}>
                <MaterialCommunityIcons
                  name="check-bold"
                  size={24}
                  color={c.Colors.mainBlue}
                />
                <Text style={{color: c.Colors.mainBlue}}>Hapus</Text>
              </TouchableOpacity>
            </View>
          </React.Fragment>
        );
        break;
      }
      default:
        return (
          <View>
            <Spinner color={c.Colors.mainBlue} />
          </View>
        );
        break;
    }
  };
  return (
    <Container>
      <Header
        androidStatusBarColor={c.Colors.mainBlue}
        style={{backgroundColor: c.Colors.mainBlue}}>
        <Left>
          <TouchableOpacity onPress={handleGoBack}>
            <MaterialCommunityIcons
              name={'keyboard-backspace'}
              color={'white'}
              size={25}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <Text
            style={{
              letterSpacing: 1.25,
              fontSize: 12,
              fontFamily: 'NotoSans-Bold',
              color: 'white',
            }}>
            Kategori Penjualan
          </Text>
        </Body>
        <Right />
      </Header>
      <View
        style={[
          c.Styles.borderBottom,
          {paddingHorizontal: 20, paddingBottom: 10, marginTop: 20},
        ]}>
        <Text style={c.Styles.ketCategory}>
          Atur kategori pada halaman penjualan dengan menggeser kategori ke atas
          atau ke bawah
        </Text>
      </View>
      {tokoCategory ? (
        tokoCategory.length ? (
          <DraggableFlatList
            activationDistance={5}
            scrollingContainerOffset={scrollOffset}
            style={{marginBottom: 70}}
            data={tokoCategory}
            renderItem={renderDraggableItem}
            keyExtractor={(item, index) => `draggable-item-${item.id}`}
            onDragEnd={({data, from, to}) =>
              handleDraggableCategory(data, from, to)
            }
          />
        ) : (
          <TouchableOpacity
            onPress={() => {
              if (isTokoAvailable) {
                openRBSheet('add');
              } else if (!isVerified) {
                if (userData.data.is_phone_verified === null) {
                  setStep(1);
                  bottomSheetRef.current.open();
                } else if (tokoId === null) {
                  setStep(2);
                  bottomSheetRef.current.open();
                } else if (!tokoAddress) {
                  setStep(3);
                  bottomSheetRef.current.open();
                }
                // bottomSheetRef.current.open();
              }
            }}>
            <View style={[c.Styles.flexRow, c.Styles.borderBottom]}>
              <View
                style={{
                  marginLeft: 24,
                  marginRight: 18,
                  marginVertical: 12,
                  borderRadius: 100,
                }}>
                <IconAntDesign
                  name="pluscircleo"
                  size={26}
                  color={c.Colors.grayWhiteDark}
                />
              </View>
              <View style={c.Styles.justifyCenter}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 13,
                    letterSpacing: 1.24,
                    color: c.Colors.grayWhiteDark,
                  }}>
                  Tambah Kategori
                </Text>
              </View>
              <View
                style={[
                  c.Styles.flexOne,
                  c.Styles.justifyCenter,
                  c.Styles.alignItemsEnd,
                ]}></View>
            </View>
          </TouchableOpacity>
        )
      ) : (
        <Spinner color={c.Colors.mainBlue} />
      )}
      <RBSheet
        onClose={() => setIsCategoryEmpty(false)}
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.5}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <View style={{paddingHorizontal: 20}}>{renderActionContent()}</View>
      </RBSheet>
      <View
        style={{
          position: 'absolute',
          paddingHorizontal: 25,
          bottom: 0,
          alignSelf: 'center',
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Button
          onPress={() => {
            if (isTokoAvailable) {
              openRBSheet('add');
            } else if (!isVerified) {
              if (userData.data.is_phone_verified === null) {
                setStep(1);
                bottomSheetRef.current.open();
              } else if (tokoId === null) {
                setStep(2);
                bottomSheetRef.current.open();
              } else if (!tokoAddress) {
                setStep(3);
                bottomSheetRef.current.open();
              }
              // bottomSheetRef.current.open();
            }
          }}
          style={c.Styles.btnPrimary}>
          <Text
            style={[
              c.Styles.txtCreateMerchantBtn,
              {fontWeight: 'bold', letterSpacing: 1},
            ]}>
            Tambah Kategori
          </Text>
        </Button>
      </View>
    </Container>
  );
}

const mapDispatchToProps = dispatch => ({
  dispatch,
});
const mapStateToProps = state => {
  return {
    getToko: state.toko.tokoData.data,

    tokoId: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0].id
          : null
        : null
      : null,
    categoryData: state.toko.categoryTokoData.data,
    userData: state.auth.userData,
    tokoData: state.toko.tokoData.data
      ? state.toko.tokoData.data.listToko
        ? state.toko.tokoData.data.listToko.length
          ? state.toko.tokoData.data.listToko[0]
          : null
        : null
      : null,
    tokoAddressData: state.toko.tokoAddressData.data,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);
