import React from 'react';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Octicons from 'react-native-vector-icons/Octicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Colors} from './../../Themes';

let Ionicons = ({name, style, size, color}) => (
  <Ionicon name={name} size={size} style={style} color={color} />
);

let FontIcon = ({name, style, size, color}) => (
  <FontAwesomeIcon name={name} size={size} style={style} color={color} />
);

let SimpleIcon = ({name, style, size, color}) => (
  <SimpleLineIcon name={name} size={size} style={style} color={color} />
);
let MaterialIcon = ({name, style, size, color}) => (
  <MaterialCommunityIcon name={name} size={size} style={style} color={color} />
);
let AntIcon = ({name, style, size, color}) => (
  <AntDesignIcon name={name} size={size} style={style} color={color} />
);
let EvilIcons = ({name, style, size, color}) => (
  <EvilIcon name={name} size={size} style={style} color={color} />
);

let EntypoIcons = ({name, style, size, color}) => (
  <Entypo name={name} size={size} style={style} color={color} />
);

let OcticonsIcon = ({name, style, size, color}) => (
  <Octicons name={name} size={size} style={style} color={color} />
);

let FontAwesome5Icon = ({name, style, size, color}) => (
  <FontAwesome5 name={name} size={size} style={style} color={color} />
);

module.exports = {
  Ionicons,
  FontIcon,
  SimpleIcon,
  MaterialIcon,
  AntIcon,
  EvilIcons,
  EntypoIcons,
  OcticonsIcon,
  FontAwesome5Icon,
};
