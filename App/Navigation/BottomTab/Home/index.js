import HomeScreen from '../../../Containers/Home';

import {createStackNavigator} from 'react-navigation-stack';
const HomeNavigator = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
export default HomeNavigator;
