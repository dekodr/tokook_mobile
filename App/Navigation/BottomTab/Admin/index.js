import React from 'react';
import AdminIntroScreen from './../../../Containers/TokoModule/Admin/Intro';
import ShippingMethodScreen from './../../../Containers/TokoModule/Admin/ShippingMethod';
import FinanceKYCScreen from './../../../Containers/TokoModule/Admin/Finance/KYC';
import FinanceScreen from './Finance';
import {createStackNavigator} from 'react-navigation-stack';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
import CameraScreen from './../../../Containers/Miscellaneous/CameraModule';
export const AdminNavigator = createStackNavigator({
  AdminIntroScreen: {
    screen: AdminIntroScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  CameraScreen: {
    screen: CameraScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  ShippingMethodScreen: {
    screen: ShippingMethodScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  FinanceScreen: {
    screen: FinanceScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  FinanceKYC: {
    screen: FinanceKYCScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
AdminNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map((route) => {
      console.log('laaaaa', route.routeName);

      if (route.routeName != 'AdminIntroScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Admin',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.user_blue_icon : Images.user_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
