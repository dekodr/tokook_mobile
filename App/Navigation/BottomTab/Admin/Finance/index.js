import FinanceIntroScreen from './../../../../Containers/TokoModule/Admin/Finance/Intro';
import FinanceTransactionScreen from './../../../../Containers/TokoModule/Admin/Finance/Transaction';
import FinanceBankTransferScreen from './../../../../Containers/TokoModule/Admin/Finance/BankTransfer';
import FinanceKYCScreen from './../../../../Containers/TokoModule/Admin/Finance/KYC';
import {createStackNavigator} from 'react-navigation-stack';
const FinanceNavigator = createStackNavigator({
  FinanceIntroScreen: {
    screen: FinanceIntroScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  FinanceTransactionScreen: {
    screen: FinanceTransactionScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  FinanceBankTransferScreen: {
    screen: FinanceBankTransferScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  FinanceKYCScreen: {
    screen: FinanceKYCScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
export default FinanceNavigator;
