import React from 'react';
import ProductScreen from './../../../Containers/TokoModule/Product';
import CategoryScreen from './../../../Containers/TokoModule/Category';
import ShippingMethodScreen from './../../../Containers/TokoModule/Admin/ShippingMethod';
import ProductEditScreen from './../../../Containers/TokoModule/Product/Edit';
import ProductAddScreen from './../../../Containers/TokoModule/Product/Add';
import AdminShippingMethodScreen from './../../../Containers/TokoModule/Admin/ShippingMethod';
import {createStackNavigator} from 'react-navigation-stack';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
export const TokoNavigator = createStackNavigator({
  ProductScreen: {
    screen: ProductScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  ProductAddScreen: {
    screen: ProductAddScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  ProductEditScreen: {
    screen: ProductEditScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  ShippingMethodScreen: {
    screen: ShippingMethodScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  CategoryScreen: {
    screen: CategoryScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  ShippingMethodScreen: {
    screen: AdminShippingMethodScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
TokoNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map((route) => {
      console.log('laaaaa', route.routeName);

      if (route.routeName != 'ProductScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Toko Saya',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.tokook_blue_icon : Images.tokook_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
