import React from 'react';
import DropshipAddScreen from './../../../Containers/TokoModule/Dropship/Add';
import DropshipDetailScreen from '../../../Containers/TokoModule/Dropship/Detail';
import DropshipIntroScreen from '../../../Containers/TokoModule/Dropship/Intro';
import {createStackNavigator} from 'react-navigation-stack';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
export const DropshipNavigator = createStackNavigator({
  DropshipIntroScreen: {
    screen: DropshipIntroScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  DropshipAddScreen: {
    screen: DropshipAddScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  DropshipDetailScreen: {
    screen: DropshipDetailScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
DropshipNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      console.log('laaaaakiii', route.routeName);

      if (route.routeName != 'DropshipIntroScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Marketplace',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.home_blue_icon : Images.home_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
