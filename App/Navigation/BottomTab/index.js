import React from 'react';
import {ToastAndroid} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors} from './../../Themes/';
import Icon from 'react-native-vector-icons/FontAwesome5';
//toko
import {TokoNavigator} from './Toko';
//order
import {OrderNavigator} from './Order';
//admin
import {AdminNavigator} from './Admin';
//marketplace
import {MarketplaceNavigator} from './Marketplace';
import {DropshipNavigator} from './Dropship';
//marketplace category
import {MarketplaceCategoryNavigator} from './Category';
export const TokoOKTab = createBottomTabNavigator(
  {
    Marketplace: MarketplaceNavigator,
    // Dropship: DropshipNavigator,
    // Category: MarketplaceCategoryNavigator,
    Toko: TokoNavigator,
    Order: OrderNavigator,
    Admin: AdminNavigator,
  },

  {
    tabBarOptions: {
      inactiveTintColor: '#737373',
      activeTintColor: Colors.mainBlue,
      showLabel: true,
      labelStyle: {
        fontSize: 9,
        fontFamily: 'NotoSans',
        letterSpacing: 1.2,
        marginBottom: 6.5,
        bottom: 2,
      },
      style: {
        backgroundColor: Colors.white,
        height: 65,
      },
    },
  },
  {
    initialRouteName: 'Marketplace',
  },
);

export const TokoOKRouter = createAppContainer(TokoOKTab);
