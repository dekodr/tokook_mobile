import React from 'react';
import MarketplaceCategoryScreen from './../../../Containers/MarketplaceModule/Category/Intro';
import MarketplaceCategoryDetailScreen from './../../../Containers/MarketplaceModule/Category/Detail';
import {createStackNavigator} from 'react-navigation-stack';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
export const MarketplaceCategoryNavigator = createStackNavigator({
  MarketplaceCategoryScreen: {
    screen: MarketplaceCategoryScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceCategoryDetailScreen: {
    screen: MarketplaceCategoryDetailScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
MarketplaceCategoryNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      console.log('laaaaa', route.routeName);
      if (route.routeName != ' MarketplaceCategoryScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Category',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.category_blue_icon : Images.category_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
