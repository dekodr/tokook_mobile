import React from 'react';
import MarketplaceIntroScreen from './../../../Containers/MarketplaceModule/Intro';
import MarketplaceDetailScreen from './../../../Containers/MarketplaceModule/Detail/Intro';
import MarketplaceProductScreen from './../../../Containers/MarketplaceModule/Product/Intro';
import MarketplaceCartScreen from './../../../Containers/MarketplaceModule/Cart/Intro';
import MarketplaceAddressScreen from './../../../Containers/MarketplaceModule/Address/Intro';
import MarketplaceCheckoutScreen from './../../../Containers/MarketplaceModule/Checkout/Intro';
import MarketplaceSupplierScreen from './../../../Containers/MarketplaceModule/Supplier/Intro';
import MarketplaceSearchScreen from './../../../Containers/MarketplaceModule/Search/Intro';
import MarketplaceSearchResultScreen from './../../../Containers/MarketplaceModule/Search/Result';
import WebviewScreen from './../../../Containers/Miscellaneous/WebviewModule';
import MarketplaceAddProductToOwnTokoScreen from './../../../Containers/MarketplaceModule/Product/Toko/Add';

import {createStackNavigator} from 'react-navigation-stack';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
export const MarketplaceNavigator = createStackNavigator({
  MarketplaceIntroScreen: {
    screen: MarketplaceIntroScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceDetailScreen: {
    screen: MarketplaceDetailScreen,
    path: 'marketplace/:product-detail',
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceProductScreen: {
    screen: MarketplaceProductScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceCartScreen: {
    screen: MarketplaceCartScreen,
    navigationOptions: {
      headerShown: false,
    },
  },

  MarketplaceAddressScreen: {
    screen: MarketplaceAddressScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  WebviewScreen: {
    screen: WebviewScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceAddProductToOwnTokoScreen: {
    screen: MarketplaceAddProductToOwnTokoScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceCheckoutScreen: {
    screen: MarketplaceCheckoutScreen,
    navigationOptions: {
      headerShown: false,
    },
  },

  MarketplaceSupplierScreen: {
    screen: MarketplaceSupplierScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceSearchScreen: {
    screen: MarketplaceSearchScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  MarketplaceSearchResultScreen: {
    screen: MarketplaceSearchResultScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});
MarketplaceNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      console.log('laaaaa', route.routeName);

      if (route.routeName != 'MarketplaceIntroScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Marketplace',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.home_blue_icon : Images.home_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
