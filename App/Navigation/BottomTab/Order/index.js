import React from 'react';
import OrderScreen from './../../../Containers/TokoModule/Order/Intro';
import OrderDetailScreen from './../../../Containers/TokoModule/Order/Detail';
import {createStackNavigator} from 'react-navigation-stack';
import Entypo from 'react-native-vector-icons/Entypo';
import {Image} from 'react-native';
import {Images} from './../../../Themes/';
export const OrderNavigator = createStackNavigator({
  OrderScreen: {
    screen: OrderScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  OrderDetailScreen: {
    screen: OrderDetailScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});

OrderNavigator.navigationOptions = ({navigation}) => {
  let tabBarVisible;

  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      console.log('laaaaa', route.routeName);

      if (route.routeName != 'OrderScreen') {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarLabel: 'Orderan',
    tabBarIcon: ({tintColor, focused}) => (
      <Image
        source={focused ? Images.order_blue_icon : Images.order_grey_icon}
        style={{width: 32, height: 32, marginTop: 7}}
      />
    ),
    tabBarVisible,
  };
};
