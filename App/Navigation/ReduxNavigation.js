import React, {useRef, useEffect, useState} from 'react';
import * as ReactNavigation from 'react-navigation';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TextInput,
  FlatList,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import {Images} from '../Themes';
import TokoActions from '../Redux/TokoRedux';
import Config from 'react-native-config';
import * as c from '../Components';
import {Button, Spinner, Card, Container, Content} from 'native-base';
import {connect} from 'react-redux';
import AppNavigation from './Main';
import GlobalActions from '../Redux/GlobalRedux';
import AuthActions from '../Redux/AuthenticationRedux';
import RBSheet from 'react-native-raw-bottom-sheet';
import styles from './styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// here is our reduximport-aware our smart component
const initialProductForm = {
  name: null,
  description: null,
  price: null,
  capitalPrice: null,
  images: [],
  category: null,
  addCategory: false,
  newCategory: null,
  categorySelected: null,
  inputOtherBusinessType: null,
  isEdit: false,
  productId: null,
};

const initialFilter = {
  isFiltered: false,
  categoryId: null,
  productFiltered: [],
};

const initialValidation = {
  isImageEmpty: false,
  isNameEmpty: false,
  isPriceEmpty: false,
  isCategoryEmpty: false,
};
const initialAddressValidation = {
  isProvinceEmpty: false,
  isCityEmpty: false,
  isDistrictEmpty: false,
  isPostalCodeEmpty: false,
  isAddressEmpty: false,
  isDetailAddressEmpty: false,
  isErrorTokoBankAlreadyRegistered: false,
};
const initialBankValidation = {
  isBankEmpty: false,
  isBankAccountNumEmpty: false,
  isBankOwnerEmpty: false,
};
function ReduxNavigation(props) {
  const {
    userData,
    tokoData,
    dispatch,
    nav,
    isModalVisible,
    getOtpWa,
    tokoAddressData,
    tokoAddress,
    provinceData,
    cityData,
    districtData,
    postalCodeData,
    tokoId,
  } = props;
  const {step} = isModalVisible;
  const [timeLeft, setTimeLeft] = useState(0);
  const [formToko, setFormToko] = useState({
    business_type_id: null,
    name: null,
    link: null,
    whatsapp: null,
    instagram: null,
    facebook: null,
    line: null,
    youtube: null,
    tokopedia: null,
    shopee: null,
    blibli: null,
    inputOtherBusinessType: null,
  });
  const [addedAddressFormValidation, setAddedAddressFormValidation] = useState(
    initialAddressValidation,
  );
  const [wanumber, setwaNumber] = useState('');
  const [addressIdValue, setAddressIdValue] = useState(0);
  const [token, setToken] = useState('');
  const [loadingStep, setloadingStep] = useState(false);
  const [isAddedAddress, setIsAddedAddress] = useState(false);
  const [isUpdatedAddress, setIsUpdatedAddress] = useState(false);
  const [province, setOpenPickerProvince] = useState(false);
  const [provinceValue, setProvinceValue] = useState(null);
  const [city, setOpenPickerCity] = useState(false);
  const [cityValue, setCityValue] = useState(null);
  const [district, setOpenPickerDistrict] = useState(false);
  const [districtValue, setDistrictValue] = useState(null);
  const [postalCode, setOpenPickerPostalCode] = useState(false);
  const [postalCodeValue, setPostalCodeValue] = useState(null);
  const [postalCodeFilteredData, setPostalCodeFilteredData] = useState([]);
  const [addressValue, setAddressValue] = useState(null);
  const [detailAddressValue, setDetailAddressValue] = useState(null);
  const [code, setCode] = useState('');
  const [errMsg, setErrMsg] = useState(null);
  const [tokoValidation, setTokoValidation] = useState({
    isNameTokoEmpty: false,
    isLinkTokoEmpty: false,
    isWhatsappEmpty: false,
    isBusinessTypeEmpty: false,
    isTokpedLinkInvalid: false,
    isShopeeLinkInvalid: false,
    isBlibliLinkInvalid: false,
    isInstagramLinkInvalid: false,
    isFacebookLinkInvalid: false,
    isLineLinkInvalid: false,
    isYoutubeLinkInvalid: false,
  });
  const isVerified =
    userData.data.is_phone_verified !== null &&
    tokoData &&
    tokoAddressData !== null
      ? tokoAddressData.listAddress.length
        ? tokoAddressData.listAddress.length !== 0
        : false
      : false;
  const ref = useRef();
  useEffect(() => {
    setIsAddedAddress(true);
    if (isModalVisible.modalVisible) {
      ref.current.open();
    } else {
      ref.current.close();
    }
  }, [isModalVisible]);
  useEffect(() => {
    if (getOtpWa.success) {
      const {token} = getOtpWa.data.data;
      setTimeLeft(30);
      setToken(token);
    }
  }, [getOtpWa.success]);
  useEffect(() => {
    if (timeLeft === 0) {
      setTimeLeft(0);
    }
    // exit early when we reach 0
    if (!timeLeft) return;

    // save intervalId to clear the interval when the
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => clearInterval(intervalId);
    // clear interval on re-render to avoid memory leaks

    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  const isLinkValid = (link) => {
    if (typeof link === 'string' && link.length) {
      if (
        link.toLowerCase().includes('http') ||
        link.toLowerCase().includes('www')
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };
  useEffect(() => {
    if (formToko.name) {
      setTokoValidation({...tokoValidation, isNameTokoEmpty: false});
    }
    if (formToko.link) {
      setTokoValidation({...tokoValidation, isLinkTokoEmpty: false});
    }
    if (formToko.whatsapp) {
      setTokoValidation({...tokoValidation, isWhatsappEmpty: false});
    }
    if (tokoValidation.isTokpedLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      });
    }
    if (tokoValidation.isShopeeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      });
    }
    if (tokoValidation.isBlibliLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      });
    }
    if (tokoValidation.isInstagramLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      });
    }
    if (tokoValidation.isFacebookLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      });
    }
    if (tokoValidation.isLineLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isLineLinkInvalid: !isLinkValid(formToko.line),
      });
    }
    if (tokoValidation.isYoutubeLinkInvalid) {
      setTokoValidation({
        ...tokoValidation,
        isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
      });
    }
  }, [formToko]);
  const handleCleanState = () => {
    let data = {
      modalVisible: false,
      step: 0,
    };
    setTimeLeft(0);
    // dispatch(GlobalActions.setModalVisible(data));
  };
  const stepNext = () => {
    setTokoValidation({
      isNameTokoEmpty: !formToko.name ? true : false,
      isLinkTokoEmpty: !formToko.link ? true : false,
      //isWhatsappEmpty: !formToko.whatsapp ? true : false,
      isTokpedLinkInvalid: !isLinkValid(formToko.tokopedia),
      isShopeeLinkInvalid: !isLinkValid(formToko.shopee),
      isBlibliLinkInvalid: !isLinkValid(formToko.blibli),
      isInstagramLinkInvalid: !isLinkValid(formToko.instagram),
      isFacebookLinkInvalid: !isLinkValid(formToko.facebook),
      isLineLinkInvalid: !isLinkValid(formToko.line),
      isYoutubeLinkInvalid: !isLinkValid(formToko.youtube),
    });

    const {
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
      tncCheck,
    } = formToko;

    switch (step) {
      case 1: {
        const data = {
          item: {
            phone: wanumber,
            code: code,
            token: token,
          },
          func: {
            reloadData: () => {
              setloadingStep(false);
              props.dispatch(AuthActions.getUserRequest(props.access_token));
              setCode('');
              setwaNumber('');
              if (!isVerified) {
                if (!tokoData) {
                  let data = {
                    modalVisible: true,
                    step: 2,
                  };
                  dispatch(GlobalActions.setModalVisible(data));
                } else if (!tokoAddress) {
                  setIsAddedAddress(true);
                  let data = {
                    modalVisible: true,
                    step: 3,
                  };
                  dispatch(GlobalActions.setModalVisible(data));
                } else {
                  let data = {
                    modalVisible: false,
                  };
                  dispatch(GlobalActions.setModalVisible(data));
                }
              }
            },
            reloadFailed: () => {
              setloadingStep(false);
            },
          },
        };
        setloadingStep(true);
        props.dispatch(AuthActions.whatsappVerificationRequest(data));
      }
      case 2: {
        // setStep(3);
        //Validate first
        if (name && link) {
          const data = {
            item: {
              name: name,
              link: link.toLowerCase(),
              whatsapp: userData.is_phone_verified ? userData.phone : wanumber,
            },
            func: {
              reloadData: () => {
                setloadingStep(false);
                props.dispatch(TokoActions.getDetailTokoRequest(tokoId));
                props.dispatch(TokoActions.getProvinceRequest());
                if (!isVerified) {
                  if (!tokoAddress) {
                    setIsAddedAddress(true);
                    let data = {
                      modalVisible: true,
                      step: 3,
                    };
                    dispatch(GlobalActions.setModalVisible(data));
                  } else {
                    let data = {
                      modalVisible: false,
                    };
                    dispatch(GlobalActions.setModalVisible(data));
                  }
                }
              },
              reloadFailed: () => {
                setloadingStep(false);
              },
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.tokoPostRequest(data));
        }
        break;
      }
      case 3: {
        if (provinceValue === null || provinceValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: true,
          });
        } else if (cityValue === null || cityValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: true,
          });
        } else if (districtValue === null || districtValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: true,
          });
        } else if (postalCodeValue === null || postalCodeValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isPostalCodeEmpty: true,
          });
        } else if (addressValue === null || addressValue === '') {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isAddressEmpty: true,
          });
        } else {
          const postData = {
            tokoId: tokoId,
            // func: {
            //   rb: refisTokoUnavailable,
            // },
            purpose: 1,
            addressId: addressIdValue,
            purpose: isAddedAddress ? 1 : isUpdatedAddress ? 2 : 1,
            detail: {
              province_id: provinceValue.id,
              city_id: cityValue.id,
              district_id: districtValue.id,
              postal_code: postalCodeValue,
              address: addressValue.concat(detailAddressValue),
            },
          };
          setloadingStep(true);
          props.dispatch(TokoActions.postTokoAddressRequest(postData));
        }
        break;
      }

      default:
        break;
    }
  };
  const createMerchant = () => {
    const {
      tncCheck,
      name,
      link,
      whatsapp,
      tokopedia,
      shopee,
      blibli,
      instagram,
      facebook,
      line,
      youtube,
      businessTypeSelected,
      inputOtherBusinessType,
    } = formToko;

    const {
      isLinkTokoEmpty,
      isNameTokoEmpty,
      isWhatsappEmpty,
      isTokpedLinkInvalid,
      isBusinessTypeEmpty,
      isShopeeLinkInvalid,
      isBlibliLinkInvalid,
      isInstagramLinkInvalid,
      isFacebookLinkInvalid,
      isLineLinkInvalid,
      isYoutubeLinkInvalid,
    } = tokoValidation;
    return step == 1 ? (
      <View>
        <View
          style={[
            c.Styles.flexRow,
            c.Styles.alignItemsCenter,
            {marginTop: 20},
          ]}>
          <Text style={c.Styles.txtverifWA}>Verifikasi Whatsapp</Text>
        </View>
        <View style={styles.justifyCenter}>
          <View style={styles.viewinputwaNumber}>
            <View style={c.Styles.flexRow}>
              <View style={c.Styles.justifyCenter}>
                <c.Image
                  source={Images.waIcon}
                  style={c.Styles.textInputIcon}
                />
              </View>
              <TextInput
                keyboardType={'number-pad'}
                style={styles.txtinputwaNumber}
                onChangeText={(text) => {
                  setwaNumber(text);
                  setErrMsg(null);
                }}
                placeholderTextColor={'#bdbdbd'}
                placeholder={'Nomor Whatsapp'}
                value={wanumber}
              />
              <TouchableOpacity
                style={[c.Styles.justifyCenter]}
                onPress={timeLeft > 0 ? null : handlegetOtp}
                disabled={props.isFetchingOtp}>
                {getOtpWa.fetching ? (
                  <View
                    style={{
                      width: 50,
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Spinner size={18} color={c.Colors.mainBlue} />
                  </View>
                ) : (
                  <Text
                    style={[
                      styles.txtkirimOtp,
                      c.Styles.alignCenter,
                      {opacity: timeLeft === 0 ? 1 : 0.4},
                    ]}>
                    Kirim OTP
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text style={styles.txtbottomwaNumber}>
              Agar pembeli bisa menghubungi kamu
            </Text>
          </View>
          <View style={[c.Styles.alignCenter, {marginTop: '10%', height: 100}]}>
            {getOtpWa.success ? (
              <View>
                <c.PinVerify
                  autoFocus={true}
                  cellStyle={styles.cellStyle}
                  cellStyleFocused={styles.cellStyleFocused}
                  textStyle={styles.txtCellStyle}
                  textStyleFocused={styles.textStyleFocused}
                  value={code}
                  editable={getOtpWa.success ? true : false}
                  onTextChange={(code) => setCode(code)}
                />
                <View
                  style={[
                    ,
                    {
                      marginTop: '10%',
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    },
                  ]}>
                  <TouchableOpacity
                    style={c.Styles.justifyCenter}
                    onPress={handlegetOtp}
                    disabled={timeLeft !== 0 ? true : false}>
                    <Text
                      style={[
                        styles.txtresendOtp,
                        {opacity: timeLeft === 0 ? 1 : 0.4},
                      ]}>
                      {' '}
                      Kirim Ulang OTP
                    </Text>
                  </TouchableOpacity>

                  <Text style={styles.txttimeLeft}>
                    00:{timeLeft < 10 ? '0' : ''}
                    {timeLeft}
                  </Text>
                </View>
              </View>
            ) : null}
          </View>

          <View
            style={{
              top: 30,
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              //onPress={handleResendOtp}
              disabled={props.isFetchingOtp}>
              <Text
                style={{
                  fontFamily: 'NotoSans-Bold',
                  fontSize: 12,
                  opacity: timeLeft === 0 ? 1 : 0.4,
                  color: 'white',
                  marginTop: 2,
                }}>
                {' '}
                Kirim Ulang
              </Text>
            </TouchableOpacity>

            <Text
              style={{
                marginLeft: 6,
                fontFamily: 'NotoSans-ExtraBold',
                color: 'white',
                fontSize: 15,
                opacity: 1,
              }}>
              00:{timeLeft < 10 ? '0' : ''}
              {timeLeft}
            </Text>
          </View>
        </View>
      </View>
    ) : step == 2 ? (
      <View>
        <View style={[c.Styles.flexRow, c.Styles.alignItemsCenter]}>
          <Text style={c.Styles.txtCreateMerchant}>Buka Toko Online</Text>
        </View>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={c.Styles.viewTokoStepContent}
          showsVerticalScrollIndicator={false}>
          <Text style={c.Styles.lineHeightTwenty}>
            Fitur ini dibuat untuk kamu yang ingin memiliki website usaha kamu
            sendiri. Bagikan dan tingkatkan penjualan kamu.
          </Text>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={c.Styles.txtInputWithIcon}>
              <View style={c.Styles.marginRightTen}>
                <c.Image source={Images.toko} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  autoCapitalize="words"
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Nama Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      name: text,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: text ? false : true,
                    });
                  }}
                  value={name}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isNameTokoEmpty: name ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              Nama toko di website kamu
            </Text>
            {isNameTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Nama toko harus diisi
              </Text>
            )}
          </View>
          <View style={[c.Styles.marginTopTen, c.Styles.marginBottomFive]}>
            <View style={[c.Styles.txtInputWithIcon]}>
              <View style={[c.Styles.marginRightTen]}>
                <c.Image source={Images.at} style={c.Styles.textInputIcon} />
              </View>
              <View style={c.Styles.flexOne}>
                <TextInput
                  style={c.Styles.txtInputCreateMerchant}
                  placeholder="Link Toko"
                  placeholderTextColor={c.Colors.gray}
                  onChangeText={(text) => {
                    let lowercasetext = text.toLowerCase();
                    setFormToko({
                      ...formToko,
                      link: lowercasetext.split(' ').join('-'),
                    });
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: text ? false : true,
                    });
                  }}
                  value={link}
                  onBlur={() =>
                    setTokoValidation({
                      ...tokoValidation,
                      isLinkTokoEmpty: link ? false : true,
                    })
                  }
                />
              </View>
            </View>
            <Text style={[c.Styles.txtInputDesc]}>
              {Config.TOKO_NAME}
              <Text
                style={{
                  fontWeight: '600',
                  fontSize: 14,
                  letterSpacing: 1,
                  textTransform: 'lowercase',
                }}>
                {!link ? 'link-toko' : link}
              </Text>
            </Text>
            {isLinkTokoEmpty && (
              <Text style={c.Styles.txtInputMerchantErrors}>
                Link toko harus diisi
              </Text>
            )}
          </View>
        </ScrollView>
      </View>
    ) : step == 3 ? (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'handled'}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 14,
            letterSpacing: 1,
            marginTop: 20,
          }}>
          Alamat Toko
        </Text>

        {isAddedAddress && (
          <View
            style={{
              width: '100%',
              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                  setOpenPickerPostalCode(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && cityData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                    setOpenPickerPostalCode(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                    setOpenPickerPostalCode(false);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && districtData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  keyboardShouldPersistTaps={'handled'}
                  nestedScrollEnabled
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
        {isUpdatedAddress && (
          <View
            style={{
              width: '100%',

              borderColor: c.Colors.gray,
              borderWidth: 0.58,
              marginTop: 17,
              paddingLeft: 18,
              paddingRight: 18,
            }}>
            <TouchableOpacity
              onPress={() => setIsUpdatedAddress(false)}
              delayPressIn={0}
              style={{position: 'absolute', right: 8, top: 8}}>
              <MaterialCommunityIcons name="close" size={22} color="#1a69d5" />
            </TouchableOpacity>
            {provinceValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 25,
                }}>
                Provinsi
              </Text>
            )}

            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (province) {
                  setOpenPickerProvince(false);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                } else {
                  setOpenPickerProvince(true);
                  setOpenPickerCity(false);
                  setOpenPickerDistrict(false);
                }
              }}
              style={{
                marginTop: provinceValue != null ? 8 : 50,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isProvinceEmpty
                  ? 'red'
                  : provinceValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  marginLeft: 2,
                }}>
                {provinceValue != null ? provinceValue.name : 'Provinsi'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!province ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isProvinceEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Provinsi
              </Text>
            )}

            {province && provinceData != null && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={provinceData.provincies}
              />
            )}
            {cityValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kota
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else {
                  if (city) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(true);
                    setOpenPickerDistrict(false);
                  }
                }
              }}
              style={{
                marginTop: cityValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isCityEmpty
                  ? 'red'
                  : cityValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,

                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {cityValue != null
                  ? `${cityValue.type} ${cityValue.name}`
                  : 'Kota'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!city ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isCityEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kota
              </Text>
            )}
            {city && (
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={cityData.cities}
              />
            )}
            {districtValue != null && (
              <Text
                style={{
                  marginLeft: 2,

                  fontFamily: 'NotoSans',
                  color: '#1a69d5',
                  fontSize: 10,
                  letterSpacing: 1.2,
                  marginTop: 32,
                }}>
                Kecamatan
              </Text>
            )}
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => {
                if (provinceValue === null) {
                  Toast.show('Pilih Provinsi Terlebih Dahulu !');
                } else if (cityValue === null) {
                  Toast.show('Pilih Kota Terlebih Dahulu !');
                } else {
                  if (district) {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(false);
                  } else {
                    setOpenPickerProvince(false);
                    setOpenPickerCity(false);
                    setOpenPickerDistrict(true);
                  }
                }
              }}
              style={{
                marginTop: districtValue != null ? 8 : 32,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isDistrictEmpty
                  ? 'red'
                  : districtValue == null
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <Text
                style={{
                  marginLeft: 2,
                  bottom: 3,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                }}>
                {districtValue != null ? districtValue.name : 'Kecamatan'}
              </Text>
              <MaterialCommunityIcons
                style={{bottom: 5}}
                name={!district ? 'menu-down' : 'menu-up'}
                size={25}
                color="#1a69d5"
              />
            </TouchableOpacity>
            {addedAddressFormValidation.isDistrictEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib memilih Kecamatan
              </Text>
            )}
            {district && (
              <FlatList
                nestedScrollEnabled
                style={{height: 300}}
                renderItem={_addressRenderItem}
                data={districtData.districts}
              />
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isPostalCodeEmpty
                  ? 'red'
                  : postalCodeValue == null || postalCodeValue === ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={searchingPostalCode}
                keyboardType={'number-pad'}
                value={postalCodeValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Kode Pos'}
              />
              {postalCodeValue != null && postalCode && (
                <TouchableOpacity
                  delayPressIn={0}
                  onPress={() => setOpenPickerPostalCode(false)}
                  style={{bottom: 9, position: 'absolute', right: 0}}>
                  <MaterialCommunityIcons
                    name="check"
                    size={25}
                    color="#1a69d5"
                  />
                </TouchableOpacity>
              )}
            </View>
            {addedAddressFormValidation.isPostalCodeEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Kode Pos
              </Text>
            )}
            {postalCode &&
              provinceValue != null &&
              cityValue != null &&
              districtValue != null && (
                <FlatList
                  nestedScrollEnabled
                  style={{height: 300}}
                  renderItem={_addressRenderItem}
                  data={
                    postalCodeFilteredData && postalCodeFilteredData.length > 0
                      ? postalCodeFilteredData
                      : postalCodeData.postalCode
                  }
                />
              )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor: addedAddressFormValidation.isAddressEmpty
                  ? 'red'
                  : addressValue == null || addressValue == ''
                  ? '#c9c6c6'
                  : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                multiline={true}
                onChangeText={(text) => {
                  setAddressValue(text);
                }}
                value={addressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Alamat Lengkap'}
              />
            </View>
            {addedAddressFormValidation.isAddressEmpty && (
              <Text style={[c.Styles.txtInputDesc, {color: 'red'}]}>
                Wajib mengisi Alamat Lengkap
              </Text>
            )}
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                width: '100%',
                borderBottomWidth: 0.5,
                borderBottomColor:
                  detailAddressValue === null || detailAddressValue === ''
                    ? '#c9c6c6'
                    : '#1a69d5',
              }}>
              <TextInput
                style={{
                  bottom: -5,
                  fontFamily: 'NotoSans',
                  fontSize: 12,
                  letterSpacing: 1,
                  color: '#7b7b7b',
                  width: '100%',
                }}
                onChangeText={(text) => setDetailAddressValue(text)}
                value={detailAddressValue}
                placeholderTextColor={'#7b7b7b'}
                placeholder={'Detail Alamat (optional)'}
              />
            </View>

            <View style={{height: 25}} />
          </View>
        )}
      </ScrollView>
    ) : null;
  };
  const handlegetOtp = () => {
    const data = {
      phone: userData.is_phone_verified ? userData.phone : wanumber,
      source: 'phone_verification',
      type: 'whatsapp',
    };
    Keyboard.dismiss();
    props.dispatch(AuthActions.getOtpPostRequest(data));
  };
  const handleOnPressPickerItem = (purpose, data) => {
    switch (purpose) {
      case 1:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isProvinceEmpty: false,
          });
          setOpenPickerProvince(false);
          setProvinceValue(data.item);
          setDistrictValue(null);
          setCityValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getCityRequest(data.item));
        }
        break;
      case 2:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isCityEmpty: false,
          });
          setOpenPickerCity(false);
          setCityValue(data.item);
          setDistrictValue(null);
          setPostalCodeValue(null);
          dispatch(TokoActions.getDistrictRequest(data.item));
        }
        break;
      case 3:
        {
          setAddedAddressFormValidation({
            ...addedAddressFormValidation,
            isDistrictEmpty: false,
          });
          setOpenPickerDistrict(false);
          setDistrictValue(data.item);
          setPostalCodeValue(null);
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: cityValue.name,
              districtName: data.item.name,
            }),
          );
        }
        break;
      case 4:
        {
          setOpenPickerPostalCode(false);
          setPostalCodeValue(data.item.postal_code.toString());
        }
        break;
      case 5:
        {
          setAddedBankFormValidation({
            ...addedBankFormValidation,
            isBankEmpty: false,
          });
          setBankNameValue(data.item);
          setOpenPickerBank(false);
          setBankValue(data.item);
        }
        break;
      default:
        null;
    }
  };
  const searchingPostalCode = (searchText) => {
    if (provinceValue != null && cityValue != null && districtValue != null) {
      if (searchText === '') {
        setOpenPickerPostalCode(false);
        setPostalCodeValue(null);
      } else {
        setAddedAddressFormValidation({
          ...addedAddressFormValidation,
          isPostalCodeEmpty: false,
        });
        setPostalCodeValue(searchText);
        setOpenPickerCity(false);
        setOpenPickerProvince(false);
        setOpenPickerDistrict(false);

        setOpenPickerPostalCode(true);
        let filteredData = postalCodeData.postalCode.filter(function (item) {
          const itemData = item.postal_code.toString().toLowerCase();

          const textData = searchText.toLowerCase();
          return itemData.includes(textData);
        });
        setPostalCodeFilteredData(filteredData);
      }
    } else {
      setOpenPickerCity(false);
      setOpenPickerProvince(false);
      setOpenPickerDistrict(false);

      setPostalCodeValue(searchText);
    }
  };
  const _tokoAddressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(TokoActions.getCityRequest(item.province_details));
          dispatch(TokoActions.getDistrictRequest(item.city_details));
          dispatch(
            TokoActions.getPostalCodeRequest({
              cityName: item.city_details.name,
              districtName: item.district_details.name,
            }),
          );
          setAddressIdValue(item.id);
          setProvinceValue(item.province_details);
          setCityValue(item.city_details);
          setDistrictValue(item.district_details);
          setAddressValue(item.address);
          setDetailAddressValue(item.address2);
          setPostalCodeValue(item.postal_code);
          setIsUpdatedAddress(true);
        }}
        delayPressIn={0}
        style={{
          width: '100%',
          borderColor: c.Colors.gray,
          borderWidth: 0.58,
          marginTop: 17,
          paddingLeft: 18,
          paddingRight: 18,
          bottom: 1,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
            marginTop: 20,
          }}>
          {item.province_details.name}
        </Text>

        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.name}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.city_details.postal_code}
        </Text>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#4a4a4a',
            fontSize: 12,
            letterSpacing: 1.5,
          }}>
          {item.fullAddress}
        </Text>
        <View style={{height: 25}} />
        {/* <View
          style={{
            borderRadius: 25,
            marginTop: 15,
            width: '100%',
            height: '50%',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '28%',
              height: '100%',
            }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              initialRegion={{
                latitude: 36.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0,
                longitudeDelta: 0.0,
              }}
              style={{
                width: '100%',
                height: '72%',
                alignSelf: 'center',
              }}>
              <MapView.Marker
                coordinate={{latitude: 37.78825, longitude: -122.4324}}
                title={'title'}
                description={'description'}
              />
            </MapView>
          </View>
          <View
            style={{
              width: '75%',
              height: '80%',
              paddingLeft: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'NotoSans',
                color: '#7b7b7b',
                letterSpacing: 1,
                fontSize: 9,
              }}>
              Jl. Blora No.32, RT.2/RW.6, Dukuh Atas, Menteng, Kec. Menteng,
              Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310
            </Text>
          </View>
        </View>
      */}
      </TouchableOpacity>
    );
  };
  const _addressRenderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        delayPressIn={0}
        onPress={() => {
          handleOnPressPickerItem(
            province
              ? 1
              : city
              ? 2
              : district
              ? 3
              : postalCode
              ? 4
              : bank
              ? 5
              : 0,
            {
              item,
              index,
            },
          );
        }}
        style={{
          padding: 15,
          borderBottomColor: '#c9c6c6',
          borderBottomWidth: 0.5,
        }}>
        <Text
          style={{
            fontFamily: 'NotoSans',
            color: '#111432',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {postalCode
            ? item.postal_code
            : city
            ? item.type + ' ' + item.name
            : item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  return (
    <View style={{flex: 1}}>
      <RBSheet
        onClose={handleCleanState}
        ref={ref}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={Dimensions.get('window').height * 0.8}
        openDuration={250}
        customStyles={{
          container: {
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          },
          draggableIcon: {
            backgroundColor: '#D8D8D8',
            width: 59.5,
          },
        }}>
        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            marginBottom: 50,
            height: '100%',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
          }}>
          {createMerchant()}
          {step !== 4 ? (
            <Button
              disabled={step === 1 && code.length < 4 ? true : false}
              onPress={stepNext}
              style={[
                c.Styles.btnPrimary,
                {
                  paddingHorizontal: 20,
                  marginTop: '10%',
                  marginBottom: '10%',
                  backgroundColor:
                    step === 1 && code.length < 4
                      ? '#c9c6c6'
                      : c.Colors.mainBlue,
                },
              ]}>
              {loadingStep ? (
                <Spinner size={18} color="#fff" />
              ) : (
                <Text style={c.Styles.txtCreateMerchantBtn}>
                  {step === 1
                    ? 'Verifikasi'
                    : step === 2
                    ? 'Buat Toko'
                    : step === 3
                    ? 'Selesai'
                    : null}
                </Text>
              )}
            </Button>
          ) : null}
        </Content>
      </RBSheet>

      <AppNavigation state={nav} dispatch={dispatch} uriPrefix={'tokook://'} />
    </View>
  );
}

const mapStateToProps = (state) => ({
  nav: state.nav,
  isModalVisible: state.app.modal.data,
  tokoAddressData: state.toko.tokoAddressData.data,
  userData: state.auth.userData,
  provinceData: state.toko.provinceData.data,
  cityData: state.toko.cityData.data,
  districtData: state.toko.districtData.data,
  postalCodeData: state.toko.postalCodeData.data,
  getOtpWa: state.auth.getOtpPostData,
  tokoData: state.toko.tokoData.data
    ? state.toko.tokoData.data.listToko
      ? state.toko.tokoData.data.listToko.length
        ? state.toko.tokoData.data.listToko[0]
        : null
      : null
    : null,
  tokoId: state.toko.tokoData.data
    ? state.toko.tokoData.data.listToko
      ? state.toko.tokoData.data.listToko.length
        ? state.toko.tokoData.data.listToko[0].id
        : null
      : null
    : null,
});
export default connect(mapStateToProps)(ReduxNavigation);
