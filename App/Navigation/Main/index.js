import LoginScreen from '../../Containers/LoginModule/Intro';
import OtpScreen from '../../Containers/AuthenticationModule/Otp';
import TokoScreen from '../../Containers/TokoModule/Intro';
import DropshipDetailScreen from '../../Containers/TokoModule/Dropship/Detail';
import DropshipIntroScreen from '../../Containers/TokoModule/Dropship/Intro';
import AddProductDropship from '../../Containers/TokoModule/Dropship/Add';
import OrderIntroScreen from '../../Containers/TokoModule/Order/Intro';
import OrderDetailScreen from '../../Containers/TokoModule/Order/Detail';
import Category from '../../Containers/TokoModule/Category';
import FinanceIntroScreen from '../../Containers/TokoModule/Admin/Finance/Intro';
import FinanceTransactionScreen from '../../Containers/TokoModule/Admin/Finance/Transaction';
import FinanceBankTransferScreen from '../../Containers/TokoModule/Admin/Finance/BankTransfer';
import FinanceKYCScreen from '../../Containers/TokoModule/Admin/Finance/KYC';
import CameraScreen from '../../Containers/Miscellaneous/CameraModule';
import ShippingMethodScreen from '../../Containers/TokoModule/Admin/ShippingMethod';
import AddProduct from '../../Containers/TokoModule/Product/Add/';
import EditProduct from '../../Containers/TokoModule/Product/Edit/';
import {TokoOKRouter} from '../BottomTab';
import StartupModule from '../../Containers/Miscellaneous/StartupModule';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createReduxContainer} from 'react-navigation-redux-helpers';
import {createStackNavigator} from 'react-navigation-stack';
const MainNavigator = createStackNavigator(
  {
    StartupModule: {
      screen: StartupModule,
      navigationOptions: {
        headerShown: false,
      },
    },
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    OtpScreen: {
      screen: OtpScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    Dashboard: {
      screen: TokoOKRouter,
      navigationOptions: {
        headerShown: false,
      },
      path: 'dashboard',
    },
    DropshipAddScreen: {
      screen: AddProductDropship,
      navigationOptions: {
        headerShown: false,
      },
    },
  },

  //   TokoScreen: {
  //     screen: TokoScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   DropshipIntroScreen: {
  //     screen: DropshipIntroScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   DropshipDetailScreen: {
  //     screen: DropshipDetailScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   OrderIntroScreen: {
  //     screen: OrderIntroScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   OrderDetailScreen: {
  //     screen: OrderDetailScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   CategoryScreen: {
  //     screen: Category,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   FinanceIntroScreen: {
  //     screen: FinanceIntroScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   //PRODUCT MAIN
  //   AddProduct: {
  //     screen: AddProduct,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   FinanceTransactionScreen: {
  //     screen: FinanceTransactionScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   EditProduct: {
  //     screen: EditProduct,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   FinanceBankTransferScreen: {
  //     screen: FinanceBankTransferScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  //   FinanceKYCScreen: {
  //     screen: FinanceKYCScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  // CameraScreen: {
  //   screen: CameraScreen,
  //   navigationOptions: {
  //     headerShown: false,
  //   },
  // },
  //   //DROPSHIP MAIN

  //   ShippingMethodScreen: {
  //     screen: ShippingMethodScreen,
  //     navigationOptions: {
  //       headerShown: false,
  //     },
  //   },
  // },
  {
    initialRouteName: 'StartupModule',
  },
);
const Main = createReduxContainer(MainNavigator);
export default Main;
