// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import Config from 'react-native-config';
// our "constructor"
const create = (baseURL = Config.SERVER_URL) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
    },
    // 10 second timeout...
    timeout: 10000,
  });

  const apiv2 = apisauce.create({
    // base URL is read from the "constructor"
    baseURL: Config.SERVER_URL_V2,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 10000,
  });
  const apiDataScience = apisauce.create({
    // base URL is read from the "constructor"
    baseURL: Config.DATA_SCIENCE_SERVER_URL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 10000,
  });
  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const setAuthToken = access_token => {
    api.setHeader('Authorization', 'Bearer '.concat(access_token));
    apiv2.setHeader('Authorization', 'Bearer '.concat(access_token));
    apiDataScience.setHeader('Authorization', 'Bearer '.concat(access_token));
  };
  //for deleting toko
  const deleteToko = data => api.delete(`toko/delete/${data.tokoId}/`);

  //for saving firebase token each devices
  const saveFirebaseToken = data => api.post('auth/notif_token/save/', data);

  //for getting toko list
  const getToko = () => api.get('toko/list/', null);

  //for getting their toko bank
  const getTokoBankApi = tokoId =>
    api.get(`toko/${tokoId}/rekening/list/`, null);

  //for getting toko address
  const getTokoAddressApi = tokoId =>
    api.get(`toko/${tokoId}/address/list/`, null);

  //get All Bank Data
  const getBankApi = () => api.get('util/bank_list/', null);

  //get All courier Data
  const getTokoCourierApi = () => api.get('toko/courier/list/', null);

  //get owner toko courier Data
  const getOwnerTokoCourierApi = tokoId =>
    api.get(`toko/${tokoId}/courier/list`, null);

  //for getting transaction list on order Tab
  const getTokoOrderTransactionApi = tokoId =>
    api.get(`toko/${tokoId}/transaction/list/`, null);

  //for getting transaction detail  on order Tab
  const getTokoOrderTransactionDetailApi = data =>
    api.get(`toko/${data.tokoId}/transaction/${data.transactionId}/`, null);

  //for proceed order transaction
  const postOrderTransactionApi = data =>
    api.post(`toko/${data.tokoId}/order/process/`, data.detail);

  //for send order transaction
  const postSendOrderTransactionApi = data =>
    api.post(`toko/${data.tokoId}/order/send/`, data.detail);

  //for finishing order transaction
  const postFinishOrderTransactionApi = data =>
    api.post(`toko/${data.tokoId}/order/finish/`, data.detail);

  //for accept payment
  const postPaymentAcceptApi = data =>
    api.post(`toko/${data.tokoId}/payment/accept/`, data.detail);
  //for declined payment
  const postPaymentDeclineApi = data =>
    api.post(`toko/${data.tokoId}/payment/decline/`, data.detail);

  //for getting address toko
  const postTokoAddressApi = data =>
    api.post(`toko/${data.tokoId}/address/add/`, data.detail);

  //for update address toko
  const updateTokoAddressApi = data =>
    api.put(`toko/${data.tokoId}/address/${data.addressId}/`, data.detail);

  //for withdraw disbursment toko
  const postWithdrawDisbursmentApi = data =>
    api.post(`toko/${data.tokoId}/withdraw/request`, data.detail);

  //for added toko bank toko
  const postTokoBankApi = data =>
    api.post(`toko/${data.tokoId}/rekening/add/`, data.detail);
  //for activated owner toko courier
  const postActivatedOwnerTokoCourierApi = data =>
    api.post(`toko/${data.tokoId}/courier/activate/`, data.detail);

  //for deactivated owner toko courier
  const deleteActivatedOwnerTokoCourierApi = data =>
    api.delete(
      `toko/${data.tokoId}/courier/deactivate/`,
      {},
      {data: data.detail},
    );

  //for set bank rekening default
  const updateRekeningDefaultApi = data =>
    api.put(`toko/${data.tokoId}/rekening/${data.bankId}/set_default`);

  //for update bank toko toko
  const updateTokoBankApi = data =>
    api.put(`toko/${data.tokoId}/rekening/${data.rekId}/`, data.detail);
  //for delete bank toko toko
  const deleteTokoBankApi = data =>
    api.delete(
      `toko/${data.tokoId}/rekening/${data.rekId}/delete`,
      data.detail,
    );

  //for checking kwc status
  const getKycStatusApi = () => api.get('toko/kyc/status', null);

  //for added kwc
  const postKycApi = data => {
    const form = new FormData();
    form.append('path', {
      uri: 'aa',
      name: 'tttt',
      type: 'image/jpeg',
    });
    form.append('image_category', 'aaaa');

    return new Promise((resolve, reject) => {
      api
        .post(`toko/kyc/request`, data, {
          headers: {
            Accept: 'multipart/form-data',
            'Content-Type':
              'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
          },
        })
        .then(response => {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        });
    });
  };
  //for getting province
  const getProvinceApi = () => api.get('util/province_list/', null);

  //for getting withdraw history
  const getWithdrawHistoryApi = tokoId =>
    api.get(`toko/${tokoId}/withdraw/history/`, null);

  //for getting city
  const getCityApi = provinceId =>
    api.get(`util/city_list?province=${provinceId}`, null);

  //for getting postal code
  const getDistrictApi = cityId =>
    api.get(`util/district_list?city_id=${cityId}`, null);

  //for getting postal code
  const getPostalCodeApi = data =>
    api.get(
      `util/postalcode_list?city=${data.cityName}&district=${data.districtName}`,
      null,
    );
  //for getting toko financial code
  const getTokoFinancialApi = tokoId =>
    api.get(`toko/${tokoId}/financial`, null);
  //for getting market place product
  const getMarketPlaceProductByUrl = data =>
    apiDataScience.post('scrape-product/', JSON.stringify(data));

  //for getting toko details
  const getDetailToko = id => api.get('toko/details/' + id, null);
  //for getting toko category
  const getCategoryToko = id => api.get(`toko/${id}/category/list/`);
  //for adding category product
  const postCategoryToko = data =>
    api.post(`toko/${data.tokoId}/category/create/`, JSON.stringify(data.item));
  //for updating category toko
  const updateCategoryToko = data =>
    api.put(
      `toko/${data.tokoId}/category/${data.categoryId}/`,
      JSON.stringify(data.item),
    );
  //for updating position category toko
  const updatePositionCategoryToko = data =>
    api.put(
      `toko/${data.tokoId}/category/change_position/`,
      JSON.stringify(data.item),
    );

  //for deleting category toko
  const deleteCategoryToko = data =>
    api.delete(
      `toko/${data.tokoId}/category/${data.categoryId}/`,
      {},
      {data: data.item},
    );

  //for getting supplier list
  const getSupplier = () => api.get(`toko/supplier/list/`);
  //for getting toko product
  const getProductToko = id => apiv2.get(`toko/${id}/product/list/`);
  const getDetailProduct = (id, productId) =>
    apiv2.get(`toko/${id}/product/${productId}`);
  //for adding toko product
  const postProductToko = (id, data, isVariant) => {
    return apiv2.post(`toko/${id}/product/create?variant=${isVariant}`, data);
  };
  //for update toko product
  const updateProductToko = (id, data, isVariant, productId) => {
    console.log('isVarian', isVariant, data);

    return apiv2.put(
      `toko/${id}/product/${productId}/update?variant=${isVariant}`,
      data,
    );
  };
  const miniUpdateProductToko = (id, data, productId) => {
    return apiv2.put(`toko/${id}/product/${productId}/miniupdate`, data);
  };
  const deleteProductToko = data =>
    api.delete(`toko/${data.tokoId}/product/${data.productId}/delete/`);

  //for upload product image
  const uploadProductImages = data => {
    const form = new FormData();
    form.append('path', {
      uri: 'aa',
      name: 'tttt',
      type: 'image/jpeg',
    });
    form.append('image_category', 'aaaa');

    return new Promise((resolve, reject) => {
      api
        .post(`toko/${data.tokoId}/product/image/upload/`, data.item, {
          headers: {
            'Content-Type':
              'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
          },
        })
        .then(response => {
          resolve(response);
        })
        .catch(function(error) {
          reject(error);
        });
    });
  };

  //for deleting token user incase they want to logout
  const deleteAuthToken = () => api.get('auth/logout/', null);
  //for refresh token incase expired
  const postRefreshAuthToken = data => api.post('auth/refresh', data);
  //for login with otp from first screen
  const postLoginOtpApi = data => api.post('auth/login', data);
  //for register with otp from first screen
  const postRegisterOtpApi = data => api.post('auth/register', data);
  //for login with email from first screen
  const postLoginEmailApi = data => api.post('auth/otp/get', data);
  //for get otp verif wa
  const getOtpWaApi = data => api.post('auth/otp/whatsapp/get', data);
  //for get verif wa
  const whatsappVerificationApi = data =>
    api.post('auth/whatsapp/verification', data);
  //for get user data
  const getUserApi = data => api.get('auth/user', data);
  //

  //for creating toko
  const postTokoApi = data => api.post('toko/create/', data);

  //for update toko
  const updateTokoApi = data =>
    api.post(`toko/update/${data.tokoId}/`, data.item);

  //for getting business type (jenis usaha)
  const getBusinessTypeApi = () => api.get('toko/business_type/', null);
  //get owner toko courier Data
  const getMarketplaceProducts = params =>
    api.get(`marketplace/products`, params);
  const getMarketplaceLastViewApi = params =>
    api.get(`marketplace/products/lastview`, params);
  const getMarketplaceSearchApi = params =>
    api.get(`marketplace/products/search`, params);
  const getMarketplaceProductDetails = data =>
    api.get(`marketplace/product/${data.product_id}`, null);

  const getMarketplaceTrendingProducts = data =>
    api.get(
      `marketplace/products/trending?since=${data.date}&page=${data.page}&limit=${data.limit}`,
      null,
    );
  const getMarketplaceTerlarisProducts = data =>
    api.get(
      `marketplace/products/terlaris?since=${data.date}&page=${data.page}&limit=${data.limit}`,
      null,
    );
  const postProductToOwnToko = data => api.post(`marketplace/catalog`, data);
  const postActivityLog = data => api.post(`marketplace/log`, data);

  const postCheckout = data => api.post(`marketplace/checkout`, data);

  //cart marketplace
  const postToCart = data => api.post(`marketplace/cart`, data);
  const getCartData = data => api.get(`marketplace/cart`, data);
  const getPaymentChannelApi = () =>
    api.get(`marketplace/paymentchannel`, null);
  const getPaymentAdminFeeApi = data => api.post(`marketplace/adminfee`, data);
  const updateCartData = data => api.put(`marketplace/cart`, data);
  const deleteCartData = data =>
    api.delete(`marketplace/cart`, {}, {data: data});

  const getTutorialBannerData = data =>
    api.get(`marketplace/tutorialBanner`, data);

  const getAllShippingFee = data =>
    api.post(`marketplace/shipping/fee/all`, data);
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    postCheckout,
    getTokoCourierApi,
    postFinishOrderTransactionApi,
    postSendOrderTransactionApi,
    postPaymentDeclineApi,
    updateTokoBankApi,
    deleteTokoBankApi,
    getTokoFinancialApi,
    getTokoOrderTransactionDetailApi,
    postPaymentAcceptApi,
    getSupplier,
    getBankApi,
    getTokoOrderTransactionApi,
    postTokoBankApi,
    postWithdrawDisbursmentApi,
    postOrderTransactionApi,
    postTokoAddressApi,
    updateTokoAddressApi,
    getTokoAddressApi,
    getTokoBankApi,
    saveFirebaseToken,
    getMarketPlaceProductByUrl,
    getDetailToko,
    getCategoryToko,
    postCategoryToko,
    getMarketplaceSearchApi,
    updateCategoryToko,
    getOwnerTokoCourierApi,
    updatePositionCategoryToko,
    deleteCategoryToko,
    getProductToko,
    getDetailProduct,
    postProductToko,
    updateProductToko,
    miniUpdateProductToko,
    deleteProductToko,
    getPaymentAdminFeeApi,
    deleteToko,
    getToko,
    uploadProductImages,
    deleteAuthToken,
    getBusinessTypeApi,
    setAuthToken,
    postRefreshAuthToken,
    postTokoApi,
    getMarketplaceLastViewApi,
    updateTokoApi,
    postLoginEmailApi,
    postLoginOtpApi,
    getOtpWaApi,
    whatsappVerificationApi,
    getUserApi,
    postRegisterOtpApi,
    getProvinceApi,
    getCityApi,
    getDistrictApi,
    getPostalCodeApi,
    updateRekeningDefaultApi,
    postActivatedOwnerTokoCourierApi,
    deleteActivatedOwnerTokoCourierApi,
    getKycStatusApi,
    postKycApi,
    getWithdrawHistoryApi,
    getMarketplaceProducts,
    getMarketplaceProductDetails,
    getMarketplaceTrendingProducts,
    postProductToOwnToko,
    postActivityLog,
    getMarketplaceTerlarisProducts,
    getAllShippingFee,
    //cart

    postToCart,
    getCartData,
    updateCartData,
    deleteCartData,
    getPaymentChannelApi,
    //tutorial
    getTutorialBannerData,
  };
};

// let's return back our create method as the default.
export default {
  create,
};
