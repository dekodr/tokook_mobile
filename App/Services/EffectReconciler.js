import API from './Api';

const api = API.api;
export default async (effect) => {
  const { uri, service, params } = effect;
  const type = effect.type ? effect.type : 'notype'; 
  let request = null;
  const apiService = API[service];
  switch (type && type.toLowerCase()) {
    case 'get':
      request = api.get(uri, params);                
      break;
  
    case 'post':
      request = api.post(uri, params);
      break;

    case 'notype':
      request = apiService(params);
      break;
  }

  return new Promise((resolve, reject) => {
    request.then(response => {
      if (response.ok) {
        resolve(response);
      }
      reject(response);
    });
  });
};