import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  productRecomendationRequest: ['payload', 'append'],
  productRecomendationRequestSuccess: ['productRecomendation'],
  productRecomendationRequestAppend: ['productRecomendation'],
  productRecomendationRequestFailed: ['error'],
});

export const MarketPlaceNewTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  error: null,
  fetching: false,
  productRecomendation: false,
  message: null,
});

/* ------------- Reducers ------------- */

export const request = (state, append) => state.merge({fetching: true});

export const success = (state, {productRecomendation}) =>
  state.merge({fetching: false, error: null, productRecomendation});

export const append = (state, {productRecomendation}) => {
  productRecomendation = state.productRecomendation.data
    ? Immutable.setIn(
        productRecomendation,
        ['data'],
        state.productRecomendation.data.concat(productRecomendation.data),
      )
    : productRecomendation;
  return state.merge({fetching: false, productRecomendation});
};

export const failure = (state, {error}) =>
  state.merge({fetching: false, error});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRODUCT_RECOMENDATION_REQUEST]: request,
  [Types.PRODUCT_RECOMENDATION_REQUEST_SUCCESS]: success,
  [Types.PRODUCT_RECOMENDATION_REQUEST_APPEND]: append,
  [Types.PRODUCT_RECOMENDATION_REQUEST_FAILED]: failure,
});
