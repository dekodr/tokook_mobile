import AsyncStorage from '@react-native-community/async-storage';

// Rehydration.js
export const updateReducers = store => {
  const reducerVersion = ReduxPersist.reducerVersion;
  const startup = () => store.dispatch(StartupActions.startup());
  const persistor = persistStore(store, null, startup);

  // Check to ensure latest reducer version
  AsyncStorage.getItem('reducerVersion')
    .then(localVersion => {
      if (localVersion !== reducerVersion) {
        if (DebugConfig.useReactotron) {
          console.tron.display({
            name: 'PURGE',
            value: {
              'Old Version:': localVersion,
              'New Version:': reducerVersion,
            },
            preview: 'Reducer Version Change Detected',
            important: true,
          });
        }
        // Purge store
        AsyncStorage.setItem('reducerVersion', reducerVersion);
        persistor.purge();
      }
    })
    .catch(() => {
      AsyncStorage.setItem('reducerVersion', reducerVersion);
    });

  return persistor;
};
