import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  // ================================== TYPE POST OTP WITHDRAW DISBURSMENT DATA ==================================== //
  otpWithdrawDisbursmentPostRequest: ['data'],
  otpWithdrawDisbursmentPostSuccess: ['data'],
  otpWithdrawDisbursmentPostFailed: ['error'],
  otpWithdrawDisbursmentPostReset: null,
  // ================================== TYPE POST EMAIL LOGIN DATA ==================================== //
  loginEmailPostRequest: ['data'],
  loginEmailPostSuccess: ['data'],
  loginEmailPostFailed: ['error'],
  loginEmailPostReset: null,
  loginEmailPostLoadingReset: null,
  // ================================== TYPE POST LOGIN OTP DATA ==================================== //
  loginOtpPostRequest: ['data'],
  loginOtpPostSuccess: ['data'],
  loginOtpPostFailed: ['error'],
  loginOtpAfterSuccess: null,
  // ================================== TYPE POST REGISTER OTP DATA ==================================== //
  registerOtpPostRequest: ['data'],
  registerOtpPostSuccess: ['data'],
  registerOtpPostFailed: ['error'],
  otpPostReset: null,
  // ================================== TYPE DELETE AUTH TOKEN USER ==================================== //
  deleteAuthTokenUserRequest: ['data'],
  deleteAuthTokenUserSuccess: ['data'],
  deleteAuthTokenUserFailed: ['error'],
  deleteAuthTokenUserReset: null,

  // ================================== TYPE POST GET OTP WA DATA ==================================== //
  getOtpPostRequest: ['data'],
  getOtpPostSuccess: ['data'],
  getOtpPostFailed: ['error'],
  getOtpPostReset: null,
  // ================================== TYPE VERIFICATION WA DATA ==================================== //
  whatsappVerificationRequest: ['data'],
  whatsappVerificationSuccess: ['data'],
  whatsappVerificationFailed: ['error'],
  whatsappVerificationReset: null,
  // ================================== TYPE VERIFICATION WA DATA ==================================== //
  getUserRequest: ['data'],
  getUserSuccess: ['data'],
  getUserFailed: ['error'],
  getUserReset: null,
});

export const LoginEmailTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  getOtpPostData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
  userData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
  whatsappVerificationData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
  loginEmailPostData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  otpWithdrawDisbursmentPostData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  otpPostData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  deleteAuthToken: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
});

/* ------------- Selectors ------------- */

export const getAccessToken = state =>
  state.auth.loginEmailPostData.otpPostData.data.access_token;

export const loginEmailSelectors = {};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST OTP WITHDRAW DISBURSMENT DATA ------------------------------ //
export const otpWithdrawDisbursmentPostRequest = (state, {data}) => {
  return state.setIn(['otpWithdrawDisbursmentPostData', 'fetching'], true);
  // .setIn(['loginEmailPostData', 'data'], data);
};
export const otpWithdrawDisbursmentPostSuccess = (state, {data}) => {
  return state
    .setIn(['otpWithdrawDisbursmentPostData', 'error'], false)
    .setIn(['otpWithdrawDisbursmentPostData', 'success'], true)
    .setIn(['otpWithdrawDisbursmentPostData', 'fetching'], false)
    .setIn(['otpWithdrawDisbursmentPostData', 'data'], data);
};
export const otpWithdrawDisbursmentPostFailed = (state, {error}) => {
  return state
    .setIn(['otpWithdrawDisbursmentPostData', 'fetching'], false)
    .setIn(['otpWithdrawDisbursmentPostData', 'error'], error);
};
export const otpWithdrawDisbursmentPostReset = (state, {error}) => {
  return state
    .setIn(['otpWithdrawDisbursmentPostData', 'data'], null)
    .setIn(['otpWithdrawDisbursmentPostData', 'fetching'], false)
    .setIn(['otpWithdrawDisbursmentPostData', 'error'], false)
    .setIn(['otpWithdrawDisbursmentPostData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST EMAIL LOGIN DATA ------------------------------ //
export const loginEmailPostRequest = (state, {data}) => {
  return state.setIn(['loginEmailPostData', 'fetching'], true);
  // .setIn(['loginEmailPostData', 'data'], data);
};
export const loginEmailPostSuccess = (state, {data}) => {
  return state
    .setIn(['loginEmailPostData', 'error'], false)
    .setIn(['loginEmailPostData', 'success'], true)
    .setIn(['loginEmailPostData', 'fetching'], false)
    .setIn(['loginEmailPostData', 'data'], data);
};
export const loginEmailPostFailed = (state, {error}) => {
  return state
    .setIn(['loginEmailPostData', 'fetching'], false)
    .setIn(['loginEmailPostData', 'error'], error);
};
export const loginEmailPostReset = (state, {error}) => {
  return state
    .setIn(['loginEmailPostData', 'data'], null)
    .setIn(['loginEmailPostData', 'fetching'], false)
    .setIn(['loginEmailPostData', 'error'], false)
    .setIn(['loginEmailPostData', 'success'], false);
};
export const loginEmailPostLoadingReset = state => {
  return state.setIn(['loginEmailPostData', 'fetching'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST LOGIN OTP DATA ------------------------------ //
export const loginOtpPostRequest = (state, {data}) => {
  return state
    .setIn(['otpPostData', 'fetching'], true)
    .setIn(['otpPostData', 'data'], data);
};
export const loginOtpPostSuccess = (state, {data}) => {
  return state
    .setIn(['otpPostData', 'success'], true)
    .setIn(['otpPostData', 'error'], false)

    .setIn(['otpPostData', 'fetching'], false)
    .setIn(['otpPostData', 'data'], data);
};
export const loginOtpPostFailed = (state, {error}) => {
  return state
    .setIn(['otpPostData', 'fetching'], false)
    .setIn(['otpPostData', 'error'], error);
};
export const loginOtpAfterSuccess = state => {
  return state.setIn(['otpPostData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST EMAIL LOGIN DATA ------------------------------ //
export const registerOtpPostRequest = (state, {data}) => {
  return state
    .setIn(['otpPostData', 'fetching'], true)
    .setIn(['otpPostData', 'data'], data);
};
export const registerOtpPostSuccess = (state, {data}) => {
  return state
    .setIn(['otpPostData', 'fetching'], false)
    .setIn(['otpPostData', 'data'], data);
};
export const registerOtpPostFailed = (state, {error}) => {
  return state
    .setIn(['otpPostData', 'fetching'], false)
    .setIn(['otpPostData', 'error'], error);
};
export const otpPostReset = (state, {error}) => {
  return state
    .setIn(['otpPostData', 'data'], {})
    .setIn(['otpPostData', 'fetching'], false)
    .setIn(['otpPostData', 'error'], false)
    .setIn(['otpPostData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST GET OTP WA DATA ------------------------------ //
export const getOtpPostRequest = (state, {data}) => {
  return state
    .setIn(['getOtpPostData', 'fetching'], true)
    .setIn(['getOtpPostData', 'success'], false);
};
export const getOtpPostSuccess = (state, {data}) => {
  return state
    .setIn(['getOtpPostData', 'fetching'], false)
    .setIn(['getOtpPostData', 'data'], data)
    .setIn(['getOtpPostData', 'error'], false)
    .setIn(['getOtpPostData', 'success'], true);
};
export const getOtpPostFailed = (state, {error}) => {
  return state
    .setIn(['getOtpPostData', 'fetching'], false)
    .setIn(['getOtpPostData', 'error'], error)
    .setIn(['getOtpPostData', 'data'], {});
};
export const getOtpPostReset = (state, {error}) => {
  return state
    .setIn(['getOtpPostData', 'data'], {})
    .setIn(['getOtpPostData', 'fetching'], false)
    .setIn(['getOtpPostData', 'error'], false)
    .setIn(['getOtpPostData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST GET OTP WA DATA ------------------------------ //
export const whatsappVerificationRequest = (state, {data}) => {
  return state
    .setIn(['whatsappVerificationData', 'fetching'], true)
    .setIn(['whatsappVerificationData', 'success'], false);
};
export const whatsappVerificationSuccess = (state, {data}) => {
  return state
    .setIn(['whatsappVerificationData', 'fetching'], false)
    .setIn(['whatsappVerificationData', 'data'], data)
    .setIn(['whatsappVerificationData', 'error'], false)
    .setIn(['whatsappVerificationData', 'success'], true);
};
export const whatsappVerificationFailed = (state, {error}) => {
  return state
    .setIn(['whatsappVerificationData', 'fetching'], false)
    .setIn(['whatsappVerificationData', 'error'], error)
    .setIn(['whatsappVerificationData', 'data'], {});
};
export const whatsappVerificationReset = (state, {error}) => {
  return state
    .setIn(['whatsappVerificationData', 'data'], {})
    .setIn(['whatsappVerificationData', 'fetching'], false)
    .setIn(['whatsappVerificationData', 'error'], false)
    .setIn(['whatsappVerificationData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST GET USER DATA ------------------------------ //
export const getUserRequest = (state, {data}) => {
  return state
    .setIn(['userData', 'fetching'], true)
    .setIn(['userData', 'success'], false);
};
export const getUserSuccess = (state, {data}) => {
  return state
    .setIn(['userData', 'fetching'], false)
    .setIn(['userData', 'data'], data)
    .setIn(['userData', 'error'], false)
    .setIn(['userData', 'success'], true);
};
export const getUserFailed = (state, {error}) => {
  return state
    .setIn(['userData', 'fetching'], false)
    .setIn(['userData', 'error'], error);
  // .setIn(['userData', 'data'], {});
};
export const getUserReset = (state, {error}) => {
  return state
    .setIn(['userData', 'data'], {})
    .setIn(['userData', 'fetching'], false)
    .setIn(['userData', 'error'], false)
    .setIn(['userData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS LOGOUT AUTH TOKEN USER ------------------------------ //
export const deleteAuthTokenUserRequest = (state, {data}) => {
  return state
    .setIn(['deleteAuthTokenUser', 'fetching'], true)
    .setIn(['deleteAuthTokenUser', 'data'], data);
};
export const deleteAuthTokenUserSuccess = (state, {data}) => {
  return state
    .setIn(['deleteAuthTokenUser', 'fetching'], false)
    .setIn(['deleteAuthTokenUser', 'data'], data);
};
export const deleteAuthTokenUserFailed = (state, {error}) => {
  return state
    .setIn(['deleteAuthTokenUser', 'fetching'], false)
    .setIn(['deleteAuthTokenUser', 'error'], error);
};
export const deleteAuthTokenUserReset = (state, {error}) => {
  return state
    .setIn(['deleteAuthTokenUser', 'data'], {})
    .setIn(['deleteAuthTokenUser', 'fetching'], false)
    .setIn(['deleteAuthTokenUser', 'error'], false)
    .setIn(['deleteAuthTokenUser', 'success'], false);
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /*-------------------POST TOKO DATA-----------------*/
  [Types.LOGIN_EMAIL_POST_REQUEST]: loginEmailPostRequest,
  [Types.LOGIN_EMAIL_POST_SUCCESS]: loginEmailPostSuccess,
  [Types.LOGIN_EMAIL_POST_FAILED]: loginEmailPostFailed,
  [Types.LOGIN_EMAIL_POST_RESET]: loginEmailPostReset,
  [Types.LOGIN_EMAIL_POST_LOADING_RESET]: loginEmailPostLoadingReset,

  /*-------------------POST GET OTP WA DATA-----------------*/
  [Types.GET_OTP_POST_REQUEST]: getOtpPostRequest,
  [Types.GET_OTP_POST_SUCCESS]: getOtpPostSuccess,
  [Types.GET_OTP_POST_FAILED]: getOtpPostFailed,
  [Types.GET_OTP_POST_RESET]: getOtpPostReset,
  /*-------------------POST VERIFICATION WHATSAPP DATA-----------------*/
  [Types.WHATSAPP_VERIFICATION_REQUEST]: whatsappVerificationRequest,
  [Types.WHATSAPP_VERIFICATION_SUCCESS]: whatsappVerificationSuccess,
  [Types.WHATSAPP_VERIFICATION_FAILED]: whatsappVerificationFailed,
  [Types.WHATSAPP_VERIFICATION_RESET]: whatsappVerificationReset,
  /*-------------------POST VERIFICATION WHATSAPP DATA-----------------*/
  [Types.GET_USER_REQUEST]: getUserRequest,
  [Types.GET_USER_SUCCESS]: getUserSuccess,
  [Types.GET_USER_FAILED]: getUserFailed,
  [Types.GET_USER_RESET]: getUserReset,
  /*-------------------POST LOGIN OTP DATA-----------------*/
  [Types.LOGIN_OTP_POST_REQUEST]: loginOtpPostRequest,
  [Types.LOGIN_OTP_POST_SUCCESS]: loginOtpPostSuccess,
  [Types.LOGIN_OTP_POST_FAILED]: loginOtpPostFailed,
  [Types.LOGIN_OTP_AFTER_SUCCESS]: loginOtpAfterSuccess,
  /*-------------------POST REGISTER OTP DATA-----------------*/
  [Types.REGISTER_OTP_POST_REQUEST]: registerOtpPostRequest,
  [Types.REGISTER_OTP_POST_SUCCESS]: registerOtpPostSuccess,
  [Types.REGISTER_OTP_POST_FAILED]: registerOtpPostFailed,
  [Types.OTP_POST_RESET]: otpPostReset,
  /*-------------------DELETE AUTH USER TOKEN-----------------*/
  [Types.DELETE_AUTH_TOKEN_USER_REQUEST]: deleteAuthTokenUserRequest,
  [Types.DELETE_AUTH_TOKEN_USER_SUCCESS]: deleteAuthTokenUserSuccess,
  [Types.DELETE_AUTH_TOKEN_USER_FAILED]: deleteAuthTokenUserFailed,
  [Types.DELETE_AUTH_TOKEN_USER_RESET]: deleteAuthTokenUserReset,
  /*-------------------POST OTP WITHDRAW DISBURSMENT DATA-----------------*/
  [Types.OTP_WITHDRAW_DISBURSMENT_POST_REQUEST]: otpWithdrawDisbursmentPostRequest,
  [Types.OTP_WITHDRAW_DISBURSMENT_POST_SUCCESS]: otpWithdrawDisbursmentPostSuccess,
  [Types.OTP_WITHDRAW_DISBURSMENT_POST_FAILED]: otpWithdrawDisbursmentPostFailed,
  [Types.OTP_WITHDRAW_DISBURSMENT_POST_RESET]: otpWithdrawDisbursmentPostReset,
});
