import {createStore, applyMiddleware, compose} from 'redux';
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';
import {composeWithDevTools} from 'remote-redux-devtools';
import {offline} from '@redux-offline/redux-offline';
import defaultOfflineConfig from '@redux-offline/redux-offline/lib/defaults';
import Config from '../Config/DebugConfig';
import createSagaMiddleware from 'redux-saga';
import ScreenTracking from './ScreenTrackingMiddleware';
import RefreshToken from './RefreshToken';
import effectReconciler from '../Services/EffectReconciler';
import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform';
import Reactotron from 'reactotron-react-native';
import {createMigrate, persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import autoMergeLevel1 from 'redux-persist/es/stateReconciler/autoMergeLevel1';

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = [];
  const enhancers = [];

  /* ------------- Analytics Middleware ------------- */
  middleware.push(ScreenTracking);
  // middleware.push(RefreshToken);

  /* ------------- Saga Middleware ------------- */
  // create our new saga monitor
  const sagaMonitor = Config.useReactotron
    ? Reactotron.createSagaMonitor
    : null;
  const sagaMiddleware = createSagaMiddleware({sagaMonitor});
  middleware.push(sagaMiddleware);

  /* ------------- Redux Navigation Middleware ------------- */

  const reduxNavigationMiddleware = createReactNavigationReduxMiddleware(
    // 'root',
    state => state.nav,
  );
  middleware.push(reduxNavigationMiddleware);

  /* ------------- Assemble Middleware ------------- */
  enhancers.push(applyMiddleware(...middleware));

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore = Config.useReactotron
    ? Reactotron.createStore
    : createStore;
  const composer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : Config.useReduxDevTools
    ? composeWithDevTools({realtime: true, port: 50001})
    : compose;

  /* ------------- Redux Offline Configuration ------------- */
  const migrations = {
    0: state => {
      // migration clear out device state
      return {
        ...state,
        device: undefined,
      };
    },
    1: state => {
      // migration to keep only device state
      return {
        device: state.device,
      };
    },
  };
  const offlineConfig = {
    ...defaultOfflineConfig,

    persistOptions: {
      whitelist: ['app', 'toko', 'auth'],
      transforms: [immutablePersistenceTransform],
      migrate: createMigrate(migrations, {debug: false}),
      stateReconciler: autoMergeLevel1,
      key: 'primary',
      version: 1,
      storage: AsyncStorage,
    },

    retry: (action, retries) => null,
    returnPromises: true,
  };
  const persistConfig = {
    key: 'primary',
    version: 1,
    storage: AsyncStorage,
    migrate: createMigrate(migrations, {debug: false}),
    effect: effectReconciler,
    whitelist: ['app', 'toko', 'auth'],
    debug: true,
    blacklist: [],
    stateReconciler: autoMergeLevel2,

    transforms: [immutablePersistenceTransform],
  };
  const store = createStore(rootReducer, composer(...enhancers));

  // kick off root saga
  const sagasManager = sagaMiddleware.run(rootSaga);
  const persistor = persistStore(store);

  return {
    persistor,
    store,
    sagasManager,
    sagaMiddleware,
  };
};
