import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import configureStore from './CreateStore';
import rootSaga from '../Sagas/';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults/index';
import {createOffline} from '@redux-offline/redux-offline';
import ReduxPersistConfig from '../Config/ReduxPersistConfig';
/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  app: require('./GlobalRedux').reducer,
  language: require('./LanguageRedux').reducer,
  marketplace: require('./MarketPlaceRedux').reducer,
  toko: require('./TokoRedux').reducer,
  auth: require('./AuthenticationRedux').reducer,
  firebase: require('./FirebaseRedux').reducer,
  camera: require('./CameraRedux').reducer,
  marketplaceNew: require('./MarketplaceNewRedux').reducer,
});

export default () => {
  let finalReducers = reducers;
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersistConfig.active) {
    const {
      middleware: offlineMiddleware,
      enhanceReducer: offlineEnhanceReducer,
      enhanceStore: offlineEnhanceStore,
    } = createOffline({
      ...offlineConfig,
      persist: false,
    });
    const persistConfig = ReduxPersistConfig.storeConfig;
    finalReducers = persistReducer(
      persistConfig,
      offlineEnhanceReducer(reducers),
    );
  }
  let {store, sagasManager, sagaMiddleware, persistor} = configureStore(
    finalReducers,
    rootSaga,
  );

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers;
      store.replaceReducer(nextRootReducer);

      const newYieldedSagas = require('../Sagas').default;
      sagasManager.cancel();
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware(newYieldedSagas);
      });
    });
  }

  return {store, persistor};
};
