import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  // ================================== TYPE ADD FOTO KTP ==================================== //
  takePictureKtpRequest: ['data'],
  takePictureKtpSuccess: ['data'],
  takePictureKtpFailed: ['error'],
  takePictureKtpReset: null,

  // ================================== TYPE ADD FOTO SELFIE KTP ==================================== //
  takePictureKtpSelfieSuccess: ['data'],
  takePictureKtpSelfieReset: null,

  // ================================== RESET ALL STATE ==================================== //
  CameraReset: null,
});

export const CameraTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  ktp: {
    fetching: false,
    ktpData: null,
    ktpSelfieData: null,
    error: false,
    success: false,
  },
});

/* ------------- Reducers ------------- */

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS TAKE KTP PICTURE ------------------------------ //
export const takePictureKtpRequest = (state, {data}) => {
  return state.setIn(['ktp', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const takePictureKtpSuccess = (state, {data}) => {
  return state
    .setIn(['ktp', 'fetching'], false)
    .setIn(['ktp', 'ktpData'], data)
    .setIn(['ktp', 'error'], false)
    .setIn(['ktp', 'success'], true);
};
export const takePictureKtpSelfieSuccess = (state, {data}) => {
  return state
    .setIn(['ktp', 'fetching'], false)
    .setIn(['ktp', 'ktpSelfieData'], data)
    .setIn(['ktp', 'error'], false)
    .setIn(['ktp', 'success'], true);
};
export const takePictureKtpFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['ktp', 'fetching'], false)
      .setIn(['ktp', 'error'], error)
      .setIn(['ktp', 'success'], false)
  );
};
export const takePictureKtpReset = (state, {error}) => {
  return state
    .setIn(['ktp', 'ktpData'], null)

    .setIn(['ktp', 'fetching'], false)
    .setIn(['ktp', 'error'], false)
    .setIn(['ktp', 'success'], false);
};
export const takePictureKtpSelfieReset = (state, {error}) => {
  return state

    .setIn(['ktp', 'ktpSelfieData'], null)
    .setIn(['ktp', 'fetching'], false)
    .setIn(['ktp', 'error'], false)
    .setIn(['ktp', 'success'], false);
};

export const reset = () => INITIAL_STATE;
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /*-------------------TAKE PICTURE KTP-----------------*/
  [Types.TAKE_PICTURE_KTP_REQUEST]: takePictureKtpRequest,
  [Types.TAKE_PICTURE_KTP_SUCCESS]: takePictureKtpSuccess,
  [Types.TAKE_PICTURE_KTP_FAILED]: takePictureKtpFailed,
  [Types.TAKE_PICTURE_KTP_RESET]: takePictureKtpReset,
  /*-------------------TAKE PICTURE KTP SELFIE-----------------*/
  [Types.TAKE_PICTURE_KTP_SELFIE_SUCCESS]: takePictureKtpSelfieSuccess,
  [Types.TAKE_PICTURE_KTP_SELFIE_RESET]: takePictureKtpSelfieReset,
  [Types.CAMERA_RESET]: reset,
});

/* ------------- Selectors ------------- */
export const callData = state => state.camera.data;
