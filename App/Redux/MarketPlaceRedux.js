import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  // ================================== TYPE POST ADMIN FEE PAYMENT ==================================== //

  postPaymentAdminFeeRequest: ['data'],
  postPaymentAdminFeeSuccess: ['data'],
  postPaymentAdminFeeFailed: ['error'],
  postPaymentAdminFeeReset: null,

  // ================================== TYPE POST CHECKOUT ==================================== //

  postCheckoutRequest: ['data'],
  postCheckoutSuccess: ['data'],
  postCheckoutFailed: ['error'],
  postCheckoutReset: null,

  // ================================== TYPE GET PAYMENT CHANNEL ==================================== //

  getPaymentChannelRequest: ['data'],
  getPaymentChannelSuccess: ['data'],
  getPaymentChannelFailed: ['error'],
  getPaymentChannelReset: null,

  // ================================== TYPE HANDLING CART FOR CHECKOUT ==================================== //
  addCartToCheckoutProceed: ['data'],
  addAddressToCheckoutProceed: ['data'],
  // ================================== TYPE GET MARKET PLACE PRODUCT BY SEARCH ==================================== //

  getProductBySearchRequest: ['data'],
  getProductBySearchSuccess: ['data'],
  getProductBySearchFailed: ['error'],
  getProductBySearchReset: null,

  // ================================== TYPE GET MARKET PLACE PRODUCT LAST VIEW ==================================== //

  getProductLastViewRequest: ['payload', 'append'],
  getProductLastViewSuccess: ['productLastView'],
  getProductLastViewAppend: ['productLastView'],
  getProductLastViewFailed: ['error'],
  getProductLastViewReset: null,

  // ================================== TYPE GET MARKET PLACE PRODUCT BY URL ==================================== //

  getProductByUrlRequest: ['data'],
  getProductByUrlSuccess: ['data'],
  getProductByUrlFailed: ['error'],
  getProductByUrlReset: null,

  // MARKETPLACE
  getMarketplaceProductsRequest: ['data'],
  getMarketplaceProductsSuccess: ['data'],
  getMarketplaceProductsFailed: ['error'],
  getMarketplaceProductsReset: null,

  getMarketplaceTrendingProductsRequest: ['data'],
  getMarketplaceTrendingProductsSuccess: ['data'],
  getMarketplaceTrendingProductsFailed: ['error'],
  getMarketplaceTrendingProductsReset: null,

  getMarketplaceTerlarisProductsRequest: ['data'],
  getMarketplaceTerlarisProductsSuccess: ['data'],
  getMarketplaceTerlarisProductsFailed: ['error'],
  getMarketplaceTerlarisProductsReset: null,

  getMarketplaceProductDetailsRequest: ['data'],
  getMarketplaceProductDetailsSuccess: ['data'],
  getMarketplaceProductDetailsFailed: ['error'],
  getMarketplaceProductDetailsReset: null,

  postProductToOwnTokoRequest: ['data'],
  postProductToOwnTokoSuccess: ['data'],
  postProductToOwnTokoFailed: ['error'],
  postProductToOwnTokoReset: null,

  postActivityLogRequest: ['data'],
  postActivityLogSuccess: ['data'],
  postActivityLogFailed: ['error'],
  postActivityLogReset: null,

  //CART
  postToCartRequest: ['data'],
  postToCartSuccess: ['data'],
  postToCartFailed: ['error'],
  postToCartReset: null,

  getCartDataRequest: ['data'],
  getCartDataSuccess: ['data'],
  getCartDataFailed: ['error'],
  getCartDataReset: null,

  getTutorialBannerDataRequest: ['data'],
  getTutorialBannerDataSuccess: ['data'],
  getTutorialBannerDataFailed: ['error'],
  getTutorialBannerDataReset: null,

  updateCartDataRequest: ['data'],
  updateCartDataSuccess: ['data'],
  updateCartDataFailed: ['error'],
  updateCartDataReset: null,

  deleteCartDataRequest: ['data'],
  deleteCartDataSuccess: ['data'],
  deleteCartDataFailed: ['error'],
  deleteCartDataReset: null,

  getAllShippingFeeRequest: ['data'],
  getAllShippingFeeSuccess: ['data'],
  getAllShippingFeeFailed: ['error'],
  getAllShippingFeeReset: null,

  setShippingFeeData: ['data'],
  setShippingDestinationData: ['data'],
});

export const MarketPlaceTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  postPaymentAdminFeeData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postCheckoutData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  paymentChannelData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  cartCheckoutData: {
    data: null,
  },
  addressCheckoutData: {
    data: null,
  },
  productBySearchData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  productLastViewData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  shippingFeeData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postActivityLogData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postToCartData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  getCartData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  updateCartData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  deleteCartData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  productByUrlData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  MarketplaceProductsData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  MarketplaceTrendingProductsData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  MarketplaceTerlarisProductsData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  MarketplaceProductDetailsData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postProductToOwnTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  getTutorialBannerData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  shippingFee: {
    data: null,
  },
  shippingDestinationData: {
    data: null,
  },
});

/* ------------- Selectors ------------- */

export const marketPlaceSelectors = {
  getData: state => state.data,
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS TO CHECKOUT PROCEED ------------------------------ //
export const setCartCheckoutProceed = (state, {data}) => {
  return state.setIn(['cartCheckoutData', 'data'], data);
  // .setIn(['tokoData', 'data'], data);
};
export const setAddressCheckoutProceed = (state, {data}) => {
  return state.setIn(['addressCheckoutData', 'data'], data);
  // .setIn(['tokoData', 'data'], data);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS PAYMENT ADMIN FEE PAYMENT ------------------------------ //
export const postPaymentAdminFeeRequest = (state, {data}) => {
  return state.setIn(['postPaymentAdminFeeData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postPaymentAdminFeeSuccess = (state, {data}) => {
  return state
    .setIn(['postPaymentAdminFeeData', 'fetching'], false)
    .setIn(['postPaymentAdminFeeData', 'data'], data)
    .setIn(['postPaymentAdminFeeData', 'error'], false)
    .setIn(['postPaymentAdminFeeData', 'success'], true);
};
export const postPaymentAdminFeeFailed = (state, {error}) => {
  return state
    .setIn(['postPaymentAdminFeeData', 'data'], null)
    .setIn(['postPaymentAdminFeeData', 'fetching'], false)
    .setIn(['postPaymentAdminFeeData', 'error'], error)
    .setIn(['postPaymentAdminFeeData', 'success'], false);
};
export const postPaymentAdminFeeReset = (state, {error}) => {
  return state
    .setIn(['postPaymentAdminFeeData', 'data'], null)
    .setIn(['postPaymentAdminFeeData', 'fetching'], false)
    .setIn(['postPaymentAdminFeeData', 'error'], false)
    .setIn(['postPaymentAdminFeeData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS PAYMENT CHANNEL DATA ------------------------------ //
export const postCheckoutRequest = (state, {data}) => {
  return state.setIn(['postCheckoutData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postCheckoutSuccess = (state, {data}) => {
  return state
    .setIn(['postCheckoutData', 'fetching'], false)
    .setIn(['postCheckoutData', 'data'], data)
    .setIn(['postCheckoutData', 'error'], false)
    .setIn(['postCheckoutData', 'success'], true);
};
export const postCheckoutFailed = (state, {error}) => {
  return state
    .setIn(['postCheckoutData', 'data'], null)
    .setIn(['postCheckoutData', 'fetching'], false)
    .setIn(['postCheckoutData', 'error'], error)
    .setIn(['postCheckoutData', 'success'], false);
};
export const postCheckoutReset = (state, {error}) => {
  return state
    .setIn(['postCheckoutData', 'data'], null)
    .setIn(['postCheckoutData', 'fetching'], false)
    .setIn(['postCheckoutData', 'error'], false)
    .setIn(['postCheckoutData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS PAYMENT CHANNEL DATA ------------------------------ //
export const getPaymentChannelRequest = (state, {data}) => {
  return state.setIn(['paymentChannelData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getPaymentChannelSuccess = (state, {data}) => {
  return state
    .setIn(['paymentChannelData', 'fetching'], false)
    .setIn(['paymentChannelData', 'data'], data)
    .setIn(['paymentChannelData', 'error'], false)
    .setIn(['paymentChannelData', 'success'], true);
};
export const getPaymentChannelFailed = (state, {error}) => {
  return state
    .setIn(['paymentChannelData', 'data'], null)
    .setIn(['paymentChannelData', 'fetching'], false)
    .setIn(['paymentChannelData', 'error'], error)
    .setIn(['paymentChannelData', 'success'], false);
};
export const getPaymentChannelReset = (state, {error}) => {
  return state
    .setIn(['paymentChannelData', 'data'], null)
    .setIn(['paymentChannelData', 'fetching'], false)
    .setIn(['paymentChannelData', 'error'], false)
    .setIn(['paymentChannelData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET  PRODUCT BY SEARCH DATA ------------------------------ //
export const getProductBySearchRequest = (state, {data}) => {
  return state.setIn(['productBySearchData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getProductBySearchSuccess = (state, {data}) => {
  return state
    .setIn(['productBySearchData', 'fetching'], false)
    .setIn(['productBySearchData', 'data'], data)
    .setIn(['productBySearchData', 'error'], false)
    .setIn(['productBySearchData', 'success'], true);
};
export const getProductBySearchFailed = (state, {error}) => {
  return state
    .setIn(['productBySearchData', 'data'], null)
    .setIn(['productBySearchData', 'fetching'], false)
    .setIn(['productBySearchData', 'error'], error)
    .setIn(['productBySearchData', 'success'], false);
};
export const getProductBySearchReset = (state, {error}) => {
  return state
    .setIn(['productBySearchData', 'data'], null)
    .setIn(['productBySearchData', 'fetching'], false)
    .setIn(['productBySearchData', 'error'], false)
    .setIn(['productBySearchData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET  PRODUCT LAST VIEW DATA ------------------------------ //
export const getProductLastViewRequest = (state, {data}) => {
  return state.setIn(['productLastViewData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};

export const getProductLastViewSuccess = (state, {productLastView}) => {
  return state
    .setIn(['productLastViewData', 'fetching'], false)
    .setIn(['productLastViewData', 'data'], productLastView.data)
    .setIn(['productLastViewData', 'error'], false)
    .setIn(['productLastViewData', 'success'], true);
};
export const getProductLastViewAppend = (state, {productLastView}) => {
  productLastView = state.productLastView.data
    ? Immutable.setIn(
        'productLastViewData',
        ['data'],
        state.productLastView.data.concat(productLastView.data),
      )
    : productLastView;
  return state.merge({fetching: false, productLastView});
};
export const getProductLastViewFailed = (state, {error}) => {
  return state
    .setIn(['productLastViewData', 'data'], null)
    .setIn(['productLastViewData', 'fetching'], false)
    .setIn(['productLastViewData', 'error'], error)
    .setIn(['productLastViewData', 'success'], false);
};
export const getProductLastViewReset = (state, {error}) => {
  return state
    .setIn(['productLastViewData', 'data'], null)
    .setIn(['productLastViewData', 'fetching'], false)
    .setIn(['productLastViewData', 'error'], false)
    .setIn(['productLastViewData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET  PRODUCT BY URL DATA ------------------------------ //
export const getProductByUrlRequest = (state, {data}) => {
  return state.setIn(['productByUrlData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getProductByUrlSuccess = (state, {data}) => {
  return state
    .setIn(['productByUrlData', 'fetching'], false)
    .setIn(['productByUrlData', 'data'], data)
    .setIn(['productByUrlData', 'error'], false)
    .setIn(['productByUrlData', 'success'], true);
};
export const getProductByUrlFailed = (state, {error}) => {
  return state
    .setIn(['productByUrlData', 'data'], null)
    .setIn(['productByUrlData', 'fetching'], false)
    .setIn(['productByUrlData', 'error'], error)
    .setIn(['productByUrlData', 'success'], false);
};
export const getProductByUrlReset = (state, {error}) => {
  return state
    .setIn(['productByUrlData', 'data'], null)
    .setIn(['productByUrlData', 'fetching'], false)
    .setIn(['productByUrlData', 'error'], false)
    .setIn(['productByUrlData', 'success'], false);
};

// ------------------------------------- REDUCERS GET  MARKETPLACE DATA ------------------------------ //
export const getMarketplaceProductsRequest = (state, {data}) => {
  return state.setIn(['MarketplaceProductsData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getMarketplaceProductsSuccess = (state, {data}) => {
  return state
    .setIn(['MarketplaceProductsData', 'fetching'], false)
    .setIn(['MarketplaceProductsData', 'data'], data)
    .setIn(['MarketplaceProductsData', 'error'], false)
    .setIn(['MarketplaceProductsData', 'success'], true);
};
export const getMarketplaceProductsFailed = (state, {error}) => {
  return state
    .setIn(['MarketplaceProductsData', 'data'], null)
    .setIn(['MarketplaceProductsData', 'fetching'], false)
    .setIn(['MarketplaceProductsData', 'error'], error)
    .setIn(['MarketplaceProductsData', 'success'], false);
};
export const getMarketplaceProductsReset = (state, {error}) => {
  return state
    .setIn(['MarketplaceProductsData', 'data'], null)
    .setIn(['MarketplaceProductsData', 'fetching'], false)
    .setIn(['MarketplaceProductsData', 'error'], false)
    .setIn(['MarketplaceProductsData', 'success'], false);
};

// ------------------------------------- REDUCERS GET  MARKETPLACE DATA ------------------------------ //
export const getMarketplaceTrendingProductsRequest = (state, data) => {
  return state.setIn(['MarketplaceTrendingProductsData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getMarketplaceTrendingProductsSuccess = (state, {data}) => {
  console.log('reduxdata', data);
  return state
    .setIn(['MarketplaceTrendingProductsData', 'fetching'], false)
    .setIn(['MarketplaceTrendingProductsData', 'data'], data)
    .setIn(['MarketplaceTrendingProductsData', 'error'], false)
    .setIn(['MarketplaceTrendingProductsData', 'success'], true);
};
export const getMarketplaceTrendingProductsFailed = (state, error) => {
  return state
    .setIn(['MarketplaceTrendingProductsData', 'data'], null)
    .setIn(['MarketplaceTrendingProductsData', 'fetching'], false)
    .setIn(['MarketplaceTrendingProductsData', 'error'], error)
    .setIn(['MarketplaceTrendingProductsData', 'success'], false);
};
export const getMarketplaceTrendingProductsReset = (state, error) => {
  return state
    .setIn(['MarketplaceTrendingProductsData', 'data'], null)
    .setIn(['MarketplaceTrendingProductsData', 'fetching'], false)
    .setIn(['MarketplaceTrendingProductsData', 'error'], false)
    .setIn(['MarketplaceTrendingProductsData', 'success'], false);
};

export const getMarketplaceTerlarisProductsRequest = (state, data) => {
  return state.setIn(['MarketplaceTerlarisProductsData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getMarketplaceTerlarisProductsSuccess = (state, {data}) => {
  console.log('reduxdata', data);
  return state
    .setIn(['MarketplaceTerlarisProductsData', 'fetching'], false)
    .setIn(['MarketplaceTerlarisProductsData', 'data'], data)
    .setIn(['MarketplaceTerlarisProductsData', 'error'], false)
    .setIn(['MarketplaceTerlarisProductsData', 'success'], true);
};
export const getMarketplaceTerlarisProductsFailed = (state, error) => {
  return state
    .setIn(['MarketplaceTerlarisProductsData', 'data'], null)
    .setIn(['MarketplaceTerlarisProductsData', 'fetching'], false)
    .setIn(['MarketplaceTerlarisProductsData', 'error'], error)
    .setIn(['MarketplaceTerlarisProductsData', 'success'], false);
};
export const getMarketplaceTerlarisProductsReset = (state, error) => {
  return state
    .setIn(['MarketplaceTerlarisProductsData', 'data'], null)
    .setIn(['MarketplaceTerlarisProductsData', 'fetching'], false)
    .setIn(['MarketplaceTerlarisProductsData', 'error'], false)
    .setIn(['MarketplaceTerlarisProductsData', 'success'], false);
};

export const getMarketplaceProductDetailsRequest = (state, {data}) => {
  return state.setIn(['MarketplaceProductDetailsData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getMarketplaceProductDetailsSuccess = (state, {data}) => {
  return state
    .setIn(['MarketplaceProductDetailsData', 'fetching'], false)
    .setIn(['MarketplaceProductDetailsData', 'data'], data)
    .setIn(['MarketplaceProductDetailsData', 'error'], false)
    .setIn(['MarketplaceProductDetailsData', 'success'], true);
};
export const getMarketplaceProductDetailsFailed = (state, {error}) => {
  return state
    .setIn(['MarketplaceProductDetailsData', 'data'], null)
    .setIn(['MarketplaceProductDetailsData', 'fetching'], false)
    .setIn(['MarketplaceProductDetailsData', 'error'], error)
    .setIn(['MarketplaceProductDetailsData', 'success'], false);
};
export const getMarketplaceProductDetailsReset = (state, {error}) => {
  return state
    .setIn(['MarketplaceProductDetailsData', 'data'], null)
    .setIn(['MarketplaceProductDetailsData', 'fetching'], false)
    .setIn(['MarketplaceProductDetailsData', 'error'], false)
    .setIn(['MarketplaceProductDetailsData', 'success'], false);
};

export const postProductToOwnTokoRequest = (state, {data}) => {
  return state.setIn(['postProductToOwnTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postProductToOwnTokoSuccess = (state, {data}) => {
  return state
    .setIn(['postProductToOwnTokoData', 'fetching'], false)
    .setIn(['postProductToOwnTokoData', 'data'], data)
    .setIn(['postProductToOwnTokoData', 'error'], false)
    .setIn(['postProductToOwnTokoData', 'success'], true);
};
export const postProductToOwnTokoFailed = (state, {error}) => {
  return state
    .setIn(['postProductToOwnTokoData', 'data'], null)
    .setIn(['postProductToOwnTokoData', 'fetching'], false)
    .setIn(['postProductToOwnTokoData', 'error'], error)
    .setIn(['postProductToOwnTokoData', 'success'], false);
};
export const postProductToOwnTokoReset = (state, {error}) => {
  return state
    .setIn(['postProductToOwnTokoData', 'data'], null)
    .setIn(['postProductToOwnTokoData', 'fetching'], false)
    .setIn(['postProductToOwnTokoData', 'error'], false)
    .setIn(['postProductToOwnTokoData', 'success'], false);
};

export const postActivityLogRequest = (state, {data}) => {
  return state.setIn(['postActivityLogData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postActivityLogSuccess = (state, {data}) => {
  return state
    .setIn(['postActivityLogData', 'fetching'], false)
    .setIn(['postActivityLogData', 'data'], data)
    .setIn(['postActivityLogData', 'error'], false)
    .setIn(['postActivityLogData', 'success'], true);
};
export const postActivityLogFailed = (state, {error}) => {
  return state
    .setIn(['postActivityLogData', 'data'], null)
    .setIn(['postActivityLogData', 'fetching'], false)
    .setIn(['postActivityLogData', 'error'], error)
    .setIn(['postActivityLogData', 'success'], false);
};
export const postActivityLogReset = (state, {error}) => {
  return state
    .setIn(['postActivityLogData', 'data'], null)
    .setIn(['postActivityLogData', 'fetching'], false)
    .setIn(['postActivityLogData', 'error'], false)
    .setIn(['postActivityLogData', 'success'], false);
};
//cart

export const postToCartRequest = (state, {data}) => {
  return state.setIn(['postToCartData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postToCartSuccess = (state, {data}) => {
  return state
    .setIn(['postToCartData', 'fetching'], false)
    .setIn(['postToCartData', 'data'], data)
    .setIn(['postToCartData', 'error'], false)
    .setIn(['postToCartData', 'success'], true);
};
export const postToCartFailed = (state, {error}) => {
  return state
    .setIn(['postToCartData', 'data'], null)
    .setIn(['postToCartData', 'fetching'], false)
    .setIn(['postToCartData', 'error'], error)
    .setIn(['postToCartData', 'success'], false);
};
export const postToCartReset = (state, {error}) => {
  return state
    .setIn(['postToCartData', 'data'], null)
    .setIn(['postToCartData', 'fetching'], false)
    .setIn(['postToCartData', 'error'], false)
    .setIn(['postToCartData', 'success'], false);
};

export const getCartDataRequest = (state, {data}) => {
  return state.setIn(['getCartData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getCartDataSuccess = (state, {data}) => {
  return state
    .setIn(['getCartData', 'fetching'], false)
    .setIn(['getCartData', 'data'], data)
    .setIn(['getCartData', 'error'], false)
    .setIn(['getCartData', 'success'], true);
};
export const getCartDataFailed = (state, {error}) => {
  return state
    .setIn(['getCartData', 'data'], null)
    .setIn(['getCartData', 'fetching'], false)
    .setIn(['getCartData', 'error'], error)
    .setIn(['getCartData', 'success'], false);
};
export const getCartDataReset = (state, {error}) => {
  return state
    .setIn(['getCartData', 'data'], null)
    .setIn(['getCartData', 'fetching'], false)
    .setIn(['getCartData', 'error'], false)
    .setIn(['getCartData', 'success'], false);
};

export const getTutorialBannerDataRequest = (state, {data}) => {
  return state.setIn(['getTutorialBannerData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTutorialBannerDataSuccess = (state, {data}) => {
  return state
    .setIn(['getTutorialBannerData', 'fetching'], false)
    .setIn(['getTutorialBannerData', 'data'], data)
    .setIn(['getTutorialBannerData', 'error'], false)
    .setIn(['getTutorialBannerData', 'success'], true);
};
export const getTutorialBannerDataFailed = (state, {error}) => {
  return state
    .setIn(['getTutorialBannerData', 'data'], [])
    .setIn(['getTutorialBannerData', 'fetching'], false)
    .setIn(['getTutorialBannerData', 'error'], error)
    .setIn(['getTutorialBannerData', 'success'], false);
};
export const getTutorialBannerDataReset = (state, {error}) => {
  return state
    .setIn(['getTutorialBannerData', 'data'], [])
    .setIn(['getTutorialBannerData', 'fetching'], false)
    .setIn(['getTutorialBannerData', 'error'], false)
    .setIn(['getTutorialBannerData', 'success'], false);
};

export const updateCartDataRequest = (state, {data}) => {
  return state.setIn(['updateCartData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const updateCartDataSuccess = (state, {data}) => {
  return state
    .setIn(['updateCartData', 'fetching'], false)
    .setIn(['updateCartData', 'data'], data)
    .setIn(['updateCartData', 'error'], false)
    .setIn(['updateCartData', 'success'], true);
};
export const updateCartDataFailed = (state, {error}) => {
  return state
    .setIn(['updateCartData', 'data'], null)
    .setIn(['updateCartData', 'fetching'], false)
    .setIn(['updateCartData', 'error'], error)
    .setIn(['updateCartData', 'success'], false);
};
export const updateCartDataReset = (state, {error}) => {
  return state
    .setIn(['updateCartData', 'data'], null)
    .setIn(['updateCartData', 'fetching'], false)
    .setIn(['updateCartData', 'error'], false)
    .setIn(['updateCartData', 'success'], false);
};

export const deleteCartDataRequest = (state, {data}) => {
  return state.setIn(['deleteCartData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const deleteCartDataSuccess = (state, {data}) => {
  return state
    .setIn(['deleteCartData', 'fetching'], false)
    .setIn(['deleteCartData', 'data'], data)
    .setIn(['deleteCartData', 'error'], false)
    .setIn(['deleteCartData', 'success'], true);
};
export const deleteCartDataFailed = (state, {error}) => {
  return state
    .setIn(['deleteCartData', 'data'], null)
    .setIn(['deleteCartData', 'fetching'], false)
    .setIn(['deleteCartData', 'error'], error)
    .setIn(['deleteCartData', 'success'], false);
};
export const deleteCartDataReset = (state, {error}) => {
  return state
    .setIn(['deleteCartData', 'data'], null)
    .setIn(['deleteCartData', 'fetching'], false)
    .setIn(['deleteCartData', 'error'], false)
    .setIn(['deleteCartData', 'success'], false);
};

export const getAllShippingFeeRequest = (state, {data}) => {
  return state.setIn(['shippingFeeData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getAllShippingFeeSuccess = (state, {data}) => {
  return state
    .setIn(['shippingFeeData', 'fetching'], false)
    .setIn(['shippingFeeData', 'data'], data)
    .setIn(['shippingFeeData', 'error'], false)
    .setIn(['shippingFeeData', 'success'], true);
};
export const getAllShippingFeeFailed = (state, {error}) => {
  return state
    .setIn(['shippingFeeData', 'data'], [])
    .setIn(['shippingFeeData', 'fetching'], false)
    .setIn(['shippingFeeData', 'error'], error)
    .setIn(['shippingFeeData', 'success'], false);
};
export const getAllShippingFeeReset = (state, {error}) => {
  return state
    .setIn(['shippingFeeData', 'data'], [])
    .setIn(['shippingFeeData', 'fetching'], false)
    .setIn(['shippingFeeData', 'error'], false)
    .setIn(['shippingFeeData', 'success'], false);
};

export const setShippingFeeData = (state, {data}) => {
  return state.setIn(['shippingFee', 'data'], data);
};
export const setShippingDestinationData = (state, {data}) => {
  return state.setIn(['shippingDestinationData', 'data'], data);
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /*-------------------GET PAYMENT CHANNEL-----------------*/
  [Types.GET_PAYMENT_CHANNEL_REQUEST]: getPaymentChannelRequest,
  [Types.GET_PAYMENT_CHANNEL_SUCCESS]: getPaymentChannelSuccess,
  [Types.GET_PAYMENT_CHANNEL_FAILED]: getPaymentChannelFailed,
  [Types.GET_PAYMENT_CHANNEL_RESET]: getPaymentChannelReset,

  /*-------------------SET CART AND ADDRESS TO PROCEED CHECKOUT-----------------*/
  [Types.ADD_CART_TO_CHECKOUT_PROCEED]: setCartCheckoutProceed,
  [Types.ADD_ADDRESS_TO_CHECKOUT_PROCEED]: setAddressCheckoutProceed,

  /*-------------------GET MARKET PLACE PRODUCT BY URL DATA-----------------*/
  [Types.GET_PRODUCT_BY_URL_REQUEST]: getProductByUrlRequest,
  [Types.GET_PRODUCT_BY_URL_SUCCESS]: getProductByUrlSuccess,
  [Types.GET_PRODUCT_BY_URL_FAILED]: getProductByUrlFailed,
  [Types.GET_PRODUCT_BY_URL_RESET]: getProductByUrlReset,

  /*-------------------GET MARKET PLACE PRODUCTS-----------------*/
  [Types.GET_MARKETPLACE_PRODUCTS_REQUEST]: getMarketplaceProductsRequest,
  [Types.GET_MARKETPLACE_PRODUCTS_SUCCESS]: getMarketplaceProductsSuccess,
  [Types.GET_MARKETPLACE_PRODUCTS_FAILED]: getMarketplaceProductsFailed,
  [Types.GET_MARKETPLACE_PRODUCTS_RESET]: getMarketplaceProductsReset,

  [Types.GET_MARKETPLACE_TRENDING_PRODUCTS_REQUEST]: getMarketplaceTrendingProductsRequest,
  [Types.GET_MARKETPLACE_TRENDING_PRODUCTS_SUCCESS]: getMarketplaceTrendingProductsSuccess,
  [Types.GET_MARKETPLACE_TRENDING_PRODUCTS_FAILED]: getMarketplaceTrendingProductsFailed,
  [Types.GET_MARKETPLACE_TRENDING_PRODUCTS_RESET]: getMarketplaceTrendingProductsReset,

  [Types.GET_MARKETPLACE_TERLARIS_PRODUCTS_REQUEST]: getMarketplaceTerlarisProductsRequest,
  [Types.GET_MARKETPLACE_TERLARIS_PRODUCTS_SUCCESS]: getMarketplaceTerlarisProductsSuccess,
  [Types.GET_MARKETPLACE_TERLARIS_PRODUCTS_FAILED]: getMarketplaceTerlarisProductsFailed,
  [Types.GET_MARKETPLACE_TERLARIS_PRODUCTS_RESET]: getMarketplaceTerlarisProductsReset,

  /*-------------------GET MARKET PLACE PRODUCT DETAIL-----------------*/
  [Types.GET_MARKETPLACE_PRODUCT_DETAILS_REQUEST]: getMarketplaceProductDetailsRequest,
  [Types.GET_MARKETPLACE_PRODUCT_DETAILS_SUCCESS]: getMarketplaceProductDetailsSuccess,
  [Types.GET_MARKETPLACE_PRODUCT_DETAILS_FAILED]: getMarketplaceProductDetailsFailed,
  [Types.GET_MARKETPLACE_PRODUCT_DETAILS_RESET]: getMarketplaceProductDetailsReset,

  [Types.POST_PRODUCT_TO_OWN_TOKO_REQUEST]: postProductToOwnTokoRequest,
  [Types.POST_PRODUCT_TO_OWN_TOKO_SUCCESS]: postProductToOwnTokoSuccess,
  [Types.POST_PRODUCT_TO_OWN_TOKO_FAILED]: postProductToOwnTokoFailed,
  [Types.POST_PRODUCT_TO_OWN_TOKO_RESET]: postProductToOwnTokoReset,

  [Types.POST_ACTIVITY_LOG_REQUEST]: postActivityLogRequest,
  [Types.POST_ACTIVITY_LOG_SUCCESS]: postActivityLogSuccess,
  [Types.POST_ACTIVITY_LOG_FAILED]: postActivityLogFailed,
  [Types.POST_ACTIVITY_LOG_RESET]: postActivityLogReset,

  //CART
  [Types.GET_CART_DATA_REQUEST]: getCartDataRequest,
  [Types.GET_CART_DATA_SUCCESS]: getCartDataSuccess,
  [Types.GET_CART_DATA_FAILED]: getCartDataFailed,
  [Types.GET_CART_DATA_RESET]: getCartDataReset,

  [Types.GET_TUTORIAL_BANNER_DATA_REQUEST]: getTutorialBannerDataRequest,
  [Types.GET_TUTORIAL_BANNER_DATA_SUCCESS]: getTutorialBannerDataSuccess,
  [Types.GET_TUTORIAL_BANNER_DATA_FAILED]: getTutorialBannerDataFailed,
  [Types.GET_TUTORIAL_BANNER_DATA_RESET]: getTutorialBannerDataReset,

  [Types.POST_TO_CART_REQUEST]: postToCartRequest,
  [Types.POST_TO_CART_SUCCESS]: postToCartSuccess,
  [Types.POST_TO_CART_FAILED]: postToCartFailed,
  [Types.POST_TO_CART_RESET]: postToCartReset,

  [Types.UPDATE_CART_DATA_REQUEST]: updateCartDataRequest,
  [Types.UPDATE_CART_DATA_SUCCESS]: updateCartDataSuccess,
  [Types.UPDATE_CART_DATA_FAILED]: updateCartDataFailed,
  [Types.UPDATE_CART_DATA_RESET]: updateCartDataReset,

  [Types.DELETE_CART_DATA_REQUEST]: deleteCartDataRequest,
  [Types.DELETE_CART_DATA_SUCCESS]: deleteCartDataSuccess,
  [Types.DELETE_CART_DATA_FAILED]: deleteCartDataFailed,
  [Types.DELETE_CART_DATA_RESET]: deleteCartDataReset,
  /*-------------------GET MARKET PLACE PRODUCT LAST VIEW DATA-----------------*/
  [Types.GET_PRODUCT_LAST_VIEW_REQUEST]: getProductLastViewRequest,
  [Types.GET_PRODUCT_LAST_VIEW_SUCCESS]: getProductLastViewSuccess,
  [Types.GET_PRODUCT_LAST_VIEW_APPEND]: getProductLastViewAppend,
  [Types.GET_PRODUCT_LAST_VIEW_FAILED]: getProductLastViewFailed,
  [Types.GET_PRODUCT_LAST_VIEW_RESET]: getProductLastViewReset,
  /*-------------------GET MARKET PLACE PRODUCT BY SEARCH DATA-----------------*/
  [Types.GET_PRODUCT_BY_SEARCH_REQUEST]: getProductBySearchRequest,
  [Types.GET_PRODUCT_BY_SEARCH_SUCCESS]: getProductBySearchSuccess,
  [Types.GET_PRODUCT_BY_SEARCH_FAILED]: getProductBySearchFailed,
  [Types.GET_PRODUCT_BY_SEARCH_RESET]: getProductBySearchReset,

  [Types.GET_ALL_SHIPPING_FEE_REQUEST]: getAllShippingFeeRequest,
  [Types.GET_ALL_SHIPPING_FEE_SUCCESS]: getAllShippingFeeSuccess,
  [Types.GET_ALL_SHIPPING_FEE_FAILED]: getAllShippingFeeFailed,
  [Types.GET_ALL_SHIPPING_FEE_RESET]: getAllShippingFeeReset,

  /*-------------------POST CHECKOUT-----------------*/

  [Types.POST_CHECKOUT_REQUEST]: postCheckoutRequest,
  [Types.POST_CHECKOUT_SUCCESS]: postCheckoutSuccess,
  [Types.POST_CHECKOUT_FAILED]: postCheckoutFailed,
  [Types.POST_CHECKOUT_RESET]: postCheckoutReset,
  /*-------------------POST CHECKOUT-----------------*/

  [Types.POST_PAYMENT_ADMIN_FEE_REQUEST]: postPaymentAdminFeeRequest,
  [Types.POST_PAYMENT_ADMIN_FEE_SUCCESS]: postPaymentAdminFeeSuccess,
  [Types.POST_PAYMENT_ADMIN_FEE_FAILED]: postPaymentAdminFeeFailed,
  [Types.POST_PAYMENT_ADMIN_FEE_RESET]: postPaymentAdminFeeReset,
  [Types.SET_SHIPPING_FEE_DATA]: setShippingFeeData,
  [Types.SET_SHIPPING_DESTINATION_DATA]: setShippingDestinationData,
});
