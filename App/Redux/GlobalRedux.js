import {createActions, createReducer} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  saveFCMToken: ['data'],
  setModalVisible: ['data'],
  dueDateRefreshToken: ['data'],
  setLoginTime: ['data'],
  // ================================== TYPE POST TOKEN FOR REQUESTING API ==================================== //
  startup: ['screenName'],
  setTokenApiGloballyRequest: ['data'],
  setTokenApiGloballySuccess: ['data'],
  setTokenApiGloballyFailed: ['error'],
  setTokenApiGloballyReset: null,
  // ================================== TYPE POST TOKEN FOR REFRESHING API ==================================== //
  refreshTokenApiGloballyRequest: ['data'],
  refreshTokenApiGloballySuccess: ['data'],
  refreshTokenApiGloballyFailed: ['error'],
  refreshTokenApiGloballyReset: null,

  // ================================== TYPE POST FIREBASE TOKEN FOR PUSH NOTIFICATION ==================================== //
  saveFirebaseTokenRequest: ['data'],
  saveFirebaseTokenSuccess: ['data'],
  saveFirebaseTokenFailed: ['error'],
  saveFirebaseTokenReset: null,
});

export const GlobalTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  loginTimeData: null,
  fcmToken: null,
  refreshToken: {
    due_date: null,
  },

  modal: {
    data: {
      modalVisible: false,
      step: 0,
    },
  },
  tokenApi: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  firebaseToken: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
});

/* ------------- Reducers ------------- */
export const loginTime = (state, {data}) => {
  return state.setIn(['loginTimeData'], data);
};
export const dueDateRefreshToken = (state, {data}) => {
  return state.setIn(['refreshToken', 'due_date'], data);
};
export const setModalVisible = (state, {data}) => {
  return state.setIn(['modal', 'data'], data);
};
export const saveFCMToken = (state, {data}) => {
  return state.setIn(['fcmToken'], data);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKEN FOR REQUESTING API ------------------------------ //
export const tokenApiRequest = (state, {data}) => {
  return state
    .setIn(['tokenApi', 'fetching'], true)
    .setIn(['tokenApi', 'data'], data);
};
export const tokenApiSuccess = (state, {data}) => {
  return state
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'success'], true)
    .setIn(['tokenApi', 'data'], data);
};
export const tokenApiFailed = (state, {error}) => {
  return state
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'error'], error);
};
export const tokenApiReset = (state, {error}) => {
  return state
    .setIn(['tokenApi', 'data'], null)
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'error'], false)
    .setIn(['tokenApi', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKEN FOR REFRESHING API ------------------------------ //
export const refreshTokenApiRequest = (state, {data}) => {
  return state
    .setIn(['tokenApi', 'fetching'], true)
    .setIn(['tokenApi', 'data'], data);
};
export const refreshTokenApiSuccess = (state, {data}) => {
  return state
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'data'], data);
};
export const refreshTokenApiFailed = (state, {error}) => {
  return state
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'error'], error);
};
export const refreshTokenApiReset = (state, {error}) => {
  return state
    .setIn(['tokenApi', 'data'], null)
    .setIn(['tokenApi', 'fetching'], false)
    .setIn(['tokenApi', 'error'], false)
    .setIn(['tokenApi', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST FIREBASE TOKEN FOR PUSH NOTIFICATION ------------------------------ //
export const saveFirebaseTokenRequest = (state, {data}) => {
  return state.setIn(['firebaseToken', 'fetching'], true);
};
export const saveFirebaseTokenSuccess = (state, {data}) => {
  return state
    .setIn(['firebaseToken', 'fetching'], false)
    .setIn(['firebaseToken', 'data'], data);
};
export const saveFirebaseTokenFailed = (state, {error}) => {
  return state
    .setIn(['firebaseToken', 'fetching'], false)
    .setIn(['firebaseToken', 'error'], error);
};
export const saveFirebaseTokenReset = (state, {error}) => {
  return state
    .setIn(['firebaseToken', 'data'], null)
    .setIn(['firebaseToken', 'fetching'], false)
    .setIn(['firebaseToken', 'error'], false)
    .setIn(['firebaseToken', 'success'], false);
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_LOGIN_TIME]: loginTime,
  [Types.SAVE_FCM_TOKEN]: saveFCMToken,
  [Types.SET_MODAL_VISIBLE]: setModalVisible,
  [Types.DUE_DATE_REFRESH_TOKEN]: dueDateRefreshToken,
  /*-------------------POST TOKEN FOR REQUESTING API-----------------*/
  [Types.SET_TOKEN_API_GLOBALLY_REQUEST]: tokenApiRequest,
  [Types.SET_TOKEN_API_GLOBALLY_SUCCESS]: tokenApiSuccess,
  [Types.SET_TOKEN_API_GLOBALLY_FAILED]: tokenApiFailed,
  [Types.SET_TOKEN_API_GLOBALLY_RESET]: tokenApiReset,
  /*-------------------POST TOKEN FOR REFRESHING API-----------------*/
  [Types.REFRESH_TOKEN_API_GLOBALLY_REQUEST]: refreshTokenApiRequest,
  [Types.REFRESH_TOKEN_API_GLOBALLY_SUCCESS]: refreshTokenApiSuccess,
  [Types.REFRESH_TOKEN_API_GLOBALLY_FAILED]: refreshTokenApiFailed,
  [Types.REFRESH_TOKEN_API_GLOBALLY_RESET]: refreshTokenApiReset,
  /*-------------------POST TOKEN FOR PUSH NOTIFICATION-----------------*/
  [Types.SAVE_FIREBASE_TOKEN_REQUEST]: saveFirebaseTokenRequest,
  [Types.SAVE_FIREBASE_TOKEN_SUCCESS]: saveFirebaseTokenSuccess,
  [Types.SAVE_FIREBASE_TOKEN_FAILED]: saveFirebaseTokenFailed,
  [Types.SAVE_FIREBASE_TOKEN_RESET]: saveFirebaseTokenReset,
  [Types.STARTUP]: null,
});

/* ------------- Selectors ------------- */

export const getRole = state => state.app.role;
export const getToken = state => state.app.tokenApi.data.access_token;
export const getProfileID = state => state.app.userProfile.data.id;
