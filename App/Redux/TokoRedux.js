import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  // ================================== TYPE POST WITHDRAW DISBURSMENT DATA ==================================== //
  postWithdrawDisbursmentTokoRequest: ['data'],
  postWithdrawDisbursmentTokoSuccess: ['data'],
  postWithdrawDisbursmentTokoFailed: ['error'],
  postWithdrawDisbursmentTokoReset: null,
  // ================================== TYPE GET WITHDRAW HISTORY TOKO DATA ==================================== //
  getWithdrawHistoryTokoRequest: ['data'],
  getWithdrawHistoryTokoSuccess: ['data'],
  getWithdrawHistoryTokoFailed: ['error'],
  getWithdrawHistoryTokoReset: null,
  // ================================== TYPE POST KWC DATA ==================================== //
  postKycTokoRequest: ['data'],
  postKycTokoSuccess: ['data'],
  postKycTokoFailed: ['error'],
  postKycTokoReset: null,

  // ================================== TYPE GET KWC STATUS TOKO DATA ==================================== //
  getKycStatusTokoRequest: ['data'],
  getKycStatusTokoSuccess: ['data'],
  getKycStatusTokoFailed: ['error'],
  getKycStatusTokoReset: null,

  // ================================== TYPE HANDLING ACTIVED N DEACTIVED OWNER TOKO COURIER DATA ==================================== //
  handlingOwnerTokoCourierStatusRequest: ['data'],
  handlingOwnerTokoCourierStatusSuccess: ['data'],
  handlingOwnerTokoCourierStatusFailed: ['error'],
  handlingOwnerTokoCourierStatusReset: null,

  // ================================== TYPE GET OWNER TOKO COURIER DATA ==================================== //
  getOwnerTokoCourierRequest: ['data'],
  getOwnerTokoCourierSuccess: ['data'],
  getOwnerTokoCourierFailed: ['error'],
  getOwnerTokoCourierReset: null,

  // ================================== TYPE GET TOKO FINANCIAL DATA ==================================== //
  getTokoFinancialRequest: ['data'],
  getTokoFinancialSuccess: ['data'],
  getTokoFinancialFailed: ['error'],
  getTokoFinancialReset: null,

  // ================================== TYPE UPDATE DEFAULT BANK REKENING  TOKO DATA ==================================== //
  updateDefaultRekeningTokoRequest: ['data'],
  updateDefaultRekeningTokoSuccess: ['data'],
  updateDefaultRekeningTokoFailed: ['error'],
  updateDefaultRekeningTokoReset: null,
  // ================================== TYPE POST SEND ORDER DATA ==================================== //
  postTokoFinishOrderRequest: ['data'],
  postTokoFinishOrderSuccess: ['data'],
  postTokoFinishOrderFailed: ['error'],
  postTokoFinishOrderReset: null,

  // ================================== TYPE POST SEND ORDER DATA ==================================== //
  postTokoSendOrderRequest: ['data'],
  postTokoSendOrderSuccess: ['data'],
  postTokoSendOrderFailed: ['error'],
  postTokoSendOrderReset: null,

  // ================================== TYPE POST ACCEPT PAYMENT DATA ==================================== //
  postTokoAcceptPaymentRequest: ['data'],
  postTokoAcceptPaymentSuccess: ['data'],
  postTokoAcceptPaymentFailed: ['error'],
  postTokoAcceptPaymentReset: null,

  // ================================== TYPE POST ORDER PROCEED DATA ==================================== //
  postTokoOrderProceedRequest: ['data'],
  postTokoOrderProceedSuccess: ['data'],
  postTokoOrderProceedFailed: ['error'],
  postTokoOrderProceedReset: null,

  // ================================== TYPE GET TOKO COURIER DATA ==================================== //
  getTokoCourierRequest: ['data'],
  getTokoCourierSuccess: ['data'],
  getTokoCourierFailed: ['error'],
  getTokoCourierReset: null,

  // ================================== TYPE GET TOKO ORDER DETAIL DATA ==================================== //
  getTokoOrderDetailRequest: ['data'],
  getTokoOrderDetailSuccess: ['data'],
  getTokoOrderDetailFailed: ['error'],
  getTokoOrderDetailReset: null,

  // ================================== TYPE GET TOKO ORDER DATA ==================================== //
  getTokoOrderRequest: ['data'],
  getTokoOrderSuccess: ['data'],
  getTokoOrderFailed: ['error'],
  getTokoOrderReset: null,

  // ================================== TYPE GET BANK  DATA ==================================== //
  getBankRequest: ['data'],
  getBankSuccess: ['data'],
  getBankFailed: ['error'],
  getBankReset: null,

  // ================================== TYPE GET TOKO BANK DATA ==================================== //
  getTokoBankRequest: ['data'],
  getTokoBankSuccess: ['data'],
  getTokoBankFailed: ['error'],
  getTokoBankReset: null,
  // ================================== TYPE POST TOKO BANK DATA ==================================== //
  postTokoBankRequest: ['data'],
  postTokoBankSuccess: ['data'],
  postTokoBankFailed: ['error'],
  postTokoBankReset: null,

  // ================================== TYPE GET TOKO ADDRESS DATA ==================================== //
  getTokoAddressRequest: ['data'],
  getTokoAddressSuccess: ['data'],
  getTokoAddressFailed: ['error'],
  getTokoAddressReset: null,
  // ================================== TYPE POST TOKO ADDRESS DATA ==================================== //
  postTokoAddressRequest: ['data'],
  postTokoAddressSuccess: ['data'],
  postTokoAddressFailed: ['error'],
  postTokoAddressReset: null,
  // ================================== TYPE GET PROVINCE DATA ==================================== //
  getProvinceRequest: ['data'],
  getProvinceSuccess: ['data'],
  getProvinceFailed: ['error'],
  getProvinceReset: null,
  // ================================== TYPE GET CITY DATA ==================================== //
  getCityRequest: ['data'],
  getCitySuccess: ['data'],
  getCityFailed: ['error'],
  getCityReset: null,
  // ================================== TYPE GET DISTRICT DATA ==================================== //
  getDistrictRequest: ['data'],
  getDistrictSuccess: ['data'],
  getDistrictFailed: ['error'],
  getDistrictReset: null,

  // ================================== TYPE GET POSTAL CODE DATA ==================================== //
  getPostalCodeRequest: ['data'],
  getPostalCodeSuccess: ['data'],
  getPostalCodeFailed: ['error'],
  getPostalCodeReset: null,

  // ================================== TYPE DELETE TOKO DATA ==================================== //
  deleteTokoRequest: ['data'],
  deleteTokoSuccess: ['data'],
  deleteTokoFailed: ['error'],
  deleteTokoReset: null,
  // ================================== TYPE GET SUPPLIER DATA ==================================== //
  getSupplierRequest: ['data'],
  getSupplierSuccess: ['data'],
  getSupplierFailed: ['error'],
  getSupplierReset: null,
  // ================================== TYPE GET TOKO DATA ==================================== //
  getTokoRequest: ['data'],
  getTokoSuccess: ['data'],
  getTokoFailed: ['error'],
  getTokoReset: null,
  // ================================== TYPE GET DETAIL TOKO DATA ==================================== //
  getDetailTokoRequest: ['data'],
  getDetailTokoSuccess: ['data'],
  getDetailTokoFailed: ['error'],
  getDetailTokoReset: null,
  // ================================== TYPE GET DETAIL TOKO DATA ==================================== //
  getDetailProductRequest: ['data'],
  getDetailProductSuccess: ['data'],
  getDetailProductFailed: ['error'],
  getDetailProductReset: null,
  // ================================== TYPE GET CATEGORY TOKO DATA ==================================== //
  getCategoryTokoRequest: ['data'],
  getCategoryTokoSuccess: ['data'],
  getCategoryTokoFailed: ['error'],
  getCategoryTokoReset: null,

  // ================================== TYPE POST CATEGORY TOKO DATA ==================================== //
  postCategoryTokoRequest: ['data'],
  postCategoryTokoSuccess: ['data'],
  postCategoryTokoFailed: ['error'],
  postCategoryTokoReset: null,
  // ================================== TYPE UPDATE CATEGORY TOKO DATA ==================================== //
  updateCategoryTokoRequest: ['data'],
  updateCategoryTokoSuccess: ['data'],
  updateCategoryTokoFailed: ['error'],
  updateCategoryTokoReset: null,
  // ================================== TYPE UPDATE POSITION CATEGORY TOKO DATA ==================================== //
  updatePositionCategoryTokoRequest: ['data'],
  updatePositionCategoryTokoSuccess: ['data'],
  updatePositionCategoryTokoFailed: ['error'],
  updatePositionCategoryTokoReset: null,
  // ================================== TYPE DELETE CATEGORY DATA ==================================== //
  deleteCategoryTokoRequest: ['data'],
  deleteCategoryTokoSuccess: ['data'],
  deleteCategoryTokoFailed: ['error'],
  deleteCategoryTokoReset: null,
  // ================================== TYPE GET PRODUCT TOKO DATA ==================================== //
  getProductTokoRequest: ['data'],
  getProductTokoSuccess: ['data'],
  getProductTokoFailed: ['error'],
  getProductTokoReset: null,
  // ================================== TYPE POST PRODUCT TOKO DATA ==================================== //
  postProductTokoRequest: ['data'],
  postProductTokoSuccess: ['data'],
  postProductTokoFailed: ['error'],
  postProductTokoReset: null,
  // ================================== TYPE UPDATE PRODUCT TOKO DATA ==================================== //
  updateProductTokoRequest: ['data'],
  updateProductTokoSuccess: ['data'],
  updateProductTokoFailed: ['error'],
  updateProductTokoReset: null,
  // ================================== TYPE MINI UPDATE PRODUCT TOKO DATA ==================================== //
  miniUpdateProductTokoRequest: ['data'],
  miniUpdateProductTokoSuccess: ['data'],
  miniUpdateProductTokoFailed: ['error'],
  miniUpdateProductTokoReset: null,
  // ================================== TYPE DELETE PRODUCT DATA ==================================== //
  deleteProductTokoRequest: ['data'],
  deleteProductTokoSuccess: ['data'],
  deleteProductTokoFailed: ['error'],
  deleteProductTokoReset: null,
  // ================================== TYPE POST TOKO DATA ==================================== //
  tokoPostRequest: ['data'],
  tokoPostSuccess: ['data'],
  tokoPostFailed: ['error'],
  tokoPostReset: null,

  // ================================== TYPE UPDATE TOKO DATA ==================================== //
  tokoUpdateRequest: ['data'],
  tokoUpdateSuccess: ['data'],
  tokoUpdateFailed: ['error'],
  tokoUpdateReset: null,

  // ================================== TYPE GET BUSINESS TYPE DATA ==================================== //
  getBusinessTypeRequest: ['data'],
  getBusinessTypeSuccess: ['data'],
  getBusinessTypeFailed: ['error'],
  getBusinessTypeReset: null,
  // ================================== TYPE UPLOAD IMAGE ==================================== //
  uploadProductImageRequest: ['data'],
  uploadProductImageSuccess: ['data'],
  uploadProductImageFailed: ['error'],
  uploadProductImageReset: null,

  // ================================== TYPE RESET TOKO STATE ==================================== //
  tokoReset: null,
  getTokoUnavailableFailed: null,
});

export const TokoTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  postWithdrawDisbursmentTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  getWithdrawHistoryTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postKycTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  getKycStatusTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },

  handlingOwnerTokoCourierStatusData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  ownerTokoCourierData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoFinancialData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  updateDefaultRekeningTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoFinishOrderData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoSendOrderData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoAcceptPaymentData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoOrderProceedData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoCourierData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoOrderDetailData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoOrderData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  bankData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoBankData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoBankData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoAddressData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postTokoAddressData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  provinceData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  cityData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  districtData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postalCodeData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoDeletedData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
  categoryTokoData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  postCategoryTokoData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  categoryTokoUpdatedData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  categoryTokoUpdatedPositionData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  categoryTokoDeletedData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  productTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  postProductTokoData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  updateProductTokoData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  productTokoDeletedData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  supplierData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoData: {
    data: [],
    fetching: false,
    error: false,
    success: false,
  },
  detailTokoData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  detailProductData: {
    data: null,
    fetching: false,
    error: false,
    success: false,
  },
  tokoPostData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },

  tokoUpdateData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },

  getBusinessTypeData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
  uploadProductImageData: {
    data: {},
    fetching: false,
    error: false,
    success: false,
  },
});

/* ------------- Selectors ------------- */

export const tokoSelectors = {
  getData: state => state.data,
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST WITHDRAW DISBURSMENT DATA ------------------------------ //
export const postWithdrawDisbursmentTokoRequest = (state, {data}) => {
  return state.setIn(['postWithdrawDisbursmentTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postWithdrawDisbursmentTokoSuccess = (state, {data}) => {
  return state
    .setIn(['postWithdrawDisbursmentTokoData', 'fetching'], false)
    .setIn(['postWithdrawDisbursmentTokoData', 'data'], data)
    .setIn(['postWithdrawDisbursmentTokoData', 'error'], false)
    .setIn(['postWithdrawDisbursmentTokoData', 'success'], true);
};
export const postWithdrawDisbursmentTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postWithdrawDisbursmentTokoData', 'fetching'], false)
      .setIn(['postWithdrawDisbursmentTokoData', 'error'], error)
      .setIn(['postWithdrawDisbursmentTokoData', 'success'], false)
  );
};
export const postWithdrawDisbursmentTokoReset = (state, {error}) => {
  return state
    .setIn(['postWithdrawDisbursmentTokoData', 'data'], null)
    .setIn(['postWithdrawDisbursmentTokoData', 'fetching'], false)
    .setIn(['postWithdrawDisbursmentTokoData', 'error'], false)
    .setIn(['postWithdrawDisbursmentTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS WITHDRAW HISTORY TOKO DATA ------------------------------ //
export const getWithdrawHistoryTokoRequest = (state, {data}) => {
  return state.setIn(['getWithdrawHistoryTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getWithdrawHistoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['getWithdrawHistoryTokoData', 'fetching'], false)
    .setIn(['getWithdrawHistoryTokoData', 'data'], data)
    .setIn(['getWithdrawHistoryTokoData', 'error'], false)
    .setIn(['getWithdrawHistoryTokoData', 'success'], true);
};
export const getWithdrawHistoryTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['getWithdrawHistoryTokoData', 'fetching'], false)
      .setIn(['getWithdrawHistoryTokoData', 'error'], error)
      .setIn(['getWithdrawHistoryTokoData', 'success'], false)
  );
};
export const getWithdrawHistoryTokoReset = (state, {error}) => {
  return state
    .setIn(['getWithdrawHistoryTokoData', 'data'], null)
    .setIn(['getWithdrawHistoryTokoData', 'fetching'], false)
    .setIn(['getWithdrawHistoryTokoData', 'error'], false)
    .setIn(['getWithdrawHistoryTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST KWC DATA ------------------------------ //
export const postKycTokoRequest = (state, {data}) => {
  return state.setIn(['postKycTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postKycTokoSuccess = (state, {data}) => {
  return state
    .setIn(['postKycTokoData', 'fetching'], false)
    .setIn(['postKycTokoData', 'data'], data)
    .setIn(['postKycTokoData', 'error'], false)
    .setIn(['postKycTokoData', 'success'], true);
};
export const postKycTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postKycTokoData', 'fetching'], false)
      .setIn(['postKycTokoData', 'error'], error)
      .setIn(['postKycTokoData', 'success'], false)
  );
};
export const postKycTokoReset = (state, {error}) => {
  return state
    .setIn(['postKycTokoData', 'data'], [])
    .setIn(['postKycTokoData', 'fetching'], false)
    .setIn(['postKycTokoData', 'error'], false)
    .setIn(['postKycTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS KWC STATUS TOKO DATA ------------------------------ //
export const getKycStatusTokoRequest = (state, {data}) => {
  return state.setIn(['getKycStatusTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getKycStatusTokoSuccess = (state, {data}) => {
  return state
    .setIn(['getKycStatusTokoData', 'fetching'], false)
    .setIn(['getKycStatusTokoData', 'data'], data)
    .setIn(['getKycStatusTokoData', 'error'], false)
    .setIn(['getKycStatusTokoData', 'success'], true);
};
export const getKycStatusTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['getKycStatusTokoData', 'fetching'], false)
      .setIn(['getKycStatusTokoData', 'error'], error)
      .setIn(['getKycStatusTokoData', 'success'], false)
  );
};
export const getKycStatusTokoReset = (state, {error}) => {
  return state
    .setIn(['getKycStatusTokoData', 'data'], null)
    .setIn(['getKycStatusTokoData', 'fetching'], false)
    .setIn(['getKycStatusTokoData', 'error'], false)
    .setIn(['getKycStatusTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS HANDLING OWNER TOKO COURIER STATUS OWNER TOKO COURIER DATA ------------------------------ //
export const handlingOwnerTokoCourierStatusRequest = (state, {data}) => {
  return state.setIn(['handlingOwnerTokoCourierStatusData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const handlingOwnerTokoCourierStatusSuccess = (state, {data}) => {
  return state
    .setIn(['handlingOwnerTokoCourierStatusData', 'fetching'], false)
    .setIn(['handlingOwnerTokoCourierStatusData', 'data'], data)
    .setIn(['handlingOwnerTokoCourierStatusData', 'error'], false)
    .setIn(['handlingOwnerTokoCourierStatusData', 'success'], true);
};
export const handlingOwnerTokoCourierStatusFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['handlingOwnerTokoCourierStatusData', 'fetching'], false)
      .setIn(['handlingOwnerTokoCourierStatusData', 'error'], error)
      .setIn(['handlingOwnerTokoCourierStatusData', 'success'], false)
  );
};
export const handlingOwnerTokoCourierStatusReset = (state, {error}) => {
  return state
    .setIn(['handlingOwnerTokoCourierStatusData', 'data'], null)
    .setIn(['handlingOwnerTokoCourierStatusData', 'fetching'], false)
    .setIn(['handlingOwnerTokoCourierStatusData', 'error'], false)
    .setIn(['handlingOwnerTokoCourierStatusData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET OWNER TOKO COURIER DATA ------------------------------ //
export const getOwnerTokoCourierRequest = (state, {data}) => {
  return state.setIn(['ownerTokoCourierData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getOwnerTokoCourierSuccess = (state, {data}) => {
  return state
    .setIn(['ownerTokoCourierData', 'fetching'], false)
    .setIn(['ownerTokoCourierData', 'data'], data)
    .setIn(['ownerTokoCourierData', 'error'], false)
    .setIn(['ownerTokoCourierData', 'success'], true);
};
export const getOwnerTokoCourierFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['ownerTokoCourierData', 'fetching'], false)
      .setIn(['ownerTokoCourierData', 'error'], error)
      .setIn(['ownerTokoCourierData', 'success'], false)
  );
};
export const getOwnerTokoCourierReset = (state, {error}) => {
  return state
    .setIn(['ownerTokoCourierData', 'data'], null)
    .setIn(['ownerTokoCourierData', 'fetching'], false)
    .setIn(['ownerTokoCourierData', 'error'], false)
    .setIn(['ownerTokoCourierData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET FINANCIAL DATA ------------------------------ //
export const getTokoFinancialRequest = (state, {data}) => {
  return state.setIn(['tokoFinancialData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoFinancialSuccess = (state, {data}) => {
  return state
    .setIn(['tokoFinancialData', 'fetching'], false)
    .setIn(['tokoFinancialData', 'data'], data)
    .setIn(['tokoFinancialData', 'error'], false)
    .setIn(['tokoFinancialData', 'success'], true);
};
export const getTokoFinancialFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoFinancialData', 'fetching'], false)
      .setIn(['tokoFinancialData', 'error'], error)
      .setIn(['tokoFinancialData', 'success'], false)
  );
};
export const getTokoFinancialReset = (state, {error}) => {
  return state
    .setIn(['tokoFinancialData', 'data'], null)
    .setIn(['tokoFinancialData', 'fetching'], false)
    .setIn(['tokoFinancialData', 'error'], false)
    .setIn(['tokoFinancialData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS DEFAULT REKENING TOKO DATA ------------------------------ //
export const updateDefaultRekeningTokoRequest = (state, {data}) => {
  return state.setIn(['updateDefaultRekeningTokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const updateDefaultRekeningTokoSuccess = (state, {data}) => {
  return state
    .setIn(['updateDefaultRekeningTokoData', 'fetching'], false)
    .setIn(['updateDefaultRekeningTokoData', 'data'], data)
    .setIn(['updateDefaultRekeningTokoData', 'error'], false)
    .setIn(['updateDefaultRekeningTokoData', 'success'], true);
};
export const updateDefaultRekeningTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['updateDefaultRekeningTokoData', 'fetching'], false)
      .setIn(['updateDefaultRekeningTokoData', 'error'], error)
      .setIn(['updateDefaultRekeningTokoData', 'success'], false)
  );
};
export const updateDefaultRekeningTokoReset = (state, {error}) => {
  return state
    .setIn(['updateDefaultRekeningTokoData', 'data'], [])
    .setIn(['updateDefaultRekeningTokoData', 'fetching'], false)
    .setIn(['updateDefaultRekeningTokoData', 'error'], false)
    .setIn(['updateDefaultRekeningTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST  FINISH ORDER DATA ------------------------------ //
export const postTokoFinishOrderRequest = (state, {data}) => {
  return state.setIn(['postTokoFinishOrderData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoFinishOrderSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoFinishOrderData', 'fetching'], false)
    .setIn(['postTokoFinishOrderData', 'data'], data)
    .setIn(['postTokoFinishOrderData', 'error'], false)
    .setIn(['postTokoFinishOrderData', 'success'], true);
};
export const postTokoFinishOrderFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoFinishOrderData', 'fetching'], false)
      .setIn(['postTokoFinishOrderData', 'error'], error)
      .setIn(['postTokoFinishOrderData', 'success'], false)
  );
};
export const postTokoFinishOrderReset = (state, {error}) => {
  return state
    .setIn(['postTokoFinishOrderData', 'data'], [])
    .setIn(['postTokoFinishOrderData', 'fetching'], false)
    .setIn(['postTokoFinishOrderData', 'error'], false)
    .setIn(['postTokoFinishOrderData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST  SEND ORDER DATA ------------------------------ //
export const postTokoSendOrderRequest = (state, {data}) => {
  return state.setIn(['postTokoSendOrderData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoSendOrderSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoSendOrderData', 'fetching'], false)
    .setIn(['postTokoSendOrderData', 'data'], data)
    .setIn(['postTokoSendOrderData', 'error'], false)
    .setIn(['postTokoSendOrderData', 'success'], true);
};
export const postTokoSendOrderFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoSendOrderData', 'fetching'], false)
      .setIn(['postTokoSendOrderData', 'error'], error)
      .setIn(['postTokoSendOrderData', 'success'], false)
  );
};
export const postTokoSendOrderReset = (state, {error}) => {
  return state
    .setIn(['postTokoSendOrderData', 'data'], [])
    .setIn(['postTokoSendOrderData', 'fetching'], false)
    .setIn(['postTokoSendOrderData', 'error'], false)
    .setIn(['postTokoSendOrderData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST  ACCEPT  PAYMENT DATA ------------------------------ //
export const postTokoAcceptPaymentRequest = (state, {data}) => {
  return state.setIn(['postTokoAcceptPaymentData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoAcceptPaymentSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoAcceptPaymentData', 'fetching'], false)
    .setIn(['postTokoAcceptPaymentData', 'data'], data)
    .setIn(['postTokoAcceptPaymentData', 'error'], false)
    .setIn(['postTokoAcceptPaymentData', 'success'], true);
};
export const postTokoAcceptPaymentFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoAcceptPaymentData', 'fetching'], false)
      .setIn(['postTokoAcceptPaymentData', 'error'], error)
      .setIn(['postTokoAcceptPaymentData', 'success'], false)
  );
};
export const postTokoAcceptPaymentReset = (state, {error}) => {
  return state
    .setIn(['postTokoAcceptPaymentData', 'data'], [])
    .setIn(['postTokoAcceptPaymentData', 'fetching'], false)
    .setIn(['postTokoAcceptPaymentData', 'error'], false)
    .setIn(['postTokoAcceptPaymentData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKO ORDER PROCEED DATA ------------------------------ //
export const postTokoOrderProceedRequest = (state, {data}) => {
  return state.setIn(['postTokoOrderProceedData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoOrderProceedSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoOrderProceedData', 'fetching'], false)
    .setIn(['postTokoOrderProceedData', 'data'], data)
    .setIn(['postTokoOrderProceedData', 'error'], false)
    .setIn(['postTokoOrderProceedData', 'success'], true);
};
export const postTokoOrderProceedFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoOrderProceedData', 'fetching'], false)
      .setIn(['postTokoOrderProceedData', 'error'], error)
      .setIn(['postTokoOrderProceedData', 'success'], false)
  );
};
export const postTokoOrderProceedReset = (state, {error}) => {
  return state
    .setIn(['postTokoOrderProceedData', 'data'], [])
    .setIn(['postTokoOrderProceedData', 'fetching'], false)
    .setIn(['postTokoOrderProceedData', 'error'], false)
    .setIn(['postTokoOrderProceedData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET COURIER DATA ------------------------------ //
export const getTokoCourierRequest = (state, {data}) => {
  return state.setIn(['tokoCourierData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoCourierSuccess = (state, {data}) => {
  return state
    .setIn(['tokoCourierData', 'fetching'], false)
    .setIn(['tokoCourierData', 'data'], data)
    .setIn(['tokoCourierData', 'error'], false)
    .setIn(['tokoCourierData', 'success'], true);
};
export const getTokoCourierFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoCourierData', 'fetching'], false)
      .setIn(['tokoCourierData', 'error'], error)
      .setIn(['tokoCourierData', 'success'], false)
  );
};
export const getTokoCourierReset = (state, {error}) => {
  return state
    .setIn(['tokoCourierData', 'data'], null)
    .setIn(['tokoCourierData', 'fetching'], false)
    .setIn(['tokoCourierData', 'error'], false)
    .setIn(['tokoCourierData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET TOKO ORDER DETAIL DATA ------------------------------ //
export const getTokoOrderDetailRequest = (state, {data}) => {
  return state.setIn(['tokoOrderDetailData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoOrderDetailSuccess = (state, {data}) => {
  return state
    .setIn(['tokoOrderDetailData', 'fetching'], false)
    .setIn(['tokoOrderDetailData', 'data'], data)
    .setIn(['tokoOrderDetailData', 'error'], false)
    .setIn(['tokoOrderDetailData', 'success'], true);
};
export const getTokoOrderDetailFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoOrderDetailData', 'fetching'], false)
      .setIn(['tokoOrderDetailData', 'error'], error)
      .setIn(['tokoOrderDetailData', 'success'], false)
  );
};
export const getTokoOrderDetailReset = (state, {error}) => {
  return state
    .setIn(['tokoOrderDetailData', 'data'], null)
    .setIn(['tokoOrderDetailData', 'fetching'], false)
    .setIn(['tokoOrderDetailData', 'error'], false)
    .setIn(['tokoOrderDetailData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET TOKO ORDER DATA ------------------------------ //
export const getTokoOrderRequest = (state, {data}) => {
  return state
    .setIn(['tokoOrderData', 'fetching'], true)
    .setIn(['tokoOrderData', 'error'], '')
    .setIn(['tokoOrderData', 'success'], false);
};
export const getTokoOrderSuccess = (state, {data}) => {
  return state
    .setIn(['tokoOrderData', 'fetching'], false)
    .setIn(['tokoOrderData', 'data'], data)
    .setIn(['tokoOrderData', 'error'], false)
    .setIn(['tokoOrderData', 'success'], true);
};
export const getTokoOrderFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoOrderData', 'fetching'], false)
      .setIn(['tokoOrderData', 'error'], error)
      .setIn(['tokoOrderData', 'success'], false)
  );
};
export const getTokoOrderReset = (state, {error}) => {
  return state
    .setIn(['tokoOrderData', 'data'], null)
    .setIn(['tokoOrderData', 'fetching'], false)
    .setIn(['tokoOrderData', 'error'], false)
    .setIn(['tokoOrderData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET BANK DATA ------------------------------ //
export const getBankRequest = (state, {data}) => {
  return state.setIn(['bankData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getBankSuccess = (state, {data}) => {
  return state
    .setIn(['bankData', 'fetching'], false)
    .setIn(['bankData', 'data'], data)
    .setIn(['bankData', 'error'], false)
    .setIn(['bankData', 'success'], true);
};
export const getBankFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['bankData', 'fetching'], false)
      .setIn(['bankData', 'error'], error)
      .setIn(['bankData', 'success'], false)
  );
};
export const getBankReset = (state, {error}) => {
  return state
    .setIn(['bankData', 'data'], null)
    .setIn(['bankData', 'fetching'], false)
    .setIn(['bankData', 'error'], false)
    .setIn(['bankData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET TOKO BANK DATA ------------------------------ //
export const getTokoBankRequest = (state, {data}) => {
  return state.setIn(['tokoBankData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoBankSuccess = (state, {data}) => {
  return state
    .setIn(['tokoBankData', 'fetching'], false)
    .setIn(['tokoBankData', 'data'], data)
    .setIn(['tokoBankData', 'error'], false)
    .setIn(['tokoBankData', 'success'], true);
};
export const getTokoBankFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoBankData', 'fetching'], false)
      .setIn(['tokoBankData', 'error'], error)
      .setIn(['tokoBankData', 'success'], false)
  );
};
export const getTokoBankReset = (state, {error}) => {
  return state
    .setIn(['tokoBankData', 'data'], null)
    .setIn(['tokoBankData', 'fetching'], false)
    .setIn(['tokoBankData', 'error'], false)
    .setIn(['tokoBankData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKO BANK DATA ------------------------------ //
export const postTokoBankRequest = (state, {data}) => {
  return state.setIn(['postTokoBankData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoBankSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoBankData', 'fetching'], false)
    .setIn(['postTokoBankData', 'data'], data)
    .setIn(['postTokoBankData', 'error'], false)
    .setIn(['postTokoBankData', 'success'], true);
};
export const postTokoBankFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoBankData', 'fetching'], false)
      .setIn(['postTokoBankData', 'error'], error)
      .setIn(['postTokoBankData', 'success'], false)
  );
};
export const postTokoBankReset = (state, {error}) => {
  return state
    .setIn(['postTokoBankData', 'data'], [])
    .setIn(['postTokoBankData', 'fetching'], false)
    .setIn(['postTokoBankData', 'error'], false)
    .setIn(['postTokoBankData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET TOKO ADDRESS DATA ------------------------------ //
export const getTokoAddressRequest = (state, {data}) => {
  return state.setIn(['tokoAddressData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoAddressSuccess = (state, {data}) => {
  return state
    .setIn(['tokoAddressData', 'fetching'], false)
    .setIn(['tokoAddressData', 'data'], data)
    .setIn(['tokoAddressData', 'error'], false)
    .setIn(['tokoAddressData', 'success'], true);
};
export const getTokoAddressFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoAddressData', 'fetching'], false)
      .setIn(['tokoAddressData', 'error'], error)
      .setIn(['tokoAddressData', 'success'], false)
  );
};
export const getTokoAddressReset = (state, {error}) => {
  return state
    .setIn(['tokoAddressData', 'data'], null)
    .setIn(['tokoAddressData', 'fetching'], false)
    .setIn(['tokoAddressData', 'error'], false)
    .setIn(['tokoAddressData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKO ADDRESS DATA ------------------------------ //
export const postTokoAddressRequest = (state, {data}) => {
  return state.setIn(['postTokoAddressData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const postTokoAddressSuccess = (state, {data}) => {
  return state
    .setIn(['postTokoAddressData', 'fetching'], false)
    .setIn(['postTokoAddressData', 'data'], data)
    .setIn(['postTokoAddressData', 'error'], false)
    .setIn(['postTokoAddressData', 'success'], true);
};
export const postTokoAddressFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postTokoAddressData', 'fetching'], false)
      .setIn(['postTokoAddressData', 'error'], error)
      .setIn(['postTokoAddressData', 'success'], false)
  );
};
export const postTokoAddressReset = (state, {error}) => {
  return state
    .setIn(['postTokoAddressData', 'data'], [])
    .setIn(['postTokoAddressData', 'fetching'], false)
    .setIn(['postTokoAddressData', 'error'], false)
    .setIn(['postTokoAddressData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET PROVINCE DATA ------------------------------ //
export const getProvinceRequest = (state, {data}) => {
  return state.setIn(['provinceData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getProvinceSuccess = (state, {data}) => {
  return state
    .setIn(['provinceData', 'fetching'], false)
    .setIn(['provinceData', 'data'], data)
    .setIn(['provinceData', 'error'], false)
    .setIn(['provinceData', 'success'], true);
};
export const getProvinceFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['provinceData', 'fetching'], false)
      .setIn(['provinceData', 'error'], error)
      .setIn(['provinceData', 'success'], false)
  );
};
export const getProvinceReset = (state, {error}) => {
  return state
    .setIn(['provinceData', 'data'], [])
    .setIn(['provinceData', 'fetching'], false)
    .setIn(['provinceData', 'error'], false)
    .setIn(['provinceData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET CITY DATA ------------------------------ //
export const getCityRequest = (state, {data}) => {
  return state.setIn(['cityData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getCitySuccess = (state, {data}) => {
  return state
    .setIn(['cityData', 'fetching'], false)
    .setIn(['cityData', 'data'], data)
    .setIn(['cityData', 'error'], false)
    .setIn(['cityData', 'success'], true);
};
export const getCityFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['cityData', 'fetching'], false)
      .setIn(['cityData', 'error'], error)
      .setIn(['cityData', 'success'], false)
  );
};
export const getCityReset = (state, {error}) => {
  return state
    .setIn(['cityData', 'data'], null)
    .setIn(['cityData', 'fetching'], false)
    .setIn(['cityData', 'error'], false)
    .setIn(['cityData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET DISTRICT DATA ------------------------------ //
export const getDistrictRequest = (state, {data}) => {
  return state.setIn(['districtData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getDistrictSuccess = (state, {data}) => {
  return state
    .setIn(['districtData', 'fetching'], false)
    .setIn(['districtData', 'data'], data)
    .setIn(['districtData', 'error'], false)
    .setIn(['districtData', 'success'], true);
};
export const getDistrictFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['districtData', 'fetching'], false)
      .setIn(['districtData', 'error'], error)
      .setIn(['districtData', 'success'], false)
  );
};
export const getDistrictReset = (state, {error}) => {
  return state
    .setIn(['districtData', 'data'], null)
    .setIn(['districtData', 'fetching'], false)
    .setIn(['districtData', 'error'], false)
    .setIn(['districtData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET POSTAL CODE DATA ------------------------------ //
export const getPostalCodeRequest = (state, {data}) => {
  return state.setIn(['postalCodeData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getPostalCodeSuccess = (state, {data}) => {
  return state
    .setIn(['postalCodeData', 'fetching'], false)
    .setIn(['postalCodeData', 'data'], data)
    .setIn(['postalCodeData', 'error'], false)
    .setIn(['postalCodeData', 'success'], true);
};
export const getPostalCodeFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['postalCodeData', 'fetching'], false)
      .setIn(['postalCodeData', 'error'], error)
      .setIn(['postalCodeData', 'success'], false)
  );
};
export const getPostalCodeReset = (state, {error}) => {
  return state
    .setIn(['postalCodeData', 'data'], null)
    .setIn(['postalCodeData', 'fetching'], false)
    .setIn(['postalCodeData', 'error'], false)
    .setIn(['postalCodeData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET SUPPLIER DATA ------------------------------ //
export const getSupplierRequest = (state, {data}) => {
  return state.setIn(['supplierData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getSupplierSuccess = (state, {data}) => {
  return state
    .setIn(['supplierData', 'fetching'], false)
    .setIn(['supplierData', 'data'], data)
    .setIn(['supplierData', 'error'], false)
    .setIn(['supplierData', 'success'], true);
};
export const getSupplierFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['supplierData', 'fetching'], false)
      .setIn(['supplierData', 'error'], error)
      .setIn(['supplierData', 'success'], false)
  );
};
export const getSupplierReset = (state, {error}) => {
  return state
    .setIn(['supplierData', 'data'], null)
    .setIn(['supplierData', 'fetching'], false)
    .setIn(['supplierData', 'error'], false)
    .setIn(['supplierData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS DELETE TOKO DATA ------------------------------ //
export const deleteTokoRequest = (state, {data}) => {
  return state
    .setIn(['tokoDeletedData', 'fetching'], true)
    .setIn(['tokoDeletedData', 'data'], data);
};
export const deleteTokoSuccess = (state, {data}) => {
  return state
    .setIn(['tokoDeletedData', 'fetching'], false)
    .setIn(['tokoDeletedData', 'data'], data)
    .setIn(['tokoDeletedData', 'error'], false)
    .setIn(['tokoDeletedData', 'success'], true);
};
export const deleteTokoFailed = (state, {error}) => {
  return state
    .setIn(['tokoDeletedData', 'fetching'], false)
    .setIn(['tokoDeletedData', 'error'], error)
    .setIn(['tokoDeletedData', 'success'], false);
};
export const deleteTokoReset = (state, {error}) => {
  return state
    .setIn(['tokoDeletedData', 'data'], {})
    .setIn(['tokoDeletedData', 'fetching'], false)
    .setIn(['tokoDeletedData', 'error'], false)
    .setIn(['tokoDeletedData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET TOKO DATA ------------------------------ //
export const getTokoRequest = (state, {data}) => {
  return state.setIn(['tokoData', 'fetching'], true);
  // .setIn(['tokoData', 'data'], data);
};
export const getTokoSuccess = (state, {data}) => {
  return state
    .setIn(['tokoData', 'fetching'], false)
    .setIn(['tokoData', 'data'], data)
    .setIn(['tokoData', 'error'], false)
    .setIn(['tokoData', 'success'], true);
};
export const getTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['tokoData', 'data'], [])
      .setIn(['tokoData', 'fetching'], false)
      .setIn(['tokoData', 'error'], error)
      .setIn(['tokoData', 'success'], false)
  );
};
export const getTokoReset = (state, {error}) => {
  return state
    .setIn(['tokoData', 'data'], [])
    .setIn(['tokoData', 'fetching'], false)
    .setIn(['tokoData', 'error'], false)
    .setIn(['tokoData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET DETAIL TOKO DATA ------------------------------ //
export const getDetailProductRequest = (state, {data}) => {
  return state.setIn(['detailProductData', 'fetching'], true);
  // .setIn(['detailProductData', 'data'], data);
};
export const getDetailProductSuccess = (state, {data}) => {
  return state
    .setIn(['detailProductData', 'fetching'], false)
    .setIn(['detailProductData', 'data'], data)
    .setIn(['detailProductData', 'success'], true);
};
export const getDetailProductFailed = (state, {error}) => {
  return (
    state
      // .setIn(['detailProductData', 'data'], [])
      .setIn(['detailProductData', 'fetching'], false)
      .setIn(['detailProductData', 'error'], error)
      .setIn(['detailProductData', 'success'], false)
  );
};
export const getDetailProductReset = (state, {error}) => {
  return state
    .setIn(['detailProductData', 'data'], null)
    .setIn(['detailProductData', 'fetching'], false)
    .setIn(['detailProductData', 'error'], error)
    .setIn(['detailProductData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET DETAIL TOKO DATA ------------------------------ //
export const getDetailTokoRequest = (state, {data}) => {
  return state.setIn(['detailTokoData', 'fetching'], true);
  // .setIn(['detailTokoData', 'data'], data);
};
export const getDetailTokoSuccess = (state, {data}) => {
  return state
    .setIn(['detailTokoData', 'fetching'], false)
    .setIn(['detailTokoData', 'data'], data)
    .setIn(['detailTokoData', 'success'], true);
};
export const getDetailTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['detailTokoData', 'data'], [])
      .setIn(['detailTokoData', 'fetching'], false)
      .setIn(['detailTokoData', 'error'], error)
      .setIn(['detailTokoData', 'success'], false)
  );
};
export const getDetailTokoReset = (state, {error}) => {
  return state
    .setIn(['detailTokoData', 'data'], null)
    .setIn(['detailTokoData', 'fetching'], false)
    .setIn(['detailTokoData', 'error'], error)
    .setIn(['detailTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET CATEGORY TOKO DATA ------------------------------ //
export const getCategoryTokoRequest = (state, {data}) => {
  return state.setIn(['categoryTokoData', 'fetching'], true);
  // .setIn(['categoryTokoData', 'data'], data);
};
export const getCategoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['categoryTokoData', 'fetching'], false)
    .setIn(['categoryTokoData', 'data'], data)
    .setIn(['categoryTokoData', 'success'], true);
};
export const getCategoryTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['categoryTokoData', 'data'], [])
      .setIn(['categoryTokoData', 'fetching'], false)
      .setIn(['categoryTokoData', 'error'], error)
      .setIn(['categoryTokoData', 'success'], false)
  );
};

export const getCategoryTokoReset = (state, {error}) => {
  return state
    .setIn(['categoryTokoData', 'data'], [])
    .setIn(['categoryTokoData', 'fetching'], false)
    .setIn(['categoryTokoData', 'error'], error)
    .setIn(['categoryTokoData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST CATEGORY TOKO DATA ------------------------------ //
export const postCategoryTokoRequest = (state, {data}) => {
  return state
    .setIn(['postCategoryTokoData', 'fetching'], true)
    .setIn(['postCategoryTokoData', 'data'], data);
};
export const postCategoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['postCategoryTokoData', 'fetching'], false)
    .setIn(['postCategoryTokoData', 'data'], data)
    .setIn(['postCategoryTokoData', 'success'], true);
};
export const postCategoryTokoFailed = (state, {error}) => {
  return state
    .setIn(['postCategoryTokoData', 'data'], [])
    .setIn(['postCategoryTokoData', 'fetching'], false)
    .setIn(['postCategoryTokoData', 'error'], error)
    .setIn(['postCategoryTokoData', 'success'], false);
};
export const postCategoryTokoReset = (state, {error}) => {
  return state
    .setIn(['postCategoryTokoData', 'data'], [])
    .setIn(['postCategoryTokoData', 'fetching'], false)
    .setIn(['postCategoryTokoData', 'error'], error)
    .setIn(['postCategoryTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS UPDATE CATEGORY TOKO DATA ------------------------------ //
export const updateCategoryTokoRequest = (state, {data}) => {
  return state
    .setIn(['categoryTokoUpdatedData', 'fetching'], true)
    .setIn(['categoryTokoUpdatedData', 'data'], data);
};
export const updateCategoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['categoryTokoUpdatedData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedData', 'data'], data)
    .setIn(['categoryTokoUpdatedData', 'success'], true);
};
export const updateCategoryTokoFailed = (state, {error}) => {
  return state
    .setIn(['categoryTokoUpdatedData', 'data'], [])
    .setIn(['categoryTokoUpdatedData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedData', 'error'], error)
    .setIn(['categoryTokoUpdatedData', 'success'], false);
};
export const updateCategoryTokoReset = (state, {error}) => {
  return state
    .setIn(['categoryTokoUpdatedData', 'data'], [])
    .setIn(['categoryTokoUpdatedData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedData', 'error'], error)
    .setIn(['categoryTokoUpdatedData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS UPDATE POSITION CATEGORY TOKO DATA ------------------------------ //
export const updatePositionCategoryTokoRequest = (state, {data}) => {
  return state
    .setIn(['categoryTokoUpdatedPositionData', 'fetching'], true)
    .setIn(['categoryTokoUpdatedPositionData', 'data'], data);
};
export const updatePositionCategoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['categoryTokoUpdatedPositionData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedPositionData', 'data'], data)
    .setIn(['categoryTokoUpdatedPositionData', 'success'], true);
};
export const updatePositionCategoryTokoFailed = (state, {error}) => {
  return state
    .setIn(['categoryTokoUpdatedPositionData', 'data'], [])
    .setIn(['categoryTokoUpdatedPositionData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedPositionData', 'error'], error)
    .setIn(['categoryTokoUpdatedPositionData', 'success'], false);
};
export const updatePositionCategoryTokoReset = (state, {error}) => {
  return state
    .setIn(['categoryTokoUpdatedPositionData', 'data'], [])
    .setIn(['categoryTokoUpdatedPositionData', 'fetching'], false)
    .setIn(['categoryTokoUpdatedPositionData', 'error'], error)
    .setIn(['categoryTokoUpdatedPositionData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS DELETE CATEGORY TOKO DATA ------------------------------ //
export const deleteCategoryTokoRequest = (state, {data}) => {
  return state
    .setIn(['categoryTokoDeletedData', 'fetching'], true)
    .setIn(['categoryTokoDeletedData', 'data'], data);
};
export const deleteCategoryTokoSuccess = (state, {data}) => {
  return state
    .setIn(['categoryTokoDeletedData', 'fetching'], false)
    .setIn(['categoryTokoDeletedData', 'data'], data)
    .setIn(['categoryTokoDeletedData', 'error'], false)
    .setIn(['categoryTokoDeletedData', 'success'], true);
};
export const deleteCategoryTokoFailed = (state, {error}) => {
  return state
    .setIn(['categoryTokoDeletedData', 'fetching'], false)
    .setIn(['categoryTokoDeletedData', 'error'], error)
    .setIn(['categoryTokoDeletedData', 'success'], false);
};
export const deleteCategoryTokoReset = (state, {error}) => {
  return state
    .setIn(['categoryTokoDeletedData', 'data'], {})
    .setIn(['categoryTokoDeletedData', 'fetching'], false)
    .setIn(['categoryTokoDeletedData', 'error'], false)
    .setIn(['categoryTokoDeletedData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET PRODUCT TOKO DATA ------------------------------ //
export const getProductTokoRequest = (state, {data}) => {
  return state.setIn(['productTokoData', 'fetching'], true);
  // .setIn(['productTokoData', 'data'], data);
};
export const getProductTokoSuccess = (state, {data}) => {
  return state
    .setIn(['productTokoData', 'fetching'], false)
    .setIn(['productTokoData', 'data'], data)
    .setIn(['productTokoData', 'success'], true);
};
export const getProductTokoFailed = (state, {error}) => {
  return (
    state
      // .setIn(['productTokoData', 'data'], [])
      .setIn(['productTokoData', 'fetching'], false)
      .setIn(['productTokoData', 'error'], error)
      .setIn(['productTokoData', 'success'], false)
  );
};
export const getProductTokoReset = (state, {error}) => {
  return state
    .setIn(['productTokoData', 'data'], null)
    .setIn(['productTokoData', 'fetching'], false)
    .setIn(['productTokoData', 'error'], error)
    .setIn(['productTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST PRODUCT TOKO DATA ------------------------------ //
export const postProductTokoRequest = (state, {data}) => {
  return state.setIn(['postProductTokoData', 'fetching'], true);
  // .setIn(['postProductTokoData', 'data'], data);
};
export const postProductTokoSuccess = (state, {data}) => {
  return state
    .setIn(['postProductTokoData', 'fetching'], false)
    .setIn(['postProductTokoData', 'data'], data)
    .setIn(['postProductTokoData', 'success'], true);
};
export const postProductTokoFailed = (state, {error}) => {
  return state
    .setIn(['postProductTokoData', 'data'], [])
    .setIn(['postProductTokoData', 'fetching'], false)
    .setIn(['postProductTokoData', 'error'], error)
    .setIn(['postProductTokoData', 'success'], false);
};
export const postProductTokoReset = (state, {error}) => {
  return state
    .setIn(['postProductTokoData', 'data'], [])
    .setIn(['postProductTokoData', 'fetching'], false)
    .setIn(['postProductTokoData', 'error'], error)
    .setIn(['postProductTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS UPDATE PRODUCT TOKO DATA ------------------------------ //
export const updateProductTokoRequest = (state, {data}) => {
  return state.setIn(['updateProductTokoData', 'fetching'], true);
  // .setIn(['updateProductTokoData', 'data'], data);
};
export const updateProductTokoSuccess = (state, {data}) => {
  return state
    .setIn(['updateProductTokoData', 'fetching'], false)
    .setIn(['updateProductTokoData', 'data'], data)
    .setIn(['updateProductTokoData', 'success'], true);
};
export const updateProductTokoFailed = (state, {error}) => {
  return state
    .setIn(['updateProductTokoData', 'data'], [])
    .setIn(['updateProductTokoData', 'fetching'], false)
    .setIn(['updateProductTokoData', 'error'], error)
    .setIn(['updateProductTokoData', 'success'], false);
};
export const updateProductTokoReset = (state, {error}) => {
  return state
    .setIn(['updateProductTokoData', 'data'], [])
    .setIn(['updateProductTokoData', 'fetching'], false)
    .setIn(['updateProductTokoData', 'error'], error)
    .setIn(['updateProductTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS UPDATE PRODUCT TOKO DATA ------------------------------ //
export const miniUpdateProductTokoRequest = (state, {data}) => {
  return state.setIn(['miniUpdateProductTokoData', 'fetching'], true);
  // .setIn(['miniUpdateProductTokoData', 'data'], data);
};
export const miniUpdateProductTokoSuccess = (state, {data}) => {
  return state
    .setIn(['miniUpdateProductTokoData', 'fetching'], false)
    .setIn(['miniUpdateProductTokoData', 'data'], data)
    .setIn(['miniUpdateProductTokoData', 'success'], true);
};
export const miniUpdateProductTokoFailed = (state, {error}) => {
  return state
    .setIn(['miniUpdateProductTokoData', 'data'], [])
    .setIn(['miniUpdateProductTokoData', 'fetching'], false)
    .setIn(['miniUpdateProductTokoData', 'error'], error)
    .setIn(['miniUpdateProductTokoData', 'success'], false);
};
export const miniUpdateProductTokoReset = (state, {error}) => {
  return state
    .setIn(['miniUpdateProductTokoData', 'data'], [])
    .setIn(['miniUpdateProductTokoData', 'fetching'], false)
    .setIn(['miniUpdateProductTokoData', 'error'], error)
    .setIn(['miniUpdateProductTokoData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS DELETE PRODUCT TOKO DATA ------------------------------ //
export const deleteProductTokoRequest = (state, {data}) => {
  return state
    .setIn(['productTokoDeletedData', 'fetching'], true)
    .setIn(['productTokoDeletedData', 'data'], data);
};
export const deleteProductTokoSuccess = (state, {data}) => {
  return state
    .setIn(['productTokoDeletedData', 'fetching'], false)
    .setIn(['productTokoDeletedData', 'data'], data)
    .setIn(['productTokoDeletedData', 'error'], false)
    .setIn(['productTokoDeletedData', 'success'], true);
};
export const deleteProductTokoFailed = (state, {error}) => {
  return state
    .setIn(['productTokoDeletedData', 'fetching'], false)
    .setIn(['productTokoDeletedData', 'error'], error)
    .setIn(['productTokoDeletedData', 'success'], false);
};
export const deleteProductTokoReset = (state, {error}) => {
  return state
    .setIn(['productTokoDeletedData', 'data'], {})
    .setIn(['productTokoDeletedData', 'fetching'], false)
    .setIn(['productTokoDeletedData', 'error'], false)
    .setIn(['productTokoDeletedData', 'success'], false);
};
//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS POST TOKO DATA ------------------------------ //
export const tokoPostRequest = (state, {data}) => {
  return state
    .setIn(['tokoPostData', 'fetching'], true)
    .setIn(['tokoPostData', 'data'], data);
};
export const tokoPostSuccess = (state, {data}) => {
  return state
    .setIn(['tokoPostData', 'fetching'], false)
    .setIn(['tokoPostData', 'data'], data)
    .setIn(['tokoPostData', 'error'], false)
    .setIn(['tokoPostData', 'success'], true);
};
export const tokoPostFailed = (state, {error}) => {
  return state
    .setIn(['tokoPostData', 'fetching'], false)
    .setIn(['tokoPostData', 'error'], error)
    .setIn(['tokoPostData', 'success'], false);
};
export const tokoPostReset = (state, {error}) => {
  return state
    .setIn(['tokoPostData', 'data'], {})
    .setIn(['tokoPostData', 'fetching'], false)
    .setIn(['tokoPostData', 'error'], false)
    .setIn(['tokoPostData', 'success'], false);
};

// ------------------------------------- REDUCERS UPDATE TOKO DATA ------------------------------ //
export const tokoUpdateRequest = (state, {data}) => {
  return state
    .setIn(['tokoUpdateData', 'fetching'], true)
    .setIn(['tokoUpdateData', 'data'], data);
};
export const tokoUpdateSuccess = (state, {data}) => {
  return state
    .setIn(['tokoUpdateData', 'fetching'], false)
    .setIn(['tokoUpdateData', 'data'], data)
    .setIn(['tokoUpdateData', 'error'], false)
    .setIn(['tokoUpdateData', 'success'], true);
};
export const tokoUpdateFailed = (state, {error}) => {
  return state
    .setIn(['tokoUpdateData', 'fetching'], false)
    .setIn(['tokoUpdateData', 'error'], error)
    .setIn(['tokoUpdateData', 'success'], false);
};
export const tokoUpdateReset = (state, {error}) => {
  return state
    .setIn(['tokoUpdateData', 'data'], {})
    .setIn(['tokoUpdateData', 'fetching'], false)
    .setIn(['tokoUpdateData', 'error'], false)
    .setIn(['tokoUpdateData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS GET BUSINESS TYPE DATA ------------------------------ //
export const getBusinessTypeRequest = (state, {data}) => {
  return state.setIn(['getBusinessTypeData', 'fetching'], true);
  // .setIn(['getBusinessTypeData', 'data'], data);
};
export const getBusinessTypeSuccess = (state, {data}) => {
  return state
    .setIn(['getBusinessTypeData', 'fetching'], false)
    .setIn(['getBusinessTypeData', 'data'], data);
};
export const getBusinessTypeFailed = (state, {error}) => {
  return state
    .setIn(['getBusinessTypeData', 'fetching'], false)
    .setIn(['getBusinessTypeData', 'error'], error);
};
export const getBusinessTypeReset = (state, {error}) => {
  return state
    .setIn(['getBusinessTypeData', 'data'], {})
    .setIn(['getBusinessTypeData', 'fetching'], false)
    .setIn(['getBusinessTypeData', 'error'], false)
    .setIn(['getBusinessTypeData', 'success'], false);
};

//**--------------------------------------------------------------------------------------------**//
// ------------------------------------- REDUCERS UPLOAD PRODUCT IMAGE DATA ------------------------------ //
export const uploadProductImageRequest = (state, {data}) => {
  return state
    .setIn(['uploadProductImageData', 'fetching'], true)
    .setIn(['uploadProductImageData', 'data'], data);
};
export const uploadProductImageSuccess = (state, {data}) => {
  return state
    .setIn(['uploadProductImageData', 'fetching'], false)
    .setIn(['uploadProductImageData', 'data'], data);
};
export const uploadProductImageFailed = (state, {error}) => {
  return state
    .setIn(['uploadProductImageData', 'fetching'], false)
    .setIn(['uploadProductImageData', 'error'], error);
};
export const uploadProductImageReset = (state, {error}) => {
  return state
    .setIn(['uploadProductImageData', 'data'], {})
    .setIn(['uploadProductImageData', 'fetching'], false)
    .setIn(['uploadProductImageData', 'error'], false)
    .setIn(['uploadProductImageData', 'success'], false);
};
export const getTokoUnavailableFailed = (state, {error}) => {
  return (
    state
      .setIn(['categoryTokoData', 'data'], null)
      .setIn(['categoryTokoData', 'fetching'], false)
      .setIn(['categoryTokoData', 'error'], error)
      .setIn(['categoryTokoData', 'success'], false)

      .setIn(['productTokoData', 'data'], null)
      .setIn(['productTokoData', 'fetching'], false)
      .setIn(['productTokoData', 'error'], error)
      .setIn(['productTokoData', 'success'], false)

      // .setIn(['tokoOrderData', 'data'], null)
      // .setIn(['tokoOrderData', 'fetching'], false)
      // .setIn(['tokoOrderData', 'error'], error)
      // .setIn(['tokoOrderData', 'success'], false)

      .setIn(['tokoAddressData', 'data'], null)
      .setIn(['tokoAddressData', 'fetching'], false)
      .setIn(['tokoAddressData', 'error'], error)
      .setIn(['tokoAddressData', 'success'], false)
  );
};
export const reset = () => INITIAL_STATE;
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /*-------------------DELETE TOKO DATA-----------------*/
  [Types.DELETE_TOKO_REQUEST]: deleteTokoRequest,
  [Types.DELETE_TOKO_SUCCESS]: deleteTokoSuccess,
  [Types.DELETE_TOKO_FAILED]: deleteTokoFailed,
  [Types.DELETE_TOKO_RESET]: deleteTokoReset,
  /*-------------------GET TOKO DATA-----------------*/
  [Types.GET_TOKO_REQUEST]: getTokoRequest,
  [Types.GET_TOKO_SUCCESS]: getTokoSuccess,
  [Types.GET_TOKO_FAILED]: getTokoFailed,
  [Types.GET_TOKO_RESET]: getTokoReset,
  /*-------------------GET DETAIL PRODUCT DATA-----------------*/
  [Types.GET_DETAIL_PRODUCT_REQUEST]: getDetailProductRequest,
  [Types.GET_DETAIL_PRODUCT_SUCCESS]: getDetailProductSuccess,
  [Types.GET_DETAIL_PRODUCT_FAILED]: getDetailProductFailed,
  [Types.GET_DETAIL_PRODUCT_RESET]: getDetailProductReset,
  /*-------------------GET DETAIL TOKO DATA-----------------*/
  [Types.GET_DETAIL_TOKO_REQUEST]: getDetailTokoRequest,
  [Types.GET_DETAIL_TOKO_SUCCESS]: getDetailTokoSuccess,
  [Types.GET_DETAIL_TOKO_FAILED]: getDetailTokoFailed,
  [Types.GET_DETAIL_TOKO_RESET]: getDetailTokoReset,
  /*-------------------GET CATEGORY TOKO DATA-----------------*/
  [Types.GET_CATEGORY_TOKO_REQUEST]: getCategoryTokoRequest,
  [Types.GET_CATEGORY_TOKO_SUCCESS]: getCategoryTokoSuccess,
  [Types.GET_CATEGORY_TOKO_FAILED]: getCategoryTokoFailed,
  [Types.GET_CATEGORY_TOKO_RESET]: getCategoryTokoReset,

  /*-------------------POST CATEGORY TOKO DATA-----------------*/
  [Types.POST_CATEGORY_TOKO_REQUEST]: postCategoryTokoRequest,
  [Types.POST_CATEGORY_TOKO_SUCCESS]: postCategoryTokoSuccess,
  [Types.POST_CATEGORY_TOKO_FAILED]: postCategoryTokoFailed,
  [Types.POST_CATEGORY_TOKO_RESET]: postCategoryTokoReset,
  /*-------------------UPDATE  CATEGORY TOKO DATA-----------------*/
  [Types.UPDATE_CATEGORY_TOKO_REQUEST]: updateCategoryTokoRequest,
  [Types.UPDATE_CATEGORY_TOKO_SUCCESS]: updateCategoryTokoSuccess,
  [Types.UPDATE_CATEGORY_TOKO_FAILED]: updateCategoryTokoFailed,
  [Types.UPDATE_CATEGORY_TOKO_RESET]: updateCategoryTokoReset,
  /*-------------------UPDATE POSITION  CATEGORY TOKO DATA-----------------*/
  [Types.UPDATE_POSITION_CATEGORY_TOKO_REQUEST]: updatePositionCategoryTokoRequest,
  [Types.UPDATE_POSITION_CATEGORY_TOKO_SUCCESS]: updatePositionCategoryTokoSuccess,
  [Types.UPDATE_POSITION_CATEGORY_TOKO_FAILED]: updatePositionCategoryTokoFailed,
  [Types.UPDATE_POSITION_CATEGORY_TOKO_RESET]: updatePositionCategoryTokoReset,
  /*-------------------DELETE CATEGORY TOKO DATA-----------------*/
  [Types.DELETE_CATEGORY_TOKO_REQUEST]: deleteCategoryTokoRequest,
  [Types.DELETE_CATEGORY_TOKO_SUCCESS]: deleteCategoryTokoSuccess,
  [Types.DELETE_CATEGORY_TOKO_FAILED]: deleteCategoryTokoFailed,
  [Types.DELETE_CATEGORY_TOKO_RESET]: deleteCategoryTokoReset,
  /*-------------------GET PRODUCT TOKO DATA-----------------*/
  [Types.GET_PRODUCT_TOKO_REQUEST]: getProductTokoRequest,
  [Types.GET_PRODUCT_TOKO_SUCCESS]: getProductTokoSuccess,
  [Types.GET_PRODUCT_TOKO_FAILED]: getProductTokoFailed,
  [Types.GET_PRODUCT_TOKO_RESET]: getProductTokoReset,
  /*-------------------POST PRODUCT TOKO DATA-----------------*/
  [Types.POST_PRODUCT_TOKO_REQUEST]: postProductTokoRequest,
  [Types.POST_PRODUCT_TOKO_SUCCESS]: postProductTokoSuccess,
  [Types.POST_PRODUCT_TOKO_FAILED]: postProductTokoFailed,
  [Types.POST_PRODUCT_TOKO_RESET]: postProductTokoReset,
  /*-------------------UPDATE PRODUCT TOKO DATA-----------------*/
  [Types.UPDATE_PRODUCT_TOKO_REQUEST]: updateProductTokoRequest,
  [Types.UPDATE_PRODUCT_TOKO_SUCCESS]: updateProductTokoSuccess,
  [Types.UPDATE_PRODUCT_TOKO_FAILED]: updateProductTokoFailed,
  [Types.UPDATE_PRODUCT_TOKO_RESET]: updateProductTokoReset,
  /*-------------------UPDATE MINI PRODUCT TOKO DATA-----------------*/
  [Types.MINI_UPDATE_PRODUCT_TOKO_REQUEST]: miniUpdateProductTokoRequest,
  [Types.MINI_UPDATE_PRODUCT_TOKO_SUCCESS]: miniUpdateProductTokoSuccess,
  [Types.MINI_UPDATE_PRODUCT_TOKO_FAILED]: miniUpdateProductTokoFailed,
  [Types.MINI_UPDATE_PRODUCT_TOKO_RESET]: miniUpdateProductTokoReset,
  /*-------------------DELETE PRODUCT TOKO DATA-----------------*/
  [Types.DELETE_PRODUCT_TOKO_REQUEST]: deleteProductTokoRequest,
  [Types.DELETE_PRODUCT_TOKO_SUCCESS]: deleteProductTokoSuccess,
  [Types.DELETE_PRODUCT_TOKO_FAILED]: deleteProductTokoFailed,
  [Types.DELETE_PRODUCT_TOKO_RESET]: deleteProductTokoReset,
  /*-------------------POST TOKO DATA-----------------*/
  [Types.TOKO_POST_REQUEST]: tokoPostRequest,
  [Types.TOKO_POST_SUCCESS]: tokoPostSuccess,
  [Types.TOKO_POST_FAILED]: tokoPostFailed,
  [Types.TOKO_POST_RESET]: tokoPostReset,

  /*-------------------POST TOKO DATA-----------------*/
  [Types.TOKO_UPDATE_REQUEST]: tokoUpdateRequest,
  [Types.TOKO_UPDATE_SUCCESS]: tokoUpdateSuccess,
  [Types.TOKO_UPDATE_FAILED]: tokoUpdateFailed,
  [Types.TOKO_UPDATE_RESET]: tokoUpdateReset,

  /*-------------------GET BUSINESS TYPE DATA-----------------*/
  [Types.GET_BUSINESS_TYPE_REQUEST]: getBusinessTypeRequest,
  [Types.GET_BUSINESS_TYPE_SUCCESS]: getBusinessTypeSuccess,
  [Types.GET_BUSINESS_TYPE_FAILED]: getBusinessTypeFailed,
  [Types.GET_BUSINESS_TYPE_RESET]: getBusinessTypeReset,
  /*-------------------UPLOAD PRODUCT IMAGE DATA-----------------*/
  [Types.UPLOAD_PRODUCT_IMAGE_REQUEST]: uploadProductImageRequest,
  [Types.UPLOAD_PRODUCT_IMAGE_SUCCESS]: uploadProductImageSuccess,
  [Types.UPLOAD_PRODUCT_IMAGE_FAILED]: uploadProductImageFailed,
  [Types.UPLOAD_PRODUCT_IMAGE_RESET]: uploadProductImageReset,
  /*-------------------GET SUPPLIER DATA-----------------*/
  [Types.GET_SUPPLIER_REQUEST]: getSupplierRequest,
  [Types.GET_SUPPLIER_SUCCESS]: getSupplierSuccess,
  [Types.GET_SUPPLIER_FAILED]: getSupplierFailed,
  [Types.GET_SUPPLIER_RESET]: getSupplierReset,
  /*-------------------GET TOKO ADDRESS DATA-----------------*/
  [Types.GET_TOKO_ADDRESS_REQUEST]: getTokoAddressRequest,
  [Types.GET_TOKO_ADDRESS_SUCCESS]: getTokoAddressSuccess,
  [Types.GET_TOKO_ADDRESS_FAILED]: getTokoAddressFailed,
  [Types.GET_TOKO_ADDRESS_RESET]: getTokoAddressReset,
  /*-------------------POST TOKO ADDRESS DATA-----------------*/
  [Types.POST_TOKO_ADDRESS_REQUEST]: postTokoAddressRequest,
  [Types.POST_TOKO_ADDRESS_SUCCESS]: postTokoAddressSuccess,
  [Types.POST_TOKO_ADDRESS_FAILED]: postTokoAddressFailed,
  [Types.POST_TOKO_ADDRESS_RESET]: postTokoAddressReset,

  /*-------------------GET PROVINCE DATA-----------------*/
  [Types.GET_PROVINCE_REQUEST]: getProvinceRequest,
  [Types.GET_PROVINCE_SUCCESS]: getProvinceSuccess,
  [Types.GET_PROVINCE_FAILED]: getProvinceFailed,
  [Types.GET_PROVINCE_RESET]: getProvinceReset,
  /*-------------------GET CITY DATA-----------------*/
  [Types.GET_CITY_REQUEST]: getCityRequest,
  [Types.GET_CITY_SUCCESS]: getCitySuccess,
  [Types.GET_CITY_FAILED]: getCityFailed,
  [Types.GET_CITY_RESET]: getCityReset,
  /*-------------------GET DISTRICT DATA-----------------*/
  [Types.GET_DISTRICT_REQUEST]: getDistrictRequest,
  [Types.GET_DISTRICT_SUCCESS]: getDistrictSuccess,
  [Types.GET_DISTRICT_FAILED]: getDistrictFailed,
  [Types.GET_DISTRICT_RESET]: getDistrictReset,
  /*-------------------GET POSTAL CODE DATA-----------------*/
  [Types.GET_POSTAL_CODE_REQUEST]: getPostalCodeRequest,
  [Types.GET_POSTAL_CODE_SUCCESS]: getPostalCodeSuccess,
  [Types.GET_POSTAL_CODE_FAILED]: getPostalCodeFailed,
  [Types.GET_POSTAL_CODE_RESET]: getPostalCodeReset,
  /*-------------------GET BANK DATA-----------------*/
  [Types.GET_BANK_REQUEST]: getBankRequest,
  [Types.GET_BANK_SUCCESS]: getBankSuccess,
  [Types.GET_BANK_FAILED]: getBankFailed,
  [Types.GET_BANK_RESET]: getBankReset,
  /*-------------------GET TOKO BANK DATA-----------------*/
  [Types.GET_TOKO_BANK_REQUEST]: getTokoBankRequest,
  [Types.GET_TOKO_BANK_SUCCESS]: getTokoBankSuccess,
  [Types.GET_TOKO_BANK_FAILED]: getTokoBankFailed,
  [Types.GET_TOKO_BANK_RESET]: getTokoBankReset,
  /*-------------------POST TOKO BANK DATA-----------------*/
  [Types.POST_TOKO_BANK_REQUEST]: postTokoBankRequest,
  [Types.POST_TOKO_BANK_SUCCESS]: postTokoBankSuccess,
  [Types.POST_TOKO_BANK_FAILED]: postTokoBankFailed,
  [Types.POST_TOKO_BANK_RESET]: postTokoBankReset,
  /*-------------------GET TOKO ORDER DATA-----------------*/
  [Types.GET_TOKO_ORDER_REQUEST]: getTokoOrderRequest,
  [Types.GET_TOKO_ORDER_SUCCESS]: getTokoOrderSuccess,
  [Types.GET_TOKO_ORDER_FAILED]: getTokoOrderFailed,
  [Types.GET_TOKO_ORDER_RESET]: getTokoOrderReset,
  /*-------------------GET TOKO ORDER DETAIL DATA-----------------*/
  [Types.GET_TOKO_ORDER_DETAIL_REQUEST]: getTokoOrderDetailRequest,
  [Types.GET_TOKO_ORDER_DETAIL_SUCCESS]: getTokoOrderDetailSuccess,
  [Types.GET_TOKO_ORDER_DETAIL_FAILED]: getTokoOrderDetailFailed,
  [Types.GET_TOKO_ORDER_DETAIL_RESET]: getTokoOrderDetailReset,
  /*-------------------GET TOKO COURIER DATA-----------------*/
  [Types.GET_TOKO_COURIER_REQUEST]: getTokoCourierRequest,
  [Types.GET_TOKO_COURIER_SUCCESS]: getTokoCourierSuccess,
  [Types.GET_TOKO_COURIER_FAILED]: getTokoCourierFailed,
  [Types.GET_TOKO_COURIER_RESET]: getTokoCourierReset,

  /*-------------------POST TOKO ORDER PROCEED DATA-----------------*/
  [Types.POST_TOKO_ORDER_PROCEED_REQUEST]: postTokoOrderProceedRequest,
  [Types.POST_TOKO_ORDER_PROCEED_SUCCESS]: postTokoOrderProceedSuccess,
  [Types.POST_TOKO_ORDER_PROCEED_FAILED]: postTokoOrderProceedFailed,
  [Types.POST_TOKO_ORDER_PROCEED_RESET]: postTokoOrderProceedReset,
  /*-------------------POST TOKO ACCEPT PAYMENT DATA-----------------*/
  [Types.POST_TOKO_ACCEPT_PAYMENT_REQUEST]: postTokoAcceptPaymentRequest,
  [Types.POST_TOKO_ACCEPT_PAYMENT_SUCCESS]: postTokoAcceptPaymentSuccess,
  [Types.POST_TOKO_ACCEPT_PAYMENT_FAILED]: postTokoAcceptPaymentFailed,
  [Types.POST_TOKO_ACCEPT_PAYMENT_RESET]: postTokoAcceptPaymentReset,
  /*-------------------POST TOKO SEND ORDER DATA-----------------*/
  [Types.POST_TOKO_SEND_ORDER_REQUEST]: postTokoSendOrderRequest,
  [Types.POST_TOKO_SEND_ORDER_SUCCESS]: postTokoSendOrderSuccess,
  [Types.POST_TOKO_SEND_ORDER_FAILED]: postTokoSendOrderFailed,
  [Types.POST_TOKO_SEND_ORDER_RESET]: postTokoSendOrderReset,
  /*-------------------POST TOKO FINISH ORDER DATA-----------------*/
  [Types.POST_TOKO_FINISH_ORDER_REQUEST]: postTokoFinishOrderRequest,
  [Types.POST_TOKO_FINISH_ORDER_SUCCESS]: postTokoFinishOrderSuccess,
  [Types.POST_TOKO_FINISH_ORDER_FAILED]: postTokoFinishOrderFailed,
  [Types.POST_TOKO_FINISH_ORDER_RESET]: postTokoFinishOrderReset,
  /*-------------------UPDATE DEFAULT REKENING DATA-----------------*/
  [Types.UPDATE_DEFAULT_REKENING_TOKO_REQUEST]: updateDefaultRekeningTokoRequest,
  [Types.UPDATE_DEFAULT_REKENING_TOKO_SUCCESS]: updateDefaultRekeningTokoSuccess,
  [Types.UPDATE_DEFAULT_REKENING_TOKO_FAILED]: updateDefaultRekeningTokoFailed,
  [Types.UPDATE_DEFAULT_REKENING_TOKO_RESET]: updateDefaultRekeningTokoReset,
  /*-------------------GET TOKO FINANCIAL DATA-----------------*/
  [Types.GET_TOKO_FINANCIAL_REQUEST]: getTokoFinancialRequest,
  [Types.GET_TOKO_FINANCIAL_SUCCESS]: getTokoFinancialSuccess,
  [Types.GET_TOKO_FINANCIAL_FAILED]: getTokoFinancialFailed,
  [Types.GET_TOKO_FINANCIAL_RESET]: getTokoFinancialReset,
  /*-------------------GET OWNER TOKO COURIER DATA-----------------*/
  [Types.GET_OWNER_TOKO_COURIER_REQUEST]: getOwnerTokoCourierRequest,
  [Types.GET_OWNER_TOKO_COURIER_SUCCESS]: getOwnerTokoCourierSuccess,
  [Types.GET_OWNER_TOKO_COURIER_FAILED]: getOwnerTokoCourierFailed,
  [Types.GET_OWNER_TOKO_COURIER_RESET]: getOwnerTokoCourierReset,
  /*-------------------HANDLING OWNER TOKO COURIER DATA-----------------*/
  [Types.HANDLING_OWNER_TOKO_COURIER_STATUS_REQUEST]: handlingOwnerTokoCourierStatusRequest,
  [Types.HANDLING_OWNER_TOKO_COURIER_STATUS_SUCCESS]: handlingOwnerTokoCourierStatusSuccess,
  [Types.HANDLING_OWNER_TOKO_COURIER_STATUS_FAILED]: handlingOwnerTokoCourierStatusFailed,
  [Types.HANDLING_OWNER_TOKO_COURIER_STATUS_RESET]: handlingOwnerTokoCourierStatusReset,
  /*-------------------HANDLING KWC STATUS TOKO DATA-----------------*/
  [Types.GET_KYC_STATUS_TOKO_REQUEST]: getKycStatusTokoRequest,
  [Types.GET_KYC_STATUS_TOKO_SUCCESS]: getKycStatusTokoSuccess,
  [Types.GET_KYC_STATUS_TOKO_FAILED]: getKycStatusTokoFailed,
  [Types.GET_KYC_STATUS_TOKO_RESET]: getKycStatusTokoReset,
  /*-------------------POST KWC DATA-----------------*/
  [Types.POST_KYC_TOKO_REQUEST]: postKycTokoRequest,
  [Types.POST_KYC_TOKO_SUCCESS]: postKycTokoSuccess,
  [Types.POST_KYC_TOKO_FAILED]: postKycTokoFailed,
  [Types.POST_KYC_TOKO_RESET]: postKycTokoReset,
  /*-------------------HANDLING WITHDRAW HISTORY TOKO DATA-----------------*/
  [Types.GET_WITHDRAW_HISTORY_TOKO_REQUEST]: getWithdrawHistoryTokoRequest,
  [Types.GET_WITHDRAW_HISTORY_TOKO_SUCCESS]: getWithdrawHistoryTokoSuccess,
  [Types.GET_WITHDRAW_HISTORY_TOKO_FAILED]: getWithdrawHistoryTokoFailed,
  [Types.GET_WITHDRAW_HISTORY_TOKO_RESET]: getWithdrawHistoryTokoReset,
  /*-------------------POST WITHDRAW DISBURSMENT DATA-----------------*/
  [Types.POST_WITHDRAW_DISBURSMENT_TOKO_REQUEST]: postWithdrawDisbursmentTokoRequest,
  [Types.POST_WITHDRAW_DISBURSMENT_TOKO_SUCCESS]: postWithdrawDisbursmentTokoSuccess,
  [Types.POST_WITHDRAW_DISBURSMENT_TOKO_FAILED]: postWithdrawDisbursmentTokoFailed,
  [Types.POST_WITHDRAW_DISBURSMENT_TOKO_RESET]: postWithdrawDisbursmentTokoReset,
  /*-------------------RESET ALL TOKO DATA-----------------*/
  [Types.TOKO_RESET]: reset,
  [Types.GET_TOKO_UNAVAILABLE_FAILED]: getTokoUnavailableFailed,
});
