import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  fcmSetData: ['data'],
  fcmReset: null,
});

export const LoginEmailTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fcmData: {
    data: null,
  },
});

/* ------------- Selectors ------------- */

const setFcmData = (state, {data}) => {
  return state.setIn(['fcmData', 'data'], data);
};
export const reset = () => INITIAL_STATE;
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /*-------------------POST TOKO DATA-----------------*/
  [Types.FCM_SET_DATA]: setFcmData,
  [Types.FCM_RESET]: reset,
});
