import Config from 'react-native-config';
import * as Sentry from '@sentry/react-native';
import NetInfo from '@react-native-community/netinfo';
export default () => {
  try {
    NetInfo.fetch().then(state => {
      if (!state.type.match(/none|unknown/)) {
        initSentry();
      }
    });
  } catch (error) {
    console.log('error on init sentry', error);
  }
};

const initSentry = () => {
  console.log('INITIALIZE SENTRY');
  Sentry.init({
    dsn: Config.SENTRY_DSN,
    environment: Config.ENV === 'dev' ? 'Development' : 'Production',
    enableAutoSessionTracking: true,
    // Sessions close after app is 10 seconds in the background.
    sessionTrackingIntervalMillis: 10000,
  });
  // Sentry.setTag('myTag', 'tag-value');
  // Sentry.setExtra('myExtra', 'extra-value');
  // Sentry.addBreadcrumb({message: 'test'});

  // Sentry.captureMessage('Hello Sentry!');
  Sentry.setContext(Config.env, {
    environment: Config.ENV,
  });
};
