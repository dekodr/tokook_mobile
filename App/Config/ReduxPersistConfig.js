import defaultOfflineConfig from '@redux-offline/redux-offline/lib/defaults';
import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform';
import {createMigrate, persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import autoMergeLevel1 from 'redux-persist/es/stateReconciler/autoMergeLevel1';
const migrations = {
  0: (state) => {
    // migration clear out device state
    return {
      ...state,
      device: undefined,
    };
  },
  1: (state) => {
    // migration to keep only device state
    return {
      device: state.device,
    };
  },
};
// export default offlineConfig = {
//   ...defaultOfflineConfig,

//   persistOptions: {
//     whitelist: ['app', 'toko', 'auth'],
//     transforms: [immutablePersistenceTransform],
//     migrate: createMigrate(migrations, {debug: false}),
//     stateReconciler: autoMergeLevel1,
//     key: 'primary',
//     version: 1,
//     storage: AsyncStorage,
//   },

//   retry: (action, retries) => null,
//   returnPromises: true,
// };

// More info here:  https://shift.infinite.red/shipping-persistant-reducers-7341691232b1
const REDUX_PERSIST = {
  active: true,
  storeConfig: {
    key: 'primary',
    version: 1,
    storage: AsyncStorage,
    migrate: createMigrate(migrations, {debug: false}),
    stateReconciler: autoMergeLevel1,
    whitelist: ['app', 'toko', 'auth', 'marketplaceNew', 'marketplace'],
    debug: true,
    blacklist: [],
    transforms: [immutablePersistenceTransform],
  },
};

export default REDUX_PERSIST;
