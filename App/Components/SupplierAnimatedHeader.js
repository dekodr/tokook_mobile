import React from 'react';
import PropTypes from 'prop-types';
import {Animated, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Colors, Fonts} from '../Themes';

const HEADER_HEIGHT   = 100;
const HEADER_OPACITY  = 1;

const AnimatedHeader = ({animatedValue, headerText}) => {
  const insets = useSafeAreaInsets();

  const headerHeight = animatedValue.interpolate({
    inputRange: [0, HEADER_HEIGHT],
    outputRange: [HEADER_HEIGHT, insets.top + 48 + 8 * 2],
    extrapolate: 'clamp',
  });
  console.log(headerHeight);

  const headerOpacity = animatedValue.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 1],
  });

  return (
    <View>
      {/* <View style={styles.fixedHeader}>
        <Pressable onPress={handleGoBack} style={{paddingRight: 16}}>
          <MaterialCommunityIcons
            name={'keyboard-backspace'}
            color='rgba(0,0,0,0)'
            size={25}
          />
        </Pressable>
        <Pressable style={styles.searchBar} onPress={() => toSearchScreen()}>
          <View style={styles.searchBarIcon}>
            <MaterialCommunityIcons
              name={'magnify'}
              color={Colors.textGreyLight}
              size={Fonts.size_24}
            />
          </View>
          <View style={styles.searchBarInput}>
            <Text style={styles.searchBarInputPlaceholder}>Cari Barang</Text>
          </View>
        </Pressable>
        {/* CART BUTTON */}
        <View style={[styles.cartButtonWrapper, {width: 32, height: 32,}]}>
          <View style={[styles.cartIndicator]}>
            <View style={[styles.cartIndicatorInner]}>
              <Text style={styles.cartIndicatorInnerText}>2</Text>
            </View>
          </View>
          <ButtonIcon
            onPress={() => _handleCart()}
            style={[styles.cart]}
            icon="cart-outline"
            color="#fff"
            size={Fonts.size_24 + 4}
            style={{
              // backgroundColor: '#000',
              height: 32,
              width: 32,
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 0,
            }}
          />
        </View>
      </View> */}
    </View>
    // <Animated.View
    //   style={{
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     right: 0,
    //     zIndex: 1,
    //     paddingHorizontal: 12*2+48,
    //     height: headerHeight,
    //     opacity: headerOpacity,
    //     backgroundColor: '#1a69d5',
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //   }}>
    //     <View style={{
    //       // backgroundColor: '#ddd',
    //       width: '100%',
    //       // height: 35,
          
    //     }}>
    //       <Text 
    //         numberOfLines={1}
    //         style={{
    //           textAlign: 'center',
    //           fontSize: Fonts.size_18, 
    //           fontFamily: 'NotoSans-Bold', 
    //           color: '#fff', 
    //           letterSpacing: 1
    //       }}>
    //         {headerText}
    //       </Text>
    //     </View>
    // </Animated.View>
  );
};

export default AnimatedHeader;
