import React from 'react';
import PropTypes from 'prop-types';
import {Pressable, Text, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../Themes/';
import {ActivityIndicator} from 'react-native-paper';

const PRIMARY_COLOR = Colors.blueBold;
const SECONDARY_COLOR = Colors.whiteBlue;

const getStyles = ({
  size,
  theme,
  login,
  logout,
  fullwidth,
  rounded,
  selected,
  option,
}) => {
  const containerStyles = [styles.containerDefault];
  const textStyles1 = [styles.textDefault];

  // theme control
  if (theme === 'white') {
    containerStyles.push(styles.containerWhite);
    textStyles1.push(styles.textWhite);
  } else if (theme === 'whiteblue') {
    containerStyles.push(styles.containerWhite);
    textStyles1.push({color: Colors.Primary});
  } else if (theme === 'secondary') {
    containerStyles.push(styles.containerSecondary);
    textStyles1.push(styles.textSecondary);
  } else {
    containerStyles.push(styles.containerPrimary);
    textStyles1.push(styles.textPrimary);
  }
  // theme control - - - END

  // size control
  if (size === 'large') {
    containerStyles.push(styles.containerLarge);
    textStyles1.push(styles.textLarge);
  } else if (size === 'small') {
    containerStyles.push(styles.containerSmall);
    textStyles1.push(styles.textSmall);
  } else if (size === 'medium') {
    containerStyles.push(styles.containerMedium);
    textStyles1.push(styles.textMedium);
  }
  // size control - - - END

  if (login) {
    containerStyles.push(styles.containerLogin);
    textStyles1.push(styles.textLogin);
  }

  if (logout) {
    containerStyles.push(styles.containerLogout);
    textStyles1.push(styles.textLogout);
  }

  if (fullwidth) {
    containerStyles.push(styles.containerFullwidth);
    textStyles1.push(styles.textFullwidth);
  }

  if (rounded) {
    containerStyles.push(styles.containerRounded);
    textStyles1.push(styles.textRounded);
  }

  if (selected) {
    containerStyles.push(styles.containerSelected);
    textStyles1.push(styles.textSelected);
  }

  if (option) {
    containerStyles.push(styles.containerOption);
    textStyles1.push(styles.textOption);
  }

  return {containerStyles, textStyles1};
};

class Button extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    // onPress: PropTypes.func.isRequired,
    outline: PropTypes.bool,
    login: PropTypes.bool,
    logout: PropTypes.bool,
    fullwidth: PropTypes.bool,
    rounded: PropTypes.bool,
    size: PropTypes.oneOf(['small', 'default', 'large']),
    theme: PropTypes.oneOf(['primary', 'secondary']),
    onPress: PropTypes.func,
    style: PropTypes.string,
    textStyle: PropTypes.string,
    selected: PropTypes.bool,
    option: PropTypes.bool,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    size: 'default',
    theme: 'primary',
    outline: false,
    login: false,
    fullwidth: true,
    rounded: false,
    selected: false,
    option: false,
    isFetching: false,
  };

  render() {
    const {
      text,
      style,
      textStyle,
      onPress,
      login,
      selected,
      isFetching,
      ...rest
    } = this.props;
    const {textStyles1, containerStyles} = getStyles({login, ...rest});
    return (
      <Pressable
        disabled={isFetching}
        onPress={onPress}
        style={[containerStyles, style]}>
        {isFetching ? (
          <ActivityIndicator color={Colors.white} size={'small'} />
        ) : (
          <Text style={[textStyles1, textStyle]}>{text}</Text>
        )}
      </Pressable>
    );
  }
}

export default Button;

const styles = StyleSheet.create({
  // Container Styles
  containerDefault: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderRadius: 10,
    marginHorizontal: 0,
    // marginTop: 16,
  },
  containerWhite: {
    backgroundColor: '#fff',
    borderColor: '#fff',
    shadowColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1.8,
    // elevation: 2.3,
  },
  containerSecondary: {
    backgroundColor: SECONDARY_COLOR,
    borderColor: SECONDARY_COLOR,
    shadowColor: SECONDARY_COLOR,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    // elevation: 2.3,
  },
  containerPrimary: {
    backgroundColor: PRIMARY_COLOR,
    borderColor: PRIMARY_COLOR,
    shadowColor: PRIMARY_COLOR,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    // elevation: 6,
  },
  containerLarge: {
    paddingVertical: 24,
    width: '100%',
  },
  containerMedium: {
    paddingVertical: 10,
    width: '100%',
  },
  containerSmall: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  containerLogin: {
    height: 63,
    paddingHorizontal: 50,
  },
  containerLogout: {
    width: '100%',
    height: 63,
    paddingHorizontal: 50,
    borderColor: Colors.White,
    backgroundColor: Colors.White,
  },
  containerFullwidth: {
    width: '100%',
  },
  containerRounded: {
    borderRadius: 100,
  },

  // Text Styles
  textDefault: {
    fontSize: Fonts.size_20,
    fontFamily: 'NotoSans-Bold',
    color: '#fff',
    letterSpacing: 0.3,
  },
  textPrimary: {},
  textSecondary: {},
  textWhite: {
    color: Colors.Black,
  },
  textLarge: {
    fontSize: Fonts.size_20,
  },
  textMedium: {
    fontSize: Fonts.size_16,
  },
  textSmall: {
    fontSize: Fonts.size_14,
  },
  textLogin: {
    letterSpacing: 0,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_22,
  },
  textLogout: {
    letterSpacing: 0,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_22,
    color: Colors.PrimaryDark,
  },

  containerSelected: {
    backgroundColor: Colors.blueBold,
    borderColor: Colors.blueBold,
  },
  textSelected: {
    color: Colors.white,
  },
  containerOption: {
    backgroundColor: Colors.white,
    borderColor: Colors.textGray,
    borderWidth: 2,
    borderStyle: 'solid',
  },
  textOption: {
    color: Colors.textGray,
  },
});
