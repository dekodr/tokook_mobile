import React from 'react';
import PropTypes from 'prop-types';
import {Animated, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Colors, Fonts} from '../Themes';

const HEADER_HEIGHT   = 100;
const HEADER_OPACITY  = 1;

// export default class AnimatedHeaderComponent extends React.Component {
//   static propTypes = {
//     text: PropTypes.string,
//     animatedValue: PropTypes.string,
//   };

//   render() {
//     const AnimatedHeader = ({animatedValue}) => {
//       const insets = useSafeAreaInsets();
    
//       const headerHeight = animatedValue.interpolate({
//         inputRange: [0, HEADER_HEIGHT],
//         outputRange: [HEADER_HEIGHT, insets.top + 48 + 8 * 2],
//         extrapolate: 'clamp',
//       });
//       console.log(headerHeight);
    
//       const headerOpacity = animatedValue.interpolate({
//         inputRange: [0, 100],
//         outputRange: [0, 1],
//       });
    
//       return (
//         <Animated.View
//           style={{
//             position: 'absolute',
//             top: 0,
//             left: 0,
//             right: 0,
//             zIndex: 999,
//             height: headerHeight,
//             opacity: 1,
//             backgroundColor: '#1a69d5',
//           }}
//         />
//       );
//     };

//     return AnimatedHeader;
//   }
// }
const AnimatedHeader = ({animatedValue, headerText}) => {
  const insets = useSafeAreaInsets();

  const headerHeight = animatedValue.interpolate({
    inputRange: [0, HEADER_HEIGHT],
    outputRange: [HEADER_HEIGHT, insets.top + 48 + 8 * 2],
    extrapolate: 'clamp',
  });
  console.log(headerHeight);

  const headerOpacity = animatedValue.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 1],
  });

  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 1,
        paddingHorizontal: 12*2+48,
        height: headerHeight,
        opacity: headerOpacity,
        backgroundColor: '#1a69d5',
        flexDirection: 'row',
        alignItems: 'center',
      }}>
        <View style={{
          // backgroundColor: '#ddd',
          width: '100%',
          // height: 35,
          
        }}>
          <Text 
            numberOfLines={1}
            style={{
              textAlign: 'center',
              fontSize: Fonts.size_18, 
              fontFamily: 'NotoSans-Bold', 
              color: '#fff', 
              letterSpacing: 1
          }}>
            {headerText}
          </Text>
        </View>
    </Animated.View>
  );
};

export default AnimatedHeader;
