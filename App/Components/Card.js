import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Pressable, Image, TextInput, ImageBackground, Dimensions, Animated } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Colors, Fonts} from '../Themes';
import Styles from '../Containers/MarketplaceModule/Intro/styles';

let {height, width} = Dimensions.get('window');

class Card extends React.Component {
  static propTypes = {
    handlePress: PropTypes.string,
    type: PropTypes.string.isRequired,
    // = = Data
    id: PropTypes.string,
    // = = Product Card Props 
    ProductThumbnail: PropTypes.string,
    ProductName: PropTypes.string,
    ProductPrice: PropTypes.string,
    ProductStoreLocation: PropTypes.string,
    // = = Tutorial Card Props
    TutorialName: PropTypes.string,
    TutorialBgImage: PropTypes.string,
    // = = Category Props
    CategoryImage: PropTypes.string,
    CategoryName: PropTypes.string,
  };

  // checkIndexIsEven (n) {
  //   return n % 2 == 0;
  // }

  render() {
    const id = this.props.id;
    let component;
    let element;
    // type 1 Card Product
    console.log('cekkkk', this.props.ProductThumbnail);

    if (this.props.type == 'CardProduct') {
      component = (
        <View style={[page.CardProduct]}>
          <View style={Styles.PlaceholderWrapper}>
            {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 160}]} /></SkeletonPlaceholder></FadeInView> */}
            <Image
              source={{uri: this.props.ProductThumbnail}}
              style={[
                page.ProductPhotoWrapper,
                {position: 'relative'},
              ]}></Image>
          </View>

          <View
            style={[
              Styles.PlaceholderWrapper,
              {marginHorizontal: 8, paddingVertical: 6},
            ]}>
            {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 20*2}]} /></SkeletonPlaceholder></FadeInView> */}
            <Text numberOfLines={2} style={[page.ProductName]}>
              {this.props.ProductName}
            </Text>
          </View>
          <View style={[Styles.PlaceholderWrapper, {marginHorizontal: 8}]}>
            {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 20, paddingTop: 6}]} /></SkeletonPlaceholder></FadeInView> */}
            <Text numberOfLines={1} style={[page.ProductPrice]}>
              {this.props.ProductPrice}
            </Text>
          </View>

        <View style={[Styles.PlaceholderWrapper, {marginHorizontal: 8, paddingVertical: 6,}]}>
          {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 20*2}]} /></SkeletonPlaceholder></FadeInView> */}
          <Text numberOfLines={2} style={[page.ProductName]}>{this.props.ProductName}</Text>
        </View>
        <View style={[Styles.PlaceholderWrapper, {marginHorizontal: 8}]}>
          {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 20, paddingTop: 6}]} /></SkeletonPlaceholder></FadeInView> */}
          <Text numberOfLines={1} style={[page.ProductPrice]}>Rp. {this.props.ProductPrice}</Text>
        </View>
        
        {/* <Image 
          source={require('../images/COD-icon.png')} 
          style={[page.ProductCODstatus]} /> */}
        <View style={[page.ProductStoreLocation]}>
          <Image 
            source={require('../assets/image/location_pin.png')} 
            style={[page.ProductStoreLocationIcon]} 
          />
          <View style={[Styles.PlaceholderWrapper, {flex: 1, marginRight: 0, paddingRight: 0, justifyContent: 'center'}]}>
            {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 18}]} /></SkeletonPlaceholder></FadeInView> */}
            <Text numberOfLines={1} style={[page.ProductStoreLocationText]}>{this.props.ProductStoreLocation}</Text>
          </View>
          
        </View>
      </View>
      )
      // - - - - -
      if (id -1 == 0) {
        element = 
        <View style={[{marginLeft: 0, marginRight: 0}]}>{component}</View>
      } else {
        element =
        <View style={[{marginleft: 0}]}>{component}</View>
      }
      // if (id % 2 == 0) {
      //   element = 
      //   <Pressable style={[{marginLeft: 6}]}>{component}</Pressable>
      // } else {
      //   element =
      //   <Pressable style={[{marginRight: 6}]}>{component}</Pressable>
      // }
    }
    // type 2 Card Tutorial
    else if (this.props.type == 'CardTutorial') {
      component = 
      <View style={[page.CardTutorial]}>
        {/* <FadeInView>
          <SkeletonPlaceholder>
            <View style={{width: '100%', height: 85}} />
          </SkeletonPlaceholder>
        </FadeInView> */}
          <Image
            source={this.props.TutorialBgImage}
            style={[page.TutorialBgImage]}>
            <View style={[page.TutorialBgImageOverlay]}>
              <Text numberOfLines={2} style={[page.TutorialName]}>
                {this.props.TutorialName}
              </Text>
            </View>
          </Image>
        </View>
      
      // - - - - -
      if (id -1 == 0) {
        element = 
        <View style={[{marginLeft: 0, marginRight: 0}]}>{component}</View>
      } else {
        element =
        <View style={[{marginleft: 0}]}>{component}</View>
      }
    }
    // type 3 Card Banner
    else if (this.props.type == 'CardBanner') {
      element = (
        <View style={[page.CardBanner]}>
          <Image
            source={this.props.BannerImage}
            style={[page.BannerImage]}></Image>
        </View>
      );
    }
    // type 4 Card Category Big
    else if (this.props.type == 'CardCategoryBig') {
      element = (
        <View style={[page.CardCategoryBig]}>
          <Image source={this.props.CategoryImage} style={[page.CategoryImage]}>
            <View style={[page.CategoryImageOverlay]}>
              <Text numberOfLines={2} style={[page.CategoryName]}>
                {this.props.CategoryName}
              </Text>
            </View>
          </Image>
        </View>
      );
    }
    // type 5 Card Image Slider
    else if (this.props.type == 'CardImageSlider') {
      element = (
        <View style={Styles.PlaceholderWrapper}>
          {/* <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: 400}]} /></SkeletonPlaceholder></FadeInView> */}
          <View style={[page.CardImageSlider]}>
            <Image
              source={this.props.SliderImage}
              style={[page.SliderImage]}></Image>
          </View>
        </View>
      )
    }
    // type 6 Card Product Small
    if (this.props.type == 'CardProductSmall') {
      element = (
        <View style={Styles.PlaceholderWrapper}>
          {/* <FadeInView><SkeletonPlaceholder><View style={[{width: width/5+2, aspectRatio: 1,}]} /></SkeletonPlaceholder></FadeInView> */}
          <View style={[page.CardProductSmall]}>
            <Image
              source={{uri: this.props.ProductThumbnail}}
              style={[page.ProductSmallThumbnail]}></Image>
          </View>
        </View>
      // </View>
      )
      // - - - - -      
    }
    // type 7 Card Category
    if (this.props.type == 'CardCategoryMedium') {
      element = 
      <View style={[page.CardCategoryMedium]}>
          <View style={[Styles.PlaceholderWrapper, {width: '100%'}]}>
            {/* <View style={{position: 'absolute', width: '100%', height: '100%', elevation: 2}}>
              <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView>
            </View> */}
            <Image
              source={this.props.CategoryImage}
              style={[page.CategoryMediumThumbnail]}></Image>
          </View>

          <View style={[Styles.PlaceholderWrapper, {marginTop: 6, width: '100%'}]}>
            {/* <View style={{position: 'absolute', width: '100%', height: '100%', zIndex: 1}}>
              <FadeInView><SkeletonPlaceholder><View style={[{width: '100%', height: '100%'}]} /></SkeletonPlaceholder></FadeInView>
            </View> */}
            <Text numberOfLines={2} style={[page.CategoryMediumName, {paddingBottom: 6}]}>{this.props.CategoryName}</Text>
          </View>
          
      </View>
      // - - - - -      
    }
    
    return element
  }
}

export default Card;

const page = StyleSheet.create({

  // Produk row card sizing & style
  CardProduct: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 8,
    overflow: 'hidden',
    height: 285,
    flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  ProductPhotoWrapper: {
    position: 'absolute',
    width: '100%',
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
    zIndex: -1,
  },
  ProductPhoto: {
    // position: 'absolute',
    // left: 24,
    // top: 32,
    height: 50,
    width: 50,
  },
  ProductName: {
    fontFamily: 'NotoSans-Regular',
    // paddingHorizontal: 8,
    // paddingVertical: 6,
    fontSize: Fonts.size_16,
    color: Colors.textGreyDark,
    lineHeight: 18,
    width: '100%',
    marginTop: 6,
  },
  ProductPrice: {
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_18,
    color: Colors.textGreyDark,
    lineHeight: 20,
    width: '100%',
  },
  ProductStoreLocation: {
    position: 'absolute',
    right: 8,
    left: 2,
    bottom: 6,
    paddingLeft: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ProductStoreLocationIcon: {
    height: 12,
    width: 9,
    marginRight: 5,
  },
  ProductStoreLocationText: {
    fontSize: Fonts.size_14,
    color: Colors.textGrey,
    letterSpacing: .4,
    textTransform: 'uppercase',
    flex: 1,
  },
  // - = - = - END - = - = -

  // Tutorial card
  CardTutorial: {
    position: 'relative',
    width: Dimensions.get('screen').width * 0.7,
    backgroundColor: '#fff',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 8,
    overflow: 'hidden',
    height: 85,
    // flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  TutorialBgImage: {
    position: 'relative',
    width: '100%',
    height: 85,
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  TutorialBgImageOverlay: {
    backgroundColor: 'rgba(15, 15, 15, 0.65)',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  TutorialName: {
    fontFamily: 'NotoSans-Bold',
    paddingHorizontal: 12,
    paddingVertical: 6,
    fontSize: Fonts.size_20,
    color: '#fff',
    lineHeight: 28,
  },
  // - = - = - END - = - = -

  // Banner card
  CardBanner: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    overflow: 'hidden',
    height: 225,
  },
  BannerImage: {
    position: 'relative',
    width: '100%',
    height: 225,
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  // - = - = - END - = - = -

  // Category big
  CardCategoryBig: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#ddd',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 8,
    overflow: 'hidden',
    height: 205,
    // flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  CategoryImage: {
    position: 'relative',
    width: '100%',
    height: 205,
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  CategoryImageOverlay: {
    backgroundColor: 'rgba(15, 15, 15, 0.65)',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  CategoryName: {
    fontFamily: 'NotoSans-Bold',
    paddingHorizontal: 12,
    paddingVertical: 6,
    fontSize: Fonts.size_20,
    color: '#fff',
    lineHeight: 28,
  },
  // - = - = - END - = - = -

  // Category medium
  CardCategoryMedium: {
    position: 'relative',
    width: '100%',
    // backgroundColor: '#ddd',
    overflow: 'hidden',
    alignItems: 'center',
    // padding: 6,
  },
  CategoryMediumThumbnail: {
    width: '100%',
    aspectRatio: 1,
    resizeMode: "cover",
    elevation: 1,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
  },
  CategoryMediumName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_18,
    textAlign: 'center',
    color: Colors.textGreyDark,
  },

  // Image Slider card
  CardImageSlider: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    overflow: 'hidden',
    height: 400,
  },
  SliderImage: {
    position: 'relative',
    width: '100%',
    height: 400,
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  // - = - = - END - = - = -

  // Produk Small
  CardProductSmall: {
    width: width/5,
    aspectRatio: 1,
    position: 'relative',
    overflow: 'hidden',
  },
  ProductSmallThumbnail: {
    position: 'relative',
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    resizeMode: "cover",
  },
  // Produk Small - - - END
});


