import React from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  ScrollView,
  View,
  Pressable,
  Text,
  StyleSheet,
} from 'react-native';
import {Colors, Fonts} from '../Themes';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export class ProdukOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeOption: this.props.options[0],
    };
  }
  updateActiveOption = activeOption => {
    this.setState({
      activeOption,
    });
  };

  render() {
    let component;
    let element;

    return (
      <ScrollView
        style={[styles.HorizontalScroll, {marginBottom: 24}]}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        {this.props.options.map((option, index) => (
          <Pressable
            style={[
              styles.containerDefault,
              this.state.activeOption === option
                ? styles.containerProdukSelected
                : styles.containerProdukOption,
              {
                marginRight: 12,
                marginLeft: index - 1 == -1 ? 12 : 0,
              },
            ]}
            onPress={() => {
              this.props.onChange(option);
              this.updateActiveOption(option);
            }}>
            <Text
              style={[
                styles.textDefault,
                this.state.activeOption === option
                  ? styles.textProdukSelected
                  : styles.textProdukOption,
              ]}>
              {option}
            </Text>
          </Pressable>
        ))}
      </ScrollView>
    );
  }
}

export class FilterOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeOption: this.props.options[0],
    };
  }
  updateActiveOption = activeOption => {
    this.setState({
      activeOption,
    });
  };

  render() {
    let {width} = Dimensions.get('window');

    return (
      <View
        style={{
          marginTop: 12,
          marginBottom: 0,
          marginHorizontal: 12,
          flexWrap: 'wrap',
          flexDirection: 'row',
          // backgroundColor: '#666'
        }}>
        {this.props.options.map((option, index) => (
          <Pressable
            style={[
              styles.containerDefault,
              styles.containerFilterOptionButton,
              this.state.activeOption === option
                ? styles.containerFilterSelected
                : styles.containerFilterOption,
              {
                width: width / 2 - 16,
                marginTop: index - 1 == -1 || index - 1 == 0 ? 0 : 6,
                marginRight: index % 2 == 0 ? 3 : 0,
                marginLeft: index % 2 == 0 ? 0 : 3,
                // backgroundColor: index % 2 == 0 ? '#ddd' : '#000',
              },
            ]}
            onPress={() => {
              this.props.onChange(option);
              this.updateActiveOption(option);
            }}>
            <Text
              numberOfLines={1}
              style={[
                styles.textDefault,
                styles.containerFilterOptionButtonText,
                {},
              ]}>
              {option}
            </Text>
            <View
              style={{
                position: 'absolute',
                right: 4,
                opacity: this.state.activeOption === option ? 1 : 0,
              }}>
              <MaterialCommunityIcons
                name="check"
                color={Colors.blueBold}
                size={22}
              />
            </View>
          </Pressable>
        ))}
      </View>
    );
  }
}

export class PaymentOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeOption: this.props.options[0],
    };
  }
  updateActiveOption = activeOption => {
    this.setState({
      activeOption,
    });
  };

  render() {
    let component;
    let element;

    return (
      <View style={{marginBottom: 0}}>
        {this.props.options.map((option, index) => (
          <Pressable
            style={[styles.containerDefault, styles.containerPaymentOption]}
            onPress={() => {
              this.props.onChange(option);
              this.updateActiveOption(option);
            }}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 22,
                width: 22,
                borderRadius: 22,
                borderColor: Colors.borderGrey,
                borderWidth: 1,
              }}>
              <MaterialCommunityIcons
                name={
                  this.state.activeOption === option
                    ? 'circle'
                    : 'circle-outline'
                }
                color={
                  this.state.activeOption === option
                    ? Colors.blueBold
                    : Colors.white
                }
                size={20}
              />
            </View>
            <Text style={[styles.textDefault, styles.textPaymentOption]}>
              {option.name}
            </Text>
          </Pressable>
        ))}
      </View>
    );
  }
}

export class DeliveryOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeOption: this.props.options[0],
    };
  }
  updateActiveOption = activeOption => {
    this.setState({
      activeOption,
    });
  };

  render() {
    return (
      <View style={{marginBottom: 0}}>
        {this.props.options.map((option, index) => (
          <Pressable
            style={
              ([styles.containerDefault, styles.containerPaymentOption],
              {
                // backgroundColor: '#ddd',
                paddingHorizontal: 24,
                paddingVertical: 12,
                borderTopColor:
                  index - 1 == -1 ? Colors.borderGrey : 'rgba(0,0,0,0)',
                borderTopWidth: index - 1 == -1 ? 0.4 : 0,
                borderBottomColor: Colors.borderGrey,
                borderBottomWidth: 0.4,
                flexDirection: 'row',
                alignItems: 'center',
              })
            }
            onPress={() => {
              this.props.onChange(option);
              this.updateActiveOption(option);
            }}>
            <Text
              style={[
                styles.textDefault,
                styles.textPaymentOption,
                {paddingLeft: 0},
              ]}>
              {`${option.service} , ${
                option.etd === null ? '0 Hari' : option.etd + ' Hari'
              }`}
            </Text>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 22,
                width: 22,
                borderRadius: 22,
                borderColor:
                  this.state.activeOption === option
                    ? 'rgba(0,0,0,0)'
                    : 'rgba(0,0,0,0)',
                borderWidth: 0.4,
                marginLeft: 'auto',
              }}>
              <MaterialCommunityIcons
                name={
                  this.state.activeOption === option ? 'check' : 'check-off'
                }
                color={
                  this.state.activeOption === option
                    ? Colors.textGreen
                    : 'rgba(0,0,0,0)'
                }
                size={20}
              />
            </View>
          </Pressable>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerDefault: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 4,
    backgroundColor: Colors.textGrey,
  },
  textDefault: {
    fontSize: Fonts.size_16,
    fontFamily: 'NotoSans-Bold',
    color: '#fff',
    letterSpacing: 0.3,
  },
  containerProdukOption: {
    backgroundColor: Colors.white,
    borderColor: Colors.textGrey,
    borderWidth: .8,
    borderStyle: 'solid',
    borderRadius: 48,
  },
  textProdukOption: {
    color: Colors.textGrey,
  },
  containerProdukSelected: {
    backgroundColor: Colors.blueBold,
    borderColor: Colors.blueBold,
    borderWidth: .8,
    borderStyle: 'solid',
    borderRadius: 48,
  },
  textProdukSelected: {
    color: Colors.white,
  },

  containerFilterOptionButton: {
    paddingHorizontal: 0,
    backgroundColor: '#f4f4f4',
    borderColor: 'transparent',
    borderWidth: 1,
    paddingVertical: 6,
    paddingHorizontal: 6,
  },
  containerFilterOptionButtonText: {
    color: Colors.textGreyDark,
    fontSize: Fonts.size_16,
    maxWidth: '75%',
    fontFamily: 'NotoSans-Regular',
  },
  containerFilterSelected: {
    backgroundColor: Colors.white,
    borderColor: Colors.blueBold,
    // borderWidth: 1,
  },
  textFilterSelected: {
    color: Colors.blueBold,
  },

  containerPaymentOption: {
    justifyContent: 'flex-start',
    paddingHorizontal: 0,
    backgroundColor: 'transparent',
  },
  textPaymentOption: {
    color: Colors.textGreyDark,
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    paddingLeft: 12,
    letterSpacing: 1,
  },
});
