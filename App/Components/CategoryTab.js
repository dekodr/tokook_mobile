import * as React from 'react';
import {StyleSheet, Dimensions, Text, View} from 'react-native';
import {
  TabBarVertical,
  TabViewVertical,
  SceneMap,
  type Route,
  type NavigationState,
} from 'react-native-vertical-tab-view';
import {Colors, Fonts} from '../Themes';
// import Men from '../Containers/MarketplaceModule/Category/Sections/MenSection';
// import Women from '../Containers/MarketplaceModule/Category/Sections/WomenSection';

type State = NavigationState<
  Route<{
    key: string,
    title: string,
  }>,
>;

const initialLayout = {
  height: Dimensions.get('window').height,
  width: Dimensions.get('window').width,
};

export default class VerticalTabBarTextExample extends React.Component<
  *,
  State,
> {
  static title = 'Scrollable left vertical bar';
  static backgroundColor = '#fff';
  static appbarElevation = 0;

  state = {
    index: 0,
    routes: [{key: 'Men', title: 'Pria'}, {key: 'Women', title: 'Wanita'}],
  };

  _handleIndexChange = index =>
    this.setState({
      index,
    });

  _renderTabBar = props => (
    <TabBarVertical
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    // Men: Men,
    // Women: Women,
  });

  render() {
    return (
      <TabViewVertical
        style={[styles.container, this.props.style]}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabbar: {
    height: Dimensions.get('window').height,
    backgroundColor: '#f3f3f3',
    borderRightColor: '#c9c6c6',
    borderRightWidth: 1,
    borderStyle: 'solid',
  },
  tab: {
    marginTop: -1,
    width: 100,
    height: 100,
    padding: 0,
    borderColor: '#c9c6c6',
    borderWidth: 1,
    borderStyle: 'solid',
    // alignItems: 'flex-start',
    // justifyContent: 'flex-start',
  },
  indicator: {
    backgroundColor: '#fff',
    borderLeftColor: Colors.blueBold,
    borderLeftWidth: 8,
    height: 100,
  },
  label: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 14,
    color: Colors.textGreyDark,
    letterSpacing: 1,
  },
});
