import React from 'react';
import {ActivityIndicator} from 'react-native';

let Loading = ({size, color, style}) => (
  <ActivityIndicator size={size} style={style} color={color} />
);

module.exports = {
  Loading,
};
