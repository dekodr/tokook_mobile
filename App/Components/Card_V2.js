import React, { useState, useEffect } from 'react';
// import useCounter from './useCounter'
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Pressable, Image, TextInput, ImageBackground, Dimensions, Animated } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Colors, Fonts} from '../Themes';
import Styles from '../Containers/MarketplaceModule/Intro/styles';

let {height, width} = Dimensions.get('window');
let component;
let element;

function TutorialCard(item) {
  component = 
  <View style={[page.CardTutorial]}>
    <ImageBackground source={item.image} style={[page.TutorialBgImage]}>
      <View style={[page.TutorialBgImageOverlay]}>
        <Text numberOfLines={2} style={[page.TutorialName]}>{item.title}</Text>
      </View>
    </ImageBackground>
  </View>

  return (
    <Pressable 
      onPress={item.onPress}
      style={[item.styles, {
        marginRight: 12,
        marginLeft: item.id == 0 ? 12 : 0,
        // opacity: , 
        // paddingRight: 0
      }]}>
      {component}
    </Pressable>
  )
} 

function ProductCard(item) {
  component = 
  <View style={[page.CardProduct]}>
    <ImageBackground 
      source={item.image} 
      style={[page.ProductPhotoWrapper, 
      {position: 'relative'}]}>  
    </ImageBackground>

    <Text 
      numberOfLines={2} 
      style={[page.ProductName, {
        paddingVertical: 6,
        marginHorizontal: 8
      }]}
    >{item.title}
    </Text>
    <Text 
      numberOfLines={1} 
      style={[page.ProductPrice, {
        marginHorizontal: 8
      }]}
    >Rp. {item.price}
    </Text>
    
    {/* <Image 
      source={require('../images/COD-icon.png')} 
      style={[page.ProductCODstatus]} /> */}
    <View style={[page.ProductStoreLocation]}>
      <Image 
        source={require('../assets/image/location_pin.png')} 
        style={[page.ProductStoreLocationIcon]} 
      />
      <Text 
        numberOfLines={1} 
        style={[page.ProductStoreLocationText, {
          flex: 1,
          marginRight: 0,
          paddingRight: 0,
        }]}
      >{item.location}
      </Text>
    </View>
  </View>

  return (
    <Pressable 
      onPress={item.onPress}
      style={[item.styles, {
        marginRight: 12,
        marginLeft: item.id == 0 ? 12 : 0,
        // opacity: , 
        // paddingRight: 0
      }]}>
      {component}
    </Pressable>
  )
}

function SectionCategory (item) {
  component = 
  <Pressable 
    onPress={item.onPress}
    style={[page.CardCategoryMedium, item.styles]}>
    <ImageBackground 
      source={item.image} 
      style={[page.CategoryMediumThumbnail]}>
    </ImageBackground>
    <Text 
      numberOfLines={2} 
      style={[page.CategoryMediumName, {
        paddingBottom: 6,
        marginTop: 6,
        width: '100%',
      }]}
    >{item.title}
    </Text>
  </Pressable>

  return component;
}

function SupplierCategory (item) {
  component = 
  <Pressable 
    onPress={item.onPress}
    style={[page.CardCategoryBig, item.styles]}>
    <ImageBackground source={item.image} style={[page.CategoryImage]}>
      <View style={[page.CategoryImageOverlay]}>
        <Text numberOfLines={2} style={[page.CategoryName]}>{item.title}</Text>
      </View>
    </ImageBackground>
  </Pressable>

  return component;
}

export {TutorialCard, ProductCard, SectionCategory, SupplierCategory};

const page = StyleSheet.create({

  // Produk row card sizing & style
  CardProduct: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 8,
    overflow: 'hidden',
    height: 285,
    flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  ProductPhotoWrapper: {
    position: 'absolute',
    width: '100%',
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
    zIndex: -1,
  },
  ProductPhoto: {
    // position: 'absolute',
    // left: 24,
    // top: 32,
    height: 50,
    width: 50,
  },
  ProductName: {
    fontFamily: 'NotoSans-Regular',
    // paddingHorizontal: 8,
    // paddingVertical: 6,
    fontSize: Fonts.size_16,
    color: Colors.textGreyDark,
    lineHeight: 18,
    // width: '100%',
    marginTop: 6,
  },
  ProductPrice: {
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_18,
    color: Colors.textGreyDark,
    lineHeight: 20,
    width: '100%',
  },
  ProductStoreLocation: {
    position: 'absolute',
    right: 8,
    left: 2,
    bottom: 12,
    paddingLeft: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ProductStoreLocationIcon: {
    height: 12,
    width: 9,
    marginRight: 5,
  },
  ProductStoreLocationText: {
    fontSize: Fonts.size_14+1,
    color: Colors.textGrey,
    letterSpacing: .8,
    // textTransform: 'uppercase',
    flex: 1,
  },
  // - = - = - END - = - = -

  // Tutorial card
  CardTutorial: {
    position: 'relative',
    width: '100%',
    height: '100%',
    // backgroundColor: '#fff',
    // elevation: 3,
    // borderColor: "#ccc",
    // borderWidth: 1,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.05,
    // shadowRadius: 2,  
    borderRadius: 7,
    overflow: 'hidden',
    // height: 85,
    // flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  TutorialBgImage: {
    position: 'relative',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  TutorialBgImageOverlay: {
    backgroundColor: 'rgba(0,0,0, 0.5)',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
  TutorialName: {
    fontFamily: 'NotoSans-Bold',
    padding: 20,
    fontSize: Fonts.size_18,
    letterSpacing: 0.9,
    color: '#fff',
  },
  // - = - = - END - = - = -

  // Banner card
  CardBanner: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    overflow: 'hidden',
    height: 225,
  },
  BannerImage: {
    position: 'relative',
    width: '100%',
    height: 225,
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  // - = - = - END - = - = -

  // Category big
  CardCategoryBig: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#ddd',
    elevation: 3,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
    borderRadius: 8,
    overflow: 'hidden',
    height: 205,
    // flexDirection: 'column',
    // width: 145,
    // height: 145,
  },
  CategoryImage: {
    position: 'relative',
    width: '100%',
    height: 205,
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  CategoryImageOverlay: {
    backgroundColor: 'rgba(0,0,0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  CategoryName: {
    fontFamily: 'NotoSans-Bold',
    letterSpacing: .8,
    paddingHorizontal: 12,
    paddingVertical: 6,
    fontSize: Fonts.size_20,
    color: '#fff',
    // lineHeight: 28,
  },
  // - = - = - END - = - = -

  // Category medium
  CardCategoryMedium: {
    position: 'relative',
    width: '100%',
    // backgroundColor: '#ddd',
    overflow: 'hidden',
    alignItems: 'center',
    // padding: 6,
  },
  CategoryMediumThumbnail: {
    width: '100%',
    aspectRatio: 1,
    resizeMode: "cover",
    elevation: 1,
    borderColor: "#ccc",
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.05,
    shadowRadius: 2,  
  },
  CategoryMediumName: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_18,
    textAlign: 'center',
    color: Colors.textGreyDark,
  },

  // Image Slider card
  CardImageSlider: {
    position: 'relative',
    width: '100%',
    backgroundColor: '#fff',
    overflow: 'hidden',
    height: 400,
  },
  SliderImage: {
    position: 'relative',
    width: '100%',
    height: 400,
    backgroundColor: '#333',
    resizeMode: "cover",
  },
  // - = - = - END - = - = -

  // Produk Small
  CardProductSmall: {
    width: width/5,
    aspectRatio: 1,
    position: 'relative',
    overflow: 'hidden',
  },
  ProductSmallThumbnail: {
    position: 'relative',
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    resizeMode: "cover",
  },
  // Produk Small - - - END
});