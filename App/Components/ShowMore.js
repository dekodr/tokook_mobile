import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
} from 'react-native';
import ViewMoreText from 'react-native-view-more-text';
import {Colors, Fonts} from '../Themes';

class ShowMore extends React.Component {
  static propTypes = {};

  renderViewMore(onPress) {
    return (
      <Text style={page.ViewMoreLink} onPress={onPress}>
        Lihat semua
      </Text>
    );
  }

  renderViewLess(onPress) {
    return (
      <Text style={page.ViewMoreLink} onPress={onPress}>
        Lebih sedikit
      </Text>
    );
  }

  render() {
    let element;

    element = (
      <ViewMoreText
        numberOfLines={3}
        renderViewMore={this.renderViewMore}
        renderViewLess={this.renderViewLess}>
        <Text style={page.UpperInfoText}>
          Lorem ipsum dolor sit amet, in quo dolorum ponderum, nam veri molestie
          constituto eu. Eum enim tantas sadipscing ne, ut omnes malorum nostrum
          cum. Errem populo qui ne, ea ipsum antiopam definitionem eos.
        </Text>
      </ViewMoreText>
    );

    return element;
  }
}

export default ShowMore;

const page = StyleSheet.create({
  UpperInfoText: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    color: Colors.panther,
    lineHeight: 20,
    paddingBottom: 12,
  },
  ViewMoreLink: {
    fontFamily: 'NotoSans-Regular',
    fontSize: Fonts.size_16,
    color: Colors.blueBold,
    lineHeight: 20,
    marginTop: 12,
  },
});
