import {Dimensions, StyleSheet} from 'react-native';
import Colors from './../Themes/Colors';

const Styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  containerWidth: {
    width: Dimensions.get('window').width,
  },
  flexOne: {
    flex: 1,
  },
  flexTwo: {
    flex: 2,
  },
  flexThree: {
    flex: 3,
  },
  paddingFive: {
    padding: 5,
  },
  paddingTen: {
    padding: 10,
  },
  paddingFifteen: {
    padding: 15,
  },
  paddingTwenty: {
    padding: 20,
  },
  paddingThirty: {
    padding: 30,
  },
  paddingleftThirty: {
    paddingLeft: 30,
  },
  paddingTopFive: {
    padding: 5,
  },
  paddingTopTen: {
    padding: 10,
  },
  paddingTopFifteen: {
    padding: 15,
  },
  paddingTopTwenty: {
    padding: 20,
  },
  paddingTopThirty: {
    padding: 30,
  },
  pxFive: {
    paddingHorizontal: 5,
  },
  marginFive: {
    margin: 5,
  },
  marginTen: {
    margin: 10,
  },
  marginfifmarginfifteen: {
    margin: 15,
  },
  marginTwenty: {
    margin: 20,
  },
  marginThirty: {
    margin: 30,
  },
  marginTopFive: {
    marginTop: 5,
  },
  marginTopTen: {
    marginTop: 10,
  },
  marginTopFifteen: {
    marginTop: 15,
  },
  marginTopTwenty: {
    marginTop: 20,
  },
  marginTopThirty: {
    marginTop: 30,
  },
  marginBottomThree: {
    marginBottom: 3,
  },
  marginBottomFive: {
    marginBottom: 5,
  },
  marginBottomTen: {
    marginBottom: 10,
  },
  marginBottomFifteen: {
    marginBottom: 15,
  },
  marginBottomTwenty: {
    marginBottom: 20,
  },
  marginBottomThirty: {
    marginBottom: 30,
  },
  marginRightTen: {
    marginRight: 10,
  },
  marginLeftTen: {
    marginLeft: 10,
  },
  marginLeftFifteen: {
    marginLeft: 15,
  },
  marginHorizontalFive: {
    marginHorizontal: 5,
  },
  heightFive: {
    height: 5,
  },
  heightTwenty: {
    height: 20,
  },
  borderGrey: {
    borderWidth: 1,
    borderColor: Colors.grayDark,
  },
  borderTop: {
    borderTopWidth: 0.25,
    borderTopColor: Colors.grayDark,
  },
  borderBottom: {
    borderBottomWidth: 0.25,
    borderBottomColor: Colors.grayDark,
  },
  borderBottomDate: {
    borderBottomWidth: 0.25,
    borderBottomColor: Colors.grayDark,
    paddingHorizontal: 0,
    paddingVertical: 5,
  },
  flexRow: {
    flexDirection: 'row',
  },
  flexRowReverse: {
    flexDirection: 'row-reverse',
  },
  flexColumn: {
    flexDirection: 'column',
  },
  justifyStart: {
    justifyContent: 'flex-start',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  justifyEnd: {
    justifyContent: 'flex-end',
  },
  justifyspaceAround: {
    justifyContent: 'space-around',
  },
  justifyspaceBetween: {
    justifyContent: 'space-between',
  },
  alignItemsStart: {
    alignItems: 'flex-start',
  },
  alignItemsEnd: {
    alignItems: 'flex-end',
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  alignCenter: {
    alignSelf: 'center',
  },
  alignEnd: {
    alignSelf: 'flex-end',
  },

  //VIEW STYLE
  viewBgmainBlue: {
    backgroundColor: Colors.mainBlue,
  },
  viewTnCWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 55,
  },
  viewCreateMerchantSectionWrapper: {
    borderColor: '#c2c2c2',
    borderWidth: 1,
    borderRadius: 5,
    padding: 15,
    marginVertical: 10,
  },
  viewheaderDay: {
    marginTop: 20,
  },
  viewCheck: {
    marginTop: 5,
  },
  viewTokoStep2WrapCategory: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 8,
  },
  viewInputWrapper: {
    marginVertical: 10,
  },

  //BUTTON STYLE
  btnPrimary: {
    width: '100%',
    justifyContent: 'center',
    backgroundColor: Colors.mainBlue,
    borderRadius: 30,
    marginVertical: 10,
  },
  btnOutline: {
    width: '100%',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    borderColor: Colors.mainBlue,
    borderWidth: 1,
    borderRadius: 30,
    marginVertical: 10,
  },
  btnImagePicker: {
    width: '100%',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    borderColor: Colors.mainBlue,
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
  },
  btnDual: {
    marginHorizontal: 5,
    width: '45%',
    justifyContent: 'center',
    borderRadius: 30,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: Colors.mainBlue,
  },
  //TEXT STYLE
  headerlistProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#636363',
  },
  amountlistProduct: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
    marginTop: 3,
    marginBottom: 3,
  },
  categoryitemnameProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#1a69d5',
  },
  viewstatusProduct: {
    paddingVertical: 3,
    paddingHorizontal: 7,
    marginRight: 8.5,
    borderColor: '#009942',
    borderWidth: 1,
    borderRadius: 3,
  },
  dropdowntxtList: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
  },
  statusProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
  },
  headerListToko: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#4a4a4a',
    letterSpacing: 2.29,
    alignContent: 'center',
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 20,
  },
  txtListAdmin: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: 'white',
    letterSpacing: 1.14,
  },
  txtList: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 1.14,
  },
  //TEXT STYLE

  txtverifWA: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 15,
    letterSpacing: 1,
    color: '#4a4a4a',
  },
  headerlistProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#636363',
  },
  amountlistProduct: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
    marginTop: 3,
    marginBottom: 3,
  },
  categoryitemnameProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#1a69d5',
  },
  viewstatusProduct: {
    paddingVertical: 3,
    paddingHorizontal: 7,
    marginRight: 8.5,
    borderColor: '#009942',
    borderWidth: 1,
    borderRadius: 3,
  },
  dropdowntxtList: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: '#4a4a4a',
  },
  statusProduct: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
  },
  //KATEGORY
  ketCategory: {
    fontFamily: 'NotoSans-Regular',
  },
  headerListToko: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 12,
    color: '#4a4a4a',
    letterSpacing: 2.29,
    alignContent: 'center',
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 20,
  },
  txtListAdmin: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    color: 'white',
    letterSpacing: 1.14,
  },
  txtList: {
    fontFamily: 'NotoSans-Regular',
    fontSize: 12,
    letterSpacing: 1.14,
  },
  letterSpacingHalf: {
    letterSpacing: 0.5,
  },
  letterSpacingOne: {
    letterSpacing: 1,
  },
  txtCreateMerchant: {
    fontSize: 18,
    fontWeight: '600',
    marginTop: 10,
    marginBottom: 5,
  },
  txtcategoryNotification: {
    fontSize: 13,
    color: Colors.graygray,
    paddingVertical: 5,
    letterSpacing: 0.93,
  },
  lineHeightTwenty: {
    lineHeight: 20,
  },
  txtCreateMerchantBtn: {
    color: '#fff',
    fontFamily: 'NotoSans-SemiBold',
    fontSize: 14,
    letterSpacing: 1.33,
  },
  textInputIcon: {
    width: 25,
    height: 25,
  },
  txtInputCreateMerchant: {
    color: '#000',
    flex: 1,
  },
  txtInputBordered: {
    color: '#000',
    paddingHorizontal: 10,
    paddingVertical: 5,
    flex: 1,
  },
  txtInputWithIcon: {
    marginTop: 15,
    marginBottom: 5,
    // paddingVertical: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  txtInputTitle: {
    color: Colors.mainBlue,
    marginVertical: 5,
  },
  txtInputDesc: {
    fontSize: 13,
    color: Colors.grayWhiteDark,
  },
  txtInputMerchantErrors: {
    color: Colors.redDark,
    marginTop: 0,
    fontSize: 12,
    fontFamily: 'NotoSans-Italic',
  },
  txtsubcategoryNotification: {
    fontSize: 10,
    letterSpacing: 0.78,
    color: Colors.grayWhite,
    fontFamily: 'NotoSans-Regular',
    textTransform: 'capitalize',
  },
  txtBold: {
    fontWeight: 'bold',
  },
  fsThirteen: {
    fontSize: 13,
  },
  fsFifteen: {
    fontSize: 15,
  },
  txtmainBlue: {
    color: Colors.mainBlue,
  },

  //IMAGE STYLE
  imgaddNotificationToTransaction: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },

  //ICON STYLE
  iconLeftModal: {
    padding: 10,
    marginLeft: 5,
  },
});

export default Styles;
