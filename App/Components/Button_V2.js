import React from 'react';
import PropTypes from 'prop-types';
import {Pressable, Text, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../Themes';


// --------- useCounter --------- EXAMPLE PURPOSE
// function useCounter(defaultValue){
//   const [state, setState] = useState(defaultValue);
//   const handleState = () => {
//     setState(currentState => currentState + 1)
//   }

//   const handleTriple = () => {
//     handleState();
//     handleState();
//     handleState();
//   };

//   return [
//     state,
//     handleState,
//     handleTriple
//   ]
// }
// --------- useCounter --------- EXAMPLE PURPOSE - - - END

function Button(item) {
  // --------- states --------- EXAMPLE PURPOSE
  // const [state, setState] = useState({
  //   subscribe: false,
  // });

  // const [like,handleLike, handleTripleLike ] = useCounter(0)
  // const [dislike,handleDislike] = useCounter(0)


  // const handleSubscribe = () => {
  //   setState({
  //     ...state,
  //     subscribe: !state.subscribe
  //   })
  // }
  // --------- states --------- EXAMPLE PURPOSE - - - END

  // button subscribe
  // <ButtonHooks 
  //   onPress={handleSubscribe}
  //   text={state.subscribe ? "Subscribe" : "Unsubscribe" }
  //   style={{
  //     backgroundColor: state.subscribe ? Colors.blueBold : '#f4f4f4',
  //   }}
  //   textStyle={{
  //     color: state.subscribe ? '#fff' : Colors.textGrey,
  //   }}
  // />
  // display subscribe state
  // <Text>{JSON.stringify(state.subscribe)}</Text>

  return (
    <Pressable
      onPress={item.onPress}
      style={[styles.containerDefault, item.style]} 
    >
      <Text style={[styles.containerDefaultText, item.textStyle]}>{item.text}</Text>
    </Pressable>
  )
}

export default Button;

const styles = StyleSheet.create({
  containerDefault: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 14,
    paddingHorizontal: 18,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: '#f4f4f4',
    backgroundColor: '#f4f4f4',
  },
  containerDefaultText: {
    fontSize: Fonts.size_18,
    fontFamily: 'NotoSans-Bold',
    color: Colors.textGreyDark,
    letterSpacing: 0.3,
  },
})