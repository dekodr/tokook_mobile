import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Pressable,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Colors, Fonts} from '../Themes/';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const PRIMARY_COLOR = Colors.blueBold;
const SECONDARY_COLOR = Colors.whiteBlue;

const getStyles = ({
  size,
  theme,
  login,
  logout,
  fullwidth,
  rounded,
  addToSomething,
}) => {
  const containerStyles = [styles.containerDefault];
  const textStyles = [styles.textDefault];

  // theme control
  if (theme === 'white') {
    containerStyles.push(styles.containerWhite);
    textStyles.push(styles.textWhite);
  } else if (theme === 'whiteblue') {
    containerStyles.push(styles.containerWhite);
    textStyles.push({color: Colors.Primary});
  } else if (theme === 'secondary') {
    containerStyles.push(styles.containerSecondary);
    textStyles.push(styles.textSecondary);
  } else {
    containerStyles.push(styles.containerPrimary);
    textStyles.push(styles.textPrimary);
  }
  // theme control - - - END

  // size control
  if (size === 'large') {
    containerStyles.push(styles.containerLarge);
    textStyles.push(styles.textLarge);
  } else if (size === 'small') {
    containerStyles.push(styles.containerSmall);
    textStyles.push(styles.textSmall);
  } else if (size === 'medium') {
    containerStyles.push(styles.containerMedium);
    textStyles.push(styles.textMedium);
  }
  // size control - - - END

  if (login) {
    containerStyles.push(styles.containerLogin);
    textStyles.push(styles.textLogin);
  }

  if (logout) {
    containerStyles.push(styles.containerLogout);
    textStyles.push(styles.textLogout);
  }

  if (fullwidth) {
    containerStyles.push(styles.containerFullwidth);
    textStyles.push(styles.textFullwidth);
  }

  if (rounded) {
    containerStyles.push(styles.containerRounded);
    textStyles.push(styles.textRounded);
  }

  if (addToSomething) {
    containerStyles.push(styles.containeraddToSomething);
    textStyles.push(styles.textaddToSomething);
  }

  return {containerStyles, textStyles};
};

class ButtonIcon extends React.Component {
  static propTypes = {
    icon: PropTypes.string.isRequired,
    text: PropTypes.string,
    textStyle: PropTypes.string,
    size: PropTypes.number,
    color: PropTypes.string,
    outline: PropTypes.bool,
    login: PropTypes.bool,
    logout: PropTypes.bool,
    fullwidth: PropTypes.bool,
    rounded: PropTypes.bool,
    style: PropTypes.string,
    // size: PropTypes.oneOf(['small', 'default', 'large']),
    theme: PropTypes.oneOf(['primary', 'secondary']),
    onPress: PropTypes.string,
    AddtoSomething: PropTypes.bool,
    plusColor: PropTypes.string,
    IconLeft: PropTypes.bool,
    IconRight: PropTypes.bool,
  };

  static defaultProps = {
    // size: 'default',
    // theme: 'primary',
    // outline: false,
    login: false,
    AddtoSomething: false,
    IconRight: false,
    // fullwidth: false,
    // rounded: false,
  };

  render() {
    const {
      text,
      icon,
      size,
      onPress,
      color,
      style,
      textStyle,
      AddtoSomething,
      plusColor,
      iconRight,
      loadingText,
      ...rest
    } = this.props;
    const {textStyles, containerStyles} = getStyles({size, ...rest});
    const TextComponent = () => {
      if (!text == '') {
        if (iconRight == true) {
          return (
            <View>
              {loadingText ? (
                <ActivityIndicator color={'#fff'} />
              ) : (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text
                    style={[styles.textDefault, textStyle, {marginLeft: 4}]}>
                    {text}
                  </Text>
                  <MaterialCommunityIcons
                    name={icon}
                    color={color}
                    size={size}
                  />
                </View>
              )}
            </View>
          );
        } else {
          return (
            <View>
              {loadingText ? (
                <ActivityIndicator color={'#fff'} />
              ) : (
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <MaterialCommunityIcons
                    name={icon}
                    color={color}
                    size={size}
                  />
                  <Text
                    style={[styles.textDefault, textStyle, {marginLeft: 4}]}>
                    {text}
                  </Text>
                </View>
              )}
            </View>
          );
        }
      }
      return <MaterialCommunityIcons name={icon} color={color} size={size} />;
    };

    const AddToSomething = () => {
      if (AddtoSomething == true) {
        return (
          <View>
            {loadingText ? null : (
              <MaterialCommunityIcons name="plus" color={plusColor} size={28} />
            )}
          </View>
        );
      }
      return <Text style={{display: 'none'}}></Text>;
    };

    return (
      <Pressable onPress={onPress} style={([containerStyles], style)}>
        <AddToSomething />
        <TextComponent />
      </Pressable>
    );
  }
}

export default ButtonIcon;

const styles = StyleSheet.create({
  // Container Styles
  containerDefault: {
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 2,
    width: 48,
    height: 48,
  },
  containerWhite: {
    backgroundColor: '#fff',
    borderColor: '#fff',
    shadowColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1.8,
    // elevation: 2.3,
  },
  containerSecondary: {
    backgroundColor: SECONDARY_COLOR,
    borderColor: SECONDARY_COLOR,
    shadowColor: SECONDARY_COLOR,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    // elevation: 2.3,
  },
  containerPrimary: {
    backgroundColor: PRIMARY_COLOR,
    borderColor: PRIMARY_COLOR,
    shadowColor: PRIMARY_COLOR,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    // elevation: 6,
  },
  containerLarge: {
    paddingVertical: 24,
    width: '100%',
  },
  containerMedium: {
    paddingVertical: 16,
    width: '100%',
  },
  containerSmall: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  containerLogin: {
    height: 63,
    paddingHorizontal: 50,
  },
  containerLogout: {
    width: '100%',
    height: 63,
    paddingHorizontal: 50,
    borderColor: Colors.White,
    backgroundColor: Colors.White,
  },
  containerFullwidth: {
    width: '100%',
  },
  containerRounded: {
    borderRadius: 100,
  },

  // Text Styles
  textDefault: {
    fontSize: Fonts.size_16,
    fontFamily: 'NotoSans-Regular',
    color: '#4a4a4a',
    letterSpacing: 0.3,
  },
  textPrimary: {},
  textSecondary: {},
  textWhite: {
    color: Colors.Black,
  },
  textLarge: {
    fontSize: Fonts.size_20,
  },
  textSmall: {
    fontSize: Fonts.size_14,
  },
  textLogin: {
    letterSpacing: 0,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_22,
  },
  textLogout: {
    letterSpacing: 0,
    fontFamily: 'NotoSans-Bold',
    fontSize: Fonts.size_22,
    color: Colors.PrimaryDark,
  },
});
