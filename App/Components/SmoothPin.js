import React from 'react';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';

const PinVerify = ({
  autoFocus,
  cellStyle,
  cellStyleFocused,
  textStyle,
  textStyleFocused,
  value,
  onTextChange,
  codeLength,
  editable,
  onFulfill,
}) => (
  <SmoothPinCodeInput
    onFulfill={onFulfill}
    autoFocus={autoFocus}
    cellStyle={cellStyle}
    cellStyleFocused={cellStyleFocused}
    textStyle={textStyle}
    textStyleFocused={textStyleFocused}
    value={value}
    editable={editable}
    codeLength={codeLength}
    onTextChange={onTextChange}
  />
);

module.exports = {
  PinVerify,
};
