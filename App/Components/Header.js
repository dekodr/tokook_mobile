import React from 'react';
import {Header as Hdr, Left, Body, Right, Title} from 'native-base';
import {TouchableOpacity, View} from 'react-native';
import * as Icon from '../assets/icons';
import {Loading} from './Loading';
import Styles from './Styles';

const Header = ({
  c,
  style,
  androidStatusBarColor,
  viewleftStyle,
  iconleft,
  iconleftName,
  iconleftStyle,
  iconleftSize,
  iconleftColor,
  viewbodyStyle,
  sourceimgBody,
  styleimgBody,
  titleStyle,
  txtTitle,
  viewrightStyle,
  iconright,
  iconrightName,
  iconrightStyle,
  iconrightSize,
  iconrightColor,
  onPressRightIcon,
  onPressLeftIcon,
  imageLeft,
  sourceimgLeft,
  styleimgLeft,
  imageRight,
  sourceimgRight,
  styleimgRight,
  styleimgRightWrapper,
  materialicon,
  evilicon,
  txtRight,
  onPressrighttxt,
  txtrightStyle,
  entypoicon,
  hasTabs,
  ionicon,
  loadingRight,
  fonticon,
  viewrightstyle,
}) => {
  return (
    <Hdr
      hasTabs={hasTabs ? hasTabs : null}
      style={style}
      androidStatusBarColor={androidStatusBarColor}>
      {/* <Left style={viewleftStyle}>
        {iconleft && fonticon ? (
          <TouchableOpacity onPress={onPressLeftIcon ? onPressLeftIcon : null}>
            <Icon.FontIcon
              name={iconleftName}
              style={iconleftStyle}
              size={iconleftSize}
              color={iconleftColor}
            />
          </TouchableOpacity>
        ) : iconleft ? (
          <TouchableOpacity onPress={onPressLeftIcon ? onPressLeftIcon : null}>
            <Icon.SimpleIcon
              name={iconleftName}
              style={iconleftStyle}
              size={iconleftSize}
              color={iconleftColor}
            />
          </TouchableOpacity>
        ) : imageLeft ? (
          <TouchableOpacity onPress={onPressLeftIcon ? onPressLeftIcon : null}>
            <c.Image source={sourceimgLeft} style={styleimgLeft} />
          </TouchableOpacity>
        ) : ionicon ? (
          <TouchableOpacity onPress={onPressLeftIcon ? onPressLeftIcon : null}>
            <Icon.Ionicons
              name={iconleftName}
              style={iconleftStyle}
              size={iconleftSize}
              color={iconleftColor}
            />
          </TouchableOpacity>
        ) : null}
      </Left> */}
      <Body style={viewbodyStyle}>
        {sourceimgBody ? (
          <c.Image source={sourceimgBody} style={styleimgBody} />
        ) : (
          <Title style={titleStyle}>{txtTitle}</Title>
        )}
      </Body>
      <Right style={viewrightStyle}>
        {txtRight ? (
          <TouchableOpacity style={viewrightstyle} onPress={onPressrighttxt}>
            {loadingRight ? (
              <View
                style={[
                  c.Styles.iconRight,
                  c.Styles.justifyStart,
                  c.Styles.alignItemsStart,
                ]}>
                <Loading />
              </View>
            ) : (
              <Title style={txtrightStyle}>{txtRight}</Title>
            )}
          </TouchableOpacity>
        ) : iconright && materialicon ? (
          <TouchableOpacity
            onPress={onPressRightIcon ? onPressRightIcon : null}>
            <Icon.MaterialIcon
              name={iconrightName}
              style={iconrightStyle}
              size={iconrightSize}
              color={iconrightColor}
            />
          </TouchableOpacity>
        ) : iconright && evilicon ? (
          <TouchableOpacity
            onPress={onPressRightIcon ? onPressRightIcon : null}>
            <Icon.EvilIcons
              name={iconrightName}
              style={iconrightStyle}
              size={iconrightSize}
              color={iconrightColor}
            />
          </TouchableOpacity>
        ) : iconright && entypoicon ? (
          <TouchableOpacity
            onPress={onPressRightIcon ? onPressRightIcon : null}>
            <Icon.EntypoIcons
              name={iconrightName}
              style={iconrightStyle}
              size={iconrightSize}
              color={iconrightColor}
            />
          </TouchableOpacity>
        ) : iconright ? (
          <TouchableOpacity
            onPress={onPressRightIcon ? onPressRightIcon : null}>
            <Icon.Ionicons
              name={iconrightName}
              style={iconrightStyle}
              size={iconrightSize}
              color={iconrightColor}
            />
          </TouchableOpacity>
        ) : imageRight ? (
          <TouchableOpacity
            onPress={onPressRightIcon ? onPressRightIcon : null}>
            <View style={styleimgRightWrapper}>
              <c.Image source={sourceimgRight} style={styleimgRight} />
            </View>
          </TouchableOpacity>
        ) : null}
      </Right>
    </Hdr>
  );
};

module.exports = {
  Header,
};
