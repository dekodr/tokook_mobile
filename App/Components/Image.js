import React from 'react';
import {
	Image as Img
} from 'react-native';


const Image = ({ source, style }) => (
	<Img 
		source={source}
		style={style}
	/>
)


module.exports = {
	Image
}