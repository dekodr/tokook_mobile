import * as React from 'react';
import { StyleSheet, View, Text, Pressable, Dimensions, useWindowDimensions, FlatList } from 'react-native';
import { ListItem } from 'native-base';
import Card from './Card';
import { ProductCard, SupplierCategory } from './Card_V2';
import ButtonIcon from './ButtonIcon';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {Colors, Fonts} from '../Themes';

let {height, width} = Dimensions.get('window');

const FirstRoute = () => (
  <View style={{ flex: 1, backgroundColor: '#fff' }}>
    <View style={{marginHorizontal: 12, marginVertical: 24, maxWidth: 125 }}>
      <ButtonIcon 
        iconRight
        icon="chevron-down"
        text="Urutkan"
        color={Colors.textGrey}
        size={26}
        style={{
          // backgroundColor: '#ddd',
          borderRadius: 48,
          // paddingHorizontal: 8,
          width: 'auto',
          height: 38,
          borderColor: Colors.textGrey,
          borderWidth: 0.4,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        textStyle={{
          color: Colors.textGrey,
          marginLeft: 8,
          letterSpacing: .4,
        }}
      />
    </View>
    <FlatList
      columnWrapperStyle={{justifyContent: 'space-between'}}
      numColumns={2}
      style={
        ([styles.CardRow],
        {
          paddingHorizontal: 12,
          marginTop: -12,
          flexDirection: 'column',
        })
      }
      data={[
        {
          ProductName:
            'Tesla model 1, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '1',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
        {
          ProductName:
            'Tesla model 2, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '2',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
        {
          ProductName:
            'Tesla model 3, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '3',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
        {
          ProductName:
            'Tesla model 4, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '4',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
        {
          ProductName:
            'Tesla model 5, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '5',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
        {
          ProductName:
            'Tesla model 6, made indonesia super speed echo friendly',
          ProductImage: require('../assets/image/honda-jazz-hero.png'),
          ProductPrice: '2.999.000.000',
          product_id: '6',
          ProductStoreLocation: 'Menteng, Jakarta Pusat',
        },
      ]}
      renderItem={({item}) => (
        <ListItem
          key={item.id}
          style={{
            marginRight: item.productId % 2 == 1 ? 6 : 0,
            marginLeft: item.productId % 2 == 0 ? 6 : 0,
            paddingTop: 0,
            paddingBottom: 0,
            paddingRight: 0,
            width: width / 2 - 18,
            marginTop: 12,
            borderBottomColor: 'rgba(0,0,0,0)',
          }}>
          <ProductCard 
            id={item.id}
            image={item.ProductImage}
            title={item.ProductName}
            price={item.ProductPrice}
            location={item.ProductStoreLocation}
            onPress={() => OnPressCarousel()}
            styles={{
              width: '100%',
            }}
          />
        </ListItem>
      )}
    />
  </View>
);

const SecondRoute = () => (
  <View style={{ flex: 1, backgroundColor: '#fff' }}>
    <FlatList
      // columnWrapperStyle={{justifyContent: 'space-between'}}
      numColumns={2}
      style={
        ([styles.CardRow],
        {
          paddingHorizontal: 12,
          marginTop: 0,
          flexDirection: 'column',
          // backgroundColor: '#222'
        })
      }
      data={[
        {
          CategoryId: '1',
          CategoryName: 'Pakaian Wanita',
          CategoryImage: require('../assets/image/honda-jazz-hero.png'),
        },
        {
          CategoryId: '2',
          CategoryName: 'Pakaian Pria',
          CategoryImage: require('../assets/image/honda-jazz-hero.png'),
        },
        {
          CategoryId: '3',
          CategoryName: 'Bawahan Pria',
          CategoryImage: require('../assets/image/honda-jazz-hero.png'),
        },
        {
          CategoryId: '4',
          CategoryName: 'Rok',
          CategoryImage: require('../assets/image/honda-jazz-hero.png'),
        },
      ]}
      renderItem={({item}) => (
        <ListItem
          key={item.id}
          style={{
            marginRight: item.CategoryId % 2 == 1 ? 6 : 0,
            marginLeft: item.CategoryId % 2 == 0 ? 6 : 0,
            paddingTop: 0,
            paddingBottom: 0,
            paddingRight: 0,
            width: width / 2 - 18,
            marginTop: 12,
            borderBottomColor: 'rgba(0,0,0,0)',
          }}>
          <SupplierCategory
            id={item.CategoryId}
            image={item.CategoryImage}
            title={item.CategoryName}
            onPress={() => _handleMarketplaceProduct()}
            styles={{
              width: '100%',
            }}
          />
        </ListItem>
        // <ListItem
        //   key={item.id}
        //   style={{
        //     marginLeft: 0,
        //     paddingTop: 0,
        //     paddingBottom: 0,
        //     paddingRight: 0,
        //     width: width / 2 - 7,
        //     marginTop: item.id - 1 == -1 ? 0 : 12,
        //     borderBottomColor: 'rgba(0,0,0,0)',
        //   }}>
            
            // {/* <Pressable
            //   delayPressIn={0}
            //   onPress={() => _handleMarketplaceProduct()}
            //   style={{
            //     paddingRight: 12,
            //     width: '100%',
            //     marginLeft: item.id % 2 == 0 ? 0 : 6,
            //     marginRight: item.id % 2 == 1 ? 6 : 0,
            //   }}>
            //   <Card
            //     type="CardCategoryBig"
            //     id={item.CategoryId}
            //     CategoryName={item.CategoryName}
            //     CategoryImage={item.CategoryImage}
            //   />
            // </Pressable> */}
        // </ListItem>
      )}
    />
  </View>
);

export default function VerticalTabBar() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Produk' },
    { key: 'second', title: 'Kategori' },
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: Colors.blueBold, height: 4 }}
      labelStyle={{fontFamily: 'NotoSans-Bold', fontSize: Fonts.size_16, color: Colors.textGreyDark, letterSpacing: 1, textTransform: 'capitalize'}}
      style={{ backgroundColor: Colors.white }}
    />
  );

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
    />
  );
}

const styles = StyleSheet.create({
  tabbar: {
    backgroundColor: '#f3f3f3',
  },
  tab: {
    borderColor: '#c9c6c6',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  indicator: {
    backgroundColor: '#fff',
    borderLeftColor: Colors.blueBold,
    borderLeftWidth: 8,
  },
  label: {
    fontFamily: 'NotoSans-Bold',
    fontSize: 14,
    color: Colors.textGreyDark,
    letterSpacing: 1,
  },
});