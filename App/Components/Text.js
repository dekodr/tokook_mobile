import React from 'react';
import {
	Text as Txt
} from 'react-native';

let Text = ({ props, txt, style, onPress, styleAdd, txtAdd, onPressAdd, styleLast, txtLast, onPressLast, txtFourth, styleFourth, onPressFourth, txtFifth, onPressFifth, styleFifth}) => (
		<Txt
			onPress={onPress ? onPress : props ? props : null}
			style={style}>
			{txt}{txtAdd ?
			<Txt
				onPress={onPressAdd ? onPressAdd : null}
				style={styleAdd}>
				{txtAdd}
			</Txt> : null}
			{txtLast ? 
			<Txt
				onPress={onPressLast ? onPressLast : null}
				style={styleLast}>{txtLast}</Txt> : null}
			{txtFourth ?
			<Txt
				onPress={onPressFourth ? onPressFourth : null}
				style={styleFourth}>{txtFourth}</Txt> : null}
			{txtFifth ?
			<Txt
				onPress={onPressFifth ? onPressFifth : null}
				style={styleFifth}>{txtFifth}</Txt> : null}
		</Txt>
)


module.exports = {
	Text
}
