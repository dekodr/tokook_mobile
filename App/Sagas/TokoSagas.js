import {put, call} from 'redux-saga/effects';
import TokoActions from '../Redux/TokoRedux';
import GlobalActions from '../Redux/GlobalRedux';
import AuthActions from '../Redux/AuthenticationRedux';
import CameraActions from '../Redux/CameraRedux';
import {NavigationActions} from 'react-navigation';
import {ToastAndroid} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import _ from 'lodash';

export function* postToko(api, data) {
  try {
    const dataPost = JSON.stringify(data.data.item);
    const getApi = yield call(api.postTokoApi, dataPost);
    if (getApi.ok) {
      yield put(TokoActions.tokoPostSuccess(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(TokoActions.getTokoRequest());
      data.data.func.reloadData();
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.tokoPostFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadFailed();
    }
  } catch (error) {
    yield put(
      TokoActions.tokoPostFailed({
        error,
      }),
    );
    ToastAndroid.show(error, ToastAndroid.SHORT);
    data.data.func.reloadFailed();
  }
}

export function* updateToko(api, data) {
  try {
    const getApi = yield call(api.updateTokoApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.tokoUpdateSuccess(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(TokoActions.getTokoRequest());
      data.data.func.reloadData();
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.tokoUpdateFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      if (data.data.func.reloadFailed) {
        data.data.func.reloadFailed();
      }
    }
  } catch (error) {
    console.log(error);
    yield put(
      TokoActions.tokoUpdateFailed({
        error,
      }),
    );
    //ToastAndroid.show(error, ToastAndroid.SHORT);
    if (data.data.func.reloadFailed) {
      data.data.func.reloadFailed();
    }
  }
}
export function* BusinessType(api, data) {
  try {
    const getApi = yield call(api.getBusinessTypeApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getBusinessTypeSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getBusinessTypeFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getBusinessTypeFailed({
        error,
      }),
    );
    ToastAndroid.show(error, ToastAndroid.SHORT);
    if (data.data.func.reloadFailed) {
      data.data.func.reloadFailed();
    }
  }
}
export function* getToko(api) {
  try {
    const getApi = yield call(api.getToko, null);
    if (getApi.ok) {
      yield put(TokoActions.getTokoSuccess(getApi.data.data));
      const fcmToken = firebase.messaging().getToken();
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getTokoFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoFailed({
        error,
      }),
    );
  }
}
export function* getSupplier(api) {
  try {
    const getApi = yield call(api.getSupplier, null);
    if (getApi.ok) {
      yield put(TokoActions.getSupplierSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getSupplierFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getSupplierFailed({
        error,
      }),
    );
  }
}

export function* getProvince(api) {
  try {
    const getApi = yield call(api.getProvinceApi, null);
    if (getApi.ok) {
      yield put(TokoActions.getProvinceSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(TokoActions.getProvinceFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getProvinceFailed({
        error,
      }),
    );
  }
}
export function* getCity(api, data) {
  try {
    const getApi = yield call(api.getCityApi, data.data.id);
    if (getApi.ok) {
      yield put(TokoActions.getCitySuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(TokoActions.getCityFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getCityFailed({
        error,
      }),
    );
  }
}
export function* getDistrict(api, data) {
  try {
    const getApi = yield call(api.getDistrictApi, data.data.id);
    if (getApi.ok) {
      yield put(TokoActions.getDistrictSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(TokoActions.getDistrictFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getDistrictFailed({
        error,
      }),
    );
  }
}
export function* getPostalCode(api, data) {
  try {
    const getApi = yield call(api.getPostalCodeApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getPostalCodeSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(TokoActions.getPostalCodeFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getPostalCodeFailed({
        error,
      }),
    );
  }
}
export function* getDetailToko(api, data) {
  try {
    const getApi = yield call(api.getDetailToko, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getDetailTokoSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 404) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getDetailTokoFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getDetailTokoFailed({
        error,
      }),
    );
  }
}
export function* getDetailProduct(api, data) {
  try {
    const dataPost = data.data;
    const getApi = yield call(
      api.getDetailProduct,
      dataPost.tokoId,
      dataPost.productId,
    );

    if (getApi.ok) {
      console.log('getapi', getApi);
      yield put(TokoActions.getDetailProductSuccess(getApi.data.data));
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getDetailProductFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getDetailProductFailed({
        error,
      }),
    );
  }
}
export function* getTokoFinancial(api, data) {
  try {
    const getApi = yield call(api.getTokoFinancialApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getTokoFinancialSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getTokoFinancialFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoFinancialFailed({
        error,
      }),
    );
  }
}
export function* getCategoryToko(api, data) {
  try {
    const getApi = yield call(api.getCategoryToko, data.data);
    console.log('cekk api category', getApi);
    if (getApi.ok) {
      yield put(TokoActions.getCategoryTokoSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 500) {
        yield put(TokoActions.getTokoUnavailableFailed(getApi.data));
        yield put(TokoActions.getTokoRequest());
      } else {
        if (getApi.status === 401) {
          firebase.messaging().deleteToken();
          yield put(AuthActions.loginEmailPostReset());

          AsyncStorage.removeItem('isLogin');
          AsyncStorage.removeItem('bearerToken');

          yield put(
            StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  //   routeName: "OrganizationCodeScreen"
                  routeName: 'LoginScreen',
                }),
              ],
            }),
          );
        }
        yield put(TokoActions.getCategoryTokoFailed(getApi.data));
      }

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getCategoryTokoFailed({
        error,
      }),
    );
  }
}
export function* getKycStatusToko(api) {
  try {
    const getApi = yield call(api.getKycStatusApi, null);
    if (getApi.ok) {
      yield put(TokoActions.getKycStatusTokoSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      if (getApi.status === 500) {
        yield put(TokoActions.getTokKycStatusTokoFailed(getApi.data));
      } else {
        yield put(TokoActions.getKycStatusTokoFailed(getApi.data));
      }

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getKycStatusTokoFailed({
        error,
      }),
    );
  }
}
export function* handlingOWnerTokoCourierStatus(api, data) {
  try {
    const getApi =
      data.data.purpose === 1
        ? yield call(api.postActivatedOwnerTokoCourierApi, data.data)
        : yield call(api.deleteActivatedOwnerTokoCourierApi, data.data);
    if (getApi.ok) {
      yield put(
        TokoActions.handlingOwnerTokoCourierStatusSuccess(getApi.data.data),
      );
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      // yield put(TokoActions.getOwnerTokoCourierRequest(data.data.tokoId));

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      // yield put(TokoActions.getOwnerTokoCourierRequest(data.data.tokoId));
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(TokoActions.handlingOwnerTokoCourierStatusFailed(errMsg));
      } else {
        yield put(TokoActions.handlingOwnerTokoCourierStatusFailed(errMsg));
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.handlingOwnerTokoCourierStatusFailed({error}));
  }
}
export function* postCategoryToko(api, data) {
  try {
    const getApi = yield call(api.postCategoryToko, data.data);
    console.log('data', data);
    if (getApi.ok) {
      yield put(TokoActions.postCategoryTokoSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadData(getApi.data.data);
    } else {
      const errMsg = {message: getApi.data.message};
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(TokoActions.postCategoryTokoFailed(errMsg));
      } else {
        yield put(TokoActions.postCategoryTokoFailed(errMsg));
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadFailed('add');
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.postCategoryTokoFailed({error}));
  }
}

export function* postWithdrawDisbursmentToko(api, data) {
  try {
    const getApi = yield call(api.postWithdrawDisbursmentApi, data.data);
    console.log('cacicu', getApi);
    if (getApi.ok) {
      yield put(
        TokoActions.postWithdrawDisbursmentTokoSuccess(getApi.data.data),
      );
      yield put(AuthActions.otpWithdrawDisbursmentPostReset());
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.rbSheet.current.close();
      data.data.func.resetState();
      yield put(
        NavigationActions.navigate({
          routeName: 'FinanceTransactionScreen',
          params: {isFromBankTf: true},
        }),
      );
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      yield put(TokoActions.postWithdrawDisbursmentTokoFailed(errMsg));
      // if (getApi.status === 500 || getApi.status === 400) {
      //   yield put(TokoActions.postWithdrawDisbursmentTokoFailed(errMsg));
      // }

      // else {
      //   yield put(TokoActions.postWithdrawDisbursmentTokoFailed(errMsg));
      // }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      // data.data.func.reloadFailed('add');
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    // yield put(TokoActions.postWithdrawDisbursmentTokoFailed({error}));
  }
}

export function* getWithdrawHistory(api, data) {
  try {
    const getApi = yield call(api.getWithdrawHistoryApi, data.data);
    console.log('cek 12345', getApi);
    if (getApi.ok) {
      yield put(TokoActions.getWithdrawHistoryTokoSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getWithdrawHistoryTokoFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getWithdrawHistoryTokoFailed({
        error,
      }),
    );
  }
}

export function* postKycToko(api, data) {
  try {
    const getApi = yield call(api.postKycApi, data.data);
    console.log('post kyc ni', getApi);
    if (getApi.ok) {
      yield put(TokoActions.postKycTokoSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(TokoActions.getKycStatusTokoRequest());
      yield put(NavigationActions.back());
      yield put(CameraActions.takePictureKtpReset());
      yield put(CameraActions.takePictureKtpSelfieReset());

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(TokoActions.postKycTokoFailed(errMsg));
      } else {
        yield put(TokoActions.postKycTokoFailed(errMsg));
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadFailed('add');
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.postKycTokoFailed({error}));
  }
}

export function* getTokoAddress(api, data) {
  try {
    const getApi = yield call(api.getTokoAddressApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getTokoAddressSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getTokoAddressFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoAddressFailed({
        error,
      }),
    );
  }
}
export function* getTokoOrder(api, data) {
  try {
    const getApi = yield call(api.getTokoOrderTransactionApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getTokoOrderSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      if (getApi.status === 500) {
        yield put(TokoActions.getTokoUnavailableFailed(getApi.data));
        yield put(TokoActions.getTokoRequest());
      } else {
        yield put(TokoActions.getTokoOrderFailed(getApi.data));
      }

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoOrderFailed({
        error,
      }),
    );
  }
}
export function* getTokoOrderDetail(api, data) {
  const dataArray = data.data;
  const dataPost = {
    tokoId: dataArray.tokoId,
    transactionId: dataArray.transactionId,
  };
  console.log(
    'dataPost',
    dataPost,
    dataArray,
    data,
    dataArray.listOrder !== undefined,
  );
  try {
    let getApi = null;
    if (dataArray.listOrder !== undefined) {
      getApi = yield call(api.getTokoOrderTransactionDetailApi, dataPost);
    } else {
      getApi = yield call(api.getTokoOrderTransactionDetailApi, data.data);
    }
    console.log('getApi', getApi);
    if (getApi.ok) {
      yield put(TokoActions.getTokoOrderDetailSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      console.log('getApierror', getApi);
      yield put(TokoActions.getTokoOrderDetailFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log(error);
    yield put(
      TokoActions.getTokoOrderDetailFailed({
        error,
      }),
    );
  }
}
export function* getTokoBank(api, data) {
  try {
    const getApi = yield call(api.getTokoBankApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getTokoBankSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getTokoBankFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoBankFailed({
        error,
      }),
    );
  }
}
export function* getTokoCourier(api, data) {
  try {
    const getApi = yield call(api.getTokoCourierApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getTokoCourierSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getTokoCourierFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getTokoCourierFailed({
        error,
      }),
    );
  }
}

export function* getOwnerTokoCourier(api, data) {
  try {
    const getApi = yield call(api.getOwnerTokoCourierApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getOwnerTokoCourierSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getOwnerTokoCourierFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getOwnerTokoCourierFailed({
        error,
      }),
    );
  }
}
export function* getBank(api, data) {
  try {
    const getApi = yield call(api.getBankApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.getBankSuccess(getApi.data.data));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.getBankFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getBankFailed({
        error,
      }),
    );
  }
}
export function* postTokoAddress(api, data) {
  try {
    const getApi =
      data.data.purpose === 1
        ? yield call(api.postTokoAddressApi, data.data)
        : yield call(api.updateTokoAddressApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.postTokoAddressSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(TokoActions.getTokoAddressRequest(data.data.tokoId));

      let dataModal = {
        modalVisible: false,
        step: 0,
      };
      yield put(GlobalActions.setModalVisible(dataModal));
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      const errMsg = {message: getApi.data.message};

      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoAddressFailed(errMsg));
      } else {
        yield put(TokoActions.postTokoAddressFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log('error', error);
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(TokoActions.postTokoAddressFailed({error}));
  }
}
export function* postTokoBank(api, data) {
  try {
    const getApi =
      data.data.purpose === 1
        ? yield call(api.postTokoBankApi, data.data)
        : data.data.purpose === 2
        ? yield call(api.updateTokoBankApi, data.data)
        : yield call(api.deleteTokoBankApi, data.data);
    console.log('vbbb', getApi);
    if (getApi.ok) {
      yield put(TokoActions.postTokoBankSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(TokoActions.getTokoBankRequest(data.data.tokoId));
      // data.data.func.updateBank.setBankIndex(-1);
      if (data.data.purpose === 1) {
        data.data.setValueBank(1, false);
      } else {
        data.data.setValueBank(2, -1);
      }
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoBankFailed(errMsg));
      } else {
        yield put(TokoActions.postTokoBankFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    // ToastAndroid.show(errMsg.message, ToastAndroid.SHORT);
    yield put(TokoActions.postTokoBankFailed({error}));
  }
}

export function* postTokoOrderProceed(api, data) {
  try {
    const getApi = yield call(api.postOrderTransactionApi, data.data);

    if (getApi.ok) {
      yield put(TokoActions.postTokoOrderProceedSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(
        TokoActions.getTokoOrderDetailRequest({
          tokoId: data.data.tokoId,
          transactionId: data.data.transactionId,
        }),
      );
      data.data.func.current.scrollBy(1);

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      console.log(getApi);
      const errMsg = {message: getApi.data.message};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 404 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoOrderProceedFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.postTokoOrderProceedFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.postTokoOrderProceedFailed({error}));
  }
}

export function* postTokoAcceptPayment(api, data) {
  try {
    const getApi = yield call(
      data.data.purpose === 1
        ? api.postPaymentAcceptApi
        : api.postPaymentDeclineApi,
      data.data,
    );

    if (getApi.ok) {
      yield put(TokoActions.postTokoAcceptPaymentSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(
        TokoActions.getTokoOrderDetailRequest({
          tokoId: data.data.tokoId,
          transactionId: data.data.transactionId,
        }),
      );
      data.data.func.swiper.current.scrollBy(1);
      data.data.func.rbSheet.current.close();
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 404 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoAcceptPaymentFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.postTokoAcceptPaymentFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.postTokoAcceptPaymentFailed({error}));
  }
}

export function* postSendOrder(api, data) {
  try {
    const getApi = yield call(api.postSendOrderTransactionApi, data.data);
    console.log('getapi', getApi);
    if (getApi.ok) {
      yield put(TokoActions.postTokoSendOrderSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      let dataPost = {
        tokoId: data.data.tokoId,
        transactionId: data.data.detail.transaction_id,
        handlesetListOrders: data.data.handlesetListOrders,
        listOrder: data.data.listOrder,
        index: data.data.index,
      };
      //yield put(TokoActions.getTokoOrderDetailRequest(dataPost));
      // data.data.func.current.scrollBy(1);
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 404 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoSendOrderFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.postTokoSendOrderFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    //yield put(TokoActions.postTokoSendOrderFailed({error}));
  }
}
export function* postFinishOrder(api, data) {
  try {
    const getApi = yield call(api.postFinishOrderTransactionApi, data.data);

    if (getApi.ok) {
      yield put(TokoActions.postTokoFinishOrderSuccess(getApi.data.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(
        TokoActions.getTokoOrderDetailRequest({
          tokoId: data.data.tokoId,
          transactionId: data.data.detail.transaction_id,
        }),
      );
      // data.data.func.current.scrollBy(1);
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      const errMsg = {message: getApi.data.message};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 404 ||
        getApi.status === 409
      ) {
        yield put(TokoActions.postTokoFinishOrderFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.postTokoFinishOrderFailed(errMsg));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.postTokoFinishOrderFailed({error}));
  }
}
export function* updateDefaultRekeningToko(api, data) {
  try {
    const getApi = yield call(api.updateRekeningDefaultApi, data.data);
    if (getApi.ok) {
      yield put(TokoActions.updateDefaultRekeningTokoSuccess(getApi.data.data));
      yield put(TokoActions.getTokoBankRequest(data.data.tokoId));

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.updateDefaultRekeningTokoFailed(getApi.data));

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.updateDefaultRekeningTokoFailed({
        error,
      }),
    );
  }
}
export function* updateCategoryToko(api, data) {
  try {
    const getApi = yield call(api.updateCategoryToko, data.data);

    if (getApi.ok) {
      yield put(TokoActions.updateCategoryTokoSuccess(getApi.data.data));
      data.data.func.reloadData();
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.updateCategoryTokoFailed(getApi.data));
      data.data.func.reloadFailed('add');
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.updateCategoryTokoFailed({
        error,
      }),
    );
  }
}
export function* updatePositionCategoryToko(api, data) {
  try {
    const getApi = yield call(api.updatePositionCategoryToko, data.data);
    if (getApi.ok) {
      yield put(
        TokoActions.updatePositionCategoryTokoSuccess(getApi.data.data),
      );
      data.data.func.reloadData();
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.updatePositionCategoryTokoFailed(getApi.data));

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.updatePositionCategoryTokoFailed({
        error,
      }),
    );
  }
}
export function* deleteCategoryToko(api, data) {
  try {
    const getApi = yield call(api.deleteCategoryToko, data.data);
    if (getApi.ok) {
      yield put(TokoActions.deleteCategoryTokoSuccess(getApi.data.data));
      data.data.func.reloadData();
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(TokoActions.deleteCategoryTokoFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.deleteCategoryTokoFailed({
        error,
      }),
    );
  }
}
export function* getProductToko(api, data) {
  try {
    const getApi = yield call(api.getProductToko, data.data);
    console.log('get api toko production', getApi);
    if (getApi.ok) {
      yield put(TokoActions.getProductTokoSuccess(getApi.data.data));
      console.log('getApi', getApi);
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      if (getApi.status === 500) {
        yield put(TokoActions.getTokoUnavailableFailed(getApi.data));
        yield put(TokoActions.getTokoRequest());
      } else {
        yield put(TokoActions.getProductTokoFailed(getApi.data));
      }

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.getProductTokoFailed({
        error,
      }),
    );
  }
}

export function* uploadProductImage(api, data) {
  try {
    const dataPost = data.data;
    console.log('dataPost', dataPost);
    const getApi = yield call(api.uploadProductImages, dataPost.data);
    if (getApi.ok) {
      yield put(TokoActions.uploadProductImageSuccess(getApi.data.data));
      //ToastAndroid.show('sukses', ToastAndroid.SHORT);
      let {
        parentName,
        dataFinal,
        name,
        imagesTemp,
        setImagesTemp,
        urlImages,
      } = dataPost;
      if (dataFinal) {
        dataFinal.map(df => {
          if (df.name === parentName) {
            df.options.map(dfo => {
              if (dfo.name === name) {
                dfo.img = getApi.data.url;
              }
            });
          }
        });
      }
      if (imagesTemp) {
        imagesTemp.map((v, i) => {
          if (!v.id) {
            urlImages[i] = getApi.data.url;
          } else {
            urlImages[i] = v.url;
          }
        });
      }
      if (dataPost.data.func) {
        dataPost.data.func.reloadData(getApi.data.url);
      }

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          TokoActions.uploadProductImageFailed({message: getApi.data.message}),
        );
      } else {
        yield put(TokoActions.uploadProductImageFailed(errMsg));
      }
      ToastAndroid.show('Koneksi Bermasalah. Coba Lagi', ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log(error);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(TokoActions.uploadProductImageFailed(errMsg));
  }
}
export function* postProductToko(api, data) {
  console.log('lala', data.data);
  try {
    const dataPost = JSON.stringify(data.data.item);
    const getApi = yield call(
      api.postProductToko,
      data.data.id,
      data.data.item,
      data.data.isVarian,
    );

    if (getApi.ok) {
      console.log('getApi', getApi);
      yield put(TokoActions.postProductTokoSuccess(getApi.data.data));
      yield put(TokoActions.postCategoryTokoReset());
      yield put(TokoActions.postProductTokoReset());

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadData();
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.postCategoryTokoReset());
      console.log(getApi);
      data.data.func.reloadFailed(false);
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      //const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 409
      ) {
        yield put(
          TokoActions.postProductTokoFailed({message: getApi.data.message}),
        );

        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.postProductTokoFailed(getApi.data.message));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    data.data.func.reloadFailed(false);
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    //const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(TokoActions.postCategoryTokoReset());
    yield put(TokoActions.postProductTokoFailed(error));
    // data.data.func.reloadData();
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}
export function* updateProductToko(api, data) {
  console.log('lala', data.data);
  try {
    const dataPost = JSON.stringify(data.data.item);
    const getApi = yield call(
      api.updateProductToko,
      data.data.id,
      data.data.item,
      data.data.isVarian,
      data.data.productId,
    );
    console.log('getApi', getApi);
    if (getApi.ok) {
      yield put(TokoActions.updateProductTokoSuccess(getApi.data.data));
      yield put(TokoActions.postCategoryTokoReset());
      yield put(TokoActions.updateProductTokoReset());

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadData();
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.postCategoryTokoReset());
      console.log(getApi);
      //const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 409
      ) {
        yield put(
          TokoActions.updateProductTokoFailed({message: getApi.data.message}),
        );

        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.updateProductTokoFailed(getApi.data.message));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    //const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(TokoActions.postCategoryTokoReset());
    yield put(TokoActions.updateProductTokoFailed(errMsg));
    // data.data.func.reloadData();
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}
export function* miniUpdateProductToko(api, data) {
  console.log('lala', data.data);
  try {
    const dataPost = JSON.stringify(data.data.item);
    const getApi = yield call(
      api.miniUpdateProductToko,
      data.data.id,
      data.data.item,
      data.data.productId,
    );
    console.log('getApi', getApi);
    if (getApi.ok) {
      yield put(TokoActions.updateProductTokoSuccess(getApi.data.data));

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadData();
    } else {
      //yield put(TokoActions.postCategoryTokoReset());
      console.log(getApi);
      //const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 409
      ) {
        yield put(
          TokoActions.miniUpdateProductTokoFailed({
            message: getApi.data.message,
          }),
        );

        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      } else {
        yield put(TokoActions.miniUpdateProductTokoFailed(getApi.data.message));
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    console.log('error', error);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    //yield put(TokoActions.postCategoryTokoReset());
    yield put(TokoActions.miniUpdateProductTokoFailed(errMsg));
    // data.data.func.reloadData();
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}

export function* deleteProductToko(api, data) {
  try {
    const getApi = yield call(api.deleteProductToko, data.data);

    if (getApi.ok) {
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      data.data.func.callback();
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.deleteProductTokoFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.deleteProductTokoFailed({
        error,
      }),
    );
  }
}

export function* deleteToko(api, data) {
  try {
    const getApi = yield call(api.deleteToko, data.data);

    if (getApi.ok) {
      yield put(TokoActions.getTokoReset());
      yield put(TokoActions.deleteTokoSuccess(getApi.data.data));
      yield put(TokoActions.getTokoReset());
      yield put(TokoActions.tokoReset());
      yield put(TokoActions.getTokoOrderReset());
      yield put(TokoActions.getCategoryTokoReset());
      yield put(TokoActions.getTokoAddressReset());

      //yield put(GlobalActions.setTokenApiGloballyReset());
      yield put(AuthActions.getUserReset());
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(AuthActions.deleteAuthTokenUserRequest(data.data));
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 400) {
        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }
      yield put(TokoActions.deleteTokoFailed(getApi.data));

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      TokoActions.deleteTokoFailed({
        error,
      }),
    );
  }
}
