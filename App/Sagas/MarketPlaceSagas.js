import {put, call} from 'redux-saga/effects';
import MarketPlaceActions from '../Redux/MarketPlaceRedux';
import {ToastAndroid, Linking} from 'react-native';
import _ from 'lodash';
import {MARKETPLACE_PRODUCT_LAST_VIEW} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import {StackActions, NavigationActions} from 'react-navigation';

export function* getProductByUrl(api, data) {
  try {
    const getApi = yield call(api.getMarketPlaceProductByUrl, {
      product_url: data.data,
    });
    if (getApi.ok) {
      yield put(MarketPlaceActions.getProductByUrlSuccess(getApi.data));

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(MarketPlaceActions.getProductByUrlFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getProductByUrlFailed({
        error,
      }),
    );
  }
}
export function* getProductBySearch(api, data) {
  try {
    console.log('get data', data.data);
    const getApi = yield call(api.getMarketplaceSearchApi, data.data);
    console.log(getApi);
    if (getApi.ok) {
      yield put(MarketPlaceActions.getProductBySearchSuccess(getApi.data));

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      if (getApi.status === 401) {
        firebase.messaging().deleteToken();
        yield put(AuthActions.loginEmailPostReset());

        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
      }

      yield put(MarketPlaceActions.getProductBySearchFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getProductBySearchFailed({
        error,
      }),
    );
  }
}
export function* getMarketplaceProducts(api, data) {
  try {
    const getApi = yield call(api.getMarketplaceProducts);
    console.log(getApi);
    if (getApi.ok) {
      yield put(MarketPlaceActions.getMarketplaceProductsSuccess(getApi.data));

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              //   routeName: "OrganizationCodeScreen"
              routeName: 'LoginScreen',
            }),
          ],
        }),
      );
      yield put(MarketPlaceActions.getMarketplaceProductsFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getMarketplaceProductsFailed({
        error,
      }),
    );
  }
}
// attempts to login
export function* getProductLastView(api, {payload, append}) {
  const params = {
    ...payload,
    name: MARKETPLACE_PRODUCT_LAST_VIEW,
  };
  const response = yield call(api.getMarketplaceLastViewApi, params);
  if (response.ok) {
    if (append) {
      yield put(MarketPlaceActions.getProductLastViewAppend(response.data));
    } else {
      yield put(MarketPlaceActions.getProductLastViewSuccess(response.data));
    }
  } else {
    yield put(MarketPlaceActions.getProductLastViewFailed(response));
  }
}
export function* getMarketplaceTrendingProducts(api, data) {
  try {
    const getApi = yield call(api.getMarketplaceLastViewApi, data.data);

    if (getApi.ok) {
      console.log('getApi', getApi);
      yield put(
        MarketPlaceActions.getMarketplaceTrendingProductsSuccess(getApi.data),
      );

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(
        MarketPlaceActions.getMarketplaceTrendingProductsFailed(getApi.data),
      );
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getMarketplaceTrendingProductsFailed({
        error,
      }),
    );
  }
}
export function* getMarketplaceTerlarisProducts(api, data) {
  try {
    const getApi = yield call(api.getMarketplaceTerlarisProducts, data.data);

    if (getApi.ok) {
      console.log('getApiMarketplaceTerlaris', getApi);
      yield put(
        MarketPlaceActions.getMarketplaceTerlarisProductsSuccess(getApi.data),
      );

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(
        MarketPlaceActions.getMarketplaceTerlarisProductsFailed(getApi.data),
      );
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getMarketplaceTerlarisProductsFailed({
        error,
      }),
    );
  }
}
export function* getMarketplaceProductDetails(api, data) {
  try {
    const getApi = yield call(api.getMarketplaceProductDetails, data.data);
    console.log('dataProduct', data, api, getApi);
    if (getApi.ok) {
      yield put(
        MarketPlaceActions.getMarketplaceProductDetailsSuccess(getApi.data),
      );

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(
        MarketPlaceActions.getMarketplaceProductDetailsFailed(getApi.data),
      );
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.getMarketplaceProductDetailsFailed({
        error,
      }),
    );
  }
}

export function* postProductToOwnToko(api, data) {
  try {
    const getApi = yield call(api.postProductToOwnToko, data.data);
    if (getApi.ok) {
      data.data.func.reloadData();
      yield put(MarketPlaceActions.postProductToOwnTokoSuccess(getApi.data));

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      data.data.func.reloadFailed(false);
      yield put(MarketPlaceActions.postProductToOwnTokoFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    data.data.func.reloadFailed(false);
    yield put(
      MarketPlaceActions.postProductToOwnTokoFailed({
        error,
      }),
    );
  }
}
export function* postCheckout(api, data) {
  const getApi = yield call(api.postCheckout, data.data);
  console.log('cek checkout', getApi);
  if (getApi.ok) {
    yield put(MarketPlaceActions.postCheckoutSuccess(getApi.data));
    if (getApi.data.data.checkoutUrl === undefined) {
      yield put(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              //   routeName: "OrganizationCodeScreen"
              routeName: 'Dashboard',
            }),
          ],
        }),
      );
      yield put(NavigationActions.navigate({routeName: 'Order'}));
    } else {
      Linking.openURL(getApi.data.data.checkoutUrl);

      yield put(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              //   routeName: "OrganizationCodeScreen"
              routeName: 'Dashboard',
            }),
          ],
        }),
      );
      yield put(NavigationActions.navigate({routeName: 'Order'}));
    }
    ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

    // yield put(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [
    //       NavigationActions.navigate({
    //         //   routeName: "OrganizationCodeScreen"
    //         routeName: 'Dashboard',
    //       }),
    //     ],
    //   }),
    // );
    // Go back to previous screen
    // yield put(NavigationActions.back());
  } else {
    yield put(MarketPlaceActions.postCheckoutFailed(getApi.data));
    ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}

export function* postPaymentAdminFee(api, data) {
  const getApi = yield call(api.getPaymentAdminFeeApi, data.data);
  console.log('cek payment admin fee', getApi);
  if (getApi.ok) {
    yield put(MarketPlaceActions.postPaymentAdminFeeSuccess(getApi.data));
  } else {
    yield put(MarketPlaceActions.postPaymentAdminFeeFailed(getApi.data));
    ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}
export function* postActivityLog(api, data) {
  try {
    const getApi = yield call(api.postActivityLog, data.data);
    if (getApi.ok) {
      // data.data.func.reloadData();
      yield put(MarketPlaceActions.postActivityLogSuccess(getApi.data));

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      // data.data.func.reloadFailed(false);
      yield put(MarketPlaceActions.postActivityLogFailed(getApi.data));
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // data.data.func.reloadFailed(false);
    yield put(
      MarketPlaceActions.postActivityLogFailed({
        error,
      }),
    );
  }
}

//CART

export function* postToCart(api, data) {
  try {
    const getApi = yield call(api.postToCart, data.data.item);
    console.log('datadata', data.data);
    if (getApi.ok) {
      yield put(MarketPlaceActions.postToCartSuccess(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      data.data.func.reloadData();
      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      // data.data.func.reloadFailed(false);
      yield put(MarketPlaceActions.postToCartFailed(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log('error', error);
    // data.data.func.reloadFailed(false);
    yield put(
      MarketPlaceActions.postToCartFailed({
        error,
      }),
    );
  }
}

export function* getTutorialBannerData(api, data) {
  try {
    const getApi = yield call(api.getTutorialBannerData, data.data);
    console.log('api', getApi);
    if (getApi.ok) {
      yield put(MarketPlaceActions.getTutorialBannerDataSuccess(getApi.data));

      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(MarketPlaceActions.getTutorialBannerDataFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log('errorrrrr', error);
    yield put(
      MarketPlaceActions.getTutorialBannerDataFailed({
        error,
      }),
    );
  }
}

export function* getCartData(api, data) {
  const getApi = yield call(api.getCartData, data.data);
  if (getApi.ok) {
    // const dataProduct = getApi.data.data.cart.map((v, i) => {
    //   return {
    //     cart_id: v.id,
    //     code: v.product_variant_code,
    //     quantity: v.quantity,
    //     product_id: v.product_details.id,
    //     name: v.product_details.name,
    //     images: v.product_details.images,
    //     is_from_supplier: null,
    //     supplier_id: v.product_details.supplier_id,
    //     supplier_name: v.product_details.supplier_name,
    //     supplier_city: v.product_details.supplier_city,
    //     variant: v.product_details.variant,
    //     price: v.product_details.price,
    //     isSelected: true,
    //   };
    // });

    // const dataChain = _.chain(dataProduct)
    //   // Group the elements of Array based on `color` property
    //   .groupBy('supplier_name')
    //   // `key` is group's name (color), `value` is the array of objects
    //   .map((value, key) => ({supplier_name: key, product_data: value}))
    //   .value();

    // const dataFinal = dataChain.map((v, i) => {
    //   const producDataManipulate = v.product_data.map((value, i) => {
    //     return {
    //       cart_id: value.cart_id,
    //       code: value.code,
    //       quantity: value.quantity,
    //       product_id: value.product_id,
    //       name: value.name,
    //       images: value.images,
    //       is_from_supplier: value.is_from_supplier,
    //       supplier_id: value.supplier_id,
    //       supplier_name: value.supplier_name,
    //       supplier_city: value.supplier_city,
    //       variant: value.variant,
    //       price: value.price,
    //       isSelected: value.isSelected,
    //     };
    //   });

    //   return {
    //     supplier_name: v.supplier_name,
    //     supplier_isSelected: true,
    //     product_data: producDataManipulate,
    //   };
    // });
    // console.log('data frrr', dataFinal);
    yield put(MarketPlaceActions.getCartDataSuccess(getApi.data));

    //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

    // Go back to previous screen
    // yield put(NavigationActions.back());
  } else {
    yield put(MarketPlaceActions.getCartDataFailed(getApi.data));
    //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}
export function* getPaymentChannel(api) {
  const getApi = yield call(api.getPaymentChannelApi);
  console.log('test v2 payment channel', getApi);
  if (getApi.ok) {
    yield put(MarketPlaceActions.getPaymentChannelSuccess(getApi.data));
  } else {
    yield put(MarketPlaceActions.getPaymentChannelFailed(getApi.data));
  }
}
export function* updateCartData(api, data) {
  try {
    const getApi = yield call(api.updateCartData, data.data.item);
    if (getApi.ok) {
      yield put(MarketPlaceActions.updateCartDataSuccess(getApi.data));
      //data.data.func.reloadData();
      //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(MarketPlaceActions.updateCartDataFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log('error', error);
    yield put(
      MarketPlaceActions.updateCartDataFailed({
        error,
      }),
    );
  }
}

export function* deleteCartData(api, data) {
  try {
    const getApi = yield call(api.deleteCartData, data.data.item);
    console.log('data', data.data, getApi);
    if (getApi.ok) {
      yield put(MarketPlaceActions.deleteCartDataSuccess(getApi.data));
      // data.data.func.reloadData();
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      // Go back to previous screen
      // yield put(NavigationActions.back());
    } else {
      yield put(MarketPlaceActions.deleteCartDataFailed(getApi.data));
      //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      MarketPlaceActions.deleteCartDataFailed({
        error,
      }),
    );
  }
}

export function* getAllShippingFee(api, data) {
  const getApi = yield call(api.getAllShippingFee, data.data);
  console.log('api', getApi, data.data.item);
  if (getApi.ok) {
    console.log('getapi', getApi);
    yield put(MarketPlaceActions.getAllShippingFeeSuccess(getApi.data));

    //   ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

    // Go back to previous screen
    // yield put(NavigationActions.back());
  } else {
    yield put(MarketPlaceActions.getAllShippingFeeFailed(getApi.data));
    //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
  }
}
