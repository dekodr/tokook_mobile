import {put, call} from 'redux-saga/effects';
import AuthActions from '../Redux/AuthenticationRedux';
import TokoActions from '../Redux/TokoRedux';
import GlobalActions from '../Redux/GlobalRedux';
import {NavigationActions, StackActions} from 'react-navigation';
import {ToastAndroid} from 'react-native';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import {get} from 'react-native/Libraries/Utilities/PixelRatio';

//VERIFICATION WA

export function* getUser(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.getUserApi, dataPost);
    console.log('getApi', getApi);
    if (getApi.ok) {
      const dataResponse = {
        data: getApi.data.data,
      };
      yield put(AuthActions.getUserSuccess(dataResponse.data.user));
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(AuthActions.getUserFailed({message: getApi.data.message}));
      } else {
        yield put(AuthActions.getUserFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    console.log('errroooorr', error);
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(AuthActions.getUserFailed(errMsg));
  }
}
export function* getOtpWa(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.getOtpWaApi, dataPost);
    if (getApi.ok) {
      const dataResponse = {
        data: getApi.data.data,
      };
      yield put(AuthActions.getOtpPostSuccess(dataResponse));
      //yield put(AuthActions.getOtpPostReset());
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      //yield put(NavigationActions.navigate({routeName: 'OtpScreen'}));
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(AuthActions.getOtpPostFailed({message: getApi.data.message}));
      } else {
        yield put(AuthActions.getOtpPostFailed(errMsg));
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(AuthActions.getOtpPostFailed(errMsg));
  }
}
export function* whatsappVerification(api, data) {
  try {
    const dataPost = JSON.stringify(data.data.item);
    const getApi = yield call(api.whatsappVerificationApi, dataPost);
    console.log('dataPost', dataPost, getApi);
    if (getApi.ok) {
      const dataResponse = {
        data: getApi.data.data,
      };
      yield put(AuthActions.whatsappVerificationSuccess(dataResponse));
      //yield put(AuthActions.getOtpPostReset());
      data.data.func.reloadData();
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    } else {
      data.data.func.reloadFailed();
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          AuthActions.whatsappVerificationFailed({
            message: getApi.data.message,
          }),
        );
      } else {
        yield put(AuthActions.whatsappVerificationFailed(errMsg));
      }
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    //ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(AuthActions.whatsappVerificationFailed(errMsg));
  }
}

export function* postLoginEmail(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.postLoginEmailApi, dataPost);
    if (getApi.ok) {
      dateFrom = moment()
        .add(7, 'days')

        .calendar();
      const loginTimeDate = moment(new Date(), 'h:mm:ss A')
        .add(0, 'seconds')
        .add(10, 'minutes')
        .format('MM-DD-YYYY hh:mm:ss');

      const dataResponse = {
        user: data.data,
        login: getApi.data.data,

        due_date: dateFrom,
        loginTime: loginTimeDate,
      };
      yield put(AuthActions.loginEmailPostSuccess(dataResponse));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(NavigationActions.navigate({routeName: 'OtpScreen'}));
      yield put(GlobalActions.dueDateRefreshToken(dateFrom));
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          AuthActions.loginEmailPostFailed({message: getApi.data.message}),
        );
      } else {
        yield put(AuthActions.loginEmailPostFailed(errMsg));
      }
      console.log('error pertama', getApi);
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    console.log('error', error);
    yield put(AuthActions.loginEmailPostFailed(errMsg));
  }
}
export function* postOtpWithdrawDisbursment(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.postLoginEmailApi, dataPost);
    if (getApi.ok) {
      const dataResponse = {
        user: data.data,
        login: getApi.data.data,
      };
      yield put(AuthActions.otpWithdrawDisbursmentPostSuccess(dataResponse));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          AuthActions.otpWithdrawDisbursmentPostFailed({
            message: getApi.data.message,
          }),
        );
      } else {
        yield put(AuthActions.otpWithdrawDisbursmentPostFailed(errMsg));
      }

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(AuthActions.otpWithdrawDisbursmentPostFailed(errMsg));
  }
}
export function* postLoginOtp(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.postLoginOtpApi, dataPost);
    if (getApi.ok) {
      yield put(AuthActions.loginOtpPostSuccess(getApi.data.data));
      yield put(TokoActions.getSupplierRequest());

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: 'Dashboard',
            }),
          ],
        }),
      );
      yield put(GlobalActions.setTokenApiGloballyRequest(getApi.data.data));
      AsyncStorage.setItem('isLogin', JSON.stringify(true));
      AsyncStorage.setItem('bearerToken', JSON.stringify(getApi.data.data));
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          AuthActions.loginOtpPostFailed({message: getApi.data.message}),
        );
      } else {
        yield put(AuthActions.loginOtpPostFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(AuthActions.loginOtpPostFailed(errMsg));
  }
}

export function* postRegisterOtp(api, data) {
  try {
    const dataPost = JSON.stringify(data.data);
    const getApi = yield call(api.postRegisterOtpApi, dataPost);

    if (getApi.ok) {
      yield put(AuthActions.registerOtpPostSuccess(getApi.data.data));
      yield put(TokoActions.getSupplierRequest());

      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      yield put(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({
              routeName: 'Dashboard',
            }),
          ],
        }),
      );
      yield put(GlobalActions.setTokenApiGloballyRequest(getApi.data.data));
      AsyncStorage.setItem('isLogin', JSON.stringify(true));
      AsyncStorage.setItem('bearerToken', JSON.stringify(getApi.data.data));

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          AuthActions.registerOtpPostFailed({message: getApi.data.message}),
        );
      } else {
        yield put(AuthActions.registerOtpPostFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
    yield put(AuthActions.registerOtpPostFailed(errMsg));
  }
}
export function* deleteAuthTokenUser(api) {
  try {
    const getApi = yield call(api.deleteAuthToken, null);
    if (getApi.ok) {
      yield put(TokoActions.getTokoReset());
      yield put(TokoActions.tokoReset());
      yield put(TokoActions.getTokoOrderReset());
      yield put(TokoActions.getCategoryTokoReset());
      yield put(TokoActions.getTokoAddressReset());
      yield put(AuthActions.loginEmailPostReset());

      //yield put(GlobalActions.setTokenApiGloballyReset());
      yield put(AuthActions.getUserReset());
      yield put(AuthActions.deleteAuthTokenUserSuccess(getApi.data.data));
      yield put(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              //   routeName: "OrganizationCodeScreen"
              routeName: 'LoginScreen',
            }),
          ],
        }),
      );
      firebase.messaging().deleteToken();

      AsyncStorage.removeItem('isLogin');
      AsyncStorage.removeItem('bearerToken');
    } else {
      yield put(
        StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              //   routeName: "OrganizationCodeScreen"
              routeName: 'LoginScreen',
            }),
          ],
        }),
      );
      yield put(AuthActions.deleteAuthTokenUserSuccess(getApi.data));
      ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    yield put(
      AuthActions.deleteAuthTokenUserSuccess({
        error,
      }),
    );
  }
}
