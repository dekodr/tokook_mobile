import {put, take, call, all, select} from 'redux-saga/effects';
import GlobalActions from '../Redux/GlobalRedux';
import TokoActions from '../Redux/TokoRedux';
import {StackActions, NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';
import moment from 'moment';
export function* setToken(api, data) {
  try {
    if (data) {
      console.log('setToken');
      yield call(api.setAuthToken, data.data.access_token);
      // yield call(api.setLanguage);
      yield put(GlobalActions.setTokenApiGloballySuccess());
      // yield put(NavigationActions.navigate({routeName: 'TokoScreen'}));
      // yield all([
      //   put(TokoActions.postCategoryTokoReset()),
      //   put(TokoActions.updateCategoryTokoReset()),
      //   put(TokoActions.updatePositionCategoryTokoReset()),
      //   put(TokoActions.deleteCategoryTokoReset()),
      //   put(TokoActions.tokoUpdateReset()),
      //   put(TokoActions.postProductTokoReset()),
      //   put(TokoActions.updateProductTokoReset()),
      //   put(TokoActions.deleteProductTokoReset()),
      //   put(TokoActions.tokoPostReset()),
      // ]);
      // yield put(
      //   StackActions.reset({
      //     index: 0,
      //     actions: [
      //       NavigationActions.navigate({
      //         //   routeName: "OrganizationCodeScreen"
      //         routeName: 'TokoScreen',
      //       }),
      //     ],
      //   }),
      // );
    }
  } catch (error) {
    yield put(GlobalActions.setTokenApiGloballyFailed(error));
  }
}
export function* postRefreshToken(api, data) {
  try {
    const dataPost = JSON.stringify({refresh_token: data.data.refresh_token});
    const getApi = yield call(api.postRefreshAuthToken, dataPost);
    console.log('refresh token knp', getApi);
    if (getApi.ok) {
      dateFrom = moment()
        .add(7, 'days')

        .calendar();
      yield put(GlobalActions.refreshTokenApiGloballySuccess(getApi.data.data));
      yield call(api.setAuthToken, data.data.access_token);
      AsyncStorage.setItem('bearerToken', JSON.stringify(getApi.data.data));
      yield put(GlobalActions.dueDateRefreshToken(dateFrom));

      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
      // yield put(GlobalActions.setTokenApiGloballyRequest(getApi.data.data));
      if (data.data.func) {
        yield put(data.data.func.reloadData());
      }
      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (
        getApi.status === 500 ||
        getApi.status === 400 ||
        getApi.status === 401
      ) {
        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');
        yield put(
          GlobalActions.refreshTokenApiGloballyFailed({
            message: getApi.data.message,
          }),
        );
        ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
        yield put(TokoActions.tokoReset());
      } else {
        AsyncStorage.removeItem('isLogin');
        AsyncStorage.removeItem('bearerToken');
        yield put(GlobalActions.refreshTokenApiGloballyFailed(errMsg));
        ToastAndroid.show(errMsg, ToastAndroid.SHORT);

        yield put(
          StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                //   routeName: "OrganizationCodeScreen"
                routeName: 'LoginScreen',
              }),
            ],
          }),
        );
        yield put(TokoActions.tokoReset());
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(GlobalActions.refreshTokenApiGloballyFailed({error}));
  }
}
export function* saveFirebaseToken(api, data) {
  try {
    const dataPost = JSON.stringify({token: data.data});
    const getApi = yield call(api.saveFirebaseToken, dataPost);
    console.log('firebase 123', getApi);
    if (getApi.ok) {
      yield put(GlobalActions.saveFirebaseTokenSuccess(getApi.data.data));

      //   // Go back to previous screen
      //   yield put(NavigationActions.back());
    } else {
      const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};
      if (getApi.status === 500 || getApi.status === 400) {
        yield put(
          GlobalActions.saveFirebaseTokenFailed({
            message: getApi.data.message,
          }),
        );
      } else {
        yield put(GlobalActions.saveFirebaseTokenFailed(errMsg));
      }
      // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    }
  } catch (error) {
    // ToastAndroid.show(getApi.data.message, ToastAndroid.SHORT);
    const errMsg = {message: 'Koneksi Bermasalah. Coba Lagi'};

    yield put(GlobalActions.saveFirebaseTokenFailed(errMsg));
  }
}
