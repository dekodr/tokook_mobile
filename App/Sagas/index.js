import {takeLatest, takeEvery, all} from 'redux-saga/effects';
import API from '../Services/Api';
import FixtureAPI from '../Services/FixtureApi';
import DebugConfig from '../Config/DebugConfig';

/* ------------- Types ------------- */
import {CameraTypes} from '../Redux/CameraRedux';
import {GlobalTypes} from '../Redux/GlobalRedux';
import {LanguageTypes} from '../Redux/LanguageRedux';
import {TokoTypes} from '../Redux/TokoRedux';
import {MarketPlaceTypes} from '../Redux/MarketPlaceRedux';
import {MarketPlaceNewTypes} from '../Redux/MarketplaceNewRedux';
import {LoginEmailTypes} from '../Redux/AuthenticationRedux';
/* ------------- Sagas ------------- */
import {takePicture} from './CameraSagas';
import {setToken, postRefreshToken, saveFirebaseToken} from './GlobalSagas';
import {getLanguage} from '../Sagas/LanguageSagas';
import {
  postWithdrawDisbursmentToko,
  updateToko,
  updateDefaultRekeningToko,
  postToko,
  postTokoAddress,
  postTokoBank,
  postTokoOrderProceed,
  postSendOrder,
  postFinishOrder,
  postTokoAcceptPayment,
  BusinessType,
  getToko,
  getBank,
  getTokoAddress,
  getTokoOrder,
  getTokoOrderDetail,
  getTokoBank,
  getTokoCourier,
  getOwnerTokoCourier,
  getSupplier,
  getProvince,
  getCity,
  getDistrict,
  getPostalCode,
  getDetailToko,
  getCategoryToko,
  postCategoryToko,
  updateProductToko,
  updateCategoryToko,
  updatePositionCategoryToko,
  deleteCategoryToko,
  getProductToko,
  getDetailProduct,
  postProductToko,
  deleteProductToko,
  deleteToko,
  uploadProductImage,
  getTokoFinancial,
  handlingOWnerTokoCourierStatus,
  getKycStatusToko,
  postKycToko,
  getWithdrawHistory,
  miniUpdateProductToko,
} from './TokoSagas';
import {getproductRecomendationList} from './MarketplaceNewSagas';
import {
  getProductByUrl,
  getMarketplaceProducts,
  getMarketplaceProductDetails,
  getMarketplaceTrendingProducts,
  postProductToOwnToko,
  postActivityLog,
  getMarketplaceTerlarisProducts,
  getCartData,
  postToCart,
  updateCartData,
  deleteCartData,
  getProductLastView,
  getProductBySearch,
  getTutorialBannerData,
  getAllShippingFee,
  getPaymentChannel,
  postCheckout,
  postPaymentAdminFee,
} from './MarketPlaceSagas';
import {
  postLoginEmail,
  postOtpWithdrawDisbursment,
  postLoginOtp,
  postRegisterOtp,
  deleteAuthTokenUser,
  getOtpWa,
  whatsappVerification,
  getUser,
} from '../Sagas/AuthenticationSagas';
/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    takeLatest(GlobalTypes.SET_TOKEN_API_GLOBALLY_REQUEST, setToken, api),
    takeEvery(
      GlobalTypes.REFRESH_TOKEN_API_GLOBALLY_REQUEST,
      postRefreshToken,
      api,
    ),
    takeEvery(GlobalTypes.SAVE_FIREBASE_TOKEN_REQUEST, saveFirebaseToken, api),
    takeLatest(LanguageTypes.LANGUAGE_REQUEST, getLanguage, api),
    takeEvery(TokoTypes.TOKO_POST_REQUEST, postToko, api),
    takeEvery(TokoTypes.POST_TOKO_ADDRESS_REQUEST, postTokoAddress, api),
    takeEvery(TokoTypes.POST_TOKO_BANK_REQUEST, postTokoBank, api),
    takeLatest(
      TokoTypes.UPDATE_DEFAULT_REKENING_TOKO_REQUEST,
      updateDefaultRekeningToko,
      api,
    ),

    takeLatest(TokoTypes.TOKO_UPDATE_REQUEST, updateToko, api),
    takeEvery(LoginEmailTypes.LOGIN_EMAIL_POST_REQUEST, postLoginEmail, api),
    takeEvery(
      LoginEmailTypes.OTP_WITHDRAW_DISBURSMENT_POST_REQUEST,
      postOtpWithdrawDisbursment,
      api,
    ),
    takeLatest(
      MarketPlaceNewTypes.PRODUCT_RECOMENDATION_REQUEST,
      getproductRecomendationList,
      api,
    ),

    takeLatest(CameraTypes.TAKE_PICTURE_KTP_REQUEST, takePicture, api),

    takeEvery(LoginEmailTypes.LOGIN_OTP_POST_REQUEST, postLoginOtp, api),

    takeEvery(LoginEmailTypes.REGISTER_OTP_POST_REQUEST, postRegisterOtp, api),

    takeEvery(LoginEmailTypes.GET_OTP_POST_REQUEST, getOtpWa, api),

    takeEvery(
      LoginEmailTypes.WHATSAPP_VERIFICATION_REQUEST,
      whatsappVerification,
      api,
    ),
    takeEvery(LoginEmailTypes.GET_USER_REQUEST, getUser, api),
    takeLatest(TokoTypes.GET_BUSINESS_TYPE_REQUEST, BusinessType, api),
    takeLatest(
      LoginEmailTypes.DELETE_AUTH_TOKEN_USER_REQUEST,
      deleteAuthTokenUser,
      api,
    ),

    takeLatest(TokoTypes.GET_TOKO_REQUEST, getToko, api),
    takeLatest(TokoTypes.GET_TOKO_ADDRESS_REQUEST, getTokoAddress, api),
    takeLatest(TokoTypes.GET_TOKO_ORDER_REQUEST, getTokoOrder, api),
    takeLatest(
      TokoTypes.GET_TOKO_ORDER_DETAIL_REQUEST,
      getTokoOrderDetail,
      api,
    ),
    takeLatest(TokoTypes.GET_TOKO_FINANCIAL_REQUEST, getTokoFinancial, api),
    takeLatest(TokoTypes.GET_TOKO_BANK_REQUEST, getTokoBank, api),
    takeLatest(TokoTypes.GET_TOKO_COURIER_REQUEST, getTokoCourier, api),
    takeLatest(
      TokoTypes.GET_OWNER_TOKO_COURIER_REQUEST,
      getOwnerTokoCourier,
      api,
    ),
    takeEvery(
      TokoTypes.POST_WITHDRAW_DISBURSMENT_TOKO_REQUEST,
      postWithdrawDisbursmentToko,
      api,
    ),

    takeLatest(TokoTypes.GET_BANK_REQUEST, getBank, api),

    takeLatest(TokoTypes.GET_SUPPLIER_REQUEST, getSupplier, api),

    takeLatest(TokoTypes.GET_PROVINCE_REQUEST, getProvince, api),
    takeLatest(TokoTypes.GET_CITY_REQUEST, getCity, api),
    takeLatest(TokoTypes.GET_DISTRICT_REQUEST, getDistrict, api),
    takeLatest(TokoTypes.GET_POSTAL_CODE_REQUEST, getPostalCode, api),

    takeLatest(TokoTypes.GET_DETAIL_TOKO_REQUEST, getDetailToko, api),
    takeLatest(TokoTypes.GET_CATEGORY_TOKO_REQUEST, getCategoryToko, api),
    takeEvery(TokoTypes.POST_CATEGORY_TOKO_REQUEST, postCategoryToko, api),
    takeEvery(
      TokoTypes.POST_TOKO_ORDER_PROCEED_REQUEST,
      postTokoOrderProceed,
      api,
    ),
    takeEvery(
      TokoTypes.POST_TOKO_ACCEPT_PAYMENT_REQUEST,
      postTokoAcceptPayment,
      api,
    ),
    takeEvery(TokoTypes.POST_TOKO_FINISH_ORDER_REQUEST, postFinishOrder, api),

    takeEvery(TokoTypes.POST_TOKO_SEND_ORDER_REQUEST, postSendOrder, api),
    takeEvery(TokoTypes.UPLOAD_PRODUCT_IMAGE_REQUEST, uploadProductImage, api),
    takeLatest(TokoTypes.GET_PRODUCT_TOKO_REQUEST, getProductToko, api),
    takeLatest(TokoTypes.GET_DETAIL_PRODUCT_REQUEST, getDetailProduct, api),
    takeEvery(TokoTypes.POST_PRODUCT_TOKO_REQUEST, postProductToko, api),
    takeLatest(TokoTypes.UPDATE_PRODUCT_TOKO_REQUEST, updateProductToko, api),
    takeLatest(
      TokoTypes.MINI_UPDATE_PRODUCT_TOKO_REQUEST,
      miniUpdateProductToko,
      api,
    ),
    takeEvery(TokoTypes.DELETE_PRODUCT_TOKO_REQUEST, deleteProductToko, api),
    takeEvery(TokoTypes.UPDATE_CATEGORY_TOKO_REQUEST, updateCategoryToko, api),
    takeEvery(
      TokoTypes.UPDATE_POSITION_CATEGORY_TOKO_REQUEST,
      updatePositionCategoryToko,
      api,
    ),

    takeEvery(TokoTypes.DELETE_CATEGORY_TOKO_REQUEST, deleteCategoryToko, api),

    takeEvery(TokoTypes.DELETE_TOKO_REQUEST, deleteToko, api),
    //market place
    takeLatest(
      MarketPlaceTypes.GET_PRODUCT_BY_URL_REQUEST,
      getProductByUrl,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_ALL_SHIPPING_FEE_REQUEST,
      getAllShippingFee,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_PAYMENT_CHANNEL_REQUEST,
      getPaymentChannel,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_PRODUCT_BY_SEARCH_REQUEST,
      getProductBySearch,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.POST_PRODUCT_TO_OWN_TOKO_REQUEST,
      postProductToOwnToko,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.POST_PAYMENT_ADMIN_FEE_REQUEST,
      postPaymentAdminFee,
      api,
    ),

    takeLatest(MarketPlaceTypes.POST_CHECKOUT_REQUEST, postCheckout, api),
    takeLatest(
      MarketPlaceTypes.POST_ACTIVITY_LOG_REQUEST,
      postActivityLog,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_MARKETPLACE_PRODUCTS_REQUEST,
      getMarketplaceProducts,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_MARKETPLACE_PRODUCT_DETAILS_REQUEST,
      getMarketplaceProductDetails,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_PRODUCT_LAST_VIEW_REQUEST,
      getProductLastView,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_MARKETPLACE_TRENDING_PRODUCTS_REQUEST,
      getMarketplaceTrendingProducts,
      api,
    ),
    takeLatest(
      MarketPlaceTypes.GET_MARKETPLACE_TERLARIS_PRODUCTS_REQUEST,
      getMarketplaceTerlarisProducts,
      api,
    ),
    //cart
    takeEvery(MarketPlaceTypes.GET_CART_DATA_REQUEST, getCartData, api),
    takeEvery(
      MarketPlaceTypes.GET_TUTORIAL_BANNER_DATA_REQUEST,
      getTutorialBannerData,
      api,
    ),
    takeEvery(MarketPlaceTypes.POST_TO_CART_REQUEST, postToCart, api),
    takeEvery(MarketPlaceTypes.UPDATE_CART_DATA_REQUEST, updateCartData, api),
    takeEvery(MarketPlaceTypes.DELETE_CART_DATA_REQUEST, deleteCartData, api),

    takeEvery(
      TokoTypes.HANDLING_OWNER_TOKO_COURIER_STATUS_REQUEST,
      handlingOWnerTokoCourierStatus,
      api,
    ),
    takeEvery(TokoTypes.POST_KYC_TOKO_REQUEST, postKycToko, api),

    takeEvery(TokoTypes.GET_KYC_STATUS_TOKO_REQUEST, getKycStatusToko, api),
    takeEvery(
      TokoTypes.GET_WITHDRAW_HISTORY_TOKO_REQUEST,
      getWithdrawHistory,
      api,
    ),
  ]);
}
