import {call, put} from 'redux-saga/effects';
import MarketplaceNewActions from './../Redux/MarketplaceNewRedux';

import {MARKETPLACE_PRODUCT_RECOMENDATION} from '../Constants';

// attempts to login
export function* getproductRecomendationList(api, {payload, append}) {
  const params = {
    ...payload,
    name: MARKETPLACE_PRODUCT_RECOMENDATION,
  };
  const response = yield call(api.getMarketplaceTrendingProducts, params);
  if (response.ok) {
    if (append) {
      yield put(
        MarketplaceNewActions.productRecomendationRequestAppend(response.data),
      );
    } else {
      yield put(
        MarketplaceNewActions.productRecomendationRequestSuccess(response.data),
      );
    }
  } else {
    yield put(
      MarketplaceNewActions.productRecomendationRequestFailed(response),
    );
  }
}
