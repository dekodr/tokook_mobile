import {call, put} from 'redux-saga/effects';
import CameraActions from './../Redux/CameraRedux';

import {ToastAndroid} from 'react-native';

export function* takePicture(api, {data}) {
  try {
    if (data.type === 1) {
      yield put(CameraActions.takePictureKtpSuccess(data.data));
    } else {
      yield put(CameraActions.takePictureKtpSelfieSuccess(data.data));
    }
    //.
  } catch (errors) {
    yield put(CameraActions.takePictureKtpFailed(errors));
  }
}
